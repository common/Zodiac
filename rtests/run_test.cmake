set( output_test output.log )

message( "test name: " ${base_name} )
message( " test dir: " ${test_dir} )
message( "   gs dir: " ${gs_dir} )

execute_process(
        COMMAND ${test_cmd} ${input_file}
        OUTPUT_FILE ${output_test}
        RESULT_VARIABLE test_result )

file( GLOB tests ${test_dir}/${base_name}* )
#message( "\nTests:\n" )
#foreach( test ${tests} )
##  file( REMOVE_RECURSE ${test} )
#  message( "\t ${test}" )
#endforeach()

if( test_result )
    message( SEND_ERROR "zodiac failed with error code ${test_result}" )
endif()

string( COMPARE NOTEQUAL "${diff_cmd}" "" DO_COMPARISON )

if( ${DO_COMPARISON} )

    foreach( outfile ${tests} )

      get_filename_component( test_name ${outfile} NAME )
      set( outfile_blessed ${gs_dir}/${test_name} )
      execute_process( COMMAND ${diff_cmd} ${outfile} ${outfile_blessed} RESULT_VARIABLE test_result )

      if( test_result )
        message( SEND_ERROR "${outfile} and ${outfile_blessed} are different!" )
      endif()
  endforeach()
else()
  message( "NOT PERFORMING COMPARISON because no GOLD_STANDARDS_DIR was specified" )
endif()
