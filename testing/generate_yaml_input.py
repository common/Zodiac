#! /usr/bin/python3

import yaml
#
# data = {'Cantera': {'GroupName': 'h2-burke', 'InputFile': 'h2-burke.xml'},
#  'InitialConditions': [{'variable': 'p', 'value': 101325},
#                        {'variable': 'tau_mix', 'value': 0.1},
#                        {'variable': 'T',
#                         'value': {'maximum': 2000,
#                                   'minimum': 300,
#                                   'npts': 512}},
#                        {'variable': 'X_H2', 'value': 0.6667},
#                        {'variable': 'X_O2', 'value': 0.3333}],
#  'Output': {'dualtime_interval': 1,
#             'time_interval': 1,
#             'Fields': [{'name': 'T', 'alias': 'temperature'},
#                        {'name': 'lambda+'},
#                        {'name': 'p'}]},
#  'TimeIntegrator': {'GESAT_Max': '1e0',
#                     'GESAT_Ramp': 1.1,
#                     'GESAT_Safety': 0.1,
#                     'end_time': 10,
#                     'timestep': '1e305',
#                     'tolerance': '1e-8'}}
#
# with open('generatedTestInput.yaml', 'w') as outfile:
#     yaml.dump(data, outfile, default_flow_style=False)

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('outfile', type=str)
parser.add_argument('-i', '--initConditions', type=float)
parser.add_argument('-t', '--timeIntegrator', type=float)
parser.add_argument('-o', '--output', type=str)
args = parser.parse_args()

fname = args.outfile

#fname = "generatedTestInput.yaml"

stream = open(fname, 'r')
data = yaml.load(stream)

data['InitialConditions'][1]['value'] = args.initConditions
data['TimeIntegrator']['timestep'] = args.timeIntegrator
data['Output']['Fields'][0]['alias'] = args.output

#data['InitialConditions'][1]['value'] = 0.1
#data['TimeIntegrator']['timestep'] = '1e305'
#data['Output']['Fields'][0]['alias'] = 'temperature'

with open(fname, 'w') as yaml_file:
    yaml_file.write(yaml.dump(data, default_flow_style=False))
