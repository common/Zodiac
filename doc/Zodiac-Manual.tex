\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,graphicx}
\usepackage[justification=centering]{caption}
\usepackage{fancyvrb}
\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\begin{document}

\title{Zodiac User Manual}
\author{}
\maketitle

\section{Input File}

Input specifications are done using YAML, and are separated into seven main blocks: Cantera, InflowConditions, InitialConditions, TimeIntegrator, Output, ReactorParameters and Restart.
Example input files with README file instructions on how to run and postprocess different cases using Zodiac can be found in the demos folder.

\subsection{Cantera}
A path to the xml file (InputFile) and the group name (GroupName) are listed in the Cantera block.
In the xml file, there should be an 'id=...' which relays what group name to specify.
For example, in some demos, a hydrogen-burke mechanism is included with 'id=h2-burke' inside the included xml file.
The specifications for this mechanism look like:
\begin{Verbatim}
Cantera:
    InputFile: h2-burke.xml
    GroupName: h2-burke
\end{Verbatim}

\subsection{ReactionInGas}
This block could be set to be `True`('true') or `False`. If it is `True` or 'true', the reaction in gas phase is considered in the process. If it is `False`, the reaction in gas phase is not considered. 

This block is not necessary. If it is not given, the default for it is `True`, that is, reaction in the gas is considered.

The specifications for this look like:
\begin{Verbatim}
ReactionInGas: True
\end{Verbatim}

\subsection{Initial Conditions}
Initial conditions can include a maximum of three non-constant variables, but the postprocessing tools currently only support visualizing results from varying two parameters at most, including the inflow parameters, and the number of grid for these two parameters must be the same.
The variables to choose from include pressure in Pascals (p), temperature in Kelvin (T) and species mole fractions (X\_H2, X\_O2, etc.).
Mass fractions and mixture fraction are not currently supported.
These variables are specified inside the InitialConditions block and the names given must match those shown in the parenthesis above (aliases can be given for the Output variables described in section 1.5).

This block is not necessary. If InitialConditions is not given, it will be set to be the same as the inflow conditions.

An example for hydrogen combustion, running 12 different cases varying the initial temperature between 800 and 1400 K and the pressure between 10132.5 Pa and one atmospheric pressure with a stoichiometric hydrogen-oxygen mixture might look like:
\begin{Verbatim}
InitialConditions:
    - variable: T
      value:
       minimum : 800
       maximum : 1400
       npts    : 12
    - variable: p
      value:
       minimum : 10132.5
       maximum : 101325
       npts    : 12
    - variable: X_H2
      value   : 0.6667
    - variable: X_O2
      value   : 0.3333
\end{Verbatim}

\subsection{Inflow Conditions}
Like initial conditions, inflow conditions can include a maximum of three non-constant variables. And the maximum of the total number of non-constant variables in InitialConditions and InflowConditions is three.
The variables to choose from include pressure in Pascals (p\_inflow), temperature in Kelvin (T\_inflow), species mole fractions (X\_H2\_inflow, X\_O2\_inflow, etc.) and and exponent of residence time in seconds (tau\_mix\_log). 
The tau\_mix\_log gives the exponent of residence time(tau\_mix): $tau\_mix = 10^{tau\_mix\_log}$.
Mass fractions are not currently supported.
However, the mixture composition could be given by mixture fraction, instead of mole fraction. ( The example using mixture fraction to give the inflow compositions could be found in `input` directory).
These variables are specified inside the InflowConditions block and the names given must match those shown in the parenthesis above. 
An example for hydrogen combustion, running 12 different cases varying the inflow temperature between 800 and 1400 K at atmospheric pressure with a stoichiometric hydrogen-oxygen mixture and residence time of $10^{-3}$ might look like:
\begin{Verbatim}
InflowConditions:
    - variable: tau_mix_log
      value   : -3
    - variable: p_inflow
      value   : 101325
    - variable: X_H2_inflow
      value   : 0.6667
    - variable: X_O2_inflow
      value   : 0.3333
    - variable: T_inflow
      value:
       minimum : 800
       maximum : 1400
       npts    : 12
\end{Verbatim}

\subsection{Time Integration}
The time integration parameters are specified inside the TimeIntegrator block.
This block is optional for steady state solves.
The parameters that may be specified include
the maximum step size in dualtime (GESAT\_Max, default 1.0),
the safety factor for dualtime integration (GESAT\_Safety, default 0.1),
the ramp value for dualtime integration (GESAT\_Ramp, default 1.1),
the tolerance to determine when convergence is met in dualtime (tolerance, default 1e-4), 
the timestep (timestep, default 1e305 for steady state solution),
and the end time (end\_time, default 1e305 for steady state solution).
An example of a block describing transient time integration parameters for a physical end time of $250 \mu s$  could be:
\begin{Verbatim}
TimeIntegrator:
    GESAT_Max   : 1.0
    GESAT_Safety: 0.1
    GESAT_Ramp  : 1.1
    tolerance   : 1e-8
    timestep    : 1e-6
    end_time    : 2.5e-4 
\end{Verbatim}

\subsection{Output}
The interval for outputting the state in dual time (dualtime\_interval) and in physical time (time\_interval) are specified in the Output block.
In addition, fields can be specified for output if they are not output by default.
Fields that are output by default include all the species mass fractions (Y\_O2, Y\_H2O, etc.), temperature (T), the explosive eigenvalue (lambda+), density (rho), pressure (p), the dual timestep (ds), and the physical timestep (dt). There is no default output for particles.

If the inflow temperature or inflow pressure is set to be the non-constant variable, fields 'T\_inflow' or 'p\_inflow' must be specified for output in this block.

Examples for fields that may be requested for output include volumetric energy (rhoegy)(constant volume condition), specific energy (egy)(constant volume condition), mixture molecular weight (Mmix), the rate of convective heat transfer (convective-rate), enthalpy(constant pressure condition).
Aliases for the fields can also be made if you want to label them differently for postprocessing.
An example Output block that specifies outputting every timestep in dual time and physical time as well as requesting the mixture molecular weight and inflow temperature for output and giving an alias of 'temperature' to the 'T' field is as follows:
\begin{Verbatim}
Output:
    time_interval    : 1
    dualtime_interval: 1
    Fields:
     - name: Mmix
     - name: T_inflow
     - {name: T, alias: temperature}
\end{Verbatim}

\subsection{ReactorParameters}
This is for specifying variables that involve reactor type, reactor radius and convective heat transfer.
The options for this include
reactor type( ReactorType: ConstantPressure or ConstantVolume. This term must be declared in the input file),
the reactor radius in meters (Radius, default 0.001),
the heat transfer coefficient for convection in $W/m^2$ (HeatTransferCoefficient, default 0),
and  the temperature of the surroundings in Kelvin (SurroundingsTemperature, default 300).
Therefore, these default values do not include convective heat transfer in the solution.
An example for the ReactorParameters block using the default values for constant volume condition is given below.
\begin{Verbatim}
ReactorParameters:
    ReactorType            : ConstantVolume
    Radius                 : 0.001
    HeatTransferCoefficient: 0
    SurroundingsTemperature: 300
\end{Verbatim}

\subsection{Restart}
The "Restart" block could be set to be "True" to set the initial conditions based on the previous Zodiac run and run Zodiac again. For example, to get S-Curve for combustion, setting the initial conditions as the equilibrium conditions based on the previous run could generate the extinction part in this curve.
To restart zodiac, it is needed to link the executable file 'zodiac' in the bin directory inside the build directory to present working directory. The link process could be done in terminal, which may look like:
\begin{Verbatim}
ln -s /Users/hang/Zodiac/build/bin/zodiac zodiac
\end{Verbatim}
When " Restart: True", it will generate a directory, named "Restart" in present working directory, then write a new input file, named "Restart\_input.yaml", copy needed .xml file from present working directory to "Restart" directory and save all results of the second run in this directory "Restart".
This block for this example looks like:
\begin{Verbatim}
Restart: True
\end{Verbatim}
if just run Zodiac once, this block does not need to be given or looks like:
\begin{Verbatim}
Restart: False
\end{Verbatim}

\subsection{Particles}
The main parameters in this block includes `ParticleType`, `InitialConditions`, NumberOfParticles`, `Size` and `ParticleParameters`.
Three types of particle could be given in Zodiac: Solid, Liquid(droplet) and Coal. 
`InitialConditions` gives the initial particle temperature `Tpart\_init`, density `rho\_part\_init` and heat capacity `cp\_part\_init`. Particle inflow parameters are set to be the same as initial parameters. Now, heat capacity and density of solid and liquid particles are set to be constant.
`NumberOfParticles` gives the number of particles in the reactor(default 1).
'Size` gives particle size. Five distributions could be given in Zodiac: `Identity`(all particles have the same size), 'Uniform'(Particle sizes distribute uniformly between `Lower` and 'Upper`), `Normal`(particle sizes distribution is normal distribution), 'LogNormal`(particle sizes distribution is log normal distribution), `Weibull`(particle sizes distribution is Weibull distribution).
`ParticleParameters` mainly gives radiation parameters for particles: particle emissivity(default 0.0) and radiation temperature(K, default 500). 
This block is only needed when particles are added into reactor, and all particles are added under constant pressure condition. An example for solid particle with identity size is given below.
\begin{Verbatim}
Particles:
    ParticleType: Solid
    InitialConditions:
      - value: 350
        variable: TPart_init
      - value: 1000
        variable: rho_part_init
      - value: 4200
        variable: cp_part_init
    NumberOfParticles: 1
    Size:
      Distribution: Same
      Value: 1e-4
    ParticleParameters:
      EmissivityPart: 1e-1
      RadiationTemperature: 1200
\end{Verbatim}

\subsection{FlowParameters}
This is mainly for specifying variable of KolmogorovScale. It is needed to involve turbulent effect on particles and to calculate particle Reynolds number. An example for this block is give below:
\begin{Verbatim}
FlowParameters:
    KolmogorovScale: 1e-8
\end{Verbatim}

\section{Running Zodiac}

To run Zodiac from the command line, give the path to the 'zodiac' executable followed by the path to the YAML input file.
The executable can be found in the bin directory inside the build directory that was created.
Running this for a transient case will generate a database for each time step taken containing the converged solution.
Running this for a steady state case will generate a single database with all the iterations taken in dual time to reach steady state.
These databases will be output in whatever directory the 'zodiac' executable was run in.

\section{Postprocessing}

A python script titled 'zodiac\_postproc.py' can be found inside the postproc folder.
This can be run from the terminal using a python 3 version.
Plotly must also be installed, which can be done using 'pip'.
The postprocessing script needs to be run from inside the directory containing the databases output from running the 'zodiac' executable.
This script currently supports visualizing one and two dimensional data.
To run this script, a path to the 'LoadDatabase.py' file must be appended to your 'PYTHONPATH'. This file is found inside 'Zodiac/build/tpl/src/ExprLib\_BUILD/util/'. The file path can be added to your 'PYTHONPATH' with the following line:

\begin{Verbatim}
export PYTHONPATH=$PYTHONPATH:[path to Zodiac]/build/tpl/src/ExprLib_BUILD/util
\end{Verbatim}
As an example, this might look like:

\begin{Verbatim}
export PYTHONPATH=$PYTHONPATH:/Users/elizabeth/
		Zodiac/build/tpl/src/ExprLib_BUILD/util
\end{Verbatim}

This line can be run each time a new terminal window is used for postprocessing, or it can be added to your bash\_profile to always be included when using python.

Arguments for the postprocessing script include the independent and dependent variables to plot specified in the form of "independent variables separated by commas ; dependent variables separated by commas" (plotvars, option: -pv), the end time for a transient case (end\_time, option: -et), the timestep taken for a transient case (timestep, option: -ts), and the interval used for outputting data in a transient case (time\_interval, option: -ti, default = 1).
For the steady state case, only the variables for plotting need to be specified.
In the transient cases, the other arguments listed need to match whatever was specified in the input file in order to correctly access all the databases (however, a larger time\_interval may be specified than was used in the input file in order to plot the state less often).


To run the postprocessing script from the command line, specify a python 3 path followed by the path to 'zodiac\_postproc.py' followed by the arguments for plotting described above.
Typing the option '-{}-help' in the place of the arguments for plotting will bring up a summary of all the options for plotting. 


For 1D cases, up to four different types of dependent variables can be specified which will be plotted using a single plot with four different axes.
Up to 20 different mass fractions can be specified, which will be plotted on a single axis.
A dropdown menu in the upper left corner can be used to select different variables to view from the list provided in the command line.
For the 2D cases, an unlimited number of dependent variables may be specified, each for which an additional plot will be generated and available for selection to view from a dropdown menu.
In both cases, a slider at the bottom of the plot can be used to access different times/iterations of the transient or steady state solve.
Plotly also provides options in the upper right corner for saving, zooming, etc.

For example, using the option
\begin{Verbatim}
-pv "T ; T,p,Y_H2O,Y_H2"
\end{Verbatim}
for a 1D case will bring up an animated line plot of the mass fractions of hydrogen and water over the initial temperature moving over each iteration in the steady state solve.
The dropdown menu will also contain plots for the temperature and pressure over the initial temperature.

If instead, for a 2D case, the option
\begin{Verbatim}
-pv "T,p ; T,p,Y_H2O,Y_H2"
\end{Verbatim}
were used, this would bring up an animated contour plot of the temperature over the initial temperature and pressure moving over each iteration in the steady state solve.
The dropdown menu would then contain plots for the pressure, water mass fraction, and hydrogen mass fraction over the initial temperature and pressure.

If " Restart: True" and tau\_mix or T\_inflow is set as the independent variable (just for this 1D case), a plot, T vs. tau\_mix or T vs. T\_inflow, including both ignition and extinction curves, could be saved in the "Restart" directory, named "S-Curve.png".


\end{document}