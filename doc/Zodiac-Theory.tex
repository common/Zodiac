\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,graphicx}
\usepackage[justification=centering]{caption}

\newcommand{\Tinf}{T_{\text{inf}}}
\newcommand{\ddt}[1]{\frac{d #1}{dt}}
\newcommand{\pder}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\Vdot}{\dot V}
\newcommand{\VV}{\frac{\Vdot}{V}}
\newcommand{\Tmix}{\tau_{\text{mix}}}
\newcommand{\tin}{\text{in}}
\newcommand{\Nidot}{\dot N_i}
\newcommand{\NiNi}{\frac{\Nidot}{N_i}}

\begin{document}

\title{Zodiac Theory}
\author{}
\maketitle

\section{The Equations Zodiac Solves Under Constant Volume Condition}

We will consider an open, rigid reactor with surface area $A$, volume $V$, and heat transfer coefficient $k$, that has air flowing over it with temperature $\Tinf$.
\begin{align} 
\ddt{(\rho V)} &= \rho_\tin \Vdot - \rho \Vdot, \label{eqn: drhoVdt} \\
\ddt{(\rho Y_i V)} &= (\rho Y_i)_{\tin}\Vdot - (\rho Y_i) \Vdot + \omega_i V, \label{eqn: drhoYiVdt} \\
\ddt{(\rho e V)} &= (\rho e)_{\tin} \Vdot - (\rho e) \Vdot - kA(T - \Tinf), \label{eqn: deVdt} 
\end{align}
where $\rho$ is density and $\Vdot$ is volumetric flow rate, $\omega_i$ is the net mass production rate, $Y_i$ is mass fraction of species $i$, $e$ is energy per unit mass and $T>\Tinf$.

Since the volume and volumetric flow rates are constant, we can rearrange Equation \eqref{eqn: drhoVdt}, \eqref{eqn: drhoYiVdt} and \eqref{eqn: deVdt} to get 
\begin{align} 
\ddt{\rho} &= \VV(\rho_\tin - \rho), \label{eqn: drhoVdt_V} \\
\ddt{(\rho Y_i)} &= \VV((\rho Y_i)_{\tin} - (\rho Y_i)) + \omega_i,  \label{eqn: drhoYiVdt_V} \\
\ddt{(\rho e)} &= \VV((\rho e)_{\tin} - (\rho e)) - \frac{kA(T - \Tinf)}{V}, \label{eqn: deVdt_V}
\end{align}

Since the volumetric flow rate over the volume is inverse mixing residence time, $\Tmix$, we rearrange Equation \eqref{eqn: drhoVdt_V}, \eqref{eqn: drhoYiVdt_V} and \eqref{eqn: deVdt_V} to get
\begin{align} 
\ddt{\rho} &= \frac{\rho_\tin - \rho}{\Tmix},  \label{eqn:drhodt_final} \\
\ddt{(\rho Y_i)} &= \frac{(\rho Y_i)_{\tin} - (\rho Y_i)}{\Tmix} + \omega_i, \label{eqn:drhoYidt_final} \\
\ddt{(\rho e)} &= \frac{(\rho e)_{\tin} - (\rho e)}{\Tmix} - \frac{kA(T - \Tinf)}{V},  \nonumber \\
&= \frac{(\rho e)_{\tin} - (\rho e)}{\Tmix} - \frac{3k(T - \Tinf)}{r} \label{eqn:drhoedt_final}
\end{align}

These equations are our final equations solved in Zodiac for constant volume. Among them, equation \eqref{eqn:drhoYidt_final} is our final species balance equation for $n_s - 1$ species, where $n_s$ denotes number of species.

\subsection{Summary}
To summarize the equations in vector form we are solving for constant volume,
\begin{equation}
    \ddt{}
    \left[ \begin{array}{c}
        \rho \\
        \rho e \\
        \rho Y_i \\
     \end{array} \right] 
    =
    \underbrace{ \left[ \begin{array}{c}
        \frac{\rho_\tin - \rho}{\Tmix} \\
       \frac{(\rho e)_{\tin} - (\rho e)}{\Tmix}  \\
        \frac{(\rho Y_i)_{\tin} - (\rho Y_i)}{\Tmix} \\
    \end{array} \right] }_{W}
    + 
    \underbrace{\left[ \begin{array}{c}
        0 \\
        0 \\
        \omega_i \\
    \end{array} \right]}_{K}
    +
    \underbrace{\left[ \begin{array}{c}
        0 \\
        - \frac{3k}{r} (T - \Tinf) \\
        0 \\
    \end{array} \right]}_{Q}
    \label{eqn: dudt array CV}
\end{equation}
or
\begin{equation} 
	\frac{\mathrm{d}U}{\mathrm{d}t} = W + K + Q \label{eqn: dudt generic CV}
\end{equation}
where  $U = (\rho, \rho e, \rho Y_1, ...,\rho Y_{n_s-1})^\mathrm{T}$.  Here, $W$, $K$ and $Q$ represent mixing, kinetics and heat transfer, respectively.

\subsection{Jacobian Matrix}
The Jacobian $J=\pder{W}{U}+\pder{K}{U}+\pder{Q}{U}$ need to be formed.  
In some cases, it is easier to obtain Jacobians in a different set of variables, $V$. For mixing term $W$, it is easier to get $\pder{W}{U}$ directly. However, it is easier to get $\pder{K}{U}$ and $\pder{Q}{U}$ from $\pder{K}{V}$ and $\pder{Q}{V}$.
In our case, we choose 
\begin{equation*}
    V=\left[\rho, T, Y_i\right]^T
\end{equation*}
so that the Jacobian is 
\begin{equation}
    J = \pder{W}{U}  + \left[\pder{Q}{V} + \pder{K}{V} \right]\pder{V}{U}.
\end{equation}
%
The transformation matrix, $\pder{V}{U}$ is given as
\begin{align} 
  \pder{V}{ U}  
  & = 
  \left[\begin{array}{ccc}
     \left. \pder{\rho}{\rho}\right|_{\rho e, \rho Y_i} & \left. \pder{\rho}{\rho e} \right|_{\rho, \rho Y_i} & \left. \pder{\rho}{\rho Y_i} \right|_{\rho, \rho e, \rho Y_{j \ne i, ns}} \\
     \left. \pder{T}{ \rho}\right|_{\rho e, \rho Y_i} & \left. \pder{T}{\rho e}\right|_{\rho, \rho Y_i} & \left. \pder{ T}{ \rho Y_i}\right|_{\rho, \rho e,  \rho Y_{j \ne i, ns}} \\
    \left. \pder{Y_i}{\rho}\right|_{\rho e, \rho Y_i} & \left. \pder{Y_i}{\rho e}\right|_{\rho, \rho Y_i} & \left. \pder{Y_i}{\rho Y_i}\right|_{\rho, \rho e,  \rho Y_{j \ne i, ns}} \\
  \end{array}\right] \\
  &= 
  \left[\begin{array}{ccc}
    1 & 0 & 0 \\
    -\frac{e_{ns}}{\rho Cv} & \frac{1}{\rho Cv} & \frac{e_{ns}-e_1}{\rho Cv} \\
   -\frac{Y_i}{\rho} & 0 & \frac{1}{\rho} 
  \end{array}\right]
  \label{eq:dVdU_CV}
\end{align}
%
And the Jacobian matrices $\pder{W}{U}$, $\pder{K}{V}$ and $\pder{Q}{V}$ are
\begin{equation} 
  \pder{W}{U} 
  = 
  \left[\begin{array}{ccc}
    -\frac{1}{\tau_{mix}} & 0 & 0 \\
    0 & -\frac{1}{\tau_{mix}}&0 \\
    0 & 0 &-\frac{1}{\tau_{mix}} 
   \end{array} \right]
   \label{eq:dWdU_CV}
\end{equation}
%
\begin{equation} 
  \pder{ Q}{V} 
   = 
   \left[ \begin{array}{ccc}
    0 & 0 & 0 \\
    0 & -\frac{3k}{r} &0 \\
    0 & 0 & 0
  \end{array}\right]
  \label{eq:dQdV_CV}
\end{equation} 
%
\begin{equation} 
  \pder{K}{V}  = 
    \left[ \begin{array}{ccc}
     0 & 0 & 0 \\
     0 & 0& 0 \\
     \left. \pder{\omega_i}{\rho}\right|_{T, Y_i} & \left. \pder{\omega_i}{T}\right|_{\rho, Y_i} & \left. \pder{\omega_i}{Y_i}\right|_{\rho, T, Y_{j \ne i, ns} }\\
  \end{array}\right] 
  \label{eq:dKdV_CV}
\end{equation}

\section{Governing Equations Under Constant Pressure Condition}

As constant volume condition, we will consider an open, rigid reactor with radius $r$, and heat transfer coefficient $k$, that has air flowing over it with temperature $\Tinf$.

\begin{align}
    \ddt{p} &= 0, \label{eqn:dpdt} \\
    \ddt{m} &= \dot m _{\tin} - \dot m, \label{eqn:dmdt} \\
    \ddt{(m Y_i )} &= (\dot m Y_i )_{\tin} - (\dot m Y_i) + \omega_i V, \label{eqn:dmidt} \\
    \ddt{(m h)} &= (\dot m h)_{\tin} - (\dot m h) - kA(T - \Tinf), \label{eqn:dhdt}
\end{align}
where $m$ and $\dot{m}$ are the mass in the reactor and mass flow rate respectively, $\omega_i$ and $Y_i$ are the net mass production rate and mass fraction of species $i$ respectively, $V$ is the volume of the reactor, $k$ is the convective coefficient and $T>\Tinf$.

Manipulating \eqref{eqn:dmidt}, we find
\begin{align}
    m\ddt{Y_i } + Y_i \ddt{m} &= \dot{m}_\tin Y_{i,\tin} - \dot{m} Y_i + \omega_i V, \nonumber \\
    m\ddt{Y_i} &= \dot m_{\tin}(Y_{i,\tin} - Y_i) + \omega_i V. \nonumber \\
    \ddt{Y_i} &= \frac{ \rho_{\tin} \dot V_{\tin}}{\rho V}(Y_{i,\tin} - Y_i) + \frac{\omega_i}{\rho}. \nonumber \\
    \ddt{Y_i} &= \frac{ \rho_{\tin} }{\rho \Tmix}(Y_{i,\tin} - Y_i) + \frac{\omega_i}{\rho}. \label{eqn:spec-mass-final}
\end{align}
Equation \eqref{eqn:spec-mass-final} is our final species balance equation for $n_s - 1$ species, where $n_s$ denotes number of species. $\Tmix = \frac{V}{\dot V_{in}}$ is based on the inlet flow rate.

Similarly, \eqref{eqn:dhdt} can be rewritten to obtain 
\begin{equation}
    \ddt{h} = \frac{ \rho_{\tin} }{\rho \Tmix }(h_{\tin} - h) - \frac{3k}{\rho r} (T - \Tinf). \label{eqn:enthalpy-final}
\end{equation}
This is our final enthalpy balance equation.

\subsection{Summary}
To summarize the equations we are solving for constant pressure,
\begin{align}
    \ddt{p} &= 0, \tag{\ref{eqn:dpdt}} \\
    \ddt{Y_i} &= \frac{ \rho_{\tin} }{\rho \Tmix}(Y_{i,\tin} - Y_i) + \frac{\omega_i}{\rho}. \tag{\ref{eqn:spec-mass-final}} \\
    \ddt{h} &= \frac{ \rho_{\tin} }{\rho \Tmix }(h_{\tin} - h) - \frac{3k}{\rho r} (T - \Tinf). \tag{\ref{eqn:enthalpy-final}}
\end{align}
In vector form,
\begin{equation}
    \ddt{}
    \left[ \begin{array}{c}
        p \\
        h \\
        Y_i \\
     \end{array} \right] 
    =
    \underbrace{ \left[ \begin{array}{c}
        0 \\
        \frac{ \rho_{\tin} }{\rho \Tmix}(h_{\tin} - h)  \\
        \frac{ \rho_{\tin} }{\rho \Tmix}(Y_{i,\tin} - Y_i) \\
    \end{array} \right] }_{W}
    + 
    \underbrace{\left[ \begin{array}{c}
        0 \\
        0 \\
        \frac{\omega_i}{\rho} \\
    \end{array} \right]}_{K}
    +
    \underbrace{\left[ \begin{array}{c}
        0 \\
        - \frac{3k}{\rho r} (T - \Tinf) \\
        0 \\
    \end{array} \right]}_{Q}
    \label{eqn: dudt array CP}
\end{equation}
or
\begin{equation} 
	\frac{\mathrm{d}U}{\mathrm{d}t} = W + K + Q \label{eqn: dudt generic CP}
\end{equation}
where  $U = (p, h, Y_1, ..., Y_{n_s-1})^\mathrm{T}$.  Here, $W$, $K$ and $Q$ represent mixing, kinetics and heat transfer, respectively.

\subsection{Jacobian Matrix}

The Jacobian $J=\pder{W}{U}+\pder{K}{U}+\pder{Q}{U}$ need to be formed.  Like constant volume in some cases, it is easier to obtain Jacobians in a different set of variables, $V$.  
In our case, we choose 
\begin{equation*}
    V=\left[\rho, T, Y_i\right]^T
\end{equation*}
so that the Jacobian is 
\begin{equation}
    J = \left[ \pder{W}{V} + \pder{Q}{V} + \pder{K}{V} \right]\pder{V}{U}.
\end{equation}
%
The transformation matrix, $\pder{V}{U}$ is given as
\begin{align}
    \pder{V}{U}  
    &= 
    \left[ \begin{array}{ccc}
        \left. \pder{\rho}{p} \right|_{h, Y_i} & \left. \pder{\rho}{h} \right|_{p, Y_i} & \left. \pder{\rho}{Y_i} \right|_{p, h, Y_{j \ne i, ns}} \\
        \left. \pder{T}{p} \right|_{h, Y_i} & \pder{T}{h}|_{p, Y_i} & \pder{T}{Y_i}|_{p, h, Y_{j \ne i, ns}} \\
        \left. \pder{Y_i}{p} \right|_{h, Y_i} & \left. \pder{Y_i}{h} \right|_{p, Y_i} & \left. \pder{Y_i}{Y_i} \right|_{p, h, Y_{j \ne i, ns}} \\
    \end{array} \right] \nonumber \\
    &=
    \left[ \begin{array}{ccc}
        \frac{\rho}{p} & -\frac{\rho}{CpT} & \frac{\rho}{Cp T} \left( h_i - h_{ns} \right) - \rho M \left( \frac{1}{M_i}-\frac{1}{M_{ns}} \right) \\
        0 & \frac{1}{Cp} & \frac{h_{ns}-h_i}{Cp} \\
        0 & 0 & 1
    \end{array}\right].
    \label{eq:dVdU_CP}
\end{align}
%
And the Jacobian matrices $\pder{W}{V}$, $\pder{K}{V}$ and $\pder{Q}{V}$ are
\begin{align}
    \pder{W}{V} 
    &=
    \left[\begin{array}{ccc}
        0 &0 & 0\\
        \left. \pder{W_h}{\rho} \right|_{T, Y_i}& \left. \pder{W_h}{T} \right|_{\rho, Y_i} & \left. \pder{W_h}{Y_i}\right|_{\rho, T, Y_{j\ne i, ns}} \\
        \left. \pder{W_{Y_i}}{\rho} \right|_{T, Y_i} & \left. \pder{W_{Y_i}}{T} \right|_{\rho, Y_i} & \left. \pder{W_{Y_i}}{Y_i} \right|_{\rho, T, Y_{j\ne i, ns}} \\
        \end{array}\right] \nonumber \\
    &=
    \left[\begin{array}{ccc}
        0 & 0 & 0 \\
        -\frac{\rho_{in} (h_{in}-h)}{\tau_{mix} \rho^2} & -\frac{\rho_{in} Cp }{\tau_{mix} \rho} & \frac{\rho_{in}(h_{ns}-h_i)}{\tau_{mix} \rho} \\
        -\frac{\rho_{in} (Y_{i,in}-Y_i)}{\tau_{mix} \rho^2} & 0 & -\frac{\rho_{in}}{\tau_{mix} \rho}
    \end{array}\right].
    \label{eq:dWdV_CP}
\end{align}
%
\begin{align}
    \pder{Q}{V} 
    &=
    \left[\begin{array}{ccc}
        0 &0 & 0\\
         \left. \pder{ Q_h}{\rho} \right|_{T, Y_i} & \left. \pder{Q_h}{T} \right|_{\rho, Y_i} & \left. \pder{Q_h}{Y_i} \right|_{\rho, T,Y_{j\ne i,ns}} \\
        0 &0 & 0\\
    \end{array}\right] \nonumber \\
    &=
    \left[\begin{array}{ccc}
        0 &0 & 0\\
        \frac{3 k}{\rho^2 r}(T-\Tinf) & -\frac{3k}{\rho r} & 0 \\
        0 &0 & 0\\
    \end{array}\right]
    \label{eq:dQdV_CP}
\end{align}
%
\begin{equation}
    \pder{K}{V}  = 
    \left[ \begin{array}{ccc}
        0 & 0 & 0 \\
        0 & 0& 0 \\
        \left. \left(\frac{1}{\rho}\pder{\omega_i}{\rho}-\frac{\omega_i}{\rho^2}\right) \right|_{T, Y_i} & \left. \frac{1}{\rho} \pder{\omega_i}{T} \right|_{\rho,Y_i} & \left. \frac{1}{\rho}\pder{\omega_i}{Y_i} \right|_{\rho, T, Y_{j\ne i,ns}} \\
    \end{array} \right].
    \label{eq:dKdV_CP}
\end{equation}

\section{Governing Equations for particles}
In Zodiac, different initial particle sizes could be given. And all the equations are derived for a stream, consisted with all particles having the same sizes, based on Eulerian volume. The two main types of equations Zodiac solves for particles include a conservation of mass and a conservation of enthalpy equation. All the particles are added into the reactor under constant pressure condition.
\begin{align}
\ddt{(N_i m_{p,i})} &= \left(\Nidot m_{p,i}\right)_{\tin} - \left(\Nidot m_{p,i}\right) \label{eqn: dNmpdt} \\
\ddt{(N_i m_{p,i}h_i)} &= \left(\Nidot m_{p,i}h_i\right)_{\tin} - \left(\Nidot m_{p,i}h_i\right) +N_i A_{p,i} \left[k_{gp}\left(T-T_{p,i}\right)+\varepsilon \sigma \left(T_{rad}^4-T_{p,i}^4\right)\right] \label{eqn: dNmphdt} 
\end{align}
where $N_i$ is the particle number in the reactor, $\Nidot$ is the number flow rate of particles, $m_{p,i}$ is mass of each particle, $h_i$ is enthalpy of every particle, and $A_{p,i}$ is the particle surface area. Subscript $i$ means $ith$ stream, that is, $ith$ size. $k_{gp}$ is the convective heat transfer coefficient between gas and particle. $\varepsilon$ is the emissivity, $\sigma$ is Stefan-Boltzmann constant, $T$ is the gas temperature and $T_{rad}$ is the radiation temperature.

Since we assume $N_{i}$ and $\Nidot$ are constant, we can arrange Equation \eqref{eqn: dNmpdt} and \eqref{eqn: dNmphdt} to get,
\begin{align}
\ddt{m_{p,i}} &=\NiNi \left( m_{p,i,in} - m_{p,i}\right) \label{eqn: dNmpdt_NN} \\
\ddt{(m_{p,i}h_i)} &= \NiNi \left(\left(m_{p,i}h_i\right)_{in} - \left(m_{p,i}h_i\right)\right) +A_{p,i} \left[k_{gp}\left(T-T_{p,i}\right)+\varepsilon \sigma \left(T_{rad}^4-T_{p,i}^4\right)\right] \label{eqn: dNmphdt_NN} 
\end{align}

Since the particle number flow rate over particle number is inverse mixing residence time for particle, $\Tmix=\NiNi$(Now particle residence time is set to be the same as gas residence time), we rearrange Equation \eqref{eqn: dNmpdt_NN} and \eqref{eqn: dNmphdt_NN} to get
\begin{align}
\ddt{m_{p,i}} &=\frac{ m_{p,i,in} - m_{p,i}}{\Tmix} \label{eqn: dNmpdt_final} \\
\ddt{(m_{p,i}h_i)} &= \frac {\left(m_{p,i}h_i\right)_{in} - \left(m_{p,i}h_i\right)}{\Tmix} +A_{p,i} \left[k_{gp}\left(T-T_{p,i}\right)+\varepsilon \sigma \left(T_{rad}^4-T_{p,i}^4\right)\right] \label{eqn: dNmphdt_NN} 
\end{align}

For particles, especially for coal particles, solving temperature equation is much easier than solving enthalpy equation. The temperature equation is derived from the enthalpy equation:
\begin{equation}
\ddt{T_{p,i}} = \frac{T_{p,i,\tin}-T_{p,i}}{\Tmix} + \frac{A_{p,i} \left[k_{gp}\left(T-T_{p,i}\right)+\varepsilon \sigma \left(T_{rad}^4-T_{p,i}^4\right)\right]} {m_{p,i} c_{p,p,i}} \label{eqn: partilce_temp}
\end{equation}
Equations \eqref{eqn: dNmpdt_final} and  \eqref{eqn: partilce_temp} are our final equations solved in Zodiac for particles. 

\subsection{Turbulent Effect on Particle}
For the convective heat transfer coefficient $k_{gp}$, 
\begin{equation}
k_{gp}=\frac{Nu\lambda}{D_{p}}  \label{eqn:k_gp_from_nu}
\end{equation}
where $\lambda$ is the conduction heat transfer coefficient. $D_{p}$ is the particle diameter, $Nu=2.0+0.6Re_{p}^{1/2}Pr_{g}^{1/3}$. To consider the turbulent flow in the reactor, we need to get the information about the flow field around particle. To get the velocity in particle length scale, turbulent energy spectrum is used here. A model for energy spectrum from Pope's book is used. 

The energy spectrum function should be:
\begin{align}
E(\kappa)&=C\epsilon^{2/3}\kappa^{-5/3}f_{L}(\kappa L)f_{\eta}(\kappa\eta) \label{eqn:spectrum} \\
f_{L}(\kappa L)&=(\frac{\kappa L}{[(\kappa L)^{2}+c_{L}]^{1/2}})^{5/3+p_{0}} \label{eqn:f_L} \\
f_{\eta}(\kappa\eta)&=\exp(\beta[(\kappa\eta)^{4}+c_{\eta}^{4}]^{1/4}-c_{\eta}) \label{eqn:f_eta} 
\end{align}
The constant numbers in the equations are: $C=1.5$, $p_{0}=2$, $\beta=5.2$, $c_{L}=6.78$, $c_{\eta}=0.40$.

Based on the definition of Kolmogorov length scale $\eta = (\frac{\nu^{3}}{\epsilon})^{1/4}$ , we could get the dissipation rate
\begin{equation}
\epsilon=\frac{\nu^{3}}{\eta^4} \label{eqn: epsilon}
\end{equation}

Based on equation \eqref{eqn:spectrum}, the kinetic energy at particle length scale is:
\begin{equation}
k(d_{p})=\int_{1/d_{p}}^{1/\eta}E(\kappa)d\kappa \label{eqn:kinetic_energy}
\end{equation}
Velocity at particle scale is:
\begin{equation}
u(d_{p})=\sqrt{2k} \label{eqn:particle_velocity}
\end{equation}
We regard this velocity as the particle slip velocity. And particle Reynolds number is:
\begin{equation}
Re_{p}=\frac{u(d_{p})d_{p}}{\nu} \label{eqn:particle_re}
\end{equation}
Prandtl number is:
\begin{align}
Pr&=\frac{\nu}{\alpha} \nonumber \\
&=\frac{\mu/\rho}{\lambda/(c_{p}\rho)} \nonumber \\
&=\frac{c_{p}\mu}{\lambda} \label{eqn:Pr}
\end{align}
where $\mu$ is gas dynamic viscosity.

By substituting Equation \eqref{eqn:particle_re} and \eqref{eqn:Pr} into the equation for Nu number and using Equation \eqref{eqn:k_gp_from_nu}, we could get the convective heat transfer coefficient $k_{gp}$. 

\subsection{Equations for Specific Particles}
In Zodiac, three different types of particles could be specified, including solid particles, liquid particles, and coal particle.

\subsubsection{Equation for Solid Particles}
For solid particles, the density and heat capacity are set to be constant, which could be given in the input file. 
Equations \eqref{eqn: dNmpdt_final} and  \eqref{eqn: partilce_temp} are solved for solid particles.

\paragraph{Source term for gas phase}
For the interaction between gas and particles,  a source term from the convective heat transfer between gas and particles needs to be added into RHS of gas enthalpy equation:

If the reactor is under constant volume condition:
\begin{equation}
S_h^{conv} = \frac{\sum_{i=0}^{K}N_{i}A_{p,i}k_{gp}\left(T-T_{p,i}\right)}{V}
\end{equation}

If the reactor is under constant pressure condition:
\begin{equation}
S_h^{conv} = \frac{\sum_{i=0}^{K}N_{i}A_{p,i}k_{gp}\left(T-T_{p,i}\right)}{m}
\end{equation}
Here, $N_i$ is the number of particles with ith size. $K$ is the number of particle stream, that is, the number of particle sizes.

\subsubsection{Equation for Liquid Particles}
The liquid particles here are referred to droplet. Both particle heat up and liquid evaporation process occur. The main equations solved here are still Equations \eqref{eqn: dNmpdt_final} and  \eqref{eqn: partilce_temp}. The main difference is that additional source terms needed to add into RHS of the equations.

\paragraph{For particle mass equation}
\begin{equation}
\ddt{m_{p,i}} = \frac{m_{p,i.\tin}-m_{p,i}}{\Tmix} + S_{p,i,H2O}^{vap} 
\end{equation}

\paragraph{For particle temperature equation}
\begin{equation}
\ddt{T_{p.i}} = \frac{T_{p,i,\tin}-T_{p,i}}{\Tmix} + \frac{A_{p,i} \left[k_{gp}\left(T-T_{p,i}\right)+\varepsilon \sigma \left(T_{rad}^4-T_{p,i}^4\right)\right]} {m_{p,i} c_{p,p,i}} + S_{p,i,T}^{vap}
\end{equation}

\paragraph{Source terms for particle equations}
To get the source terms $S_{p,i,H2O}^{vap}$ and $S_{p,i,T}^{vap}$ in particle equations, the vaporization rate of water need to be calculated:

\begin{equation}
\ddt{m_{H2O,i}} = k_{v,i} (\frac{p_{H2O,sat,i}}{RT_{p,i}}-\frac{p_{H2O}}{RT})A_{p,i}M_{w,H2O}
\end{equation}
Here, $k_{v,i}$ is the mass transfer coefficient of vapor to gas for the particle with ith size, and it could be calculated from Sherwood number Sh,
\begin{align}
k_{v,i} &= \frac{Sh_i D_{H2O,gas}}{d_{p,i}} \\
Sh_i &= 2.0 + 0.6 Re_{p,i}^{1/2}Sc^{1/3} \\
D_{H2O,gas} &= -2.775 \times 10^{-6} + 4.479 \times 10^{-8} T + 1.656 \times 10^{-10} T^2
\end{align}

$p_{H2O,sat,i}$ is the saturation pressure of water at particle temperature,
\begin{equation}
p_{H2O,sat,i} = 611.21 exp\left[ \left(18.678 - \frac{T_p-273.15}{234.5}\right) \frac{T_p - 273.15}{257.14+(T-273.15)} \right]
\end{equation}

$p_{H2O}$ is the partial pressure of H2O in the gas mixture,
\begin{equation}
p_{H2O} = pX_{H2O} = p \frac{Y_{H2O}M_w}{M_{w,H2O}}
\end{equation}
$A_{p,i}$ is the surface area of particle. $M_{w,H2O}$ is the molecular weight of H2O. $M_w$ is the molecular weight of gas mixture.

For the source term  $S_{p,i,H2O}^{vap}$  in particle mass equation,
\begin{equation}
 S_{p,i,H2O}^{vap}  = - \ddt{m_{H2O,i}} 
\end{equation}
The source term $S_{p,T}^{vap}$  in particle temperature equation should be the latent heat of vaporization of water, which is calculated from the Waston relation:
\begin{align}
\frac{\lambda_{Evap,i}}{\lambda_{ref}} = (\frac{1-T_{p,i,r}}{1-T_{ref,r}})^{0.38} \\
S_{p,i,T}^{vap} = \frac{S_{p,i,H2O}^{vap}  \lambda_{Evap,i}}{m_{p,i} c_{p,p,i}} 
\end{align}
Here, $T_{p,i,r} = T_{p,i} /Tc$ amd $T_{ref,r} = T_{ref}/Tc$  are the reduced temperature(based on water critical temperature) of particle and reference temperature with $Tc = 647.096 K$. $\lambda_{Evap,i}$ and $\lambda_{ref}$ are the water latent heat of vaporization at particle and a reference temperature. The reference temperature is chosen as $300K$. Latent heat at reference temperature is $2438 kJ/kg$.

\paragraph {Source terms for gas mass equation}
If the reactor is under constant volume condition, its mass equation would be 
\begin{align}
\ddt{\rho} &= \frac{\rho_\tin - \rho}{\Tmix} + S_\rho^{vap} \\
S_\rho^{vap} &= \sum_{i=0}^K \ddt{m_{H2O,i}}/V = - \sum_{i=0}^K \frac{S_{p,i,H2O}^{vap}}{V} \label{eqn:S_rho_vap}
\end{align}

\paragraph {Source terms for gas species equation}
Because water in the particle vaporized into gas phase, an additional term should be added into RHS of species H2O balance equation,

If the reactor is under constant volume condition,
\begin{align}
\ddt{\rho Y_{H2O}}&=\frac{(\rho Y_{H2O})_\tin - (\rho Y_{H2O})}{\Tmix}+\omega_{H2O} + S_{Y_{H2O}}^{vap} \\
 S_{Y_{H2O}}^{vap} &= \sum_{i=0}^K\frac{dm_{H2O,i}}{dt} / V = - \sum_{i=0}^K\frac{S_{p,i,H2O}^{vap}}{V}
\end{align}

If the reactor is under constant pressure condition,
\begin{align}
\ddt{Y_{H2O}}&=\frac{\rho_\tin}{\rho \Tmix}(Y_{H2O, in}-Y_i)+\frac{\omega_i}{\rho} + S_{Y_{H2O}}^{vap} \\
 S_{Y_{H2O}}^{vap} &= \sum_{i=0}^K\frac{dm_{H2O,i}}{dt} / m = - \sum_{i=0}^K \frac{S_{p,i,H2O}^{vap}}{m}
\end{align}

\paragraph{Source terms for gas enthalpy/energy equation} 
The source term from evaporation in gas enthalpy/energy equation should be the total vapor enthalpy at particle temperature. And the source from the convection heat transfer between gas and particle should also be added. 

If the reactor is under constant volume condition,
\begin{align}
\ddt{\rho e}&=\frac{(\rho e)_\tin - (\rho e)}{\Tmix}-\frac{3k(T-T_{inf})}{r}+ S_h^{conv} + S_h^{vap} \\
 S_h^{vap} &=  \sum_{i=0}^K\frac{\frac{dm_{H2O,i}}{dt} h_{H2O}(T_{p.i})}{ V}= - \sum_{i=0}^K \frac{S_{p,i,H2O}^{vap}h_{H2O}(T_{p.i})}{V}
\end{align}

If the reactor is under constant pressure condition,
\begin{align}
\ddt{h}&= \frac{\rho_\tin}{\rho \Tmix} (h_{in}-h) - \frac{3k}{\rho r} (T-T_{inf})+ S_h^{conv} + S_h^{vap} \\
S_h^{vap} &=  \sum_{i=0}^K\frac{\frac{dm_{H2O,i}}{dt} h_{H2O}(T_{p.i})}{m}= - \sum_{i=0}^K \frac{S_{p,i,H2O}^{vap}h_{H2O}(T_{p.i})}{m}
\end{align}

The convection heat transfer term $S_h^{conv}$ is the same as that for solid particles. The difference is that the density of liquid particle is set to be constant, instead of particle size, like that for solid particle.

\section{The Solution Method of Zodiac}
To solve \eqref{eqn: dudt array CV} and \eqref{eqn: dudt array CP}, Zodiac uses pseudo-transient continuation methods developed for combustion applications by Hansen and Sutherland~\cite{Hansen2016,Hansen:2016aa}.
These methods offer stability at any physical time step, $\Delta t$, enabling full choice of resolved physical phenomena. 
In Hansen and Sutherland~\cite{Hansen2016,Hansen:2016aa} this is shown on very stiff reactors in which fast, highly nonlinear ignition and extinction events are forced by relatively slow changes in the feed state.
On such problems these methods show better performance than naive techniques and well-developed, traditional methods such as those in the CVODE (SUNDIALS) code.
The time step, $\Delta t$, is unconstrained by problem dynamics, as stiffness and nonlinearity are managed by the \emph{dual time step} $\Delta\sigma$.
Full descriptions of the pseudo-transient continuation method is given in the references~\cite{Hansen2016,Hansen:2016aa}.
\\
\\
In Zodiac we solve an ensemble of independent reactors to steady state, $\ddt{U}\to0$ or in a transient (time-accurate) manner by evolving each reactor with its own adaptive dual time step, $\Delta\sigma$.
The BDF methods are used to discretize the physical time derivative, as described in \cite{Hansen2016,Hansen:2016aa}.
\\
\\
An issue with these pseudo-transient continuation methods is cost scaling with mechanism size.
As these methods rely on an eigenvalue decomposition of the Jacobian matrix, whose cost scales cubically with mechanism size.
While a dense linear solver (LU factorization) is also cubic, it is several orders of magnitude cheaper than the eigenvalue calculation, as shown in Figure \ref{fig: cost scaling}.
Note that on this figure the costs are only increasing with the number of species squared, but this is only due to the small size of the matrices considered.
Zodiac will likely be uncompetitive and impractical on mechanisms of enormous size, say, over 500 chemical species.

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.5\textwidth]{cpp-ajac-profile.pdf}
		\caption{Cost scaling of the Jacobian, LU factorization, and eigenvalue decomposition with number of species for an analytical Jacobian with Eigen linear algebra routines in C++.}
		\label{fig: cost scaling}
	\end{center}
\end{figure}

\bibliographystyle{siam}
\bibliography{ref} 

\hrulefill

\section*{Appendix}
Recall that 
$$
  W_h = \frac{\rho_\tin}{\rho \Tmix}(h_\tin-h)
$$
Let's consider $\pder{W_h}{U}$ for a moment.
This is given as
\begin{align}
    \left. \pder{W_h}{p} \right|_{h,Y_i} 
        &= \left. -\frac{W_h}{\rho}\pder{\rho}{p}\right|_{h,Y_i} \nonumber \\
        &= -\frac{W_h}{\rho} \frac{M}{RT} \nonumber \\
        &= -\frac{W_h}{p} \\
    \left. \pder{W_h}{h} \right|_{p,Y_i} 
        &= \left. - \frac{\rho_\tin}{\rho \Tmix} - \frac{W_h}{\rho} \pder{\rho}{h}\right|_{p,Y_i} \nonumber \\
        &= \left. - \frac{\rho_\tin}{\rho \Tmix} - \frac{W_h}{\rho} \pder{\rho}{T} \pder{T}{h}\right|_{p,Y_i} \nonumber \\
        &= - \frac{\rho_\tin}{\rho \Tmix} - \frac{W_h}{\rho}(-\frac{\rho}{T})(\frac{1}{Cp}) \nonumber \\
        & =  - \frac{\rho_\tin}{\rho \Tmix} + \frac{W_h}{Cp T} \label{eq:dWhdh_U} \\
    \left. \pder{W_h}{Y_i} \right|_{p,h,Y_j\ne i,n} 
        &= \left. -\frac{W_h}{\rho} \pder{\rho}{Y_i}\right|_{p,h,Y_{j \ne i,n}} \nonumber \\
        &= \left. -\frac{W_h}{\rho}(\pder{\rho}{M}\pder{M}{Y_i} + \pder{\rho}{T}\pder{T}{Y_i})\right|_{p,h,Y_j\ne i,n} \nonumber \\
        &= -\frac{W_h}{\rho} \left[ \frac{p}{RT}\left(-M^2 \left(\frac{1}{M_i}-\frac{1}{M_{ns}}\right)\right) - \frac{\rho}{T} \frac{h_{ns}-h_i}{Cp}\right] \nonumber\\
        &= -W_h \left[ - M  \left(\frac{1}{M_i}-\frac{1}{M_{ns}}\right) + \frac{1}{Cp T} \left( h_i - h_{ns}\right)\right] \label{eq:dWhdYi_U}
\end{align}

If we have a variable transformation $\pder{W_h}{U} = \pder{W_h}{V}\pder{V}{U}$ then we should obtain the same result as above.
Verifying this helps us ensure that our derivations of the various transformations are consistent.

Consider \eqref{eq:dWhdh_U}, which we can rewrite using $\pder{V}{U}$ as
\begin{align*}
    \left. \pder{W_h}{h} \right|_{p,Y_i}
        &= \left. \pder{W_h}{\rho} \right|_{T,Y_i} \left. \pder{\rho}{h} \right|_{p,Y_i} \\
        &+ \left. \pder{W_h}{T} \right|_{\rho,Y_i} \left. \pder{T}{h} \right|_{p,Y_i} \\
        &+ \sum_1^{ns-1} \left. \pder{W_h}{Y_i} \right|_{\rho,T,Y_{j\ne i}} \left. \pder{Y_i}{h} \right|_{p,Y_i} 
\end{align*}
Substituting information from \eqref{eq:dVdU_CP}, we find
\begin{align}
    \left. \pder{W_h}{h} \right|_{p,Y_i}
        &= - \frac{\rho}{c_p T} \left. \pder{W_h}{\rho} \right|_{T,Y_i} \nonumber \\
        &+ \frac{1}{c_p} \left. \pder{W_h}{T} \right|_{\rho,Y_i} \nonumber \\
        &+ \sum_1^{ns-1} 0 \left. \pder{W_h}{Y_i} \right|_{\rho,T,Y_{j\ne i}} \label{eq:dWhdh_V_1}
\end{align}
%
Looking now at $\pder{W_h}{V}$, 
\begin{align}
    \left. \pder{W_h}{\rho} \right|_{T,Y_i} 
        &= -\frac{W_h}{\rho} - \frac{\rho_\tin}{\rho\Tmix} \left. \pder{h}{\rho} \right|_{T,Y_i} \nonumber \\
        &= -\frac{W_h}{\rho} \label{eq:dWhdrho_V}\\
    \left. \pder{W_h}{T} \right|_{\rho,Y_i} &= -\frac{\rho_\tin}{\rho\Tmix} \left. \pder{h}{T} \right|_{\rho,Y_i} 
    \nonumber \\
        &= -c_p\frac{\rho_\tin}{\rho\Tmix} \label{eq:dWhdT_V}\\
    \left. \pder{W_h}{Y_i} \right|_{\rho,T,Y_{j\ne i}} 
        &= - \frac{\rho_\tin}{\rho\Tmix} \left. \pder{h}{Y_i} \right|_{\rho,T,Y_{j\ne i,n}} \nonumber \\
        &= - \frac{\rho_\tin}{\rho\Tmix} (h_i-h_{ns}) \label{eq:dWhdYi_V}
\end{align}
Substituting these back into \eqref{eq:dWhdh_V_1}, we find
\begin{equation}
    \left. \pder{W_h}{h} \right|_{p,Y_i} 
        = - \frac{\rho_\tin}{\rho \Tmix} + \frac{W_h}{c_p T},
    \label{eq:dWhdh_V_2}
\end{equation}
which is identical to \eqref{eq:dWhdh_U} as expected.  
{\bf 
This gives us confidence in our derivation of $\pder{W_h}{V}$
}
(except perhaps $\left. \pder{W_h}{Y_i}\right|_{\rho,T,Y_{j\ne i}}$ since it doesn't contribute as seen in \eqref{eq:dWhdh_V_1}).

Now, consider \eqref{eq:dWhdYi_U}, which we can rewrite using $\pder{V}{U}$ as
\begin{align*}
    \left. \pder{W_h}{Y_i} \right|_{p,h,Y_{j \ne i,ns}}
        &= \left. \pder{W_h}{\rho} \right|_{T,Y_i} \left. \pder{\rho}{Y_i} \right|_{p,h,Y_{j \ne i,ns}} \\
        &+ \left. \pder{W_h}{T} \right|_{\rho,Y_i} \left. \pder{T}{Y_i} \right|_{p,h,Y_{j \ne i,ns}} \\
        &+ \sum_1^{ns-1} \left. \pder{W_h}{Y_m} \right|_{\rho,T,Y_{n\ne m,ns}} \left. \pder{Y_m}{Y_i} \right|_{p,h,Y_{j \ne i,ns}} 
\end{align*}
Substituting information from \eqref{eq:dVdU_CP}, we find
\begin{align}
    \left. \pder{W_h}{Y_i} \right|_{p,Y_i}
        &= \left(\frac{\rho}{Cp T} \left( h_i - h_{ns} \right) - \rho M \left( \frac{1}{M_i}-\frac{1}{M_{ns}} \right)\right) \left. \pder{W_h}{\rho} \right|_{T,Y_i} \nonumber \\
        &+ \frac{h_{ns}-h_i}{c_p} \left. \pder{W_h}{T} \right|_{\rho,Y_i} \nonumber \\
        &+ 1 \left. \pder{W_h}{Y_i} \right|_{\rho,T,Y_{j\ne i,ns}} \label{eq:dWhdYi_V_1}
\end{align}
%
Substituting Equation \eqref{eq:dWhdrho_V}, \eqref{eq:dWhdT_V} and \eqref{eq:dWhdYi_V} back into \eqref{eq:dWhdYi_V_1}, we find
\begin{equation}
    \left. \pder{W_h}{Y_i} \right|_{p,h,Y_{j\ne i, ns} }
        = - W_h \left[\frac{1}{Cp T} \left(h_i - h_{ns}\right)- M \left(\frac{1}{M_i}-\frac{1}{M_{ns}}\right)\right]
    \label{eq:dWhdYi_V_2}
\end{equation}
which is identical to \eqref{eq:dWhdYi_U} as expected.  
{\bf 
This gives us confidence about $\left. \pder{W_h}{Y_i}\right|_{\rho,T,Y_{j\ne i}}$ in our derivation of $\pder{W_h}{V}$
}.

\end{document}
