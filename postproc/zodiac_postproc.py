import DataLoader as dl
import numpy as np
import plotly.offline as opy
import argparse
import yaml
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import os
import shutil
import glob
import re
import cantera as ct

parser = argparse.ArgumentParser()
parser.add_argument('-pv', '--plotvars', type=str, help='"list of independent variables separated by commas ; list of '
                                                        'dependent variables separated by commas" e.g. "T,p;Y_H2,T" '
                                                        'which would give two 2D contour plots of Y_H2 and T over the '
                                                        'initial temperature and pressure ranges. '
                                                        'Independent variables are plotted at the initial condition. '
                                                        '1D cases can plot up to 4 different types of variables (mass '
                                                        'fractions being considered one type of which 20 different ones'
                                                        ' can be specified for plotting)')
parser.add_argument('-et', '--end_time', type=float, help='the end time from the input file used to generate the data -'
                                                          ' not needed for steady state cases')
parser.add_argument('-ts', '--timestep', type=float, default=-1, help='the timestep taken in the input file used to '
                                                                      'generate the data - not needed for steady state '
                                                                      'cases')
parser.add_argument('-ti', '--time_interval', type=int, default=1, help='the interval for outputting data for transient'
                                                                        ' case plotting: default 1 - not needed for '
                                                                        'steady state cases')
args = parser.parse_args()

pFactor = 101325

N = 1  # 'N' = number of zodiac being run: when N=1, it means we just run zodiac once. That is, 'Restart' = 'False'
IV_plot = []

temp_alias = ['T', 'temperature']
pres_alias = ['p', 'pressure']
rho_alias = ['rho', 'density']
tempInflow_alias = ['T_inflow', 'temperature_inflow']
presInflow_alias = ['p_inflow', 'pressure_inflow']

PlotVars = args.plotvars

IV, DV = PlotVars.split(";")
IVs = IV.split(",")
list_IVs = []
for vars in IVs:
    list_IVs.append(vars.strip())

if len(list_IVs) > 1:
    twoD = 1
else:
    twoD = 0

DVs = DV.split(",")
list_DVs = []
for vars in DVs:
    list_DVs.append(vars.strip())

if args.timestep > -1:
    transient = True
    TimeStep = np.arange(0, args.end_time / args.timestep, args.time_interval).astype(int)
else:
    transient = False
    TimeStep = [0]

Fields = set()
Y_vars = []
other_vars = []

for var in list_DVs:
    Fields.add(var)
    if var[0] == 'Y':
        Y_vars.append(var)
    else:
        other_vars.append(var)
for var in list_IVs:
    Fields.add(var)

if len(Y_vars) != 0:
    Ys = 1  # boolean for mass fraction DV
else:
    Ys = 0


# find all fields for dict[timestep][iter][field]
def get_fields(timestep, fieldNames):
    dataDict = {}
    icBase = dl.DataLoader('initial_condition')
    for steps in timestep:
        baseDir = 'database_tstep_' + str(steps) + '/'
        database = dl.DataLoader(baseDir)
        print('loading:' + baseDir)
        times = set()
        for t in database.available_times():
            times.add(t)
        iterDict = {}
        for iteration in times:
            fieldDict = {}
            for names in fieldNames:
                try:
                    if twoD:
                        fieldDict[names] = dl.xyplane(database.get_field(iteration, names).data())
                    else:
                        fieldDict[names] = dl.flat1d(database.get_field(iteration, names).data())
                except RuntimeError:
                    if twoD:
                        a, b = dl.xyplane(icBase.get_field('ic', list_IVs[0]).data()).shape
                        fieldDict[names] = np.zeros(shape=(a, b))
                    else:
                        l = len(dl.flat1d(icBase.get_field('ic', list_IVs[0]).data()))
                        fieldDict[names] = np.zeros(l)
            iterDict[iteration] = fieldDict
        dataDict[str(steps)] = iterDict
    fieldDict = {}
    for names in fieldNames:
        try:
            if twoD:
                fieldDict[names] = dl.xyplane(icBase.get_field('ic', names).data())
            else:
                fieldDict[names] = dl.flat1d(icBase.get_field('ic', names).data())
        except RuntimeError:
            if twoD:
                a, b = dl.xyplane(icBase.get_field('ic', list_IVs[0]).data()).shape
                fieldDict[names] = np.zeros(shape=(a, b))
            else:
                l = len(dl.flat1d(icBase.get_field('ic', list_IVs[0]).data()))
                fieldDict[names] = np.zeros(l)
    dataDict['ic'] = fieldDict
    return dataDict

def plot():
    # put fields in order of iteration from ic to converged
    def for_iter_all_plot(timeStep, plotvars):
        vars = []
        for steps in timeStep:
            if(transient == False):
                keys = list(DATA[str(steps)].keys())
                newkeys = []
                for di in keys:
                    if di != 'converged' and di != 'ic':
                        a = str(di).replace('iter', '')
                        newkeys.append(int(a))
                sort = sorted(newkeys)
                for iters in sort:
                    val = []
                    for names in plotvars:
                        if names == 'p' or names == 'p_inflow':
                            val.append(DATA[str(steps)]['iter' + str(iters)][names] / pFactor)
                        else:
                            val.append(DATA[str(steps)]['iter' + str(iters)][names])
                    vars.append(val)
            val = []
            for names in plotvars:
                if names == 'p' or names == 'p_inflow':
                    val.append((DATA[str(steps)]['converged'][names] / pFactor))
                else:
                    val.append((DATA[str(steps)]['converged'][names]))
            vars.append(val)
        val = []
        for names in plotvars:
            if names == 'p' or names == 'p_inflow':
                val.append(DATA['ic'][names] / pFactor)
            else:
                val.append(DATA['ic'][names])
        vars.insert(0, val)
        return vars


    def barname(var):
        if var in pres_alias:
            return 'Pressure (atm)'
        elif var in temp_alias:
            return 'Temperature (K)'
        elif var in rho_alias:
            return 'Density (kg/m^3)'
        elif var in presInflow_alias:
            return 'Pressure_inflow (atm)'
        elif var in tempInflow_alias:
            return 'Temperature_inflow (K)'
        elif var == 'tau_mix':
            return 'tau_mix (s)'
        elif var == 'lambda+':
            return 'Explosive Eigenvalue (Hz)'
        elif var == 'mass_frac':
            return 'Mass Fractions'
        else:
            return var


    def figcol(var):
        if var == 'lambda+':
            contour_levels = {'start': 10 ** lammin, 'end': 10 ** lammax, 'showlines': False}
            contour_colormap = 'Rainbow'
        elif var == 'tau_mix':
            contour_levels = {'start': 10 ** taumin, 'end': 10 ** taumax, 'showlines': False}
            contour_colormap = 'Rainbow'
        else:
            if (var in temp_alias) or (var in tempInflow_alias):
                min = Tmin
                max = Tmax
                spacing = 300
            elif (var in pres_alias) or (var in presInflow_alias):
                min = pmin
                max = pmax
                spacing = 1
            elif var in rho_alias:
                min = rhomin
                max = rhomax
                spacing = 0.5
            else:
                min = 0
                max = 1
                spacing = 0.05
            contour_levels = {'start': min, 'end': max, 'size': spacing, 'showlines': False}
            contour_colormap = 'Rainbow'
        return contour_levels, contour_colormap

    # plot axes limits:
    Tmin = 300
    Tmax = 3901
    pmin = 0
    pmax = 12
    lammin = -2 #logscaled
    lammax = 7 #logscaled
    rhomin = 0
    rhomax = 3
    Ymin = -4 #logscaled
    Ymax = 0 #logscaled
    taumin = -15 #logscaled
    taumax = 3 #logscaled

    durationval = 0

    DATA = get_fields(TimeStep, Fields)
    if twoD:
        if list_IVs[0] == 'p' or list_IVs[0] == 'p_inflow':
            IVplot1 = DATA['ic'][list_IVs[0]] / pFactor
        else:
            IVplot1 = DATA['ic'][list_IVs[0]]
        if list_IVs[1] == 'p' or list_IVs[1] == 'p_inflow':
            IVplot2 = DATA['ic'][list_IVs[1]] / pFactor
        else:
            IVplot2 = DATA['ic'][list_IVs[1]]
    else:
        if list_IVs[0] == 'p' or list_IVs[0] == 'p_inflow':
            IVplot = DATA['ic'][list_IVs[0]] / pFactor
        else:
            IVplot = DATA['ic'][list_IVs[0]]
            if list_IVs[0] == 'tau_mix' or list_IVs[0] == 'T_inflow':
                if len(DATA['ic'][list_IVs[0]]) > 1:
                    # Get the temperature values for different tau_mix. This is used to get S-curve of combustion.
                    for i in range(len(DATA['ic'][list_IVs[0]])):
                        if N == 1:
                            IV_plot.append([])
                            IV_plot[i].append(IVplot[i])
                        IV_plot[i].append( DATA[str(TimeStep[-1])]['converged']['T'][i] )

    if twoD:
        DVplotalls = for_iter_all_plot(TimeStep, list_DVs)
    else:
        DVplotalls = for_iter_all_plot(TimeStep, other_vars)
        if Ys:
           Yplotalls = for_iter_all_plot(TimeStep, Y_vars)

    if transient:
        prefix = 'Time:'
        strlabel = 's'
        num = len(list(DATA.keys()))
        rng = np.arange(0,num)
        timebar = []
        for r in rng:
            timebar.append("{0:.2e}".format(min(args.timestep * r * args.time_interval, args.end_time)))
    else:
        prefix = 'Percent Converged:'
        strlabel = '%'
        outerkeys = len(list(DATA.keys())) - 1
        num = len(list(DATA['0'].keys())) + outerkeys
        rng = np.arange(0,num)
        timebar = []
        for r in rng:
            timebar.append("{0:.2f}".format((r + 1) / num * 100))

    sliders_dict = {
        'active': 0,
        'yanchor': 'top',
        'xanchor': 'left',
        'currentvalue': {
            'font': {'size': 20},
            'prefix': prefix,
            'visible': True,
            'xanchor': 'right'
        },
        'transition': {'duration': durationval, 'easing': 'cubic-in-out'},
        'pad': {'b': 10, 't': 50},
        'len': 0.9,
        'x': 0.1,
        'y': 0,
        'steps': []
    }

    MARKERvec = [27, 1, 13, 17, 22, 23, 25, 2, 3, 5, 18, 19, 24, 26, 4, 6, 16, 20, 7, 8]

    figure = {
        'data': [],
        'layout': {},
        'frames': []
    }

    if not twoD:
        if list_IVs[0] == 'tau_mix':
            figure['layout']['xaxis'] = {'title': barname(list_IVs[0]) + ' at  ic',
                                         'type': 'log'}
        else:
            figure['layout']['xaxis'] = {'range': [0.9 * np.min(IVplot), 1.1 * np.max(IVplot)],
                                         'title': barname(list_IVs[0]) + ' at  ic'}

        colors = ['#ff7f0e', '#0000ff', '#d62728', '#008000']  # for each axes (4 possible)
        yaxes = ['y', 'y2', 'y3', 'y4']
        yaxesnm = ['yaxis', 'yaxis2', 'yaxis3', 'yaxis4']

    figure['layout']['hovermode'] = 'closest'
    figure['layout']['sliders'] = {
        'args': [
            'sliders.value', {
                'duration': 0,
                'ease': 'cubic-in-out'
            }
        ],
        'initialValue': '0',
        'plotlycommand': 'animate',
        'values': timebar,
        'visible': True
    }

    if twoD:
        X = []
        Y = []
        # check if varying:
        if (IVplot1[0][0] == IVplot1[0][1]) and (IVplot1[0][0] == IVplot1[1][0]):
            raise ValueError(list_IVs[0] + ' does not vary.')
        if (IVplot2[0][0] == IVplot2[0][1]) and (IVplot2[0][0] == IVplot2[1][0]):
            raise ValueError(list_IVs[1] + ' does not vary.')

        # check if is independent variable (only varies in 1 direction):
        if (IVplot1[0][0] != IVplot1[0][1]) and (IVplot1[0][0] != IVplot1[1][0]):
            raise ValueError(list_IVs[0] + ' is not independent.')
        if (IVplot2[0][0] != IVplot2[0][1]) and (IVplot2[0][0] != IVplot2[1][0]):
            raise ValueError(list_IVs[1] + ' is not independent.')

        # set X and Y data:
        if IVplot1[0][0] < IVplot1[0][1]:
            X = IVplot1
            xname = list_IVs[0]
        elif IVplot1[0][0] < IVplot1[1][0]:
            Y = IVplot1
            yname = list_IVs[0]
        else:
            raise ValueError(list_IVs[0] + ' is ordered incorrectly: check min and max values in input file.')

        if IVplot2[0][0] < IVplot2[0][1]:
            X = IVplot2
            xname = list_IVs[1]
        elif IVplot2[0][0] < IVplot2[1][0]:
            Y = IVplot2
            yname = list_IVs[1]
        else:
            raise ValueError(list_IVs[1] + ' is ordered incorrectly: check min and max values in input file.')

        if len(X) == 0:
            raise NameError('Both ' + list_IVs[0] + ' and ' + list_IVs[1] + ' vary in the same direction.')
        else:
            shpr, shpc = X.shape
            numrow = shpr
            numcol = shpc

            x = X[0][0:numrow]

        if len(Y) == 0:
            raise NameError('Both ' + list_IVs[0] + ' and ' + list_IVs[1] + ' vary in the same direction.')
        else:
            y = []
            for row in Y:
                for i in range(0, numcol, numrow):
                    if row[i] not in y:
                        y.append(row[i])

        if xname == 'tau_mix':
            figure['layout']['xaxis'] = {'title': barname(xname) + ' at ic', 'type': 'log'}
        else:
            figure['layout']['xaxis'] = {'range': [np.min(x), np.max(x)], 'title': barname(xname) + ' at ic'}
        if yname == 'tau_mix':
            figure['layout']['yaxis'] = {'title': barname(yname) + ' at ic', 'type': 'log'}
        else:
            figure['layout']['yaxis'] = {'range': [np.min(y), np.max(y)], 'title': barname(yname) + ' at ic'}

        for j, plts in enumerate(DVplotalls[0]):
            if j == 0:
                viz = True
            else:
                viz = False
            contour_levels, contour_colormap = figcol(list_DVs[j])
            data_dict = {
                'type': 'contour',
                'x': x,
                'y': y,
                'z': plts,
                'colorscale': contour_colormap,
                'contours': contour_levels,
                'autocolorscale': False,
                'colorbar': {'title': barname(list_DVs[j]), 'titleside': 'right', 'titlefont': {'size': 18}},
                'visible': viz
            }
            figure['data'].append(data_dict)

        for i,tb in enumerate(timebar):
            frame = {'data': [], 'name': str(timebar[i])}
            for j, plts in enumerate(DVplotalls[i]):
                contour_levels, contour_colormap = figcol(list_DVs[j])
                data_dict = {
                    'type': 'contour',
                    'x': x,
                    'y': y,
                    'z': plts,
                    'colorscale': contour_colormap,
                    'contours': contour_levels
                }
                frame['data'].append(data_dict)

            figure['frames'].append(frame)

            slider_step = {'args': [
                [tb],
                {'frame': {'duration': durationval, 'redraw': True},
                'mode': 'immediate',
                'transition': {'duration': durationval}}
                ],
                'label': str(tb)+strlabel,
                'method': 'animate'
            }
            sliders_dict['steps'].append(slider_step)
    else:
        if Ys:
            for j, pls in enumerate(Yplotalls[0]):
                data_dict = {
                    'x': IVplot,
                    'y': pls,
                    'mode': 'lines+markers',
                    'marker': {'color': colors[0], 'symbol': MARKERvec[j]},
                    'name': Y_vars[j],
                    'visible': True,
                    'yaxis': yaxes[0]
                }
                figure['data'].append(data_dict)

            for j, pls in enumerate(DVplotalls[0]):
                if j > len(yaxes) - 2:
                    break
                else:
                    data_dict = {
                        'x': IVplot,
                        'y': pls,
                        'mode': 'lines+markers',
                        'marker': {'color': colors[j + 1], 'symbol': MARKERvec[0]},
                        'name': other_vars[j],
                        'visible': False,
                        'yaxis': yaxes[j + 1]
                    }
                    figure['data'].append(data_dict)
        else:
            for j, pls in enumerate(DVplotalls[0]):
                if j == 0:
                    viz = True
                else:
                    viz = False

                if j > len(yaxes) - 1:
                    break
                else:
                    data_dict = {
                        'x': IVplot,
                        'y': pls,
                        'mode': 'lines+markers',
                        'marker': {'color': colors[j], 'symbol': MARKERvec[0]},
                        'name': other_vars[j],
                        'visible': viz,
                        'yaxis': yaxes[j]
                    }
                    figure['data'].append(data_dict)

        for i, tb in enumerate(timebar):
            frame = {'data': [], 'name': str(timebar[i])}
            if Ys:
                for j, pls in enumerate(Yplotalls[i]):
                    data_dict = {
                        'x': IVplot,
                        'y': pls,
                        'mode': 'lines+markers',
                        'marker': {'color': colors[0], 'symbol': MARKERvec[j]},
                        'name': Y_vars[j],
                        'yaxis': yaxes[0]
                    }
                    frame['data'].append(data_dict)

                for j, pls in enumerate(DVplotalls[i]):
                    if j > len(yaxes) - 2:
                        break
                    else:
                        data_dict = {
                            'x': IVplot,
                            'y': pls,
                            'mode': 'lines+markers',
                            'marker': {'color': colors[j + 1], 'symbol': MARKERvec[0]},
                            'name': other_vars[j],
                            'yaxis': yaxes[j + 1]
                        }
                        frame['data'].append(data_dict)
            else:
                for j, pls in enumerate(DVplotalls[i]):
                    if j > len(yaxes) - 1:
                        break
                    else:
                        data_dict = {
                            'x': IVplot,
                            'y': pls,
                            'mode': 'lines+markers',
                            'marker': {'color': colors[j], 'symbol': MARKERvec[0]},
                            'name': other_vars[j],
                            'yaxis': yaxes[j]
                        }
                        frame['data'].append(data_dict)

            figure['frames'].append(frame)

            slider_step = {'args': [
                [tb],
                {'frame': {'duration': durationval, 'redraw': True},
                 'mode': 'immediate',
                 'transition': {'duration': durationval}}
            ],
                'label': str(tb) + strlabel,
                'method': 'animate'}
            sliders_dict['steps'].append(slider_step)

    figure['layout']['sliders'] = [sliders_dict]

    if twoD:
        buttonList = []
        for j,dv in enumerate(list_DVs):
            v = [False] * len(figure['data'])
            v[j] = True
            buttonList.append(
                dict(
                    args=['visible', v],
                    label=barname(dv),
                    method='restyle'
                )
            )
    else:
        buttonList = []
        v = [False] * len(figure['data'])
        if Ys:
            for j, dv in enumerate(Y_vars):
                v[j] = True
            buttonList.append(
                dict(
                    args=['visible', v],
                    label='Mass fractions',
                    method='restyle'
                )
            )
            for j, dv in enumerate(other_vars):
                if j > len(yaxes) - 2:
                    break
                else:
                    v = [False] * len(figure['data'])
                    v[j + len(Y_vars)] = True
                    buttonList.append(
                        dict(
                            args=['visible', v],
                            label=barname(dv),
                            method='restyle'
                        )
                    )
        else:
            buttonList = []
            for j, dv in enumerate(other_vars):
                if j > len(yaxes) - 1:
                    break
                else:
                    v = [False] * len(figure['data'])
                    v[j] = True
                    buttonList.append(
                        dict(
                            args=['visible', v],
                            label=barname(dv),
                            method='restyle'
                        )
                    )

    figure['layout']['updatemenus'] = [
        dict(
            x=-0.05,
            y=1,
            yanchor='top',
            buttons=buttonList
        ),
        {
            'buttons': [
                {
                    'args': [None, {'frame': {'duration': 200, 'redraw': True},
                             'fromcurrent': True, 'transition': {'duration': durationval, 'easing': 'quadratic-in-out'}}],
                    'label': 'Play',
                    'method': 'animate'
                },
                {
                    'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate',
                    'transition': {'duration': 0}}],
                    'label': 'Pause',
                    'method': 'animate'
                }
            ],
            'direction': 'left',
            'pad': {'r': 10, 't': 87},
            'showactive': False,
            'type': 'buttons',
            'x': 0.1,
            'xanchor': 'right',
            'y': 0,
            'yanchor': 'top'
        }
    ]
    figure['layout']['legend'] = {'orientation': 'h'}


    def Ylab(name):
        if (name in pres_alias) or (name in presInflow_alias):
            mn = pmin
            mx = pmax
            tp = 'linear'
        elif (name in temp_alias) or (name in tempInflow_alias):
            mn = Tmin
            mx = Tmax
            tp = 'linear'
        elif name in rho_alias:
            mn = rhomin
            mx = rhomax
            tp = 'linear'
        elif name == 'lambda+':
            mn = lammin
            mx = lammax
            tp = 'log'
        elif name == 'mass_frac':
            mn = Ymin
            mx = Ymax
            tp = 'log'
        elif name == 'tau_mix':
            mn = taumin
            mx = taumax
            tp = 'linear'
        nm = barname(name)
        return mn, mx, nm, tp


    def Yfig(id, mnv, mxv, nmv, tpv):
        if id == 0:
            return {'range': [mnv, mxv], 'title': nmv, 'type': tpv, 'titlefont': {'color': colors[id]},
                    'tickfont': {'color': colors[id]}}
        elif id == 1:
            return {'range': [mnv, mxv], 'title': nmv, 'type': tpv, 'titlefont': {'color': colors[id]},
                    'tickfont': {'color': colors[id]}, 'anchor': 'x', 'overlaying': 'y','side': 'right'}
        elif id == 2:
            return {'range': [mnv, mxv], 'title': nmv, 'type': tpv, 'titlefont': {'color': colors[id]},
                    'tickfont': {'color': colors[id]}, 'anchor': 'free', 'overlaying': 'y','side': 'left', 'position': 0.09}
        elif id == 3:
            return {'range': [mnv, mxv], 'title': nmv, 'type': tpv, 'titlefont': {'color': colors[id]},
                    'tickfont': {'color': colors[id]}, 'anchor': 'free','overlaying': 'y', 'side': 'right', 'position': 0.92}

    if not twoD:
        if Ys:
            mn, mx, nm, tp = Ylab('mass_frac')
            figure['layout'][yaxesnm[0]] = Yfig(0, mn, mx, nm, tp)

            for j, vs in enumerate(other_vars):
                if j > len(yaxes) - 2:
                    break
                else:
                    mn, mx, nm, tp = Ylab(vs)
                    figure['layout'][yaxesnm[j+1]] = Yfig(j+1, mn, mx, nm, tp)
        else:
            for j, vs in enumerate(other_vars):
                if j > len(yaxes) - 1:
                    break
                else:
                    mn, mx, nm, tp = Ylab(vs)
                    figure['layout'][yaxesnm[j]] = Yfig(j, mn, mx, nm, tp)

    opy.plot(figure, auto_open=True)


for file in glob.glob("*.yaml"):
    Input = yaml.load( open(file) )
tree = ET.parse(Input['Cantera']['InputFile'])
root = tree.getroot()
Species_list = root[1][1].text
Species = Species_list.split()

plot()

if ('Restart' in Input and Input['Restart'] ==True):
    Variables = set()
    GridVariables = set()
    Variables.add('T')
    Variables.add('p')
    for var in Species[0:len(Species)-1]:
        Variables.add('Y_'+ var)
    Result = get_fields(TimeStep, Variables)
    MassFractions = np.zeros([len(Species),])
    MoleFractions = np.zeros([len(Species),])
    # convert mass fraction to mole fraction
    for i in range(len(Species)-1):
        MassFractions[i] = Result[str(TimeStep[-1])]['converged']['Y_'+ Species[i]][-1]
    MassFractions[-1] = 1- np.sum(MassFractions[0:len(Species)-1])
    gas = ct.Solution(Input['Cantera']['InputFile'])
    MoleWeights = gas.molecular_weights

    mmw = 1 / np.sum(MassFractions / MoleWeights)
    MoleFractions = MassFractions * mmw / MoleWeights
    MoleFractions[-1] = 1 - np.sum(MoleFractions[0:len(Species)-1])

    i = 0
    if('InitialConditions' not in Input):
        Input['InitialConditions'] = []
        for var in Variables:
            if var[0:2] == 'Y_':
                Index = gas.species_index(var[2:])
                if re.search('[a-zA-Z]', str(MoleFractions[Index]) ):
                    Input['InitialConditions'].append(dict(value=str(MoleFractions[Index])[0:5]+str(MoleFractions[Index])[-4:], variable='X_'+ var[2:]))
                else:
                    Input['InitialConditions'].append(dict(value=str(MoleFractions[Index]), variable='X_'+ var[2:]))
            else:
                if re.search('[a-zA-Z]', str(Result[str(TimeStep[-1])]['converged'][var][-1]) ):
                    Input['InitialConditions'].append(dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1])[0:5] + str(Result[str(TimeStep[-1])]['converged'][var][-1])[-4:], variable=var))
                else:
                    Input['InitialConditions'].append(dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1]), variable=var))
    else:
        length = len(Input['InitialConditions'])
        for var in Variables:
            if var not in GridVariables:
                if i < length:
                    if isinstance(Input['InitialConditions'][i]['value'],dict):
                        GridVariables.add(Input['InitialConditions'][i]['variable'])
                    else:
                        if var[0:2] == 'Y_':
                            Index = gas.species_index(var[2:])
                            if re.search('[a-zA-Z]', str(MoleFractions[Index]) ):
                                Input['InitialConditions'][i] = dict(value=str(MoleFractions[Index])[0:5]+str(MoleFractions[Index])[-4:], variable='X_'+ var[2:])
                            else:
                                Input['InitialConditions'][i] = dict(value=str(MoleFractions[Index]), variable='X_'+ var[2:])
                        else:
                            if re.search('[a-zA-Z]', str(Result[str(TimeStep[-1])]['converged'][var][-1]) ):
                                Input['InitialConditions'][i] = dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1])[0:5] + str(Result[str(TimeStep[-1])]['converged'][var][-1])[-4:], variable=var)
                            else:
                                Input['InitialConditions'][i] = dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1]), variable=var)
                else:
                    if var[0:2] == 'Y_':
                        Index = gas.species_index(var[2:])
                        if re.search('[a-zA-Z]', str(MoleFractions[Index]) ):
                            Input['InitialConditions'].insert(i, dict(value=str(MoleFractions[Index])[0:5]+str(MoleFractions[Index])[-4:], variable='X_'+ var[2:]))
                        else:
                            Input['InitialConditions'].insert(i, dict(value=str(MoleFractions[Index]), variable='X_'+ var[2:]))
                    else:
                        if re.search('[a-zA-Z]', str(Result[str(TimeStep[-1])]['converged'][var][-1]) ):
                            Input['InitialConditions'].insert(i,dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1])[0:5] + str(Result[str(TimeStep[-1])]['converged'][var][-1])[-4:], variable=var))
                        else:
                            Input['InitialConditions'].insert(i,dict(value=str(Result[str(TimeStep[-1])]['converged'][var][-1]), variable=var))

            i = i + 1
    os.system('mkdir Restart')
    ReactionFileName = Input['Cantera']['InputFile']
    ReactionFilePath = (os.getcwd() + '/' + ReactionFileName)
    os.chdir('./Restart')
    CurrentPath = (os.getcwd() + '/' + ReactionFileName)

    shutil.copy(ReactionFilePath, CurrentPath)
    yaml.dump(Input, open('Restart_input.yaml', 'w'), default_flow_style=False)
    os.system('../zodiac Restart_input.yaml')
    RestartInput = yaml.load( open('Restart_input.yaml') )
    N = 2
    plot()

    # plot the S-curve (T vs. tau_mix / T_inflow)
    if list_IVs[0] == 'tau_mix' or list_IVs[0] == 'T_inflow':
        T_ignition = np.empty([len(IV_plot),1])
        T_extinction = np.empty([len(IV_plot),1])
        IV_list = np.empty([len(IV_plot),1])
        for i in range(0,len(IV_plot) ):
            IV_list[i] = IV_plot[i][0]
            T_ignition[i] = IV_plot[i][1]
            T_extinction[i] = IV_plot[i][2]

        plt.figure(1)
        if list_IVs[0] == 'tau_mix':
            plt.semilogx(IV_list, T_ignition, '.-', label='Ignition' )
            plt.semilogx(IV_list, T_extinction, '.-', label='Extinction' )
            plt.xlabel('Tau_mix (s)')
            plt.title('S-Curve: T vs. tau_mix')
        else:
            plt.plot(IV_list, T_ignition, '.-', label='Ignition' )
            plt.plot(IV_list, T_extinction, '.-', label='Extinction' )
            plt.xlabel('T_inflow (K)')
            plt.title('S-Curve: T vs. T_inflow')
        plt.legend()
        plt.ylabel('T (K)')
        plt.savefig('S-Curve.png')
