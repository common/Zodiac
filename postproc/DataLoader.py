# Note that the LoadDatabase.py file is available from ExprLib.  It should be put somewhere in the PYTHONPATH

import LoadDatabase as ld

def flat1d( field ):
  return field[:,0,0]

def xyplane( field ):
  return field[:,:,0].reshape((field.shape[1], field.shape[0])) #y,x

class DataLoader( ld.FieldDatabase ):
    '''Loads data from Zodiac runs and provide tools to query it'''

    def __init__(self, databasePath):
        ld.FieldDatabase.__init__( self, databasePath )
        # extract available field names (assumes that each database entry contains the same fields
        dbEntries = self.get_entry_names( )
        entry = self.get_database_entry( dbEntries[0] )
        self.__fieldNames = entry.get_field_names( )
        self.__metadata = entry.get_metadata_entries( )
        self.__requestedFields = []
        self.__requestedTimes = []

    def available_times(self):
        '''Obtain the names of the subdirectories in this database'''
        return self.get_entry_names( )

    def available_fields(self):
        '''Obtain the names of the fields available for query'''
        return self.__fieldNames

    def available_metadata(self):
        '''Obtain the names of metadata keys that are available to query'''
        return self.__metadata

    def select_times(self, times):
        '''Sets times that will be loaded when 'extract_requested_data' is called
        See also 'available_times' '''
        for t in times:
            assert( t in self.get_entry_names() )
        self.__requestedTimes = times

    def select_fields(self, fields):
        '''Sets the fields that will be loaded when 'extract_requested_data' is called.
        See also 'available_fields' '''
        for f in fields:
            assert( f in self.__fieldNames )
        self.__requestedFields = fields

    def extract_metadata_value(self, key):
        '''Extracts the value for the requested metadata.  See also 'available_metadata' '''
        dbEntries = self.get_entry_names( )
        return self.__db.get_database_entry( dbEntries[0] ).get_metadata( key )

    def extract_requested_data(self):
        '''Extract the data requested by previous calls to 'select_times' and 'select_fields'
        Returns a map:
          data[ fieldName ] -> [ dataTime1, dataTime2 ... dataTimeN ]
        Where dataTimeX is a numpy array of field data at the given time.
        '''
        data = {}
        for field in self.__requestedFields:
            fdata = []
            for t in self.__requestedTimes:
                fdata.append( self.get_field( t, field ).data().view() )
            data[field] = fdata
        return data
