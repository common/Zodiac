This demo is of a 1-dimensional transient solution for 10 reactors evolving through 2.5e-4 seconds whose inflow temperatures vary from 500 to 1400K.

To run zodiac from the command line, give the path to the zodiac executable followed by the name of the yaml input file. The executable can be found in the bin directory inside the build directory that was created.

For this demo, the input file is named 1Dtrans_input.yaml so from the terminal run (where ‘build’ is the name of the build directory created):
	‘../../build/bin/zodiac 1Dtrans_input.yaml’

This generates a database for each time step taken containing the converged solution.

The zodiac_postproc.py file in the postproc folder can be used to generate 1D plots for the data over the timesteps taken to the final time. To do this, python 3 must be used, and plotly must be installed. To use the postprocessing script, from the command line specify a python 3 path followed by the path to zodiac_postproc.py followed by the arguments for plotting (independent and dependent variables as well as end time and timestep taken). For 1D cases, up to four different types of dependent variables can be specified which will be plotted on a single plot with four different axes. Up to 20 different mass fractions can be specified, which will be plotted on a single axis. For transient cases, a time interval can also be specified for how often the state will be plotted during the transient solve. The default interval is 1.

For this demo, we will plot temperature, pressure, explosive eigenvalue (lambda+), and water, hydrogen, and oxygen mass fractions (Y_H2O, Y_H2, Y_O2) over inflow temperature. These are given after the -pv option in the format of “independent variables separated by commas ; dependent variables separated by commas”. For the transient case, an end time and timestep must also be given. From the input file, these are 2.5e-4 and 1e-6 respectively, which will come after the -et and -ts options. We will also specify a time interval of 10 after the -ti option to output every 10th timestep taken, but this doesn’t have to be included in which case every timestep will be plotted.

From the terminal run (where ‘python3’ is a path to a python 3 version):
	‘python3 ../../postproc/zodiac_postproc.py -pv "T_inflow;T,p,lambda+,Y_H2O,Y_H2,Y_O2" -et 2.5e-4 -ts 1e-6 -ti 10’

This brings up a plot of the mass fractions over the inflow T. In the upper left corner is a dropdown menu where the other variables can be selected to plot. The different colors correspond to the different y axes. The slider at the bottom of the plot can be used to access different times of the transient solve. Plotly provides options in the upper right corner for saving, zooming, etc.

If problems are experienced running the ‘zodiac_postproc.py’ script, make sure the path to ‘LoadDatabase.py’ is included in your ‘PYTHONPATH’ as explained in the Postprocessing section (3) of the “Zodiac-Manual” in the ‘doc’ folder.
