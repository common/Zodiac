#include "RestartFieldWriter.h"
#include "FieldTypes.h"

#include <sstream>
#include <stdexcept>
#include <boost/filesystem/operations.hpp>

//-------------------------------------------------------------------

RestartFileWriter::RestartFileWriter( Expr::FieldManagerList& fml,
                                      const std::string dbName,
                                      const std::string restartFile,
                                      std::map<Expr::Tag,int> consVarIdxMap,
                                      std::map<std::string, std::string> varMap)
      : fml_(fml),
        dbName_(dbName),
        rootPath_(dbName),
        restartFile_(restartFile),
        consVarIdxMap_(consVarIdxMap),
        varMap_(varMap)
{}

//-------------------------------------------------------------------

RestartFileWriter::~RestartFileWriter()
{}

//-------------------------------------------------------------------
void
RestartFileWriter::set_fields_from_file()
{
  Expr::FieldOutputDatabase db( fml_, dbName_, false );
  for( const auto& c : consVarIdxMap_ ){
    db.request_field_output<FieldT>( Expr::Tag(c.first.name(),Expr::STATE_N), varMap_.find(c.first.name())->second );
    db.extract_field_from_database( restartFile_, Expr::Tag( c.first.name(), Expr::STATE_N ), varMap_.find(c.first.name())->second  );
  }

  for( std::map<std::string,std::string>::const_iterator inm=varMap_.begin(); inm!=varMap_.end(); ++inm ){
    db.request_field_output<FieldT>( Expr::Tag(inm->first,Expr::STATE_NONE), inm->second );
    db.extract_field_from_database( restartFile_, Expr::Tag( inm->first, Expr::STATE_NONE ), inm->second  );
  }
}

//-------------------------------------------------------------------
void
RestartFileWriter::set_reacted_fields_from_file()
{
  std::string alias, tmp;
  char ctmp;
  int nx=0, ny=0, nz=0, nghost=0;

  boost::filesystem::ifstream  fin( rootPath_/restartFile_/"fields.dat"   );
  boost::filesystem::ifstream mdin( rootPath_/restartFile_/"metadata.txt" );

  if( !fin.good() ){
    std::ostringstream msg;
    msg << "Could not open file " << rootPath_/restartFile_/"fields.dat" << " for reading" << std::endl;
    throw std::runtime_error( msg.str() );
  }
  if( !mdin.good() ){
    std::ostringstream msg;
    msg << "Could not open file " << rootPath_/restartFile_/"metadata.txt" << " for reading" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  std::vector<std::string> aliasVec, tagNameVec;
  for( std::map<std::string,std::string>::const_iterator inm=varMap_.begin(); inm!=varMap_.end(); ++inm ){
    tagNameVec.push_back( inm->first);
    aliasVec.push_back( inm->second);
  }

  std::string line;
  while(std::getline(fin, line)){
    std::istringstream iss(line);
    std::vector<std::string> result{ std::istream_iterator<std::string>(iss), {} }; //split line by space
    // name      : [ nx,ny,nz ] : nghost
    mdin >> alias >> tmp >> tmp >> nx >> ctmp >> ny >> ctmp >> nz >> ctmp >> ctmp >> nghost;
    const typename SpatialOps::IntVec dim(nx,ny,nz);

    std::vector<std::string>::iterator it = std::find(aliasVec.begin(), aliasVec.end(), alias);
    if(it != aliasVec.end()){
      int index = std::distance(aliasVec.begin(), it);
      const Expr::Tag tag(tagNameVec[index], Expr::STATE_NONE);
      FieldT& field = fml_.field_ref<FieldT>(tag);
      if( field.window_with_ghost().extent() != dim ){
        std::ostringstream msg;
        msg << "Error reading " << tag << " from file.  Dimension mismatch." << std::endl
            << "  expected " << field.window_with_ghost().extent() << std::endl
            << "  found    " << dim << std::endl;
        throw std::runtime_error( msg.str());
      }

      typename FieldT::iterator ifld = field.begin();
      int i =0;
      std::map<Expr::Tag,int>::iterator itr = consVarIdxMap_.find(tag);
      if( itr != consVarIdxMap_.end()){
        const Expr::Tag tagN(tagNameVec[index], Expr::STATE_N);
        FieldT& fieldN = fml_.field_ref<FieldT>(tagN);
        typename FieldT::iterator ifldN = fieldN.begin();
        for( ;ifld != field.end();++ifld, ++ifldN){
          *ifld = std::stof(result[result.size()-1-nghost]);
          *ifldN = std::stof(result[result.size()-1-nghost]);
        }
      }
      else{
        for( ;ifld != field.end();++ifld, ++i ){
          *ifld = std::stof(result[i]);
        }
      }
    }
  }
}