#include "InitialInflowConditionsWriter.h"
#include "FieldTypes.h"
#include "ReactorEnsembleUtil.h"

#include <sstream>
#include <pokitt/CanteraObjects.h>


typedef read_from_file <FieldT>::Builder readT;

//-------------------------------------------------------------------

InitialInflowConditionsWriter::InitialInflowConditionsWriter( const YAML::Node parser,
                                                              Expr::FieldManagerList& fml,
                                                              std::set<Expr::ExpressionID>& initRoots,
                                                              Expr::ExpressionFactory& initFactory)
      : parser_(parser),
        fml_(fml),
        initRoots_(initRoots),
        initFactory_(initFactory)
{}

//-------------------------------------------------------------------

InitialInflowConditionsWriter::~InitialInflowConditionsWriter()
{}

//-------------------------------------------------------------------
void
InitialInflowConditionsWriter::set_init_inflow_fields_from_file()
{
  const std::string reactorType = parser_["ReactorParameters"]["ReactorType"].as<std::string>();
  const YAML::Node& fieldsOutput = parser_["Output"]["Fields"];
  const std::string dataFile = parser_["InitialInflowData"]["FileName"].as<std::string>();
  const int nspec = CanteraObjects::number_species();

  boost::filesystem::ifstream dataStream( dataFile );
  if( !dataStream.good() ){
    std::ostringstream msg;
    msg << "Could not open file " << dataFile << " for reading" << std::endl;
    throw std::runtime_error( msg.str() );
  }
  std::vector<std::string> DataVector;
  std::string line;
  while(std::getline(dataStream, line)){
    DataVector.push_back(line);
  }

  if(reactorType!="PFR"){
    const YAML::Node inflow = parser_["InflowConditions"];
    BOOST_FOREACH( const YAML::Node& inflow_Spec, inflow ){
      const std::string variable = inflow_Spec["variable"].as<std::string>();
      const Expr::Tag tag( variable, Expr::STATE_N );
      const YAML::Node& val = inflow_Spec["value"];

      if( val.size() <= 1 ){
        long double ld;
        if (val.as<std::string>()=="FromDataBase"){
          std::string alias = variable;
          BOOST_FOREACH( const YAML::Node& f, fieldsOutput ){
                  const std::string name = f["name"].as<std::string>();
                  if(name == variable){
                    alias = f["alias"].as<std::string>(name);
                  }
                }
          initRoots_.insert(initFactory_.register_expression( new readT(tag, alias, DataVector)));
        }
      }
    }
    const std::string InflowSpecies = parser_["InitialInflowData"]["InflowSpeciesSetting"].as<std::string>("FromInputFile");
    if(InflowSpecies == "FromDataBase"){
      BOOST_FOREACH( const std::string& sp_inflow, CanteraObjects::species_names() ){
              if( sp_inflow == CanteraObjects::species_name( nspec - 1 )) continue;
              const Expr::Tag tag( "X_"+sp_inflow+"_inflow", Expr::STATE_N );
              std::string alias = "X_"+sp_inflow+"_inflow";
              BOOST_FOREACH( const YAML::Node& f, fieldsOutput ){
                      const std::string name = f["name"].as<std::string>();
                      if(name == "X_"+sp_inflow){
                        alias = f["alias"].as<std::string>(name);
                      }
                    }
              initRoots_.insert(initFactory_.register_expression( new readT(tag, alias, DataVector)));
            }
    }
  }
  if(parser_["InitialConditions"]){
    const YAML::Node initial = parser_["InitialConditions"];
    BOOST_FOREACH( const YAML::Node& ic_Spec, initial ){
            const std::string variable = ic_Spec["variable"].as<std::string>();
            const Expr::Tag tag( variable, Expr::STATE_N );
            const YAML::Node& val = ic_Spec["value"];

            if( val.size() <= 1 ){
              long double ld;
              if (val.as<std::string>()=="FromDataBase"){
                std::string alias = variable;
                BOOST_FOREACH( const YAML::Node& f, fieldsOutput ){
                        const std::string name = f["name"].as<std::string>();
                        if(name == variable){
                          alias = f["alias"].as<std::string>(name);
                        }
                      }
                initRoots_.insert(initFactory_.register_expression( new readT(tag, alias, DataVector)));
              }
            }
          }
    const std::string InitialSpecies = parser_["InitialInflowData"]["InitialSpeciesSetting"].as<std::string>("FromInputFile");
    if(InitialSpecies == "FromDataBase"){
      BOOST_FOREACH( const std::string& sp_ic, CanteraObjects::species_names() ){
              if( sp_ic == CanteraObjects::species_name( nspec - 1 )) continue;
              const Expr::Tag tag( "X_"+sp_ic, Expr::STATE_N );
              std::string alias = "X_"+sp_ic;
              BOOST_FOREACH( const YAML::Node& f, fieldsOutput ){
                      const std::string name = f["name"].as<std::string>();
                      if(name == "X_"+sp_ic){
                        alias = f["alias"].as<std::string>(name);
                      }
                    }
              initRoots_.insert(initFactory_.register_expression( new readT(tag, alias, DataVector)));
            }
    }
  }
}