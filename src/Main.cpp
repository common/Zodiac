/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <ZDCVersion.h>

#include <string>
#include <chrono>

#include <pokitt/CanteraObjects.h>

#include <boost/make_shared.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <spatialops/util/MemoryUsage.h>
#include <spatialops/util/TimeLogger.h>

#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/ScaledIdentityMatrix.h>
#include <expression/matrix-assembly/PermutationSubMatrix.h>
#include <expression/matrix-assembly/EigenvalueExpression.h>

#include <pokitt/thermo/Pressure.h>
#include <pokitt/MassToMoleFracs.h>

#include <ReactorEnsembleUtil.h>
#include <EigenvalueModifier.h>

#include <parser/ParseInputFile.h>
#include <particles/ParticleInterface.h>

#include "TransformationMatrixExpressions.h"
#include "RestartFieldWriter.h"
#include "InitialInflowConditionsWriter.h"
#include <PlugFlowReactor/PlugFlowReactorInterface.h>

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >( #name );

using Expr::matrix::sensitivity;

using ClockT = std::chrono::high_resolution_clock;

#ifdef ENABLE_CUDA
# define LOCATION GPU_INDEX
#else
# define LOCATION CPU_INDEX
#endif

int main( int iarg, char* carg[] )
{
  SpatialOps::TimeLogger timer( "zodiac-high-level-timings" );

  timer.start( "parsing & setup" );
  std::string inputFileName;
  bool doTimings = false;

  try{ // load command line args

    po::options_description desc( "Supported Options" );
    desc.add_options()
          ( "help"   , "print help message" )
          ( "timings", "log detailed execution timings");

    po::options_description hidden("Hidden options");
    hidden.add_options()
          ( "input-file", po::value<std::string>(&inputFileName), "input file (yaml format)" );

    po::positional_options_description p;
    p.add("input-file", -1);

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    po::variables_map args;
    po::store( po::command_line_parser(iarg,carg).
          options(cmdline_options).positional(p).run(), args );
    po::notify(args);

    if( args.count( "help" ) ){
      std::cout << desc << std::endl;
      return 0;
    }

    doTimings = args.count("timings");

    if( inputFileName.empty() ){
      std::cout << "Must provide an input file!\n" << desc << std::endl;
      return -1;
    }
  }
  catch( std::exception& err ){
    std::cout << "Error parsing input arguments" << std::endl << err.what() << std::endl;
    return -1;
  }

  std::cout << "-------------------------------------------------------\n"
            << " Zodiac - A code to solve ensembles of batch reactors\n\n"
            << " For more information, contact James.Sutherland@utah.edu\n\n"
            << " This executable was built from source code version:\n"
            << "    Git Hash: " << ZDCVersionHash << std::endl
            << "        Date: " << ZDCVersionDate << std::endl
            << std::endl
            << " Built against:" << std::endl
            << "   SpatialOps Hash: " << SOPS_REPO_HASH << std::endl
            << "              Date: " << SOPS_REPO_DATE << std::endl
            << "      ExprLib Hash: " << EXPR_REPO_HASH << std::endl
            << "              Date: " << EXPR_REPO_DATE << std::endl
            << "       PoKiTT Hash: " << PoKiTTVersionHash << std::endl
            << "              Date: " << PoKiTTVersionDate << std::endl
            << "-------------------------------------------------------\n\n";

  // Build a parser from the input file:
  const YAML::Node parser = YAML::LoadFile( inputFileName );

  try{ // setup cantera
    const YAML::Node cantera = parser["Cantera"];
    const CanteraObjects::Setup setup( "Mix",
                                       cantera["InputFile"].as<std::string>(),
                                       cantera["GroupName"].as<std::string>("") );
    CanteraObjects::setup_cantera( setup );
  }
  catch( std::exception& err ){
    std::cout << "Caught error while parsing\n" << err.what() << std::endl;
    throw std::runtime_error("ERROR - terminating\n");
  }

  std::string     soDate(SOPS_REPO_DATE   ),     soHash(SOPS_REPO_HASH   );
  std::string   exprDate(EXPR_REPO_DATE   ),   exprHash(EXPR_REPO_HASH   );
  std::string pokittDate(PoKiTTVersionDate), pokittHash(PoKiTTVersionHash);
  std::string zodiacDate(ZDCVersionDate   ), zodiacHash(ZDCVersionHash   );

  Expr::ExpressionFactory initFactory;
  std::set<Expr::ExpressionID> initRoots;
  const bool isRestart = parser["Restart"];
  const std::string reactorType = parser["ReactorParameters"]["ReactorType"].as<std::string>();
  int EquIndex = 0;

  const GridPtr grid = parse_initial_inflow_conditions( parser, initFactory, initRoots, isRestart );

  const size_t nspec = CanteraObjects::number_species();
  const size_t nrxn = CanteraObjects::number_rxns();

  Expr::ExprPatch patch( grid->extent(0), grid->extent(1), grid->extent(2) );
  Expr::FieldManagerList& fml = patch.field_manager_list();

  const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
  const ReactorEnsembleUtil::ReactorEnsembleTags tagsN( Expr::STATE_N );
  const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );


  typedef pokitt::ChemicalSourceJacobian ChemJacT;
  const boost::shared_ptr<ChemJacT  > csj = boost::make_shared<ChemJacT>();

  typedef FieldT  FT;
  typedef SingleValueField SVFT;

  Expr::TagList primitiveTags, kinRhsTags;
  if(reactorType=="PFR"){
    primitiveTags.push_back( tagsEpfr.uTag );
  }
  primitiveTags.push_back( tagsE.rhoTag );
  primitiveTags.push_back( tagsE.tempTag );
  for( size_t i=0; i<nspec-1; ++i ){
    primitiveTags.push_back( tagsE.massTags[i] );
  }

  const unsigned bdfOrder = 1;
  typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FT,FT> IntegratorType;
  IntegratorType* integrator = new IntegratorType( patch.id(), "BIBDF", tagsE.dtTag, tagsE.dsTag, bdfOrder );

  const IntegratorParameters integratorParams = parse_integrator_params( parser, integrator );
  const double dt = integratorParams.timeStep;

  parse_reactor_parameters( parser, integrator->factory() );

  if(parser["InitialInflowData"]){
    InitialInflowConditionsWriter* InitialInflowDatabase = new InitialInflowConditionsWriter( parser, fml, initRoots, initFactory);
    InitialInflowDatabase->set_init_inflow_fields_from_file();
  }

  if( reactorType == "PSRConstantVolume" ){
    kinRhsTags.push_back( tagsE.rhoKinRhsTag );
    kinRhsTags.push_back( tagsE.rhoEgyKinRhsTag );
    for( size_t i = 0;i < nspec - 1;++i ){
      kinRhsTags.push_back( tagsE.rhoYKinRhsTags[i] );
    }

    if(!isRestart){
      ReactorEnsembleUtil::setup_cv( initRoots, initFactory, parser);
    }
    ReactorEnsembleUtil::build_equation_system_cv( integrator, csj, parser, EquIndex );
  }

  if( reactorType == "PSRConstantPressure" ){
    kinRhsTags.push_back( tagsE.presKinRhsTag );
    kinRhsTags.push_back( tagsE.enthKinRhsTag );
    for( size_t i = 0;i < nspec - 1;++i ){
      kinRhsTags.push_back( tagsE.yKinRhsTags[i] );
    }

    if(!isRestart){
      ReactorEnsembleUtil::setup_cp( initRoots, initFactory, parser);
    }
    ReactorEnsembleUtil::build_equation_system_cp( integrator, csj, parser, EquIndex);
  }

  if( reactorType == "PFR" ){
    kinRhsTags.push_back( tagsEpfr.uFullRhsTag );
    kinRhsTags.push_back( tagsE.rhoFullRhsTag );
    kinRhsTags.push_back( tagsE.enthFullRhsTag );
    for( size_t i = 0;i < nspec - 1;++i ){
      kinRhsTags.push_back( tagsE.yFullRhsTags[i] );
    }

    if(!isRestart){
      PlugFlowReactorInterface::setup_pfr( initRoots, initFactory, parser);
    }
    PlugFlowReactorInterface::build_equation_system_pfr( integrator, csj, parser, EquIndex);
  }

  std::vector<std::string> particleNumArray;
  Particles::ParticleSetup* particleSetup;
  if(parser["Particles"]){
    particleSetup = new Particles::ParticleSetup(particleNumArray, parser, initRoots, initFactory, integrator, primitiveTags, kinRhsTags, EquIndex, isRestart);
  }
  integrator->done_adding_variables();

  Expr::ExpressionTree initTree( initRoots, initFactory, patch.id(), "initialization"  );

  // variable index maps
  const auto consVarIdxMap = integrator->variable_tag_index_map();
  const auto rhsIdxMap     = integrator->rhs_tag_index_map();
  const auto consVarRhsMap = integrator->variable_rhs_tag_map();
  std::map<int,int> varRhsIdxMap;
  for( const auto& c : consVarIdxMap ){
    varRhsIdxMap[c.second] = rhsIdxMap.at( consVarRhsMap.at(c.first) );
  }

  std::map<Expr::Tag,Expr::Tag> primConsMap; // primitive variables
  std::map<Expr::Tag,int> primVarIdxMap;
  std::map<Expr::Tag,int> kinRhsIdxMap;      // kinetic rhs indices

  if( reactorType == "PSRConstantVolume" ){
    primConsMap[tagsE.rhoTag] = tagsE.rhoTag;
    primConsMap[tagsE.tempTag] = tagsE.rhoEgyTag;
    for( size_t i=0; i<nspec-1; ++i ){
      primConsMap[tagsE.massTags[i]] = tagsE.rhoYTags[i];
    }

    for( const auto& pc : primConsMap ){
      primVarIdxMap[pc.first] = consVarIdxMap.at( pc.second );
    }

    kinRhsIdxMap[tagsE.rhoKinRhsTag   ] = rhsIdxMap.at( tagsE.rhoFullRhsTag    );
    kinRhsIdxMap[tagsE.rhoEgyKinRhsTag] = rhsIdxMap.at( tagsE.rhoEgyFullRhsTag );
    for( auto i=0; i<nspec-1; ++i ){
      kinRhsIdxMap[tagsE.rhoYKinRhsTags[i]] = rhsIdxMap.at( tagsE.rhoYFullRhsTags[i] );
    }

    // state transformation matrix
    auto make_state_transform = [&]()
    {
        ASSEMBLER( Expr::matrix::SparseMatrix, FT, dVdU )
        // make some new tags in local scope to make things clearer
        const Expr::Tag&     T     = tagsE.tempTag;
        const Expr::Tag&     rho   = tagsE.rhoTag;
        const Expr::Tag&     rhoe  = tagsE.rhoEgyTag;
        const Expr::TagList& Y     = tagsE.massTags;
        const Expr::TagList& rhoY  = tagsE.rhoYTags;

        const auto& tempIdx = primVarIdxMap.at( T );
        const auto& rhoConsIdx = consVarIdxMap.at( rho );
        const auto& rhoPrimIdx = primVarIdxMap.at( rho );
        dVdU->element<double>( rhoPrimIdx, rhoConsIdx ) = 1.0;
        const auto& egyConsIdx = consVarIdxMap.at( rhoe );
        dVdU->element<FT>( tempIdx, rhoConsIdx ) = tagsE.egyOffsetTags[nspec-1];
        dVdU->element<FT>( tempIdx, egyConsIdx ) = tagsE.invrhocvTag;
        for( auto i=0; i<nspec-1; ++i ){
          dVdU->element<FT>( primVarIdxMap.at( Y[i] ),
                             consVarIdxMap.at( rhoY[i] ) ) = tagsE.invrhoTag;
          dVdU->element<FT>( primVarIdxMap.at( Y[i] ),
                             consVarIdxMap.at( rho     ) ) = tagsE.mYoverRhoTags[i];
          dVdU->element<FT>( primVarIdxMap.at( T    ),
                             consVarIdxMap.at( rhoY[i] ) ) = tagsE.egyOffsetTags[i];
        }
        dVdU->finalize();
        return dVdU;
    };

    auto make_kinetics_jacobian = [&]()
    {
        ASSEMBLER( Expr::matrix::DenseSubMatrix, FT, dKdV )
        for( const auto& K : kinRhsTags ){
          for( const auto& V : primitiveTags ){
            dKdV->element<FT>( kinRhsIdxMap.at( K ), primVarIdxMap.at( V ) ) = sensitivity( K, V );
          }
        }
        dKdV->finalize();
        return dKdV;
    };

    auto make_heat_transfer_jacobian = [&](){
      ASSEMBLER( Expr::matrix::SparseMatrix, FT, dqdV )
      const Expr::Tag& qConv = tagsE.rhoEgyHeatRhsTag;
      const Expr::Tag& temp  = tagsE.tempTag;
      const Expr::Tag& rhoe  = tagsE.rhoEgyTag;
      const auto& rhoEgyRhsIdx = rhsIdxMap.at( consVarRhsMap.at( rhoe ) );
      auto tempIdx = primVarIdxMap.at( temp );
      dqdV->element<FT>( rhoEgyRhsIdx, tempIdx ) = sensitivity( qConv, temp );
      dqdV->finalize();
      return dqdV;
    };

    auto make_flow_jacobian = [&](){
      ASSEMBLER( Expr::matrix::SparseMatrix, FT, dmixdu )
      const Expr::Tag& rhoegymix = tagsE.rhoEgyMixRhsTag;
      const Expr::Tag& rhomix = tagsE.rhoMixRhsTag;
      const Expr::Tag& rhoegy = tagsE.rhoEgyTag;
      const Expr::Tag& rho = tagsE.rhoTag;
      const auto& rhoegyRhsIdx = rhsIdxMap.at( consVarRhsMap.at( rhoegy ));
      const auto& rhoRhsIdx = rhsIdxMap.at( consVarRhsMap.at( rho ));
      auto rhoIdx = consVarIdxMap.at( rho );
      auto rhoegyIdx = consVarIdxMap.at( rhoegy );

      dmixdu->element<FT>( rhoegyRhsIdx, rhoegyIdx ) = tagsE.invTauTag;
      dmixdu->element<FT>( rhoRhsIdx, rhoIdx ) = tagsE.invTauTag;
      for( auto i = 0;i < nspec - 1;++i ){
        const Expr::Tag& rhoYi = tagsE.rhoYTags[i];
        const Expr::Tag& rhoYimix = tagsE.rhoYMixRhsTags[i];
        const auto& rhoYiRhsIdx = rhsIdxMap.at( consVarRhsMap.at( rhoYi ));
        auto rhoYiIdx = consVarIdxMap.at( rhoYi );
        dmixdu->element<FT>( rhoYiRhsIdx, rhoYiIdx ) =tagsE.invTauTag;
      }
      dmixdu->finalize();
      return dmixdu;
    };

    boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > jacobian;
    if(parser["Particles"]){
      particleSetup->modify_idxmap(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap);

      integrator->set_jacobian( ( (make_heat_transfer_jacobian() + particleSetup->dqdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                                  + make_kinetics_jacobian() + particleSetup->dmMixingdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                                * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap))
                                - make_flow_jacobian());

      jacobian = ( (make_heat_transfer_jacobian() + particleSetup->dqdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                   + make_kinetics_jacobian()+ particleSetup->dmMixingdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                 * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap))
                 - make_flow_jacobian();
    }
    else{
      integrator->set_jacobian( (make_heat_transfer_jacobian() + make_kinetics_jacobian()) * make_state_transform() - make_flow_jacobian() );
      jacobian = (make_heat_transfer_jacobian() + make_kinetics_jacobian()) * make_state_transform() - make_flow_jacobian();
    }

    typedef Expr::EigenvalueExpression<FT>::Builder ExplosiveEigT;
    integrator->factory().register_expression( new ExplosiveEigT( tagsE.expEigTag,
                                                                  jacobian,
                                                                  EquIndex+1,
                                                                  ExplosiveEigT::MOST_POS_REAL_PART ) );
    typedef ReactorEnsembleUtil::EigenvalueModifier<FieldT>::Builder GesatEigT;
    integrator->factory().register_expression( new GesatEigT( tagsE.gesatEigTag,
                                                              tagsE.expEigTag,
                                                              dt ) );

    integrator->register_root_expression( new ExplosiveEigT( tagsE.reactEigTag,
                                                             make_kinetics_jacobian() * make_state_transform(),
                                                             EquIndex+1,
                                                             ExplosiveEigT::MOST_NEG_REAL_PART ));

    integrator->register_root_expression( new pokitt::Pressure<FT>::Builder( tagsE.presTag, tagsE.tempTag, tagsE.rhoTag, tagsE.mmwTag ) );
    integrator->register_root_expression( new pokitt::MassToMoleFracs<FT>::Builder( tagsE.moleTags, tagsE.massTags, tagsE.mmwTag ));

    const double dsInit = parser["TimeIntegrator"]["dsInitial"].as<double>(integratorParams.gesatMax);
    ReactorEnsembleUtil::initialize_cv( integrator, fml, patch, initTree, tagsE.dsTag, dsInit, dt, isRestart );
  }

  if( reactorType == "PSRConstantPressure" ){

    primConsMap[tagsE.rhoTag] = tagsE.presTag;
    primConsMap[tagsE.tempTag] = tagsE.enthTag;
    for( size_t i = 0;i < nspec - 1;++i ){
      primConsMap[tagsE.massTags[i]] = tagsE.massTags[i];
    }
    for( const auto& pc : primConsMap ){
      primVarIdxMap[pc.first] = consVarIdxMap.at( pc.second );
    }

    kinRhsIdxMap[tagsE.presKinRhsTag] = rhsIdxMap.at( tagsE.presFullRhsTag );
    kinRhsIdxMap[tagsE.enthKinRhsTag] = rhsIdxMap.at( tagsE.enthFullRhsTag );
    for( auto i = 0;i < nspec - 1;++i ){
      kinRhsIdxMap[tagsE.yKinRhsTags[i]] = rhsIdxMap.at( tagsE.yFullRhsTags[i] );
    }

    // state transformation matrix
    auto make_state_transform = [&]()
    {
        ASSEMBLER( Expr::matrix::SparseMatrix, FT, dVdU )
        // make some new tags in local scope to make things clearer
        const Expr::Tag& T = tagsE.tempTag;
        const Expr::Tag& rho = tagsE.rhoTag;
        const Expr::Tag& p = tagsE.presTag;
        const Expr::Tag& h = tagsE.enthTag;
        const Expr::TagList& Y = tagsE.massTags;

        const auto& tempPrimIdx = primVarIdxMap.at( T );
        const auto& rhoPrimIdx = primVarIdxMap.at( rho );
        const auto& presConsIdx = consVarIdxMap.at( p );
        const auto& enthConsIdx = consVarIdxMap.at( h );
        dVdU->element<FT>( rhoPrimIdx, presConsIdx ) = tagsE.rhoOverPresTag;
        dVdU->element<FT>( rhoPrimIdx, enthConsIdx ) = tagsE.rhoPartialHTag;
        dVdU->element<FT>( tempPrimIdx, enthConsIdx ) = tagsE.invCpTag;
        for( auto i = 0;i < nspec - 1;++i ){
          dVdU->element<double>( primVarIdxMap.at( Y[i] ),
                                 consVarIdxMap.at( Y[i] )) = 1.0;
          dVdU->element<FT>( primVarIdxMap.at( rho ),
                             consVarIdxMap.at( Y[i] )) = tagsE.rhoPartialYTags[i];
          dVdU->element<FT>( primVarIdxMap.at( T ),
                             consVarIdxMap.at( Y[i] )) = tagsE.enthOffsetTags[i];
        }
        dVdU->finalize();
        return dVdU;
    };

    auto make_heat_transfer_jacobian = [&]()
    {
        ASSEMBLER( Expr::matrix::SparseMatrix, FT, dqdV )
        const Expr::Tag& enthHeat = tagsE.enthHeatRhsTag;
        const Expr::Tag& T = tagsE.tempTag;
        const Expr::Tag& h = tagsE.enthTag;
        const Expr::Tag& rho = tagsE.rhoTag;
        const auto& enthRhsIdx = rhsIdxMap.at( consVarRhsMap.at( h ));
        auto tempIdx = primVarIdxMap.at( T );
        auto rhoIdx = primVarIdxMap.at( rho );
        dqdV->element<FT>( enthRhsIdx, tempIdx ) = sensitivity(enthHeat, T);
        dqdV->element<FT>( enthRhsIdx, rhoIdx ) = sensitivity( enthHeat, rho );
        dqdV->finalize();
        return dqdV;
    };

    auto make_flow_jacobian = [&](){
      ASSEMBLER( Expr::matrix::SparseMatrix, FT, dmixdv )
      const Expr::Tag& enthmix = tagsE.enthMixRhsTag;
      const Expr::Tag& h = tagsE.enthTag;
      const Expr::Tag& T = tagsE.tempTag;
      const Expr::Tag& rho = tagsE.rhoTag;
      const auto& enthRhsIdx = rhsIdxMap.at( consVarRhsMap.at( h ));
      auto tempIdx = primVarIdxMap.at( T );
      auto rhoIdx = primVarIdxMap.at( rho );

      dmixdv->element<FT>( enthRhsIdx, tempIdx ) = tagsE.hMixPartialTTag;
      dmixdv->element<FT>( enthRhsIdx, rhoIdx ) = sensitivity( enthmix, rho );
      for( auto i = 0;i < nspec - 1;++i ){
        const Expr::Tag& Yi = tagsE.massTags[i];
        const Expr::Tag& Yimix = tagsE.yMixRhsTags[i];
        const auto& YiRhsIdx = rhsIdxMap.at( consVarRhsMap.at( Yi ));
        auto YiIdx = primVarIdxMap.at( Yi );
        dmixdv->element<FT>( enthRhsIdx, YiIdx ) =tagsE.hMixPartialYTags[i];
        dmixdv->element<FT>( YiRhsIdx, rhoIdx ) = sensitivity( Yimix, rho );
        dmixdv->element<FT>( YiRhsIdx, YiIdx ) = tagsE.yMixPartialYTag;
      }
      dmixdv->finalize();
      return dmixdv;
    };

    auto make_kinetics_jacobian = [&]()
    {
        ASSEMBLER( Expr::matrix::DenseSubMatrix, FT, dKdV )
        for( const auto& K : kinRhsTags ){
          for( const auto& V : primitiveTags ){
            dKdV->element<FT>( kinRhsIdxMap.at( K ), primVarIdxMap.at( V )) = sensitivity( K, V );
          }
        }
        dKdV->finalize();
        return dKdV;
    };

    boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > jacobian;
    if(parser["Particles"]){
      particleSetup->modify_idxmap(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap);

      integrator->set_jacobian( ( (make_heat_transfer_jacobian() + particleSetup->dqdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                                  + make_flow_jacobian() + make_kinetics_jacobian()
                                  + particleSetup->dmMixingdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                                * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap)) );

      jacobian = ( (make_heat_transfer_jacobian() + particleSetup->dqdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                   + make_flow_jacobian() + make_kinetics_jacobian()
                   + particleSetup->dmMixingdv_part(primVarIdxMap, rhsIdxMap, consVarRhsMap))
                 * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap));
    }
    else{
      jacobian = (make_heat_transfer_jacobian()  + make_kinetics_jacobian() + make_flow_jacobian()) * make_state_transform();
      integrator->set_jacobian(  (make_heat_transfer_jacobian()  + make_kinetics_jacobian() + make_flow_jacobian()) * make_state_transform() );
    }

    typedef Expr::EigenvalueExpression<FT>::Builder ExplosiveEigT;
    integrator->factory().register_expression( new ExplosiveEigT( tagsE.expEigTag,
                                                                  jacobian,
                                                                  EquIndex+1,
                                                                  ExplosiveEigT::MOST_POS_REAL_PART ));
    typedef ReactorEnsembleUtil::EigenvalueModifier<FieldT>::Builder GesatEigT;
    integrator->factory().register_expression( new GesatEigT( tagsE.gesatEigTag,
                                                                tagsE.expEigTag,
                                                                dt ));

    integrator->register_root_expression( new ExplosiveEigT( tagsE.reactEigTag,
                                                             make_kinetics_jacobian() * make_state_transform(),
                                                             EquIndex+1,
                                                             ExplosiveEigT::MOST_NEG_REAL_PART ));

    integrator->register_root_expression( new pokitt::MassToMoleFracs<FT>::Builder( tagsE.moleTags, tagsE.massTags, tagsE.mmwTag ));
    const double dsInit = parser["TimeIntegrator"]["dsInitial"].as<double>(integratorParams.gesatMax);
    ReactorEnsembleUtil::initialize_cp( integrator, fml, patch, initTree, tagsE.dsTag, dsInit, dt, isRestart );
  }

  if( reactorType == "PFR" ){
    primConsMap[tagsEpfr.uTag] = tagsEpfr.uTag;
    primConsMap[tagsE.rhoTag] = tagsE.rhoTag;
    primConsMap[tagsE.tempTag] = tagsE.enthTag;
    for( size_t i = 0;i < nspec - 1;++i ){
      primConsMap[tagsE.massTags[i]] = tagsE.massTags[i];
    }
    for( const auto& pc : primConsMap ){
      primVarIdxMap[pc.first] = consVarIdxMap.at( pc.second );
    }

    kinRhsIdxMap[tagsEpfr.uFullRhsTag] = rhsIdxMap.at( tagsEpfr.uFullRhsTag );
    kinRhsIdxMap[tagsE.rhoFullRhsTag]  = rhsIdxMap.at( tagsE.rhoFullRhsTag );
    kinRhsIdxMap[tagsE.enthFullRhsTag]  = rhsIdxMap.at( tagsE.enthFullRhsTag );
    for( auto i = 0;i < nspec - 1;++i ){
      kinRhsIdxMap[tagsE.yFullRhsTags[i]] = rhsIdxMap.at( tagsE.yFullRhsTags[i] );
    }

    // state transformation matrix
    auto make_state_transform = [&]()
    {
        ASSEMBLER( Expr::matrix::SparseMatrix, FT, dVdU )
        // make some new tags in local scope to make things clearer
        const Expr::Tag& u = tagsEpfr.uTag;
        const Expr::Tag& T = tagsE.tempTag;
        const Expr::Tag& rho = tagsE.rhoTag;
        const Expr::Tag& h = tagsE.enthTag;
        const Expr::TagList& Y = tagsE.massTags;

        const auto& uPrimIdx = primVarIdxMap.at( u );
        const auto& tempPrimIdx = primVarIdxMap.at( T );
        const auto& rhoPrimIdx = primVarIdxMap.at( rho );
        const auto& uConsIdx = consVarIdxMap.at( u );
        const auto& rhoConsIdx = consVarIdxMap.at( rho );
        const auto& enthConsIdx = consVarIdxMap.at( h );
        dVdU->element<double>( uPrimIdx, uConsIdx ) = 1.0;
        dVdU->element<double>( rhoPrimIdx, rhoConsIdx ) = 1.0;
        dVdU->element<FT>( tempPrimIdx, enthConsIdx ) = tagsE.invCpTag;
        for( auto i = 0;i < nspec - 1;++i ){
          dVdU->element<double>( primVarIdxMap.at( Y[i] ),
                                 consVarIdxMap.at( Y[i] )) = 1.0;
          dVdU->element<FT>( primVarIdxMap.at( T ),
                             consVarIdxMap.at( Y[i] )) = tagsE.enthOffsetTags[i];
        }
        dVdU->finalize();
        return dVdU;
    };

    auto make_kinetics_jacobian = [&]()
    {
        ASSEMBLER( Expr::matrix::DenseSubMatrix, FT, dKdV )
        for( const auto& K : kinRhsTags ){
          for( const auto& V : primitiveTags ){
            dKdV->element<FT>( kinRhsIdxMap.at( K ), primVarIdxMap.at( V )) = sensitivity( K, V );
          }
        }
        dKdV->finalize();
        return dKdV;
    };

    boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > jacobian;
    if(parser["Particles"]){
      particleSetup->modify_idxmap(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap);

      integrator->set_jacobian( make_kinetics_jacobian() * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap)) );

      jacobian = make_kinetics_jacobian() * (make_state_transform() + particleSetup->dvdu_part(primVarIdxMap, consVarIdxMap));
    }
    else{
      jacobian = make_kinetics_jacobian() * make_state_transform();
      integrator->set_jacobian( make_kinetics_jacobian() * make_state_transform() );
    }

    typedef Expr::EigenvalueExpression<FT>::Builder ExplosiveEigT;
    integrator->factory().register_expression( new ExplosiveEigT( tagsE.expEigTag,
                                                                  jacobian,
                                                                  EquIndex+1,
                                                                  ExplosiveEigT::MOST_POS_REAL_PART ));
    typedef ReactorEnsembleUtil::EigenvalueModifier<FieldT>::Builder GesatEigT;
    integrator->factory().register_expression( new GesatEigT( tagsE.gesatEigTag,
                                                              tagsE.expEigTag,
                                                              dt ));

    integrator->register_root_expression( new ExplosiveEigT( tagsE.reactEigTag,
                                                             make_kinetics_jacobian() * make_state_transform(),
                                                             EquIndex+1,
                                                             ExplosiveEigT::MOST_NEG_REAL_PART ));

    integrator->register_root_expression( new pokitt::MassToMoleFracs<FT>::Builder( tagsE.moleTags, tagsE.massTags, tagsE.mmwTag ));
    const double dsInit = parser["TimeIntegrator"]["dsInitial"].as<double>(integratorParams.gesatMax);
    PlugFlowReactorInterface::initialize_pfr( integrator, fml, patch, initTree, tagsE.dsTag, dsInit, dt, isRestart );
  }

  if(parser["Particles"]){
    particleSetup->initialize_particle();
  }

  integrator->lock_all_execution_fields();

  integrator->set_tolerance( integratorParams.tolerance );

  if( doTimings ) integrator->request_timings( true );

  const unsigned maxIterPerStep = 10000;

  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;

  // save all variables needed to restart into this vector
  std::vector<std::string> restartVars;
  for( const auto& c : consVarIdxMap ){
    restartVars.push_back(c.first.name());
    if(reactorType != "PFR"){
      restartVars.push_back( c.first.name() + "_inflow" );
    }
  }
  restartVars.push_back(tagsE.tempTag.name());
  restartVars.push_back(tagsE.convecTempTag.name());
  restartVars.push_back(tagsE.convecCoeffTag.name());
  if(reactorType == "PSRConstantPressure"){
    restartVars.push_back(tagsE.rhoInflowTag.name());
  }
  if(reactorType != "PFR"){
    restartVars.push_back(tagsE.tauMixTag.name());
    restartVars.push_back(tagsE.tempInflowTag.name());
  }
  if(parser["Particles"]){
    particleSetup->restart_var_particle(restartVars);
  }

  /* Initialize a map of restart variable names (first entry) and their respective aliases (second entry).
     * The default alias will be the variable name.
     */
  std::map<std::string, std::string> varMap;

  // grab information about which fields we want to output, and the destination prefix
  FieldOutputInfo fieldOutput = parse_field_output( parser, particleNumArray, restartVars, varMap);

  std::string icDest = "initial_condition";
  std::string stepPrefix = "database";

  if( fieldOutput.customPrefix ){
    icDest = fieldOutput.outputPrefix + "_initial_condition";
    stepPrefix = fieldOutput.outputPrefix;
  }

  double t = 0;

  typedef Expr::FieldMgrSelector<FieldT>::type FM;
  FM& fmgr = fml.field_manager<FieldT>();
  if(!isRestart)
  {
    Expr::FieldOutputDatabase db( fml, icDest, true );
    BOOST_FOREACH( const FieldOutputInfo::FieldOutputRequest::value_type& f, fieldOutput.fields ){
      if( fmgr.has_field(Expr::Tag(f.first,Expr::STATE_N)) ){
        db.request_field_output<FieldT>( Expr::Tag(f.first,Expr::STATE_N), f.second.alias);
      }
      else{
        std::cout << "Field '" << f.first << "' requested for output was not registered for initial conditions.  "
                                              "Its initial values won't be included in output.\n";
      }
    }
    db.write_database("ic");
  }

  // lock fields requested for output
  BOOST_FOREACH( FieldOutputInfo::FieldOutputRequest::value_type& f, fieldOutput.fields ){
    Expr::Context& context = f.second.context;
    const Expr::Tag tag( f.first, context );
    if( !fmgr.has_field(tag) ){
      context = Expr::STATE_N;
      if( !fmgr.has_field(tag) ){
        std::cout << "Field '" << f.first << "' requested for output was not found.  Won't be included in output.\n";
        context = Expr::INVALID_CONTEXT;
      }
    }
    if( context != Expr::INVALID_CONTEXT ) fmgr.lock_field( tag );
  }

  timer.stop( "parsing & setup" );
  timer.start( "time integration" );

  FieldT& simTimeField = fml.field_ref<FieldT>( tagsE.timeTag );
  SingleValueField& timeStepField = fml.field_ref<SingleValueField>( tagsE.dtTag );

  int restartStep;
  std::string restartFile = "converged";
  if( isRestart ){
    restartStep = parser["Restart"]["Step"].as<int>();
    //transient simulation restarts from certain physical time step
    if(parser["TimeIntegrator"]["end_time"]){
      stepCount = restartStep;
      std::cout << "RESTARTING from time step: " << stepCount << std::endl;
      t = stepCount * parser["TimeIntegrator"]["timestep"].as<double>();
    }
      //steady state simulation restarts from certain dual time step
    else if(restartStep != 0){
      dualIter = restartStep;
      totalIter = restartStep;
      restartFile = "iter" + std::to_string(dualIter);
      std::cout << "RESTARTING from dual time step: " << dualIter << std::endl;
    }
    //steady state simulation restarts using `converged` databese as initial conditions, used to get S-curve
    else{
      std::cout << "RESTARTING steady state simulation using reacted conditions as initial conditions." << std::endl;
    }
    RestartFileWriter* restartDatabase = new RestartFileWriter(fml,
                                                               stepPrefix + "_tstep_" + boost::lexical_cast<std::string>(stepCount),
                                                               restartFile,
                                                               consVarIdxMap,
                                                               varMap);
    //---------------------------------------------------------------
    // Request fields that we want saved as the simulation progresses
    if(!parser["TimeIntegrator"]["end_time"] and restartStep == 0){ // restart to get S-curve
      restartDatabase->set_reacted_fields_from_file();
    }
    else{
      restartDatabase->set_fields_from_file();
    }
    // rename the database based on ` restart_prefix` in the input file
    if(parser["Restart"]["Restart_prefix"].as<std::string>(stepPrefix) == stepPrefix){
      std::cout << "OVERWRITE previous database during restart!" << std::endl;
    }
    else{
      stepPrefix = parser["Restart"]["Restart_prefix"].as<std::string>(stepPrefix);
    }

    if(parser["TimeIntegrator"]["end_time"]){
      stepCount += 1;
    }
  }

  while( t < integratorParams.tend ){

    // increment the simulation time so that the RHS is evaluated at t^{n+1}
    simTimeField <<= simTimeField + timeStepField;

    integrator->begin_time_step();

    //---------------------------------------------------------------
    // Request fields that we want saved as the simulation progresses
    Expr::FieldOutputDatabase db( fml, stepPrefix + "_tstep_" + boost::lexical_cast<std::string>(stepCount), true );
    BOOST_FOREACH( const FieldOutputInfo::FieldOutputRequest::value_type& f, fieldOutput.fields ){
      if( f.second.context == Expr::INVALID_CONTEXT ) continue;
      db.request_field_output<FieldT>( Expr::Tag(f.first,f.second.context), f.second.alias );
    }
    db.request_string_output( zodiacHash, "Zodiac-Version"    );   db.request_string_output( zodiacDate, "Zodiac-Date"    );
    db.request_string_output(     soHash, "SpatialOps-Version");   db.request_string_output(     soDate, "SpatialOps-Date");
    db.request_string_output(   exprHash, "ExprLib-Version"   );   db.request_string_output(   exprDate, "ExprLib-Date"   );
    db.request_string_output( pokittHash, "PoKiTT-Version"    );   db.request_string_output( pokittDate, "PoKiTT-Date"    );
    //---------------------------------------------------------------
    if( !(isRestart and restartFile == "iter" + std::to_string(restartStep))){
      dualIter = 0;
    }

    if( !(stepCount % fieldOutput.timeOutputInterval ) ){
      std::cout << "Timestep " << stepCount << "  time: " << t << std::endl;
    }
    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;

      if( !(stepCount % fieldOutput.timeOutputInterval ) ){
        if( !( dualIter % fieldOutput.dualtimeOutputInterval )){
          db.write_database( "iter" + boost::lexical_cast<std::string>( dualIter ));
          std::cout << "  iter " << std::setw( 5 ) << dualIter << "/" << std::setw( 5 ) << maxIterPerStep
                    << ",   max T: " << std::fixed << std::setprecision( 10 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.tempTag ))
                    << ",   max pres: " << std::fixed << std::setprecision( 10 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.presTag ))
                    << ",   max rho: " << std::fixed << std::setprecision( 10 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.rhoTag ))
                    << ",   max mmw: " << std::fixed << std::setprecision( 10 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.mmwTag ))
                    << ",   max lambda-gesat: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.gesatEigTag ))
                    << ",   max lambda+: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.expEigTag ))
                    << ",   max lambda-: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.reactEigTag ))
                    << ",   min ds: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_min( fml.field_ref<FieldT>( tagsE.dsTag ))
                    << ",   max ds: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsE.dsTag ))
                    << std::endl;
          if(reactorType == "PFR"){
            const Particles::ParticleEnsembleTags tagsEpart( Expr::STATE_NONE, particleNumArray );
            const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE);
            std::cout << ",  u: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpfr.uTag ))
                      << ",  dpdx: " << std::scientific << std::setprecision( 1 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpfr.dpdxTag ))
                      << std::endl;
          }
          if( parser["Particles"] ){
            const Particles::ParticleEnsembleTags tagsEpart( Expr::STATE_NONE, particleNumArray );
            std::cout << ",   max Re: " << std::scientific << std::setprecision( 5 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.reynoldsNumberTags[0] ))
                      << ",   max cppart: " << std::scientific << std::setprecision( 5 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.partCpTags[0] ))
                      << ",   max Tpart: " << std::scientific << std::setprecision( 5 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.partTempTags[0] ))
                      << ",   max massEachPart: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.massEachPartTags[0] ))
                      << ",   max massFracPart: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.partMassFracTags[0] ))
                      << ",   max numFracPart: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEpart.partNumFracTags[0] ))
                      << std::endl;
            if( parser["Particles"]["ParticleType"].as<std::string>() == "Coal" ){
              const Coal::CoalEnsembleTags tagsEcoal( Expr::STATE_NONE, particleNumArray );

              std::cout << ",   max moisture: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEcoal.moistureMassEachPartTags[0] ))
                        << ",   max vol: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEcoal.volMassEachPartTags[0] ))
                        << ",   max char: " << std::scientific << std::setprecision( 7 ) << SpatialOps::nebo_max( fml.field_ref<FieldT>( tagsEcoal.charMassEachPartTags[0] ))
                        << std::endl;
            }
          }
        }
      }

    } while( !converged && dualIter <= maxIterPerStep );

    if( !(stepCount % fieldOutput.timeOutputInterval) ){
      db.write_database( "converged" );
    }

    integrator->end_time_step();
    stepCount++;
    t += dt;
  }
  timer.stop( "time integration" );

  std::cout << "\nMemory usage: " << SpatialOps::get_memory_usage()/1e6 << " MB\n"
            << "CPU time in setup and parsing: " << timer.timer( "parsing & setup" ).elapsed_time() << " s\n"
            << "CPU time in time integration : " << timer.timer( "time integration" ).elapsed_time() << " s\n";

  // clean up
  delete integrator;
  if(parser["Particles"]){
    delete particleSetup;
  }
}

