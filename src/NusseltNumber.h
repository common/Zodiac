/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   NusseltNumber.h
 *  @par    Getting the Nusselt number of gas phase
 *  @date   Oct 4, 2018
 *  @authors Hang
 *
 */

#ifndef NUSSELTNUMBER_H_
#define NUSSELTNUMBER_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil {

    /**
     * @class NusseltNumber
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the Nusselt number of gas phase
     * @author Hang
     *
     * Nusselt number is given as
     * \f[
     *    Nu = 2.0 + 0.6 Re_p ^{1/2} Pr_g^{1/3}
     * \f]
     *
     * \f[
     * Pr_g = \frac{cp \mu }{k}
     * \f]
     * Here, \f$ cp \f$ is the constant pressure heat capacity of gas phase (\f$ J/(kg \cdot K) \f$),
     *       \f$ \mu \f$ is the dynamic viscosity of gas phase ( \f$ kg/(m \cdot s) \f$),
     *       \f$ k \f$ is the thermal conductivity of gas phase ( \f$ W/(m \cdot K) \f$).
     *
     * The sensitivity is given as
     * \f[
     *    \frac{\partial Nu}{\partial \phi} = 0.3 Re_p ^{-1/2} Pr_g^{1/3} \frac{\partial Re_p}{\partial \phi} + 0.2 Re_p ^{1/2} Pr_g^{-2 /3} \frac{\partial Pr_g}{\partial \phi}
     *                                      = Re^{1/2} \left(\frac{cp \mu }{k}\right)^{1/3} \left(\frac{0.3}{Re_p} \frac{\partial Re_p}{\partial \phi}
     *                                      + 0.2 \left(\frac{\frac{\partial cp}{\partial \phi}}{cp} + \frac{\frac{\partial \mu}{\partial \phi}}{\mu} - \frac{\frac{\partial k}{\partial \phi}}{k} \right)\right)
     * \f]
     */
    template<typename FieldT>
    class NusseltNumber : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, cp_, dynviscosity_, thermalconduc_, renumber_ );
      NusseltNumber( const Expr::Tag cpTag,
                     const Expr::Tag dynviscosityTag,
                     const Expr::Tag thermalconducTag,
                     const Expr::Tag renumberTag)
            : Expr::Expression<FieldT>(){
          this->set_gpu_runnable( true );
          cp_            = this->template create_field_request<FieldT>( cpTag            );
          dynviscosity_  = this->template create_field_request<FieldT>( dynviscosityTag  );
          thermalconduc_ = this->template create_field_request<FieldT>( thermalconducTag );
          renumber_      = this->template create_field_request<FieldT>( renumberTag      );
      }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag cpTag_, dynviscosityTag_, thermalconducTag_, renumberTag_;
        public:
          /**
           * @brief The mechanism for building a NusseltNumber object
           * @tparam FieldT
           * @param nunumberTag Nusselt number
           * @param cpTag Constant pressure heat capacity of gas phase
           * @param dynviscosityTag Dynamic viscosity of gas phase
           * @param thermalconducTag Thermal conductivity of gas phase
           * @param renumberTag Particle Reynolds number
           */
            Builder( const Expr::Tag nunumberTag,
                     const Expr::Tag cpTag,
                     const Expr::Tag dynviscosityTag,
                     const Expr::Tag thermalconducTag,
                     const Expr::Tag renumberTag)
                  : Expr::ExpressionBuilder( nunumberTag      ),
                    cpTag_                 ( cpTag            ),
                    dynviscosityTag_       ( dynviscosityTag  ),
                    thermalconducTag_      ( thermalconducTag ),
                    renumberTag_           ( renumberTag      )
            {}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
                return new NusseltNumber<FieldT>( cpTag_, dynviscosityTag_, thermalconducTag_, renumberTag_ );
            }
        };


        void evaluate(){
          using namespace SpatialOps;
          this->value() <<= 2.0 + 0.6 * sqrt(renumber_->field_ref()) * pow( cp_->field_ref() * dynviscosity_->field_ref() / thermalconduc_->field_ref(), 1.0/3.0) ;
        }


        void sensitivity( const Expr::Tag& sensVarTag ){
            FieldT & dfdv = this->sensitivity_result( sensVarTag );
            const FieldT& cp = cp_->field_ref();
            const FieldT& dynviscosity = dynviscosity_->field_ref();
            const FieldT& thermalconduc = thermalconduc_->field_ref();
            const FieldT& re = renumber_->field_ref();
            const FieldT& dcpdv = cp_->sens_field_ref( sensVarTag );
            const FieldT& ddynviscositydv = dynviscosity_->sens_field_ref( sensVarTag );
            const FieldT& dthermalconducdv = thermalconduc_->sens_field_ref( sensVarTag );
            const FieldT& dRedv = renumber_->sens_field_ref( sensVarTag );

            dfdv <<= cond(re == 0, 0.0)
                         ( sqrt(re) * pow(cp * dynviscosity / thermalconduc, 1.0/3.0)
                          * ( 0.3/re * dRedv + 0.2 * ( dcpdv / cp + ddynviscositydv / dynviscosity - dthermalconducdv / thermalconduc)));
        }
    };

} // namespace ReactorEnsembleUtil

#endif /* NUSSELTNUMBER_H_ */
