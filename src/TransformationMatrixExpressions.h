/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file    TransformationMatrixExpressions.h
 *  \par     Getting the expressions needed to build transformation matrix
 *  \authors Mike, Hang
 */

#ifndef TRANSFORMATIONMATRIXEXPRESSIONS_H_
#define TRANSFORMATIONMATRIXEXPRESSIONS_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil{

    /**
     * @class InverseExpression
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the inverse of an expression
     * @author Mike
     *
     * The inverse of an expression is given as
     * \f[
     *     invExpr = \frac{1}{Expr}
     * \f]
     */

  template< typename FieldT >
  class InverseExpression : public Expr::Expression<FieldT>
  {
    DECLARE_FIELD( FieldT, toInvert_ );

    InverseExpression( const Expr::Tag tagToInvert )
    : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      toInvert_ = this->template create_field_request<FieldT>( tagToInvert );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tagToInvert_;
    public:
      /**
       * @brief The mechanism for building a InverseExpression object
       * @tparam FieldT
       * @param inverseTag The inverse of the expression
       * @param tagToInvert The expression need to be inverted
       */
      Builder( const Expr::Tag inverseTag,
               const Expr::Tag tagToInvert ) :
        Expr::ExpressionBuilder( inverseTag ),
        tagToInvert_( tagToInvert )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new InverseExpression<FieldT>( tagToInvert_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= 1.0 / toInvert_->field_ref();
    }
  };

  /**
   * @class ProductExpression
   * @ingroup ReactorEnsembleUtil
   * @brief Getting the production of two expressions
   * @author Mike
   *
   * The production is given as
   * \f[
   *    Production = Expression1 * Expression2
   * \f]
   *
   * The sensitivity of production is give as
   * \f[
   *    \frac{\partial Production}{\partial \phi} = \frac{\partial Expression1}{\partial \phi} * Expression2
   *                                                + Expression1 * \frac{\partial Expression2}{\partial \phi}
   * \f]
   */

  template< typename FieldT >
  class ProductExpression : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, expr1_, expr2_ );

    ProductExpression( const Expr::Tag tag1,
                       const Expr::Tag tag2 )
  : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      expr1_ = this->template create_field_request<FieldT>( tag1 );
      expr2_ = this->template create_field_request<FieldT>( tag2 );
    }


  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tag1_, tag2_;
    public:
      /**
       * @brief The mechanism for building a ProductExpression object
       * @tparam FieldT
       * @param productTag Production result of two expressions
       * @param tag1 The first expression in the production
       * @param tag2 The second expression in the production
       */
      Builder( const Expr::Tag productTag,
               const Expr::Tag tag1,
               const Expr::Tag tag2 ) :
        Expr::ExpressionBuilder( productTag ),
        tag1_( tag1 ),
        tag2_( tag2 )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new ProductExpression<FieldT>( tag1_, tag2_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= expr1_->field_ref() * expr2_->field_ref();
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      FieldT& dfdv = this->sensitivity_result( sensVarTag );
      dfdv <<= expr1_->sens_field_ref( sensVarTag ) * expr2_->field_ref()
               + expr1_->field_ref() * expr2_->sens_field_ref( sensVarTag );
    }
  };

    /**
     * @class DivisionExpression
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the division of two expressions
     * @author Mike
     *
     * The division is given as
     * \f[
     *    Division = numeratorExpr / denominatorExpr
     * \f]
     */

  template< typename FieldT >
  class DivisionExpression : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, numerator_, denominator_ );

    DivisionExpression( const Expr::Tag numeratorTag,
                        const Expr::Tag denominatorTag )
    : Expr::Expression<FieldT>()
      {
      this->set_gpu_runnable(true);
      numerator_   = this->template create_field_request<FieldT>( numeratorTag );
      denominator_ = this->template create_field_request<FieldT>( denominatorTag );
      }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag numeratorTag_, denominatorTag_;
    public:
      /**
       * @brief The mechanism for building a DivisionExpression object
       * @tparam FieldT
       * @param resultTag Division results of two expressions
       * @param numeratorTag The numerator expression in the division
       * @param denominatorTag The denominator expression in the division
       */
      Builder( const Expr::Tag resultTag,
               const Expr::Tag numeratorTag,
               const Expr::Tag denominatorTag ) :
                 Expr::ExpressionBuilder( resultTag ),
                 numeratorTag_( numeratorTag ),
                 denominatorTag_( denominatorTag )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new DivisionExpression<FieldT>( numeratorTag_, denominatorTag_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= numerator_->field_ref() / denominator_->field_ref();
    }
    void sensitivity( const Expr::Tag& sensVarTag ){
      FieldT & dfdv = this->sensitivity_result( sensVarTag );
      dfdv <<= numerator_->sens_field_ref( sensVarTag )  / denominator_->field_ref()
               - numerator_->field_ref() / square( denominator_->field_ref()) * denominator_->sens_field_ref(sensVarTag);
    }
  };

    /**
     * @class SubtractionExpression
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the subtraction of two expressions
     * @author Mike
     *
     * The subtraction is given as
     * \f[
     *    Subtraction = Expression1 - Expression2
     * \f]
     */

  template< typename FieldT >
  class SubtractionExpression : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, expr1_, expr2_ );

    SubtractionExpression( const Expr::Tag tag1,
                           const Expr::Tag tag2 )
    : Expr::Expression<FieldT>()
      {
      this->set_gpu_runnable(true);
      expr1_ = this->template create_field_request<FieldT>( tag1 );
      expr2_ = this->template create_field_request<FieldT>( tag2 );
      }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tag1_, tag2_;
    public:
      /**
       * @brief The mechanism for building a SubtractionExpression object
       * @tparam FieldT
       * @param productTag Production results of two expressions
       * @param tag1 The first expression in the subtraction
       * @param tag2 The second expression in the subtraction
       */
      Builder( const Expr::Tag productTag,
               const Expr::Tag tag1,
               const Expr::Tag tag2 ) :
                 Expr::ExpressionBuilder( productTag ),
                 tag1_( tag1 ),
                 tag2_( tag2 )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new SubtractionExpression<FieldT>( tag1_, tag2_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= expr1_->field_ref() - expr2_->field_ref();
    }
  };

    /**
     * @class SpeciesEnergyOffsets
     * @ingroup ReactorEnsembleUtil
     * @brief Calculate the species energy offset.
     *        It is needed for matrix transformation: \f$ dV/dU \f$ for constant volume condition.
     * @author Mike
     *
     * The species energy offset is given as
     * \f[
     *     \frac{\partial T}{\partial \rho Y_i}|_{\rho, \rho e, \rho Y_{j \ne i}} = \frac{e_{nspec}-e_i}{\rho cv}
     * \f]
     * Here, \f$ nspec \f$ is the number of species.
     */

  template< typename FieldT >
  class SpeciesEnergyOffsets : public Expr::Expression<FieldT>
  {
      DECLARE_FIELDS( FieldT, rho_, cv_ );
      DECLARE_VECTOR_OF_FIELDS( FieldT, speciesEnergies_ );

    SpeciesEnergyOffsets( const Expr::Tag rhoTag,
                          const Expr::Tag cvTag,
                          const Expr::TagList speciesEnergyTags )
    : Expr::Expression<FieldT>()
      {
      this->set_gpu_runnable(true);
      rho_ = this->template create_field_request<FieldT>( rhoTag );
      cv_ = this->template create_field_request<FieldT>( cvTag );
      this->template create_field_vector_request<FieldT>( speciesEnergyTags, speciesEnergies_ );
      }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag rhoTag_, cvTag_;
        const Expr::TagList speciesEnergyTags_;
    public:
      /**
       * @brief The mechanism for building a SpeciesEnergyOffsets object
       * @tparam FieldT
       * @param offsetTags List of offset results of species energy
       * @param rhoTag Density of gas phase
       * @param cvTag Constant volume heat capacity of gas phase
       * @param speciesEnergyTags List of all species specific energies in gas phase
       */
      Builder( const Expr::TagList offsetTags,
               const Expr::Tag rhoTag,
               const Expr::Tag cvTag,
               const Expr::TagList speciesEnergyTags ) :
                 Expr::ExpressionBuilder( offsetTags ),
                 rhoTag_( rhoTag ),
                 cvTag_( cvTag ),
                 speciesEnergyTags_( speciesEnergyTags )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new SpeciesEnergyOffsets<FieldT>( rhoTag_, cvTag_, speciesEnergyTags_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& offsets = this->get_value_vec();
      const FieldT& rho = rho_->field_ref();
      const FieldT& cv = cv_->field_ref();
      const int ns = offsets.size();
      for( size_t n=0; n<ns-1; ++n ){
        *offsets[n] <<= (speciesEnergies_[ns-1]->field_ref() - speciesEnergies_[n]->field_ref()) / rho / cv;
      }
      *offsets[ns-1] <<= - speciesEnergies_[ns-1]->field_ref() / rho / cv;
    }
  };

  /**
   * @class MassFracsOverRho
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the negative ratio between species mass fraction and density.
   *        It is needed for matrix transformation: \f$ dV/dU \f$ for constant volume condition.
   * @author Mike
   *
   * The mass fraction over rho is given as
   * \f[
   *    \frac{\partial Y_i}{\partial \rho}|_{\rho Y_i, \rho e} = - \frac{Y_i}{\rho}
   * \f]
   */

  template< typename FieldT >
  class MassFracsOverRho : public Expr::Expression<FieldT>
  {
      DECLARE_FIELD( FieldT, rho_);
      DECLARE_VECTOR_OF_FIELDS( FieldT, massFracs_ );

    MassFracsOverRho( const Expr::Tag rhoTag,
                      const Expr::TagList massFracsTags )
    : Expr::Expression<FieldT>()
      {
      this->set_gpu_runnable(true);
      rho_ = this->template create_field_request<FieldT>( rhoTag );
      this->template create_field_vector_request<FieldT>( massFracsTags, massFracs_ );
      }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag rhoTag_;
        const Expr::TagList massFracsTags_;
    public:
      /**
       * @brief The mechanism for building a MassFracsOverRho object
       * @tparam FieldT
       * @param resultTags Results of MassFracsOverRho: \f$ - \frac{Y_i}{\rho} \f$
       * @param rhoTag Density of gas phase
       * @param massFracsTags List of species mass fractions in gas phase
       */
      Builder( const Expr::TagList resultTags,
               const Expr::Tag rhoTag,
               const Expr::TagList massFracsTags ) :
                 Expr::ExpressionBuilder( resultTags ),
                 rhoTag_( rhoTag ),
                 massFracsTags_( massFracsTags )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new MassFracsOverRho<FieldT>( rhoTag_, massFracsTags_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
      const FieldT& rho = rho_->field_ref();
      const int nres = results.size();
      for( size_t n=0; n<nres; ++n ){
        *results[n] <<= - massFracs_[n]->field_ref() / rho;
      }
    }
  };

  /**
   * @class LogFunction
   * @ingroup ReactorEnsembleUtil
   * @brief Getting the result of  \f$ \log_{10} Expr \f$.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    Result = \log_{10} Expr
   * \f]
   */

  template<typename FieldT>
  class LogFunction : public Expr::Expression<FieldT> {
      DECLARE_FIELD( FieldT, ivar_);

      LogFunction( const Expr::Tag indepVarTag )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        ivar_ = this->template create_field_request<FieldT>( indepVarTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag ivarTag_;
      public:
        /**
         * @brief The mechanism for building a LogFunction object
         * @tparam FieldT
         * @param depVarTag Log result of the expression: \f$ \log_{10} Expr \f$
         * @param indepVarTag The expression need to get the log function
         */
          Builder( const Expr::Tag depVarTag,
                   const Expr::Tag indepVarTag )
                : Expr::ExpressionBuilder( depVarTag ),
                  ivarTag_( indepVarTag ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new LogFunction<FieldT>( ivarTag_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        this->value() <<= pow( 10.0, ivar_->field_ref());
      }
  };

  /**
   * @class SpeciesEnthOffsets
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the species enthalpy offset.
   *        It is needed for matrix transformation: \f$ dV/dU \f$ for constant pressure condition.
   * @author Hang
   *
   * The species energy offset is given as
   * \f[
   *    \frac{\partial T}{\partial Y_i}|_{p,h,Y_{j \ne i}} = \frac{h_{nspec}-h_i}{cp}
   * \f]
   * Here, \f$ nspec \f$ is the number of species.
   */

  template<typename FieldT>
  class SpeciesEnthOffsets : public Expr::Expression<FieldT> {
      DECLARE_FIELD( FieldT, cp_);
      DECLARE_VECTOR_OF_FIELDS( FieldT, speciesEnthalpies_ );

      SpeciesEnthOffsets( const Expr::Tag cpTag,
                          const Expr::TagList speciesEnthalpyTags )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        cp_ = this->template create_field_request<FieldT>( cpTag );
        this->template create_field_vector_request<FieldT>( speciesEnthalpyTags, speciesEnthalpies_ );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag cpTag_;
          const Expr::TagList speciesEnthalpyTags_;
      public:
        /**
         * @brief The mechanism for building a SpeciesEnthOffsets object
         * @tparam FieldT
         * @param offsetTags List of offset results of species enthalpy
         * @param cpTag Constant pressure heat capacity of gas phase
         * @param speciesEnthalpyTags List of all species specific enthalpies in gas phase
         */
          Builder( const Expr::TagList offsetTags,
                   const Expr::Tag cpTag,
                   const Expr::TagList speciesEnthalpyTags ) :
                Expr::ExpressionBuilder( offsetTags ),
                cpTag_( cpTag ),
                speciesEnthalpyTags_( speciesEnthalpyTags ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new SpeciesEnthOffsets<FieldT>( cpTag_, speciesEnthalpyTags_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        typename Expr::Expression<FieldT>::ValVec& offsets = this->get_value_vec();
        const FieldT& cp = cp_->field_ref();
        const int ns = offsets.size();
        for( size_t n = 0;n < ns - 1;++n ){
          *offsets[n] <<= ( speciesEnthalpies_[ns - 1]->field_ref() - speciesEnthalpies_[n]->field_ref()) / cp;
        }
      }
  };

  /**
   * @class RateOverRho
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the ratio between reaction rate and density.
   *        It is needed for kinetic term \f$ K \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    K = \frac{\omega_i}{\rho}
   * \f]
   *
   * The sensitivity of this term \f$ \frac{\partial K}{\partial V} \f$ is
   * <ul>
   *    <li> Sensitivity to gas phase density \f$ \rho \f$:
   *         \f[
   *             \frac{\partial K}{\partial \rho}|_{\rho, Y_i} = \frac{1}{\rho} \frac{\partial \omega_i}{\partial \rho} - \frac{\omega_i}{\rho^2}
   *         \f]
   *         </li>
   *     <li> Sensitivity to other variables:
   *          \f[
   *             \frac{\partial K}{\partial \phi} = \frac{1}{\rho} \frac{\partial \omega_i}{\partial \phi}
   *          \f]
   *           </li>
   *          Here, \f$ \phi \f$ could be \f$ T \f$ or \f$ Y_i \f$.
   * </ul>
   * In Zodiac, we choose \f$ \rho \f$,  \f$ T \f$ and \f$ Y_i \f$ as the primitive variables.
   * Therefore,
   * \f[
   *    \frac{\partial \rho}{\partial T}|_{\rho, Y_i} = 0
   * \f]
   *
   * \f[
   *    \frac{\partial \rho}{\partial Y_i}|_{\rho, T, Y_{j \ne i}} = 0
   * \f]
   */

  template<typename FieldT>
  class RateOverRho : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, rate_);

      RateOverRho( const Expr::Tag rateTag,
                   const Expr::Tag rhoTag)
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_ = this->template create_field_request<FieldT>( rhoTag );
        rate_ = this->template create_field_request<FieldT>( rateTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rateTag_, rhoTag_;
      public:
        /**
         * @brief The mechanism for building a RateOverRho object
         * @tparam FieldT
         * @param resultTag Result of RateOverRho
         * @param rateTag Chemcial reaction rate
         * @param rhoTag Density of gas phase
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag rateTag,
                   const Expr::Tag rhoTag) :
                Expr::ExpressionBuilder( resultTag ),
                rateTag_( rateTag ),
                rhoTag_( rhoTag ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new RateOverRho<FieldT>( rateTag_, rhoTag_);
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        this->value() <<= rate_->field_ref() / rho_->field_ref();
      }

      void sensitivity( const Expr::Tag& sensVarTag ){
        FieldT & dfdv = this->sensitivity_result( sensVarTag );
        dfdv <<= rate_->sens_field_ref( sensVarTag )  / rho_->field_ref()-
                 rate_->field_ref() / square( rho_->field_ref()) * rho_->sens_field_ref(sensVarTag);
      }
  };

  /**
   * @class RhoPartialH
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of density to enthalpy.
   *        It is needed for matrix transformation: \f$ dV/dU \f$ for constant pressure condition.
   * @author Hang
   *
   * The sensitivity of density to enthalpy is given as
   * \f[
   *    \frac{\partial \rho}{\partial h}|_{P, Y_i} = - \frac{\rho}{cp T}
   * \f]
   */

  template<typename FieldT>
  class RhoPartialH : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, cp_, temp_);

      RhoPartialH( const Expr::Tag rhoTag,
                   const Expr::Tag cpTag,
                   const Expr::Tag tempTag )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_ = this->template create_field_request<FieldT>( rhoTag );
        cp_ = this->template create_field_request<FieldT>( cpTag );
        temp_ = this->template create_field_request<FieldT>( tempTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rhoTag_;
          const Expr::Tag cpTag_;
          const Expr::Tag tempTag_;
      public:
        /**
         * @brief The mechanism for building a RhoPartialH object
         * @tparam FieldT
         * @param resultTag Result of RhoPartialH
         * @param rhoTag Density of gas phase
         * @param cpTag Constant pressure heat capacity of gas phase
         * @param tempTag Temperature of gas phase
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag rhoTag,
                   const Expr::Tag cpTag,
                   const Expr::Tag tempTag ) :
                Expr::ExpressionBuilder( resultTag ),
                rhoTag_( rhoTag ),
                cpTag_( cpTag ),
                tempTag_( tempTag ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new RhoPartialH<FieldT>( rhoTag_, cpTag_, tempTag_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
        const FieldT& rho = rho_->field_ref();
        const FieldT& cp = cp_->field_ref();
        const FieldT& temp = temp_->field_ref();
        this->value() <<= -rho / ( cp * temp );
      }
  };

  /**
   * @class RhoPartialY
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of density to species mass fraction.
   *        It is needed for matrix transformation: \f$ dV/dU \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    \frac{\partial \rho}{\partial Y_i}|_{P, h, Y_j} = \frac{\rho}{cp T} (h_i-h_{nspec}) - \rho M_{mw} ( \frac{1}{M_i} - \frac{1}{M_{nspec}})
   * \f]
   * Here, \f$ nspec \f$ is the number of species.
   */

  template<typename FieldT>
  class RhoPartialY : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, cp_, temp_, mmw_);
      DECLARE_VECTOR_OF_FIELDS( FieldT, molarWeights_);
      DECLARE_VECTOR_OF_FIELDS( FieldT, speciesEnthalpies_);

      RhoPartialY( const Expr::Tag rhoTag,
                   const Expr::Tag cpTag,
                   const Expr::Tag tempTag,
                   const Expr::Tag mmwTag,
                   const Expr::TagList molarWeightTags,
                   const Expr::TagList speciesEnthalpyTags )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_  = this->template create_field_request<FieldT>( rhoTag );
        cp_   = this->template create_field_request<FieldT>( cpTag );
        temp_ = this->template create_field_request<FieldT>( tempTag );
        mmw_ = this->template create_field_request<FieldT>( mmwTag );
        this->template create_field_vector_request<FieldT>( molarWeightTags, molarWeights_ );
        this->template create_field_vector_request<FieldT>( speciesEnthalpyTags, speciesEnthalpies_ );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rhoTag_, cpTag_, tempTag_, mmwTag_;
          const Expr::TagList molarWeightTags_, speciesEnthalpyTags_;
      public:
        /**
         * @brief The mechanism for building a RhoPartialY object
         * @tparam FieldT
         * @param resultTags List of results for RhoPartialY
         * @param rhoTag Density of gas phase
         * @param cpTag Constant pressure heat capacity of gas phase
         * @param tempTag Temperature of gas phase
         * @param mmwTag Mixture molecular weight
         * @param molarWeightTags List of species molecular weights
         * @param speciesEnthalpyTags List of species specific enthalpy
         */
          Builder( const Expr::TagList resultTags,
                   const Expr::Tag rhoTag,
                   const Expr::Tag cpTag,
                   const Expr::Tag tempTag,
                   const Expr::Tag mmwTag,
                   const Expr::TagList molarWeightTags,
                   const Expr::TagList speciesEnthalpyTags ) :
                Expr::ExpressionBuilder( resultTags ),
                rhoTag_( rhoTag ),
                cpTag_( cpTag ),
                tempTag_( tempTag ),
                mmwTag_( mmwTag ),
                molarWeightTags_( molarWeightTags ),
                speciesEnthalpyTags_( speciesEnthalpyTags ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new RhoPartialY<FieldT>( rhoTag_, cpTag_, tempTag_, mmwTag_, molarWeightTags_, speciesEnthalpyTags_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
        const FieldT& rho = rho_->field_ref();
        const FieldT& cp = cp_->field_ref();
        const FieldT& temp = temp_->field_ref();
        const FieldT& mmw = mmw_->field_ref();
        const int nres = results.size();
        for( size_t n = 0;n < nres;++n ){
          *results[n] <<= rho / (temp *cp) * ( speciesEnthalpies_[n]->field_ref() - speciesEnthalpies_[nres - 1]->field_ref())
                                             - rho * mmw *(1 / molarWeights_[n]->field_ref() - 1 / molarWeights_[nres - 1]->field_ref());
        }
      }
  };

  /**
   * @class YmixPartialY
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of mixing term of mass fraction equation to species mass fraction.
   *        It is needed for matrix formation: \f$ dW/dV \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    \frac{\partial Y_{i,mix}}{\partial Y_i}|_{\rho, T, Y_{j \ne i}} = - \frac{\rho_{in}}{\rho \tau_{mix}}
   * \f]
   * Here, \f$ Y_{i,mix} \f$ is the mixing term of mass fraction equation of speceies \f$ i \f$.
   * \f[
   *    Y_{i,mix} = \frac{\rho_{in}}{\rho \tau_{mix}} (Y_{i,in}-Y_i)
   * \f]
   *
   */

  template<typename FieldT>
  class YmixPartialY : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, rhoIn_, tau_);

      YmixPartialY( const Expr::Tag rhoTag,
                    const Expr::Tag rhoInflowTag,
                    const Expr::Tag tauTag )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_   = this->template create_field_request<FieldT>( rhoTag );
        rhoIn_ = this->template create_field_request<FieldT>( rhoInflowTag );
        tau_   = this->template create_field_request<FieldT>( tauTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rhoTag_, rhoInTag_, tauTag_;
      public:
        /**
         * @brief The mechanism for building a YmixPartialY object
         * @tparam FieldT
         * @param resultTag Result of YmixPartialY
         * @param rhoTag Density of gas phase
         * @param rhoInflowTag Inflowing density of gas phase
         * @param tauTag Residence time based on inlet volume flow rate for gas phase \f$\tau_{mix} = \frac{V}{\dot V_{in}}\f$
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag rhoTag,
                   const Expr::Tag rhoInflowTag,
                   const Expr::Tag tauTag ) :
                Expr::ExpressionBuilder( resultTag ),
                rhoTag_( rhoTag ),
                rhoInTag_( rhoInflowTag ),
                tauTag_( tauTag ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new YmixPartialY<FieldT>( rhoTag_, rhoInTag_, tauTag_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        const FieldT& rho = rho_->field_ref();
        const FieldT& rhoIn = rhoIn_->field_ref();
        const FieldT& tau = tau_->field_ref();

        this->value() <<= - rhoIn / (rho * tau);
      }
  };

  /**
   * @class HmixPartialT
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of mixing term of enthalpy equation to temperature.
   *        It is needed for matrix formation: \f$ dW/dV \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    \frac{\partial h_{mix}}{\partial T}|_{\rho, Y_i} = - \frac{\rho_{in} cp}{\rho \tau_{mix}}
   * \f]
   * Here, \f$ h_{mix} \f$ is the mixing term of enthalpy equation.
   * \f[
   *    h_{mix} = \frac{\rho_{in}}{\rho \tau_{mix}} (h_{in}-h)
   * \f]
   */

  template<typename FieldT>
  class HmixPartialT : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, rhoIn_, tau_, cp_);

      HmixPartialT( const Expr::Tag rhoTag,
                    const Expr::Tag rhoInflowTag,
                    const Expr::Tag tauTag,
                    const Expr::Tag cpTag)
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_   = this->template create_field_request<FieldT>( rhoTag );
        rhoIn_ = this->template create_field_request<FieldT>( rhoInflowTag );
        tau_   = this->template create_field_request<FieldT>( tauTag );
        cp_    = this->template create_field_request<FieldT>( cpTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rhoTag_, rhoInTag_, tauTag_, cpTag_;
      public:
        /**
         * @brief The mechanism for building a HmixPartialT object
         * @tparam FieldT
         * @param resultTag Result for HmixPartialT
         * @param rhoTag Density of gas phase
         * @param rhoInflowTag Inflowing density of gas phase
         * @param tauTag Residence time based on inlet volume flow rate for gas phase \f$\tau_{mix} = \frac{V}{\dot V_{in}}\f$
         * @param cpTag Constant pressure heat capacity of gas phase
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag rhoTag,
                   const Expr::Tag rhoInflowTag,
                   const Expr::Tag tauTag,
                   const Expr::Tag cpTag) :
                Expr::ExpressionBuilder( resultTag ),
                rhoTag_( rhoTag ),
                rhoInTag_( rhoInflowTag ),
                tauTag_( tauTag ),
                cpTag_(cpTag){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new HmixPartialT<FieldT>( rhoTag_, rhoInTag_, tauTag_, cpTag_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        const FieldT& rho = rho_->field_ref();
        const FieldT& rhoIn = rhoIn_->field_ref();
        const FieldT& tau = tau_->field_ref();
        const FieldT& cp = cp_->field_ref();

        this->value() <<= - rhoIn * cp / (rho * tau);
      }
  };

  /**
   * @class HmixPartialY
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of mixing term of enthalpy equation to mass fraction of species \f$ i \f$.
   *        It is needed for matrix formation: \f$ dW/dV \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    \frac{\partial h_{mix}}{\partial Y_i}|_{\rho, T, Y_j} = \frac{\rho_{in} (h_{nspec}-h_i)}{\rho \tau_{mix}}
   * \f]
   * Here, \f$ h_{mix} \f$ is the mixing term of enthalpy equation.
   * \f[
   *    h_{mix} = \frac{\rho_{in}}{\rho \tau_{mix}} (h_{in}-h)
   * \f]
   * Here, \f$ nspec \f$ is the number of species.
   */

  template<typename FieldT>
  class HmixPartialY : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, rhoIn_, tau_);
      DECLARE_VECTOR_OF_FIELDS( FieldT, speciesEnthalpies_ );

      HmixPartialY( const Expr::Tag rhoTag,
                    const Expr::Tag rhoInflowTag,
                    const Expr::Tag tauTag,
                    const Expr::TagList speciesEnthalpyTags )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        rho_   = this->template create_field_request<FieldT>( rhoTag );
        rhoIn_ = this->template create_field_request<FieldT>( rhoInflowTag );
        tau_   = this->template create_field_request<FieldT>( tauTag );
        this->template create_field_vector_request<FieldT>( speciesEnthalpyTags, speciesEnthalpies_ );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag rhoTag_, rhoInTag_, tauTag_;
          const Expr::TagList speciesEnthalpyTags_;
      public:
        /**
         * @brief The mechanism for building a HmixPartialY object
         * @tparam FieldT
         * @param resultTags List of results for HmixPartialY
         * @param rhoTag Density of gas phase
         * @param rhoInflowTag Inflowing density of gas phase
         * @param tauTag Residence time based on inlet volume flow rate for gas phase \f$\tau_{mix} = \frac{V}{\dot V_{in}}\f$
         * @param speciesEnthalpyTags List of species specific enthalpy
         */
          Builder( const Expr::TagList resultTags,
                   const Expr::Tag rhoTag,
                   const Expr::Tag rhoInflowTag,
                   const Expr::Tag tauTag,
                   const Expr::TagList speciesEnthalpyTags) :
                Expr::ExpressionBuilder( resultTags ),
                rhoTag_( rhoTag ),
                rhoInTag_( rhoInflowTag ),
                tauTag_( tauTag ),
                speciesEnthalpyTags_(speciesEnthalpyTags){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new HmixPartialY<FieldT>( rhoTag_, rhoInTag_, tauTag_, speciesEnthalpyTags_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
        const FieldT& rho = rho_->field_ref();
        const FieldT& rhoIn = rhoIn_->field_ref();
        const FieldT& tau = tau_->field_ref();

        const int ns = results.size();
        for( size_t n = 0;n < ns - 1;++n ){
          *results[n] <<=  rhoIn* ( speciesEnthalpies_[ns - 1]->field_ref() - speciesEnthalpies_[n]->field_ref()) / (rho * tau);
        }
      }
  };

  /**
   * @class HheatPartialT
   * @ingroup ReactorEnsembleUtil
   * @brief Calculate the sensitivity of heat transfer term of enthalpy equation to temperature.
   *        It is needed for matrix formation: \f$ dQ/dV \f$ for constant pressure condition.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    \frac{\partial h_{heat}}{\partial T}|_{\rho, Y_i} = - \frac{3 k}{\rho r}
   * \f]
   * Here, \f$ h_{heat} \f$ is the heat transfer term of enthalpy equation.
   * \f[
   *    h_{heat} = - \frac{3 k}{\rho r} (T-T_{inf})
   * \f]
   * \f$ k \f$ is the convection coefficient. \f$ r \f$ is the reactor radius. \f$ T_{inf} \f$ is the surroundings temperature.
   */

  template<typename FieldT>
  class HheatPartialT : public Expr::Expression<FieldT> {

      DECLARE_FIELD( FieldT, rho_);
      const double convectionCoeff_, radiusReactor_;

      HheatPartialT( const Expr::Tag rhoTag,
                     const double convectionCoeff,
                     const double radiusReactor)
            : Expr::Expression<FieldT>(),
              convectionCoeff_( convectionCoeff ),
              radiusReactor_( radiusReactor){
        this->set_gpu_runnable( true );
        rho_   = this->template create_field_request<FieldT>( rhoTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const double convectionCoeff_, radiusReactor_;
          const Expr::Tag rhoTag_;
      public:
        /**
         * @brief The mechanism for building a HheatPartialT object
         * @tparam FieldT
         * @param resultTag Result of HheatPartialT
         * @param rhoTag Density of gas phase
         * @param convectionCoeff Convective heat transfer coefficient between gas and particle
         * @param radiusReactor Radius of the reactor
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag rhoTag,
                   const double convectionCoeff,
                   const double radiusReactor) :
                Expr::ExpressionBuilder( resultTag ),
                rhoTag_( rhoTag ),
                convectionCoeff_( convectionCoeff ),
                radiusReactor_( radiusReactor ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new HheatPartialT<FieldT>( rhoTag_, convectionCoeff_, radiusReactor_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        const FieldT& rho = rho_->field_ref();

        this->value() <<= - 3 * convectionCoeff_ / ( rho  * radiusReactor_ );
      }
  };


    /**
    * @class YNRHS
    * @ingroup ReactorEnsembleUtil
    * @brief Calculate the RHS of species equation for the Nth species, \f$\frac{dY_N}{dt}\f$.
    * @author Hang
    *
    * The result is given as
    * \f[
    *    \frac{dY_N}{dt} = - \sum_i^{N-1} \frac{dY_i}{dt}
    * \f]
    * Here, \f$ N \f$ is the number of species.
*/

  template<typename FieldT>
  class YNRHS : public Expr::Expression<FieldT> {
    DECLARE_VECTOR_OF_FIELDS( FieldT, yrhs_);
      const int nspec_;

    YNRHS( const Expr::TagList yrhsTags)
          : Expr::Expression<FieldT>(),
            nspec_(yrhsTags.size()){
        this->set_gpu_runnable( true );
        this->template create_field_vector_request<FieldT>( yrhsTags, yrhs_ );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
        Expr::TagList yrhsTag_;
      public:
        /**
         * @brief The mechanism for building a HheatPartialT object
         * @tparam FieldT
         * @param resultTag RHS of species equation for the Nth species, \f$\frac{dY_N}{dt}\f$.
         * @param yrhsTags RHS list of species equation for all species
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::TagList yrhsTags) :
                Expr::ExpressionBuilder( resultTag )
          {
            for( size_t i=0; i<yrhsTags.size(); ++i ){
              if( yrhsTags[i] != resultTag ) yrhsTag_.push_back(yrhsTags[i]);
            }
          }

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new YNRHS<FieldT>( yrhsTag_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        FieldT& result = this->value();
        result <<= 0.0;
        for( size_t i=0; i<nspec_-1; i++){
          result <<= result - yrhs_[i]->field_ref();
        }
      }
      void sensitivity( const Expr::Tag& sensVarTag ){
        using namespace SpatialOps;
        FieldT& dfdv = this->sensitivity_result( sensVarTag );
        dfdv <<= 0.0;
        for( size_t i=0; i<nspec_-1; i++){
          dfdv <<= dfdv - yrhs_[i]->sens_field_ref(sensVarTag);
        }
      }
  };

}


#endif /* TRANSFORMATIONMATRIXEXPRESSIONS_H_ */
