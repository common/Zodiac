/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   ReactorEnsembleUtil.h
 *  \date   Sep 21, 2016
 *  \author Mike
 */

#ifndef REACTOR_ENSEMBLE_UTIL_H_
#define REACTOR_ENSEMBLE_UTIL_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <yaml-cpp/yaml.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/MatVecFields.h>

#include <expression/ExprLib.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <pokitt/kinetics/ReactionRates.h>

#include <FieldTypes.h>

namespace ReactorEnsembleUtil{

  struct ReactorEnsembleTags
  {
    const Expr::Tag tempTag,           ///< temperature
                    presTag,           ///< pressure
                    egyTag,            ///< specific internal energy
                    enthTag,           ///< specific enthalpy
                    rhoTag,            ///< mixture mass density
                    mmwTag,            ///< mixture molecular weight
                    rhoEgyTag,         ///< volumetric internal energy
                    keTag,             ///< specific kinetic energy
                    cvTag,             ///< specific constant-volume heat capacity
                    cpTag,             ///< specific constant-pressure heat capacity
                    expEigTag,         ///< chemical explosive eigenvalue
                    reactEigTag,       ///< reactive explosive eigenvalue
                    gesatEigTag,       ///< eigenvalue for gesat adaptation
                    tauMixTag,         ///< mixing time
                    tauMixLogTag,      ///< log exponent of mixing time( 10^a, a is this tag)
                    mmwInflowTag,      ///< inflowing mixture molecular weight
                    tempInflowTag,     ///< inflowing temperature
                    presInflowTag,     ///< inflowing pressure
                    rhoInflowTag,      ///< inflowing mixture mass density
                    egyInflowTag,      ///< inflowing energy
                    rhoEgyInflowTag,   ///< inflowing specific volumetric energy
                    enthInflowTag,     ///< inflowing enthalpy
                    rhoKinRhsTag,      ///< kinetics RHS for mass equation
                    rhoEgyKinRhsTag,   ///< kinetics RHS for energy equation
                    presKinRhsTag,     ///< kinetics RHS for pressure equation
                    enthKinRhsTag,     ///< kinetics RHS for enthalpy equation
                    rhoMixRhsTag,      ///< mixing RHS for mass equation
                    rhoEgyMixRhsTag,   ///< mixing RHS for energy equation
                    presMixRhsTag,     ///< mixing RHS for pressure equation
                    enthMixRhsTag,     ///< mixing RHS for enthalpy equation
                    rhoEgyHeatRhsTag,  ///< heat transferring RHS for energy equation
                    enthHeatRhsTag,    ///< heat transferring RHS for enthalpy equation
                    rhoFullRhsTag,     ///< full RHS for mass equation
                    rhoEgyFullRhsTag,  ///< full RHS for energy equation
                    presFullRhsTag,    ///< full RHS for pressure equation
                    enthFullRhsTag,    ///< full RHS for enthalpy equation
                    rhocvTag,          ///< rho * cv - needed for matrix transformation
                    invrhoTag,         ///< 1 / rho - needed for matrix transformation
                    invrhocvTag,       ///< 1 / (rho * cv) - needed for matrix transformation
                    rhoOverPresTag,    ///< rho/P - needed for matrix transformation for constant pressure
                    rhoPartialHTag,    ///< (\partial rho)/(\partial h)=-rho/(cp*T) - needed for matrix transformation for constant pressure
                    invCpTag,          ///< 1 / cp - needed for matrix transformation for constant pressure
                    invTauTag,         ///< 1 / tau - needed for matrix transformation of particle equations for constant pressure
                    yMixPartialYTag,   ///< (\partial Yi_mix)/(\partial Yi) = - rho_in / (tau * rho) - needed for matrix build for constant pressure
                    hMixPartialTTag,   ///< (\partial h_mix)/(\partial T) = - rho_in * cp / (tau * rho) - needed for matrix build for constant pressure
                    thermalConducTag,  ///< thermal conductivity of gas mixture -- used to get convective coefficient between gas and particles
                    dynviscosityTag,   ///< dynamic viscosity of gas mixture -- used to get particle Re number: kg/m/s
                    scNumberTag,       ///< Schmidt number -- used to get sherwood number
                    convecCoeffTag,    ///< convection coefficient between gas phase and surroundings
                    convecTempTag,     ///< temperature of surroundings, having convection heat transfer with gas phase
                    dsTag,             ///< dualtime
                    dtTag,             ///< timestep
                    mixtureFractionTag,///< mixture fraction (used for initialization)
                    timeTag;           ///< simulation time

    Expr::TagList moleTags,            ///< mole fractions
                  massTags,            ///< mass fractions
                  moleInflowTags,      ///< inflowing mole fractions
                  massInflowTags,      ///< inflowing mass fractions
                  rhoYTags,            ///< species mass densities
                  rateTags,            ///< species net mass production rates
                  specEgyTags,         ///< species specific internal energies
                  specEnthTags,        ///< species specific ehthalpy
                  molarWeightTags,     ///< species molar mass
                  rhoYInflowTags,      ///< inflowing species mass densities
                  rhoYKinRhsTags,      ///< kinetics RHS for species mass equations
                  rhoYMixRhsTags,      ///< mixing RHS for species mass equations
                  rhoYFullRhsTags,     ///< full RHS for species mass equations
                  egyOffsetTags,       ///< (e_n-e_i)/rho/cv - for matrix transformation
                  mYoverRhoTags,       ///< -Y_i/rho - for matrix transformation
                  yKinRhsTags,         ///< kinetics RHS for species mass equations for constant pressure
                  yMixRhsTags,         ///< mixing RHS for species mass equations for constant pressure
                  yFullRhsTags,        ///< full RHS for species mass equations for constant pressure
                  enthOffsetTags,      ///< (h_n-h_i)/cp - for matrix transformation for constant pressure
                  rhoPartialYTags,     ///< (\partial rho)/(\partial Y_i) - for matrix transformation
                  hMixPartialYTags;    ///< (\partial h_mix)/(\partial Y_i) - needed for matrix build for constant pressure

    ReactorEnsembleTags( const Expr::Context& state );
  };
   /**
     * @brief Set the initial conditions for constant volume reactor
     */
  void setup_cv( std::set<Expr::ExpressionID>& initRoots,
                 Expr::ExpressionFactory& initFactory,
                 const YAML::Node& parser);
  /**
   * @brief Register all expressions required for constant volume reactor and add equation into the time integrator
   */
  void build_equation_system_cv( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                                 const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                 const YAML::Node& parser,
                                 int& equIndex,
                                 const double tempTolerance = 1e-3,
                                 const double maximumTemp   = 5000,
                                 const double maxTempIter   = 1000 );

  /**
   * @brief Initialize the reactor system for constant volume reactor
   */
  void initialize_cv( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                      Expr::FieldManagerList& fml,
                      const Expr::ExprPatch& patch,
                      Expr::ExpressionTree& initTree,
                      const Expr::Tag dsTag,
                      const double gesatMax,
                      const double timeStep,
                      const bool isRestart);
   /**
    * @brief Set the initial conditions for constant pressure reactor
    */
  void setup_cp( std::set<Expr::ExpressionID>& initRoots,
                 Expr::ExpressionFactory& initFactory,
                 const YAML::Node& parser);
  /**
   * @brief Register all expressions required for constant pressure reactor and add equation into the time integrator
   */
  void build_equation_system_cp( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                                 const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                 const YAML::Node& parser,
                                 int& equIndex,
                                 const double tempTolerance = 1e-3,
                                 const double maximumTemp = 5000,
                                 const double maxTempIter = 1000 );

  /**
   * @brief Initialize the reactor system for constant pressure reactor
   */
  void initialize_cp( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                      Expr::FieldManagerList& fml,
                      const Expr::ExprPatch& patch,
                      Expr::ExpressionTree& initTree,
                      const Expr::Tag dsTag,
                      const double gesatMax,
                      const double timeStep,
                      const bool isRestart);

} // namespace ReactorEnsembleUtil


#endif /* REACTOR_ENSEMBLE_UTIL_H_ */
