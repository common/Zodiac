/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file    ZeroDimMixing.h
 *  @par     Getting the mixing term in equations for both constant pressure and constant volume conditions
 *  @date    Oct 20, 2016 / Oct 30, 2018
 *  @authors Mike, Hang
 */

#ifndef ZERODIMMIXING_H_
#define ZERODIMMIXING_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil{

    /**
     * @class ZeroDMixing
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the mixing term in gas phase equations under constant volume conditions and in particle equations
     * @author Mike
     *
     * The mixing term \f$ W \f$ is given as
     * \f[
     *     W = \frac{\phi_{in}-\phi}{\tau_{mix}}
     * \f]
     *
     * Here, \f$ \phi \f$ is the variable that the equation solves.
     * In our case, it could be \f$ \rho \f$, \f$ (\rho Y_i) \f$, and \f$ (\rho e) \f$ for gas phase under constant volume condition
     * and could be \f$ m_p \f$, \f$ T_p \f$ and other variables solved for coal particles.
     */

    template< typename FieldT >
    class ZeroDMixing : public Expr::Expression<FieldT> {

        DECLARE_FIELDS( FieldT, inflow_, var_, mixingTime_ );
        ZeroDMixing( const Expr::Tag& inflowTag,
                     const Expr::Tag& varTag,
                     const Expr::Tag& mixingTimeTag )
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable(true);
          inflow_ = this->template create_field_request<FieldT>( inflowTag );
          var_ = this->template create_field_request<FieldT>( varTag );
          mixingTime_ = this->template create_field_request<FieldT>( mixingTimeTag );
        }

    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag inflowTag_, varTag_, mixingTimeTag_;
        public:
          /**
           * @brief The mechanism for building a ZeroDMixing object
           * @tparam FieldT
           * @param mixingTag Mixing term in gas phase equations under constant volume conditions and in particle equations
           * @param inflowTag Inflowing value of the variable that the equation solves
           * @param varTag Value of the variable in the reactor and the outlet
           * @param mixingTimeTag Residence time for gas phase \f$\tau_{mix} = \frac{V}{\dot V}\f$ and for particle phase \f$\tau_{mix} = \frac{N}{\dot N} \f$
            */
            Builder( const Expr::Tag& mixingTag,
                     const Expr::Tag& inflowTag,
                     const Expr::Tag& varTag,
                     const Expr::Tag& mixingTimeTag )
                  : Expr::ExpressionBuilder( mixingTag ),
                    inflowTag_( inflowTag ),
                    varTag_( varTag ),
                    mixingTimeTag_( mixingTimeTag ){}

            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new ZeroDMixing<FieldT>( inflowTag_, varTag_, mixingTimeTag_ );
            }
        };

        void evaluate()
        {
          using namespace SpatialOps;
          this->value() <<= ( inflow_->field_ref() - var_->field_ref() ) / mixingTime_->field_ref();
        }

        void sensitivity( const Expr::Tag& sensVarTag )
        {
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& vin = this->inflow_->field_ref();
          const FieldT& var = this->var_->field_ref();
          const FieldT& tau = this->mixingTime_->field_ref();
          const FieldT& dvindv = this->inflow_->sens_field_ref( sensVarTag );
          const FieldT& dvardv = this->var_->sens_field_ref( sensVarTag );
          const FieldT& dtaudv = this->mixingTime_->sens_field_ref( sensVarTag );
          dfdv <<= 1 / tau * ( dvindv - dvardv - ( vin - var ) / tau * dtaudv );
        }
    };

    /**
     * @class ZeroDMixingCP
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the mixing term in gas phase equations under constant pressure conditions
     * @author Hang
     *
     * The mixing term \f$ W \f$ is given as
     * \f[
     *     W = \frac{\rho_{in}}{\rho \tau_{mix}}(\phi_{in}-\phi)
     * \f]
     *
     * Here, \f$ \phi \f$ is the variable that the equation solves.
     * In our case, it could be \f$ p \f$, \f$ Y_i \f$, and \f$ h \f$ for gas phase under constant pressure condition.
     */

    template< typename FieldT >
    class ZeroDMixingCP : public Expr::Expression<FieldT> {

        DECLARE_FIELDS( FieldT, inflow_, var_, mixingTime_, rhoInflow_, rho_);

        ZeroDMixingCP( const Expr::Tag& inflowTag,
                       const Expr::Tag& varTag,
                       const Expr::Tag& mixingTimeTag,
                       const Expr::Tag& rhoInflowTag,
                       const Expr::Tag& rhoTag )
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable(true);
          inflow_     = this->template create_field_request<FieldT>( inflowTag     );
          var_        = this->template create_field_request<FieldT>( varTag        );
          mixingTime_ = this->template create_field_request<FieldT>( mixingTimeTag );
          rhoInflow_  = this->template create_field_request<FieldT>( rhoInflowTag  );
          rho_        = this->template create_field_request<FieldT>( rhoTag        );
        }

    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag inflowTag_, varTag_, mixingTimeTag_, rhoInflowTag_, rhoTag_;
        public:
          /**
           * @brief The mechanism for building a ZeroDMixingCP object
           * @tparam FieldT
           * @param mixingTag Mixing term in gas phase equations under constant pressure conditions
           * @param inflowTag Inflowing value of the variable that the equation solves
           * @param varTag Value of the variable in the reactor and the outlet
           * @param mixingTimeTag Residence time based on inlet volume flow rate for gas phase \f$\tau_{mix} = \frac{V}{\dot V_{in}}\f$
           * @param rhoInflowTag Inflowing density value of gas phase
           * @param rhoTag Density value of gas phase in the reactor and the outlet
           *
          */
            Builder( const Expr::Tag& mixingTag,
                     const Expr::Tag& inflowTag,
                     const Expr::Tag& varTag,
                     const Expr::Tag& mixingTimeTag,
                     const Expr::Tag& rhoInflowTag,
                     const Expr::Tag& rhoTag )
                  : Expr::ExpressionBuilder( mixingTag ),
                    inflowTag_( inflowTag ),
                    varTag_( varTag ),
                    mixingTimeTag_( mixingTimeTag ),
                    rhoInflowTag_( rhoInflowTag ),
                    rhoTag_( rhoTag )
            {}

            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new ZeroDMixingCP<FieldT>( inflowTag_, varTag_, mixingTimeTag_, rhoInflowTag_, rhoTag_);
            }
        };

        void evaluate()
        {
          using namespace SpatialOps;
          this->value() <<= rhoInflow_->field_ref() / (rho_->field_ref() * mixingTime_->field_ref() )
                            * ( inflow_->field_ref() - var_->field_ref() );
        }

        void sensitivity( const Expr::Tag& sensVarTag )
        {
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& vin       = inflow_->field_ref();
          const FieldT& var       = var_->field_ref();
          const FieldT& tau       = mixingTime_->field_ref();
          const FieldT& rhoInflow = rhoInflow_->field_ref();
          const FieldT& rho       = rho_->field_ref();
          const FieldT& dvindv    = inflow_->sens_field_ref( sensVarTag );
          const FieldT& dvardv    = var_->sens_field_ref( sensVarTag );
          const FieldT& dtaudv    = mixingTime_->sens_field_ref( sensVarTag );
          const FieldT& drhoIndv  = rhoInflow_->sens_field_ref( sensVarTag );
          const FieldT& drhodv    = rho_->sens_field_ref( sensVarTag );
          dfdv <<= ( drhoIndv * (vin - var) + rhoInflow * (dvindv - dvardv)
                     - rhoInflow * (vin - var) * (drhodv * tau + rho * dtaudv) / (rho * tau) )
                   / (rho * tau);
        }
    };

} // namespace ReactorEnsembleUtil

#endif /* ZERODIMMIXING_H_ */
