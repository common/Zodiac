#ifndef INITIAL_INFLOW_CONDITIONS_WRITER_H
#define INITIAL_INFLOW_CONDITIONS_WRITER_H

#include <expression/FieldWriter.h>
#include <expression/FieldManagerList.h>
#include "parser/ParseInputFile.h"

/**
 * @class InitialInflowConditionsWriter
 * @author Hang Zhou
 *
 * @brief  Setting initial and inflow conditions based on database.
 *         When having grid variable, like surrounding temperature for heat transfer,
 *         we can use this class to set responding initial or inflow conditions from the database,
 *         instead of setting them as constant for all grids from input file.
 */

class InitialInflowConditionsWriter
{
  const YAML::Node parser_;
  Expr::FieldManagerList& fml_;
  std::set<Expr::ExpressionID>& initRoots_;
  Expr::ExpressionFactory& initFactory_;

public:
  /**
   * @param parser yaml node read from input file
   * @param fml the FieldManagerList that holds all of the fields that will be considered for output.
   * @param initRoots initial root
   * @param initFactory initial factory
   */
  explicit InitialInflowConditionsWriter( const YAML::Node parser,
                                          Expr::FieldManagerList& fml,
                                          std::set<Expr::ExpressionID>& initRoots,
                                          Expr::ExpressionFactory& initFactory);

  ~InitialInflowConditionsWriter();


  /**
   * @brief register inflow and initial fields based on database.
   */
  void
  set_init_inflow_fields_from_file();
};

template< typename FieldT >
class read_from_file : public Expr::Expression<FieldT>
{
    const std::string alias_;
    std::vector<std::string> dataVector_;

    read_from_file( const std::string alias,
                    const std::vector<std::string> DataVector)
          : Expr::Expression<FieldT>(),
                alias_( alias),
                dataVector_(DataVector)
    {
      this->set_gpu_runnable(true);
    }

public:
    class Builder : public Expr::ExpressionBuilder
    {
        const std::string alias_;
        std::vector<std::string> dataVector_;
    public:
        /**
         * @brief The mechanism for building a read_from_file object
         * @tparam FieldT
         * @param fieldTag field that needs to be registered
         * @param alias alias of the field
         * @param DataVector vector including all data
         */
        Builder( const Expr::Tag fieldTag,
                 const std::string alias,
                 const std::vector<std::string> DataVector)
              : Expr::ExpressionBuilder( fieldTag ),
                alias_( alias ),
                dataVector_( DataVector )
        {}
        ~Builder(){}
        Expr::ExpressionBase* build() const{
          return new read_from_file<FieldT>( alias_, dataVector_ );
        }
    };


    void evaluate()
    {
      using namespace SpatialOps;
      FieldT& field = this->value();
      for (const auto& line : dataVector_) {
        std::istringstream iss(line);
        std::vector<std::string> result{ std::istream_iterator<std::string>(iss), {} }; //split line by space

        if(result[0] == alias_){
          typename FieldT::iterator ifld = field.begin();
          int i =1;
          for( ;ifld != field.end();++ifld, ++i ){
            *ifld = std::stof(result[i]);
          }
          break;
        }
        else{
          if(&line == &dataVector_.back()){
            if(alias_.substr( 0, 2 ) == "X_" || alias_.substr( 0, 2 ) == "Y_"){
              std::cout << "Could not find data for " << alias_ << " in the database file. It is set to be zero." << std::endl;
              field <<= 0.0;
            }
            else{
              std::ostringstream msg;
              msg << "Must give data for " << alias_ << " in the database file." << std::endl;
              throw std::runtime_error( msg.str());
            }
          }
        }

      }
    }

};

#endif //INITIAL_INFLOW_CONDITIONS_WRITER_H
