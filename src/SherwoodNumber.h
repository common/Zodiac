/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   SherwoodNumber.h
 *  @par    Getting Sherwood number using Reynolds number and Schmidt number
 *  @date   Oct 4, 2018
 *  @author hang
 *
 */

#ifndef SHERWOODNUMBER_H_
#define SHERWOODNUMBER_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil {

  /**
   * @class SherwoodNumber
   * @ingroup ReactorEnsembleUtil
   * @brief Getting Sherwood number using Reynolds number and Schmidt number
   *
   * The expression is given as
   * \f[
   *    Sh = 2 + 0.6 Re^{1/2} Sc^{1/3}
   * \f]
   *
   * The sensitivity of the result is given as
   * \f[
   *    \frac{\partial Sh}{\partial \phi} = Re^{1/2} Sc^{1/3} (\frac{0.3}{Re}\frac{\partial Re}{\partial \phi}
   *                                                          + \frac{0.2}{Sc}\frac{\partial Sc}{\partial \phi})
   * \f]
   */

    template<typename FieldT>
    class SherwoodNumber : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, scnumber_, renumber_ )

        SherwoodNumber( const Expr::Tag scNumberTag,
                        const Expr::Tag renumberTag)
            : Expr::Expression<FieldT>(){
          this->set_gpu_runnable( true );
          scnumber_ = this->template create_field_request<FieldT>( scNumberTag );
          renumber_ = this->template create_field_request<FieldT>( renumberTag );
      }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag scnumberTag_, renumberTag_;
        public:
          /**
           * @brief The mechanism for building a SherwoodNumber object
           * @tparam FieldT
           * @param shNumberTag Sherwood Number
           * @param scNumberTag Reynolds Number
           * @param reNumberTag Schmidt Number
           */
            Builder( const Expr::Tag shNumberTag,
                     const Expr::Tag scNumberTag,
                     const Expr::Tag reNumberTag)
                  : Expr::ExpressionBuilder( shNumberTag        ),
                    scnumberTag_           ( scNumberTag        ),
                    renumberTag_           ( reNumberTag        )
            {}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
                return new SherwoodNumber<FieldT>( scnumberTag_, renumberTag_ );
            }
        };


        void evaluate(){
            using namespace SpatialOps;

          this->value() <<= 2.0 + 0.6 * sqrt(renumber_->field_ref()) * pow( scnumber_->field_ref(), 1.0/3.0) ;
        }


        void sensitivity( const Expr::Tag& sensVarTag ){
            FieldT & dfdv = this->sensitivity_result( sensVarTag );
            const FieldT& sc = scnumber_->field_ref();
            const FieldT& dScdv = scnumber_->sens_field_ref( sensVarTag );
            const FieldT& re = renumber_->field_ref();
            const FieldT& dRedv = renumber_->sens_field_ref( sensVarTag );

            dfdv <<= cond( re == 0, 0.0)
                         ( sqrt(re) * pow(sc, 1.0/3.0) * (0.3/re * dRedv + 0.2/sc * dScdv));
        }
    };

} // namespace ReactorEnsembleUtil

#endif /* SHERWOODNUMBER_H_ */
