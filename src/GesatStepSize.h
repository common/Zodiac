/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   GesatStepSize.h
 *  \date   Sep 26, 2016
 *  \author mike
 */

#ifndef GESATSTEPSIZE_H_
#define GESATSTEPSIZE_H_

#include <expression/Expression.h>

template< typename FieldT >
class GesatStepSize : public Expr::Expression<FieldT>
{
public:

  struct Builder : public Expr::ExpressionBuilder
  {

    Builder( const Expr::Tag& dsTag,
             const Expr::Tag& expEigTag,
             const double safetyFactor  = 0.1,
             const double maxStepSize   = 1.0,
             const double maxRampFactor = 1.1 )
    : Expr::ExpressionBuilder( dsTag ),
      expEigTag_( expEigTag ),
      safetyFactor_ (safetyFactor),
      maxStepSize_  (maxStepSize),
      maxRampFactor_(maxRampFactor)
    {}

    ~Builder(){}
    Expr::ExpressionBase* build() const{
      return new GesatStepSize<FieldT>( expEigTag_, safetyFactor_, maxStepSize_, maxRampFactor_ );
    }

  private:
    const Expr::Tag expEigTag_;
    const double safetyFactor_;
    const double maxStepSize_;
    const double maxRampFactor_;
  };

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& ds = this->value();
    const FieldT& lambda = expEig_->field_ref();
    ds <<= min( safetyFactor_ / lambda, min( maxStepSize_, maxRampFactor_ * ds ) );
  }

  private:
  GesatStepSize( const Expr::Tag& expEigTag,
                 const double safetyFactor  = 0.1,
                 const double maxStepSize   = 1.0,
                 const double maxRampFactor = 1.1 )
  : Expr::Expression<FieldT>(),
    safetyFactor_ ( safetyFactor  ),
    maxStepSize_  ( maxStepSize   ),
    maxRampFactor_( maxRampFactor )
  {
    this->set_gpu_runnable(true);
    expEig_ = this->template create_field_request<FieldT>( expEigTag );
  }

  DECLARE_FIELD( FieldT, expEig_ )

  const double safetyFactor_;
  const double maxStepSize_;
  const double maxRampFactor_;
};

#endif /* GESATSTEPSIZE_H_ */
