#ifndef ZODIAC_RESTARTFIELDWRITER_H
#define ZODIAC_RESTARTFIELDWRITER_H

#include <expression/FieldWriter.h>
#include <expression/FieldManagerList.h>
#include "parser/ParseInputFile.h"

/**
 * @class RestartFileWriter
 * @author Hang Zhou
 *
 * @brief  Manage data read from and write to database
 */

class RestartFileWriter
{
  const std::string dbName_, restartFile_;
  const boost::filesystem::path rootPath_;
  Expr::FieldManagerList& fml_;
  std::map<Expr::Tag,int> consVarIdxMap_;
  std::map<std::string, std::string> varMap_;

public:
  /**
   * @param fml the FieldManagerList that holds all of the fields that will be considered for output.
   * @param dbName the name of the output database
   * @param restartFile the name of the data file: it should be "converged" or "iter+(restartStep)"
   * @param consVarIdxMap a map of tag of solved variables (first entry) and their equation index (second entry)
   * @param varMap a map of restart variable names (first entry) and their respective aliases (second entry)
   */
  explicit RestartFileWriter( Expr::FieldManagerList& fml,
                              const std::string dbName,
                              const std::string restartFile,
                              std::map<Expr::Tag,int> consVarIdxMap,
                              std::map<std::string, std::string> varMap);

  ~RestartFileWriter();


  /**
   * @brief set fields with the given names by loading values from the
   *        output database with the supplied name.
   *
   * Note that all fields should be registered in the FieldManagerList and requested for output
   * before read their data from database
   */
  void
  set_fields_from_file();

  /**
  * @brief used to generate S-Curve in combustion. Set reacted conditions as initial conditions.
  *       For example, databased includes data for all residence time.
  *       Then the values of all the resolved variables will be set to be the values for the biggest residence time
   *      (reacted value) and other variables, like residence time, are set to be the same as the list shown in the database.
  */
  void
  set_reacted_fields_from_file();
};




#endif //ZODIAC_RESTARTFIELDWRITER_H
