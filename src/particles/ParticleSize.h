/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   ParticleSize.h
 *  @par    Getting particle size
 *  @date   Jun 12, 2018
 *  @author Hang
 */

#ifndef PARTICLESIZE_H_
#define PARTICLESIZE_H_

#include <expression/Expression.h>

namespace Particles {

    /**
     * @class PartSize
     * @ingroup Particles
     * @brief Getting the particle size(diameter) from particle density, particle mass per gas mass and particle number per gas mass
     * @author Hang
     *
     * The particle size is
     * pow(massFracPart/rhoPart/numPart/(M_PI/6), 1.0/3.0)
     * \f[
     *    d_p = (\frac{m_p}{\rho_p N_p \pi/6})^{1/3}
     * \f]
     * Here, \f$m_p = Y_p m_g\f$ is particle mass.
     *       \f$N_p = n_p m_g\f$ is number of particles.
     *       \f$Y_p\f$ is the particle mass per gas mass.
     *       \f$n_p\f$ is the number of particles per gas mass.
     *       \\f$m_g\f$ is the mass of gas.
     *
     * Final expression becomes
     * \f[
     *    d_p = (\frac{Y_p m_g}{\rho_p n_p m_g \pi/6})^{1/3} = (\frac{Y_p}{\rho_p n_p \pi/6})^{1/3}
     * \f]
     */

    template<typename FieldT>
    class PartSize : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, partRho_, partMassFrac_, partNumFrac_ )

      PartSize( const Expr::Tag partRhoTag,
                const Expr::Tag partMassFracTag,
                const Expr::Tag partNumFracTag)
              : Expr::Expression<FieldT>(){
            this->set_gpu_runnable( true );
            partRho_     = this->template create_field_request<FieldT>( partRhoTag      );
            partMassFrac_= this->template create_field_request<FieldT>( partMassFracTag );
            partNumFrac_ = this->template create_field_request<FieldT>( partNumFracTag  );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag partRhoTag_, partMassFracTag_, partNumFracTag_;
        public:
          /**
           * @brief The mechanism for building a PartSize object
           * @tparam FieldT
           * @param partSizeTag Particle size/diameter
           * @param partRhoTag Particle density
           * @param partMassFracTag Particle mass fraction
           * @param partNumFracTag Number of particles per gas mass
           */
            Builder( const Expr::Tag partSizeTag,
                     const Expr::Tag partRhoTag,
                     const Expr::Tag partMassFracTag,
                     const Expr::Tag partNumFracTag)
                  : Expr::ExpressionBuilder( partSizeTag     ),
                    partRhoTag_            ( partRhoTag      ),
                    partMassFracTag_       ( partMassFracTag ),
                    partNumFracTag_        ( partNumFracTag  ){}

            ~Builder(){}
            Expr::ExpressionBase* build() const{
                return new PartSize<FieldT>( partRhoTag_, partMassFracTag_, partNumFracTag_ );
            }
        };


        void evaluate(){
            using namespace SpatialOps;
            const FieldT& rhoPart      = partRho_->field_ref();
            const FieldT& massFracPart = partMassFrac_->field_ref();
            const FieldT& numFracPart  = partNumFrac_->field_ref();
            this->value() <<= pow(massFracPart/rhoPart/numFracPart/(M_PI/6), 1.0/3.0);
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
            FieldT & dfdv = this->sensitivity_result( sensVarTag );
            const FieldT& rhoPart      = partRho_->field_ref();
            const FieldT& massFracPart = partMassFrac_->field_ref();
            const FieldT& numFracPart  = partNumFrac_->field_ref();
            const FieldT& drhoPartdv     = partRho_->sens_field_ref( sensVarTag );
            const FieldT& dmassFracPartdv= partMassFrac_->sens_field_ref( sensVarTag );
            const FieldT& dnumFracPartdv = partNumFrac_->sens_field_ref( sensVarTag );
            dfdv <<= 1.0/3.0/(M_PI/6)*pow(massFracPart/rhoPart/numFracPart/(M_PI/6), -2.0/3.0)
                     * (dmassFracPartdv/rhoPart/numFracPart
                     - massFracPart/square(rhoPart*numFracPart) * (drhoPartdv*numFracPart+ rhoPart*dnumFracPartdv));
        }
    };

} // namespace Particles

#endif /* PARTICLESIZE_H_ */
