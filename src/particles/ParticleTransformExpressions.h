/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
/**
 * @file ParticleTransformExpressions.h
 * @par  Getting the expressions added into the source term of gas phase equations
 * @author Hang
 */


#ifndef PARTICLETRANSFORMEXPRESSIONS_H_
#define PARTICLETRANSFORMEXPRESSIONS_H_

#include <expression/Expression.h>

namespace Particles {

    /**
     * @class PartToGasHeatCP
     * @ingroup Particles
     * @brief Summarizing the heat transfer from each particle to gas under constant pressure condition
     * @author Hang
     *
     * The heat transfer from particles to gas is given as
     * \f[
     *    Q_{cov,PtoG} = -\frac{\sum_i Q_{cov,p,i} \rho V Y_{p,i} cp_{p,i}}{\rho V} = -\sum_i Q_{cov,p,i} Y_{p,i} cp_{p.i}
     * \f]
     * Here, \f$ Q_{cov,p,i} \f$ is the convective heat transfer term for particles with ith size.
     *       \f$ Y_{p,i} \f$ is the mass fraction of particles with ith size.
     *       \f$ cp_{p,i} \f$ is the heat capacity of particles with ith size.
     *
     * The sensitivity is given as
     * \f[
     *    \frac{\partial Q_{cov,PtoG}}{\partial \phi} = -\sum_i (\frac{\partial Q_{cov,p,i}}{\partial \phi} Y_{p,i} cp_{p.i}
     *                                                           + Q_{cov,p,i} \frac{\partial Y_{p,i}}{\partial \phi} cp_{p.i}
     *                                                           + Q_{cov,p,i} Y_{p,i} \frac{\partial cp_{p.i}}{\partial \phi})
     * \f]
     *
     */
    template<typename FieldT>
    class PartToGasHeatCP: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleTerms_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, massFracParts_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, cpParts_      );
        const int nparsize_;

        PartToGasHeatCP( const Expr::TagList particleTermTags,
                         const Expr::TagList massFracPartTags,
                         const Expr::TagList cpPartTags)
              : Expr::Expression<FieldT>(),
                nparsize_(particleTermTags.size())
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleTermTags, particleTerms_);
          this->template create_field_vector_request<FieldT> ( massFracPartTags, massFracParts_);
          this->template create_field_vector_request<FieldT> ( cpPartTags, cpParts_);
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleTermTags_, massFracPartTags_, cpPartTags_;
        public:
            /**
             * @brief The mechanism for building a PartToGasHeatCP object
             * @tparam FieldT
             * @param partToGasHeatTag Convective heat transfer from particles to gas
             * @param particleTermTags List of convective heat transfer term between particle and gas in particle temperature equation
             * @param massFracPartTags List of particle mass fraction
             * @param cpPartTags List of particle specific heat capacity
             */
            Builder( const Expr::Tag partToGasHeatTag,
                     const Expr::TagList particleTermTags,
                     const Expr::TagList massFracPartTags,
                     const Expr::TagList cpPartTags)
                  : Expr::ExpressionBuilder( partToGasHeatTag ),
                    particleTermTags_      ( particleTermTags ),
                    massFracPartTags_      ( massFracPartTags ),
                    cpPartTags_            ( cpPartTags       ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new PartToGasHeatCP<FieldT>( particleTermTags_, massFracPartTags_, cpPartTags_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 0;
          for ( size_t i = 0; i < nparsize_; ++i){
            result <<= result - particleTerms_[i]->field_ref() * ( massFracParts_[i]->field_ref() * cpParts_[i]->field_ref());
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          FieldT& result = this->value();
          dfdv <<= 0;
          for ( size_t i = 0; i< nparsize_; ++i){
            const FieldT& particleTermi    = particleTerms_[i]->field_ref();
            const FieldT& massFracParti    = massFracParts_[i]->field_ref();
            const FieldT& cpParti          = cpParts_[i]->field_ref();
            const FieldT& dparticleTermdvi = particleTerms_[i]->sens_field_ref(sensVarTag);
            const FieldT& dmassFracPartdvi = massFracParts_[i]->sens_field_ref(sensVarTag);
            const FieldT& dcpPartdvi       = cpParts_[i]->sens_field_ref(sensVarTag);
            dfdv <<= dfdv - ( dparticleTermdvi * massFracParti * cpParti
                              + particleTermi * dmassFracPartdvi * cpParti
                              + particleTermi * massFracParti * dcpPartdvi);
          }
        }
    };

    /**
    * @class PartToGasHeatCV
    * @ingroup Particles
    * @brief Summarizing the heat transfer from each particle to gas under constant volume condition
    * @author Hang
    *
    * The heat transfer from particles to gas is given as
    * \f[
    *    Q_{cov,PtoG} = -\frac{\sum_i  Q_{cov,p,i} \rho V Y_{p,i} cp_{p,i}}{V} = -\sum_i  Q_{cov,p,i} \rho Y_{p,i} cp_{p,i}
    * \f]
    * Here, \f$ Q_{cov,p,i} \f$ is the convective heat transfer term for particles with ith size.
    *       \f$ Y_{p,i} \f$ is the mass fraction of particles with ith size.
    *       \f$ cp_{p,i} \f$ is the heat capacity of particles with ith size.
    *       \f$ \rho \f$ is the density of gas phase.
    *
    * The sensitivity is given as
    * \f[
    *    \frac{\partial Q_{cov,PtoG}}{\partial \phi} = -\sum_i (\frac{\partial Q_{cov,p,i}}{\partial \phi} \rho Y_{p,i} cp_{p,i}
    *                                                           + Q_{cov,p,i} \frac{\partial \rho}{\partial \phi} Y_{p,i} cp_{p,i}
    *                                                           + Q_{cov,p,i} \rho \frac{\partial Y_{p,i}}{\partial \phi} cp_{p,i}
    *                                                           + Q_{cov,p,i} \rho Y_{p,i} \frac{\partial cp_{p,i}}{\partial \phi})
    * \f]
    */
    template<typename FieldT>
    class PartToGasHeatCV: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleTerms_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, massFracParts_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, cpParts_      );
        DECLARE_FIELD(FieldT, rho_);
        const int nparsize_;

        PartToGasHeatCV( const Expr::TagList particleTermTags,
                         const Expr::TagList massFracPartTags,
                         const Expr::TagList cpPartTags,
                         const Expr::Tag rhoTag)
              : Expr::Expression<FieldT>(),
                nparsize_(particleTermTags.size())
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleTermTags, particleTerms_);
          this->template create_field_vector_request<FieldT> ( massFracPartTags, massFracParts_);
          this->template create_field_vector_request<FieldT> ( cpPartTags, cpParts_);
          rho_ = this->template create_field_request<FieldT>( rhoTag );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleTermTags_;
            const Expr::TagList massFracPartTags_, cpPartTags_;
            const Expr::Tag rhoTag_;
        public:
            /**
             * @brief The mechanism for building a PartToGasHeatCP object
             * @tparam FieldT
             * @param partToGasHeatTag Convective heat transfer from particles to gas
             * @param particleTermTags List of convective heat transfer term between particle and gas in particle temperature equation
             * @param massFracPartTags List of particle mass fraction
             * @param cpPartTags List of particle specific heat capacity
             * @param rhoTag Density of gas phase
             */
            Builder( const Expr::Tag partToGasHeatTag,
                     const Expr::TagList particleTermTags,
                     const Expr::TagList massFracPartTags,
                     const Expr::TagList cpPartTags,
                     const Expr::Tag rhoTag)
                  : Expr::ExpressionBuilder( partToGasHeatTag ),
                    particleTermTags_      ( particleTermTags ),
                    massFracPartTags_      ( massFracPartTags ),
                    cpPartTags_            ( cpPartTags       ),
                    rhoTag_                ( rhoTag           ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new PartToGasHeatCV<FieldT>( particleTermTags_, massFracPartTags_, cpPartTags_, rhoTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 0;
          for ( size_t i = 0; i < nparsize_; ++i){
            result <<= result - particleTerms_[i]->field_ref() * massFracParts_[i]->field_ref() * cpParts_[i]->field_ref() * rho_->field_ref();
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          FieldT& result = this->value();
          const FieldT& rho = rho_->field_ref();
          const FieldT& drhodv = rho_->sens_field_ref(sensVarTag);
          dfdv <<= 0;
          for ( size_t i = 0; i< nparsize_; ++i){
            const FieldT& particleTermi    = particleTerms_[i]->field_ref();
            const FieldT& massFracParti    = massFracParts_[i]->field_ref();
            const FieldT& cpParti          = cpParts_[i]->field_ref();
            const FieldT& dparticleTermdvi = particleTerms_[i]->sens_field_ref(sensVarTag);
            const FieldT& dmassFracPartdvi = massFracParts_[i]->sens_field_ref(sensVarTag);
            const FieldT& dcpPartdvi       = cpParts_[i]->sens_field_ref(sensVarTag);
            dfdv <<= dfdv - ( dparticleTermdvi * massFracParti * cpParti * rho
                              + particleTermi * dmassFracPartdvi * cpParti * rho
                              + particleTermi * massFracParti * dcpPartdvi * rho
                              + particleTermi * massFracParti * cpParti * drhodv);
          }
        }
    };
    /**
    * @class PartToGasCP
    * @ingroup Particles
    * @brief Getting the species/energy transfer rhs in the equation.
    *        It includes two part: one comes from the species/energy source term, one comes from the mass source term
    * @author Hang
    *
    * The transfer from particles to gas is given as
    * \f[
    *    M_{PtoG_RHS} = S_{horYi} - S_m \Phi
    * \f]
    * Here, \f$ S_{horYi} \f$ is the source term for species/energy transportation from all particles to gas. It has been divided by \f$\rho V\f$.
    *       \f$ S_m \f$ is the source term for gas phase mass from all particles to gas. It has been divided by \f$\rho V\f$.
    *       \f$ \Phi \f$ is the species mass fraction or enthalpy.
    *
    * The sensitivity is given as
    * \f[
    *      \frac{\partial M_{PtoG_RHS}}{\partial \phi} = \frac{\partial S_{horYi}}{\partial \phi}
    *                                                     -\frac{\partial S_m}{\partial \phi} \Phi - S_m \frac{\partial \Phi}{\partial \phi}
    * \f]
    */
    template<typename FieldT>
    class PartToGasCP: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, sourceTerm_, massSourceTerm_, gasTerm_);

        PartToGasCP( const Expr::Tag sourceTermTag,
                     const Expr::Tag massSourceTermTag,
                     const Expr::Tag gasTermTag)
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable( true );
          sourceTerm_     = this->template create_field_request<FieldT>( sourceTermTag     );
          massSourceTerm_ = this->template create_field_request<FieldT>( massSourceTermTag );
          gasTerm_        = this->template create_field_request<FieldT>( gasTermTag        );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag sourceTermTag_, massSourceTermTag_, gasTermTag_;
        public:
            /**
             * @brief The mechanism for building a PartToGasV object
             * @tparam FieldT
             * @param rhsTag rhs terms in species/energy equation from qll particles to gas
             * @param sourceTermTag source term for species/energy from all particles to gas
             * @param massSourceTermTag source term for mass from all particles to gas
             * @param gasTermTag Gas phase parameter: h or Y_i
             */
            Builder( const Expr::Tag rhsTag,
                     const Expr::Tag sourceTermTag,
                     const Expr::Tag massSourceTermTag,
                     const Expr::Tag gasTermTag)
                  : Expr::ExpressionBuilder( rhsTag            ),
                    sourceTermTag_         ( sourceTermTag     ),
                    massSourceTermTag_     ( massSourceTermTag ),
                    gasTermTag_            ( gasTermTag        ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new PartToGasCP<FieldT>( sourceTermTag_, massSourceTermTag_, gasTermTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          const FieldT& sourceTerm     = sourceTerm_    ->field_ref();
          const FieldT& massSourceTerm = massSourceTerm_->field_ref();
          const FieldT& gasTerm        = gasTerm_       ->field_ref();
          result <<= sourceTerm - massSourceTerm * gasTerm;
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& sourceTerm     = sourceTerm_    ->field_ref();
          const FieldT& massSourceTerm = massSourceTerm_->field_ref();
          const FieldT& gasTerm        = gasTerm_       ->field_ref();
          const FieldT& dsourceTermdv     = sourceTerm_    ->sens_field_ref( sensVarTag );
          const FieldT& dmassSourceTermdv = massSourceTerm_->sens_field_ref( sensVarTag );
          const FieldT& dgasTermdv        = gasTerm_       ->sens_field_ref( sensVarTag );
          dfdv<<= dsourceTermdv - dmassSourceTermdv * gasTerm - massSourceTerm * dgasTermdv;
        }
    };


    /**
    * @class PartToGasCV
    * @ingroup Particles
    * @brief Getting the mass/energy transfer rhs in the equation, which is equal to the source term divided by the reactor volume
    * @author Hang
    *
    * The transfer from particles to gas is given as
    * \f[
    *    M_{PtoG_RHS} = S \rho
    * \f]
    * Here, \f$ S \f$ is the source term for mass/energy transportation from all particles to gas. It has divided by \f$\rho V\f$.
    *       \f$ \rho \f$ is the density of the gas ohase.
    *
    * The sensitivity is given as
     * \f[
    *      \frac{\partial M_{PtoG_RHS}}{\partial \phi} = \frac{\partial S}{\partial \phi}\rho + S \frac{\partial \rho}{\partial \phi}
    * \f]
    */
    template<typename FieldT>
    class PartToGasCV: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, sourceTerm_, rho_);

        PartToGasCV( const Expr::Tag sourceTermTag,
                     const Expr::Tag rhoTag)
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable( true );
          sourceTerm_ = this->template create_field_request<FieldT>( sourceTermTag );
          rho_        = this->template create_field_request<FieldT>( rhoTag        );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag sourceTermTag_, rhoTag_;
        public:
            /**
             * @brief The mechanism for building a PartToGasV object
             * @tparam FieldT
             * @param rhsTag rhs terms in mass/energy equation from qll particles to gas
             * @param sourceTermTag source term for mass/energy from all particles to gas
             * @param rhoTag Density of gas phase
             */
            Builder( const Expr::Tag rhsTag,
                     const Expr::Tag sourceTermTag,
                     const Expr::Tag rhoTag)
                  : Expr::ExpressionBuilder( rhsTag        ),
                    sourceTermTag_         ( sourceTermTag ),
                    rhoTag_                ( rhoTag        ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new PartToGasCV<FieldT>( sourceTermTag_, rhoTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= sourceTerm_->field_ref() * rho_->field_ref();
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          dfdv<<= sourceTerm_->sens_field_ref(sensVarTag) * rho_->field_ref()
                  + sourceTerm_->field_ref() * rho_->sens_field_ref( sensVarTag);
        }
    };

    /**
     * @class PartSummation
     * @ingroup Particles
     * @brief Summarizing all the particle terms
     * @author Hang
     *
     * The sum of particle terms is given as
     * \f[
     *    S = \sum_{i=1}^{npar}  \Phi_i
     * \f]
     * Here, \f$ npar \f$ is the number of particles.
     *       \f$ \Phi_i \f$ is the term for ith particle.
     *
     * The sensitivity is given as
      * \f[
     *      \frac{\partial S}{\partial \phi} = \sum_{i=1}^{npar} \frac{\partial \Phi_i}{\partial \phi}
     * \f]
     */
    template<typename FieldT>
    class PartSummation: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleTerms_);
        const int nparsize_;

        PartSummation( const Expr::TagList particleTermTags)
              : Expr::Expression<FieldT>(),
                nparsize_ (particleTermTags.size())
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleTermTags, particleTerms_);
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleTermTags_;
        public:
            /**
             * @brief The mechasism for building a PartSummation object
             * @tparam FieldT
             * @param partSummationTag Sum of all particle terms
             * @param particleTermTags List of particle parameters
             */
            Builder( const Expr::Tag partSummationTag,
                     const Expr::TagList particleTermTags)
                  : Expr::ExpressionBuilder( partSummationTag ),
                    particleTermTags_      ( particleTermTags ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new PartSummation<FieldT>( particleTermTags_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 0;
          for ( size_t i = 0; i < nparsize_; ++i){
            result <<= result + particleTerms_[i]->field_ref();
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= 0;
          for ( size_t i = 0; i< nparsize_; ++i){
            dfdv <<= dfdv + particleTerms_[i]->sens_field_ref( sensVarTag );
          }
        }
    };

    /**
     * @class GasMassSourceFromPart
     * @ingroup Particles
     * @brief Getting the gas phase mass source term from particle
     * @author Hang
     *
     * The result is given as
     * \f[
     *    S = - \sum_{i=1}^{npar}\sum_{j=1}^{nspecies}  \Phi_{i,j}
     * \f]
     * Here, \f$ npar \f$ is the number of particles.
     *       \f$ \Phi_{i,j} \f$ is the term for jth species from ith particle
     *
     * The sensitivity is given as
     * \f[
     *      \frac{\partial S}{\partial \phi} = -\sum_{i=1}^{npar}\sum_{j=1}^{nspecies} \frac{\partial \Phi_{i,j}}{\partial \phi}
     * \f]
     */
    template<typename FieldT>
    class GasMassSourceFromPart: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleTerms_);
        const int nsize_;

        GasMassSourceFromPart( const Expr::TagList particleTermTags)
              : Expr::Expression<FieldT>(),
                nsize_(particleTermTags.size())
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleTermTags, particleTerms_);
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleTermTags_;
        public:
            /**
             * @brief The mechanism for building a GasMassSourceFromPart object
             * @tparam FieldT
             * @param resultTag Gas source term from particles
             * @param particleTermTags  List of particle parameters
             */
            Builder( const Expr::Tag resultTag,
                     const Expr::TagList particleTermTags)
                  : Expr::ExpressionBuilder( resultTag ),
                    particleTermTags_      ( particleTermTags){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new GasMassSourceFromPart<FieldT>( particleTermTags_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 0;
          for ( size_t i = 0; i < nsize_; ++i){
            result <<= result - particleTerms_[i]->field_ref();
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= 0;
          for ( size_t i = 0; i< nsize_; ++i){
            dfdv <<= dfdv - particleTerms_[i]->sens_field_ref( sensVarTag );
          }
        }
    };

    /**
     * @class SpeciesSourceFromPart
     * @ingroup Particles
     * @brief Getting the species mass fraction source term from particle
     * @author Hang
     *
     * For species \f$j\f$, the result is given as
     * \f[
     *    S_j = \sum_{i=1}^{npar} \Phi_{i,j}
     * \f]
     *
     * Its sensitivity is given as
     * \f[
     *      \frac{\partial S_j}{\partial \phi} = \sum_{i=1}^{npar}\frac{\partial \Phi_{i,j}}{\partial \phi}
     * \f]
     */
    template<typename FieldT>
    class SpeciesSourceFromPart: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleTerms_);
        const int nspecies_, nparsize_;

        SpeciesSourceFromPart( const Expr::TagList particleTermTags,
                               const int nspecies)
              : Expr::Expression<FieldT>(),
                nspecies_(nspecies),
                nparsize_(particleTermTags.size()/nspecies)
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleTermTags, particleTerms_);
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleTermTags_;
            const int nspecies_;
        public:
            /**
             * @brief The mechanism for building a SpeciesSourceFromPart vector
             * @tparam FieldT
             * @param resultTags Species source from particle to gas
             * @param particleTermTags List of particle parameters
             * @param nspecies number of species
             */
            Builder( const Expr::TagList resultTags,
                     const Expr::TagList particleTermTags,
                     const int nspecies)
                  : Expr::ExpressionBuilder( resultTags       ),
                    particleTermTags_      ( particleTermTags ),
                    nspecies_              ( nspecies         ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new SpeciesSourceFromPart<FieldT>( particleTermTags_, nspecies_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
          for ( size_t j = 0; j < nspecies_; ++j){
            FieldT& speciesj = *results[j];
            speciesj <<= 0.0;
            for ( size_t i = 0; i < nparsize_; ++i){
              speciesj <<= speciesj + particleTerms_[j+i*nspecies_]->field_ref();
            }
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          const Expr::TagList& targetTags = this->get_tags();
          std::vector<FieldT > dresultsdv;
          for(size_t j=0;j<nspecies_;++j){
            dresultsdv.push_back(this->sensitivity_result(targetTags[j], sensVarTag));
          }

          for ( size_t j = 0; j < nspecies_; ++j){
            FieldT& dspeciesjdv = dresultsdv[j];
            dspeciesjdv <<= 0.0;
            for ( size_t i = 0; i < nparsize_; ++i){
              dspeciesjdv <<= dspeciesjdv + particleTerms_[j+i*nspecies_]->sens_field_ref(sensVarTag);
            }
          }
        }
    };

    /**
     * @class SourceFromGasMassChange
     * @ingroup Particles
     * @brief Getting the source term related to the mass source term of gas phase
     * @author Hang
     *
     * The result is given as
     * \f[
     *    S_i = - S_{m,g}  \phi
     * \f]
     * Here, \f$ S_{m,g} \f$ is the source term for gas phase mass. It has been divided by \f$\rho V\f$.
     *       \f$ \phi \f$ is the calculated parameter.
     *       It could be gas phase enthalpy, species mass fraction, particle mass fraction, particle number fraction and coal composition mass fraction.
     *
     */
    template<typename FieldT>
    class SourceFromGasMassChange: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, gasMassSource_, phi_)

        SourceFromGasMassChange( const Expr::Tag& gasMassSourceTag,
                                 const Expr::Tag& phiTag)
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable( true );
          gasMassSource_ = this->template create_field_request<FieldT>( gasMassSourceTag );
          phi_           = this->template create_field_request<FieldT>( phiTag           );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag gasMassSourceTag_, phiTag_;
        public:
            /**
             * @brief The mechanism for building a SourceFromGasMassChange object
             * @tparam FieldT
             * @param resultTag Source term related to the mass source term of gas phase.
             * @param gasMassSourceTag Source term for gas phase mass. It has been divided by \f$\rho V\f$.
             * @param phiTag Calculated parameter.
             */
            Builder( const Expr::Tag& resultTag,
                     const Expr::Tag& gasMassSourceTag,
                     const Expr::Tag& phiTag)
                  : Expr::ExpressionBuilder( resultTag ),
                    gasMassSourceTag_      ( gasMassSourceTag ),
                    phiTag_                ( phiTag           ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new SourceFromGasMassChange<FieldT>( gasMassSourceTag_, phiTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= - gasMassSource_->field_ref() * phi_->field_ref();
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& gasMassSource = gasMassSource_->field_ref();
          const FieldT& phi = phi_->field_ref();
          const FieldT& dgasMassSourcedv = gasMassSource_->sens_field_ref( sensVarTag );
          const FieldT& dphidv = phi_->sens_field_ref( sensVarTag );
          dfdv <<= - (dgasMassSourcedv * phi+ gasMassSource * dphidv);
        }
    };

    /**
     * @class GasEnergySourceFromPart
     * @ingroup Particles
     * @brief Getting the gas phase energy source term from particle because of the speceis transportation
     * @author Hang
     *
     * The expression is given as
     * \f[
     *    S = - \sum_{i=1}^{npar}\sum_{j=1}^{nspecies}  S_{i,j} h_j
     * \f]
     *
     * Here, \f$ npar \f$ is the number of particles.
     *       \f$ nspecies \f$ is the number of species.
     *       \f$ S_{i,j} \f$ is the mass source term for jth species from ith particle.
     *       \f$ h_j \f$ is the enthalpy of jth species at ith particle temperature.
     *
     * The sensitivity is given as
     * \f[
     *    \frac{\partial S}{\partial \phi} = - \sum_{i=1}^{npar}\sum_{j=1}^{nspecies} ( \frac{\partial S_{i,j}}{\partial \phi}
     *                                                                                + \frac{\partial h_j}{\partial \phi} S_{i,j})
     * \f]
     *
     * NOTE: In the expression, negative sign is used. So, adding or substracting this source depends on the sign of S_{i,j}.
     *       Need to be careful about the sign.
     */
    template<typename FieldT>
    class GasEnergySourceFromPart: public Expr::Expression<FieldT> {
        DECLARE_VECTOR_OF_FIELDS(FieldT, particleMassTerms_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, enthSpeciesTerms_);

        GasEnergySourceFromPart( const Expr::TagList particleMassTermTags,
                                 const Expr::TagList enthSpeciesTermTags)
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable( true );
          this->template create_field_vector_request<FieldT> ( particleMassTermTags, particleMassTerms_);
          this->template create_field_vector_request<FieldT> ( enthSpeciesTermTags, enthSpeciesTerms_);
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::TagList particleMassTermTags_, enthSpeciesTermTags_;
        public:
            /**
             * @brief The mechansim for building a GasEnergySourceFromPart object
             * @tparam FieldT
             * @param resultTag Energy source term from particles because of the speceis transportation
             * @param particleMassTermTags List of species source terms from particle to gas (It size is equal to npar*nspecies)
             * @param enthSpeciesTermTags List of species enthalpies at particle temperature
             */
            Builder( const Expr::Tag resultTag,
                     const Expr::TagList particleMassTermTags,
                     const Expr::TagList enthSpeciesTermTags)
                  : Expr::ExpressionBuilder( resultTag ),
                    particleMassTermTags_  ( particleMassTermTags ),
                    enthSpeciesTermTags_   ( enthSpeciesTermTags  ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new GasEnergySourceFromPart<FieldT>( particleMassTermTags_, enthSpeciesTermTags_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 0;
          for ( size_t i = 0; i < particleMassTerms_.size(); ++i){
            result <<= result - particleMassTerms_[i]->field_ref() * enthSpeciesTerms_[i]->field_ref();
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= 0;
          for ( size_t i = 0; i< particleMassTerms_.size(); ++i){
            dfdv <<= dfdv
                     - particleMassTerms_[i]->sens_field_ref( sensVarTag ) * enthSpeciesTerms_[i]->field_ref()
                     - particleMassTerms_[i]->field_ref() * enthSpeciesTerms_[i]->sens_field_ref( sensVarTag );
          }
        }
    };

    /**
     * @class DensitySourceFromGasMassChange
     * @ingroup Particles
     * @brief Getting the source term for density equation related to the mass source term of gas phase for plug flow reactor
     * @author Hang
     *
     * The result is given as
     * \f[
     *    S_i = 2 \frac{S_{m,g}}{u}
     * \f]
     * Here, \f$ S_{m,g} \f$ is the source term for gas phase mass (kg/m^3/s).
     *       \f$ u \f$ is the velocity of gas phase.
     *
     */
    template<typename FieldT>
    class DensitySourceFromGasMassChange: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, gasMassSource_, u_, rho_)

        DensitySourceFromGasMassChange( const Expr::Tag& gasMassSourceTag,
                                        const Expr::Tag& rhoTag,
                                        const Expr::Tag& uTag)
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable( true );
          gasMassSource_ = this->template create_field_request<FieldT>( gasMassSourceTag );
          u_             = this->template create_field_request<FieldT>( uTag             );
          rho_           = this->template create_field_request<FieldT>( rhoTag           );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag gasMassSourceTag_, uTag_, rhoTag_;
        public:
            /**
             * @brief The mechanism for building a SourceFromGasMassChange object
             * @tparam FieldT
             * @param resultTag Source term related to the mass source term of gas phase.
             * @param gasMassSourceTag Source term for gas phase mass(1/s). It has been divided by \f$\rho\f$.
             * @param rhoTag Density of gas phase.
             * @param uTag Velocity of gas phase.
             */
            Builder( const Expr::Tag& resultTag,
                     const Expr::Tag& gasMassSourceTag,
                     const Expr::Tag& rhoTag,
                     const Expr::Tag& uTag)
                  : Expr::ExpressionBuilder( resultTag ),
                    gasMassSourceTag_      ( gasMassSourceTag ),
                    rhoTag_                ( rhoTag           ),
                    uTag_                  ( uTag             ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new DensitySourceFromGasMassChange<FieldT>( gasMassSourceTag_, rhoTag_, uTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= 2.0 * gasMassSource_->field_ref() * rho_->field_ref() / u_->field_ref();
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& gasMassSource = gasMassSource_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& u = u_->field_ref();
          const FieldT& dgasMassSourcedv = gasMassSource_->sens_field_ref( sensVarTag );
          const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
          const FieldT& dudv = u_->sens_field_ref( sensVarTag );
          dfdv <<= 2.0 * (dgasMassSourcedv*rho/u + gasMassSource*drhodv/u - gasMassSource*rho/square(u)*dudv);
        }
    };

    /**
    * @class DpDxSourceFromGasMassChange
    * @ingroup Particles
    * @brief Getting the source term for `Dp/Dx` term related to the mass source term of gas phase for plug flow reactor
    * @author Hang
    *
    * The result is given as
    * \f[
    *    S_{DpDx}} = \frac{(2 T -h/cp) S_{m,g} \rho}{u(\frac{Mw}{R}-\frac{T}{u^2}-\frac{1}{cp})}
    * \f]
    * Here, \f$ S_{m,g} \f$ is the source term for gas phase mass (kg/m^3/s).
    *       \f$ u \f$ is the velocity of gas phase.
    *      \f$ T \f$, \f$ h \f$ and \f$ cp \f$ are temperature, enthalpy and specific heat copacity of gas phase.
    *      \f$ Mw \f$ is the molecular weigh of gas mixture.
    *      \f$ R \f$ is the gas constant.
    *
    */
    template<typename FieldT>
    class DpDxSourceFromGasMassChange: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, gasMassSource_, u_, rho_, T_, enth_, cp_, mmw_)
        const double gasConstant_;

        DpDxSourceFromGasMassChange( const Expr::Tag& gasMassSourceTag,
                                     const Expr::Tag& rhoTag,
                                     const Expr::Tag& tempTag,
                                     const Expr::Tag& enthTag,
                                     const Expr::Tag& mmwTag,
                                     const Expr::Tag& cpTag,
                                     const Expr::Tag& uTag)
              : Expr::Expression<FieldT>(),
                gasConstant_(CanteraObjects::gas_constant())
        {
          this->set_gpu_runnable( true );
          gasMassSource_ = this->template create_field_request<FieldT>( gasMassSourceTag );
          rho_           = this->template create_field_request<FieldT>( rhoTag           );
          T_             = this->template create_field_request<FieldT>( tempTag          );
          enth_          = this->template create_field_request<FieldT>( enthTag          );
          mmw_           = this->template create_field_request<FieldT>( mmwTag           );
          cp_            = this->template create_field_request<FieldT>( cpTag            );
          u_             = this->template create_field_request<FieldT>( uTag             );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag gasMassSourceTag_, uTag_, rhoTag_, tempTag_, enthTag_, cpTag_, mmwTag_;
        public:
            /**
             * @brief The mechanism for building a SourceFromGasMassChange object
             * @tparam FieldT
             * @param resultTag Source term related to the mass source term of gas phase.
             * @param gasMassSourceTag Source term for gas phase mass(1/s). It has been divided by \f$\rho f$.
             * @param rhoTag Density of gas phase.
             * @param tempTag Temperature of gas phase.
             * @param enthTag Specific enthalpy of gas phase.
             * @param mmwTag Molecular weight of gas phase.
             * @param cpTag Heat capacity of gas phase.
             * @param uTag Velocity of gas phase.
             */
            Builder( const Expr::Tag& resultTag,
                     const Expr::Tag& gasMassSourceTag,
                     const Expr::Tag& rhoTag,
                     const Expr::Tag& tempTag,
                     const Expr::Tag& enthTag,
                     const Expr::Tag& mmwTag,
                     const Expr::Tag& cpTag,
                     const Expr::Tag& uTag)
                  : Expr::ExpressionBuilder( resultTag ),
                    gasMassSourceTag_      ( gasMassSourceTag ),
                    rhoTag_                ( rhoTag           ),
                    tempTag_               ( tempTag          ),
                    enthTag_               ( enthTag          ),
                    mmwTag_                ( mmwTag           ),
                    cpTag_                 ( cpTag            ),
                    uTag_                  ( uTag             ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new DpDxSourceFromGasMassChange<FieldT>( gasMassSourceTag_, rhoTag_, tempTag_, enthTag_, mmwTag_, cpTag_, uTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          const FieldT& gasMassSource = gasMassSource_->field_ref();
          const FieldT& rho           = rho_          ->field_ref();
          const FieldT& T             = T_            ->field_ref();
          const FieldT& enth          = enth_         ->field_ref();
          const FieldT& mmw           = mmw_          ->field_ref();
          const FieldT& cp            = cp_           ->field_ref();
          const FieldT& u             = u_            ->field_ref();
          result <<= (2.0*T-enth/cp)*gasMassSource*rho / u / (mmw/gasConstant_-T/square(u)-1/cp);
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& gasMassSource = gasMassSource_->field_ref();  const FieldT& dgasMassSourcedv = gasMassSource_->sens_field_ref( sensVarTag );
          const FieldT& rho           = rho_          ->field_ref();  const FieldT& drhodv           = rho_          ->sens_field_ref( sensVarTag );
          const FieldT& T             = T_            ->field_ref();  const FieldT& dTdv             = T_            ->sens_field_ref( sensVarTag );
          const FieldT& enth          = enth_         ->field_ref();  const FieldT& denthdv          = enth_         ->sens_field_ref( sensVarTag );
          const FieldT& mmw           = mmw_          ->field_ref();  const FieldT& dmmwdv           = mmw_          ->sens_field_ref( sensVarTag );
          const FieldT& cp            = cp_           ->field_ref();  const FieldT& dcpdv            = cp_           ->sens_field_ref( sensVarTag );
          const FieldT& u             = u_            ->field_ref();  const FieldT& dudv             = u_            ->sens_field_ref( sensVarTag );

          dfdv <<= ((2.0*dTdv-denthdv/cp+enth/square(cp)*dcpdv)*gasMassSource*rho + (2.0*T-enth/cp)*(dgasMassSourcedv*rho+gasMassSource*drhodv) ) / u / (mmw/gasConstant_-T/square(u)-1/cp)
                   - (2.0*T-enth/cp)*gasMassSource*rho / square(u) * dudv / (mmw/gasConstant_-T/square(u)-1/cp)
                   - (2.0*T-enth/cp)*gasMassSource*rho / u / square(mmw/gasConstant_-T/square(u)-1/cp) * (dmmwdv/gasConstant_-dTdv/square(u) + 2.0*T/cube(u)*dudv+1/square(cp)*dcpdv);
        }
    };

    /**
  * @class DpDxSourceFromGasEnthalpyChange
  * @ingroup Particles
  * @brief Getting the source term for `Dp/Dx` term related to the enthalpy source term of gas phase for plug flow reactor.
  * @author Hang
  *
  * The result is given as
  * \f[
  *    S_{DpDx}} = \frac{ S_{h,g}}{u cp(\frac{Mw}{R}-\frac{T}{u^2}-\frac{1}{cp})}
  * \f]
  * Here, \f$ S_{h,g} \f$ is the source term for gas phase enthalpy (J/m^3/s).
  *       So it needs to time \f$\rho\f$ to get the source term every unit volume.
  *       \f$ u \f$ is the velocity of gas phase.
  *      \f$ T \f$, \f$ h \f$ and \f$ cp \f$ are temperature, enthalpy and specific heat copacity of gas phase.
  *      \f$ Mw \f$ is the molecular weigh of gas mixture.
  *      \f$ R \f$ is the gas constant.
  *
  */
    template<typename FieldT>
    class DpDxSourceFromGasEnthalpyChange: public Expr::Expression<FieldT> {
        DECLARE_FIELDS(FieldT, gasEnthalpySource_, u_, rho_, T_, enth_, cp_, mmw_)
        const double gasConstant_;

        DpDxSourceFromGasEnthalpyChange( const Expr::Tag& gasEnthalpySourceTag,
                                         const Expr::Tag& rhoTag,
                                         const Expr::Tag& tempTag,
                                         const Expr::Tag& enthTag,
                                         const Expr::Tag& mmwTag,
                                         const Expr::Tag& cpTag,
                                         const Expr::Tag& uTag)
              : Expr::Expression<FieldT>(),
                gasConstant_(CanteraObjects::gas_constant())
        {
          this->set_gpu_runnable( true );
          gasEnthalpySource_ = this->template create_field_request<FieldT>( gasEnthalpySourceTag );
          rho_               = this->template create_field_request<FieldT>( rhoTag               );
          T_                 = this->template create_field_request<FieldT>( tempTag              );
          enth_              = this->template create_field_request<FieldT>( enthTag              );
          mmw_               = this->template create_field_request<FieldT>( mmwTag               );
          cp_                = this->template create_field_request<FieldT>( cpTag                );
          u_                 = this->template create_field_request<FieldT>( uTag                 );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag gasEnthalpySourceTag_, uTag_, rhoTag_, tempTag_, enthTag_, cpTag_, mmwTag_;
        public:
            /**
             * @brief The mechanism for building a SourceFromGasMassChange object
             * @tparam FieldT
             * @param resultTag Source term related to the mass source term of gas phase.
             * @param gasEnthalpySourceTag Source term for gas phase enthalpy (J/kg/s). It has been divided by \f$\rho \f$.
             * @param rhoTag Density of gas phase.
             * @param tempTag Temperature of gas phase.
             * @param enthTag Specific enthalpy of gas phase.
             * @param mmwTag Molecular weight of gas phase.
             * @param cpTag Heat capacity of gas phase.
             * @param uTag Velocity of gas phase.
             */
            Builder( const Expr::Tag& resultTag,
                     const Expr::Tag& gasEnthalpySourceTag,
                     const Expr::Tag& rhoTag,
                     const Expr::Tag& tempTag,
                     const Expr::Tag& enthTag,
                     const Expr::Tag& mmwTag,
                     const Expr::Tag& cpTag,
                     const Expr::Tag& uTag)
                  : Expr::ExpressionBuilder( resultTag ),
                    gasEnthalpySourceTag_ ( gasEnthalpySourceTag ),
                    rhoTag_               ( rhoTag               ),
                    tempTag_              ( tempTag              ),
                    enthTag_              ( enthTag              ),
                    mmwTag_               ( mmwTag               ),
                    cpTag_                ( cpTag                ),
                    uTag_                 ( uTag                 ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new DpDxSourceFromGasEnthalpyChange<FieldT>( gasEnthalpySourceTag_, rhoTag_, tempTag_, enthTag_, mmwTag_, cpTag_, uTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          const FieldT& gasEnthalpySource = gasEnthalpySource_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& T = T_->field_ref();
          const FieldT& enth = enth_->field_ref();
          const FieldT& mmw = mmw_->field_ref();
          const FieldT& cp = cp_->field_ref();
          const FieldT& u = u_->field_ref();
          result <<= gasEnthalpySource * rho / u / cp / (mmw/gasConstant_-T/square(u)-1/cp);
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& gasEnthalpySource = gasEnthalpySource_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& T = T_->field_ref();
          const FieldT& enth = enth_->field_ref();
          const FieldT& mmw = mmw_->field_ref();
          const FieldT& cp = cp_->field_ref();
          const FieldT& u = u_->field_ref();
          const FieldT& dgasEnthalpySourcedv = gasEnthalpySource_->sens_field_ref( sensVarTag );
          const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
          const FieldT& dTdv = T_->sens_field_ref( sensVarTag );
          const FieldT& denthdv = enth_->sens_field_ref( sensVarTag );
          const FieldT& dmmwdv = mmw_->sens_field_ref( sensVarTag );
          const FieldT& dcpdv = cp_->sens_field_ref( sensVarTag );
          const FieldT& dudv = u_->sens_field_ref( sensVarTag );
          dfdv <<= (dgasEnthalpySourcedv*rho+ gasEnthalpySource*drhodv) / u / cp / (mmw/gasConstant_-T/square(u)-1/cp)
                   - (gasEnthalpySource*rho/square(u)*dudv/cp + gasEnthalpySource*rho/u/square(cp)*dcpdv) / (mmw/gasConstant_-T/square(u)-1/cp)
                   - gasEnthalpySource*rho/u/cp / square(mmw/gasConstant_-T/square(u)-1/cp) * (dmmwdv/gasConstant_-dTdv/square(u) + 2.0*T/cube(u)*dudv+1/square(cp)*dcpdv);
        }
    };
}


#endif /* PARTICLETRANSFORMEXPRESSIONS_H_ */
