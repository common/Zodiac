
/**
 * @file ParticleDensity.h
 * @par Getting particle density
 * @author Hang
 */

#ifndef ParticleDensity_Expr_h
#define ParticleDensity_Expr_h

#include <expression/Expression.h>
#include <spatialops/particles/ParticleFieldTypes.h>

namespace Particles{

  /**
   *  @class ParticleDensity
   *  @ingroup Particle
   *  @brief Evaluates the particle density.
   *
   *  The particle density is given as
   *  \f[
   *     \rho_p=\frac{m_p}{N_p (\pi/6) d_p^3}
   *  \f]
   *
   *  Here, \f$ m_p \f$ is the total mass of particles with this size.
   *        \f$ N_p \f$ is the number of particles with this size.
   *        \f$ d_p \f$ is the size(diameter) of the particle.
   *
   *  In Zodiac, we solve \f$ Y_p \f$, mass fraction of particles per gas mass
   *  and \f$ n_p \f$, number of particles per gas mass.
   *  So, we have
   *  \f[
   *     m_p = \rho V Y_p
   *  \f]
   *  \f[
   *     N_p = \rho V n_p
   *  \f]
   *  Here, \f$ \rho \f$ is the gas density. \f$ V\f$ is the reactor volume.
   *  Then, the expression becomes
   *  \f[
   *     \rho_p=\frac{\rho V Y_p}{\rho V n_p (\pi/6) d_p^3} = \frac{Y_p}{n_p (\pi/6) d_p^3}
   *  \f]
   *
   *
   */

  template< typename FieldT >
  class ParticleDensity : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, pMassFrac_, pSize_, pNumFrac_ )

    ParticleDensity( const Expr::Tag& pMassFracTag,
                     const Expr::Tag& pSizeTag,
                     const Expr::Tag& pNumFracTag)
    : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      pMassFrac_ = this->template create_field_request<FieldT>( pMassFracTag );
      pSize_     = this->template create_field_request<FieldT>( pSizeTag     );
      pNumFrac_  = this->template create_field_request<FieldT>( pNumFracTag  );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag pMassFracTag_,pSizeTag_, pNumFracTag_;
    public:
      /**
       * @brief The mechanisum for building a ParticleDensity object
       * @tparam FieldT
       * @param pDensityTag particle density
       * @param pMassFracTag mass fraction of particles per gas mass
       * @param pSizeTag particle size(diameter)
       * @param pNumFracTag number of particles per gas mass
       */
      Builder( const Expr::Tag& pDensityTag,
               const Expr::Tag& pMassFracTag,
               const Expr::Tag& pSizeTag,
               const Expr::Tag& pNumFracTag)
            : ExpressionBuilder(pDensityTag),
              pMassFracTag_( pMassFracTag ),
              pSizeTag_( pSizeTag ),
              pNumFracTag_( pNumFracTag )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new ParticleDensity(pMassFracTag_,pSizeTag_, pNumFracTag_);
      }
    };

    ~ParticleDensity(){}

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();
      const FieldT& pMassFrac = pMassFrac_->field_ref();
      const FieldT& pSize = pSize_->field_ref();
      const FieldT& pNumFrac = pNumFrac_->field_ref();
      result <<= pMassFrac / (M_PI/6*pSize*pSize*pSize) / pNumFrac;
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      FieldT & dfdv = this->sensitivity_result( sensVarTag );
      const FieldT& pMassFrac = pMassFrac_->field_ref();
      const FieldT& pSize = pSize_->field_ref();
      const FieldT& pNumFrac = pNumFrac_->field_ref();
      const FieldT& dpMassFracdv = pMassFrac_->sens_field_ref(sensVarTag);
      const FieldT& dpSizedv = pSize_->sens_field_ref(sensVarTag);
      const FieldT& dpNumFracdv = pNumFrac_->sens_field_ref(sensVarTag);
      dfdv <<= pMassFrac / (M_PI/6*pSize*pSize*pSize) / pNumFrac
               * (dpMassFracdv/pMassFrac - dpNumFracdv/pNumFrac - 3*dpSizedv/pSize);
    }
  };

} // Particle

#endif // ParticleDensity_Expr_h
