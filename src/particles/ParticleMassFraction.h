/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   ParticleMassFraction.h
 *  @par    Getting the particle mass fraction per gas mass: (kg particle)/(kg gas)
 *  @date   Jun 12, 2018
 *  @author Hang
 */

#ifndef PARTICLEMASSFRACTION_H_
#define PARTICLEMASSFRACTION_H_

#include <expression/Expression.h>

namespace Particles {

    /**
     * @class PartMassFraction
     * @ingroup Particles
     * @brief Getting the particle mass fraction per gas mass
     * @author Hang
     *
     * The particle mass fraction per gas mass is given as
     * \f[
     *    Y_{p,j} = \frac{(\pi/6) \rho_{p,j} d_{p,j}^3 N_{p,j}}{m_g}
     * \f]
     *
     * Here, \f$j\f$ is the jth particle size.
     *      (For every particle size, one group of equations, including particle mass fraction, particle temperature and particle number fraction).
     *      \f$ m_g \f$ is the mass of gas phase in the reactor.
     *      \f$\rho_{p,j}\f$ is the density of particls for jth particle size.
     *      \f$\d_{p,j}\f$ is the size of particls for jth particle size.
     *      \f$N_{p,j}\f$ is the number of particls for jth particle size.
     *  In Zodiac, we solve number of particle per gas mass, \f$ n_{p,j}\f$.
     *  So, we have
     *  \f[
     *     N_{p,j} = m_g n_{p,j}
     *  \f]
     *  Here,
     *  Then, the final expression is
     *  \f[
     *     Y_{p,j} = \frac{(\pi/6) \rho_{p,j} d_{p,j}^3 m_g n_{p,j}}{m_g} = (\pi/6) \rho_{p,j} d_{p,j}^3 n_{p,j}
     *  \f]
     *
     */
    template<typename FieldT>
    class PartMassFraction : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, partRho_, partSize_, partNumFrac_);

      PartMassFraction( const Expr::Tag partRhoTag,
                        const Expr::Tag partSizeTag,
                        const Expr::Tag partNumFracTag)
                      : Expr::Expression<FieldT>(){
                    this->set_gpu_runnable( true );
                    partRho_      = this->template create_field_request<FieldT>( partRhoTag     );
                    partSize_     = this->template create_field_request<FieldT>( partSizeTag    );
                    partNumFrac_  = this->template create_field_request<FieldT>( partNumFracTag );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag partRhoTag_, partSizeTag_, partNumFracTag_;
        public:
          /**
           * @brief The mechanism for building a PartMass object
           * @tparam FieldT
           * @param partMassFracTag Particle mass fraction in the reactor
           * @param partRhoTag Particle density
           * @param partSizeTag Particle size/diameter
           * @param npar Number of particles per mass of gas
           */
            Builder( const Expr::Tag partMassFracTag,
                     const Expr::Tag partRhoTag,
                     const Expr::Tag partSizeTag,
                     const Expr::Tag partNumFracTag)
                  : Expr::ExpressionBuilder( partMassFracTag  ),
                    partRhoTag_            ( partRhoTag       ),
                    partSizeTag_           ( partSizeTag      ),
                    partNumFracTag_        ( partNumFracTag   ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
                return new PartMassFraction<FieldT>( partRhoTag_, partSizeTag_, partNumFracTag_ );
            }
        };

        void evaluate(){
            using namespace SpatialOps;
            const FieldT& sizeP = partSize_->field_ref();
            const FieldT& rhoP  = partRho_->field_ref();
            const FieldT& numP  = partNumFrac_->field_ref();
            this->value() <<= M_PI/6 * rhoP * sizeP*sizeP*sizeP * numP;
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
            FieldT & dfdv = this->sensitivity_result( sensVarTag );
            const FieldT& rhoP  = partRho_->field_ref();
            const FieldT& sizeP = partSize_->field_ref();
            const FieldT& numP  = partNumFrac_->field_ref();
            const FieldT& drhoPdv = partRho_->sens_field_ref( sensVarTag );
            const FieldT& dsizePdv= partSize_->sens_field_ref( sensVarTag );
            const FieldT& dnumPdv  = partNumFrac_->sens_field_ref( sensVarTag );
            dfdv <<= M_PI/6 * (drhoPdv* sizeP*sizeP*sizeP * numP + rhoP*3*square(sizeP)*dsizePdv*numP + rhoP*sizeP*sizeP*sizeP*dnumPdv);
        }
    };

} // namespace Particles

#endif /* PARTICLEMASSFRACTION_H_ */
