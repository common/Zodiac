/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the nuSoftware is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>

#include <pokitt/CanteraObjects.h>
#include <pokitt/thermo/Enthalpy.h>

#include <fstream>

#include <expression/Functions.h>
#include <expression/ExpressionFactory.h>

#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "ZeroDimMixing.h"
#include "SumOp.h"

#include "particles/ParticleInterface.h"
#include "particles/ParticleTransformExpressions.h"
#include "VaporizationInterface.h"
#include "VaporizationHeatPart.h"
#include "VaporizationMass.h"

#include "particles/coal/CoalData.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

using Expr::matrix::sensitivity;

namespace Vap {

    typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
    typedef pokitt::SpeciesEnthalpy<FieldT>::Builder SpeciesEnthalpyT;
    typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivideT;

    typedef Particles::PartToGasCV<FieldT>::Builder PartToGasCVT;
    typedef Particles::GasMassSourceFromPart<FieldT>::Builder GasMassSourceT;
    typedef Particles::GasEnergySourceFromPart<FieldT>::Builder GasEnergySourceT;
    typedef Particles::DpDxSourceFromGasEnthalpyChange<FieldT>::Builder DpDxEnthalpyT;

    typedef Vap::VaporizationMass<FieldT>::Builder VapMassT;
    typedef Vap::VaporizationHeatPart<FieldT>::Builder VapHeatPartT;


    //---------------------------------------------------------------------------

    VapTags::
    VapTags( const Expr::Context& state,
             const std::vector<std::string>& particleNumArray)
          : vapMassGasSourceTag       ( Expr::Tag( "vap_mass_gas_source"     , state ) ),
            vapHeatGasSourceTag       ( Expr::Tag( "vap_heat_gas_source"     , state ) ),
            vapMassGasSourcePFRTag    ( Expr::Tag( "vap_mass_gas_source_pfr" , state ) ),
            vapHeatGasSourcePFRTag    ( Expr::Tag( "vap_heat_gas_source_pfr" , state ) ),
            vapMassToGasTag           ( Expr::Tag( "vap_mass_to_gas"         , state ) ),
            vapHeatToGasTag           ( Expr::Tag( "vap_heat_to_gas"         , state ) ),
            vapHeatDpDxTag            ( Expr::Tag( "vap_heat_dpdx"           , state ) )
    {
      vapRateTags        = Expr::tag_list( particleNumArray, state, "vap_rate_"          );
      vapH2OEnthalpyTags = Expr::tag_list( particleNumArray, state, "vap_h2o_enthalpy_"  );
      vapHeatPartTags    = Expr::tag_list( particleNumArray, state, "vap_heat_part_"     );
    }

    //---------------------------------------------------------------------------
    Vaporization::
    Vaporization(Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                 const YAML::Node& rootParser,
                 const std::vector<std::string> particleNumArray,
                 int& EquIndex)
          :integrator_(integrator),
           rootParser_(rootParser),
           particleNumArray_(particleNumArray),
           equIndex_(EquIndex)
    {
      build_equation_system_particle_vap();
    }

    //---------------destructor---------------------------
    Vaporization::~Vaporization()
    {
      std::cout << "Delete ~Vaporization(): " << this << std::endl;
    }

    //---------------------------------------------------------------------------
    void Vaporization::build_equation_system_particle_vap(){

      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const Vap::VapTags tagsEVap( Expr::STATE_NONE, particleNumArray_ );
      const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
      const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

      Expr::ExpressionFactory& execFactory = integrator_->factory();
      const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();
      const int IdxH2O = CanteraObjects::species_index( "H2O" );
      const int nparsize = particleNumArray_.size();

      for( size_t i = 0;i < nparsize;i++ ){

        execFactory.register_expression( new SpeciesEnthalpyT( tagsEVap.vapH2OEnthalpyTags[i], tagsEPart.partTempTags[i], IdxH2O ));

        if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
          execFactory.register_expression( new VapMassT( tagsEVap.vapRateTags[i], tagsE.tempTag, tagsE.presTag, tagsE.mmwTag, tagsEPart.partTempTags[i],
                                                         tagsEPart.partSizeTags[i], tagsEPart.partNumFracTags[i], tagsEPart.partMassFracTags[i], tagsE.massTags[IdxH2O],
                                                         tagsEPart.shNumberTags[i], tagsEPart.partMassFracInitialTags[i])); //It has been divided by rho*V
        }
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Coal"){
          execFactory.register_expression( new VapMassT( tagsEVap.vapRateTags[i], tagsE.tempTag, tagsE.presTag, tagsE.mmwTag, tagsEPart.partTempTags[i],
                                                         tagsEPart.partSizeTags[i], tagsEPart.partNumFracTags[i], tagsECoal.moistureMassFracTags[i], tagsE.massTags[IdxH2O],
                                                         tagsEPart.shNumberTags[i], tagsECoal.moistureMassFracInitialTags[i]));

          execFactory.attach_dependency_to_expression(tagsEVap.vapRateTags[i], tagsECoal.moistureMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
        }
        // modify particle temperature equations
        execFactory.register_expression( new VapHeatPartT( tagsEVap.vapHeatPartTags[i], tagsEVap.vapRateTags[i], tagsEPart.partTempTags[i],
                                                           tagsEPart.partMassFracTags[i], tagsEPart.partCpTags[i] ));
        execFactory.attach_dependency_to_expression( tagsEVap.vapHeatPartTags[i], tagsEPart.partTempKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
        // modify particle mass fraction equations
        execFactory.attach_dependency_to_expression( tagsEVap.vapRateTags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
      }

      execFactory.register_expression( new GasMassSourceT( tagsEVap.vapMassGasSourceTag, tagsEVap.vapRateTags)); //positive
      execFactory.register_expression( new GasEnergySourceT( tagsEVap.vapHeatGasSourceTag, tagsEVap.vapRateTags, tagsEVap.vapH2OEnthalpyTags));
      execFactory.attach_dependency_to_expression( tagsEVap.vapMassGasSourceTag, tagsEPart.partToGasMassTag, Expr::ADD_SOURCE_EXPRESSION);

      //modify gas equation
      if(reactorType == "PSRConstantVolume"){
        execFactory.register_expression( new PartToGasCVT( tagsEVap.vapMassToGasTag, tagsEVap.vapMassGasSourceTag, tagsE.rhoTag ) );
        execFactory.register_expression( new PartToGasCVT( tagsEVap.vapHeatToGasTag, tagsEVap.vapHeatGasSourceTag, tagsE.rhoTag ) );
        execFactory.attach_dependency_to_expression( tagsEVap.vapMassToGasTag, tagsE.rhoKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsEVap.vapHeatToGasTag, tagsE.rhoEgyKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsEVap.vapMassToGasTag, tagsE.rhoYKinRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
      }
      if(reactorType == "PSRConstantPressure"){
        execFactory.attach_dependency_to_expression( tagsEVap.vapHeatGasSourceTag, tagsE.enthKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsEVap.vapMassGasSourceTag, tagsE.yKinRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
      }
      if(reactorType == "PFR"){
        execFactory.register_expression( new DivideT( tagsEVap.vapMassGasSourcePFRTag, tagsEVap.vapMassGasSourceTag, tagsEpfr.uTag ));
        execFactory.register_expression( new DivideT( tagsEVap.vapHeatGasSourcePFRTag, tagsEVap.vapHeatGasSourceTag, tagsEpfr.uTag ));
        execFactory.register_expression( new DpDxEnthalpyT( tagsEVap.vapHeatDpDxTag, tagsEVap.vapHeatGasSourceTag, tagsE.rhoTag, tagsE.tempTag,
                                                            tagsE.enthTag, tagsE.mmwTag, tagsE.cpTag, tagsEpfr.uTag));
        execFactory.attach_dependency_to_expression( tagsEVap.vapHeatGasSourcePFRTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsEVap.vapMassGasSourcePFRTag, tagsE.yFullRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsEVap.vapHeatDpDxTag, tagsEpfr.dpdxTag, Expr::ADD_SOURCE_EXPRESSION );
      }
    }

} // namespace Vap
