#ifndef VAPORIZATIONBase_h
#define VAPORIZATIONBase_h

#include <string>

/**
 *  \file VaporizationBase.h
 *
 */

namespace Vap{

  enum VapModel{
    ON,
    OFF,
    INVALID_VAPMODEL
  };

  /**
   * @fn std::string vap_model_name( const VapModel model )
   * @param model the vaporization model
   * @return the string name of the model
   */
  std::string vap_model_name( const VapModel model );

  /**
   * @fn VapModel vap_model( const std::string& modelName )
   * @param modelName the string name of the model
   * @return the corresponding enum value
   */
  VapModel vap_model( const std::string& modelName );

} // namespace Vap

#endif // VAPORIZATION_h
