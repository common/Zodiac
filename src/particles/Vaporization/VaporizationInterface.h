/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   VaporizationInterface.h
 *  \date   Jun 26, 2018
 *  \author hang
 */

#ifndef VAPORIZATION_INTERFACE_UTIL_H_
#define VAPORIZATION_INTERFACE_UTIL_H_

#include <expression/ExprLib.h>
#include <expression/Functions.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>
#include <yaml-cpp/yaml.h>

namespace Vap {
   /**
    * @class VapTags
    * @ingroup Vap
    * @brief Getting tags used in vaporization model
    * @author Hang Zhou
    */
    class VapTags {
    public:
        const Expr::Tag vapMassGasSourceTag,    ///< gas phase mass source term from vaporization (only one species: H2O)
                        vapHeatGasSourceTag,    ///< gas phase energy source term from vaporization
                        vapMassGasSourcePFRTag, ///< gas phase mass source term from vaporization (only one species: H2O) for plug flow reactor
                        vapHeatGasSourcePFRTag, ///< gas phase energy source term from vaporization for plug flow reactor
                        vapMassToGasTag,        ///< rhs in gas phase mass equation from vaporization
                        vapHeatToGasTag,        ///< rhs in gas phase energy equation from vaporization
                        vapHeatDpDxTag;         ///< enthalpy source term in `Dp/Dx` from vaporization for plug flow reactor
        Expr::TagList   vapRateTags,            ///< vaporization rate
                        vapH2OEnthalpyTags,     ///< enthalpy of H2O at particle temperature
                        vapHeatPartTags;        ///< heat transfer term in particle temperature equation from vaporization

        VapTags( const Expr::Context& state,
                 const std::vector<std::string>& particleNumArray);
    };
   /**
    *  @ingroup Vap
    *  @class Vaporization
    *  @brief Provides an interface to the vaporization model
    */
    class Vaporization{

        Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator_;
        const YAML::Node& rootParser_;
        const std::vector<std::string> particleNumArray_;
        int& equIndex_;

    public:
      /**
       * @param integrator time integrator used in Zodiac
       * @param rootParser yaml node read from input file
       * @param particleNumArray list of particle numbers with different sizes
       * @param equIndex index of equation
       */
        Vaporization( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                      const YAML::Node& rootParser,
                      const std::vector<std::string> particleNumArray,
                      int& equIndex );

        ~Vaporization();

    private:
    /**
     * @brief Register all expressions required to implement the vaporzation model
     */
        void build_equation_system_particle_vap();
    };

} // namespace Vap

#endif /* VAPORIZATION_INTERFACE_UTIL_H_ */
