#include "VaporizationBase.h"

#include <stdexcept>
#include <sstream>

namespace Vap{

  std::string vap_model_name( const VapModel model )
  {
    std::string name;
    switch (model){
      case ON              : name="ON"     ; break;
      case OFF             : name="OFF"    ; break;
      case INVALID_VAPMODEL: name="INVALID"; break;
    }
    return name;
  }

  VapModel vap_model( const std::string& name )
  {
    if     ( name == vap_model_name( ON  ) ) return ON;
    else if( name == vap_model_name( OFF ) ) return OFF;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported vaporization model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << vap_model_name( ON )
          << "\n\t" << vap_model_name( OFF)
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_VAPMODEL;
  }
}
