/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   VaporizationHeatPart.h
 *  @date   Jun 12, 2018
 *  @author hang
 */

#ifndef PARTICLEVAPORIZATIONHEATPART_H_
#define PARTICLEVAPORIZATIONHEATPART_H_

#include <expression/Expression.h>
#include <pokitt/thermo/Enthalpy.h>

namespace Vap {

    /**
     * @class VaporizationHeatPart
     * @ingroup Vap
     * @brief Getting the source term from vaporization in particle temperature equation (unit: K/s)
     * @author Hang
     *
     * The source term from vaporization is given as
     * \f[
     *    S_{p,T}^{vap} = \frac{S_{p,H2O}^{vap} \lambda_{Evap}}{Y_p c_{p,p}}
     * \f]
     * \f$ S_{p,H2O}^{vap}\f$ is the source term of the vaporization rate in particle mass fraction equation.
     * \f$ \lambda_{Evap} \f$ is the latent heat of vaporization.
     * (ref: K. M. Watson. Thermodynamics of the liquid state. Ind. Eng. Chem., 35(4):398-406, 1943.)
     * \f[
     *    \lambda_{Evap} = \lambda_{ref} \left( \frac{1-T_{p,r}}{1-T_{ref,r}} \right)^{0.38}
     * \f]
     * \f$ T_{p,r} = T_p / Tc \f$ is the reduced particle temperature.
     * \f$ T_{ref,r} = T_{ref} / Tc \f$ is the reduced reference temperature.
     * Here, the reference temperature is \f$ 300 K \f$. Latent heat at reference temperature is \f$ 2438 kJ/kg \f$.
     * \f$ Tc \f$ is 647.096 K.
     *
     * The sensitivity of this source term is given as
     * \f[
     *    \frac{\partial S_{p,T}^{vap} }{\partial \phi} = \frac{S_{p,H2O}^{vap} \lambda_{Evap}}{Y_p c_{p,p}}
     *                                                    (\frac{\frac{\partial S_{p,H2O}^{vap}}{\partial \phi}}{S_{p,H2O}^{vap}}
     *                                                     - \frac{0.38}{Tc - T_p}\frac{\partial T_p}{\partial \phi}
     *                                                     - \frac{\frac{\partial Y_p}{\partial \phi}}{Y_p}
     *                                                     - \frac{\frac{\partial c_{p,p}}{\partial \phi}}{c_{p,p}})
     * \f]
     *
     * NOTE: 1. This expression calculates the source term from all particles with ONE specific particle size.
     *          All the parameters used here are the value for ONE specific particle size.
     *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
     */

    template<typename FieldT>
    class VaporizationHeatPart : public Expr::Expression<FieldT> {
        DECLARE_FIELDS( FieldT, vapMassRhs_, tempPart_, massFracPart_, cpPart_ )
        const double Tcrit_;

        VaporizationHeatPart( const Expr::Tag vapMassRhsTag,
                              const Expr::Tag partTempTag,
                              const Expr::Tag partMassFracTag,
                              const Expr::Tag partCpTag)
              : Expr::Expression<FieldT>(),
                 Tcrit_(647.3) // 647.096 used in ODT
        {
          this->set_gpu_runnable( true );
          vapMassRhs_   = this->template create_field_request<FieldT>( vapMassRhsTag   );
          tempPart_     = this->template create_field_request<FieldT>( partTempTag     );
          massFracPart_ = this->template create_field_request<FieldT>( partMassFracTag );
          cpPart_       = this->template create_field_request<FieldT>( partCpTag       );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag vapMassRhsTag_, tempPartTag_, massFracPartTag_, cpPartTag_;
        public:
          /**
           * @brief The mechanism for building a VaporizationHeatPart object
           * @tparam FieldT
           * @param vapHeatPartTag Source term from vaporization in particle temperature equation
           * @param vapMassRhsTag Rate of vaporization of water in particle
           * @param partTempTag Particle temperature
           * @param partMassFracTag Particle mass fraction
           * @param partCpTag Particle specific heat capacity
           */
            Builder( const Expr::Tag vapHeatPartTag,
                     const Expr::Tag vapMassRhsTag,
                     const Expr::Tag partTempTag,
                     const Expr::Tag partMassFracTag,
                     const Expr::Tag partCpTag)
                  : Expr::ExpressionBuilder( vapHeatPartTag ),
                    vapMassRhsTag_         ( vapMassRhsTag  ),
                    tempPartTag_           ( partTempTag    ),
                    massFracPartTag_       ( partMassFracTag),
                    cpPartTag_             ( partCpTag      ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new VaporizationHeatPart<FieldT>( vapMassRhsTag_, tempPartTag_, massFracPartTag_, cpPartTag_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();

          const FieldT& TPart        = tempPart_     ->field_ref();
          const FieldT& vapMassRhs   = vapMassRhs_   ->field_ref();
          const FieldT& massFracPart = massFracPart_ ->field_ref();
          const FieldT& cpPart       = cpPart_       ->field_ref();

          // vapMassRhs has been divided by rho*V
          result <<= cond(TPart >= Tcrit_, 0.0)
                         ( vapMassRhs * ( 2438e3 * pow((1-TPart/Tcrit_)/(1-300/Tcrit_), 0.38 ) ) / (massFracPart * cpPart ));
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& TPart           = tempPart_     ->field_ref();
          const FieldT& vapMassRhs      = vapMassRhs_   ->field_ref();
          const FieldT& massFracPart    = massFracPart_ ->field_ref();
          const FieldT& cpPart          = cpPart_       ->field_ref();
          const FieldT& dvapMassRhsdv   = vapMassRhs_  ->sens_field_ref( sensVarTag );
          const FieldT& dTPartdv        = tempPart_    ->sens_field_ref( sensVarTag );
          const FieldT& dmassFracPartdv = massFracPart_->sens_field_ref( sensVarTag );
          const FieldT& dcpPartdv       = cpPart_      ->sens_field_ref( sensVarTag );

          dfdv <<= cond(TPart >= Tcrit_, 0.0)
                (( 2438e3 * pow((1-TPart/Tcrit_)/(1-300/Tcrit_), 0.38 ) ) / (massFracPart * cpPart )
                   * ( dvapMassRhsdv
                       - vapMassRhs * 0.38 / (Tcrit_-TPart) * dTPartdv
                       - vapMassRhs * (dmassFracPartdv / massFracPart + dcpPartdv / cpPart)
                   ));
        }
    };

} // namespace Vap

#endif /* PARTICLEEVAPORATIONHEATTRANS_H_ */
