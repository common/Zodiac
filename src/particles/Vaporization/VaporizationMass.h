/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   VaporizationMass.h
 *  @date   Jun 12, 2018
 *  @author Hang
 */

#ifndef PARTICLEVAPORIZATIONMASS_H_
#define PARTICLEVAPORIZATIONMASS_H_

#include <expression/Expression.h>

namespace Vap {

  /**
   * @classs VaporizationMass
   * @ingroup Vap
   * @brief Getting the rate of vaporization for particle mass equation (negative)
   * @author Hang
   *
   * The vaporization rate is given as (positive)
   * \f[
   *    \frac{dm_{H2O}}{dt} = k_v\left(\frac{p_{H2O,sat}}{RT_p} - \frac{p_{H2O}}{RT}\right)A_p M_{w,H2O}
   * \f]
   *
   * Here, \f$k_v\f$ is the mass transfer coefficient of vapor.
   * \f[
   *    k_v = \frac{Sh D_{H2O,gas}}{d_p}
   * \f]
   * \f[
   *    Sh = 2.0 + 0.6 Re_p^{1/2} Sc^{1/3}
   * \f]
   * \f[
   *    D_{H2O,gas} = -2.775 \times 10^{-6} + 4.479 \times 10^{-8} T + 1.656 \times 10^{-10} T^2
   * \f]
   * \f$ p_{H2O,sat} \f$ is the saturation pressure of water at particle temperature.
   * \f[
   *     p_{H2O,sat} = 611.21 exp\left[ \left(18.678 - \frac{T_p-273.15}{234.5}\right) \frac{T_p - 273.15}{257.14+(T_p-273.15)} \right]
   * \f]
   * \f$p_{H2O}=pX_{H2O}=p\frac{Y_{H2O}M_w}{M_{w,H2O}}\f$ is the partial pressure of H2O in gas phase.
   * \f$ A_p \f$ is the surface area of particle. \f$ M_{w,H2O} \f$ is the molecular weight of H2O.
   * \f$ M_w \f$ is the molecular weight og gas mixture.
   *
   * The source term calculated in this class is given as
   * \f[
   *    S_{p,H2O}^{vap} = - \frac{dm_{H2O}}{dt}
   * \f]
   *
   * If the moisture mass in the particle is negative, it means that all the moisture goes into the reactor is vaporized. This source term is set to be:
   * \f[
   *   \frac{dm_{H2O}}{dt} = 0
   * \f]
   *
   *
   * The sensitivity of the source term if the moisture mass in the particle is positive is given as
   * \f[
   *    \frac{\partial S_{p,H2O}^{vap}}{\partial \phi} = \frac{\partial k_v}{\partial \phi} \left(\frac{P_{H2O,sat}}{RT_p} - \frac{p_{H2O}}{RT}\right)A_p M_{w,H2O}
   *                                                     + k_v (\frac{\partial (\frac{P_{H2O,sat}}{RT_p})}{\partial \phi } - \frac{\partial (\frac{p_{H2O}}{RT}) }{\partial \phi })A_p M_{w,H2O}
   *                                                     + k_v \left(\frac{P_{H2O,sat}}{RT_p} - \frac{p_{H2O}}{RT}\right) M_{w,H2O} \frac{\partial A_p}{\partial \phi}
   * \f]
   *
   *
   * To make the code converge better, here, a function
   * \f[
   *    F = max(0, 1-exp(-(1/a)*m_{H2O}))
   * \f]
   * is used. By multiplying \f$1/a\f$, the slope near \f$m_{H2O} = 0\f$ is not so big like the step function.
   *
   * When \f$m_{H2O} < 0\f$, \f$F=0\f$. When \f$m_{H2O} > 0\f$, \f$F=1\f$.
   * To confirm this function work well for all particle sizes, $a = m_{H2O,0}*1e4$ is used.
   * (notice: The solver could help the moisture mass to converge to zero. Therefore, there is no need to clip the mass to be zero in `VaporizationInterface.cpp`).
   *
   */
  template<typename FieldT>
  class VaporizationMass : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, temp_ , p_, mmw_, tempPart_, partSize_, numFracPart_)
      DECLARE_FIELDS( FieldT, moistureFrac_, massFractionH2O_, shNumber_, moistureFracIn_)
      const double mmwH2O_, R_, Tcrit_ ;

      VaporizationMass( const Expr::Tag tempTag,
                        const Expr::Tag presTag,
                        const Expr::Tag mmwTag,
                        const Expr::Tag tempPartTag,
                        const Expr::Tag partSizeTag,
                        const Expr::Tag partNumFracTag,
                        const Expr::Tag moistureFracTag,
                        const Expr::Tag massFracH2OTag,
                        const Expr::Tag shNumberTag,
                        const Expr::Tag moistureFracInflowTag)
            : Expr::Expression<FieldT>(),
              mmwH2O_(CanteraObjects::molecular_weights()[CanteraObjects::species_index("H2O")]),
              R_(CanteraObjects::gas_constant()/1000.0),
              Tcrit_(647.3)
      {
          this->set_gpu_runnable( true );
          temp_            = this->template create_field_request<FieldT>( tempTag        );
          p_               = this->template create_field_request<FieldT>( presTag        );
          mmw_             = this->template create_field_request<FieldT>( mmwTag         );
          tempPart_        = this->template create_field_request<FieldT>( tempPartTag    );
          partSize_        = this->template create_field_request<FieldT>( partSizeTag    );
          numFracPart_     = this->template create_field_request<FieldT>( partNumFracTag );
          moistureFrac_    = this->template create_field_request<FieldT>( moistureFracTag);
          massFractionH2O_ = this->template create_field_request<FieldT>( massFracH2OTag );
          shNumber_        = this->template create_field_request<FieldT>( shNumberTag    );
          moistureFracIn_  = this->template create_field_request<FieldT>( moistureFracInflowTag );
      }

  public:

      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag tempTag_, presTag_, mmwTag_, tempPartTag_, partSizeTag_, numFracPartTag_,
                          moistureFracTag_, massFractionH2OTag_, shNumberTag_, moistureFracInTag_;
      public:
        /**
         * @brief The mechanism for building a VaporizationMass object
         * @tparam FieldT
         * @param vapMassTag Rate of vaporization of water in particle
         * @param tempTag Gas phase temperature
         * @param presTag Gas phase pressure
         * @param mmwTag Molecular weight of gas mixture
         * @param tempPartTag Particle temperature
         * @param partSizeTag Particle size/diameter
         * @param partNumFracTag Number of particles per gas mass
         * @param moistureFracTag Moisture mass fraction
         * @param massH2OTag Mass fraction of H2O in gas phase
         * @param shNumberTag Sherwood number
         * @param moistureFracInflowTag Moisture mass fraction inflowing
         */
          Builder( const Expr::Tag vapMassTag,
                   const Expr::Tag tempTag,
                   const Expr::Tag presTag,
                   const Expr::Tag mmwTag,
                   const Expr::Tag tempPartTag,
                   const Expr::Tag partSizeTag,
                   const Expr::Tag partNumFracTag,
                   const Expr::Tag moistureFracTag,
                   const Expr::Tag massH2OTag,
                   const Expr::Tag shNumberTag,
                   const Expr::Tag moistureFracInflowTag)
                : Expr::ExpressionBuilder( vapMassTag     ),
                  tempTag_            ( tempTag           ),
                  presTag_            ( presTag           ),
                  mmwTag_             ( mmwTag            ),
                  tempPartTag_        ( tempPartTag       ),
                  partSizeTag_        ( partSizeTag       ),
                  numFracPartTag_     ( partNumFracTag    ),
                  moistureFracTag_    ( moistureFracTag   ),
                  massFractionH2OTag_ ( massH2OTag        ),
                  shNumberTag_        ( shNumberTag       ),
                  moistureFracInTag_  ( moistureFracInflowTag){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
              return new VaporizationMass<FieldT>( tempTag_, presTag_, mmwTag_, tempPartTag_, partSizeTag_, numFracPartTag_,
                                                   moistureFracTag_, massFractionH2OTag_, shNumberTag_, moistureFracInTag_);
          }
      };

      void evaluate(){
        using namespace SpatialOps;

        FieldT& result = this->value();

        const FieldT& T = temp_->field_ref();
        const FieldT& p = p_->field_ref();
        const FieldT& mmw = mmw_->field_ref();
        const FieldT& TPart = tempPart_->field_ref();
        const FieldT& partSize = partSize_->field_ref();
        const FieldT& partNumFrac = numFracPart_->field_ref();
        const FieldT& moistureFrac = moistureFrac_->field_ref();
        const FieldT& YH2O = massFractionH2O_->field_ref();
        const FieldT& shNumber = shNumber_->field_ref();
        const FieldT& moistureFracIn = moistureFracIn_->field_ref();

        SpatFldPtr<FieldT> ptemp = SpatialFieldStore::get<FieldT,FieldT>( result );
        SpatFldPtr<FieldT> weight = SpatialFieldStore::get<FieldT,FieldT>( result );
        SpatFldPtr<FieldT> condition = SpatialFieldStore::get<FieldT>( result );
        SpatFldPtr<FieldT> a = SpatialFieldStore::get<FieldT>( result );

        *a <<= cond(moistureFracIn == 0.0, 1e-10)
                   (                   moistureFracIn*1e3);

        *condition <<= max(0, 1-exp(-(1/ *a)*(Tcrit_-TPart)));
        *ptemp <<= *condition * TPart + (1- *condition) * Tcrit_;
        *weight <<= max(0, 1-exp(-(1/ *a)*moistureFrac));

        this->value() <<= *weight * min( 0.0,
                                         - (-2.775e-6 + 4.479e-8 * T + 1.656e-10 * square(T) ) * shNumber / partSize
                                           * ( 6.1121e2 * exp((18.678 - (*ptemp-273.15)/234.5) * (*ptemp-273.15) / (257.14+(*ptemp-273.15))) / R_ / *ptemp
                                             -YH2O * mmw * p / (mmwH2O_*R_*T) )
                                           * M_PI * square(partSize) * partNumFrac
                                           * mmwH2O_ / 1000.0); // negative always

      }

      void sensitivity( const Expr::Tag& sensVarTag ){

        using namespace SpatialOps;
        FieldT & dfdv = this->sensitivity_result( sensVarTag );

        const FieldT& T = temp_->field_ref();
        const FieldT& TPart = tempPart_->field_ref();
        const FieldT& p = p_->field_ref();
        const FieldT& mmw = mmw_->field_ref();
        const FieldT& YH2O = massFractionH2O_->field_ref();
        const FieldT& partSize = partSize_->field_ref();
        const FieldT& partNumFrac = numFracPart_->field_ref();
        const FieldT& shNumber = shNumber_->field_ref();
        const FieldT& moistureFrac = moistureFrac_->field_ref();
        const FieldT& moistureFracIn = moistureFracIn_->field_ref();
        const FieldT& dTdv = temp_->sens_field_ref(sensVarTag);
        const FieldT& dpdv = p_->sens_field_ref(sensVarTag);
        const FieldT& dmmwdv = mmw_->sens_field_ref(sensVarTag);
        const FieldT& dTPartdv = tempPart_->sens_field_ref(sensVarTag);
        const FieldT& dYH2Odv = massFractionH2O_->sens_field_ref(sensVarTag);
        const FieldT& dsizePdv = partSize_->sens_field_ref( sensVarTag );
        const FieldT& dnumFracPdv = numFracPart_->sens_field_ref( sensVarTag );
        const FieldT& dshNumberdv = shNumber_->sens_field_ref( sensVarTag );
        const FieldT& dmoistureFracdv = moistureFrac_->sens_field_ref( sensVarTag );

        FieldT& value = this->value();

        SpatFldPtr<FieldT> ptemp = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> condition = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> dconditiondv = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> dptempdv = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> a = SpatialFieldStore::get<FieldT>( value );

        *a <<= cond(moistureFracIn == 0.0, 1e-10)
                 (                   moistureFracIn*1e3);

        *condition <<= max(0, 1-exp(-(1/ *a)*(Tcrit_-TPart)));
        *dconditiondv <<= cond( Tcrit_<=TPart, 0.0)
                              (            - (1/ *a) * exp(-(1/ *a)*(Tcrit_-TPart)) * dTPartdv);
        *ptemp <<= *condition * TPart + (1- *condition) * Tcrit_;

        *dptempdv <<= *condition * dTPartdv + *dconditiondv * TPart - *dconditiondv * Tcrit_;

        SpatFldPtr<FieldT> diffusivity = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> ddiffudv    = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> psat        = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> dpsatdv     = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> ph2o        = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> dph2odv     = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> rate        = SpatialFieldStore::get<FieldT>( value );
        SpatFldPtr<FieldT> dratedv     = SpatialFieldStore::get<FieldT>( value );

        *diffusivity <<= - (-2.775e-6 + 4.479e-8 * T + 1.656e-10 * square(T) ) * shNumber / partSize;
        *ddiffudv    <<= - ( 4.479e-8 + 2 * 1.656e-10 * T ) * dTdv * shNumber / partSize
                         -  (-2.775e-6 + 4.479e-8 * T + 1.656e-10 * square(T) ) * dshNumberdv / partSize
                         + (-2.775e-6 + 4.479e-8 * T + 1.656e-10 * square(T) ) * shNumber / square(partSize) * partSize_->sens_field_ref(sensVarTag);
        *psat        <<= 6.1121e2 * exp((18.678 - (*ptemp-273.15)/234.5) * (*ptemp-273.15) / (257.14+(*ptemp-273.15))) / R_ / *ptemp;
        *dpsatdv     <<= 6.1121e2 * exp((18.678 - (*ptemp-273.15)/234.5) * (*ptemp-273.15) / (257.14+(*ptemp-273.15)))
                         *( (-1/234.5 * (*ptemp-273.15) / (257.14+(*ptemp-273.15)) + (18.678 - (*ptemp-273.15)/234.5) * 257.14 / square(257.14+(*ptemp-273.15))) * *ptemp -1.0)
                         * *dptempdv / R_ / square(*ptemp);
        *ph2o        <<= YH2O * mmw * p / (mmwH2O_ * R_ * T);
        *dph2odv     <<= (dYH2Odv * mmw * p + YH2O * dmmwdv * p + YH2O * mmw * dpdv) / (mmwH2O_ * R_ * T) - YH2O * mmw * p / (mmwH2O_ * R_ * square(T)) * dTdv;

        *rate        <<= - (-2.775e-6 + 4.479e-8 * T + 1.656e-10 * square(T) ) * shNumber / partSize
                            * ( 6.1121e2 * exp((18.678 - (*ptemp-273.15)/234.5) * (*ptemp-273.15) / (257.14+(*ptemp-273.15))) / R_ / *ptemp
                                  -YH2O * mmw * p / (mmwH2O_*R_*T) )
                            * M_PI * square(partSize) * partNumFrac
                            * mmwH2O_ / 1000.0;

		    *dratedv     <<= cond(*rate >= 0.0, 0.0)
		                         ( ( *ddiffudv * ( *psat - *ph2o) + *diffusivity * ( *dpsatdv - *dph2odv ) )
		                              * M_PI * square(partSize) * partNumFrac * mmwH2O_ / 1000.0
                              + *diffusivity * (*psat - - *ph2o) * M_PI *
                                (2 * partSize * dsizePdv * partNumFrac + square(partSize) * dnumFracPdv) * mmwH2O_ / 1000.0 );

        SpatFldPtr<FieldT> weight = SpatialFieldStore::get<FieldT,FieldT>( value );
        SpatFldPtr<FieldT> dweightdv = SpatialFieldStore::get<FieldT,FieldT>( value );
        *weight <<= max(0, 1-exp(-(1/ *a)*moistureFrac));
        *dweightdv <<= cond( moistureFrac<=0.0, 0.0)
                           (                (1/ *a) * exp(-(1/ *a)*moistureFrac) * dmoistureFracdv);

        dfdv <<= *weight * *dratedv+ *dweightdv * *rate;
    }
  };

} // namespace Vap

#endif /* PARTICLEVAPORIZATIONMASS_H_ */
