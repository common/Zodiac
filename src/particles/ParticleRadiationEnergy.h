/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   ParticleRadiationEnergy.h
 *  @par    Getting the radiation heat transfer term between particle and the walls or gas phase in particle temperature equation
 *  @date   Jun 12, 2018
 *  @author Hang
 */

#ifndef PARTICLERADIATIONENERGY_H_
#define PARTICLERADIATIONENERGY_H_

#include <expression/Expression.h>

namespace Particles {

  /**
   * @class RadiationPart
   * @ingroup Particles
   * @brief Getting the radiation heat transfer term between particle and the walls in particle temperature equation
   * @author Hang
   *
   * The radiation heat transfer term is given as
   * \f[
   *    Q_{p, rad} = \frac{A_{p,total} \varepsilon \sigma (T_{rad}^4 - T_p^4) }{\rho V Y_p cp_p}
   * \f]
   * Here, \f$ A_{p,total} = (n_p \rho V) \pi d_p^2 \f$ is the total surface area of all particles with this size.
   *       \f$ n_p \f$ is the number of particle per gas mass.
   *       \f$ \varepsilon \f$ is the emissivity.
   *       \f$ \sigma \f$ is Stefan-Boltzmann constant: \f$ 5.67e-8 kg/(s^3 \cdot K^4) \f$.
   *       \f$ T_{rad} \f$ is the radiation temperature.
   *
   */
  template<typename FieldT>
  class RadiationPart : public Expr::Expression<FieldT> {
    const double emissivityPart_;
    DECLARE_FIELDS( FieldT, tempPart_, tempRadia_, sizePart_, massFracPart_, cpPart_, numPart_)

    RadiationPart( const Expr::Tag partTempTag,
                   const Expr::Tag tempRadiaTag,
                   const Expr::Tag partSizeTag,
                   const Expr::Tag massFracPartTag,
                   const Expr::Tag cpPartTag,
                   const Expr::Tag numPartTag,
                   const double emissivityPart)
          : Expr::Expression<FieldT>(),
            emissivityPart_ ( emissivityPart ){
        this->set_gpu_runnable( true );
        tempPart_     = this->template create_field_request<FieldT>( partTempTag     );
        tempRadia_    = this->template create_field_request<FieldT>( tempRadiaTag    );
        sizePart_     = this->template create_field_request<FieldT>( partSizeTag     );
        massFracPart_ = this->template create_field_request<FieldT>( massFracPartTag );
        cpPart_       = this->template create_field_request<FieldT>( cpPartTag       );
        numPart_      = this->template create_field_request<FieldT>( numPartTag      );
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag tempPartTag_, tempRadiaTag_, sizePartTag_, massFracPartTag_, cpPartTag_, numPartTag_;
      const double emissivityPart_;
    public:
      /**
       * @brief The mechanism for building a RadiationPartWall object
       * @tparam FieldT
       * @param partRadiaTag Radiation heat transfer term between particle and the walls or gas phase in particle temperature equation
       * @param partTempTag Particle temperature
       * @param tempRadiaTag Radiation temperature
       * @param partSizeTag Particle size/diameter
       * @param massFracPartTag Particle mass per gas mass
       * @param cpPartTag Particle specific heat capacity
       * @param numPartTag number of particle per gas mass
       * @param emissivityPart Emissivity of particle
       */
      Builder( const Expr::Tag partRadiaTag,
               const Expr::Tag partTempTag,
               const Expr::Tag tempRadiaTag,
               const Expr::Tag partSizeTag,
               const Expr::Tag massFracPartTag,
               const Expr::Tag cpPartTag,
               const Expr::Tag numPartTag,
               const double emissivityPart)
            : Expr::ExpressionBuilder( partRadiaTag ),
              tempPartTag_    ( partTempTag     ),
              tempRadiaTag_   ( tempRadiaTag    ),
              sizePartTag_    ( partSizeTag     ),
              massFracPartTag_( massFracPartTag ),
              cpPartTag_      ( cpPartTag       ),
              numPartTag_     ( numPartTag      ),
              emissivityPart_ ( emissivityPart  ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
          return new RadiationPart<FieldT>( tempPartTag_, tempRadiaTag_, sizePartTag_, massFracPartTag_,
                                            cpPartTag_, numPartTag_, emissivityPart_ );
      }
    };

    void evaluate(){
        using namespace SpatialOps;
        const FieldT& tempPart     = tempPart_->field_ref();
        const FieldT& tempRadia    = tempRadia_->field_ref();
        const FieldT& sizePart     = sizePart_->field_ref();
        const FieldT& massFracPart = massFracPart_->field_ref();
        const FieldT& cpPart       = cpPart_->field_ref();
        const FieldT& numPart      = numPart_->field_ref();
        // numPart is number fraction of particles per gas mass. It has been divided by rho*V
        this->value() <<= M_PI * numPart * square(sizePart) * 5.67e-8 * emissivityPart_
                        * (square(tempRadia)*square(tempRadia) - square(tempPart) * square(tempPart))
                         / (massFracPart * cpPart);
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
        using namespace SpatialOps;
        FieldT & dfdv = this->sensitivity_result( sensVarTag );
        const FieldT& tempPart     = tempPart_->field_ref();
        const FieldT& tempRadia    = tempRadia_->field_ref();
        const FieldT& sizePart     = sizePart_->field_ref();
        const FieldT& massFracPart = massFracPart_->field_ref();
        const FieldT& cpPart       = cpPart_->field_ref();
        const FieldT& numPart      = numPart_->field_ref();
        const FieldT& dTPartdv     = tempPart_->sens_field_ref(sensVarTag);
        const FieldT& dTRadiadv    = tempRadia_->sens_field_ref(sensVarTag);
        const FieldT& dsizedv      = sizePart_->sens_field_ref(sensVarTag);
        const FieldT& dmassFracPdv = massFracPart_->sens_field_ref(sensVarTag);
        const FieldT& dcpPdv       = cpPart_->sens_field_ref(sensVarTag);
        const FieldT& dnumPdv      = numPart_->sens_field_ref(sensVarTag);

        dfdv <<= M_PI * 5.67e-8 * emissivityPart_
                * ( ((dnumPdv*square(sizePart)+numPart*2*dsizedv*sizePart)*(square(tempRadia)*square(tempRadia)-square(tempPart)*square(tempPart))
                    +numPart*square(sizePart)*4*(square(tempRadia)*tempRadia*dTRadiadv - square(tempPart)*tempPart*dTPartdv))/(massFracPart*cpPart)
                   - numPart*square(sizePart)*(square(tempRadia)*square(tempRadia)-square(tempPart)*square(tempPart))/square(massFracPart*cpPart)
                    * (dmassFracPdv/massFracPart+dcpPdv/cpPart)
                  );
    }
  };

} // namespace Particles

#endif /* PARTICLERADIATIONENERGY_H_ */
