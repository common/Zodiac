/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#include <pokitt/CanteraObjects.h>
#include <pokitt/transport/ThermalCondMix.h>
#include <pokitt/transport/ViscosityMix.h>
#include <pokitt/SpecificToVolumetric.h>
#include <pokitt/VolumetricToSpecific.h>
#include <pokitt/thermo/HeatCapacity_Cp.h>

#include <fstream>
#include <boost/random.hpp>

#include <expression/Functions.h>
#include <expression/ExpressionFactory.h>
#include <expression/ClipValue.h>

#include "FieldTypes.h"
#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "ZeroDimMixing.h"
#include "SumOp.h"
#include "NusseltNumber.h"
#include "SherwoodNumber.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

#include "ParticleInterface.h"
#include "ParticleMassFraction.h"
#include "ParticleSize.h"
#include "ParticleDensity.h"
#include "ParticleConvectionEnergy.h"
#include "ParticleRadiationEnergy.h"
#include "ParticleTransformExpressions.h"
#include "Vaporization/VaporizationInterface.h"
#include "coal/CoalInterface.h"
#include "coal/Devolatilization/DevolatilizationInterface.h"
#include "coal/CharCombustion/CharInterface.h"
#include "particles/ParticleTempMixing.h"

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >( #name );

using Expr::matrix::sensitivity;

namespace Particles {

    typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
    typedef Expr::LinearFunction<FieldT>::Builder LinearT;
    typedef Expr::PlaceHolder<FieldT>::Builder PlaceHolderT;

    typedef pokitt::ThermalConductivity<FieldT>::Builder ThermalConducT;
    typedef pokitt::Viscosity<FieldT>::Builder ViscosityT;
    typedef pokitt::HeatCapacity_Cp<FieldT>::Builder HeatCpT;

    typedef ReactorEnsembleUtil::SumOp<FieldT>::Builder SumOpT;
    typedef ReactorEnsembleUtil::ZeroDMixingCP<FieldT>::Builder ZeroDMixingCPT;
    typedef ReactorEnsembleUtil::NusseltNumber<FieldT>::Builder NuNumberT;
    typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivideT;
    typedef ReactorEnsembleUtil::ProductExpression<FieldT>::Builder ProductT;
    typedef ReactorEnsembleUtil::SherwoodNumber<FieldT>::Builder ShNumberT;

    typedef Particles::PartMassFraction<FieldT>::Builder MassFracPartT;
    typedef Particles::PartSize<FieldT>::Builder SizePartT;
    typedef Particles::ParticleDensity<FieldT>::Builder RhoPartT;
    typedef Particles::ConvectionPart<FieldT>::Builder ConvectionPartT;
    typedef Particles::RadiationPart<FieldT>::Builder RadiationPartT;
    typedef Particles::PartToGasHeatCP<FieldT>::Builder PartToGasHeatCPT;
    typedef Particles::PartToGasHeatCV<FieldT>::Builder PartToGasHeatCVT;
    typedef Particles::SourceFromGasMassChange<FieldT>::Builder SourceFromGasMassT;
    typedef Particles::ParticleTempMixing<FieldT>::Builder PartTempMixT;
    typedef Particles::DensitySourceFromGasMassChange<FieldT>::Builder DensitySourceFromGasMassT;
    typedef Particles::DpDxSourceFromGasMassChange<FieldT>::Builder DpDxSourceFromGasMassT;


    //---------------------------------------------------------------------------

    ParticleEnsembleTags::
    ParticleEnsembleTags( const Expr::Context& state,
                          const std::vector<std::string>& particleNumArray)
          : partTempInitTag       ( Expr::Tag( "T_part_init"               , state )),
            partTempInflowTag     ( Expr::Tag( "T_part_inflow"             , state )),
            partRhoInitTag        ( Expr::Tag( "rho_part_init"             , state )),
            partRhoInflowTag      ( Expr::Tag( "rho_part_inflow"           , state )),
            partCpInitTag         ( Expr::Tag( "cp_part_init"              , state )),
            partConvecToGasTag    ( Expr::Tag( "convection_part_to_gas"    , state )),
            partRadiaToGasTag     ( Expr::Tag( "radiation_part_to_gas    " , state )),
            partConvecToGasPFRTag ( Expr::Tag( "convection_part_to_gas_pfr", state )),
            partRadiaToGasPFRTag  ( Expr::Tag( "radiation_part_to_gas_pfr" , state )),
            tempRadiaWallTag      ( Expr::Tag( "T_radia_wall"              , state )),
            partToGasMassTag      ( Expr::Tag( "part_to_gas_mass"          , state )),
            gasEnthSrcTag         ( Expr::Tag( "gas_enth_src"              , state )),
            gasEnthSrcPFRTag      ( Expr::Tag( "gas_enth_src_pfr"          , state )),
            gasDensitySrcPFRTag   ( Expr::Tag( "gas_density_src_pfr"       , state )),
            gasDpDxSrcPFRTag      ( Expr::Tag( "gas_dpdx_src_pfr"          , state ))
    {
      massEachPartTags        = Expr::tag_list( particleNumArray, state, "mass_each_part_"             );
      partNumPerVolumeTags    = Expr::tag_list( particleNumArray, state, "num_part_per_volume_"        );
      partNumFracTags         = Expr::tag_list( particleNumArray, state, "num_frac_part_"              );
      partTempTags            = Expr::tag_list( particleNumArray, state, "temp_part_"                  );
      partCpTags              = Expr::tag_list( particleNumArray, state, "cp_part_"                    );
      partMassFracTags        = Expr::tag_list( particleNumArray, state, "mass_frac_part_"             );
      partRhoTags             = Expr::tag_list( particleNumArray, state, "rho_part_"                   );
      partSizeTags            = Expr::tag_list( particleNumArray, state, "size_part_"                  );
      partConvecTags          = Expr::tag_list( particleNumArray, state, "convection_part_"            );
      partGasRadiaTags        = Expr::tag_list( particleNumArray, state, "radiation_part_gas_"         );
      partWallRadiaTags       = Expr::tag_list( particleNumArray, state, "radiation_part_wall_"        );
      partTempInflowTags      = Expr::tag_list( particleNumArray, state, "temp_part_", "_inflow"       );
      partTempMixRhsTags      = Expr::tag_list( particleNumArray, state, "temp_part_", "_mix_rhs"      );
      partTempKinRhsTags      = Expr::tag_list( particleNumArray, state, "temp_part_", "_kin_rhs"      );
      partTempHeatRhsTags     = Expr::tag_list( particleNumArray, state, "temp_part_", "_heat_rhs"     );
      partTempKinRhsPFRTags   = Expr::tag_list( particleNumArray, state, "temp_part_", "_kin_rhs_pfr"  );
      partTempHeatRhsPFRTags  = Expr::tag_list( particleNumArray, state, "temp_part_", "_heat_rhs_pfr" );
      partTempFullRhsTags     = Expr::tag_list( particleNumArray, state, "temp_part_", "_full_rhs"     );
      partMassFracInitialTags = Expr::tag_list( particleNumArray, state, "mass_frac_part_", "_initial" );
      partMassFracInflowTags  = Expr::tag_list( particleNumArray, state, "mass_frac_part_", "_inflow"  );
      partMassFracMixRhsTags  = Expr::tag_list( particleNumArray, state, "mass_frac_part_", "_mix_rhs" );
      partMassFracKinRhsTags  = Expr::tag_list( particleNumArray, state, "mass_frac_part_", "_kin_rhs" );
      partMassFracFullRhsTags = Expr::tag_list( particleNumArray, state, "mass_frac_part_", "_full_rhs");
      partNumFracInflowTags   = Expr::tag_list( particleNumArray, state, "num_frac_part_", "_inflow"   );
      partNumFracMixRhsTags   = Expr::tag_list( particleNumArray, state, "num_frac_part_", "_mix_rhs"  );
      partNumFracKinRhsTags   = Expr::tag_list( particleNumArray, state, "num_frac_part_", "_kin_rhs"  );
      partNumFracFullRhsTags  = Expr::tag_list( particleNumArray, state, "num_frac_part_", "_full_rhs" );
      shNumberTags            = Expr::tag_list( particleNumArray, state, "sh_number_"                  );
      reynoldsNumberTags      = Expr::tag_list( particleNumArray, state, "re_number_part_"             );
      nusseltNumberTags       = Expr::tag_list( particleNumArray, state, "nu_number_"                  );
      gasSpeciesSrcTags       = Expr::tag_list( CanteraObjects::species_names(), state, "gas_species_src_"    );
      gasSpeciesSrcPFRTags    = Expr::tag_list( CanteraObjects::species_names(), state, "gas_species_src_pfr_");
    }

    ParticleSetup::ParticleSetup( std::vector<std::string>& particleNumArray,
                                  const YAML::Node& rootParser,
                                  std::set<Expr::ExpressionID>& initRoots,
                                  Expr::ExpressionFactory& initFactory,
                                  Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                                  Expr::TagList& primitiveTags,
                                  Expr::TagList& kinRhsTags,
                                  int& equIndex,
                                  const bool isRestart)
          : particleNumArray_(particleNumArray),
            rootParser_(rootParser),
            initRoots_(initRoots),
            initFactory_(initFactory),
            integrator_(integrator),
            primitiveTags_(primitiveTags),
            kinRhsTags_(kinRhsTags),
            equIndex_(equIndex),
            isRestart_(isRestart),
            coalimplement_(false),
            liquidimplement_(false)
    {
      reactorType_ = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();
      if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal"){
        if(rootParser_["Particles"]["ParticleLoading"]){
          npar_ = rootParser_["Particles"]["ParticleLoading"].as<double>(); // number of particles per gas mass
        }
        else{
          std::ostringstream msg;
          msg << "No particle loading block, 'ParticleLoading', was found in 'Particles' block!\n";
          throw std::runtime_error( msg.str());
        }

        if(reactorType_!="PFR"){
          if(rootParser_["Particles"]["ParticleLoadingInflow"]){
            nparInflow_ = rootParser_["Particles"]["ParticleLoadingInflow"].as<double>();
          }
          else{
            std::cout << "No inflow particle loading block, 'ParticleLoadingInflow', was found in 'Particles' block! \n"
                         "Inflow particle loading is set to be the same as initial particle loading\n"
                      << std::endl;
            nparInflow_ = npar_;
          }
        }
      }
      /**
       * @brief Get an array with index for every particle size. For example, if there are 3 particle sizes, the array would be {0,1,2}.
       */
      particleNumArray_.clear();
      if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "Identity"){
        particleNumArray_.push_back( "0" );
      }
      else if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
        const std::vector<double> nparList = rootParser_["Particles"]["Size"]["NumbersFraction"].as<std::vector<double> >();
        const int listNum = nparList.size();
        for( size_t i = 0;i < listNum;i++){
          std::string I = std::to_string(i);
          particleNumArray_.push_back( I );
        }
      }
      else {
        for( size_t i = 0;i < npar_;i++){
          std::string I = std::to_string(i);
          particleNumArray_.push_back( I );
        }
      }
      nparsize_ = particleNumArray.size();
      if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Coal"){
        coalimplement_ = true;
      }

      if (!isRestart_){
        setup_initial_conditions_particle();
      }
      build_equation_system_particle();
      modify_prim_kinrhs_tags();

      if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
        liquidimplement_ = true;
        Vaporization_ = new Vap::Vaporization(integrator_, rootParser_, particleNumArray_, equIndex_);
      }
      if(coalimplement_){
        setup_coal_subprocess();
      }
    }

    //---------------destructor---------------------------
    ParticleSetup::~ParticleSetup()
    {
      std::cout << "Delete ~ParticleSetup(): " << this << std::endl;
      if(liquidimplement_) delete Vaporization_;
      if(coalimplement_) delete CoalInterface_;
    }

    //---------------------------------------------------------------------------

    void ParticleSetup::setup_initial_conditions_particle(){
      const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_N );
      const Particles::ParticleEnsembleTags tagsPart( Expr::STATE_N, particleNumArray_ );

      // Parser particle initial conditions based on input file
      BOOST_FOREACH( const YAML::Node& parserPart_Spec, rootParser_["Particles"]["InitialConditions"] ){
              assert( parserPart_Spec.IsDefined());
              const YAML::Node& val = parserPart_Spec["value"];
              const Expr::Tag tagPart( parserPart_Spec["variable"].as<std::string>(), Expr::STATE_N);
              initRoots_.insert( initFactory_.register_expression( new ConstantT( tagPart, val.as<double>())));
            }
      // Parser particle inflow conditions based on input file
      if(reactorType_!="PFR"){
        if(rootParser_["Particles"]["InflowConditions"]){
          BOOST_FOREACH( const YAML::Node& parserPart_Spec, rootParser_["Particles"]["InflowConditions"] ){
                  assert( parserPart_Spec.IsDefined());
                  const YAML::Node& val = parserPart_Spec["value"];
                  const Expr::Tag tagPart( parserPart_Spec["variable"].as<std::string>(), Expr::STATE_N);
                  initRoots_.insert( initFactory_.register_expression( new ConstantT( tagPart, val.as<double>())));
                }
        }
        else{
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partTempInflowTag, tagsPart.partTempInitTag, 1.0, 0.0)));
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partRhoInflowTag, tagsPart.partRhoInitTag, 1.0, 0.0)));
        }
      }

      std::vector<double> ParticleSizeArray;
      particle_size(rootParser_["Particles"], ParticleSizeArray, nparsize_);
      for( size_t i = 0;i < nparsize_;i++){
        initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsPart.partSizeTags[i], ParticleSizeArray[i] ) ) );
      }

      for( size_t i = 0;i < nparsize_;i++){
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partTempTags[i], tagsPart.partTempInitTag, 1, 0 )));
        initFactory_.register_expression( new LinearT( tagsPart.partRhoTags[i], tagsPart.partRhoInitTag, 1, 0 ));
        initRoots_.insert( initFactory_.register_expression( new MassFracPartT( tagsPart.partMassFracTags[i], tagsPart.partRhoTags[i], tagsPart.partSizeTags[i],
                                                                                tagsPart.partNumFracTags[i]) ) );
        if(reactorType_!="PFR"){
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partTempInflowTags[i], tagsPart.partTempInflowTag, 1, 0)));
          initRoots_.insert( initFactory_.register_expression( new MassFracPartT( tagsPart.partMassFracInflowTags[i], tagsPart.partRhoInflowTag, tagsPart.partSizeTags[i],
                                                                                  tagsPart.partNumFracInflowTags[i] )));
        }

        if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partCpTags[i], tagsPart.partCpInitTag, 1, 0 ) ) );
          if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "Identity"){
            initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar_)));
            if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow_))); }
          }
          else if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
            const std::vector<double> nparFracList = rootParser_["Particles"]["Size"]["NumbersFraction"].as<std::vector<double> >();
            initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar_*nparFracList[i])));
            if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow_*nparFracList[i])));}
          }
          else{
            // npar/nparsize is always equal to 1 in the present code.
            initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar_/nparsize_)));
            if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow_/nparsize_)));}
          }
        }
        initRoots_.insert( initFactory_.register_expression( new DivideT( tagsPart.massEachPartTags[i], tagsPart.partMassFracTags[i], tagsPart.partNumFracTags[i])));
        initRoots_.insert( initFactory_.register_expression( new ProductT( tagsPart.partNumPerVolumeTags[i], tagsPart.partNumFracTags[i], tags.rhoTag)));
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsPart.partMassFracInitialTags[i], tagsPart.partMassFracTags[i], 1.0, 0.0)));
      }
      initRoots_.insert( initFactory_.register_expression( new ViscosityT( tags.dynviscosityTag, tags. tempTag, tags.massTags ) ) );

    }

    //---------------------------------------------------------------------------
    void ParticleSetup::build_equation_system_particle(){

      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

      Expr::ExpressionFactory& execFactory = integrator_->factory();

      parse_particle_parameters();

      execFactory.register_expression( new ThermalConducT( tagsE.thermalConducTag, tagsE.tempTag, tagsE.massTags, tagsE.mmwTag ) );
      // To simplify the jacobian matrix and because there is no sensitivity in viscosity function in PoKiTT now,
      // viscosity is set to be constant in present code.
//      execFactory.register_expression( new ViscosityT( tagsE.dynviscosityTag, tagsE. tempTag, tagsE.massTags ) );
      execFactory.register_expression( new PlaceHolderT( tagsE.dynviscosityTag ));
      if(reactorType_ == "PSRConstantVolume"){
        execFactory.register_expression( new HeatCpT( tagsE.cpTag, tagsE.tempTag, tagsE.massTags ));
      }

      execFactory.register_expression( new ConstantT( tagsEPart.partToGasMassTag, 0.0));
      execFactory.register_expression( new ConstantT( tagsE.scNumberTag, 1.0 ) );  // set sc to be constant

      for( size_t i = 0;i < nparsize_;i++){
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Solid"){
          execFactory.register_expression( new ShNumberT( tagsEPart.shNumberTags[i], tagsE.scNumberTag, tagsEPart.reynoldsNumberTags[i] ) );
        }
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partRhoTags[i] ));
          execFactory.register_expression( new SizePartT( tagsEPart.partSizeTags[i], tagsEPart.partRhoTags[i], tagsEPart.partMassFracTags[i],
                                                          tagsEPart.partNumFracTags[i]));
        }
        else{
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partSizeTags[i] ));
          integrator_->register_root_expression( new RhoPartT( tagsEPart.partRhoTags[i], tagsEPart.partMassFracTags[i], tagsEPart.partSizeTags[i],
                                                         tagsEPart.partNumFracTags[i]));
        }
        integrator_->register_root_expression( new PlaceHolderT( tagsEPart.partMassFracInitialTags[i] ));

        if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partCpTags[i] ));
        }
        execFactory.register_expression( new NuNumberT( tagsEPart.nusseltNumberTags[i], tagsE.cpTag, tagsE.dynviscosityTag, tagsE.thermalConducTag, tagsEPart.reynoldsNumberTags[i] ) );
        execFactory.register_expression( new ConvectionPartT( tagsEPart.partConvecTags[i], tagsE.tempTag, tagsEPart.partTempTags[i], tagsEPart.partSizeTags[i],
                                                              tagsE.thermalConducTag, tagsEPart.nusseltNumberTags[i], tagsEPart.partMassFracTags[i], tagsEPart.partCpTags[i],
                                                              tagsEPart.partNumFracTags[i] ));

        // particle equations
        if(reactorType_ != "PFR"){
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partTempInflowTags[i] ));
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partNumFracInflowTags[i] ));
          execFactory.register_expression( new PlaceHolderT( tagsEPart.partMassFracInflowTags[i] ));
          if( rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
            execFactory.register_expression( new PartTempMixT( tagsEPart.partTempMixRhsTags[i], tagsEPart.partTempTags[i], tagsEPart.partTempInflowTags[i],
                                                               tagsEPart.partMassFracTags[i], tagsEPart.partMassFracInflowTags[i], tagsE.tauMixTag,
                                                               tagsE.rhoInflowTag, tagsE.rhoTag ));
          }
          execFactory.register_expression( new ZeroDMixingCPT( tagsEPart.partNumFracMixRhsTags[i], tagsEPart.partNumFracInflowTags[i], tagsEPart.partNumFracTags[i],
                                                               tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
          execFactory.register_expression( new ZeroDMixingCPT( tagsEPart.partMassFracMixRhsTags[i], tagsEPart.partMassFracInflowTags[i], tagsEPart.partMassFracTags[i],
                                                               tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));

          execFactory.register_expression( new SumOpT( tagsEPart.partNumFracFullRhsTags[i], {tagsEPart.partNumFracMixRhsTags[i], tagsEPart.partNumFracKinRhsTags[i]}));
          execFactory.register_expression( new SumOpT( tagsEPart.partTempFullRhsTags[i], {tagsEPart.partTempMixRhsTags[i], tagsEPart.partTempHeatRhsTags[i], tagsEPart.partTempKinRhsTags[i] } ) );
          execFactory.register_expression( new SumOpT( tagsEPart.partMassFracFullRhsTags[i], {tagsEPart.partMassFracMixRhsTags[i], tagsEPart.partMassFracKinRhsTags[i] }) );
        }
        if(reactorType_ == "PFR"){
          execFactory.register_expression( new DivideT( tagsEPart.partNumFracFullRhsTags[i], tagsEPart.partNumFracKinRhsTags[i], tagsEpfr.uTag));
          execFactory.register_expression( new DivideT( tagsEPart.partMassFracFullRhsTags[i], tagsEPart.partMassFracKinRhsTags[i], tagsEpfr.uTag));
          execFactory.register_expression( new DivideT( tagsEPart.partTempHeatRhsPFRTags[i], tagsEPart.partTempHeatRhsTags[i], tagsEpfr.uTag));
          execFactory.register_expression( new DivideT( tagsEPart.partTempKinRhsPFRTags[i], tagsEPart.partTempKinRhsTags[i], tagsEpfr.uTag));
          execFactory.register_expression( new SumOpT( tagsEPart.partTempFullRhsTags[i], {tagsEPart.partTempHeatRhsPFRTags[i], tagsEPart.partTempKinRhsPFRTags[i] } ) );
        }
        // massSourceTag has been divided by (rho*V)
        execFactory.register_expression( new SourceFromGasMassT( tagsEPart.partMassFracKinRhsTags[i], tagsEPart.partToGasMassTag, tagsEPart.partMassFracTags[i]));
        execFactory.register_expression( new SourceFromGasMassT( tagsEPart.partNumFracKinRhsTags[i], tagsEPart.partToGasMassTag, tagsEPart.partNumFracTags[i]));
        execFactory.register_expression( new ConstantT( tagsEPart.partTempKinRhsTags[i], 0.0 ) );
        execFactory.register_expression( new SumOpT( tagsEPart.partTempHeatRhsTags[i], {tagsEPart.partGasRadiaTags[i], tagsEPart.partWallRadiaTags[i], tagsEPart.partConvecTags[i] } ) );

        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsEPart.partNumFracTags[i].name(), tagsEPart.partNumFracFullRhsTags[i], equIndex_, equIndex_ );
        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsEPart.partMassFracTags[i].name(), tagsEPart.partMassFracFullRhsTags[i], equIndex_, equIndex_ );
        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsEPart.partTempTags[i].name(), tagsEPart.partTempFullRhsTags[i], equIndex_, equIndex_);

        integrator_->register_root_expression( new DivideT( tagsEPart.massEachPartTags[i], tagsEPart.partMassFracTags[i], tagsEPart.partNumFracTags[i]));
        integrator_->register_root_expression( new ProductT( tagsEPart.partNumPerVolumeTags[i], tagsEPart.partNumFracTags[i], tagsE.rhoTag));
      }
      // modify gas phase equations
      if(reactorType_ == "PSRConstantPressure" or reactorType_ == "PFR"){
        const int ns = CanteraObjects::number_species();
        execFactory.register_expression( new SourceFromGasMassT( tagsEPart.gasEnthSrcTag, tagsEPart.partToGasMassTag, tagsE.enthTag));
        if(reactorType_ == "PSRConstantPressure"){
          execFactory.attach_dependency_to_expression( tagsEPart.gasEnthSrcTag, tagsE.enthKinRhsTag, Expr::ADD_SOURCE_EXPRESSION);
        }
        else{
          execFactory.register_expression( new DensitySourceFromGasMassT( tagsEPart.gasDensitySrcPFRTag, tagsEPart.partToGasMassTag, tagsE.rhoTag, tagsEpfr.uTag));
          execFactory.attach_dependency_to_expression( tagsEPart.gasDensitySrcPFRTag, tagsE.rhoFullRhsTag, Expr::ADD_SOURCE_EXPRESSION);
          execFactory.register_expression( new DivideT( tagsEPart.gasEnthSrcPFRTag, tagsEPart.gasEnthSrcTag, tagsEpfr.uTag));
          execFactory.attach_dependency_to_expression( tagsEPart.gasEnthSrcPFRTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION);
          execFactory.attach_dependency_to_expression( tagsEPart.partToGasMassTag, tagsEpfr.uFullRhsTag, Expr::SUBTRACT_SOURCE_EXPRESSION);
          execFactory.register_expression( new DpDxSourceFromGasMassT( tagsEPart.gasDpDxSrcPFRTag, tagsEPart.partToGasMassTag, tagsE.rhoTag, tagsE.tempTag, tagsE.enthTag,
                                                                       tagsE.mmwTag, tagsE.cpTag, tagsEpfr.uTag ));
          execFactory.attach_dependency_to_expression( tagsEPart.gasDpDxSrcPFRTag, tagsEpfr.dpdxTag, Expr::ADD_SOURCE_EXPRESSION);
        }

        for( size_t j=0; j<ns-1; ++j ){
          execFactory.register_expression( new SourceFromGasMassT( tagsEPart.gasSpeciesSrcTags[j], tagsEPart.partToGasMassTag, tagsE.massTags[j]));
          if(reactorType_ == "PSRConstantPressure"){
            execFactory.attach_dependency_to_expression( tagsEPart.gasSpeciesSrcTags[j], tagsE.yFullRhsTags[j], Expr::ADD_SOURCE_EXPRESSION);}
          else{
            execFactory.register_expression( new DivideT( tagsEPart.gasSpeciesSrcPFRTags[j], tagsEPart.gasSpeciesSrcTags[j], tagsEpfr.uTag));
            execFactory.attach_dependency_to_expression( tagsEPart.gasSpeciesSrcPFRTags[j], tagsE.yFullRhsTags[j], Expr::ADD_SOURCE_EXPRESSION);
          }
        }
      }
    }

    //---------------------------------------------------------------------------
    void ParticleSetup::modify_prim_kinrhs_tags(){
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );

      for( size_t i = 0;i < nparsize_;i++ ){
        primitiveTags_.push_back( tagsEPart.partNumFracTags[i] );
        primitiveTags_.push_back( tagsEPart.partMassFracTags[i] );
        primitiveTags_.push_back( tagsEPart.partTempTags[i] );

        if(reactorType_ != "PFR"){
          kinRhsTags_.push_back( tagsEPart.partNumFracKinRhsTags[i] );
          kinRhsTags_.push_back( tagsEPart.partMassFracKinRhsTags[i] );
          kinRhsTags_.push_back( tagsEPart.partTempKinRhsTags[i] );
        }
        else{
          kinRhsTags_.push_back( tagsEPart.partNumFracFullRhsTags[i] );
          kinRhsTags_.push_back( tagsEPart.partMassFracFullRhsTags[i] );
          kinRhsTags_.push_back( tagsEPart.partTempFullRhsTags[i] );
        }
      }
    }

    //---------------------------------------------------------------------------

    void ParticleSetup::initialize_particle(){
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );

      if(!isRestart_){
        for( size_t i = 0;i < nparsize_;i++ ){
          if(reactorType_!="PFR"){
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partTempInflowTags[i].name());
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partNumFracInflowTags[i].name());
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partMassFracInflowTags[i].name());
          }
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partMassFracInitialTags[i].name());
          if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partCpTags[i].name());
          }
          if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partRhoTags[i].name());
          }
          else {
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEPart.partSizeTags[i].name());
          }
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsE.dynviscosityTag.name());
        }
      }
      for( size_t i = 0;i < nparsize_;i++ ){
        if(reactorType_!="PFR"){
          integrator_->lock_field<FieldT>( tagsEPart.partTempInflowTags[i] );
          integrator_->lock_field<FieldT>( tagsEPart.partNumFracInflowTags[i] );
          integrator_->lock_field<FieldT>( tagsEPart.partMassFracInflowTags[i] );
        }
        integrator_->lock_field<FieldT>( tagsEPart.partMassFracInitialTags[i] );
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
          integrator_->lock_field<FieldT>( tagsEPart.partCpTags[i] );
        }
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
          integrator_->lock_field<FieldT>( tagsEPart.partRhoTags[i] );
        }
        else {
          integrator_->lock_field<FieldT>( tagsEPart.partSizeTags[i] );
        }
      }
      integrator_->lock_field<FieldT>( tagsE.dynviscosityTag );

      if(coalimplement_){
        CoalInterface_->initialize_coal();
      }
    }

    void ParticleSetup::particle_size( const YAML::Node& params,
                                       std::vector<double>& particleSizeArray,
                                       const int& nparsize)
    {
      particleSizeArray.clear();

      boost::random::mt19937 gen; // random generator
      for( size_t i = 0;i < nparsize;i++){
        // all particles have the same size
        if( params["Size"]["Distribution"].as<std::string>() == "Identity"){
          const double value = params["Size"]["Value"].as<double>();
          particleSizeArray.push_back( value );
        }
        // Two lists are given for particle values and numbers fraction respectivally.
        // For example, sizeList = [1e-5, 1e-4, 1e-3]. nparList=[0.1,0.3,0.6]
        // It means that there are three particle sizes and the fraction of numbers with each particle size are 0.1, 0.3 and 0.6.
        else if( params["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
          const std::vector<double> sizeList = params["Size"]["Values"].as<std::vector<double> >();
          const std::vector<double> nparFracList = params["Size"]["NumbersFraction"].as<std::vector<double> >();
          if(sizeList.size() != nparFracList.size()){
            throw std::runtime_error("Particles Initilization: The size of 'Numbers' and 'Values' should be equal!");
          }
          int nparFraccheck = 0;
          for( size_t i = 0;i < sizeList.size();i++){
            nparFraccheck += nparFracList[i];
            particleSizeArray.push_back(sizeList[i]);
          }
          if (int(nparFraccheck) != 1) {
            throw std::runtime_error("Particles Initilization: The sum of the number fraction of particles specified in the 'NumbersFraction' is not unity!");
          }
          return;
        }
        // Uniform ditribution between `lower` and 'upper`.
        // If using `boost::random::uniform_real_distribution`, the number of particles need to be very big to get this distribution.
        // To make sure getting uniform distribution even number of particle is not big, the size is calculated manually here.
        else if( params["Size"]["Distribution"].as<std::string>() == "Uniform"){
          const double lower = params["Size"]["Lower"].as<double>();
          const double upper = params["Size"]["Upper"].as<double>();
          // random size between [Lower, Upper]
//          boost::random::uniform_real_distribution<> num(lower, upper);
//          boost::random::variate_generator<boost::mt19937&,boost::random::uniform_real_distribution<>> size(gen,num);
//          particleSizeArray.push_back( size() );
          // uniform distribution between [Lower, Upper]
          const double dsize = (upper-lower)/(npar_-1);
          particleSizeArray.push_back( lower );
          for(size_t i=1; i < npar_; i++){
            particleSizeArray.push_back( lower+i*dsize );
          }
        }
        else if( params["Size"]["Distribution"].as<std::string>() == "Normal"){
          const double mean = params["Size"]["Mean"].as<double>();
          const double standardDeviation = params["Size"]["StandardDeviation"].as<double>();
          boost::random::normal_distribution<> num(mean, standardDeviation);
          boost::random::variate_generator<boost::mt19937&,boost::random::normal_distribution<>> size(gen,num);
          particleSizeArray.push_back( size() );
        }
        else if( params["Size"]["Distribution"].as<std::string>() == "LogNormal"){
          const double location = params["Size"]["Location"].as<double>();
          const double scale = params["Size"]["Scale"].as<double>();
          boost::random::lognormal_distribution<> num(location, scale);
          boost::random::variate_generator<boost::mt19937&,boost::random::lognormal_distribution<>> size(gen,num);
          particleSizeArray.push_back( size() );
        }
        else if( params["Size"]["Distribution"].as<std::string>() == "Weibull"){
          const double shape = params["Size"]["Shape"].as<double>();
          const double scale = params["Size"]["Scale"].as<double>();
          boost::random::weibull_distribution<> num(shape, scale);
          boost::random::variate_generator<boost::mt19937&,boost::random::weibull_distribution<>> size(gen,num);
          particleSizeArray.push_back( size() );
        }
      }
    }

    //-------------------------------------------------------------------
    void ParticleSetup::parse_particle_parameters(){

      const YAML::Node parserPart = rootParser_["Particles"];

      if( !parserPart["ParticleParameters"] ){
        std::cout << "No 'ParticleParameters' spec was found in the input file.  Using default parameters\n";
      }

      const double emissivityPartWall = parserPart["ParticleParameters"]["EmissivityPartWall"].as<double>( 0.0 );
      const double emissivityPartGas = parserPart["ParticleParameters"]["EmissivityPartGas"].as<double>( 0.0 );
      const double Twall = parserPart["ParticleParameters"]["SurroundingsTemperature"].as<double>( 300 );

      const double Renumber = rootParser_["FlowParameters"]["ReynoldsNumber"].as<double>( 1e5 );

      // build the expressions implied by these quantities
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
      Expr::ExpressionFactory& execFactory = integrator_->factory();

      if(parserPart["ParticleParameters"]["RadiationWithWall"].as<std::string>("OFF") == "ON"){
        execFactory.register_expression( new ConstantT( tagsEPart.tempRadiaWallTag, Twall));
        for( size_t i = 0;i < nparsize_;i++){
          execFactory.register_expression( new RadiationPartT( tagsEPart.partWallRadiaTags[i], tagsEPart.partTempTags[i], tagsEPart.tempRadiaWallTag,
                                                               tagsEPart.partSizeTags[i], tagsEPart.partMassFracTags[i], tagsEPart.partCpTags[i],
                                                               tagsEPart.partNumFracTags[i], emissivityPartWall ));
        }
      }
      else{
        for( size_t i = 0;i < nparsize_;i++){
          execFactory.register_expression( new ConstantT( tagsEPart.partWallRadiaTags[i], 0.0));
        }
      }

      for( size_t i = 0;i < nparsize_;i++){
        if(parserPart["ParticleParameters"]["RadiationWithGas"].as<std::string>("OFF") == "ON"){
          execFactory.register_expression( new RadiationPartT( tagsEPart.partGasRadiaTags[i], tagsEPart.partTempTags[i], tagsE.tempTag,
                                                               tagsEPart.partSizeTags[i], tagsEPart.partMassFracTags[i], tagsEPart.partCpTags[i],
                                                               tagsEPart.partNumFracTags[i], emissivityPartGas ));
        }
        else{
          execFactory.register_expression( new ConstantT( tagsEPart.partGasRadiaTags[i], 0.0));
        }
        execFactory.register_expression( new ConstantT( tagsEPart.reynoldsNumberTags[i], Renumber ) );
      }

      //modify gas equations
      if(reactorType_ == "PSRConstantVolume"){
        execFactory.register_expression( new PartToGasHeatCVT( tagsEPart.partConvecToGasTag, tagsEPart.partConvecTags, tagsEPart.partMassFracTags, tagsEPart.partCpTags, tagsE.rhoTag ));
        execFactory.attach_dependency_to_expression(tagsEPart.partConvecToGasTag, tagsE.rhoEgyHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
        execFactory.register_expression( new PartToGasHeatCVT( tagsEPart.partRadiaToGasTag, tagsEPart.partGasRadiaTags, tagsEPart.partMassFracTags, tagsEPart.partCpTags, tagsE.rhoTag ));
        execFactory.attach_dependency_to_expression(tagsEPart.partRadiaToGasTag, tagsE.rhoEgyHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
      }
      if(reactorType_ == "PSRConstantPressure" or reactorType_ == "PFR"){
        execFactory.register_expression( new PartToGasHeatCPT( tagsEPart.partConvecToGasTag, tagsEPart.partConvecTags, tagsEPart.partMassFracTags, tagsEPart.partCpTags));
        execFactory.register_expression( new PartToGasHeatCPT( tagsEPart.partRadiaToGasTag, tagsEPart.partGasRadiaTags, tagsEPart.partMassFracTags, tagsEPart.partCpTags));

        if(reactorType_ == "PSRConstantPressure"){
          execFactory.attach_dependency_to_expression(tagsEPart.partConvecToGasTag, tagsE.enthHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
          execFactory.attach_dependency_to_expression(tagsEPart.partRadiaToGasTag, tagsE.enthHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
        }
        else{
          execFactory.register_expression( new DivideT( tagsEPart.partConvecToGasPFRTag, tagsEPart.partConvecToGasTag, tagsEpfr.uTag ));
          execFactory.register_expression( new DivideT( tagsEPart.partRadiaToGasPFRTag, tagsEPart.partRadiaToGasTag, tagsEpfr.uTag ));
          execFactory.attach_dependency_to_expression(tagsEPart.partConvecToGasPFRTag, tagsE.enthHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
          execFactory.attach_dependency_to_expression(tagsEPart.partRadiaToGasPFRTag, tagsE.enthHeatRhsTag, Expr::ADD_SOURCE_EXPRESSION);
        }
      }

      std::cout << std::endl;
      std::cout << "Particle Parameters" << std::endl
                << "-----------------------------------------" << std::endl
                << std::endl;
      if (!coalimplement_){
        std::cout << " - particle loading for initial condition  : " << npar_           << std::endl;
        if(reactorType_!="PFR"){
          std::cout << " - particle loading for inflow stream      : " << nparInflow_     << std::endl;
        }
      }
      std::cout << " - number of particles size                : " << nparsize_          << std::endl
                << " - particle emissivity                     : " << emissivityPartWall << std::endl
                << " - gas emissivity                          : " << emissivityPartGas  << std::endl
                << " - radiation temperature (K)               : " << Twall              << std::endl
                << " - Reynolds number                         : " << Renumber           << std::endl
                << " - radiation between particle and wall     : " << parserPart["ParticleParameters"]["RadiationWithWall"].as<std::string>("OFF") << std::endl
                << " - radiation between particle and gas      : " << parserPart["ParticleParameters"]["RadiationWithGas"].as<std::string>("OFF") << std::endl
                << "------------------------------------" << std::endl
                << std::endl;
    }

    void ParticleSetup::modify_idxmap( std::map<Expr::Tag, int>& primVarIdxMap,
                                       const std::map<Expr::Tag, int>& consVarIdxMap,
                                       std::map<Expr::Tag, int>& kinRhsIdxMap,
                                       const std::map<Expr::Tag, int>& rhsIdxMap){

      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );

      for( size_t i = 0;i < nparsize_;i++ ){
        primVarIdxMap[tagsEPart.partNumFracTags[i]] = consVarIdxMap.at( tagsEPart.partNumFracTags[i] );
        primVarIdxMap[tagsEPart.partMassFracTags[i]] = consVarIdxMap.at( tagsEPart.partMassFracTags[i] );
        primVarIdxMap[tagsEPart.partTempTags[i]] = consVarIdxMap.at( tagsEPart.partTempTags[i] );

        if(reactorType_ != "PFR"){
          kinRhsIdxMap[tagsEPart.partNumFracKinRhsTags[i]] = rhsIdxMap.at( tagsEPart.partNumFracFullRhsTags[i] );
          kinRhsIdxMap[tagsEPart.partMassFracKinRhsTags[i]] = rhsIdxMap.at( tagsEPart.partMassFracFullRhsTags[i] );
          kinRhsIdxMap[tagsEPart.partTempKinRhsTags[i]] = rhsIdxMap.at( tagsEPart.partTempFullRhsTags[i] );
        }
        else{
          kinRhsIdxMap[tagsEPart.partNumFracFullRhsTags[i]] = rhsIdxMap.at( tagsEPart.partNumFracFullRhsTags[i] );
          kinRhsIdxMap[tagsEPart.partMassFracFullRhsTags[i]] = rhsIdxMap.at( tagsEPart.partMassFracFullRhsTags[i] );
          kinRhsIdxMap[tagsEPart.partTempFullRhsTags[i]] = rhsIdxMap.at( tagsEPart.partTempFullRhsTags[i] );
        }
      }
      if(coalimplement_ ){
        CoalInterface_->modify_idxmap_coal( primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap );
      }
    }

    typedef boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>> sharePtrT;
    sharePtrT ParticleSetup::dvdu_part( const std::map<Expr::Tag, int> primVarIdxMap,
                                        const std::map<Expr::Tag, int> consVarIdxMap){

      ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, dVdUPart )
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE , particleNumArray_);

      for( size_t i = 0;i < nparsize_;i++ ){
        const Expr::Tag& TParti = tagsEPart.partTempTags[i];
        const Expr::Tag& MassFracParti = tagsEPart.partMassFracTags[i];
        const Expr::Tag& numFracParti = tagsEPart.partNumFracTags[i];
        dVdUPart->element<double>( primVarIdxMap.at( numFracParti ), consVarIdxMap.at( numFracParti )) = 1.0;
        dVdUPart->element<double>( primVarIdxMap.at( MassFracParti ), consVarIdxMap.at( MassFracParti )) = 1.0;
        dVdUPart->element<double>( primVarIdxMap.at( TParti ), consVarIdxMap.at( TParti )) = 1.0;
      }
      if(coalimplement_ ){
        CoalInterface_->dvdu_coal(dVdUPart, primVarIdxMap, consVarIdxMap);
      }
      dVdUPart->finalize();
      return dVdUPart;
    }

    sharePtrT ParticleSetup::dqdv_part( const std::map<Expr::Tag, int> primVarIdxMap,
                                        const std::map<Expr::Tag, int> rhsIdxMap,
                                        const std::map<Expr::Tag, Expr::Tag> consVarRhsMap){

      ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, dqdVPart )
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const Expr::Tag&     rho          = tagsE.rhoTag;
      const Expr::Tag&     temp         = tagsE.tempTag;
      const Expr::TagList& massFrac     = tagsE.massTags;
      const Expr::TagList& tempPart     = tagsEPart.partTempTags;
      const Expr::TagList& massFracPart = tagsEPart.partMassFracTags;
      const Expr::TagList& numFracPart  = tagsEPart.partNumFracTags;
      const Expr::TagList& qPart        = tagsEPart.partTempHeatRhsTags;

      Expr::Tag erg_enth;
      Expr::Tag qGas;

      if(reactorType_ == "PSRConstantVolume"){
        erg_enth = tagsE.rhoEgyTag;
        qGas     = tagsE.rhoEgyHeatRhsTag;
      }
      else{
        erg_enth = tagsE.enthTag;
        qGas     = tagsE.enthHeatRhsTag;
      }
      const auto& erg_enthRhsIdx = rhsIdxMap.at( consVarRhsMap.at( erg_enth ));

      const size_t nspec = CanteraObjects::number_species();
      for( size_t i = 0;i < nparsize_;i++ ){
        const auto& tempPartRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tempPart[i] ));
        auto tempPartIdxi     = primVarIdxMap.at( tempPart[i] );
        auto massFracPartIdxi = primVarIdxMap.at( massFracPart[i] );

        auto numPartIdxi = primVarIdxMap.at( numFracPart[i] );
        dqdVPart->element<FieldT>( erg_enthRhsIdx, numPartIdxi ) = sensitivity( qGas, numFracPart[i] );
        dqdVPart->element<FieldT>( erg_enthRhsIdx, tempPartIdxi ) = sensitivity( qGas, tempPart[i] );
        dqdVPart->element<FieldT>( erg_enthRhsIdx, massFracPartIdxi ) = sensitivity( qGas, massFracPart[i] );

        dqdVPart->element<FieldT>( tempPartRhsIdxi, numPartIdxi ) = sensitivity( qPart[i], numFracPart[i] );
        dqdVPart->element<FieldT>( tempPartRhsIdxi, tempPartIdxi ) = sensitivity( qPart[i], tempPart[i] );
        dqdVPart->element<FieldT>( tempPartRhsIdxi, massFracPartIdxi ) = sensitivity( qPart[i], massFracPart[i] );

        dqdVPart->element<FieldT>( tempPartRhsIdxi, primVarIdxMap.at( temp ) ) = sensitivity( qPart[i], temp );
        dqdVPart->element<FieldT>( tempPartRhsIdxi, primVarIdxMap.at( rho ) ) = sensitivity( qPart[i], rho );
        for( size_t j = 0;j < nspec-1;j++ ){
          dqdVPart->element<FieldT>( tempPartRhsIdxi, primVarIdxMap.at( massFrac[j] ) ) = sensitivity( qPart[i], massFrac[j] );
        }
      }
      for( size_t j = 0;j < nspec-1;j++ ){
        dqdVPart->element<FieldT>( erg_enthRhsIdx, primVarIdxMap.at( massFrac[j] )) = sensitivity( qGas, massFrac[j] );
      }
      if(reactorType_ == "PSRConstantVolume"){
          dqdVPart->element<FieldT>( erg_enthRhsIdx, primVarIdxMap.at( rho ) ) = sensitivity( qGas, rho );
      }
      dqdVPart->finalize();
      return dqdVPart;
    }

      sharePtrT ParticleSetup::dmMixingdv_part( const std::map<Expr::Tag, int> primVarIdxMap,
                                                const std::map<Expr::Tag, int> rhsIdxMap,
                                                const std::map<Expr::Tag, Expr::Tag> consVarRhsMap){

      ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, dmMixingdVPart )
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE , particleNumArray_);

      const Expr::Tag&     rho             = tagsE.rhoTag;
      const Expr::TagList& tempPart        = tagsEPart.partTempTags;
      const Expr::TagList& massFracPart    = tagsEPart.partMassFracTags;
      const Expr::TagList& numFracPart     = tagsEPart.partNumFracTags;
      const Expr::TagList& mixPartTemp     = tagsEPart.partTempMixRhsTags;
      const Expr::TagList& mixPartMassFrac = tagsEPart.partMassFracMixRhsTags;
      const Expr::TagList& mixPartNumFrac  = tagsEPart.partNumFracMixRhsTags;
      const Expr::TagList& massPart        = tagsEPart.partMassFracTags;

      for( size_t i = 0;i < nparsize_;i++ ){

        const auto& numFracPartRhsIdxi  = rhsIdxMap.at( consVarRhsMap.at( tagsEPart.partNumFracTags[i] ) );
        const auto& massFracPartRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsEPart.partMassFracTags[i] ) );
        const auto& tempPartRhsIdxi     = rhsIdxMap.at( consVarRhsMap.at( tagsEPart.partTempTags[i] ) );
        const auto& numFracPartIdxi     = primVarIdxMap.at( tagsEPart.partNumFracTags[i] );
        const auto& massFracPartIdxi    = primVarIdxMap.at( tagsEPart.partMassFracTags[i] );
        const auto& tempPartIdxi        = primVarIdxMap.at( tagsEPart.partTempTags[i] );
        const auto& massPartIdxi        = primVarIdxMap.at( tagsEPart.partMassFracTags[i] );

        dmMixingdVPart->element<FieldT>( numFracPartRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixPartNumFrac[i], rho);
        dmMixingdVPart->element<FieldT>( massFracPartRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixPartMassFrac[i], rho);
        dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixPartTemp[i], rho);

        dmMixingdVPart->element<FieldT>( numFracPartRhsIdxi, numFracPartIdxi ) = sensitivity(mixPartNumFrac[i], numFracPart[i]);
        dmMixingdVPart->element<FieldT>( massFracPartRhsIdxi, massFracPartIdxi ) = sensitivity(mixPartMassFrac[i], massFracPart[i]);
        dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, tempPartIdxi ) = sensitivity(mixPartTemp[i], tempPart[i]);

        dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, massPartIdxi ) = sensitivity(mixPartTemp[i], massPart[i]);

      }
      if(coalimplement_){
        CoalInterface_->dmMixingdv_coal(dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap);
      }
      dmMixingdVPart->finalize();
      return dmMixingdVPart;
    }

    void ParticleSetup::restart_var_particle(std::vector<std::string>& restartVars){
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_);
      for( size_t i=0; i<particleNumArray_.size(); ++i){
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() != "Coal" ){
          restartVars.push_back(tagsEPart.partCpTags[i].name());
        }
        if(rootParser_["Particles"]["ParticleType"].as<std::string>() == "Liquid"){
          restartVars.push_back(tagsEPart.partRhoTags[i].name());
        }
        else {
          restartVars.push_back(tagsEPart.partSizeTags[i].name());
        }
      }
      restartVars.push_back(tagsE.dynviscosityTag.name());
      if(coalimplement_){
        CoalInterface_->restart_var_coal(restartVars);
      }
    }

    void ParticleSetup::setup_coal_subprocess(){

      const Coal::CoalType coalType = Coal::coal_type( rootParser_["Particles"]["CoalType"].as<std::string>() );
      const Vap::VapModel vapModel = Vap::vap_model( rootParser_["Particles"]["VapModel"].as<std::string>() );
      const Dev::DevModel devModel = Dev::devol_model( rootParser_["Particles"]["DevModel"].as<std::string>() );
      const Tarsoot::TarsootModel tarSootModel = Tarsoot::tarsoot_model( rootParser_["Particles"]["TarSootModel"].as<std::string>() );
      Char::CharModel charModel;
      if(rootParser_["Particles"]["CharModel"].size() > 1){
        charModel = Char::char_model("GasifOxid");
      }
      else{
        charModel = Char::char_model( rootParser_["Particles"]["CharModel"].as<std::string>() );
      }

      CoalInterface_ = new Coal::CoalInterface(coalType, vapModel, devModel, tarSootModel, charModel, particleNumArray_, rootParser_, initRoots_, initFactory_,
                                               integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_ );

      std::cout << std::endl;
      std::cout << "Coal Model" << std::endl
                << "----------------------------------------" << std::endl
                << " - Coal type                          : " << Coal::coal_type_name(coalType)            << std::endl
                << " - Vaporization model                 : " << Vap::vap_model_name(vapModel)             << std::endl
                << " - Devolatilization model             : " << Dev::dev_model_name(devModel)             << std::endl
                << " - Tar & soot model                   : " << Tarsoot::tarsoot_model_name(tarSootModel) << std::endl
                << " - Char gasification/oxidation model  : " << Char::char_model_name(charModel)          << std::endl;
      if(rootParser_["Particles"]["CharModel"].size() > 1){
        std::cout << " - Char gasification model            : " << rootParser_["Particles"]["CharModel"]["GasificationModel"].as<std::string>() << std::endl
                  << " - Char oxidation model               : " << rootParser_["Particles"]["CharModel"]["OxidationModel"].as<std::string>() << std::endl;
      }
      std::cout << " - Number of equations                : " << equIndex_ + 1 << std::endl
                << "----------------------------------------" << std::endl
                << std::endl;
    }

} // namespace ParticleEnsembleUtil
