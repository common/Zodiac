#include <stdexcept>
#include <sstream>

#include "SumOp.h"
#include "ZeroDimMixing.h"
#include "TransformationMatrixExpressions.h"
#include "ReactorEnsembleUtil.h"
#include "particles/ParticleInterface.h"
#include "particles/ParticleTransformExpressions.h"
#include "CoalInterface.h"
#include "CoalData.h"
#include "CoalHeatCapacity.h"
#include "particles/Vaporization/VaporizationInterface.h"
#include "Devolatilization/DevolatilizationInterface.h"
#include "CharCombustion/CharInterface.h"
#include "Devolatilization/CPD/c0_fun.h"
#include "CoalTempMixRHS.h"
#include "CoalParticleLoading.cpp"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

using Expr::matrix::sensitivity;

namespace Coal{

  typedef Expr::LinearFunction<FieldT>::Builder LinearT;
  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef Expr::PlaceHolder<FieldT>::Builder PlaceHolderT;

  typedef ReactorEnsembleUtil::SumOp<FieldT>::Builder SumOpT;
  typedef ReactorEnsembleUtil::ZeroDMixingCP<FieldT>::Builder ZeroDMixingCPT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivideT;

  typedef Particles::SourceFromGasMassChange<FieldT>::Builder SourceFromGasMassT;
  typedef Coal::CoalHeatCapacity<FieldT>::Builder CoalCpT;
  typedef Coal::CoalTempMixRHS<FieldT>::Builder CoalTempMixRHST;
  typedef Coal::CoalParticleLoading<FieldT>::Builder PartLoadingT;
  //--------------------------------------------------------------------------------------------------------

  CoalEnsembleTags::
  CoalEnsembleTags( const Expr::Context& state,
                    const std::vector<std::string>& particleNumArray)
        : fuelAirEquivalenceRatio    ( Expr::Tag( "fuel_air_equiv_ratio", state ))
  {
    moistureMassEachPartTags    = Expr::tag_list( particleNumArray, state, "moisture_mass_each_part_"        );
    moistureMassFracTags        = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_"             );
    moistureMassFracInitialTags = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_", "_initial" );
    moistureMassFracInflowTags  = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_", "_inflow"  );
    moistureMassFracMixRhsTags  = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_", "_mix_rhs" );
    moistureMassFracKinRhsTags  = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_", "_kin_rhs" );
    moistureMassFracFullRhsTags = Expr::tag_list( particleNumArray, state, "moisture_mass_frac_", "_full_rhs");
    charMassFracTags            = Expr::tag_list( particleNumArray, state, "char_mass_frac_"                 );
    charMassFracInitialTags     = Expr::tag_list( particleNumArray, state, "char_mass_frac_", "_initial"     );
    charMassEachPartTags        = Expr::tag_list( particleNumArray, state, "char_mass_each_part_"            );
    charMassFracInflowTags      = Expr::tag_list( particleNumArray, state, "char_mass_frac_", "_inflow"      );
    charMassFracMixRhsTags      = Expr::tag_list( particleNumArray, state, "char_mass_frac_", "_mix_rhs"     );
    charMassFracKinRhsTags      = Expr::tag_list( particleNumArray, state, "char_mass_frac_", "_kin_rhs"     );
    charMassFracFullRhsTags     = Expr::tag_list( particleNumArray, state, "char_mass_frac_", "_full_rhs"    );
    volMassEachPartTags         = Expr::tag_list( particleNumArray, state, "vol_mass_each_part_"             );
    volMassFracTags             = Expr::tag_list( particleNumArray, state, "vol_mass_frac_"                  );
    volMassFracInflowTags       = Expr::tag_list( particleNumArray, state, "vol_mass_frac_", "_inflow"       );
    volMassFracMixRhsTags       = Expr::tag_list( particleNumArray, state, "vol_mass_frac_", "_mix_rhs"      );
    volMassFracKinRhsTags       = Expr::tag_list( particleNumArray, state, "vol_mass_frac_", "_kin_rhs"      );
    volMassFracFullRhsTags      = Expr::tag_list( particleNumArray, state, "vol_mass_frac_", "_full_rhs"     );
  };

  CoalInterface::
  CoalInterface( const CoalType coalType,
                 const Vap::VapModel vapModel,
                 const Dev::DevModel devModel,
                 const Tarsoot::TarsootModel tarsootModel,
                 const Char::CharModel charModel,
                 const std::vector<std::string>& particleNumArray,
                 const YAML::Node& rootParser,
                 std::set<Expr::ExpressionID>& initRoots,
                 Expr::ExpressionFactory& initFactory,
                 Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                 Expr::TagList& primitiveTags,
                 Expr::TagList& kinRhsTags,
                 int& equIndex,
                 const bool isRestart)
    : coaltype_(coalType),
      vapModel_(vapModel),
      devModel_(devModel),
      tarsootModel_(tarsootModel),
      charModel_(charModel),
      particleNumArray_(particleNumArray),
      rootParser_(rootParser),
      initRoots_(initRoots),
      initFactory_(initFactory),
      integrator_(integrator),
      primitiveTags_(primitiveTags),
      kinRhsTags_(kinRhsTags),
      equIndex_(equIndex),
      isRestart_(isRestart)
  {
    reactorType_ = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();
    if(!isRestart_){
      setup_initial_conditions_coal();
    }
    build_equation_system_coal();
    modify_prim_kinrhs_tags_coal();
    std::vector<std::string> partNumSpeciesArray, speciesArray, partNumSpeciesCharArray, speciesCharArray, binarySpeciesArray;
    if( vapModel_ != Vap::OFF){
      vap_ = new Vap::Vaporization(integrator_, rootParser_, particleNumArray_, equIndex_ );
    }
    if (devModel_ != Dev::OFF){
      dev_ = new Dev::DevolatilizationInterface(coaltype_, devModel_, tarsootModel_, particleNumArray_, speciesArray, partNumSpeciesArray,
                                                rootParser_, initRoots_, initFactory_, integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_);
    }
    if (charModel_ != Char::OFF){
      char_ = new Char::CharInterface(coaltype_, charModel_, particleNumArray_, speciesCharArray, binarySpeciesArray, partNumSpeciesCharArray,
                                      rootParser_, initRoots_, initFactory_, integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_);
    }
  }

  //------------------------------------------------------------------
  CoalInterface::
  ~CoalInterface()
  {
    std::cout << "Delete ~CoalInterface(): " << this << std::endl;
    if( vapModel_ != Vap::OFF){
      delete vap_;
    }
    if (devModel_ != Dev::OFF){
      delete dev_;
    }
    if (charModel_ != Char::OFF){
      delete char_;
    }
  }

  //---------------------------------------------------------------------------------------------
  void CoalInterface::setup_initial_conditions_coal(){

    const ReactorEnsembleUtil::ReactorEnsembleTags tags ( Expr::STATE_N);
    const Particles::ParticleEnsembleTags tagsPart( Expr::STATE_N, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsCoal( Expr::STATE_N, particleNumArray_ );
    const Vap::VapTags tagsVap( Expr::STATE_N, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    const Coal::CoalComposition coalComp(coaltype_, rootParser_["Particles"]);
    const int IdxO2 = CanteraObjects::species_index( "O2" );

    std::vector<double> nparList(nparsize, 1);
    if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
      nparList = rootParser_["Particles"]["Size"]["Numbers"].as<std::vector<double>>();
    }

    double ymoisInit, yvolInit, ycharInit, ymoisInflow, yvolInflow, ycharInflow;
    const std::vector<double> defaultCoalComp={coalComp.get_moisture(), coalComp.get_vm(), coalComp.get_fixed_c()};
    const std::vector<double> coalInitComp = rootParser_["Particles"]["CoalInitComp"].as<std::vector<double> >(defaultCoalComp);
    ymoisInit = coalInitComp[0];
    yvolInit  = coalInitComp[1];
    ycharInit = coalInitComp[2];
    if(reactorType_!="PFR"){
      const std::vector<double> coalInflowComp = rootParser_["Particles"]["CoalInflowComp"].as<std::vector<double> >(defaultCoalComp);
      ymoisInflow = coalInflowComp[0];
      yvolInflow  = coalInflowComp[1];
      ycharInflow = coalInflowComp[2];
    }

    for( size_t i = 0;i < nparsize;i++){

      // moisture, volatile and char mass
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.moistureMassFracTags[i], tagsPart.partMassFracTags[i], ymoisInit, 0.0) ) );
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.moistureMassFracInitialTags[i], tagsPart.partMassFracTags[i], ymoisInit, 0.0) ) );
      if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.moistureMassFracInflowTags[i], tagsPart.partMassFracInflowTags[i], ymoisInflow, 0.0) ) );}

      if(devModel_ == Dev::OFF or devModel_ == Dev::CPDM){
        const double c0 = CPD::c0_fun( coalComp.get_C(), coalComp.get_O());
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.volMassFracTags[i], tagsPart.partMassFracTags[i], yvolInit*(1-c0), 0.0 ) ) );
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.charMassFracTags[i], tagsPart.partMassFracTags[i], ycharInit+yvolInit*c0, 0.0 ) ) );
        if(reactorType_!="PFR"){
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.volMassFracInflowTags[i], tagsPart.partMassFracInflowTags[i], yvolInflow * ( 1 - c0 ), 0.0 )));
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.charMassFracInflowTags[i], tagsPart.partMassFracInflowTags[i], ycharInflow + yvolInflow * c0, 0.0 )));
        }
      }
      else{
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.volMassFracTags[i], tagsPart.partMassFracTags[i], yvolInit, 0.0 )));
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.charMassFracTags[i], tagsPart.partMassFracTags[i], ycharInit, 0.0 )));
        if(reactorType_!="PFR"){
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.volMassFracInflowTags[i], tagsPart.partMassFracInflowTags[i], yvolInflow, 0.0 )));
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.charMassFracInflowTags[i], tagsPart.partMassFracInflowTags[i], ycharInflow, 0.0 )));
        }
      }
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCoal.charMassFracInitialTags[i], tagsCoal.charMassFracTags[i], 1.0, 0.0 ) ) );
      initRoots_.insert( initFactory_.register_expression( new DivideT( tagsCoal.moistureMassEachPartTags[i], tagsCoal.moistureMassFracTags[i], tagsPart.partNumFracTags[i])));
      initRoots_.insert( initFactory_.register_expression( new DivideT( tagsCoal.volMassEachPartTags[i], tagsCoal.volMassFracTags[i], tagsPart.partNumFracTags[i])));
      initRoots_.insert( initFactory_.register_expression( new DivideT( tagsCoal.charMassEachPartTags[i], tagsCoal.charMassFracTags[i], tagsPart.partNumFracTags[i])));
    }
    // number fraction of particle
    if(reactorType_!="PFR"){
      if(rootParser_["Particles"]["ParticleLoadingInflow"]){
        const double nparInflow = rootParser_["Particles"]["ParticleLoadingInflow"].as<double>();
        for( size_t i = 0;i < nparsize;i++){
          if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "Identity"){
            initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow)));
          }
          else if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
            const std::vector<double> nparFracList = rootParser_["Particles"]["Size"]["NumbersFraction"].as<std::vector<double> >();
            initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow*nparFracList[i])));
          }
          else{
            // npar/nparsize is always equal to 1 in the present code.
            initRoots_.insert( initFactory_.register_expression( new ConstantT(tagsPart.partNumFracInflowTags[i], nparInflow/nparsize)));
          }
        }
      }
      else{
        initRoots_.insert( initFactory_.register_expression( new PartLoadingT( tagsPart.partNumFracInflowTags, tags.massInflowTags[IdxO2], tagsCoal.fuelAirEquivalenceRatio, tagsPart.partRhoTags, tagsPart.partSizeTags,
                                                                               nparList, coalComp, yvolInflow, ycharInflow, devModel_, charModel_ )));
      }
    }

    if(rootParser_["Particles"]["ParticleLoading"]){
      const double npar = rootParser_["Particles"]["ParticleLoading"].as<double>(); // number of particles per gas mass
      for( size_t i = 0;i < nparsize;i++){
        if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "Identity"){
          initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar)));
        }
        else if(rootParser_["Particles"]["Size"]["Distribution"].as<std::string>() == "UserDefinedList"){
          const std::vector<double> nparFracList = rootParser_["Particles"]["Size"]["NumbersFraction"].as<std::vector<double> >();
          initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar*nparFracList[i])));
        }
        else{
          // npar/nparsize is always equal to 1 in the present code.
          initRoots_.insert(initFactory_.register_expression( new ConstantT( tagsPart.partNumFracTags[i], npar/nparsize)));
        }
      }
    }
    else{
      initRoots_.insert( initFactory_.register_expression( new PartLoadingT( tagsPart.partNumFracTags, tags.massTags[IdxO2], tagsCoal.fuelAirEquivalenceRatio, tagsPart.partRhoTags, tagsPart.partSizeTags,
                                                                             nparList, coalComp, yvolInit, ycharInit, devModel_, charModel_ )));
    }
  }

  //----------------------------------------------------------------------------------------------
  void CoalInterface::build_equation_system_coal(){

    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Vap::VapTags tagsEVap( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );
    const int nparsize = particleNumArray_.size();
    Expr::ExpressionFactory& execFactory = integrator_->factory();

    for( size_t i = 0;i < nparsize;i++){
      execFactory.register_expression( new CoalCpT( tagsEPart.partCpTags[i], tagsECoal.volMassFracTags[i], tagsECoal.charMassFracTags[i], tagsECoal.moistureMassFracTags[i],
                                                    tagsEPart.partMassFracTags[i], tagsEPart.partTempTags[i] ));

      if(reactorType_ != "PFR"){
        // mixing term in particle temperature equation
        execFactory.register_expression( new CoalTempMixRHST( tagsEPart.partTempMixRhsTags[i], tagsECoal.volMassFracTags[i], tagsECoal.charMassFracTags[i], tagsECoal.moistureMassFracTags[i],
                                                              tagsEPart.partMassFracTags[i], tagsEPart.partTempTags[i], tagsEPart.partTempInflowTags[i], tagsEPart.partCpTags[i],
                                                              tagsE.rhoTag, tagsE.rhoInflowTag, tagsE.tauMixTag));
        execFactory.register_expression( new ZeroDMixingCPT( tagsECoal.moistureMassFracMixRhsTags[i], tagsECoal.moistureMassFracInflowTags[i], tagsECoal.moistureMassFracTags[i],
                                                             tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
        execFactory.register_expression( new ZeroDMixingCPT( tagsECoal.charMassFracMixRhsTags[i], tagsECoal.charMassFracInflowTags[i], tagsECoal.charMassFracTags[i],
                                                             tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ) );
        execFactory.register_expression( new SumOpT( tagsECoal.moistureMassFracFullRhsTags[i], { tagsECoal.moistureMassFracMixRhsTags[i], tagsECoal.moistureMassFracKinRhsTags[i] } ));
        execFactory.register_expression( new SumOpT( tagsECoal.charMassFracFullRhsTags[i], {tagsECoal.charMassFracMixRhsTags[i], tagsECoal.charMassFracKinRhsTags[i]}));

        execFactory.register_expression( new PlaceHolderT( tagsECoal.moistureMassFracInflowTags[i] ));
        execFactory.register_expression( new PlaceHolderT(tagsECoal.charMassFracInflowTags[i]));
      }
      else{
        execFactory.register_expression( new DivideT( tagsECoal.moistureMassFracFullRhsTags[i], tagsECoal.moistureMassFracKinRhsTags[i], tagsEpfr.uTag ));
        execFactory.register_expression( new DivideT( tagsECoal.charMassFracFullRhsTags[i], tagsECoal.charMassFracKinRhsTags[i], tagsEpfr.uTag ));
      }
      // moisture mass equation
      integrator_->register_root_expression( new PlaceHolderT( tagsECoal.moistureMassFracInitialTags[i] ));

      execFactory.register_expression( new SourceFromGasMassT( tagsECoal.moistureMassFracKinRhsTags[i], tagsEPart.partToGasMassTag, tagsECoal.moistureMassFracTags[i]));

      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsECoal.moistureMassFracTags[i].name(), tagsECoal.moistureMassFracFullRhsTags[i], equIndex_, equIndex_ );

      // char mass equation
      integrator_->register_root_expression( new PlaceHolderT(tagsECoal.charMassFracInitialTags[i]));

      execFactory.register_expression( new SourceFromGasMassT( tagsECoal.charMassFracKinRhsTags[i], tagsEPart.partToGasMassTag, tagsECoal.charMassFracTags[i] ));

      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsECoal.charMassFracTags[i].name(), tagsECoal.charMassFracFullRhsTags[i], equIndex_, equIndex_ );

      if(devModel_ != Dev::CPDM){
        if(reactorType_ != "PFR"){
          execFactory.register_expression( new ZeroDMixingCPT( tagsECoal.volMassFracMixRhsTags[i], tagsECoal.volMassFracInflowTags[i], tagsECoal.volMassFracTags[i],
                                                               tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ) );
          execFactory.register_expression( new SumOpT( tagsECoal.volMassFracFullRhsTags[i], {tagsECoal.volMassFracMixRhsTags[i], tagsECoal.volMassFracKinRhsTags[i]} ) );
          execFactory.register_expression(( new PlaceHolderT( tagsECoal.volMassFracInflowTags[i] ) ));
        }
        else{
          execFactory.register_expression( new DivideT( tagsECoal.volMassFracFullRhsTags[i], tagsECoal.volMassFracKinRhsTags[i], tagsEpfr.uTag ));
        }
        // volatile mass equations
        execFactory.register_expression( new SourceFromGasMassT( tagsECoal.volMassFracKinRhsTags[i], tagsEPart.partToGasMassTag, tagsECoal.volMassFracTags[i] ));

        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsECoal.volMassFracTags[i].name(), tagsECoal.volMassFracFullRhsTags[i], equIndex_, equIndex_ );
      }

      integrator_->register_root_expression( new DivideT( tagsECoal.moistureMassEachPartTags[i], tagsECoal.moistureMassFracTags[i], tagsEPart.partNumFracTags[i]));
      integrator_->register_root_expression( new DivideT( tagsECoal.volMassEachPartTags[i], tagsECoal.volMassFracTags[i], tagsEPart.partNumFracTags[i]));
      integrator_->register_root_expression( new DivideT( tagsECoal.charMassEachPartTags[i], tagsECoal.charMassFracTags[i], tagsEPart.partNumFracTags[i]));
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::initialize_coal()
  {
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Vap::VapTags tagsEVap( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      if(!isRestart_){
        if(reactorType_ != "PFR"){
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsECoal.moistureMassFracInflowTags[i].name());
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsECoal.charMassFracInflowTags[i].name());
          if(devModel_ != Dev::CPDM){
            integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsECoal.volMassFracInflowTags[i].name());
          }
        }

        integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsECoal.moistureMassFracInitialTags[i].name());
        integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsECoal.charMassFracInitialTags[i].name());
      }
      if(reactorType_ != "PFR"){
        integrator_->lock_field<FieldT>( tagsECoal.moistureMassFracInflowTags[i] );
        integrator_->lock_field<FieldT>( tagsECoal.charMassFracInflowTags[i] );
        if( devModel_ != Dev::CPDM ){
          integrator_->lock_field<FieldT>( tagsECoal.volMassFracInflowTags[i] );
        }
      }
    }
    if (devModel_ != Dev::OFF){
      dev_->initialize_coal_dev();
    }
    if (charModel_ != Char::OFF){
      char_->initialize_coal_char();
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::modify_prim_kinrhs_tags_coal()
  {
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primitiveTags_.push_back( tagsECoal.moistureMassFracTags[i] );
      primitiveTags_.push_back( tagsECoal.charMassFracTags[i] );
      if(reactorType_!="PFR"){
        kinRhsTags_.push_back( tagsECoal.moistureMassFracKinRhsTags[i] );
        kinRhsTags_.push_back( tagsECoal.charMassFracKinRhsTags[i] );
      }
      else{
        kinRhsTags_.push_back( tagsECoal.moistureMassFracFullRhsTags[i] );
        kinRhsTags_.push_back( tagsECoal.charMassFracFullRhsTags[i] );
      }
      if(devModel_ != Dev::CPDM){
        primitiveTags_.push_back( tagsECoal.volMassFracTags[i] );
        if(reactorType_!="PFR"){
          kinRhsTags_.push_back( tagsECoal.volMassFracKinRhsTags[i] );
        }
        else{
          kinRhsTags_.push_back( tagsECoal.volMassFracFullRhsTags[i] );
        }
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::modify_idxmap_coal( std::map<Expr::Tag, int>& primVarIdxMap,
                                          const std::map<Expr::Tag, int>& consVarIdxMap,
                                          std::map<Expr::Tag, int>& kinRhsIdxMap,
                                          const std::map<Expr::Tag, int>& rhsIdxMap ){

    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primVarIdxMap[tagsECoal.moistureMassFracTags[i]] = consVarIdxMap.at( tagsECoal.moistureMassFracTags[i] );
      primVarIdxMap[tagsECoal.charMassFracTags[i]] = consVarIdxMap.at( tagsECoal.charMassFracTags[i] );
      if(reactorType_!="PFR"){
        kinRhsIdxMap[tagsECoal.moistureMassFracKinRhsTags[i]] = rhsIdxMap.at( tagsECoal.moistureMassFracFullRhsTags[i] );
        kinRhsIdxMap[tagsECoal.charMassFracKinRhsTags[i]] = rhsIdxMap.at( tagsECoal.charMassFracFullRhsTags[i] );
      }
      else{
        kinRhsIdxMap[tagsECoal.moistureMassFracFullRhsTags[i]] = rhsIdxMap.at( tagsECoal.moistureMassFracFullRhsTags[i] );
        kinRhsIdxMap[tagsECoal.charMassFracFullRhsTags[i]] = rhsIdxMap.at( tagsECoal.charMassFracFullRhsTags[i] );
      }
      if(devModel_ != Dev::CPDM){
        primVarIdxMap[tagsECoal.volMassFracTags[i]] = consVarIdxMap.at( tagsECoal.volMassFracTags[i] );
        if(reactorType_!="PFR"){
          kinRhsIdxMap[tagsECoal.volMassFracKinRhsTags[i]] = rhsIdxMap.at( tagsECoal.volMassFracFullRhsTags[i] );
        }
        else{
          kinRhsIdxMap[tagsECoal.volMassFracFullRhsTags[i]] = rhsIdxMap.at( tagsECoal.volMassFracFullRhsTags[i] );
        }
      }
    }

    if (devModel_ != Dev::OFF){
      dev_->modify_idxmap_dev(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap );
    }
    if (charModel_ != Char::OFF){
      char_->modify_idxmap_char(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap );
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::dvdu_coal( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                 const std::map<Expr::Tag, int> primVarIdxMap,
                                 const std::map<Expr::Tag, int> consVarIdxMap){

    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();

    for( size_t i = 0;i < nparsize;i++ ){
      dVdUPart->element<double>(primVarIdxMap.at( tagsECoal.moistureMassFracTags[i] ), consVarIdxMap.at( tagsECoal.moistureMassFracTags[i] )) = 1.0;
      dVdUPart->element<double>(primVarIdxMap.at( tagsECoal.charMassFracTags[i] ), consVarIdxMap.at( tagsECoal.charMassFracTags[i] )) = 1.0;
      if(devModel_ != Dev::CPDM){
        dVdUPart->element<double>(primVarIdxMap.at( tagsECoal.volMassFracTags[i] ), consVarIdxMap.at( tagsECoal.volMassFracTags[i] )) = 1.0;
      }
    }

    if (devModel_ != Dev::OFF){
      dev_->dvdu_coal_dev(dVdUPart, primVarIdxMap, consVarIdxMap);
    }
    if (charModel_ != Char::OFF){
      char_->dvdu_coal_char( dVdUPart, primVarIdxMap, consVarIdxMap );
    }
  }


  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::dmMixingdv_coal( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                       std::map<Expr::Tag, int> primVarIdxMap,
                                       std::map<Expr::Tag, int> rhsIdxMap,
                                       std::map<Expr::Tag, Expr::Tag> consVarRhsMap){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    const Expr::Tag&     rho             = tagsE.rhoTag;
    const Expr::TagList& moisFracPart    = tagsECoal.moistureMassFracTags;
    const Expr::TagList& mixMoisMassFrac = tagsECoal.moistureMassFracMixRhsTags;
    const Expr::TagList& charFracPart    = tagsECoal.charMassFracTags;
    const Expr::TagList& mixCharMassFrac = tagsECoal.charMassFracMixRhsTags;
    const Expr::TagList& tempPart        = tagsEPart.partTempTags;
    const Expr::TagList& mixPartTemp     = tagsEPart.partTempMixRhsTags;

    for( size_t i = 0;i < nparsize;i++ ){
      const auto& moisMassRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsECoal.moistureMassFracTags[i] ) );
      const auto& moisMassIdxi    = primVarIdxMap.at( tagsECoal.moistureMassFracTags[i] );
      const auto& charMassRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsECoal.charMassFracTags[i] ));
      const auto& charMassIdxi    = primVarIdxMap.at( tagsECoal.charMassFracTags[i] );
      const auto& tempPartRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsEPart.partTempTags[i] ) );
      const auto& tempPartIdxi    = primVarIdxMap.at( tagsEPart.partTempTags[i] );

      dmMixingdVPart->element<FieldT>( moisMassRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixMoisMassFrac[i], rho);
      dmMixingdVPart->element<FieldT>( moisMassRhsIdxi, moisMassIdxi ) = sensitivity(mixMoisMassFrac[i], moisFracPart[i]);
      dmMixingdVPart->element<FieldT>( charMassRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixCharMassFrac[i], rho);
      dmMixingdVPart->element<FieldT>( charMassRhsIdxi, charMassIdxi ) = sensitivity(mixCharMassFrac[i], charFracPart[i]);

      dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, moisMassIdxi ) = sensitivity(mixPartTemp[i], moisFracPart[i]);
      dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, charMassIdxi ) = sensitivity(mixPartTemp[i], charFracPart[i]);

      if(devModel_ != Dev::CPDM){
        const Expr::TagList& volFracPart    = tagsECoal.volMassFracTags;
        const Expr::TagList& mixVolMassFrac = tagsECoal.volMassFracMixRhsTags;
        const auto& volMassRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsECoal.volMassFracTags[i] ));
        const auto& volMassIdxi    = primVarIdxMap.at( tagsECoal.volMassFracTags[i] );
        dmMixingdVPart->element<FieldT>( volMassRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(mixVolMassFrac[i], rho);
        dmMixingdVPart->element<FieldT>( volMassRhsIdxi, volMassIdxi ) = sensitivity(mixVolMassFrac[i], volFracPart[i]);

        dmMixingdVPart->element<FieldT>( tempPartRhsIdxi, volMassIdxi ) = sensitivity(mixPartTemp[i], volFracPart[i]);
      }
    }

    if (devModel_ != Dev::OFF){
      dev_->dmMixingdv_coal_dev(dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap);
    }
    if (charModel_ != Char::OFF){
      char_->dmMixingdv_coal_char(dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap );
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CoalInterface::restart_var_coal(std::vector<std::string>& restartVars){
    if (devModel_ != Dev::OFF){
      dev_->restart_var_coal_dev(restartVars);
    }
    if (charModel_ != Char::OFF){
      char_->restart_var_coal_char(restartVars);
    }
  }

} // namespace Coal
