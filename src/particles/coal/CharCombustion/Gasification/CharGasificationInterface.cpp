
#include <stdexcept>
#include <sstream>
#include <expression/Expression.h>

#include "ReactorEnsembleUtil.h"
#include "particles/ParticleInterface.h"
#include "particles/coal/CoalInterface.h"

#include "CharGasificationInterface.h"
#include "CharGasificationBase.h"
#include "FirstOrderArrhenius/FirstOrderArrhenius.h"

namespace CharGasification {

  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef CharGasification::FirstOrderArrheniusGasif<FieldT>::Builder FirstOrderArrhT;

  CharGasificationInterface::
  CharGasificationInterface( const Coal::CoalType coalType,
                             const CharGasificationModel chargasifmodel,
                             std::vector<std::string> particleNumArray,
                             std::vector<std::string> speciesCharArray,
                             std::vector<std::string> binarySpeciesArray,
                             std::vector<std::string> partNumSpeciesCharArray,
                             const YAML::Node& rootParser,
                             Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                             const bool isRestart)
        : coaltype_( coalType ),
          chargasifmodel_( chargasifmodel ),
          particleNumArray_( particleNumArray ),
          speciesCharArray_( speciesCharArray ),
          binarySpeciesArray_( binarySpeciesArray ),
          partNumSpeciesCharArray_( partNumSpeciesCharArray ),
          rootParser_(rootParser ),
          integrator_( integrator ),
          isRestart_( isRestart ),
          charData_( coalType, rootParser["Particles"] ),
          gasificationData_( coalType, rootParser["Particles"] )
  {
    if(!isRestart_){
      setup_initial_conditions_char_gasification();
    }
    build_equation_system_coal_char_gasification();
    modify_prim_kinrhs_tags_char_gasification();
  }

  //---------------destructor---------------------------
  CharGasificationInterface::~CharGasificationInterface(){
    std::cout << "Delete ~CharGasificationInterface(): " << this << std::endl;
  }

  //------------------------------------------------------------------
  void CharGasificationInterface::setup_initial_conditions_char_gasification()
  {}

  //------------------------------------------------------------------
  void CharGasificationInterface::build_equation_system_coal_char_gasification(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Char::CharTags tagsEChar( Expr::STATE_NONE, particleNumArray_, speciesCharArray_,
                                    binarySpeciesArray_, partNumSpeciesCharArray_ );
    Expr::ExpressionFactory& execFactory = integrator_->factory();
    const int nparsize = particleNumArray_.size();

    if( chargasifmodel_ == OFF ){
      for( size_t i = 0;i < nparsize;i++ ){
        execFactory.register_expression( new ConstantT( tagsEChar.heteroH2OTags[i], 0.0 ));
        execFactory.register_expression( new ConstantT( tagsEChar.heteroCO2Tags[i], 0.0 ));
      }
    }
    if( chargasifmodel_ == FIRST_ORDER ){
      for( size_t i = 0;i < nparsize;i++ ){
        execFactory.register_expression( new FirstOrderArrhT( Expr::tag_list( tagsEChar.heteroH2OTags[i], tagsEChar.heteroCO2Tags[i] ), tagsE.massTags,
                                                              tagsEChar.binaryDiffCoeffTags, tagsEChar.effecDiffCoeffTags, tagsE.mmwTag,
                                                              tagsE.presTag, tagsE.tempTag, tagsEPart.partTempTags[i], tagsECoal.charMassFracTags[i],
                                                              tagsECoal.charMassFracInitialTags[i], tagsEPart.partSizeTags[i], tagsEPart.partNumFracTags[i],
                                                              tagsEPart.shNumberTags[i], charData_, gasificationData_ ));
      }
    }
    if( chargasifmodel_ == LH ){}
    if( chargasifmodel_ == RANDOM_PORE ){}
  }

  //---------------------------------------------------------------------------
  void CharGasificationInterface::initialize_coal_char_gasification()
  {}

  //---------------------------------------------------------------------------
  void CharGasificationInterface::modify_prim_kinrhs_tags_char_gasification()
  {}

  //---------------------------------------------------------------------------
  void CharGasificationInterface::modify_idxmap_char_gasification( std::map<Expr::Tag, int>& primVarIdxMap,
                                                                   const std::map<Expr::Tag, int>& consVarIdxMap,
                                                                   std::map<Expr::Tag, int>& kinRhsIdxMap,
                                                                   const std::map<Expr::Tag, int>& rhsIdxMap )
  {}

  //---------------------------------------------------------------------------
  void CharGasificationInterface::dvdu_coal_char_gasification( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                      std::map<Expr::Tag, int> primVarIdxMap,
                                      std::map<Expr::Tag, int> consVarIdxMap )
  {}

  //---------------------------------------------------------------------------
  void CharGasificationInterface::dmMixingdv_coal_char_gasification( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                            std::map<Expr::Tag, int> primVarIdxMap,
                                            std::map<Expr::Tag, int> rhsIdxMap,
                                            std::map<Expr::Tag, Expr::Tag> consVarRhsMap )
  {}

  //---------------------------------------------------------------------------------------------------------------
  void CharGasificationInterface::restart_var_coal_char_gasification(std::vector<std::string>& restartVars)
  {}
}
