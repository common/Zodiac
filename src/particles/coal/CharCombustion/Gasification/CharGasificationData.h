#ifndef CharGasificationData_h
#define CharGasificationData_h

/**
 * @file CharGasificationData.h
 * @par Getting required char information for char gasification
 */

#include "particles/coal/CoalData.h"

namespace CharGasification{

class CharGasificationData {
public:

  /**
   * @brief Getting required char information for char gasification based on given coal type
   * @param coalType coal type
   */
  CharGasificationData( const Coal::CoalType coalType,
                        const YAML::Node& particleParser);

  /**
   *  @brief returns \f$\a_{H2O,gasif}\f$ - pre-exponential factor for char gasification with H2O
   */
  inline const double get_a_h2o_gasif()  const{ return aH2OGasif_; }
  /**
   *  @brief returns \f$\a_{CO2,gasif}\f$ - pre-exponential factor for char gasification with CO2
   */
  inline const double get_a_co2_gasif()  const{ return aCO2Gasif_; }
  /**
   *  @brief returns \f$\a_{H2O,gasif}\f$ - activation energy for char gasification with H2O and CO2
   */
  inline const double get_ea_gasif()     const{ return eaGasif_;   }
  /**
   *  @brief returns \f$\nu_{H2O,gasif}\f$ - consumed mole of H2O per char mass from char gasification with H2O (mol H2O/g C)
   */
  inline const double get_stoicoeff_h2o_gasif()  const{ return stoicoeffH2OGasif_; }
  /**
   *  @brief returns \f$\nu_{CO2,gasif}\f$ - consumed mole of CO2 per char mass from char gasification with CO2 (mol CO2/g C)
   */
  inline const double get_stoicoeff_co2_gasif()  const{ return stoicoeffCO2Gasif_; }

protected:
  double aH2OGasif_, aCO2Gasif_, eaGasif_, stoicoeffH2OGasif_, stoicoeffCO2Gasif_;

private:
  const Coal::CoalComposition coalComp_;
};
} // namespace CharGasification

#endif /* CharGasificationData_h */
