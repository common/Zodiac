/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * @file FirstOrderArrhenius.h
 * @par Getting char gasification rate using first-oder model
 * @author Hang Zhou
 */
#ifndef FirstOrderArrheniusGasif_Expr_h
#define FirstOrderArrheniusGasif_Expr_h

#include <expression/Expression.h>
#include <particles/coal/CharCombustion/CharData.h>
#include <particles/coal/CharCombustion/Gasification/CharGasificationData.h>

/**
 *  @class  FirstOrderArrheniusGasif
 *  @ingroup CharGasification
 *  @brief Getting char gasification rates with H2O and CO2 using first-oder model (unit: 1/s)
 *  @date   Apr 2019
 *  @author Hang
 *
 *  The reaction rates are given as
 *  \f[
 *     S_{H2O, gasif} = - A_p k_{H2O} P_{H2O,s}
 *  \f]
 *  \f[
 *     S_{CO2, gasif} = - A_p k_{CO2} P_{CO2,s}
 *  \f]
 *  Here, \f$A_p = \pi d_p^2 n_p\f$ is the total particel surface area per gas mass,
 *        with \f$d_p\f$ is the diameter of particle and \f$n_p\f$ is the number of particles per gas mass.
 *        \f$k_{H2O}=A_{H2O}\exp(-E_{H2O}/(R T_p))\f$ is the rate constant for gasificaiton with H2O.
 *        \f$k_{CO2}=A_{CO2}\exp(-E_{CO2}/(R T_p))\f$ is the rate constant for gasificaiton with CO2.
 *        \f$P_{H2O,s}=\frac{h_{m,H2O}P_{H2O}}{1000 \nu_{H2O}RT_g k_{H2O}+h_{m,H2O}T_g/T_p}\f$
 *        is the particle pressure of H2O at particle surface.
 *        \f$P_{CO2,s}=\frac{h_{m,CO2}P_{CO2}}{1000 \nu_{CO2}RT_g k_{CO2}+h_{m,CO2}T_g/T_p}\f$
 *        is the particle pressure of CO2 at particle surface.
 *        \f$h_{m,H2O}=Sh D_{H2O}/d_p\f$ is the mass transfer coefficient of H2O.
 *        \f$h_{m,CO2}=Sh D_{CO2}/d_p\f$ is the mass transfer coefficient of CO2.
 *        \f$P_{H2O}=P Y_{H2O}Mw /Mw_{H2O}\f$ is the particle pressure of H2O in the bulk.
 *        \f$P_{CO2}=P Y_{CO2}Mw /Mw_{CO2}\f$ is the particle pressure of CO2 in the bulk.
 *        \f$Mw\f$ is the mixture molecular weight.
 *        \f$Sh\f$ is Sherwood number.
 *        \f$D_{H2O}\f$ and \f$D_{CO2}\f$ are the effective diffusivities of H2O and CO2.
 *        \f$Y_{H2O}\f$ and \f$Y_{CO2}\f$ are the mass fraction of H2O and CO2 in the bulk.
 *        \f$Mw_{H2O}\f$ and \f$Mw{CO2}\f$ are the molecular weight of H2O and CO2.
 *
 * Reference:
 *   Shurtz, R. C. (2011). Effects of pressure on the properties of coal char under gasification conditions at high initial heating rates.
 *   (PhD thesis). Brigham Young University. Provo, UT.
 */
namespace CharGasification{

  template< typename FieldT >
  class FirstOrderArrheniusGasif : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, charMass_, charMassInitial_,  pressure_, tempG_, tempP_, pSize_, shNum_, mmw_, pNum_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, yi_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, binaryDiffCoeff_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, effecDiffCoeff_ )

    const double gasConst_;
    const std::vector<int> indexVec_;
    const std::vector<double> mmwVec_;
    const Char::CharCombustionData& charData_;
    const CharGasification::CharGasificationData&  foaData_;
    double ea_, aH2O_, aCO2_, stoiCoeffH2O_, stoiCoeffCO2_;

    FirstOrderArrheniusGasif( const Expr::TagList& massTags,
                              const Expr::TagList& binaryDiffCoeffTags,
                              const Expr::TagList& effecDiffCoeffTags,
                              const Expr::Tag&     mmwTag,
                              const Expr::Tag&     presTag,
                              const Expr::Tag&     tempGTag,
                              const Expr::Tag&     tempPTag,
                              const Expr::Tag&     charMassTag,
                              const Expr::Tag&     charMassInitialTag,
                              const Expr::Tag&     pSizeTag,
                              const Expr::Tag&     pNumTag,
                              const Expr::Tag&     shNumTag,
                              const Char::CharCombustionData& charData,
                              const CharGasificationData&     foaData )
          : Expr::Expression<FieldT>(),
            charData_( charData ),
            foaData_ ( foaData  ),
            gasConst_( 8.3144621),
            indexVec_( charData.get_indexVec()),
            mmwVec_( charData.get_mwVec())
    {
      this->set_gpu_runnable( true );
      ea_       = foaData_.get_ea_gasif();
      aH2O_     = foaData_.get_a_h2o_gasif();
      aCO2_     = foaData_.get_a_co2_gasif();
      stoiCoeffCO2_ = foaData_.get_stoicoeff_co2_gasif();
      stoiCoeffH2O_ = foaData_.get_stoicoeff_h2o_gasif();

      this->template create_field_vector_request<FieldT>( massTags, yi_ );
      this->template create_field_vector_request<FieldT>( binaryDiffCoeffTags, binaryDiffCoeff_ );
      this->template create_field_vector_request<FieldT>( effecDiffCoeffTags, effecDiffCoeff_ );

      mmw_             = this->template create_field_request<FieldT>( mmwTag             );
      pressure_        = this->template create_field_request<FieldT>( presTag            );
      tempG_           = this->template create_field_request<FieldT>( tempGTag           );
      tempP_           = this->template create_field_request<FieldT>( tempPTag           );
      charMass_        = this->template create_field_request<FieldT>( charMassTag        );
      charMassInitial_ = this->template create_field_request<FieldT>( charMassInitialTag );
      pSize_           = this->template create_field_request<FieldT>( pSizeTag           );
      pNum_            = this->template create_field_request<FieldT>( pNumTag            );
      shNum_           = this->template create_field_request<FieldT>( shNumTag           );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::TagList massTags_, binaryDiffCoeffTags_, effecDiffCoeffTags_;
      const Expr::Tag charMassTag_, charMassInitialTag_, presTag_, pSizeTag_, tempGTag_, tempPTag_, shNumTag_, mmwTag_, pNumTag_;
      const Char::CharCombustionData& charData_;
      const CharGasification::CharGasificationData&  foaData_;

    public:
      /**
       * @brief Build a FirstOrderArrhenius expression
       * @param resultTags
       * @param massTags mass fractions of all species in gas phase
       * @param binaryDiffCoeffTags a vector of binary diffusivities (this include N2)
       * @param effecDiffCoeffTags a vector of effective diffusivities for transportation within porous media (such as char)
       * @param mmwTag mixture molecular weight
       * @param presTag gas pressure
       * @param tempGTag gas temperature
       * @param tempPTag particle temperature
       * @param charMassTag char mass per gas mass
       * @param charMassInitialTag initial char mass per gas mass
       * @param pSizeTag particle size/diameter
       * @param pNumTag number of particle per gas mass
       * @param shNumTag Sherwood number
       * @param charData Char data
       * @param foaData gasification model
       */
      Builder( const Expr::TagList& resultTags,
               const Expr::TagList& massTags,
               const Expr::TagList& binaryDiffCoeffTags,
               const Expr::TagList& effecDiffCoeffTags,
               const Expr::Tag&     mmwTag,
               const Expr::Tag&     presTag,
               const Expr::Tag&     tempGTag,
               const Expr::Tag&     tempPTag,
               const Expr::Tag&     charMassTag,
               const Expr::Tag&     charMassInitialTag,
               const Expr::Tag&     pSizeTag,
               const Expr::Tag&     pNumTag,
               const Expr::Tag&     shNumTag,
               const Char::CharCombustionData& charData,
               const CharGasification::CharGasificationData& foaData)
      : ExpressionBuilder( resultTags ),
        massTags_            ( massTags            ),
        binaryDiffCoeffTags_ ( binaryDiffCoeffTags ),
        effecDiffCoeffTags_  ( effecDiffCoeffTags  ),
        mmwTag_              ( mmwTag              ),
        presTag_             ( presTag             ),
        tempGTag_            ( tempGTag            ),
        tempPTag_            ( tempPTag            ),
        charMassTag_         ( charMassTag         ),
        charMassInitialTag_  ( charMassInitialTag  ),
        pSizeTag_            ( pSizeTag            ),
        pNumTag_             ( pNumTag             ),
        shNumTag_            ( shNumTag            ),
        charData_            ( charData            ),
        foaData_             ( foaData             )
      {}
      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new FirstOrderArrheniusGasif<FieldT>( massTags_, binaryDiffCoeffTags_, effecDiffCoeffTags_, mmwTag_, presTag_,  tempGTag_, tempPTag_, charMassTag_,
                                                     charMassInitialTag_, pSizeTag_, pNumTag_, shNumTag_, charData_, foaData_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec&  result = this->get_value_vec();

      FieldT& rH2O = *result[0];
      FieldT& rCO2 = *result[1];

      const FieldT& mmw        = mmw_      ->field_ref();
      const FieldT& press      = pressure_ ->field_ref();
      const FieldT& gTemp      = tempG_    ->field_ref();
      const FieldT& pTemp      = tempP_    ->field_ref();
      const FieldT& pSize      = pSize_    ->field_ref();
      const FieldT& pNum       = pNum_     ->field_ref();
      const FieldT& shNumb     = shNum_    ->field_ref();
      const FieldT& charMass   = charMass_ ->field_ref();
      const FieldT& charMassIn = charMassInitial_ ->field_ref();

      SpatFldPtr<FieldT> partialPresCO2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> partialPresH2O = SpatialFieldStore::get<FieldT,FieldT>( press );
      *partialPresCO2 <<= press * mmw * yi_[indexVec_[0]]->field_ref() / mmwVec_[0];
      *partialPresH2O <<= press * mmw * yi_[indexVec_[4]]->field_ref() / mmwVec_[4];

      // rate constants
      SpatFldPtr<FieldT> kH2O = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> kCO2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      *kH2O <<= aH2O_ * exp( -ea_ / (gasConst_ * pTemp));
      *kCO2 <<= aCO2_ * exp( -ea_ / (gasConst_ * pTemp));

      // mass transfer coefficients for CO2 and H2O
      SpatFldPtr<FieldT> mtcH2o = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> mtcCo2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      *mtcH2o <<= effecDiffCoeff_[4]->field_ref() * shNumb / pSize;
      *mtcCo2 <<= effecDiffCoeff_[0]->field_ref() * shNumb / pSize;

      // partial pressures of CO2 and H2O at particle surface
      SpatFldPtr<FieldT> pH2oSurf = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> pCo2Surf = SpatialFieldStore::get<FieldT,FieldT>( press );
      *pH2oSurf <<= (*mtcH2o * *partialPresH2O)
                    / ( 1000.0 * stoiCoeffH2O_*gasConst_*gTemp* *kH2O + *mtcH2o*gTemp/pTemp);
      *pCo2Surf <<= (*mtcCo2 * *partialPresCO2)
                    / ( 1000.0 * stoiCoeffCO2_*gasConst_*gTemp* *kCO2 + *mtcCo2*gTemp/pTemp);

      // exterior surface area of particle
      SpatFldPtr<FieldT> surfArea = SpatialFieldStore::get<FieldT,FieldT>( press );
      *surfArea <<= M_PI*square(pSize)*pNum;

      SpatFldPtr<FieldT> a = SpatialFieldStore::get<FieldT,FieldT>( mmw );
      SpatFldPtr<FieldT> weight = SpatialFieldStore::get<FieldT,FieldT>( mmw );

      *a <<= cond(charMassIn == 0.0, 1e-10)
                 (                   charMassIn);
      *weight <<= max(0, 1-exp(-(1/ *a)*charMass));

      rH2O <<= *weight * (-*surfArea * *kH2O * *pH2oSurf); // negative
      rCO2 <<= *weight * (-*surfArea * *kCO2 * *pCo2Surf); // negative
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      typename Expr::Expression<FieldT>::ValVec&  result = this->get_value_vec();

      std::vector<FieldT > drhsdv;
      for(size_t i=0;i<targetTags.size();++i){
        drhsdv.push_back(this->sensitivity_result(targetTags[i], var));
      }
      const FieldT& mmw        = mmw_      ->field_ref();
      const FieldT& press      = pressure_ ->field_ref();
      const FieldT& gTemp      = tempG_    ->field_ref();
      const FieldT& pTemp      = tempP_    ->field_ref();
      const FieldT& pSize      = pSize_    ->field_ref();
      const FieldT& pNum       = pNum_     ->field_ref();
      const FieldT& shNumb     = shNum_    ->field_ref();
      const FieldT& charMass   = charMass_ ->field_ref();
      const FieldT& charMassIn = charMassInitial_ ->field_ref();

      const FieldT& dmmwdv      = mmw_      ->sens_field_ref(var);
      const FieldT& dpressdv    = pressure_ ->sens_field_ref(var);
      const FieldT& dgTempdv    = tempG_    ->sens_field_ref(var);
      const FieldT& dpTempdv    = tempP_    ->sens_field_ref(var);
      const FieldT& dpSizedv    = pSize_    ->sens_field_ref(var);
      const FieldT& dpNumdv     = pNum_     ->sens_field_ref(var);
      const FieldT& dshNumbdv   = shNum_    ->sens_field_ref(var);
      const FieldT& dcharMassdv = charMass_ ->sens_field_ref(var);

      SpatFldPtr<FieldT> partialPresH2O = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dpartialPresdvH2O = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> partialPresCO2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dpartialPresdvCO2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      *partialPresCO2 <<= press * mmw * yi_[indexVec_[0]]->field_ref() / mmwVec_[0];
      *partialPresH2O <<= press * mmw * yi_[indexVec_[4]]->field_ref() / mmwVec_[4];
      *dpartialPresdvCO2 <<= (press * mmw * yi_[indexVec_[0]]->sens_field_ref(var)
                             + dpressdv * mmw * yi_[indexVec_[0]]->field_ref()
                             + press * dmmwdv * yi_[indexVec_[0]]->field_ref() ) / mmwVec_[0];
      *dpartialPresdvH2O <<= (press * mmw * yi_[indexVec_[4]]->sens_field_ref(var)
                             + dpressdv * mmw * yi_[indexVec_[4]]->field_ref()
                             + press * dmmwdv * yi_[indexVec_[4]]->field_ref()) / mmwVec_[4];

      // rate constants
      SpatFldPtr<FieldT> kH2O = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> kCO2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dkH2Odv = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dkCO2dv = SpatialFieldStore::get<FieldT,FieldT>( press );
      *kH2O <<= aH2O_ * exp( -ea_ / (gasConst_ * pTemp));
      *kCO2 <<= aCO2_ * exp( -ea_ / (gasConst_ * pTemp));
      *dkH2Odv <<= aH2O_ * exp( -ea_ / (gasConst_ * pTemp)) * ea_/(gasConst_ * square(pTemp)) * dpTempdv;
      *dkCO2dv <<= aCO2_ * exp( -ea_ / (gasConst_ * pTemp)) * ea_/(gasConst_ * square(pTemp)) * dpTempdv;

      // mass transfer coefficients for CO2 and H2O
      SpatFldPtr<FieldT> mtcH2o = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> mtcCo2 = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dmtcH2odv = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dmtcCo2dv = SpatialFieldStore::get<FieldT,FieldT>( press );
      *mtcH2o <<= effecDiffCoeff_[4]->field_ref() * shNumb / pSize;
      *mtcCo2 <<= effecDiffCoeff_[0]->field_ref() * shNumb / pSize;
      *dmtcH2odv <<= (effecDiffCoeff_[4]->sens_field_ref(var)*shNumb + effecDiffCoeff_[4]->field_ref()*dshNumbdv) / pSize
                     - effecDiffCoeff_[4]->field_ref() * shNumb / square(pSize) *dpSizedv;
      *dmtcCo2dv <<= (effecDiffCoeff_[0]->sens_field_ref(var)*shNumb + effecDiffCoeff_[0]->field_ref()*dshNumbdv) / pSize
                     - effecDiffCoeff_[0]->field_ref() * shNumb / square(pSize) *dpSizedv;

      // partial pressures of CO2 and H2O at particle surface
      SpatFldPtr<FieldT> pH2oSurf = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> pCo2Surf = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dpH2oSurfdv = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dpCo2Surfdv = SpatialFieldStore::get<FieldT,FieldT>( press );
      *pH2oSurf <<= (*mtcH2o * *partialPresH2O)
                    / ( 1000.0 * stoiCoeffH2O_*gasConst_*gTemp* *kH2O + *mtcH2o*gTemp/pTemp);
      *pCo2Surf <<= (*mtcCo2 * *partialPresCO2)
                    / ( 1000.0 * stoiCoeffCO2_*gasConst_*gTemp* *kCO2 + *mtcCo2*gTemp/pTemp);

      *dpH2oSurfdv <<= (*dmtcH2odv * *partialPresH2O + *mtcH2o* *dpartialPresdvH2O)
                         / ( 1000.0 * stoiCoeffH2O_*gasConst_*gTemp* *kH2O + *mtcH2o*gTemp/pTemp)
                       -  (*mtcH2o * *partialPresH2O) / square( 1000.0 * stoiCoeffH2O_*gasConst_*gTemp* *kH2O + *mtcH2o*gTemp/pTemp)
                         * (1000.0 * stoiCoeffH2O_*gasConst_*(dgTempdv* *kH2O + gTemp * *dkH2Odv)
                            + (*dmtcH2odv*gTemp+*mtcH2o*dgTempdv)/pTemp - *mtcH2o*gTemp/square(pTemp)*dpTempdv);

      *dpCo2Surfdv <<= (*dmtcCo2dv * *partialPresCO2 + *mtcCo2* *dpartialPresdvCO2)
                       / ( 1000.0 * stoiCoeffCO2_*gasConst_*gTemp* *kCO2 + *mtcCo2*gTemp/pTemp)
                       -  (*mtcCo2 * *partialPresCO2) / square( 1000.0 * stoiCoeffCO2_*gasConst_*gTemp* *kCO2 + *mtcCo2*gTemp/pTemp)
                          * (1000.0 * stoiCoeffCO2_*gasConst_*(dgTempdv* *kCO2 + gTemp * *dkCO2dv)
                             + (*dmtcCo2dv*gTemp+*mtcCo2*dgTempdv)/pTemp - *mtcCo2*gTemp/square(pTemp)*dpTempdv);

      // exterior surface area of particle
      SpatFldPtr<FieldT> surfArea = SpatialFieldStore::get<FieldT,FieldT>( press );
      SpatFldPtr<FieldT> dsurfAreadv = SpatialFieldStore::get<FieldT,FieldT>( press );
      *surfArea <<= M_PI*square(pSize)*pNum;
      *dsurfAreadv <<= 2*M_PI*pSize*dpSizedv*pNum + M_PI*square(pSize)*dpNumdv;

      SpatFldPtr<FieldT> a = SpatialFieldStore::get<FieldT>( mmw );
      SpatFldPtr<FieldT> weight = SpatialFieldStore::get<FieldT,FieldT>( mmw );
      SpatFldPtr<FieldT> dweightdv = SpatialFieldStore::get<FieldT,FieldT>( mmw );

      *a <<= cond(charMassIn == 0.0, 1e-10)
            (                   charMassIn);
      *weight <<= max(0, 1-exp(-(1/ *a)*charMass));
      *dweightdv <<= cond( charMass<=0.0, 0.0)
                         (                (1/ *a) * exp(-(1/ *a)*charMass) * dcharMassdv);

      drhsdv[0] <<= *weight * (-*surfArea * (*dkH2Odv * *pH2oSurf+*kH2O* *dpH2oSurfdv)-*dsurfAreadv*(*kH2O* *pH2oSurf))
                    + *dweightdv * (-*surfArea * *kH2O * *pH2oSurf);
      drhsdv[1] <<= *weight * (-*surfArea * (*dkCO2dv * *pCo2Surf+*kCO2* *dpCo2Surfdv)-*dsurfAreadv*(*kCO2* *pCo2Surf))
                    + *dweightdv * (-*surfArea * *kCO2 * *pCo2Surf);
    }

  };


//--------------------------------------------------------------------

}// namespace CharGasification

#endif // FirstOrderArrheniusGasif_Expr_h
