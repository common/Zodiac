#ifndef GasificationCO2RandomPore_h
#define GGasificationCO2RandomPore_h

#include <expression/Expression.h>

/**
 *  \class GasificationCO2
 */
namespace CharGasification{

  template< typename FieldT >
  class GasificationCO2RandomPore
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, mchar_, prtdim_, tempp_, massfrcco2_, gaspress_, mmw_ )
    const double R_, A1_, A2_, E1_, E2_, n1_, n2_;

    GasificationCO2RandomPore( const Expr::Tag& mcharTag,
                               const Expr::Tag& prtdimTag,
                               const Expr::Tag& temppTag,
                               const Expr::Tag& massfrcco2Tag,
                               const Expr::Tag& gaspressTag,
                               const Expr::Tag& mmwTag)
          : Expr::Expression<FieldT>(),
            R_ ( 8.314 ),
            A1_( 3.34E8 ),
            A2_( 6.78E4 ),
            E1_( 2.71E5 ), // j/mol
            E2_( 1.63E5 ), // j/mol
            n1_( 0.54   ),
            n2_( 0.73   )
    {
      this->set_gpu_runnable(true);

      mchar_      = this->template create_field_request<FieldT>( mcharTag      );
      prtdim_     = this->template create_field_request<FieldT>( prtdimTag     );
      tempp_      = this->template create_field_request<FieldT>( temppTag      );
      massfrcco2_ = this->template create_field_request<FieldT>( massfrcco2Tag );
      gaspress_   = this->template create_field_request<FieldT>( gaspressTag   );
      mmw_        = this->template create_field_request<FieldT>( mmwTag        );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag mcharTag_, prtdimTag_, temppTag_, massfrcco2Tag_, gaspressTag_, mmwTag_;
    public:
      Builder(const Expr::Tag& gasifCO2Tag,
              const Expr::Tag& mcharTag,
              const Expr::Tag& prtdimTag,
              const Expr::Tag& temppTag,
              const Expr::Tag& massfrcco2Tag,
              const Expr::Tag& gaspressTag,
              const Expr::Tag& mmwTag)
            : ExpressionBuilder(gasifCO2Tag),
              mcharTag_     ( mcharTag     ),
              prtdimTag_    ( prtdimTag    ),
              temppTag_     ( temppTag     ),
              massfrcco2Tag_( massfrcco2Tag),
              gaspressTag_  ( gaspressTag  ),
              mmwTag_       ( mmwTag       )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new GasificationCO2RandomPore( mcharTag_, prtdimTag_, temppTag_, massfrcco2Tag_, gaspressTag_, mmwTag_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;

      FieldT& result = this->value();

      const FieldT& mchar      = mchar_     ->field_ref();
      const FieldT& prtdim     = prtdim_    ->field_ref();
      const FieldT& tempp      = tempp_     ->field_ref();
      const FieldT& massfrcco2 = massfrcco2_->field_ref();
      const FieldT& gaspress   = gaspress_  ->field_ref();
      const FieldT& mmw        = mmw_       ->field_ref();

      /*
       * Rate of char gasification by species i calculated as follows:
       *
       * r_i = -k * charMass,
       * k   = A * (P_i)^n * exp(-E/RT),
       * P_i = x_i * P,
       *
       * where P_i is the partial pressure of species i, x_i is the
       * mole fraction of species i,
       *
       * The rate equation form is taken from [1], k is taken from [2].
       */

      result <<= cond( mchar <= 0.0 || massfrcco2 <= 0.0, 0.0 )
                     ( tempp < 1473.0 , - mchar * A1_*exp(-E1_/(R_ * tempp))* pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n1_) )
                     (                 - mchar * A2_*exp(-E2_/(R_ * tempp))* pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n2_) );
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      FieldT& dfdv = this->sensitivity_result( sensVarTag );

      const FieldT& mchar      = mchar_     ->field_ref();
      const FieldT& prtdim     = prtdim_    ->field_ref();
      const FieldT& tempp      = tempp_     ->field_ref();
      const FieldT& massfrcco2 = massfrcco2_->field_ref();
      const FieldT& gaspress   = gaspress_  ->field_ref();
      const FieldT& mmw        = mmw_       ->field_ref();

      const FieldT& dmchardv      = mchar_     ->sens_field_ref(sensVarTag);
      const FieldT& dprtdimdv     = prtdim_    ->sens_field_ref(sensVarTag);
      const FieldT& dtemppdv      = tempp_     ->sens_field_ref(sensVarTag);
      const FieldT& dmassfrcco2dv = massfrcco2_->sens_field_ref(sensVarTag);
      const FieldT& dgaspressdv   = gaspress_  ->sens_field_ref(sensVarTag);
      const FieldT& dmmwdv        = mmw_       ->sens_field_ref(sensVarTag);


      dfdv <<= cond( mchar <= 0.0 || massfrcco2 <= 0.0 , 0.0)
                   ( tempp < 1473.0 ,  - dmchardv * A1_*exp(-E1_/(R_ * tempp)) * pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n1_)
                                       - mchar * A1_*exp(-E1_/(R_ * tempp))* E1_/R_/tempp/tempp*dtemppdv *pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n1_)
                                       - mchar * A1_*exp(-E1_/(R_ * tempp))* pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n1_-1)
                                       * (dmassfrcco2dv*mmw*gaspress+massfrcco2*dmmwdv*gaspress+massfrcco2*mmw*dgaspressdv) *1e-6/44.0)
                   (                   - dmchardv * A2_*exp(-E2_/(R_ * tempp)) * pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n2_)
                                       - mchar * A2_*exp(-E2_/(R_ * tempp))* E2_/R_/tempp/tempp*dtemppdv * pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n2_)
                                       - mchar * A2_*exp(-E2_/(R_ * tempp))* pow(massfrcco2 * mmw * gaspress*1e-6/44.0, n2_-1)
                                       * (dmassfrcco2dv*mmw*gaspress+massfrcco2*dmmwdv*gaspress+massfrcco2*mmw*dgaspressdv) *1e-6/44.0);
    }
  };

}  // namesspace CharGasification

/* [1] Xiaofang Wang, X, B Jin, and W Zhong. Three-dimensional simulation of fluidized bed coal gasification.
      Chemical Engineering and Processing: Process Intensification 48, no. 2 (February 2009): 695-705.
         http://linkinghub.elsevier.com/retrieve/pii/S0255270108001803.

   [2]Watanabe, H, and M Otaka. Numerical simulation of coal gasification in entrained flow coal gasifier.Fuel 85,
       no. 12-13 (September 2006): 1935-1943.
      http://linkinghub.elsevier.com/retrieve/pii/S0016236106000548.
*/

#endif // GasificationCO2RandomPore_h
