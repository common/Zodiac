#ifndef GasificationH2ORandomPore_h
#define GasificationH2ORandomPore_h

#include <expression/Expression.h>

/**
 *  \class GasificationH2O
 *
 *  Reaction: \f$C_\mathrm{(char)} + H_{2}O \rightarrow CO + H_{2}\f$
 *
 *  \par References:
 *
 * [1] Xiaofang Wang, X, B Jin, and W Zhong. Three-dimensional simulation of fluidized bed coal gasification.
 *    Chemical Engineering and Processing: Process Intensification 48, no. 2 (February 2009): 695-705.
 *       http://linkinghub.elsevier.com/retrieve/pii/S0255270108001803.
 *
 * [2] Watanabe, H, and M Otaka. Numerical simulation of coal gasification in entrained flow coal gasifier.Fuel 85,
 *     no. 12-13 (September 2006): 1935-1943.
 *    http://linkinghub.elsevier.com/retrieve/pii/S0016236106000548.
 *    http://www.sciencedirect.com/science/article/pii/S0016236106000548
 *
 */
namespace CharGasification{

  template< typename FieldT >
  class GasificationH2ORandomPore
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, mchar_, prtdim_, tempp_, massfrch2o_, gaspress_, mmw_ )
    const double R_, A1_, A2_, E1_, E2_, n1_, n2_;

    GasificationH2ORandomPore( const Expr::Tag& mcharTag,
                               const Expr::Tag& prtdimTag,
                               const Expr::Tag& temppTag,
                               const Expr::Tag& massfrch2oTag,
                               const Expr::Tag& gaspressTag,
                               const Expr::Tag& mmwTag)
          : Expr::Expression<FieldT>(),
            R_ ( 8.314 ),
            A1_( 2.89E8 ),
            A2_( 8.55E4 ), // see ref [2] below
            E1_( 2.52E5 ), // j/mol
            E2_( 1.40E5 ), // j/mol
            n1_( 0.64   ),
            n2_( 0.84   )
    {
      this->set_gpu_runnable(true);

      mchar_      = this->template create_field_request<FieldT>( mcharTag      );
      prtdim_     = this->template create_field_request<FieldT>( prtdimTag     );
      tempp_      = this->template create_field_request<FieldT>( temppTag      );
      massfrch2o_ = this->template create_field_request<FieldT>( massfrch2oTag );
      gaspress_   = this->template create_field_request<FieldT>( gaspressTag   );
      mmw_        = this->template create_field_request<FieldT>( mmwTag        );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag mcharTag_, prtdimTag_, temppTag_, massfrch2oTag_, gaspressTag_, mmwTag_;
    public:
      Builder( const Expr::Tag& gasifH2OTag,
               const Expr::Tag& mcharTag,
               const Expr::Tag& prtdimTag,
               const Expr::Tag& temppTag,
               const Expr::Tag& massfrch2oTag,
               const Expr::Tag& gaspressTag,
               const Expr::Tag& mmwTag)
        :ExpressionBuilder(gasifH2OTag),
        mcharTag_(mcharTag),
        prtdimTag_(prtdimTag),
        temppTag_(temppTag),
        massfrch2oTag_(massfrch2oTag),
        gaspressTag_(gaspressTag),
        mmwTag_(mmwTag)
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new GasificationH2ORandomPore( mcharTag_, prtdimTag_, temppTag_, massfrch2oTag_, gaspressTag_, mmwTag_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();
      const FieldT& mchar      = mchar_     ->field_ref();
      const FieldT& prtdim     = prtdim_    ->field_ref();
      const FieldT& tempp      = tempp_     ->field_ref();
      const FieldT& massfrch2o = massfrch2o_->field_ref();
      const FieldT& gaspress   = gaspress_  ->field_ref();
      const FieldT& mmw        = mmw_       ->field_ref();

      /*
       * Rate of char gasification by species i calculated as follows:
       *
       * r_i = -k * charMass,
       * k   = A * (P_i)^n * exp(-E/RT),
       * P_i = x_i * P,
       *
       * where P_i is the partial pressure of species i, x_i is the
       * mole fraction of species i.
       */

      result <<= cond( mchar <= 0.0 || massfrch2o <= 0.0, 0.0 )
            ( tempp < 1533.0, - mchar * A1_*exp(-E1_/(R_ * tempp))* pow(massfrch2o * mmw * gaspress*1e-6/18.0, n1_) )
            (                 - mchar * A2_*exp(-E2_/(R_ * tempp))* pow(massfrch2o * mmw * gaspress*1e-6/18.0, n2_) );
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      FieldT& dfdv = this->sensitivity_result( sensVarTag );

      const FieldT& mchar      = mchar_     ->field_ref();
      const FieldT& prtdim     = prtdim_    ->field_ref();
      const FieldT& tempp      = tempp_     ->field_ref();
      const FieldT& massfrch2o = massfrch2o_->field_ref();
      const FieldT& gaspress   = gaspress_  ->field_ref();
      const FieldT& mmw        = mmw_       ->field_ref();

      const FieldT& dmchardv      = mchar_     ->sens_field_ref(sensVarTag);
      const FieldT& dprtdimdv     = prtdim_    ->sens_field_ref(sensVarTag);
      const FieldT& dtemppdv      = tempp_     ->sens_field_ref(sensVarTag);
      const FieldT& dmassfrch2odv = massfrch2o_->sens_field_ref(sensVarTag);
      const FieldT& dgaspressdv   = gaspress_  ->sens_field_ref(sensVarTag);
      const FieldT& dmmwdv        = mmw_       ->sens_field_ref(sensVarTag);


      dfdv <<= cond( mchar <= 0.0 || massfrch2o <= 0.0, 0.0)
                   ( tempp < 1533.0,  - dmchardv * A1_*exp(-E1_/(R_ * tempp)) * pow(massfrch2o * mmw * gaspress*1e-6/18.0, n1_)
                                      - mchar * A1_*exp(-E1_/(R_ * tempp))* E1_/R_/tempp/tempp*dtemppdv * pow(massfrch2o * mmw * gaspress*1e-6/18.0, n1_)
                                      - mchar * A1_*exp(-E1_/(R_ * tempp))* pow(massfrch2o * mmw * gaspress*1e-6/18.0, n1_-1)
                                      * (dmassfrch2odv*mmw*gaspress+massfrch2o*dmmwdv*gaspress+massfrch2o*mmw*dgaspressdv) *1e-6)
            (                        - dmchardv * A2_*exp(-E2_/(R_ * tempp)) * pow(massfrch2o * mmw * gaspress*1e-6/18.0, n2_)
                                      - mchar * A2_*exp(-E2_/(R_ * tempp))* E2_/R_/tempp/tempp*dtemppdv * pow(massfrch2o * mmw * gaspress*1e-6/18.0, n2_)
                                      - mchar * A2_*exp(-E2_/(R_ * tempp))* pow(massfrch2o * mmw * gaspress*1e-6/18.0, n2_-1)
                                      * (dmassfrch2odv*mmw*gaspress+massfrch2o*dmmwdv*gaspress+massfrch2o*mmw*dgaspressdv) *1e-6);

    }
};

}  // namesspace CharGasification


#endif // GasificationH2ORandomPore_h
