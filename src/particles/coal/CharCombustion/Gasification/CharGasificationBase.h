
#ifndef CharGasificationBase_h
#define CharGasificationBase_h

#include <stdexcept>
#include <sstream>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace CharGasification{

  enum CharGasificationModel{
    LH,
    RANDOM_PORE,
    FIRST_ORDER,
    INVALID_CHARGASIFICATIONMODEL,
    OFF
  };

  /**
   * @fn std::string char_gasification_model_name( const CharGasificationModel model )
   * @param model: char gasification model
   * @return the string name of the model
   */
  std::string char_gasification_model_name( const CharGasificationModel model );

  /**
   * @fn CharModel char_gasification_model( const std::string& modelName )
   * @param modelName: the string name of the char gasification model
   * @return the corresponding enum value
   */
  CharGasificationModel char_gasification_model( const std::string& modelName );

} // namespace CharGasification

#endif /* CharGasificationBase_h */
