#include "CharGasificationBase.h"

#include <stdexcept>
#include <sstream>

namespace CharGasification{

  std::string char_gasification_model_name( const CharGasificationModel model )
  {
    std::string name;
    switch (model){
      case LH                           : name = "LH";          break;
      case RANDOM_PORE                  : name = "RandomPore";  break;
      case FIRST_ORDER                  : name = "FirstOrder";  break;
      case INVALID_CHARGASIFICATIONMODEL: name = "INVALID";     break;
      case OFF                          : name = "OFF";         break;
    }
    return name;
  }

  CharGasificationModel char_gasification_model( const std::string& name )
  {
    if     ( name == char_gasification_model_name(LH         ) ) return LH;
    else if( name == char_gasification_model_name(RANDOM_PORE) ) return RANDOM_PORE;
    else if( name == char_gasification_model_name(FIRST_ORDER) ) return FIRST_ORDER;
    else if( name == char_gasification_model_name(OFF        ) ) return OFF;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported char gasification model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << char_gasification_model_name( LH          )
          << "\n\t" << char_gasification_model_name( RANDOM_PORE )
          << "\n\t" << char_gasification_model_name( FIRST_ORDER )
          << "\n\t" << char_gasification_model_name( OFF         )
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_CHARGASIFICATIONMODEL;
  }

}
