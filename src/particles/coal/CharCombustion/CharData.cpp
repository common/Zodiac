#include <stdexcept>
#include <sstream>

#include "CharData.h"

#include <cantera/IdealGasMix.h>
#include <pokitt/CanteraObjects.h> // include cantera wrapper

#include <math.h>

namespace Char{

//--------------------------------------------------------------------

CharCombustionData::
CharCombustionData( const Coal::CoalType sel,
                    const YAML::Node& particleParser)
 : sel_    ( sel ),
  coalComp_( sel, particleParser )
{
  // I have call this class for two reason :
  // 1- Extract O  mass fraction from parent coal
  // 2- In case if I want to close the H balance over the coal particle I can use it.
  o_           = coalComp_.get_O();
  c_           = coalComp_.get_C();
  fixedCarbon_ = coalComp_.get_fixed_c();
  volatiles_   = coalComp_.get_vm();
  set_data();

  mwVec_.clear();
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("CO2")]);
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("CO")]);
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("O2")]);
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("H2")]);
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("H2O")]);
  mwVec_.push_back(CanteraObjects::molecular_weights()[CanteraObjects::species_index("CH4")]);

  indexVec_.clear();
  indexVec_.push_back(CanteraObjects::species_index("CO2"));
  indexVec_.push_back(CanteraObjects::species_index("CO"));
  indexVec_.push_back(CanteraObjects::species_index("O2"));
  indexVec_.push_back(CanteraObjects::species_index("H2"));
  indexVec_.push_back(CanteraObjects::species_index("H2O"));
  indexVec_.push_back(CanteraObjects::species_index("CH4"));
}

//--------------------------------------------------------------------
// =====>>>>>  I have to ask Prof Smith about T_ciritical of coal. !!!!!!    <<<<<<<=====
void
CharCombustionData::
set_data()
{
  s0_    = 300E3; // m2/kg
  e0_    = 0.47;
  rPore_ = 6E-10; // Angstrom

  switch (sel_) {
    case Coal::NorthDakota_Lignite:
    case Coal::Gillette_Subbituminous:
    case Coal::MontanaRosebud_Subbituminous:
    case Coal::Illinois_Bituminous:
    case Coal::Kentucky_Bituminous:
    case Coal::Pittsburgh_Shaddix:
    case Coal::Black_Thunder:
    case Coal::Shenmu:
    case Coal::Guizhou:
    case Coal::Russian_Bituminous:
    case Coal::Utah_Bituminous:
    case Coal::Utah_Skyline:
    case Coal::Utah_Skyline_Char_10atm:
    case Coal::Utah_Skyline_Char_12_5atm:
    case Coal::Utah_Skyline_Char_15atm:
    case Coal::Highvale:
    case Coal::Highvale_Char:
    case Coal::Eastern_Bituminous:
    case Coal::Eastern_Bituminous_Char:
    case Coal::Illinois_No_6:
    case Coal::Illinois_No_6_Char:
    case Coal::Given_From_Input_File:

      insufficient_data();
      break;

    case Coal::Pittsburgh_Bituminous:
      e0_ = 0.1;
      rPore_ = 6E-10; // Angstrom
      break;

    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported coal type" << std::endl
          << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------
// insufficient data for char oxidation calculation
void
CharCombustionData::
insufficient_data()
{
  std::cout << " ------------------------------------------------------------------------ \n"
      << " WARNING:\n"
      << "  For more accurate calculation in Char oxidation model, this coal type   \n"
      << "  requires the following values to be specified in CharData.cpp:          \n"
      << "   - initial coal porosity                      (default = " << e0_ << ")\n"
      << "   - mean pore radius (m)                       (default = " << rPore_ << ")\n"
      << "   - initial internal char surface area (m2/kg) (default = " << s0_ << ")\n"
      << " ------------------------------------------------------------------------ \n";
}
//--------------------------------------------------------------------

// Initial Particle density, if the size the particle remains constant

  double initial_particle_density(double mass, double diameter){
    return mass/(pow(diameter, 3.0)*3.141592653589793/6);
  }


//--------------------------------------------------------------------


} // namespace char

/*
[1] Lewis et. al. Pulverized Steam Gasification Rates of Three Bituminous Coal Chars in an Entrained-Flow
    Reactor at Pressurized Conditions. Energy and Fuels. 2015, 29, 1479-1493.
    http://pubs.acs.org/doi/abs/10.1021/ef502608y
*/
