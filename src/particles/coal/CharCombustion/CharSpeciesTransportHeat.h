
/**
 * @file CharSpeciesTransportHeat.h
 * @par Calculate the heat release to gas phase
 *      resulting from species transportation reacted in char model
 * @author Hang Zhou
 */

#ifndef CharSpeciesTransportHeat_CHAR_h
#define CharSpeciesTransportHeat_CHAR_h

#include <expression/Expression.h>
#include <pokitt/CanteraObjects.h>
#include "particles/coal/Devolatilization/DevolatilizationBase.h"

namespace Char {

  /**
   *  @class CharSpeciesTransportHeat
   *  @ingroup Char
   *  @brief Calculate heat release to gas phase because the species transportation between gas and particle phase in char model
   *         (unit: J/kg/s)
   *
   *  The heat release from species transportation consists of two part:
   *  <ul>
   *      <li> Gas transported from gas to particle
   *           \f[
   *             Q_{1} = S_{ox, O_2} h_{O_2, T_g}
   *                    + S_{gasif, CO_2}\frac{Mw_{CO_2}}{Mw_C} h_{CO_2, T_g}
   *                    + S_{gasif, H_2O}\frac{Mw_{H_2O}}{Mw_C} h_{H_2O, T_g}
   *          \f]
   *      </li>
   *      <li> Gas transported from particle to gas
   *          \f[
   *             Q_{2} = S_{ox, CO_2} h_{CO_2, T_p}
   *                    + S_{ox, CO} h_{CO, T_p}
   *                    - S_{gasif, CO_2}\frac{2 Mw_{CO}}{Mw_C} h_{CO, T_p}
   *                    - S_{gasif, H_2O}\frac{Mw_{CO}}{Mw_C} h_{CO, T_p}
   *                    - S_{gasif, H_2O}\frac{Mw_{H_2}}{Mw_C} h_{H_2, T_p}
   *          \f]
   *      </li>
   *  </ul>
   *  The final source term is given:
   *  \f[ S = Q_1 + Q_2 \f]
   *
   *  Here, \f$S_{ox, O_2}\f$, \f$S_{ox, CO_2}\f$ and \f$S_{ox, CO}\f$ are the reaction rate of O2, CO2 and CO in char oxidation respectively.
   *        \f$S_{gasif, CO_2}\f$ and \f$S_{gasif, H_2O}\f$ are the char gasification rates with CO2 and H2O respectively.
   *        \f$h_{O_2, T_g} \f$, \f$h_{CO_2, T_g} \f$ and \f$h_{H_2O, T_g}\f$ are the specifice enthalpy
   *        of O2, CO2 and H2O under gas temperature respectively.
   *        \f$h_{CO_2, T_p}\f$, \f$h_{CO, T_p} \f$ and \f$h_{H_2, T_p}\f$ are the specifice enthalpy
   *        of CO2, CO and H2 under particle temperature respectively.
   *        \f$Mw_{CO_2}\f$, \f$Mw_{H_2O}\f$, \f$Mw_C\f$, \f$Mw_{CO}\f$ and \f$Mw_{H_2}\f$ are the molecular weights
   *        of CO2, H2O, C, CO and H2 respectively.
   *  \f[
   *     S_{ox, CO_2} = - S_{ox} \frac{CO2/CO}{1+CO2/CO}\frac{Mw_{CO_2}}{Mw_C}
   *  \f]
   *  \f[
   *     S_{ox, CO} = - S_{ox} \frac{1}{1+CO2/CO}\frac{Mw_{CO}}{Mw_C}
   *  \f]
   *  Here, \f$S_{ox}\f$ is the oxidation rate of char.
   *        \f$CO2/CO\f$ is the production rate ratio between CO2 and CO from char oxidation.
   *
   *  NOTE: 1. This expression calculates the heat release from all particles with ONE specific particle size.
   *           All the parameters used here are the value for ONE specific particle size.
   *        2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
   */
  template< typename FieldT >
  class CharSpeciesTransportHeat
   : public Expr::Expression<FieldT>
  {
      DECLARE_FIELDS( FieldT, charoxidation_, charo2rhs_, co2coratio_, hetroco2_, hetroh2o_ )
      DECLARE_VECTOR_OF_FIELDS( FieldT, enthTparts_)
      DECLARE_VECTOR_OF_FIELDS( FieldT, enthTgass_)
      const int nspec_, partIdx_;
      const double mwco_, mwco2_, mwh2o_, mwchar_, mwh2_;

      CharSpeciesTransportHeat( const Expr::TagList& enthTpartTags,
                                const Expr::TagList& enthTgasTags,
                                const Expr::Tag& charoxidationTag,
                                const Expr::Tag& charO2rhsTag,
                                const Expr::Tag& co2coratioTag,
                                const Expr::Tag& hetroco2Tag,
                                const Expr::Tag& hetroh2oTag,
                                const int nspec,
                                const int partIdx)
                : Expr::Expression<FieldT>(),
                  nspec_(nspec),
                  partIdx_(partIdx),
                  mwco_(28.0),
                  mwco2_(44.0),
                  mwh2o_(18.0),
                  mwchar_(12.0),
                  mwh2_(2.0)
    {
      this->set_gpu_runnable(true);
      this->template create_field_vector_request<FieldT> ( enthTpartTags, enthTparts_  );
      this->template create_field_vector_request<FieldT> ( enthTgasTags,  enthTgass_   );
      charoxidation_ = this->template create_field_request<FieldT>( charoxidationTag);
      charo2rhs_     = this->template create_field_request<FieldT>( charO2rhsTag    );
      co2coratio_    = this->template create_field_request<FieldT>( co2coratioTag   ),
      hetroco2_      = this->template create_field_request<FieldT>( hetroco2Tag     );
      hetroh2o_      = this->template create_field_request<FieldT>( hetroh2oTag     );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag charoxidationTag_, charo2rhsTag_, co2coratioTag_, hetroco2Tag_, hetroh2oTag_;
        const Expr::TagList enthTpartTags_, enthTgasTags_;
        const int nspec_, partIdx_;
    public:
      /**
       * @brief The mechanism for building a CharSpeciesTransportHeat object
       * @tparam FieldT
       * @param charSpecTransHeatTag heat release to gas phase from species transportation in char model
       * @param enthTpartTags species specific enthalpy under particle temperature
       * @param enthTgasTags species specific enthalpy under gas temperature
       * @param charoxidationTag char oxidation rate
       * @param charo2rhsTag O2 consumption rate in char model
       * @param co2coratioTag production ratio of CO2 and CO from char oxidation
       * @param hetroco2Tag gasification rate of char with CO2
       * @param hetroh2oTag gasification rate of char with H2O
       * @param nspec number of species involved in char model
       * @param partIdx particle index in list of particle number with different sizes
       */
      Builder( const Expr::Tag& charSpecTransHeatTag,
               const Expr::TagList& enthTpartTags,
               const Expr::TagList& enthTgasTags,
               const Expr::Tag& charoxidationTag,
               const Expr::Tag& charo2rhsTag,
               const Expr::Tag& co2coratioTag,
               const Expr::Tag& hetroco2Tag,
               const Expr::Tag& hetroh2oTag,
               const int nspec,
               const int partIdx)
            : ExpressionBuilder(charSpecTransHeatTag),
              enthTpartTags_( enthTpartTags ),
              enthTgasTags_( enthTgasTags ),
              charoxidationTag_( charoxidationTag ),
              charo2rhsTag_ (charo2rhsTag  ),
              co2coratioTag_(co2coratioTag ),
              hetroco2Tag_  ( hetroco2Tag  ),
              hetroh2oTag_  ( hetroh2oTag  ),
              nspec_        ( nspec        ),
              partIdx_      ( partIdx      )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new CharSpeciesTransportHeat<FieldT>( enthTpartTags_, enthTgasTags_, charoxidationTag_, charo2rhsTag_,
                                                     co2coratioTag_, hetroco2Tag_, hetroh2oTag_, nspec_, partIdx_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      FieldT& result = this->value();

      const FieldT& charoxidation = charoxidation_->field_ref();
      const FieldT& charo2rhs     = charo2rhs_    ->field_ref();
      const FieldT& co2coratio    = co2coratio_   ->field_ref();
      const FieldT& hetroco2      = hetroco2_     ->field_ref();
      const FieldT& hetroh2o      = hetroh2o_     ->field_ref();

      //_____________________________________________
      // compute contribution from mass coming to the particle from the gas phase
      // (participating in gasification & char oxidation)
      // O2, H2O and CO2 come to the particle at gas temperature

      // charo2rhs: negative; hetroco2: negative; hetroh2o: negative
      result <<= charo2rhs * enthTgass_[partIdx_*nspec_+2]->field_ref()
               + (hetroco2 / mwchar_ * mwco2_) * enthTgass_[partIdx_*nspec_+0]->field_ref()
               + (hetroh2o/mwchar_* mwh2o_) * enthTgass_[partIdx_*nspec_+4]->field_ref();

      //_____________________________________________________
      // compute contributions from mass carrying energy from the particle due to char oxidation and gasification

      // CO2 and CO from CharOxidation
      // CO and H2 from gasification reaction
      SpatFldPtr<FieldT> oxiCO2  = SpatialFieldStore::get<FieldT,FieldT>( result );
      SpatFldPtr<FieldT> oxiCO = SpatialFieldStore::get<FieldT,FieldT>( result );

      // charoxidation: negative
      *oxiCO2 <<= - charoxidation * co2coratio / (1.0 + co2coratio) /mwchar_ * mwco2_;
      *oxiCO  <<= - charoxidation / (1.0 + co2coratio) /mwchar_ * mwco_;

      result <<= result + *oxiCO2 * enthTparts_[partIdx_*nspec_+0]->field_ref()
                        + *oxiCO * enthTparts_[partIdx_*nspec_+1]->field_ref()
                        + (hetroco2 / mwchar_ * -mwco_*2) * enthTparts_[partIdx_*nspec_+1]->field_ref()
                        + (hetroh2o / mwchar_ * -mwco_) * enthTparts_[partIdx_*nspec_+1]->field_ref()
                        + (hetroh2o/mwchar_* -mwh2_) * enthTparts_[partIdx_*nspec_+3]->field_ref();
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );

      const FieldT& charoxidation    = charoxidation_->field_ref();
      const FieldT& charo2rhs        = charo2rhs_    ->field_ref();
      const FieldT& co2coratio       = co2coratio_   ->field_ref();
      const FieldT& hetroco2         = hetroco2_     ->field_ref();
      const FieldT& hetroh2o         = hetroh2o_     ->field_ref();
      const FieldT& dcharoxidationdv = charoxidation_->sens_field_ref(var);
      const FieldT& dcharo2rhsdv     = charo2rhs_    ->sens_field_ref(var);
      const FieldT& dco2coratiodv    = co2coratio_   ->sens_field_ref(var);
      const FieldT& dhetroco2dv      = hetroco2_     ->sens_field_ref(var);
      const FieldT& dhetroh2odv      = hetroh2o_     ->sens_field_ref(var);

      SpatFldPtr<FieldT> oxiCO2 = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
      SpatFldPtr<FieldT> oxiCO = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
      *oxiCO2 <<= - charoxidation * co2coratio / (1.0 + co2coratio) / mwchar_ * mwco2_;
      *oxiCO  <<= - charoxidation / (1.0 + co2coratio) / mwchar_ * mwco_;

      SpatFldPtr<FieldT> doxiCO2dv = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
      SpatFldPtr<FieldT> doxiCOdv = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
      *doxiCO2dv <<= - (dcharoxidationdv*co2coratio+charoxidation*dco2coratiodv) / (co2coratio+1.0) / mwchar_ * mwco2_
                     + charoxidation * co2coratio / square(co2coratio+1.0) * dco2coratiodv / mwchar_ * mwco2_;
      *doxiCOdv  <<= (- dcharoxidationdv / (co2coratio+1.0) + charoxidation /square(co2coratio+1.0) *dco2coratiodv) / mwchar_ * mwco_;

      dfdv <<= dcharo2rhsdv * enthTgass_[partIdx_*nspec_+2]->field_ref() + charo2rhs * enthTgass_[partIdx_*nspec_+2]->sens_field_ref(var)
              + (dhetroco2dv / mwchar_ * mwco2_) * enthTgass_[partIdx_*nspec_+0]->field_ref() + (hetroco2 / mwchar_ * mwco2_) * enthTgass_[partIdx_*nspec_+0]->sens_field_ref(var)
              + (dhetroh2odv /mwchar_* mwh2o_) * enthTgass_[partIdx_*nspec_+4]->field_ref() + (hetroh2o/mwchar_* mwh2o_) * enthTgass_[partIdx_*nspec_+4]->sens_field_ref(var)
              + *doxiCO2dv * enthTparts_[partIdx_*nspec_+0]->field_ref() + *oxiCO2 * enthTparts_[partIdx_*nspec_+0]->sens_field_ref(var)
              + *doxiCOdv * enthTparts_[partIdx_*nspec_+1]->field_ref() + *oxiCO * enthTparts_[partIdx_*nspec_+1]->sens_field_ref(var)
              + (dhetroco2dv / mwchar_ * -mwco_*2) * enthTparts_[partIdx_*nspec_+1]->field_ref() + (hetroco2 / mwchar_ * -mwco_*2) * enthTparts_[partIdx_*nspec_+1]->sens_field_ref(var)
              + (dhetroh2odv / mwchar_ * -mwco_) * enthTparts_[partIdx_*nspec_+1]->field_ref() + (hetroh2o / mwchar_ * -mwco_) * enthTparts_[partIdx_*nspec_+1]->sens_field_ref(var)
              + (dhetroh2odv/mwchar_* -mwh2_) * enthTparts_[partIdx_*nspec_+3]->field_ref() + (hetroh2o/mwchar_* -mwh2_) * enthTparts_[partIdx_*nspec_+3]->sens_field_ref(var);
    }
  };
} // namespace Char
#endif // CharSpeciesTransportHeat_CHAR_h
