
#ifndef CharBase_h
#define CharBase_h

#include <stdexcept>
#include <sstream>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace Char{

enum CharGasSpecies{
  O2,
  CO2,
  CO,
  H2,
  H2O,
  CH4,
  INVALID_SPECIES = 99
};


  enum CharModel{
    GASIF_OXID,
    CCK,
    INVALID_CHARMODEL,
    OFF
  };

  /**
   * @fn std::string char_model_name( const CharModel model )
   * @param model: char chemistry model
   * @return the string name of the model
   */
  std::string char_model_name( const CharModel model );

  /**
   * @fn CharModel char_model( const std::string& modelName )
   * @param modelName: the string name of the char model
   * @return the corresponding enum value
   */
  CharModel char_model( const std::string& modelName );

} // namespace Char

#endif /* CharBase_h */
