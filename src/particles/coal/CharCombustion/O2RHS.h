
/**
 * @file O2RHS.h
 * @par Getting the reaction rate of O2 from char oxidation per gas mass
 * @author Hang Zhou
 */
#ifndef O2RHS_Expr_h
#define O2RHS_Expr_h

#include <expression/Expression.h>
namespace Char{

/**
 *  @ingroup Char
 *  @class O2RHS
 *  @brief Getting the reaction rate of O2 from char oxidation per gas mass (unit: 1/s)
 *
 * Char oxidation includes two reactions:
 * \f[ C+O2 \rightarrow CO2 \f]
 * \f[ C+1/2O2 \rightarrow CO \f]
 * O2 reaction rate is given as:
 * \f[
 *    S_{O2} = S_{ox} \left( \frac{CO2/CO}{1+CO2/CO} + \frac{0.5}{CO2/CO} \right) \frac{Mw_O2}{Mw_C}
 * \f]
 * Here, \f$S_{ox}\f$ is the char oxidation rate per gas mass.
 *       \f$CO2/CO\f$ is the production ratio of CO2 and CO from char oxidation.
 *       \f$Mw_O2\f$ and \f$Mw_C\f$ are the molecular weight of O2 and C respectively.
 *
 *  NOTE: 1. This expression calculates the O2 reaction rate from all particles with ONE specific particle size.
 *           All the parameters used here are the value for ONE specific particle size.
 *        2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */
  template< typename FieldT >
  class O2RHS
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, oxidation_, co2coratio_ )

    O2RHS( const Expr::Tag& oxidationtag,
           const Expr::Tag& co2coratiotag )
          : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);

      oxidation_  = this->template create_field_request<FieldT>( oxidationtag  );
      co2coratio_ = this->template create_field_request<FieldT>( co2coratiotag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag oxidationtag_, co2coratiotag_;
    public:
      /**
       * @brief The meachanism for building a O2RHS object
       * @tparam FieldT
       * @param o2rhsTag O2 reaction rate by char oxidation per gas mass
       * @param oxidationtag char oxidation rate per gas mass
       * @param co2coratiotag production ratio of CO2 and CO from char oxidation
       */
      Builder( const Expr::Tag& o2rhsTag,
               const Expr::Tag& oxidationtag,
               const Expr::Tag& co2coratiotag )
            : ExpressionBuilder( o2rhsTag ),
              oxidationtag_ ( oxidationtag  ),
              co2coratiotag_( co2coratiotag )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new O2RHS<FieldT>( oxidationtag_, co2coratiotag_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();
      const FieldT& oxidation  = oxidation_ ->field_ref();
      const FieldT& co2coratio = co2coratio_->field_ref();
      result <<= oxidation/12.0 * ( 0.5/ (1.0 + co2coratio) + co2coratio /(1.0+ co2coratio)) * 32.0; // negative
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );

      const FieldT& oxidation  = oxidation_ ->field_ref();
      const FieldT& co2coratio = co2coratio_->field_ref();
      const FieldT& doxidationdv  = oxidation_ ->sens_field_ref(var);
      const FieldT& dco2coratiodv = co2coratio_->sens_field_ref(var);
      dfdv <<= 32.0/12.0 * (doxidationdv*( 0.5/ (1.0 + co2coratio) + co2coratio /(1.0+ co2coratio))
                             + oxidation*(1/(1.0 + co2coratio)-(0.5+co2coratio)/square(1.0 + co2coratio))*dco2coratiodv);
    }
  };
}



#endif // O2RHS_Expr_h
