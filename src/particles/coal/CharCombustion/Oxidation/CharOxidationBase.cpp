#include "CharOxidationBase.h"

#include <stdexcept>
#include <sstream>

namespace CharOxidation{

  std::string char_oxidation_model_name( const CharOxidationModel model )
  {
    std::string name;
    switch (model){
      case LH                        : name = "LH";          break;
      case FRACTAL                   : name = "Fractal";     break;
      case FIRST_ORDER               : name = "FirstOrder";  break;
      case INVALID_CHAROXIDATIONMODEL: name = "INVALID";     break;
      case OFF                       : name = "OFF";         break;
    }
    return name;
  }

  CharOxidationModel char_oxidation_model( const std::string& name )
  {
    if     ( name == char_oxidation_model_name(LH         ) ) return LH;
    else if( name == char_oxidation_model_name(FRACTAL    ) ) return FRACTAL;
    else if( name == char_oxidation_model_name(FIRST_ORDER) ) return FIRST_ORDER;
    else if( name == char_oxidation_model_name(OFF        ) ) return OFF;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported char oxidation model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << char_oxidation_model_name( LH          )
          << "\n\t" << char_oxidation_model_name( FRACTAL     )
          << "\n\t" << char_oxidation_model_name( FIRST_ORDER )
          << "\n\t" << char_oxidation_model_name( OFF         )
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_CHAROXIDATIONMODEL;
  }

}
