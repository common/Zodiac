#include "CharOxidationFunctionsLH_Fractal.h"

namespace CharOxidation{

  // -----------------------------------------------------------------------------
  double struc_param( const double rho,
                      const double rho_0,
                      const double e0,
                      const double s,
                      const double r )

  {
    // Modified Structure Parameter from Fei et al (2011).
    const double rhop = rho*(1.0-e0)/rho_0;
    const double psi = 2.0*rhop/s/r;
    return psi;
  }

  // -----------------------------------------------------------------------------
  double Do2_mix( const double& pressure,
                  const double& temp,
                  const double& co_ratio )
  {
    const double Cst= 1.013e-2;
    const double c13 = 1.0/3.0;
    // Diffusion of Oxygen in CO2
    const double D_CO2 = Cst * pow(temp,1.75)*pow((32.0+44.0)/(32.0*44.0),0.5)/
                         (pressure * (pow(16.3, c13)+pow(26.7, c13))*(pow(16.3, c13)+pow(26.7, c13))); // Danner and Daubert (1983)

    // Diffusion of Oxygen in CO
    const double D_CO = Cst * pow(temp,1.75)*pow((32.0+28.0)/(32.0*28.0),0.5)/
                        (pressure * (pow(16.3, c13)+pow(18.0, c13))* (pow(16.3, c13)+pow(18.0, c13)));

    const double y_co  = 1.0/(co_ratio + 1.0);
    const double y_co2 = co_ratio/(co_ratio + 1.0);
    return 1.0/(y_co/D_CO + y_co2/D_CO2);
  }

  // -----------------------------------------------------------------------------
  double SurfaceO2_error( double& q,           double& co2CoRatio,
                          const double po2S,   const double gaspres,
                          const double tempg,  const double tempP,
                          const double po2Inf, const double c_s,
                          const double dp,     const double k1,
                          const double k2,     const double k_a_s,
                          const CharOxidation::CharOxidationModel charoximodel )
  {
    co2CoRatio = CharOxidation::co2coratio_fun( po2S, tempP );
    const double landa = -1.0/(1+co2CoRatio)/2.0;
    const double do2mix = Do2_mix(gaspres,tempg,co2CoRatio);
    if( charoximodel == CharOxidation::CharOxidationModel::LH){
      q = CharOxidation::q_fun(k1,k2,po2S); // k1*k2*pow(po2S/101325, 0.3)/(k1*pow(po2S/101325, 0.3)+k2)
    }
    else if( charoximodel == CharOxidation::CharOxidationModel::FRACTAL){
      q = k_a_s *  po2S/ 8.314 / tempg; // bg : what temperature should be used here ?  tp, tg or (tp+tg)/2
    }

    return po2S/ gaspres - (landa + (po2Inf/ gaspres - landa)*exp(-q * dp/(2* c_s * do2mix)));
  }
  // -----------------------------------------------------------------------------
  double ka_fun( const double& theta, const double& S_g,
                 const double& rho,   const double& dp,
                 const double& temp )
  {
    const double chi_value = CharOxidation::chi(theta, S_g, rho, dp);
    const double k = CharOxidation::k_fun(temp);
    const double Q = CharOxidation::Q_number(chi_value, k, temp);
    return 3*k*(1.0/(tanh(Q)*Q)-1.0/pow(Q,2.0));
  }
  // -----------------------------------------------------------------------------

} // namespace CharOxidation
