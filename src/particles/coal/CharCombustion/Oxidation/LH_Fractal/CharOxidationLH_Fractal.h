
/**
 * @file CharOxidationLH_Fractal.h
 * @par Getting char oxidaiton rate per gas mass using nth-order LH or Fractal model
 * @author Hang Zhou
 */
#ifndef CharOxidationLH_Fractal_Expr_h
#define CharOxidationLH_Fractal_Expr_h

#include <expression/Expression.h>

#include <particles/coal/CharCombustion/CharData.h>
#include <particles/coal/Devolatilization/CPD/c0_fun.h>
#include <particles/coal/CharCombustion/Oxidation/CharOxidationBase.h>
#include "CharOxidationFunctionsLH_Fractal.h"

namespace CharOxidation{

  /**
   *  @ingroup CharOxidation
   *  @class CharOxidationLH_Fractal
   *  @brief Getting char oxidaiton rate per gas mass using nth-order LH or Fractal model (unit: 1/s)
   *  @todo Check and document Fractal model
   *
   * @par References:
   * [1] "Combustion kinetics of coal chars in oxygen-enriched environments", Jeffrey J. Murphy,Christopher R. Shaddix, Combustion and Flame 144 (2006) 710-729
   * [2] He, Wei, Yuting Liu, Rong He, Takamasa Ito, Toshiyuki Suda, Toshiro Fujimori, Hideto Ikeda, and Jun’ichi Satko.
   *     "Combustion Rate for Char with Fractal Pore Characteristics" Combustion Science and Technology no. July (July 11, 2013)
   *
   *  @par Warnings:
   *   - Initial mass of Char must be coupled with CPD model
   *   - Initial amount of particle density must be coupled with CPD model
   *   - We must add a logic variable to the particle that shows if its
   *     eligible to enter to the reaction expression.
   *   - For structure parameter I have used initial amount of internal
   *     surface area.
   *
   *  For nth-order LH model, the oxidation rate of char per gas mass is given as:
   *  \f[
   *    S_{oxi} = \frac{r_c Mw_C}{\phi}\pi d_p^2 n_p
   *  \f]
   *  Here, \f$r_c\f$ is the
   *        \f$\phi=1/(1+\psi)\f$, with \f$\psi=(CO2/CO)/(1+CO2/CO)\f$ representing moles of CO2 producted from per mole C.
   *        \f$d_p\f$ is the particle diameter. \f$n_p\f$ is the number of particles per gas mass. \f$Mw_c\f$ is the molecular weight of carbon.
   *
   * \f$r_c\f$ and \f$CO2/CO\f$ are calculated by solving the follow equations:
   * <ul>
   *    <li>
   *        \f[ r_c = \frac{k_1 k_2 (P_{O2,s}/101325)^n_r}{k_1 (P_{O2,s}/101325)^n_r + k_2} \f]
   *    </li>
   *    <li>
   *        \f[ \frac{CO_2}{CO} = A (P_{O2,s}/101325) \exp(\frc{B}{T_p}) \f]
   *    </li>
   *    <li>
   *        \f[ P_{O2,s} = (P_{O2,\inf}+\frac{P}{2(1+CO2/CO)})\exp(-\frac{r_c d_p}{2C_g D_{O2,g}} + \frac{P}{2(1+CO2/CO)}) \f]
   *    </li>
   * </ul>
   * Here, \f$k_1=A_1\exp{E_1/(RT_p)}\f$ and \f$k_2=A_2\exp{E_12/(RT_p)}\f$.
   *       \f$P_{O2,\inf}=P Y_{O2}Mw /Mw_{O2}\f$ is the particle pressure of O2 in the bulk.
   *       \f$P\f$ is the pressure of gas phase. \f$Mw\f$ is the mixture molecular weight.
   *       \f$T_p\f$ is particle temperature. \f$Y_{O2}\f$ is the mass fraction of O2 in the bulk.
   *       \f$Mw_{O2}\f$ is the molecular weight of O2.
   *       \f$C_g = P/R/T_g\f$ is the average molar concentration of the gases.
   *       \f$\frac{1}{D_{O2,g}}=\frac{1/(CO2/CO+1)}{D_{CO}}+ frac{CO2/CO/(CO2/CO+1)}{D_{CO2}}\f$
   *       is the diffusion coefficient of oxygen into char oxidation product (the mixture of CO2 and CO)
   */
  template< typename FieldT >
  class CharOxidationLHFractal : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, tempP_, numP_, tempG_, massFracO2_, mmw_, density_, charMass_, charMassIn_ )
    DECLARE_FIELDS( FieldT, gasPressure_, prtDiam_, initprtmas_ )

    const CharOxidationModel charoxidamodel_;
    const Char::CharCombustionData oxiData_;
    const double Ocoal_, R_;

    CharOxidationLHFractal( const Expr::Tag& prtDiamTag,
                            const Expr::Tag& tempPTag,
                            const Expr::Tag& numPTag,
                            const Expr::Tag& intTempGasTag,
                            const Expr::Tag& massFracO2Tag,
                            const Expr::Tag& mmwTag,
                            const Expr::Tag& densityTag,
                            const Expr::Tag& charMassTag,
                            const Expr::Tag& charMassInTag,
                            const Expr::Tag& intGasPressTag,
                            const Expr::Tag& initprtmasTag,
                            const Char::CharCombustionData oxiData,
                            const CharOxidationModel charoxidamodel)
          : Expr::Expression<FieldT>(),
            charoxidamodel_(charoxidamodel),
            oxiData_       ( oxiData ),
            Ocoal_         ( oxiData.get_O()),
            R_             ( 8.314)  // Gas constant
    {
      tempP_       = this->template create_field_request<FieldT>( tempPTag       );
      numP_        = this->template create_field_request<FieldT>( numPTag        );
      tempG_       = this->template create_field_request<FieldT>( intTempGasTag  );
      massFracO2_  = this->template create_field_request<FieldT>( massFracO2Tag  );
      mmw_         = this->template create_field_request<FieldT>( mmwTag         );
      density_     = this->template create_field_request<FieldT>( densityTag     );
      charMass_    = this->template create_field_request<FieldT>( charMassTag    );
      charMassIn_  = this->template create_field_request<FieldT>( charMassInTag  );
      gasPressure_ = this->template create_field_request<FieldT>( intGasPressTag );
      prtDiam_     = this->template create_field_request<FieldT>( prtDiamTag     );
      initprtmas_  = this->template create_field_request<FieldT>( initprtmasTag  );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tempPTag_, numPTag_, tempGTag_, massFracO2Tag_, mmwTag_, densityTag_, charMassTag_,
                        charMassInTag_, gasPressTag_, prtDiamTag_, initprtmasTag_;
      const CharOxidationModel charoxidamodel_;
      const Char::CharCombustionData oxiData_;
    public:
      /**
       * @brief The mechanism for building a CharOxidationLHFractal object
       * @tparam FieldT
       * @param charOxTags oxidation rate of char per gas mass
       * @param prtDiamTag particle diameter
       * @param tempPTag particle temperature
       * @param numPTag number of particles per gas mass
       * @param tempGasTag gas temperature
       * @param massFracO2Tag mass fraction of O2
       * @param mmwTag mixture molecular weight
       * @param densityTag particle density
       * @param charMassTag char mass per gas mass
       * @param charMassInTag initial char mass per gas mass
       * @param gasPressTag gas pressure
       * @param intPrtmasTag initial particle mass
       * @param oxiData char oxidation data
       * @param charoxidamodel char oxidation model
       */
      Builder( const Expr::TagList& charOxTags,
               const Expr::Tag& prtDiamTag,
               const Expr::Tag& tempPTag,
               const Expr::Tag& numPTag,
               const Expr::Tag& tempGasTag,
               const Expr::Tag& massFracO2Tag,
               const Expr::Tag& mmwTag,
               const Expr::Tag& densityTag,
               const Expr::Tag& charMassTag,
               const Expr::Tag& charMassInTag,
               const Expr::Tag& gasPressTag,
               const Expr::Tag& initPrtmassTag,
               const Char::CharCombustionData oxiData,
               const CharOxidationModel charoxidamodel)
            : ExpressionBuilder(charOxTags),
              prtDiamTag_    ( prtDiamTag ),
              tempPTag_      ( tempPTag ),
              numPTag_       ( numPTag  ),
              tempGTag_      ( tempGasTag ),
              massFracO2Tag_ ( massFracO2Tag ),
              mmwTag_        ( mmwTag ),
              densityTag_    ( densityTag ),
              charMassTag_   ( charMassTag ),
              charMassInTag_ ( charMassInTag ),
              gasPressTag_   ( gasPressTag ),
              initprtmasTag_ ( initPrtmassTag ),
              oxiData_       ( oxiData ),
              charoxidamodel_( charoxidamodel )
      {
      }
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new CharOxidationLHFractal<FieldT>(prtDiamTag_ ,tempPTag_, numPTag_, tempGTag_, massFracO2Tag_, mmwTag_, densityTag_,
                                                  charMassTag_, charMassInTag_, gasPressTag_, initprtmasTag_, oxiData_, charoxidamodel_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      // results[0]  :   Char Oxidation
      // results[1]  :   CO2/CO;
      // results[2]  :   q;
      // results[3]  :   po2s;

      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
      double st1_, err_, st2_;
      const double f0err = 1e-15;
      const double h1err = 1e-10;
      double po2S, q, c_s, po2st,  cs;
      double f_0,f_1,f_2, po2S_1, po2S_2, po2S_r;
      double af,bf,cf,h1,h2,gama;
      double k1, k2,  co2CORatio, k_a_s;
      double a, weight;

      const double S0 = oxiData_.get_S_0(); // m2/kg same for all type of the coal -- SurfaceArea
      const double Rpore = oxiData_.get_r_pore();  //  -- SurfaceArea
      const double e0 = oxiData_.get_e0(); //  -- SurfaceArea

      const double c0 = CPD::c0_fun( oxiData_.get_coal_composition().get_C(),
                                     oxiData_.get_coal_composition().get_O() );

      const double m0 = oxiData_.get_coal_composition().get_fixed_c()
                        + oxiData_.get_coal_composition().get_vm()*c0;

      typename FieldT::const_iterator iprtdiam    = prtDiam_    ->field_ref().begin();
      typename FieldT::const_iterator itempP      = tempP_      ->field_ref().begin();
      typename FieldT::const_iterator inumP       = numP_       ->field_ref().begin();
      typename FieldT::const_iterator idensity    = density_    ->field_ref().begin();
      typename FieldT::const_iterator icharMass   = charMass_   ->field_ref().begin();
      typename FieldT::const_iterator icharMassIn = charMassIn_ ->field_ref().begin();
      typename FieldT::const_iterator iinitprtmas = initprtmas_ ->field_ref().begin();
      typename FieldT::const_iterator io2farc     = massFracO2_ ->field_ref().begin();
      typename FieldT::const_iterator immw        = mmw_        ->field_ref().begin();
      typename FieldT::const_iterator igaspres    = gasPressure_->field_ref().begin();
      typename FieldT::const_iterator itempg      = tempG_      ->field_ref().begin();

      typename FieldT::iterator iresult     = results[0]->begin();
      typename FieldT::iterator ico2coratio = results[1]->begin();
      typename FieldT::iterator iq          = results[2]->begin();
      typename FieldT::iterator ipo2s       = results[3]->begin();

      const typename FieldT::iterator ire = results[0]->end();

      for(; iresult != ire; ++iprtdiam, ++itempP, ++idensity, ++icharMass, ++icharMassIn, ++iinitprtmas,
            ++io2farc, ++immw, ++igaspres, ++itempg, ++iresult, ++ico2coratio, ++iq, ++ipo2s){

        if( charoxidamodel_ == LH){
          /* The activation energies in [1] are said to be in kJ/mol, but if one calculates depletion
           * flux, q, assuming PO2_s = PO2_inf = 0.12 Atm and the maximum particle temperature (~2100K)
           * the result is ~10^-2 mol/m^2-s, and according to figure 12 it should be ~10 mol/m^2-s.
           * However, if the activation energies are assumed to be in J/mol q is calculated to be
           * ~10mol/m^2-s. Therefore it is assumed that the activation energies are in J/mol rather than
           * kJ/mol.
           *
           * Note, this also explains the nearly constant predicted rate of char consumption shown in
           * figure 14.
           */
          if( Ocoal_ < 0.13 ){ // High Rank Coal
            k1 = (61.0)*exp(-0.5/R_/ *itempP);
            k2 = (20.0)*exp(-107.4/R_/ *itempP);
          }
          else {
            k1 = (93.0)*exp(-0.1/R_/ *itempP);
            k2 = (26.2)*exp(-109.9/R_/ *itempP);
          }
        }
        else if( charoxidamodel_ == FRACTAL){
          /*
            ************    Start of Calculating particle surface area
           */
          //const double surfacearea = 3.141593* square(*iprtdiam); // Particle Surface Area
          st2_ = S0/15.0 * *iinitprtmas *m0;
          const double initchar = *iinitprtmas;
          const double burnoff = ( initchar - *icharMass ) / (initchar);
          double surfacearea = S0 * (1.0 - burnoff) * initchar;

          if( Ocoal_ < 0.13 ){ // High Rank Coal
            const double particleV = 1.0 / (*idensity) * (*icharMass);
            // Loop to obtain the result for S which depends on the structure parameter.
            st1_ = st2_;
            st2_ = surfacearea - 1.0;
            err_ = 1.0;
            while (err_>1E-8) {
              st2_ = surfacearea * sqrt(1.0 - struc_param(*idensity,Char::initial_particle_density(*iinitprtmas,*iprtdiam),
                                                          e0, st1_ /particleV , Rpore)*log(1.0-burnoff));
              const double tmp = (st1_-st2_)/st1_;
              err_ = tmp*tmp;
              st1_ = st2_;
            }
            surfacearea = st2_;
          }
          /*
           ************    End of Calculating particle surface area
           */
          k_a_s = ka_fun(e0, S0, *idensity, *iprtdiam, *itempP) * surfacearea;
        }

        cs = (*io2farc * *immw * *igaspres / R_ / *itempg / 32.0);
        const double po2Inf = cs*R_* *itempg;
        c_s = *igaspres/R_/ *itempg;

        // for f1
        double po2S_1 = po2Inf;
        f_1 = SurfaceO2_error( q, co2CORatio, po2S_1, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);

        // for f2
        po2S_2 = 0.0;
        f_2 = SurfaceO2_error( q, co2CORatio, po2S_2, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);

        // for f0
        po2S = po2Inf/2.0;
        po2st = po2S + 100;
        f_0   = 1.0;

        bool dobisection = false;
        err_ =1.0;

        int i=1;
        while ( err_ > f0err ) {
          i++;
          if(i>100){
            dobisection = true;
            break;
          }
          po2st = po2S;

          // Function part
          // for f0
          f_0 = SurfaceO2_error( q, co2CORatio, po2S, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);
          err_ = f_0*f_0;
          h1 = po2S_1-po2S;
          h2 = po2S-po2S_2;
          if (h1 < h1err || err_ < f0err) {
            break;
          }
          gama = h2/h1;
          af = (gama*f_1-f_0*(1+gama)+f_2)/(gama*h1*h1*(1.0+gama));
          bf = (f_1-f_0-af*h1*h1)/h1;
          cf = f_0 ;

          if (bf > 0){
            po2S_r = po2S- 2.0*cf/(bf+sqrt(bf*bf-4.0*af*cf));
          }
          else {
            po2S_r = po2S- 2.0*cf/(bf-sqrt(bf*bf-4.0*af*cf));
          }
          if (po2S_r>po2S) {
            if (po2S_1-po2S_r < h1err){
              po2S_r = po2S_r - h1/10;
            }
            po2S_2 = po2S;
            f_2    = f_0;
            po2S   = po2S_r;
          }
          else {
            po2S_1 = po2S;
            f_1    = f_0;
            po2S   = po2S_r;
          }

          if (po2S > po2Inf + h1err || po2S < 0){
            dobisection = true;
            break;
          }
        }

        /*
         ************    Start of Bisection Method
         */

        // Initialization
        double  f_2;
        double bisec=0.0;

        if (dobisection) {

          h1 = po2Inf/10000;
          f_1 = SurfaceO2_error( q, co2CORatio, h1, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);

          h2 = po2Inf;
          f_2 = SurfaceO2_error( q, co2CORatio, h2, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);
          if (f_1 * f_2 > 0.0) {
            bisec = f_1 + (f_2-f_1)/2.0;
          }
        }
        err_ = 1.0;
        i=0;
        double halff = 1.0;
        while (dobisection && err_>f0err && halff/h2>f0err) {
          i++;
          halff = (h2-h1)/2.0;
          po2S = h1 + halff ;
          err_ = SurfaceO2_error( q, co2CORatio, po2S, *igaspres, *itempg, *itempP, po2Inf, c_s, *iprtdiam, k1, k2, k_a_s, charoxidamodel_);
          if (err_ > bisec) {
            h2=po2S;
          }
          else {
            h1=po2S;
          }
          err_ *= err_;
          if( i>100 ){
            std::cout << " CharOxidationLH_Fractal.h : bisection method couldn't converge on oxygen concentration at particle surface! \n";
            break;
          }
        }
        /*
         ************    End of Bisection Method
         */
        a = (*icharMassIn == 0.0)?(1e-10):*icharMassIn;
        weight = (1-exp(-(1/ a)* *icharMass) > 0.0)?(1-exp(-(1/ a)* *icharMass)):0.0;

        *ico2coratio = co2CORatio;
        *iq = q;
        *ipo2s = po2S;
        *iresult = - weight * (q * 12.0 / (2*(1+co2CORatio)/(1+2*co2CORatio)) * *inumP*M_PI* (*iprtdiam * *iprtdiam) / 1000.); //negative
      }
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
      std::vector<FieldT > dresultdv;
      for(size_t i=0;i<targetTags.size();++i){
        dresultdv.push_back(this->sensitivity_result(targetTags[i], var));
      }

      const FieldT& prtdiam    = prtDiam_    ->field_ref();  const FieldT& dprtdiamdv    = prtDiam_    ->sens_field_ref(var);
      const FieldT& tempP      = tempP_      ->field_ref();  const FieldT& dtempPdv      = tempP_      ->sens_field_ref(var);
      const FieldT& numP       = numP_       ->field_ref();  const FieldT& dnumPdv       = numP_       ->sens_field_ref(var);
      const FieldT& density    = density_    ->field_ref();  const FieldT& ddensitydv    = density_    ->sens_field_ref(var);
      const FieldT& charMass   = charMass_   ->field_ref();  const FieldT& dcharMassdv   = charMass_   ->sens_field_ref(var);
      const FieldT& charMassIn = charMassIn_ ->field_ref();  const FieldT& dcharMassIndv = charMassIn_ ->sens_field_ref(var);
      const FieldT& initprtmas = initprtmas_ ->field_ref();  const FieldT& dinitprtmasdv = initprtmas_ ->sens_field_ref(var);
      const FieldT& o2farc     = massFracO2_ ->field_ref();  const FieldT& do2farcdv     = massFracO2_ ->sens_field_ref(var);
      const FieldT& mmw        = mmw_        ->field_ref();  const FieldT& dmmwdv        = mmw_        ->sens_field_ref(var);
      const FieldT& gaspres    = gasPressure_->field_ref();  const FieldT& dgaspresdv    = gasPressure_->sens_field_ref(var);
      const FieldT& tempG      = tempG_      ->field_ref();  const FieldT& dtempGdv      = tempG_      ->sens_field_ref(var);

      const FieldT& result     = *results[0];
      const FieldT& co2coratio = *results[1];
      const FieldT& q          = *results[2];
      const FieldT& po2s       = *results[3];

      SpatFldPtr<FieldT> po2Inf = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dpo2Infdv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      *po2Inf <<= o2farc * mmw * gaspres / 32.0;
      *dpo2Infdv <<= (do2farcdv * mmw * gaspres + o2farc * dmmwdv * gaspres + o2farc * mmw * dgaspresdv) / 32.0;

      SpatFldPtr<FieldT> cg = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dcgdv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      *cg <<= gaspres/R_/ tempG;
      *dcgdv <<= (dgaspresdv/ tempG - gaspres/square(tempG) * dtempGdv) / R_;

      SpatFldPtr<FieldT> do2mix = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> D_CO  = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> D_CO2 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dD_COdv  = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dD_CO2dv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> y_co  = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> y_co2 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );

      const double Cst= 1.013e-2;    const double c13 = 1.0/3.0;
      // Diffusion of Oxygen in CO2
      *D_CO2 <<= Cst * pow(tempG,1.75)*pow((32.0+44.0)/(32.0*44.0),0.5)/
                           (gaspres * (pow(16.3, c13)+pow(26.7, c13)) *  (pow(16.3, c13)+pow(26.7, c13))); // Danner and Daubert (1983)
      // Diffusion of Oxygen in CO
      *D_CO  <<= Cst * pow(tempG,1.75)*pow((32.0+28.0)/(32.0*28.0),0.5)/
                          (gaspres * (pow(16.3, c13)+pow(18.0, c13)) * (pow(16.3, c13)+pow(18.0, c13)));
      *dD_CO2dv <<= Cst *pow((32.0+44.0)/(32.0*44.0),0.5)/ (pow(16.3, c13)+pow(26.7, c13)) /(pow(16.3, c13)+pow(26.7, c13))
                    * ( 1.75 * pow(tempG, 0.75) / gaspres * dtempGdv - pow(tempG,1.75) / square(gaspres) * dgaspresdv);
      *dD_COdv <<= Cst *pow((32.0+28.0)/(32.0*28.0),0.5)/ (pow(16.3, c13)+pow(18.0, c13)) / (pow(16.3, c13)+pow(18.0, c13))
                    * ( 1.75 * pow(tempG, 0.75) / gaspres * dtempGdv - pow(tempG,1.75) / square(gaspres) * dgaspresdv);
      *y_co  <<= 1.0/(co2coratio + 1.0);
      *y_co2 <<= co2coratio/(co2coratio + 1.0);
      *do2mix <<= 1.0/(*y_co/ *D_CO + *y_co2/ *D_CO2);

      SpatFldPtr<FieldT> k1 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> k2 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dk1dv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dk2dv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );

      if( Ocoal_ < 0.13 ){ // High Rank Coal
        *k1 <<= (61.0)*exp(-0.5/R_/ tempP);
        *k2 <<= (20.0)*exp(-107.4/R_/ tempP);
        *dk1dv <<= (61.0)*exp(-0.5/R_/ tempP) * (0.5/R_/square(tempP) * dtempPdv);
        *dk2dv <<= (20.0)*exp(-107.4/R_/ tempP) * (107.4/R_/square(tempP) * dtempPdv);
      }
      else {
        *k1 <<= (93.0)*exp(-0.1/R_/ tempP);
        *k2 <<= (26.2)*exp(-109.9/R_/ tempP);
        *dk1dv <<= (93.0)*exp(-0.1/R_/ tempP) * (0.1/R_/square(tempP) * dtempPdv);
        *dk2dv <<= (26.2)*exp(-109.9/R_/ tempP) * (109.9/R_/square(tempP) * dtempPdv);
      }

      SpatFldPtr<FieldT> C1 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C2 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C3 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C4 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C5 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C6 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C7 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> C8 = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );

      *C1 <<= 0.21 * 0.02 * pow(po2s/101325, 0.21-1.0) /101325 * exp(3070/tempP);
      *C2 <<= 0.02 * pow(po2s/101325, 0.21) * 3070/square(tempP) * exp(3070/tempP) * dtempPdv;
      *C3 <<= (*k2* pow(po2s, 0.3) / (*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3))
               - *k1 * *k2*pow(po2s, 2*0.3)/square(*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3)) ) * *dk1dv
              + (*k1* pow(po2s, 0.3) / (*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3))
                 - *k1 * *k2*pow(po2s*101325, 0.3)/square(*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3)) ) * *dk2dv;
      *C4 <<= *k1* *k2 *0.3*pow(po2s, 0.3-1.0) / (*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3))
               - square(*k1)* *k2 *0.3*pow(po2s, 2*0.3-1.0)/square(*k1*pow(po2s, 0.3)+*k2*pow(101325, 0.3));
      *C5 <<= *dpo2Infdv*exp(-q*prtdiam/2/ *cg/ *do2mix) + (1+exp(-q*prtdiam/2/ *cg/ *do2mix))/2/(1+co2coratio)*dgaspresdv
              - (*po2Inf+gaspres/2/(1+co2coratio))*exp(-q*prtdiam/2/ *cg/ *do2mix)
                 * (q/2/ *cg/ *do2mix*dprtdiamdv - q*prtdiam/2/square(*cg)/ *do2mix* *dcgdv );
      *C6 <<= (*po2Inf+gaspres/2/(1+co2coratio))*exp(-q*prtdiam/2/ *cg/ *do2mix) *q*prtdiam/2/square(*do2mix)/ *cg
              * ( (co2coratio**D_CO2/(*D_CO2+co2coratio**D_CO) - (1+co2coratio)**D_CO**D_CO2/square(*D_CO2+co2coratio**D_CO))**dD_COdv
                + (co2coratio**D_CO/(*D_CO2+co2coratio**D_CO) - (1+co2coratio)**D_CO**D_CO2/square(*D_CO2+co2coratio**D_CO))**dD_CO2dv );
      *C7 <<= gaspres/2/square(1+co2coratio)*(exp(-q*prtdiam/2/ *cg/ *do2mix)-1)
             + (*po2Inf+gaspres/2/(1+co2coratio))*exp(-q*prtdiam/2/ *cg/ *do2mix) *q*prtdiam/2/square(*do2mix)/ *cg
               * (*D_CO**D_CO2/(*D_CO2+co2coratio**D_CO) - (1+co2coratio)*square(*D_CO)**D_CO2/square(*D_CO2+co2coratio**D_CO));
      *C8 <<= (*po2Inf+gaspres/2/(1+co2coratio))*exp(-q*prtdiam/2/ *cg/ *do2mix)*prtdiam/2/ *cg/ *do2mix;


      SpatFldPtr<FieldT> a = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> weight = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      SpatFldPtr<FieldT> dweightdv = SpatialFieldStore::get<FieldT,FieldT>( prtdiam );
      *a <<= cond(charMassIn == 0.0, 1e-10)
                 (                   charMassIn);
      *weight <<= max(0, 1-exp(-(1/ *a)* charMass));
      *dweightdv <<= cond( charMassIn<=0.0, 0.0)
                         (                  (1/ *a) * exp(-(1/ *a)*charMass) * dcharMassdv);

      dresultdv[3] <<= (*C5 + *C6 - *C2**C7 - *C3**C8)/ (1 + *C4**C8 - *C1**C7);
      dresultdv[2] <<= *C3 + *C4 * dresultdv[3];
      dresultdv[1] <<= -*C2 + *C1 * dresultdv[3];
      dresultdv[0] <<= - *weight * 12.0*M_PI/1000.0 * (dresultdv[2]*square(prtdiam)/2/(1+co2coratio)*(1+2*co2coratio)*numP
                                           +2*q*prtdiam/2/(1+co2coratio)*(1+2*co2coratio)*dprtdiamdv*numP
                                           +q*square(prtdiam/(1+co2coratio))/2*dresultdv[1]*numP
                                           + q/2/(1+co2coratio)*(1+2*co2coratio)*square(prtdiam)*dnumPdv)
                       - *dweightdv * (q * 12.0 / (2*(1+co2coratio)/(1+2*co2coratio)) *numP*M_PI* square(prtdiam) / 1000.);

    }
  };


} // namespace CharOxidation


#endif // CharOxidationLH_Fractal_Expr_h
