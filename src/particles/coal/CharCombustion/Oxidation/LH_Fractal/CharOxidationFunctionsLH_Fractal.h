#ifndef CharOxidationFunctionsLH_Fractal_h
#define CharOxidationFunctionsLH_Fractal_h

#include <cmath>
#include "particles/coal/CharCombustion/Oxidation/CharOxidationBase.h"

namespace CharOxidation{

// -----------------------------------------------------------------------------
/**
 * @brief Getting reaction rate of char
 * @param k1 Arrhenius rate constant
 * @param k2 Arrhenius rate constant
 * @param po2S partial pressure of oxygen at particle surface (unit: Pa)
 * @return reaction rate of char \f$r_c\f$
 */
inline double q_fun (double k1, double k2, double po2S){
  return k1*k2*pow(po2S/101325, 0.3)/(k1*pow(po2S/101325, 0.3)+k2);
}
// -----------------------------------------------------------------------------
/**
 * @brief Getting the moles ratio between CO2 and CO produced by char oxidation
 * @param po2S  partial pressure of oxygen at particle surface (unit: Pa)
 * @param tempP particle temperature
 * @return the moles ratio between CO2 and CO produced by char oxidation
 */
inline double co2coratio_fun(const double& po2S,const double& tempP){
  return  0.02* pow(po2S/101325, 0.21)*exp(3070/tempP);  // CO2/CO = A * P_{O2,s}^n *exp(B/T). P in atm
}

// -----------------------------------------------------------------------------
/**
 * @todo document this
 */
double struc_param( const double rho,
                    const double rho_0,
                    const double e0,
                    const double s,
                    const double r );

// -----------------------------------------------------------------------------
/**
 * @brief Getting the diffusion coefficient of oxygen into char oxidation product (the mixture of CO2 and CO)
 * @param pressure pressure of gas
 * @param temp gas temperature
 * @param co_ratio moles ratio between CO2 and CO produced by char oxidation
 * @return the diffusion coefficient of oxygen into char oxidation product (the mixture of CO2 and CO)
 */
double Do2_mix( const double& pressure,
                const double& temp,
                const double& co_ratio );

// -----------------------------------------------------------------------------
/**
 * @brief Getting the residual of the equation for partial pressure of O2 at particle surface
 * @param q reaction rate of char
 * @param co2CoRatio moles ratio between CO2 and CO produced by char oxidation
 * @param po2S partial pressure of O2 at particle surface
 * @param gaspres gas phase pressure
 * @param tempg gas temperature
 * @param tempP particle temperature
 * @param po2Inf partial pressure of O2 in the bulk
 * @param c_s the average molar concentration of the gases.
 * @param dp particle diameter
 * @param k1 Arrhenius rate constant for LH model
 * @param k2 Arrhenius rate constant for LH model
 * @param k_a_s Arrhenius rate constant for Fractal model
 * @param charoximodel char oxidatio model
 * @return the residual of the equation for partial pressure of O2 at particle surface
 */
double SurfaceO2_error( double& q,           double& co2CoRatio,
                        const double po2S,   const double gaspres,
                        const double tempg,  const double tempP,
                        const double po2Inf, const double c_s,
                        const double dp,     const double k1,
                        const double k2,     const double k_a_s,
                        const CharOxidation::CharOxidationModel charoximodel );

// -----------------------------------------------------------------------------
/**
 * @todo document this
 * @brief
 * @param theta
 * @param S_g
 * @param rho
 * @param dp
 * @return
 */

inline double chi( const double& theta, const double& S_g,
                   const double& rho,   const double& dp )
{
  return 2*pow(theta,5.0/3.0)/(pow(S_g*rho*dp*(1-theta),2))*exp(-1.15); // [2]
}

// -----------------------------------------------------------------------------
/**
 * @todo document this
 * @brief
 * @param chi_value
 * @param k
 * @param temp
 * @return
 */
inline double Q_number( const double& chi_value,
                        const double& k,
                        const double& temp )
{
  return 0.5*pow(k/(1.975*chi_value*pow(temp/0.032,0.5)),0.5); // [2]
}

// -----------------------------------------------------------------------------
/**
 * @todo document this
 * @brief
 * @param temp
 * @return
 */
inline double k_fun( const double& temp )
{
  return 62.37*exp(-54.0E3/8.314/temp);   // m/s
}

// -----------------------------------------------------------------------------
/**
 * @brief Getting the Arrhenius rate constant for Fractal model
 * @param theta initial porosity of coal particle
 * @param S_g internal surface area of initial char (m2/kg)
 * @param rho particle density
 * @param dp particle diameter
 * @param temp particle temperature
 * @return Arrhenius rate constant for Fractal model
 */
double ka_fun( const double& theta, const double& S_g,
               const double& rho,   const double& dp,
               const double& temp );

// -----------------------------------------------------------------------------
}

#endif /* CharOxidationFunctionsLH_Fractal_h */
