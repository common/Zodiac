
#ifndef CharOxidationBase_h
#define CharOxidationBase_h

#include <stdexcept>
#include <sstream>

namespace Expr{ class ExpressionFactory; class TimeStepper; class FieldManagerList; }

namespace CharOxidation{

  enum CharOxidationModel{
    LH,
    FRACTAL,
    FIRST_ORDER,
    INVALID_CHAROXIDATIONMODEL,
    OFF
  };

  /**
   * @fn std::string char_oxidation_model_name( const CharOxidationModel model )
   * @param model: char oxidation model
   * @return the string name of the model
   */
  std::string char_oxidation_model_name( const CharOxidationModel model );

  /**
   * @fn CharModel char_oxidation_model( const std::string& modelName )
   * @param modelName: the string name of the char oxidation model
   * @return the corresponding enum value
   */
  CharOxidationModel char_oxidation_model( const std::string& modelName );

} // namespace CharOxidation

#endif /* CharOxidationBase_h */
