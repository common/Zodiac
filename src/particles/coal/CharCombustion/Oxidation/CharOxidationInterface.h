#ifndef CharOxidationInterface_h
#define CharOxidationInterface_h


/**
 *  @file CharOxidationInterface.h
 *  @author Hang Zhou
 *
 */
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <particles/coal/CharCombustion/CharData.h>

#include "CharOxidationBase.h"

namespace CharOxidation{

  /**
   *  @class CharInterface
   *  @ingroup CharOxidation
   *  @brief Provides an interface to the char oxidation models
   *
   */

  class CharOxidationInterface
  {
      const Coal::CoalType coaltype_;
      const CharOxidationModel charoxidamodel_;
      const Char::CharCombustionData charData_;

      std::vector<std::string> particleNumArray_;
      std::vector<std::string> speciesCharArray_;
      std::vector<std::string> binarySpeciesArray_;
      std::vector<std::string> partNumSpeciesCharArray_;
      const YAML::Node& rootParser_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      const bool isRestart_;

  public:
    /**
    * @param coalType coal type
    * @param charOxiModel char oxidation model
    * @param particleNumArray list of particle numbers with different sizes
    * @param speciesCharArray list of species involved in char reaction
    * @param binarySpeciesArray list of species combination for calculating binary diffusion coefficients
    * @param partNumSpeciesCharArray list of combination of particle number index and species index
    * @param rootParser yaml node read from input file
    * @param integrator time integrator used in Zodiac
    * @param isRestart if restart or not (bool)
    */
    CharOxidationInterface( const Coal::CoalType coalType,
                            const CharOxidationModel charOxiModel,
                            std::vector<std::string> particleNumArray,
                            std::vector<std::string> speciesCharArray,
                            std::vector<std::string> binarySpeciesArray,
                            std::vector<std::string> partNumSpeciesCharArray,
                            const YAML::Node& rootParser,
                            Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                            const bool isRestart);
    ~CharOxidationInterface();

     /**
      * @brief Adding required variables needed to restart in char oxidation model
      * @param restartVars variables needed to restart
      */
      void restart_var_coal_char_oxidation(std::vector<std::string>& restartVars);

     /**
      * @brief Initialize char oxidation model
      */
      void initialize_coal_char_oxidation();
     /**
     * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in char oxidation model
     * @param primVarIdxMap primitive variables indices
     * @param consVarIdxMap solved variables indices
     * @param kinRhsIdxMap kinetic rhs indices
     * @param rhsIdxMap rhs tags indices
     */
      void modify_idxmap_char_oxidation( std::map<Expr::Tag, int> &primVarIdxMap,
                                         const std::map<Expr::Tag, int> &consVarIdxMap,
                                         std::map<Expr::Tag, int> &kinRhsIdxMap,
                                         const  std::map<Expr::Tag, int> &rhsIdxMap);
      /**
       * @brief Modify particle state transformation matrix
       * @param dVdUPart particle state transformation matrix
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       */
      void dvdu_coal_char_oxidation( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                     std::map<Expr::Tag, int> primVarIdxMap,
                                     std::map<Expr::Tag, int> consVarIdxMap);
      /**
       * @brief Modify particle mixing rhs matrix (dmixPartdV)
       * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       */
      void dmMixingdv_coal_char_oxidation( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                           std::map<Expr::Tag, int> primVarIdxMap,
                                           std::map<Expr::Tag, int> rhsIdxMap,
                                           std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  private:
     /**
      * @brief Set the initial conditions for the char oxidation model
      */
      void setup_initial_conditions_char_oxidation();

      /**
       * @brief Register all expressions required to implement the char oxidation model
       */
      void build_equation_system_coal_char_oxidation();

     /**
      * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in char oxidation model
      */
      void modify_prim_kinrhs_tags_char_oxidation();
  };

} // namespace Char

#endif // CharOxidationInterface_h
