
#include <stdexcept>
#include <sstream>
#include <expression/Expression.h>

#include "ReactorEnsembleUtil.h"
#include "particles/ParticleInterface.h"
#include "particles/coal/CoalInterface.h"

#include "CharOxidationInterface.h"
#include "CharOxidationBase.h"
#include "LH_Fractal/CharOxidationLH_Fractal.h"
#include "LH_Fractal/CharOxidationFunctionsLH_Fractal.h"

namespace CharOxidation {

  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef CharOxidation::CharOxidationLHFractal<FieldT>::Builder LHFractalT;

  CharOxidationInterface::
  CharOxidationInterface( const Coal::CoalType coalType,
                          const CharOxidationModel charoxidamodel,
                          std::vector<std::string> particleNumArray,
                          std::vector<std::string> speciesCharArray,
                          std::vector<std::string> binarySpeciesArray,
                          std::vector<std::string> partNumSpeciesCharArray,
                          const YAML::Node& rootParser,
                          Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                          const bool isRestart)
        : coaltype_( coalType ),
          charoxidamodel_( charoxidamodel ),
          particleNumArray_( particleNumArray ),
          speciesCharArray_( speciesCharArray ),
          binarySpeciesArray_( binarySpeciesArray ),
          partNumSpeciesCharArray_( partNumSpeciesCharArray ),
          rootParser_(rootParser),
          integrator_( integrator ),
          isRestart_( isRestart ),
          charData_( coalType, rootParser["Particles"] )
  {
    if(!isRestart_){
      setup_initial_conditions_char_oxidation();
    }
    build_equation_system_coal_char_oxidation();
    modify_prim_kinrhs_tags_char_oxidation();
  }

  //---------------destructor---------------------------
  CharOxidationInterface::~CharOxidationInterface(){
    std::cout << "Delete ~CharOxidationInterface(): " << this << std::endl;
  }

  //------------------------------------------------------------------
  void CharOxidationInterface::setup_initial_conditions_char_oxidation()
  {}

  void CharOxidationInterface::build_equation_system_coal_char_oxidation(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Char::CharTags tagsEChar( Expr::STATE_NONE, particleNumArray_, speciesCharArray_,
                                    binarySpeciesArray_, partNumSpeciesCharArray_ );
    Expr::ExpressionFactory& execFactory = integrator_->factory();
    const int nparsize = particleNumArray_.size();

    if( charoxidamodel_ == OFF ){
      for( size_t i = 0;i < nparsize;i++ ){
        execFactory.register_expression( new ConstantT( tagsEChar.charOxiRhsTags[i], 0.0 ));
        execFactory.register_expression( new ConstantT( tagsEChar.co2CoRatioTags[i], 0.0 ));
      }
    }

    if( charoxidamodel_ == LH or charoxidamodel_ == FRACTAL){
      for( size_t i = 0;i < nparsize;i++ ){
        Expr::TagList oxiTags;
        oxiTags.clear();
        oxiTags.push_back( tagsEChar.charOxiRhsTags[i] );
        oxiTags.push_back( tagsEChar.co2CoRatioTags[i] );
        oxiTags.push_back( tagsEChar.qTags[i] );
        oxiTags.push_back( tagsEChar.po2sTags[i] );
        const int IdxO2  = CanteraObjects::species_index("O2");

        execFactory.register_expression( new LHFractalT( oxiTags, tagsEPart.partSizeTags[i], tagsEPart.partTempTags[i], tagsEPart.partNumFracTags[i], tagsE.tempTag,
                                                         tagsE.massTags[IdxO2], tagsE.mmwTag, tagsEPart.partRhoTags[i], tagsECoal.charMassFracTags[i], tagsECoal.charMassFracInitialTags[i],
                                                         tagsE.presTag, tagsEPart.partMassFracInitialTags[i], charData_, charoxidamodel_));
      }
    }
    if( charoxidamodel_ == FIRST_ORDER ){}
  }

  //---------------------------------------------------------------------------
  void CharOxidationInterface::initialize_coal_char_oxidation()
  {}

  //---------------------------------------------------------------------------
  void CharOxidationInterface::modify_prim_kinrhs_tags_char_oxidation()
  {}

  //---------------------------------------------------------------------------
  void CharOxidationInterface::modify_idxmap_char_oxidation( std::map<Expr::Tag, int>& primVarIdxMap,
                                                             const std::map<Expr::Tag, int>& consVarIdxMap,
                                                             std::map<Expr::Tag, int>& kinRhsIdxMap,
                                                             const std::map<Expr::Tag, int>& rhsIdxMap )
  {}

  //---------------------------------------------------------------------------
  void CharOxidationInterface::dvdu_coal_char_oxidation( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                                         std::map<Expr::Tag, int> primVarIdxMap,
                                                         std::map<Expr::Tag, int> consVarIdxMap )
  {}

  //---------------------------------------------------------------------------
  void CharOxidationInterface::dmMixingdv_coal_char_oxidation( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                                               std::map<Expr::Tag, int> primVarIdxMap,
                                                               std::map<Expr::Tag, int> rhsIdxMap,
                                                               std::map<Expr::Tag, Expr::Tag> consVarRhsMap )
  {}

  //---------------------------------------------------------------------------------------------------------------
  void CharOxidationInterface::restart_var_coal_char_oxidation( std::vector<std::string>& restartVars )
  {}
}
