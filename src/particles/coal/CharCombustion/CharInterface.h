#ifndef CharInterface_h
#define CharInterface_h

/**
 *  @file CharInterface.h
 *  @author Hang Zhou
 */

#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include <expression/ExpressionID.h>

#include "Gasification/CharGasificationBase.h"
#include "Gasification/CharGasificationInterface.h"
#include "Oxidation/CharOxidationBase.h"
#include "Oxidation/CharOxidationInterface.h"
#include "CharBase.h"
#include "CharData.h"

namespace Char{
  /**
   * @class CharTags
   * @ingroup Char
   * @brief Getting tags used in char reaction
   * @author Hang Zhou
   */
  class CharTags {
  public:
      const Expr::Tag charMassGasSourceTag,       ///< gas phase mass source term from char reaction
                      charHeatGasSourceTag,       ///< gas phase energy source term from char reaction
                      charHeatGasSourcePFRTag,    ///< gas phase energy source term from char reaction for plug flow reactor
                      charHeatGasSourceDpDxTag,   ///< gas phase energy source term in `Dp/Dx` from char reaction for plug flow reactor
                      charMassGasRHSTag,          ///< rhs in gas phase mass equation from char reaction
                      charHeatGasRHSTag;          ///< rhs in gas phase energy equation from char reaction
      Expr::TagList qTags,                        ///< char oxidization rate
                    po2sTags,                     ///< particle pressure of oxygen at particle surface
                    co2CoRatioTags,               ///< ratio of CO2/CO
                    charOxiRhsTags,               ///< char oxidation rhs
                    heteroCO2Tags,                ///< CO2 from gasification of char
                    heteroH2OTags,                ///< H2O from gasification of char
                    charSpeciesTags,              ///< production of species from char gas/oxi
                    charSpeciesSourceTags,        ///< gas species source term from char gas/oxi
                    charSpeciesSourcePFRTags,     ///< gas species source term from char gas/oxi for plug flow reactor
                    charSpeciesRHSTags,           ///< rhs in species mass fraction from char gas/oxi
                    enthTgasCharTags,             ///< enthalpy of species used in char model under gas temperature
                    enthTpartCharTags,            ///< enthalpy of species used in char model under particle temperature
                    charSpecTransHeatTags,        ///< total enthalpy of species produced/consumed in char model
                    charHeatReleaseGasTags,       ///< total heat release from char model to gas
                    charHeatReleasePartTags,      ///< (heat release from char model to particle)/(mp*Cp_p)
                    binaryDiffCoeffTags,          ///< binary diffusion coefficients: [CO2 CO O2 H2 H2O CH4 N2]
                    effecDiffCoeffTags;           ///< effective diffusion coefficients: [CO2 CO O2 H2 H2O CH4]
      CharTags( const Expr::Context& state,
                const std::vector<std::string>& particleNumArray,
                const std::vector<std::string>& speciesCharArray,
                const std::vector<std::string>& binarySpeciesArray,
                const std::vector<std::string>& partNumSpeciesCharArray);

  };

  /**
   *  @class CharInterface
   *  @ingroup Char
   *  @brief Provides an interface to the Char models
   *
   */
  class CharInterface
  {
      const Coal::CoalType coaltype_;
      const Char::CharModel charmodel_;
      CharOxidation::CharOxidationModel charoxidamodel_;
      CharGasification::CharGasificationModel chargasifmodel_;
      CharGasification::CharGasificationInterface* gasification_;
      CharOxidation::CharOxidationInterface* oxidation_;
      const Char::CharCombustionData charData_;

      std::vector<std::string> particleNumArray_;
      std::vector<std::string> speciesCharArray_;
      std::vector<std::string> binarySpeciesArray_;
      std::vector<std::string> partNumSpeciesCharArray_;
      const YAML::Node& rootParser_;
      std::set<Expr::ExpressionID>& initRoots_;
      Expr::ExpressionFactory& initFactory_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      Expr::TagList& primitiveTags_;
      Expr::TagList& kinRhsTags_;
      int& equIndex_;
      const bool isRestart_;

  public:
    /**
     * @param coalType coal type
     * @param charModel char model
     * @param particleNumArray list of particle numbers with different sizes
     * @param speciesCharArray list of species involved in char reaction
     * @param binarySpeciesArray list of species combination for calculating binary diffusion coefficients
     * @param partNumSpeciesCharArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
    CharInterface( const Coal::CoalType coalType,
                   const Char::CharModel charModel,
                   std::vector<std::string> particleNumArray,
                   std::vector<std::string> speciesCharArray,
                   std::vector<std::string> binarySpeciesArray,
                   std::vector<std::string> partNumSpeciesCharArray,
                   const YAML::Node& rootParser,
                   std::set<Expr::ExpressionID>& initRoots,
                   Expr::ExpressionFactory& initFactory,
                   Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                   Expr::TagList& primitiveTags,
                   Expr::TagList& kinRhsTags,
                   int& equIndex,
                   const bool isRestart);
    ~CharInterface();

      /**
       * @brief Adding required variables needed to restart in char model
       * @param restartVars variables needed to restart
       */
      void restart_var_coal_char(std::vector<std::string>& restartVars);

      /**
       * @brief Initialize char model
       */
      void initialize_coal_char();

      /**
       * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in char model
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       * @param kinRhsIdxMap kinetic rhs indices
       * @param rhsIdxMap rhs tags indices
       */
      void modify_idxmap_char( std::map<Expr::Tag, int> &primVarIdxMap,
                               const std::map<Expr::Tag, int> &consVarIdxMap,
                               std::map<Expr::Tag, int> &kinRhsIdxMap,
                               const  std::map<Expr::Tag, int> &rhsIdxMap);
      /**
       * @brief Modify particle state transformation matrix
       * @param dVdUPart particle state transformation matrix
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       */
      void dvdu_coal_char( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                           std::map<Expr::Tag, int> primVarIdxMap,
                           std::map<Expr::Tag, int> consVarIdxMap);

      /**
       * @brief Modify particle mixing rhs matrix (dmixPartdV)
       * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       */
      void dmMixingdv_coal_char( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                 std::map<Expr::Tag, int> primVarIdxMap,
                                 std::map<Expr::Tag, int> rhsIdxMap,
                                 std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  private:
     /**
      * @brief Set the initial conditions for the char model
      */
      void setup_initial_conditions_char();

      /**
       * @brief Register all expressions required to implement the char model and add equation into the time integrator
       */
      void build_equation_system_coal_char();

      /**
       * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in char model
       */
      void modify_prim_kinrhs_tags_char();
  };

} // namespace Char

#endif // CharInterface_h
