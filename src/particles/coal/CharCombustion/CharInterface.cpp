
#include <stdexcept>
#include <sstream>
#include <expression/Expression.h>
#include <pokitt/thermo/Enthalpy.h>
#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "particles/ParticleTransformExpressions.h"
#include "particles/ParticleInterface.h"
#include "particles/coal/CoalInterface.h"
#include "CharInterface.h"
#include "CO2andCO_RHS.h"
#include "H2andH2ORHS.h"
#include "O2RHS.h"
#include "CharHeatRelease.h"
#include "CharSpeciesTransportHeat.h"
#include "EffecDiffCoeff.h"
#include "BinaryDiffCoeff.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

namespace Char {

  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef pokitt::SpeciesEnthalpy<FieldT>::Builder SpecEnthT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivisionT;
  typedef Particles::PartToGasCV<FieldT>::Builder PartToGasCVT;
  typedef Particles::GasMassSourceFromPart<FieldT>::Builder GasMassSourceT;
  typedef Particles::SpeciesSourceFromPart<FieldT>::Builder SpeciesSourceT;
  typedef Particles::DpDxSourceFromGasEnthalpyChange<FieldT>::Builder DpDxEnthalpyT;

  typedef Char::BinaryDiffCoeff<FieldT>::Builder BinaryDiffCoeffT;
  typedef Char::EffecDiffCoeff<FieldT>::Builder EffecDiffCoeffT;
  typedef Char::CO2andCORHS<FieldT>::Builder CO2andCORHST;
  typedef Char::H2andH2ORHS<FieldT>::Builder H2andH2ORHST;
  typedef Char::O2RHS<FieldT>::Builder O2RHST;
  typedef Char::CharHeatRelease<FieldT>::Builder CharHeatReleaseT;
  typedef Char::CharSpeciesTransportHeat<FieldT>::Builder CharSpeciesTransportHeatT;

  CharTags::
  CharTags( const Expr::Context& state,
            const std::vector<std::string>& particleNumArray,
            const std::vector<std::string>& speciesCharArray,
            const std::vector<std::string>& binarySpeciesArray,
            const std::vector<std::string>& partNumSpeciesCharArray )
        : charMassGasSourceTag    ( Expr::Tag( "char_mass_gas_source"     , state )),
          charHeatGasSourceTag    ( Expr::Tag( "char_heat_gas_source"     , state )),
          charHeatGasSourcePFRTag ( Expr::Tag( "char_heat_gas_source_pfr" , state )),
          charHeatGasSourceDpDxTag( Expr::Tag( "char_heat_gas_source_dpdx", state )),
          charMassGasRHSTag       ( Expr::Tag( "char_mass_gas_rhs"        , state )),
          charHeatGasRHSTag       ( Expr::Tag( "char_heat_gas_rhs"        , state ))
          {
              qTags                      = Expr::tag_list( particleNumArray,        state, "q_"                       );
              po2sTags                   = Expr::tag_list( particleNumArray,        state, "p02s_"                    );
              co2CoRatioTags             = Expr::tag_list( particleNumArray,        state, "co2CoRatio_"              );
              charOxiRhsTags             = Expr::tag_list( particleNumArray,        state, "charOxiRhs_"              );
              heteroCO2Tags              = Expr::tag_list( particleNumArray,        state, "heteroCO2_"               );
              heteroH2OTags              = Expr::tag_list( particleNumArray,        state, "heteroH2O_"               );
              charSpeciesTags            = Expr::tag_list( partNumSpeciesCharArray, state, "char_species_"            );
              charSpeciesSourceTags      = Expr::tag_list( speciesCharArray,        state, "char_species_source_"     );
              charSpeciesSourcePFRTags   = Expr::tag_list( speciesCharArray,        state, "char_species_source_pfr_" );
              charSpeciesRHSTags         = Expr::tag_list( speciesCharArray,        state, "char_species_rhs_"        );
              enthTgasCharTags           = Expr::tag_list( partNumSpeciesCharArray, state, "enthTgasChar_"            );
              enthTpartCharTags          = Expr::tag_list( partNumSpeciesCharArray, state, "enthTpartChar_"           );
              charSpecTransHeatTags      = Expr::tag_list( particleNumArray,        state, "charSpeciesTransHeat_"    );
              charHeatReleaseGasTags     = Expr::tag_list( particleNumArray,        state, "charHeatReleaseGas_"      );
              charHeatReleasePartTags    = Expr::tag_list( particleNumArray,        state, "charHeatReleaseGPart_"    );
              binaryDiffCoeffTags        = Expr::tag_list( binarySpeciesArray,      state, "binaryDiffCoeff_"         );
              effecDiffCoeffTags         = Expr::tag_list( speciesCharArray,        state, "effecDiffCoeff_"          );
          }

  CharInterface::
  CharInterface( const Coal::CoalType coalType,
                 const Char::CharModel charModel,
                 std::vector<std::string> particleNumArray,
                 std::vector<std::string> speciesCharArray,
                 std::vector<std::string> binarySpeciesArray,
                 std::vector<std::string> partNumSpeciesCharArray,
                 const YAML::Node& rootParser,
                 std::set<Expr::ExpressionID>& initRoots,
                 Expr::ExpressionFactory& initFactory,
                 Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                 Expr::TagList& primitiveTags,
                 Expr::TagList& kinRhsTags,
                 int& equIndex,
                 const bool isRestart)
        : coaltype_( coalType ),
          charmodel_( charModel ),
          particleNumArray_( particleNumArray ),
          speciesCharArray_( speciesCharArray ),
          binarySpeciesArray_( binarySpeciesArray ),
          partNumSpeciesCharArray_( partNumSpeciesCharArray ),
          rootParser_( rootParser ),
          initRoots_( initRoots ),
          initFactory_( initFactory ),
          integrator_( integrator ),
          primitiveTags_( primitiveTags ),
          kinRhsTags_( kinRhsTags ),
          equIndex_( equIndex ),
          isRestart_( isRestart ),
          charData_( coalType, rootParser["Particles"] ){

    // [CO2 CO O2 H2 H2O CH4 N2]
    speciesCharArray_.clear();
    speciesCharArray_.push_back( "CO2");
    speciesCharArray_.push_back( "CO" );
    speciesCharArray_.push_back( "O2" );
    speciesCharArray_.push_back( "H2" );
    speciesCharArray_.push_back( "H2O");
    speciesCharArray_.push_back( "CH4");

    binarySpeciesArray_.clear();
    for( size_t i = 0;i < speciesCharArray_.size();i++ ){
      for( size_t j = 0;j < speciesCharArray_.size();j++ ){
        binarySpeciesArray_.push_back( speciesCharArray_[i] + "_" + speciesCharArray_[j] );
      }
      binarySpeciesArray_.push_back( speciesCharArray_[i] + "_N2" );
    }
    for( size_t i = 0;i < speciesCharArray_.size();i++ ){
      binarySpeciesArray_.push_back( "N2_" + speciesCharArray_[i] );
    }
    binarySpeciesArray_.push_back( "N2_N2" );

    partNumSpeciesCharArray_.clear();
    for( size_t i = 0;i < particleNumArray_.size();i++ ){
      std::string I = std::to_string( i );
      for( size_t j = 0;j < speciesCharArray_.size();j++ ){
        partNumSpeciesCharArray_.push_back( speciesCharArray_[j] + "_" + I );
      }
    }

    switch (charmodel_){
      case INVALID_CHARMODEL:
        throw std::invalid_argument( "Invalid model in Char Interface constructor" );
        break;
      case OFF:
        std::cout << "Char model is turned off" << std::endl;
        break;
      case GASIF_OXID:
        chargasifmodel_ = CharGasification::char_gasification_model( rootParser_["Particles"]["CharModel"]["GasificationModel"].as<std::string>() );
        charoxidamodel_ = CharOxidation::char_oxidation_model( rootParser_["Particles"]["CharModel"]["OxidationModel"].as<std::string>() );
        gasification_ = new CharGasification::CharGasificationInterface( coaltype_, chargasifmodel_, particleNumArray_, speciesCharArray_, binarySpeciesArray_,
                                                                         partNumSpeciesCharArray_, rootParser_, integrator_, isRestart_);
        oxidation_ = new CharOxidation::CharOxidationInterface( coaltype_, charoxidamodel_, particleNumArray_, speciesCharArray_, binarySpeciesArray_,
                                                                partNumSpeciesCharArray_, rootParser_, integrator_, isRestart_);
        break;
      case CCK:
        break;
    }

    if(!isRestart_){
      setup_initial_conditions_char();
    }
    build_equation_system_coal_char();
    modify_prim_kinrhs_tags_char();
  }

  //---------------destructor---------------------------
  CharInterface::~CharInterface(){
    std::cout << "Delete ~CharInterface(): " << this << std::endl;
    if( charmodel_ == GASIF_OXID ){
      delete gasification_;
      delete oxidation_;
    }
    else if(charmodel_ == CCK){}
  }

  //------------------------------------------------------------------
  void CharInterface::setup_initial_conditions_char()
  {}

  void CharInterface::build_equation_system_coal_char(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Char::CharTags tagsEChar( Expr::STATE_NONE, particleNumArray_, speciesCharArray_,
                                    binarySpeciesArray_, partNumSpeciesCharArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );
    Expr::ExpressionFactory& execFactory = integrator_->factory();
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    const int nparsize = particleNumArray_.size();

    execFactory.register_expression( new BinaryDiffCoeffT( tagsEChar.binaryDiffCoeffTags, tagsE.presTag, tagsE.tempTag ));
    execFactory.register_expression( new EffecDiffCoeffT( tagsEChar.effecDiffCoeffTags, tagsE.massTags, tagsEChar.binaryDiffCoeffTags,
                                                          tagsE.mmwTag, charData_ ));

    execFactory.register_expression( new ConstantT( tagsEChar.charHeatGasSourceTag, 0.0 ));
    for( size_t i = 0;i < nparsize;i++ ){
      // modify char equations
      execFactory.attach_dependency_to_expression( tagsEChar.charOxiRhsTags[i], tagsECoal.charMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.heteroH2OTags[i], tagsECoal.charMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.heteroCO2Tags[i], tagsECoal.charMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );

      // modify particle mass equations
      execFactory.attach_dependency_to_expression( tagsEChar.charOxiRhsTags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.heteroH2OTags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.heteroCO2Tags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );

      //modify gas equations
      const int nspec = speciesCharArray_.size();
      execFactory.register_expression( new CO2andCORHST( Expr::tag_list( tagsEChar.charSpeciesTags[i * nspec + 0], tagsEChar.charSpeciesTags[i * nspec + 1] ),
                                                         tagsEChar.charOxiRhsTags[i], tagsEChar.co2CoRatioTags[i], tagsEChar.heteroCO2Tags[i], tagsEChar.heteroH2OTags[i] ));
      execFactory.register_expression( new O2RHST( tagsEChar.charSpeciesTags[i * nspec + 2], tagsEChar.charOxiRhsTags[i], tagsEChar.co2CoRatioTags[i] ));
      execFactory.register_expression( new H2andH2ORHST( Expr::tag_list( tagsEChar.charSpeciesTags[i * nspec + 3],
                                                         tagsEChar.charSpeciesTags[i * nspec + 4] ), tagsEChar.heteroH2OTags[i] ));
      execFactory.register_expression( new ConstantT( tagsEChar.charSpeciesTags[i * nspec + 5], 0.0 ));  // CH4
      for( size_t j = 0;j < nspec;j++ ){
        const int Idx = CanteraObjects::species_index( speciesCharArray_[j] );
        execFactory.register_expression( new SpecEnthT( tagsEChar.enthTpartCharTags[i * nspec + j], tagsEPart.partTempTags[i], Idx ));
        execFactory.register_expression( new SpecEnthT( tagsEChar.enthTgasCharTags[i * nspec + j], tagsE.tempTag, Idx ));
      }
      execFactory.register_expression( new CharHeatReleaseT( Expr::tag_list( tagsEChar.charHeatReleaseGasTags[i], tagsEChar.charHeatReleasePartTags[i] ),
                                                             tagsEChar.charOxiRhsTags[i], tagsEChar.co2CoRatioTags[i], tagsEChar.heteroCO2Tags[i], tagsEChar.heteroH2OTags[i],
                                                             tagsEPart.partMassFracTags[i], tagsEPart.partCpTags[i] ));
      execFactory.register_expression( new CharSpeciesTransportHeatT( tagsEChar.charSpecTransHeatTags[i], tagsEChar.enthTpartCharTags,
                                                                      tagsEChar.enthTgasCharTags, tagsEChar.charOxiRhsTags[i],
                                                                      tagsEChar.charSpeciesTags[i * nspec + 2], tagsEChar.co2CoRatioTags[i],
                                                                      tagsEChar.heteroCO2Tags[i], tagsEChar.heteroH2OTags[i], nspec, i ));
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatReleaseGasTags[i], tagsEChar.charHeatGasSourceTag, Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.charSpecTransHeatTags[i], tagsEChar.charHeatGasSourceTag, Expr::ADD_SOURCE_EXPRESSION );
      // modify particle temperature equation
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatReleasePartTags[i], tagsEPart.partTempKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION );
    }

    execFactory.register_expression( new GasMassSourceT( tagsEChar.charMassGasSourceTag, tagsEChar.charSpeciesTags ));
    execFactory.register_expression( new SpeciesSourceT( tagsEChar.charSpeciesSourceTags, tagsEChar.charSpeciesTags, speciesCharArray_.size()));
    execFactory.attach_dependency_to_expression( tagsEChar.charMassGasSourceTag, tagsEPart.partToGasMassTag, Expr::SUBTRACT_SOURCE_EXPRESSION );

    if(  reactorType == "PSRConstantVolume" ){
      for( size_t j = 0;j < speciesCharArray_.size();j++ ){
        const int Idx = CanteraObjects::species_index( speciesCharArray_[j] );
        execFactory.register_expression( new PartToGasCVT( tagsEChar.charSpeciesRHSTags[j], tagsEChar.charSpeciesSourceTags[j], tagsE.rhoTag ));
        execFactory.attach_dependency_to_expression( tagsEChar.charSpeciesRHSTags[j], tagsE.rhoYKinRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
      execFactory.register_expression( new PartToGasCVT( tagsEChar.charMassGasRHSTag, tagsEChar.charMassGasSourceTag, tagsE.rhoTag ));
      execFactory.register_expression( new PartToGasCVT( tagsEChar.charHeatGasRHSTag, tagsEChar.charHeatGasSourceTag, tagsE.rhoTag ));
      execFactory.attach_dependency_to_expression( tagsEChar.charMassGasRHSTag, tagsE.rhoKinRhsTag, Expr::SUBTRACT_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatGasRHSTag, tagsE.rhoEgyKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
    }
    if( reactorType == "PSRConstantPressure" ){
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatGasSourceTag, tagsE.enthKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      for( size_t j = 0;j < speciesCharArray_.size();j++ ){
        const int Idx = CanteraObjects::species_index( speciesCharArray_[j] );
        execFactory.attach_dependency_to_expression( tagsEChar.charSpeciesSourceTags[j], tagsE.yKinRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
    }
    if( reactorType == "PFR"){
      execFactory.register_expression( new DivisionT( tagsEChar.charHeatGasSourcePFRTag, tagsEChar.charHeatGasSourceTag, tagsEpfr.uTag ));
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatGasSourcePFRTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      for( size_t j = 0;j < speciesCharArray_.size();j++ ){
        const int Idx = CanteraObjects::species_index( speciesCharArray_[j] );
        execFactory.register_expression( new DivisionT( tagsEChar.charSpeciesSourcePFRTags[j], tagsEChar.charSpeciesSourceTags[j], tagsEpfr.uTag ));
        execFactory.attach_dependency_to_expression( tagsEChar.charSpeciesSourcePFRTags[j], tagsE.yFullRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
      execFactory.register_expression( new DpDxEnthalpyT( tagsEChar.charHeatGasSourceDpDxTag, tagsEChar.charHeatGasSourceTag, tagsE.rhoTag, tagsE.tempTag,
                                                          tagsE.enthTag, tagsE.mmwTag, tagsE.cpTag, tagsEpfr.uTag));
      execFactory.attach_dependency_to_expression( tagsEChar.charHeatGasSourceDpDxTag, tagsEpfr.dpdxTag, Expr::ADD_SOURCE_EXPRESSION );
    }
  }

  //---------------------------------------------------------------------------
  void CharInterface::initialize_coal_char(){
    if( charmodel_ == GASIF_OXID ){
      gasification_->initialize_coal_char_gasification();
      oxidation_->initialize_coal_char_oxidation();
    }
  }

  //---------------------------------------------------------------------------
  void CharInterface::modify_prim_kinrhs_tags_char()
  {}

  //---------------------------------------------------------------------------
  void CharInterface::modify_idxmap_char( std::map<Expr::Tag, int>& primVarIdxMap,
                                          const std::map<Expr::Tag, int>& consVarIdxMap,
                                          std::map<Expr::Tag, int>& kinRhsIdxMap,
                                          const std::map<Expr::Tag, int>& rhsIdxMap ){
    if( charmodel_ == GASIF_OXID ){
      gasification_->modify_idxmap_char_gasification( primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap );
      oxidation_->modify_idxmap_char_oxidation( primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap );
    }
  }

  //---------------------------------------------------------------------------
  void CharInterface::dvdu_coal_char( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                      std::map<Expr::Tag, int> primVarIdxMap,
                                      std::map<Expr::Tag, int> consVarIdxMap ){
    if( charmodel_ == GASIF_OXID ){
      gasification_->dvdu_coal_char_gasification( dVdUPart, primVarIdxMap, consVarIdxMap );
      oxidation_->dvdu_coal_char_oxidation( dVdUPart, primVarIdxMap, consVarIdxMap );
    }
  }

  //---------------------------------------------------------------------------
  void CharInterface::dmMixingdv_coal_char( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                            std::map<Expr::Tag, int> primVarIdxMap,
                                            std::map<Expr::Tag, int> rhsIdxMap,
                                            std::map<Expr::Tag, Expr::Tag> consVarRhsMap ){
    if( charmodel_ == GASIF_OXID ){
      gasification_->dmMixingdv_coal_char_gasification( dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap );
      oxidation_->dmMixingdv_coal_char_oxidation( dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap );
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CharInterface::restart_var_coal_char(std::vector<std::string>& restartVars){
    if( charmodel_ == GASIF_OXID ){
      gasification_->restart_var_coal_char_gasification( restartVars );
      oxidation_->restart_var_coal_char_oxidation( restartVars );
    }
  }
}
