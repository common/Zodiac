
/**
 * @file CharHeatRelease.h
 * @par Calculate heat release to gas and particle due to char reactions.
 * @author Hang Zhou
 */

#ifndef CharHeatRelease_CHAR_h
#define CharHeatRelease_CHAR_h

#include <expression/Expression.h>
#include "particles/coal/CoalData.h"

namespace Char {
/**
 *  @class CharHeatRelease
 *  @ingroup char
 *  @brief Calculate heat release to gas and particle due to char reactions for one particle size.
 *        (unit: gas: J/kg/s; particle: K/s)
 *
 *  The heat release comes from three part：
 *  <ul>
 *     <li> Reaction heat of char oxidation: (Exothermic)
 *          \f[ Q_{1,gas} = - \alpha(r_{ox,CO2} \Delta H_{ox,CO2} + r_{ox,CO} \Delta H_{ox,CO}) \f]
 *         \f[ Q_{1,part} = - (1-\alpha)(r_{ox,CO2} \delta H_{ox,CO2} + r_{ox,CO} \delta H_{ox,CO}) \f]
 *         Here, \f$\alpha\f$ is the fraction of heat absorbed/released by gas in the total heat release from char reaction.
 *               \f$\Delta_H_{ox,CO2}\f$ and \f$\Delta H_{ox,CO} are the reaction heats of char oxidation productng CO2 and CO respectively.
 *               \f$r_{ox,CO2}=r_{ox} \frac{CO2/CO}{CO2/CO+1}\f$ is the reaction rate of char oxidation producing CO2.
 *               \f$r_{ox,CO}=r_{ox} \frac{1}{CO2/CO+1}\f$ is the reaction rate of char oxidation producing CO.
 *               \f$r_{ox}\f$ is the total reaction rate of char oxidation producing both CO2 and CO.
 *               \f$CO2/CO\f$ is the produced ratio of CO2 and CO from char oxidation.
 *     </li>
 *     <li> Reaction heat of char gasification with CO2: (Endothermic)
 *          \f[ Q_{2,gas} = \alpha(r_{gasif,CO2} \Delta H_{gasif,CO2}\f]
 *          \f[ Q_{2,part} = (1-\alpha)(r_{gasif,CO2} \Delta H_{gasif,CO2}\f]
 *          Here, \f$r_{gasif,CO2}\f$ are the char gasification rate reacting with CO2 per gas mass.
 *                \f$\Delta H_{gasif,CO2}\f$ are the reaction heat of char gasification reacting with CO2.
 *     </li>
 *     <li> Reaction heat of char gasification with H2O: (Endothermic)
 *          \f[ Q_{3,gas} = \alpha(r_{gasif,H2O} \Delta H_{gasif,H2O}\f]
 *          \f[ Q_{3,part} = (1-\alpha)(r_{gasif,H2O} \Delta H_{gasif,H2O}\f]
 *          Here, \f$r_{gasif,H2O}\f$ are the char gasification rate reacting with H2O per gas mass.
 *                \f$\Delta H_{gasif,H2O}\f$ are the reaction heat of char gasification reacting with H2O.
 *     </li>
 *  </ul>
 *
 *  The final source terms are given as:
 *  \f[
 *     S_{gas} = Q_{1,gas}+Q_{2,gas}+Q_{3,gas}
 *  \f]
 *  \f[
 *     S_{part} = \frac{Q_{1,part}+Q_{2,part}+Q_{3,part}}{Cp_,p m_p}
 *  \f]
 *  Here, \f$Cp,p\f$ is the heat capacity of particle.
 *        \f$m_p\f$ is the mass of particle per gas mass for one particle size.
 *
 *  NOTE: 1. This expression calculates the heat release from all particles with ONE specific particle size.
 *           All the parameters used here are the value for ONE specific particle size.
 *        2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 *
 *  Reference:
 *  [1] The Combustion Rates of Coal Chars : A Review, L. W. Smith, symposium on combustion 19, 1982, pp 1045-1065
 *  [2] Watanabe, H, and M Otaka. Numerical simulation of coal gasification in entrained flow coal gasifier
 *      Fuel 85, no. 12-13 (September 2006): 1935-1943.  http://linkinghub.elsevier.com/retrieve/pii/S0016236106000548.
 */
template< typename FieldT >
class CharHeatRelease
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, charoxidation_, co2coratio_, hetroco2_, hetroh2o_, partmass_, partcp_ )
  const double alpha_, hco_, hco2_;

  CharHeatRelease( const Expr::Tag& charoxidationTag,
                   const Expr::Tag& co2coratioTag,
                   const Expr::Tag& hetroco2Tag,
                   const Expr::Tag& hetroh2oTag,
                   const Expr::Tag& partmassTag,
                   const Expr::Tag& partcpTag)
        : Expr::Expression<FieldT>(),
          alpha_ (1.0 - Coal::absored_heat_fraction_particle()),
          hco_   (9629.64E3), // J/kg
          hco2_  (33075.72E3) // J/kg [1]
  {
    this->set_gpu_runnable(true);
    charoxidation_ = this->template create_field_request<FieldT>(charoxidationTag);
    co2coratio_    = this->template create_field_request<FieldT>(co2coratioTag),
    hetroco2_      = this->template create_field_request<FieldT>(hetroco2Tag );
    hetroh2o_      = this->template create_field_request<FieldT>(hetroh2oTag );
    partmass_      = this->template create_field_request<FieldT>(partmassTag);
    partcp_        = this->template create_field_request<FieldT>(partcpTag);
  }

public:
  class Builder : public Expr::ExpressionBuilder
  {
      const Expr::Tag charoxidationTag_, co2coratioTag_, hetroco2Tag_, hetroh2oTag_, partmassTag_, partcpTag_;
  public:
    /**
     * @brief The mechanism for buiiding a CharHeatRelease object
     * @tparam FieldT
     * @param charheatreleaseTags List of heat releases to gas and particle due to char reactions
     * @param charoxidationTag char oxidation rate
     * @param co2coratioTag produced ratio of CO2 and CO from char oxidation
     * @param hetroco2Tag gasification rate of char reacts with CO2
     * @param hetroh2oTag gasification rate of char reacts with H2O
     * @param partmassTag particle mass per gas mass for one particle size
     * @param partcpTag particle heat capacity
     */
    Builder( const Expr::TagList& charheatreleaseTags,
             const Expr::Tag& charoxidationTag,
             const Expr::Tag& co2coratioTag,
             const Expr::Tag& hetroco2Tag,
             const Expr::Tag& hetroh2oTag,
             const Expr::Tag& partmassTag,
             const Expr::Tag& partcpTag)
          : ExpressionBuilder(charheatreleaseTags),
            charoxidationTag_( charoxidationTag ),
            co2coratioTag_(co2coratioTag),
            hetroco2Tag_ ( hetroco2Tag  ),
            hetroh2oTag_ ( hetroh2oTag  ),
            partmassTag_ ( partmassTag  ),
            partcpTag_   ( partcpTag    )
    {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{
      return new CharHeatRelease<FieldT>( charoxidationTag_, co2coratioTag_, hetroco2Tag_, hetroh2oTag_, partmassTag_, partcpTag_ );
    }
  };

  void evaluate()
  {
    using namespace SpatialOps;
    typename Expr::Expression<FieldT>::ValVec&  result = this->get_value_vec();
    FieldT& releaseGas = *result[0];
    FieldT& releasePart= *result[1];

    const FieldT& charoxidation = charoxidation_->field_ref();
    const FieldT& co2coratio    = co2coratio_   ->field_ref();
    const FieldT& hetroco2      = hetroco2_     ->field_ref();
    const FieldT& hetroh2o      = hetroh2o_     ->field_ref();
    const FieldT& partmass      = partmass_     ->field_ref();
    const FieldT& partcp        = partcp_       ->field_ref();

    SpatFldPtr<FieldT> oxiCO2 = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    SpatFldPtr<FieldT> oxiCO = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    // Char oxidation - Exothermic
    *oxiCO2 <<= charoxidation * co2coratio / (co2coratio+1.0);
    *oxiCO  <<= charoxidation / (co2coratio+1.0);
    releaseGas <<= - alpha_ * (*oxiCO2 * hco2_ + *oxiCO * hco_);
    releasePart <<= - (1.0 - alpha_) * (*oxiCO2 * hco2_ + *oxiCO * hco_);

    // CO2 Gasification reaction - Endothermic
    releaseGas <<= releaseGas + alpha_ * (hetroco2 * 14.37E6 );
    releasePart <<= releasePart + ( 1.0 - alpha_) * (hetroco2 * 14.37E6 );

    // H2O Gasification reaction - Endothermic
    releaseGas <<= releaseGas + alpha_ * (hetroh2o * 10.94E6 );
    releasePart <<= releasePart + ( 1.0 - alpha_) * (hetroh2o * 10.94E6 );

    releasePart <<= releasePart/partcp/partmass;
  }

  void sensitivity( const Expr::Tag& var){
    using namespace SpatialOps;
    const Expr::TagList& targetTags = this->get_tags();
    typename Expr::Expression<FieldT>::ValVec& result = this->get_value_vec();

    std::vector<FieldT> dreleasedv;
    for( size_t i = 0;i < targetTags.size();++i ){
      dreleasedv.push_back( this->sensitivity_result( targetTags[i], var ));
    }
    const FieldT& charoxidation    = charoxidation_->field_ref();
    const FieldT& co2coratio       = co2coratio_   ->field_ref();
    const FieldT& hetroco2         = hetroco2_     ->field_ref();
    const FieldT& hetroh2o         = hetroh2o_     ->field_ref();
    const FieldT& partmass         = partmass_     ->field_ref();
    const FieldT& partcp           = partcp_       ->field_ref();
    const FieldT& dcharoxidationdv = charoxidation_->sens_field_ref(var);
    const FieldT& dco2coratiodv    = co2coratio_   ->sens_field_ref(var);
    const FieldT& dhetroco2dv      = hetroco2_     ->sens_field_ref(var);
    const FieldT& dhetroh2odv      = hetroh2o_     ->sens_field_ref(var);
    const FieldT& dpartmassdv      = partmass_     ->sens_field_ref(var);
    const FieldT& dpartcpdv        = partcp_       ->sens_field_ref(var);
    SpatFldPtr<FieldT> oxiCO2 = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    SpatFldPtr<FieldT> oxiCO = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    *oxiCO2 <<= charoxidation * co2coratio / (co2coratio+1.0);
    *oxiCO  <<= charoxidation / (co2coratio+1.0);
    SpatFldPtr<FieldT> doxiCO2dv = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    SpatFldPtr<FieldT> doxiCOdv = SpatialFieldStore::get<FieldT,FieldT>( charoxidation );
    *doxiCO2dv <<= (dcharoxidationdv*co2coratio+charoxidation*dco2coratiodv) / (co2coratio+1.0)
                   - charoxidation * co2coratio / square(co2coratio+1.0) * dco2coratiodv;
    *doxiCOdv  <<= dcharoxidationdv / (co2coratio+1.0) - charoxidation /square(co2coratio+1.0) *dco2coratiodv;

    dreleasedv[0] <<= alpha_ * (- *doxiCO2dv * hco2_ - *doxiCOdv * hco_ + dhetroco2dv * 14.37E6 + dhetroh2odv * 10.94E6);
    dreleasedv[1] <<= (1.0 - alpha_) * (- *doxiCO2dv * hco2_ - *doxiCOdv * hco_ + dhetroco2dv * 14.37E6 + dhetroh2odv * 10.94E6) / partcp/partmass
                       - (1.0 - alpha_) *(- *oxiCO2 * hco2_ - *oxiCO * hco_ + hetroco2 * 14.37E6 + hetroh2o * 10.94E6) / square(partcp*partmass)
                         *(partcp*dpartmassdv+dpartcpdv*partmass);

  }
};
} // namespace Char
#endif // CharHeatRelease_CHAR_h
