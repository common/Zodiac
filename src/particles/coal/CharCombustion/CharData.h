#ifndef CharData_h
#define CharData_h

/**
 * @file CharData.h
 * @par Getting required char information for char oxidation and gasification
 */

#include <cmath>
#include <boost/multi_array.hpp>
#include <particles/coal/Devolatilization/CPD/CPDData.h>
#include <particles/coal/CoalData.h>

namespace Char{

  /**
   * @fn double initial_particle_density(double mass, double diameter)
   * @brief Calculating initial particle density based on its initial mass mass recent size
   * @param mass mass of particle
   * @param diameter diameter of particle
   * @return the density of particle
   */
  double initial_particle_density(double mass, double diameter);

  /**
   *  @ingroup Char
   *  @class CharCombustionData
   *  @brief Getting required char information for char oxidation and gasification
   *
   *  
   *  "Study on coal chars combustion under O2/CO2 atmosphere with
   *   fractal random pore model", Hua Fei, Song Hu, Jun Xiang, Lushi
   *   Sun, Peng Fu, Gang Chen, Fuel 90 (2011) 441–448.
   *
   */
  class CharCombustionData {
  public:
    /**
     * @brief Getting required char information for char oxidation and gasification based on given coal type
     * @param coalType specification of the type of coal to consider for the char oxidation model.
     */
    CharCombustionData( const Coal::CoalType coalType,
                        const YAML::Node& particleParser);
    
    /**
     * @fn const Coal::CoalComposition& get_coal_composition()
     * @return the coal composition
     */
    const Coal::CoalComposition& get_coal_composition() const{ return coalComp_; }
   
    /**
     *  @brief returns \f$\varepsilon{0}\f$ - initial porosity of coal particle
     */
    double get_e0() const {return e0_;};

    /**
     *  @brief returns $r_{pore}$ - mean pore radius (m)
     */
    double get_r_pore() const {return rPore_;};

    /**
     *  @brief returns $S_{0}$ - internal surface area of initial char (m2/kg)
     */
    double get_S_0() const {return s0_;};

   /**
     *  @brief returns Oxygen mass fraction of coal
     */
    double get_O() const{ return o_;};

    /**
      *  @brief returns Carbon mass fraction of coal
      */
     double get_C() const{ return c_;};

     /**
      * @brief Returns fixed carbon mass fraction of coal
      */
     double get_fixed_C() const{ return fixedCarbon_;}

     /**
      * @brief Returns volatile mass fraction of coal
      */
     double get_vm() const{ return volatiles_;}

     /**
       *  @brief returns selected coal type
       */
     Coal::CoalType get_coal_type() const{ return sel_;};

     /**
       *  @brief returns molecular weight of all species invloved in char reaction
       */
     const std::vector<double>& get_mwVec() const{ return mwVec_; };

    /**
      *  @brief returns species index of all species invloved in char reaction
      */
     const std::vector<int>& get_indexVec() const{ return indexVec_; };

    /**
     * @brief Returns number of species involved in the char oxidation mechanism
     *        Right now the value is hard coded...similar to CPD data gas phase species data
     *        remember we might change the number of species in the future....
     */
    int get_ncomp() const {return 3 ;};
   
  protected:
    double e0_, rPore_, s0_, o_, c_, fixedCarbon_, volatiles_;
    std::vector<double> mwVec_;
    std::vector<int> indexVec_;
    Coal::CoalType sel_;
    Coal::CoalComposition coalComp_;
    void insufficient_data();
    void set_data();

  };

} // namespace CHAR

#endif  // OxidationFunctions_h
