
/**
 * @file BinaryDiffCoeff.h
 * @par Assembles a vector of binary diffusion coefficients using a mysterious correlation found in the BYU version of the CCK code.
 */
#ifndef BinaryDiffCoeff_Char_h
#define BinaryDiffCoeff_Char_h

#include <expression/Expression.h>

/**
 *  @class BinaryDiffCoeff
 *  @ingroup Char
 *  @brief This function assembles a vector of binary diffusion coefficients
 *         using a mysterious correlation found in the BYU version of the CCK code.
 *
 * Order here is REALLY important!!!
 * [CO2 CO O2 H2 H2O CH4 N2]
 * The list is ordered as:
 * [CO2-CO2, CO2-CO, CO2-O2, CO2-H2, CO2-H2O, CO2-CH4, CO2-N2,
 *  CO-CO2,  CO-CO,  CO-O2,  CO-H2,  CO-H2O,  CO-CH4,  CO-N2,
 *  O2-CO2,  O2-CO,  O2-O2,  O2-H2,  O2-H2O,  O2-CH4,  O2-N2,
 *  H2-CO2,  H2-CO,  H2-O2,  H2-H2,  H2-H2O,  H2-CH4,  H2-N2,
 *  H2O-CO2, H2O-CO, H2O-O2, H2O-H2, H2O-H2O, H2O-CH4, H2O-N2,
 *  CH4-CO2, CH4-CO, CH4-O2, CH4-H2, CH4-H2O, CH4-CH4, CH4-N2,
 *  N2-CO2,  N2-CO,  N2-O2,  N2-H2,  N2-H2O,  N2-CH4,  N2-N2 ]
 *
 */
namespace Char {

  template< typename FieldT >
  class BinaryDiffCoeff
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, pres_, temp_)

    BinaryDiffCoeff( const Expr::Tag& presTag,
                     const Expr::Tag& tempTag)
                 : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      pres_ = this->template create_field_request<FieldT>(presTag );
      temp_ = this->template create_field_request<FieldT>(tempTag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag presTag_, tempTag_;
    public:
      /**
       * @brief The mechanism for building a BinaryDiffCoeff object
       * @tparam FieldT
       * @param binaryDiffCoeffTags a list of binary diffusion coefficients
       * @param presTag gas pressure
       * @param tempTag gas temperature
       */
      Builder( const Expr::TagList& binaryDiffCoeffTags,
               const Expr::Tag& presTag,
               const Expr::Tag& tempTag)
            : ExpressionBuilder(binaryDiffCoeffTags),
              presTag_( presTag ),
              tempTag_ ( tempTag  )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new BinaryDiffCoeff<FieldT>( presTag_, tempTag_);
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
      const FieldT& pres = pres_->field_ref();
      const FieldT& temp = temp_ ->field_ref();

      // temperature and pressure correlations
      SpatFldPtr<FieldT> ftp2 = SpatialFieldStore::get<FieldT,FieldT>( pres );
      SpatFldPtr<FieldT> ftp1 = SpatialFieldStore::get<FieldT,FieldT>( pres );
      *ftp2 <<= pow(temp, 7.0/3.0)*pres/101325.0; // H2O
      *ftp1 <<= pow(temp, 5.0/3.0)*pres/101325.0; // everything else

      // CO2-X
      *results[0] <<= 0.0              ; //CO2-CO2
      *results[1] <<= 1.199e-09 * *ftp1; //CO2-CO
      *results[2] <<= 1.208e-09 * *ftp1; //CO2-O2
      *results[3] <<= 4.713e-09 * *ftp1; //CO2-H2
      *results[4] <<= 2.726e-11 * *ftp2; //CO2-H2O
      *results[5] <<= 1.365e-09 * *ftp1; //CO2-CH4
      *results[6] <<= 1.191e-09 * *ftp1; //CO2-N2

      //CO-X
      *results[7] <<= *results[1]      ; //CO-CO2
      *results[8] <<= 0.0              ; //CO-CO
      *results[9] <<= 1.537e-09 * *ftp1; //CO-O2
      *results[10]<<= 5.487e-09 * *ftp1; //CO-H2
      *results[11]<<= 4.272e-11 * *ftp2; //CO-H2O
      *results[12]<<= 1.669e-09 * *ftp1; //CO-CH4
      *results[13]<<= 1.500e-09 * *ftp1; //CO-N2

      // O2-X
      *results[14] <<= *results[2]      ; //O2-CO2
      *results[15] <<= *results[9]      ; //O2-CO
      *results[16] <<= 0.0              ; //O2-O2
      *results[17] <<= 5.783e-09 * *ftp1; //O2-H2
      *results[18] <<= 4.202e-11 * *ftp2; //O2-H2O
      *results[19] <<= 1.711e-09 * *ftp1; //O2-CH4
      *results[20] <<= 1.523e-09 * *ftp1; //O2-N2

      // H2-X
      *results[21] <<= *results[3]      ; //H2-CO2
      *results[22] <<= *results[10]     ; //H2-CO
      *results[23] <<= *results[17]     ; //H2-O2
      *results[24] <<= 0.0              ; //H2-H2
      *results[25] <<= 2.146e-10 * *ftp2; //H2-H2O
      *results[26] <<= 5.286e-09 * *ftp1; //H2-CH4
      *results[27] <<= 5.424e-09 * *ftp1; //H2-N2

      // H2O-X
      *results[28] <<= *results[4]      ; //H2O-CO2
      *results[29] <<= *results[11]     ; //H2O-CO
      *results[30] <<= *results[18]     ; //H2O-O2
      *results[31] <<= *results[25]     ; //H2O-H2
      *results[32] <<= 0.0              ; //H2O-H2O
      *results[33] <<= 4.059e-11 * *ftp2; //H2O-CH4
      *results[34] <<= 4.339e-11 * *ftp2; //H2O-N2

      // CH4- X
      *results[35] <<= *results[5]      ; //CH4-CO2
      *results[36] <<= *results[12]     ; //CH4-CO
      *results[37] <<= *results[19]     ; //CH4-O2
      *results[38] <<= *results[26]     ; //CH4-H2
      *results[39] <<= *results[33]     ; //CH4-H2O
      *results[40] <<= 0.0              ; //CH4-CH4
      *results[41] <<= 1.657e-09 * *ftp1; //CH4-N2

      //N2-X
      *results[42] <<= *results[6]    ; //N2-CO2
      *results[43] <<= *results[13]   ; //N2-CO
      *results[44] <<= *results[20]   ; //N2-O2
      *results[45] <<= *results[27]   ; //N2-H2
      *results[46] <<= *results[34]   ; //N2-H2O
      *results[47] <<= *results[41]   ; //N2-CH4
      *results[48] <<= 0.0            ; //N2-N2
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      const FieldT& pres = pres_->field_ref();
      const FieldT& temp  = temp_ ->field_ref();
      const FieldT& dpresdv = pres_->sens_field_ref(var);
      const FieldT& dtempdv = temp_ ->sens_field_ref(var);

      // temperature and pressure correlations
      SpatFldPtr<FieldT> dftp2dv = SpatialFieldStore::get<FieldT,FieldT>( pres );
      SpatFldPtr<FieldT> dftp1dv = SpatialFieldStore::get<FieldT,FieldT>( pres );
      *dftp2dv <<= pow(temp, 4.0/3.0)/101325.0 * (7.0/3.0*pres*dtempdv + temp*dpresdv);  // H2O
      *dftp1dv <<= pow(temp, 2.0/3.0)/101325.0 * (5.0/3.0*pres*dtempdv + temp*dpresdv);  // everything else

      std::vector<FieldT > dcoeffdv;
      for(size_t i=0;i<targetTags.size();++i){
        dcoeffdv.push_back(this->sensitivity_result(targetTags[i], var));
      }

      // CO2-X
      dcoeffdv[0] <<= 0.0                 ; //CO2-CO2
      dcoeffdv[1] <<= 1.199e-09 * *dftp1dv; //CO2-CO
      dcoeffdv[2] <<= 1.208e-09 * *dftp1dv; //CO2-O2
      dcoeffdv[3] <<= 4.713e-09 * *dftp1dv; //CO2-H2
      dcoeffdv[4] <<= 2.726e-11 * *dftp2dv; //CO2-H2O
      dcoeffdv[5] <<= 1.365e-09 * *dftp1dv; //CO2-CH4
      dcoeffdv[6] <<= 1.191e-09 * *dftp1dv; //CO2-N2

      //CO-X
      dcoeffdv[7] <<= dcoeffdv[1]         ; //CO-CO2
      dcoeffdv[8] <<= 0.0                 ; //CO-CO
      dcoeffdv[9] <<= 1.537e-09 * *dftp1dv; //CO-O2
      dcoeffdv[10]<<= 5.487e-09 * *dftp1dv; //CO-H2
      dcoeffdv[11]<<= 4.272e-11 * *dftp2dv; //CO-H2O
      dcoeffdv[12]<<= 1.669e-09 * *dftp1dv; //CO-CH4
      dcoeffdv[13]<<= 1.500e-09 * *dftp1dv; //CO-N2

      // O2-X
      dcoeffdv[14] <<= dcoeffdv[2]         ; //O2-CO2
      dcoeffdv[15] <<= dcoeffdv[9]         ; //O2-CO
      dcoeffdv[16] <<= 0.0                 ; //O2-O2
      dcoeffdv[17] <<= 5.783e-09 * *dftp1dv; //O2-H2
      dcoeffdv[18] <<= 4.202e-11 * *dftp2dv; //O2-H2O
      dcoeffdv[19] <<= 1.711e-09 * *dftp1dv; //O2-CH4
      dcoeffdv[20] <<= 1.523e-09 * *dftp1dv; //O2-N2

      // H2-X
      dcoeffdv[21] <<= dcoeffdv[3]         ; //H2-CO2
      dcoeffdv[22] <<= dcoeffdv[10]        ; //H2-CO
      dcoeffdv[23] <<= dcoeffdv[17]        ; //H2-O2
      dcoeffdv[24] <<= 0.0                 ; //H2-H2
      dcoeffdv[25] <<= 2.146e-10 * *dftp2dv; //H2-H2O
      dcoeffdv[26] <<= 5.286e-09 * *dftp1dv; //H2-CH4
      dcoeffdv[27] <<= 5.424e-09 * *dftp1dv; //H2-N2

      // H2O-X
      dcoeffdv[28] <<= dcoeffdv[4]         ; //H2O-CO2
      dcoeffdv[29] <<= dcoeffdv[11]        ; //H2O-CO
      dcoeffdv[30] <<= dcoeffdv[18]        ; //H2O-O2
      dcoeffdv[31] <<= dcoeffdv[25]        ; //H2O-H2
      dcoeffdv[32] <<= 0.0                 ; //H2O-H2O
      dcoeffdv[33] <<= 4.059e-11 * *dftp2dv; //H2O-CH4
      dcoeffdv[34] <<= 4.339e-11 * *dftp2dv; //H2O-N2

      // C    H4-X
      dcoeffdv[35] <<= dcoeffdv[5]         ; //CH4-CO2
      dcoeffdv[36] <<= dcoeffdv[12]        ; //CH4-CO
      dcoeffdv[37] <<= dcoeffdv[19]        ; //CH4-O2
      dcoeffdv[38] <<= dcoeffdv[26]        ; //CH4-H2
      dcoeffdv[39] <<= dcoeffdv[33]        ; //CH4-H2O
      dcoeffdv[40] <<= 0.0                 ; //CH4-CH4
      dcoeffdv[41] <<= 1.657e-09 * *dftp1dv; //CH4-N2

      //N2-X
      dcoeffdv[42] <<= dcoeffdv[6]    ; //N2-CO2
      dcoeffdv[43] <<= dcoeffdv[13]   ; //N2-CO
      dcoeffdv[44] <<= dcoeffdv[20]   ; //N2-O2
      dcoeffdv[45] <<= dcoeffdv[27]   ; //N2-H2
      dcoeffdv[46] <<= dcoeffdv[34]   ; //N2-H2O
      dcoeffdv[47] <<= dcoeffdv[41]   ; //N2-CH4
      dcoeffdv[48] <<= 0.0            ; //N2-N2
    }

  };

} // namespace Char
#endif // BinaryDiffCoeff_Char_h
