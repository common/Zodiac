/**
 * @file EffecDiffCoeff.h
 * @par Calculates effective diffusivities for transport within porous media (such as char) and puts them into a vector
 */

#ifndef EffecDiffCoeff_Char_h
#define EffecDiffCoeff_Char_h

#include <expression/Expression.h>
#include "particles/coal/CharCombustion/CharData.h"

/**
 *  @class EffecDiffCoeff
 *  @ingroup Char
 *  @brief This function calculates effective diffusivities for transportation within porous media (such as char)
 *         and puts them into a vector. Formula is from [1].
 *
 * [1] Lewis et. al. Pulverized Steam Gasification Rates of Three Bituminous Coal Chars in an Entrained-Flow
 * Reactor at Pressurized Conditions. Energy and Fuels. 2015, 29, 1479-1493.
 * http://pubs.acs.org/doi/abs/10.1021/ef502608y
 */
namespace Char {

    template<typename FieldT>
    class EffecDiffCoeff : public Expr::Expression<FieldT> {
        DECLARE_FIELDS( FieldT, pres_, temp_, mmw_ )
        DECLARE_VECTOR_OF_FIELDS( FieldT, yi_ )
        DECLARE_VECTOR_OF_FIELDS( FieldT, binaryDiffCoeff_ )

        const double epsilon_, tauf_;
        const std::vector<double> mwVec_;
        const std::vector<int> indexVec_;
        const Char::CharCombustionData& charData_;

        EffecDiffCoeff( const Expr::TagList& massTags,
                        const Expr::TagList& binaryDiffCoeffTags,
                        const Expr::Tag& mmwTag,
                        const Char::CharCombustionData&  charData,
                        const double epsilon = 1.0,
                        const double tauf = 1.0 )
              : Expr::Expression<FieldT>(),
                epsilon_(epsilon),
                tauf_(tauf),
                charData_(charData),
                mwVec_(charData.get_mwVec()),
                indexVec_(charData.get_indexVec())
        {

          this->set_gpu_runnable( true );
          mmw_ = this->template create_field_request<FieldT>( mmwTag );
          this->template create_field_vector_request<FieldT>( massTags, yi_ );
          this->template create_field_vector_request<FieldT>( binaryDiffCoeffTags, binaryDiffCoeff_ );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag mmwTag_;
            const Expr::TagList massTags_, binaryDiffCoeffTags_;

            const double epsilon_, tauf_;
            const Char::CharCombustionData& charData_;
        public:
          /**
           * @brief The mechanism for building a EffecDiffCoeff object
           * @param effecDiffCoeffTags a vector of effective diffusivities for transportation within porous media (such as char)
           * @param massTags a vector of mass fraction of species in gas phase
           * @param binaryDiffCoeffTags a vector of binary diffusivities (this include N2)
           * @param mmwTag mixture molecular weight
           * @param charData Char Data
           * @param epsilon particle (core) porosity
           * @param tauf ratio of tortuosity and macroporosity
           */
            Builder( const Expr::TagList& effecDiffCoeffTags,
                     const Expr::TagList& massTags,
                     const Expr::TagList& binaryDiffCoeffTags,
                     const Expr::Tag&     mmwTag,
                     const Char::CharCombustionData&  charData,
                     const double epsilon = 1.0,
                     const double tauf = 1.0 )
                  : ExpressionBuilder( effecDiffCoeffTags ),
                    massTags_( massTags ),
                    binaryDiffCoeffTags_( binaryDiffCoeffTags ),
                    mmwTag_( mmwTag ),
                    charData_(charData),
                    epsilon_(epsilon),
                    tauf_(tauf){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new EffecDiffCoeff<FieldT>( massTags_, binaryDiffCoeffTags_, mmwTag_, charData_, epsilon_, tauf_ );
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
          const FieldT& mmw = mmw_->field_ref();

          SpatFldPtr<FieldT> xnp1 = SpatialFieldStore::get<FieldT, FieldT>( mmw);
          const int nspec = mwVec_.size();
          *xnp1 <<= 1.0;
          for( size_t i = 0;i < nspec;i++ ){
            *xnp1 <<= *xnp1 - (yi_[indexVec_[i]]->field_ref() * mmw / mwVec_[i]);
          }
          *xnp1 <<= max( 0.0, min( 1.0, *xnp1 ));

          for( size_t i = 0;i < nspec;i++ ){
            *results[i] <<= *xnp1 / binaryDiffCoeff_[i * (nspec+1) + nspec]->field_ref();
            for( size_t j = 0;j < nspec;j++ ){
              if( i != j ){
                *results[i] <<= *results[i] + (yi_[indexVec_[j]]->field_ref() * mmw / mwVec_[j] )/ binaryDiffCoeff_[i * (nspec+1) + j]->field_ref();
              }
            }
            *results[i] <<= epsilon_ * ( 1 - (yi_[indexVec_[i]]->field_ref()* mmw / mwVec_[i] )) / ( *results[i] * tauf_ );
          }
        }

        void sensitivity( const Expr::Tag& var ){
          using namespace SpatialOps;
          const Expr::TagList& targetTags = this->get_tags();
          typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
          std::vector<FieldT> dcoeffdv;
          for( size_t i = 0;i < targetTags.size();++i ){
            dcoeffdv.push_back( this->sensitivity_result( targetTags[i], var ));
          }

          const FieldT& mmw = mmw_->field_ref();
          const FieldT& dmmwdv = mmw_->sens_field_ref(var);

          SpatFldPtr<FieldT> xnp1 = SpatialFieldStore::get<FieldT, FieldT>(mmw );
          SpatFldPtr<FieldT> dxnp1dv = SpatialFieldStore::get<FieldT, FieldT>(mmw );
          const int nspec = mwVec_.size();
          *xnp1 <<= 1.0;
          *dxnp1dv <<= 0.0;
          for( size_t i = 0;i < nspec;i++ ){
            *xnp1 <<= *xnp1 - yi_[indexVec_[i]]->field_ref() * mmw / mwVec_[i];
          }

          *xnp1 <<= max( 0.0, min( 1.0, *xnp1 ));
          for( size_t i = 0;i < nspec;i++ ){
            *dxnp1dv <<= cond((*xnp1 > 0.0 and *xnp1 < 1.0), *dxnp1dv - yi_[indexVec_[i]]->sens_field_ref(var) * mmw / mwVec_[i]
                                                             - yi_[indexVec_[i]]->field_ref() * dmmwdv / mwVec_[i])
                              (0.0);
          }

          for( size_t i = 0;i < nspec;i++ ){
            SpatFldPtr<FieldT> sum = SpatialFieldStore::get<FieldT,FieldT>( mmw );
            SpatFldPtr<FieldT> dsumdv = SpatialFieldStore::get<FieldT,FieldT>( mmw );
            const FieldT& yi = yi_[indexVec_[i]]->field_ref();
            const FieldT& dyidv = yi_[indexVec_[i]]->sens_field_ref(var);
            const FieldT& binaryDiffCoeffi = binaryDiffCoeff_[i * (nspec+1) + nspec]->field_ref();
            const FieldT& dbinaryDiffCoeffidv = binaryDiffCoeff_[i * (nspec+1) + nspec]->sens_field_ref(var);
            *sum <<= *xnp1 / binaryDiffCoeffi;
            *dsumdv <<= *dxnp1dv / binaryDiffCoeffi - *xnp1 / square( binaryDiffCoeffi) * dbinaryDiffCoeffidv;
            for( size_t j = 0;j < nspec;j++ ){
              if( i != j ){
                const FieldT& yj = yi_[indexVec_[j]]->field_ref();
                const FieldT& dyjdv = yi_[indexVec_[j]]->sens_field_ref(var);
                const FieldT& binaryDiffCoeffj = binaryDiffCoeff_[i * (nspec+1) + j]->field_ref();
                const FieldT& dbinaryDiffCoeffjdv = binaryDiffCoeff_[i * (nspec+1) + j]->sens_field_ref( var );
                *sum <<= *sum + (yj * mmw / mwVec_[j]) / binaryDiffCoeffj;
                *dsumdv <<= *dsumdv + (dyjdv* mmw  + yj * dmmwdv)/ mwVec_[j]/ binaryDiffCoeffj
                                   - yj * mmw / mwVec_[j] / square( binaryDiffCoeffj) *dbinaryDiffCoeffjdv;
              }
            }
            dcoeffdv[i] <<= epsilon_ / tauf_ * (- (dyidv*mmw + yi * dmmwdv )/ mwVec_[i] / *sum
                                                - ( 1 - yi* mmw / mwVec_[i]) / square( *sum ) * *dsumdv );
          }
        }
    };
}// namespace Char
#endif // EffecDiffCoeff_Char_h
