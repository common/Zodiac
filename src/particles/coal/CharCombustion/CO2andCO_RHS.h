
/**
 * @file CO2andCO_RHS.h
 * @par Getting the reaction rate of CO2 and CO from char reaction per gas mass
 * @author Hang Zhou
 */

#ifndef CO2andCO_RHS_Expr_h
#define CO2andCO_RHS_Expr_h

#include <expression/Expression.h>
namespace Char{

/**
 *  @ingroup Char
 *  @class CO2andCORHS
 *  @brief Getting the reaction rate of CO2 and CO from char reaction per gas mass (unit: 1/s)
 *
 * CO2 reaction rate includes production in oxidation and consumption in gasification.
 * \f[
 *    S_{CO2} = - S_{ox} \frac{CO2/CO}{1+CO2/CO} \frac{Mw_CO2}{Mw_C}
 *              + S_{gasif, CO2} \frac{Mw_CO2}{Mw_C}
 * \f]
 * CO reaction rate includes production in oxidation, gasification with CO2 and gasification with H2O.
 * \f[
 *    S_{CO} = - S_{ox} \frac{1}{1+CO2/CO} \frac{Mw_CO}{Mw_C}
 *             - S_{gasif, CO2} \frac{Mw_CO}{Mw_C}
 *             - S_{gasif, H2O} \frac{Mw_CO}{Mw_C}
 * \f]
 * Here, \f$S_{ox}\f$ is the char oxidation rate per gas mass.
 *       \f$CO2/CO\f$ is the production ratio of CO2 and CO from char oxidation.
 *       \f$S_{gasif, CO2}\f$ and \f$S_{gasif, H2O}\f$ are the char gasification rates reacted with CO2 and H2O per gas mass.
 *       \f$Mw_CO2\f$, \f$Mw_CO\f$ and \f$Mw_C\f$ are the molecular weight of CO2, CO and C respectively.
 *
 *  NOTE: 1. This expression calculates the Co2 and CO reaction rate from all particles with ONE specific particle size.
 *           All the parameters used here are the value for ONE specific particle size.
 *        2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */
  template< typename FieldT >
  class CO2andCORHS
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, charoxidationRHS_, co2CORatio_, hetroco2reac_, hetroh2oreac_ )

    CO2andCORHS( const Expr::Tag& charoxidationRHStag,
                  const Expr::Tag& co2CORatioTag,
                  const Expr::Tag& hetroco2reactag,
                  const Expr::Tag& hetroh2oreactag )
          : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);

      charoxidationRHS_ = this->template create_field_request<FieldT>( charoxidationRHStag );
      co2CORatio_       = this->template create_field_request<FieldT>( co2CORatioTag       );
      hetroco2reac_     = this->template create_field_request<FieldT>( hetroco2reactag     );
      hetroh2oreac_     = this->template create_field_request<FieldT>( hetroh2oreactag     );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag charoxidationRHSTag_, co2CORatioTag_, hetroco2reacTag_, hetroh2oreacTag_;
    public:
      /**
       * @brief The mechanism for buiding a CO2andCORHS object
       * @tparam FieldT
       * @param coco2rhsTags production rates of CO2 and CO from char reaction per gas mass
       * @param charoxidationRHSTag char oxidation rate per gas mass
       * @param co2CORatioTag production rate ratio between CO2 and CO from char oxidation
       * @param hetroco2reacTag char gasification rate reacting with CO2 per gas mass
       * @param hetroh2oreacTag char gasification rate reacting with H2O per gas mass
       */
      Builder( const Expr::TagList& coco2rhsTags,
               const Expr::Tag& charoxidationRHSTag,
               const Expr::Tag& co2CORatioTag,
               const Expr::Tag& hetroco2reacTag,
               const Expr::Tag& hetroh2oreacTag)
            : ExpressionBuilder(coco2rhsTags),
              charoxidationRHSTag_ ( charoxidationRHSTag ),
              co2CORatioTag_       ( co2CORatioTag       ),
              hetroco2reacTag_     ( hetroco2reacTag     ),
              hetroh2oreacTag_     ( hetroh2oreacTag     )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new CO2andCORHS<FieldT>( charoxidationRHSTag_, co2CORatioTag_, hetroco2reacTag_, hetroh2oreacTag_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;

      typename Expr::Expression<FieldT>::ValVec& dyi = this->get_value_vec();
      assert( dyi.size() == 2 );

      const FieldT& charoxidationRHS = charoxidationRHS_->field_ref();
      const FieldT& co2CORatio       = co2CORatio_      ->field_ref();
      const FieldT& hetroco2reac     = hetroco2reac_    ->field_ref();
      const FieldT& hetroh2oreac     = hetroh2oreac_    ->field_ref();

      FieldT& co2 = *dyi[0];
      co2 <<= - charoxidationRHS * co2CORatio / (1.0 + co2CORatio) /12.0 * 44.0
              + hetroco2reac/12.0 * 44.0; // (charoxdidationRHS: negative; hetroco2reac: negative)
      FieldT& co = *dyi[1];
      co  <<= - charoxidationRHS / (1.0 + co2CORatio) / 12.0 * 28.0
              - hetroco2reac/12.0 * 28.0 * 2.0 - hetroh2oreac/12.0*28.0;
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      typename Expr::Expression<FieldT>::ValVec& result = this->get_value_vec();

      std::vector<FieldT> drhsdv;
      for( size_t i = 0;i < targetTags.size();++i ){
        drhsdv.push_back( this->sensitivity_result( targetTags[i], var ));
      }

      const FieldT& charoxidationRHS    = charoxidationRHS_->field_ref();
      const FieldT& co2CORatio          = co2CORatio_      ->field_ref();
      const FieldT& hetroco2reac        = hetroco2reac_    ->field_ref();
      const FieldT& hetroh2oreac        = hetroh2oreac_    ->field_ref();
      const FieldT& dcharoxidationRHSdv = charoxidationRHS_->sens_field_ref(var);
      const FieldT& dco2CORatiodv       = co2CORatio_      ->sens_field_ref(var);
      const FieldT& dhetroco2reacdv     = hetroco2reac_    ->sens_field_ref(var);
      const FieldT& dhetroh2oreacdv     = hetroh2oreac_    ->sens_field_ref(var);

      drhsdv[0] <<= -44.0/12.0 * ((dcharoxidationRHSdv*co2CORatio + charoxidationRHS*dco2CORatiodv) / (1+co2CORatio)
                                   - charoxidationRHS*co2CORatio/square(1+co2CORatio)*dco2CORatiodv-dhetroco2reacdv);

      drhsdv[1] <<= - 28.0/12.0 * (dcharoxidationRHSdv/(1+co2CORatio)-charoxidationRHS/square(1+co2CORatio)*dco2CORatiodv
                                  + 2.0 *dhetroco2reacdv + dhetroh2oreacdv);
    }
  };

} // namesapace CHAR


#endif // CO2andCO_RHS_Expr_h
