/**
 * @file H2andH2O_RHS.h
 * @par Getting the reaction rate of H2 and H2O from char gasification per gas mass
 * @author Hang Zhou
 */
#ifndef H2andH2ORHS_CHAR_h
#define H2andH2ORHS_CHAR_h

#include <expression/Expression.h>

/**
 *  @ingroup Char
 *  @class H2andH2ORHS
 *  @brief Getting the reaction rate of H2 and H2O from char gasification per gas mass (unit: 1/s)
 *
 * The expressions are given as:
 * H2 is produced by char gasificaiton with H2O:
 * \f[
 *    S_{H2} = - S_{gasif, H2O} \frac{Mw_H2}{Mw_C}
 * \f]
 * H2O is consumed by char gasification:
 * \f[
 *   S_{H2)} = S_{gasif, H2O} \frac{Mw_H2O}{Mw_C}
 * \f]
 * Here, \f$S_{gasif, H2O}\f$ is the char gasification rate reacted with H2O per gas mass.
 *       \f$Mw_H2\f$, \f$Mw_H2O\f$ and \f$Mw_C\f$ are the molecular weight of H2, H2O and C respectively.
 *
 *  NOTE: 1. This expression calculates the H2 and H2O reaction rate from all particles with ONE specific particle size.
 *           All the parameters used here are the value for ONE specific particle size.
 *        2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */
namespace Char{

  template< typename FieldT >
  class H2andH2ORHS
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELD( FieldT, hetroh2o_ )

    H2andH2ORHS( const Expr::Tag& hetroh2otag )
    : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      hetroh2o_ = this->template create_field_request<FieldT>( hetroh2otag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag hetroh2otag_;
    public:
      /**
       * @brief The mechanism for building a H2andH2ORHS object
       * @tparam FieldT
       * @param h2h20rhsTag H2 and H2O reaction rate from char gasificaiton with H2O per gas mass
       * @param hetroh2otag char gasification rate with H2O per gas mass
       */
      Builder( const Expr::TagList& h2h20rhsTag,
               const Expr::Tag& hetroh2otag )
            : ExpressionBuilder( h2h20rhsTag ),
              hetroh2otag_ ( hetroh2otag )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new H2andH2ORHS<FieldT>( hetroh2otag_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
      FieldT& H2rhs = *results[0];
      FieldT& H2Orhs = *results[1];
      const FieldT& hetroh2o = hetroh2o_->field_ref();

      H2rhs  <<= - hetroh2o / 12.0 * 2;  // positive
      H2Orhs <<= hetroh2o / 12.0 * 18.0; // negative
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      typename Expr::Expression<FieldT>::ValVec& result = this->get_value_vec();

      std::vector<FieldT> drhsdv;
      for( size_t i = 0;i < targetTags.size();++i ){
        drhsdv.push_back( this->sensitivity_result( targetTags[i], var ));
      }
      const FieldT& dhetroh2odv = hetroh2o_->sens_field_ref(var);
      drhsdv[0] <<= - dhetroh2odv / 12.0 * 2.0;
      drhsdv[1] <<= dhetroh2odv / 12.0 * 18.0;
    }
  };

} //namespace CHAR


#endif // H2andH2ORHS_CHAR_h
