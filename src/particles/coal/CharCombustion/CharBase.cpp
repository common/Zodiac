#include "CharBase.h"

#include <stdexcept>
#include <sstream>

namespace Char{

  std::string char_model_name( const CharModel model )
  {
    std::string name;
    switch (model){
      case GASIF_OXID       : name = "GasifOxid";   break;
      case CCK              : name = "CCK";         break;
      case INVALID_CHARMODEL: name = "INVALID";     break;
      case OFF              : name = "OFF";         break;
    }
    return name;
  }

  CharModel char_model( const std::string& name )
  {
    if     ( name == char_model_name(GASIF_OXID ) ) return GASIF_OXID;
    else if( name == char_model_name(CCK        ) ) return CCK;
    else if( name == char_model_name(OFF        ) ) return OFF;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported char chemistry model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << char_model_name( GASIF_OXID  )
          << "\n\t" << char_model_name( CCK         )
          << "\n\t" << char_model_name( OFF         )
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_CHARMODEL;
  }

}
