#ifndef CoalInterface_h
#define CoalInterface_h

#include <vector>
#include <string>
#include <expression/ExprLib.h>
#include <expression/Tag.h>
#include "CoalData.h"
#include "particles/Vaporization/VaporizationInterface.h"
#include "Devolatilization/DevolatilizationInterface.h"
#include "CharCombustion/CharInterface.h"
#include "CharCombustion/CharBase.h"
#include "particles/Vaporization/VaporizationBase.h"
#include "particles/coal/TarSootReaction/TarSootBase.h"

/**
 *  @file CoalInterface.h
 *  @author Hang
 */

namespace Coal{

  /**
   * @class CoalEnsembleTags
   * @ingroup Coal
   * @brief Getting tags used in coal reaction
   * @author Hang Zhou
   */
  class CoalEnsembleTags {
  public:
      const Expr::Tag fuelAirEquivalenceRatio;     ///< fuel-air equivalence ratio
      Expr::TagList   moistureMassEachPartTags,    ///< H2O mass in each particle
                      moistureMassFracTags,        ///< H2O mass fraction
                      moistureMassFracInitialTags, ///< initial H2O mass fraction
                      moistureMassFracInflowTags,  ///< inflowing H2O mass fraction
                      moistureMassFracMixRhsTags,  ///< mixing RHS of H2O mass fraction equation
                      moistureMassFracKinRhsTags,  ///< kinetic RHS of H2O mass fraction equation
                      moistureMassFracFullRhsTags, ///< full RHS of H2O mass fraction equation
                      charMassEachPartTags,        ///< char mass in each particle
                      charMassFracTags,            ///< char mass fraction
                      charMassFracInitialTags,     ///< initial char mass fraction
                      charMassFracInflowTags,      ///< inflowing fixed carbon mass fraction
                      charMassFracMixRhsTags,      ///< mixing RHS of char mass fraction equation
                      charMassFracKinRhsTags,      ///< kinetic RHS of char mass fraction equation
                      charMassFracFullRhsTags,     ///< full RHS of char mass fraction equation
                      volMassEachPartTags,         ///< volatile mass in each particle
                      volMassFracTags,             ///< volatile mass fraction
                      volMassFracInflowTags,       ///< inflowing devolatilization mass fraction in particle
                      volMassFracMixRhsTags,       ///< mixing RHS of devolatilization mass fraction equation
                      volMassFracKinRhsTags,       ///< kinetic RHS of devolatilization mass fraction equation
                      volMassFracFullRhsTags;      ///< full RHS of devolatilization mass fractionequation
      CoalEnsembleTags( const Expr::Context& state,
                        const std::vector<std::string>& particleNumArray);
  };


  /**
   *  @class CoalInterface
   *  @ingroup Coal
   *  @brief Provides a concise interface to the coal models
   *
   */
  class CoalInterface
  {
      const CoalType coaltype_;
      const Vap::VapModel vapModel_;
      const Dev::DevModel devModel_;
      const Tarsoot::TarsootModel tarsootModel_;
      const Char::CharModel charModel_;
      const std::vector<std::string>& particleNumArray_;
      const YAML::Node& rootParser_;
      std::set<Expr::ExpressionID>& initRoots_;
      Expr::ExpressionFactory& initFactory_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      Expr::TagList& primitiveTags_;
      Expr::TagList& kinRhsTags_;
      int& equIndex_;
      const bool isRestart_;
      std::string reactorType_;

      Vap::Vaporization* vap_;
      Dev::DevolatilizationInterface* dev_;
      Char::CharInterface* char_;

  public:
    /**
     * @param coalType coal type
     * @param vapModel vaporization model
     * @param devModel devolatilization model
     * @param tarsootModel tarSoot model
     * @param charModel char reaction model
     * @param particleNumArray list of particle numbers with different sizes
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
     CoalInterface( const CoalType coalType,
                    const Vap::VapModel vapModel,
                    const Dev::DevModel devModel,
                    const Tarsoot::TarsootModel tarsootModel,
                    const Char::CharModel charModel,
                    const std::vector<std::string>& particleNumArray,
                    const YAML::Node& rootParser,
                    std::set<Expr::ExpressionID>& initRoots,
                    Expr::ExpressionFactory& initFactory,
                    Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                    Expr::TagList& primitiveTags,
                    Expr::TagList& kinRhsTags,
                    int& equIndex,
                    const bool isRestart);

    ~CoalInterface();
    /**
     * @brief Adding required variables needed to restart in coal reaction
     * @param restartVars variables needed to restart
     */
    void restart_var_coal(std::vector<std::string>& restartVars);
    /**
     * @brief Initialize coal reaction
     */
    void initialize_coal();
    /**
      * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in coal reaction
      * @param primVarIdxMap primitive variables indices
      * @param consVarIdxMap solved variables indices
      * @param kinRhsIdxMap kinetic rhs indices
      * @param rhsIdxMap rhs tags indices
      */
    void modify_idxmap_coal( std::map<Expr::Tag, int>& primVarIdxMap,
                             const std::map<Expr::Tag, int>& consVarIdxMap,
                             std::map<Expr::Tag, int>& kinRhsIdxMap,
                             const std::map<Expr::Tag, int>& rhsIdxMap );
    /**
     * @brief Modify particle state transformation matrix
     * @param dVdUPart particle state transformation matrix
     * @param primVarIdxMap primitive variables indices
     * @param consVarIdxMap solved variables indices
     */
    void dvdu_coal( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                    const std::map<Expr::Tag, int> primVarIdxMap,
                    const std::map<Expr::Tag, int> consVarIdxMap);
    /**
     * @brief Modify particle mixing rhs matrix (dmixPartdV)
     * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
     * @param primVarIdxMap primitive variables indices
     * @param rhsIdxMap rhs tags indices
     * @param consVarRhsMap solved variables rhs tags indices
     */
    void dmMixingdv_coal( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                          std::map<Expr::Tag, int> primVarIdxMap,
                          std::map<Expr::Tag, int> rhsIdxMap,
                          std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  private:
    /**
     * @brief Set the initial conditions for the coal reaction
     */
    void setup_initial_conditions_coal();

    /**
     * @brief Register all expressions required to implement the coal reaction and add equation into the time integrator
     */
    void build_equation_system_coal();

    /**
     * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in coal reaction
     */
    void modify_prim_kinrhs_tags_coal();
  };

} // namespace Coal

#endif // CoalInterface_h
