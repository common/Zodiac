
/**
 * @file CoalTempMixRHS.h
 * @par Getting the mixing term in the coal particle temperature equation
 * @author Hang
 */

#ifndef CoalTempMixRHS_coal_h
#define CoalTempMixRHS_coal_h

#include <expression/Expression.h>

namespace Coal {

  /**
   * @class CoalTempMixRHS
   * @ingroup Coal
   * @brief Getting the mixing term in the coal particle temperature equation
   *
   *
   * The mixing source term is given as
   * \f[
   *    S_{mix} = \frac{\rho_{in}}{\rho \tau_{mix} Y_p, Cp_p}\sum_{i=1}^{ncomposition} Y_{p,j,in}(h_{p,j,in}-h_{p,j})
   * \f]
   * Here, \f$ \rho_{in}\f$ is the density of the inflow gas stream.
   *       \f$ \rho\f$ is the density of the gas phase in the reactor.
   *       \f$ \tau_{mix} is the residence time.
   *       \f$ Y_p \f$ is the particle mass per gas mass.
   *       \f$ Cp,p \f$ is the heat capacity of particle.
   *       \f$ ncomposition \f$ is the number of composition in the particle. For coal, it includes water, volatile, char and ash.
   *       \f$ Y_{p,j,in} \f$ is the jth composition mass per gas mass in the inflow particle stream.
   *       \f$ h_{p,j,in} \f$ is the jth composition specific enthalpy under the inflow particle temperature.
   *       \f$ h_{p,j} \f$ is the jth composition specific enthalpy under the particle temperature in the reactor.
   *
   * For solid or liquid particles, only one composition is in the particle (the solid material or water, \f$ j=1\f$),
   * and their heat capacities (\f$Cp_p\f$) are assumed to be constant. So, this mixing term could be simplified to be
   * \f[
   *     S_{mix} = \frac{\rho_{in}}{\rho \tau_{mix}} (T_{p,in}-T_p)
   * \f]
   * This has the same format as all the other equations under constant pressure, calculated by `ZeroDMixingCP` in `ZeroDimMixing/h`.
   *
   * The term in the summation including composition specific enthalpies are given as
   * <ul>
   *     <li>
   *         For water:
   *          \f[  Cp_w = 4200 J/kg/K \f]
   *          \f[
   *               Y_{p,w,in}(h_{p,w,in}-h_{p,w}) = Y_{p,w,in}\int_{T_p}^{T_{p,in}} Cp_w dT_p
   *                                              = Y_{p,w,in} Cp_w (T_{p,in}-T_p)
   *          \f]
   *      </li>
   *      <li> For volatile:
   *           \f[   Cp_v = 1500.5 + 2.9725 T_p  \f]
   *           \f[
   *               Y_{p,v,in}(h_{p,v,in}-h_{p,v}) = Y_{p,v,in}\int_{T_p}^{T_{p,in}} Cp_v dT_p
   *                                              = Y_{p,v,in} (1500.5(T_{p,in}-T_p) + 2.9725 (T_{p,in}^2-T_p^2)/2)
   *          \f]
   *       </li>
   *       <li> For char:
   *           \f[ Cp_c = \frac{R}{M_{w,c} \left[ f1 + 2f2 \right] \f]
   *           Here, \f$M_{w,c}\f$ is the molecular weight of char. \f$R\f$ is the gas constant.
   *                \f[ f1 = \frac{(380.0/T_p)^2 e^{380.0/T_p}}{(e^{380.0/T_p} - 1)^2} \f]
   *                \f[ f2 = \frac{(1800.0/T_p)^2 e^{1800.0/T_p}}{(e^{1800.0/T_p} - 1)^2} \f]
   *           \f[
   *               Y_{p,c,in}(h_{p,c,in}-h_{p,c}) = Y_{p,c,in}\int_{T_p}^{T_{p,in}} Cp_c dT_p
   *                                              = Y_{p,c,in} (g1-g2)
   *          \f]
   *          Here,
   *          \f[ g1 = \frac{R}{M_{w,c} \left(\frac{380.0}{(e^{380.0/T_{p,in}}-1)} + 2\frac{1800.0}{(e^{1800.0/T_{p,in}}-1)}\right) \f]
   *          \f[ g2 = \frac{R}{M_{w,c} \left(\frac{380.0}{(e^{380.0/T_p}-1)} + 2\frac{1800.0}{(e^{1800.0/T_p}-1)}\right) \f]
   *       </li>
   *       <li> For ash:
   *           \f[   Cp_{ash} = 594 + 0.586T_p  \f]
   *           \f[
   *               Y_{p,ash,in}(h_{p,ash,in}-h_{p,ash}) = Y_{p,ash,in}\int_{T_p}^{T_{p,in}} Cp_ash dT_p
   *                                                    = Y_{p,v,in} (594(T_{p,in}-T_p) + 0.586(T_{p,in}^2-T_p^2)/2)
   *          \f]
   *       </li>
   * </ul>
   */

  template< typename FieldT >
  class CoalTempMixRHS: public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, mvol_, mchar_, mmois_, prtmass_, tempP_, tempPIn_, cpP_ )
      DECLARE_FIELDS( FieldT, rho_, rhoIn_, tau_)

      CoalTempMixRHS( const Expr::Tag& mvolTag,
                      const Expr::Tag& mcharTag,
                      const Expr::Tag& mmoistTag,
                      const Expr::Tag& prtmassTag,
                      const Expr::Tag& tempPTag,
                      const Expr::Tag& tempPInTag,
                      const Expr::Tag& cpPTag,
                      const Expr::Tag& rhoTag,
                      const Expr::Tag& rhoInTag,
                      const Expr::Tag& tauTag)
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );
        mvol_ = this->template create_field_request<FieldT>( mvolTag );
        mchar_ = this->template create_field_request<FieldT>( mcharTag );
        mmois_ = this->template create_field_request<FieldT>( mmoistTag );
        prtmass_ = this->template create_field_request<FieldT>( prtmassTag );
        tempP_ = this->template create_field_request<FieldT>( tempPTag );
        tempPIn_ = this->template create_field_request<FieldT>( tempPInTag );
        cpP_ = this->template create_field_request<FieldT>( cpPTag );
        rho_ = this->template create_field_request<FieldT>( rhoTag );
        rhoIn_ = this->template create_field_request<FieldT>( rhoInTag );
        tau_ = this->template create_field_request<FieldT>( tauTag );
      }

  public:
    class Builder : public Expr::ExpressionBuilder {
        const Expr::Tag mvolTag_, mcharTag_, mmoistTag_, prtmassTag_, tempPTag_, tempPInTag_, cpPTag_;
        const Expr::Tag rhoTag_, rhoInTag_, tauTag_;
    public:
        /**
         * @brief The mechanism for building a CoalTempMixRHS object
         * @tparam FieldT
         * @param tempPMixRhsTag mixing term in coal partial temperature equation
         * @param mvolTag volatile mass in the reactor (per gas mass)
         * @param mcharTag char mass in the reactor (per gas mass)
         * @param mmoistTag moisture mass in the reactor (per gas mass)
         * @param prtmassTag particle mass in the reactor (per gas mass)
         * @param tempPTag particle temperature
         * @param tempPInTag inflow particle temperature
         * @param cpPTag particle heat capacity
         * @param rhoTag density of gas in the reactor
         * @param rhoInTag density of inflow gas stream
         * @param tauTag residence time
         */
        Builder( const Expr::Tag& tempPMixRhsTag,
                 const Expr::Tag& mvolTag,
                 const Expr::Tag& mcharTag,
                 const Expr::Tag& mmoistTag,
                 const Expr::Tag& prtmassTag,
                 const Expr::Tag& tempPTag,
                 const Expr::Tag& tempPInTag,
                 const Expr::Tag& cpPTag,
                 const Expr::Tag& rhoTag,
                 const Expr::Tag& rhoInTag,
                 const Expr::Tag& tauTag )
              : ExpressionBuilder( tempPMixRhsTag ),
                mvolTag_( mvolTag ),
                mcharTag_( mcharTag ),
                mmoistTag_( mmoistTag ),
                prtmassTag_( prtmassTag ),
                tempPTag_( tempPTag ),
                tempPInTag_( tempPInTag),
                cpPTag_(cpPTag),
                rhoTag_( rhoTag ),
                rhoInTag_( rhoInTag ),
                tauTag_( tauTag ){}

        ~Builder(){}

        Expr::ExpressionBase* build() const{
          return new CoalTempMixRHS<FieldT>( mvolTag_, mcharTag_, mmoistTag_, prtmassTag_, tempPTag_, tempPInTag_, cpPTag_, rhoTag_, rhoInTag_, tauTag_ );
        }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();

      const FieldT& mvol = mvol_->field_ref();
      const FieldT& mchar = mchar_->field_ref();
      const FieldT& mmois = mmois_->field_ref();
      const FieldT& prtmass = prtmass_->field_ref();
      const FieldT& tempP = tempP_->field_ref();
      const FieldT& tempPIn = tempPIn_->field_ref();
      const FieldT& cpP = cpP_->field_ref();
      const FieldT& rho = rho_->field_ref();
      const FieldT& rhoIn = rhoIn_->field_ref();
      const FieldT& tau = tau_->field_ref();

      SpatFldPtr<FieldT> charIn = SpatialFieldStore::get<FieldT, FieldT>( result );
      SpatFldPtr<FieldT> charTemp = SpatialFieldStore::get<FieldT, FieldT>( result );

      *charIn <<= 8.314 / 12.0 * (380.0/(exp( 380.0/tempPIn ) - 1.0) + 2 * 1800.0/(exp( 1800.0/tempPIn ) - 1.0));
      *charTemp <<= 8.314 / 12.0 * (380.0/(exp( 380.0/tempP ) - 1.0) + 2 * 1800.0/(exp( 1800.0/tempP ) - 1.0));

      SpatFldPtr<FieldT> ashMass = SpatialFieldStore::get<FieldT, FieldT>( result );
      *ashMass <<= prtmass - ( mchar + mmois + mvol );

      result <<= (  cond( mmois > 0.0, 4.2 * (tempPIn-tempP) * mmois) (0.0)
                  + cond( mvol > 0.0, ((1.5005*tempPIn + 2.9725E-3*square(tempPIn)/2)-(1.5005*tempP + 2.9725E-3*square(tempP)/2)) * mvol)(0.0)
                  + cond( mchar > 0.0, (*charIn-*charTemp)* mchar)(0.0)
                  + ((0.594*tempPIn + 5.86E-4*square(tempPIn)/2) - (0.594*tempP + 5.86E-4*square(tempP)/2)) * *ashMass
                 ) * 1000.0 * rhoIn / rho / tau / prtmass / cpP;
    }


    void sensitivity( const Expr::Tag& sensVarTag ){
      using namespace SpatialOps;
      FieldT& result = this->value();
      FieldT& dfdv = this->sensitivity_result( sensVarTag );
      const FieldT& mvol = mvol_->field_ref();
      const FieldT& mchar = mchar_->field_ref();
      const FieldT& mmois = mmois_->field_ref();
      const FieldT& prtmass = prtmass_->field_ref();
      const FieldT& tempP = tempP_->field_ref();
      const FieldT& tempPIn = tempPIn_->field_ref();
      const FieldT& cpP = cpP_->field_ref();
      const FieldT& rho = rho_->field_ref();
      const FieldT& rhoIn = rhoIn_->field_ref();
      const FieldT& tau = tau_->field_ref();
      const FieldT& dmvoldv = mvol_->sens_field_ref( sensVarTag );
      const FieldT& dmchardv = mchar_->sens_field_ref( sensVarTag );
      const FieldT& dmmoisdv = mmois_->sens_field_ref( sensVarTag );
      const FieldT& dprtmassdv = prtmass_->sens_field_ref( sensVarTag );
      const FieldT& dtempPdv = tempP_->sens_field_ref( sensVarTag );
      const FieldT& dtempPIndv = tempPIn_->sens_field_ref( sensVarTag );
      const FieldT& dcpPdv = cpP_->sens_field_ref( sensVarTag );
      const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
      const FieldT& drhoIndv = rhoIn_->sens_field_ref( sensVarTag );
      const FieldT& dtaudv = tau_->sens_field_ref( sensVarTag );

      SpatFldPtr<FieldT> dchardv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dvoldv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dmoisdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dashdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );

      SpatFldPtr<FieldT> g1 = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      *g1 <<= ( exp( 380.0 / tempP ) - 1.0 ) / ( 380.0 / tempP );
      *g1 <<= exp( 380.0 / tempP ) / ( *g1 * *g1 );

      SpatFldPtr<FieldT> g2 = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      *g2 <<= ( exp( 1800.0 / tempP ) - 1.0 ) / ( 1800.0 / tempP );
      *g2 <<= exp( 1800.0 / tempP ) / ( *g2 * *g2 );

      SpatFldPtr<FieldT> g1In = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      *g1In <<= ( exp( 380.0 / tempPIn ) - 1.0 ) / ( 380.0 / tempPIn );
      *g1In <<= exp( 380.0 / tempPIn ) / ( *g1In * *g1In );

      SpatFldPtr<FieldT> g2In = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      *g2In <<= ( exp( 1800.0 / tempPIn ) - 1.0 ) / ( 1800.0 / tempPIn );
      *g2In <<= exp( 1800.0 / tempPIn ) / ( *g2In * *g2In );

      SpatFldPtr<FieldT> charIn = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> charTemp = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dcharIndv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dcharTempdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );

      *charIn <<= 8.314 / 12.0 * (380.0/(exp( 380.0/tempPIn ) - 1.0) + 2 * 1800.0/(exp( 1800.0/tempPIn ) - 1.0));
      *charTemp <<= 8.314 / 12.0 * (380.0/(exp( 380.0/tempP ) - 1.0) + 2 * 1800.0/(exp( 1800.0/tempP ) - 1.0));

      *dcharIndv <<= 8.314 / 12.0 * (*g1In + 2 * *g2In) * dtempPIndv;
      *dcharTempdv <<= 8.314 / 12.0 * (*g1 + 2 * *g2) * dtempPdv;

      SpatFldPtr<FieldT> ashMass = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dashMassdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      *ashMass <<= prtmass - ( mchar + mmois + mvol );
      *dashMassdv <<= dprtmassdv - ( dmchardv + dmmoisdv + dmvoldv );

      *dmoisdv <<= cond(mmois>0.0, 4.2 * (dtempPIndv-dtempPdv) * mmois + 4.2 * (tempPIn-tempP)*dmmoisdv)(0.0);
      *dvoldv <<= cond(mvol>0.0, ((1.5005*dtempPIndv + 2.9725E-3*tempPIn*dtempPIndv)-(1.5005*dtempPdv + 2.9725E-3*tempP*dtempPdv))*mvol
                                  + ((1.5005*tempPIn + 2.9725E-3*square(tempPIn)/2)-(1.5005*tempP + 2.9725E-3*square(tempP)/2))*dmvoldv)
                       (0.0);
      *dchardv <<= cond(mchar>0.0, (*dcharIndv- *dcharTempdv)*mchar + (*charIn-*charTemp)*dmchardv )(0.0);
      *dashdv <<= ((0.594*dtempPIndv + 5.86E-4*tempPIn*dtempPIndv) - (0.594*dtempPdv + 5.86E-4*tempP*dtempPdv)) * *ashMass
                 + ((0.594*tempPIn + 5.86E-4*square(tempPIn)/2) - (0.594*tempP + 5.86E-4*square(tempP)/2)) * *dashMassdv;

      dfdv <<= (*dmoisdv+*dvoldv+*dchardv+*dashdv)* 1000.0 * rhoIn / rho / tau / prtmass / cpP
               + result * (drhoIndv/rhoIn - 1/rho/tau/prtmass/cpP*(drhodv*tau*prtmass*cpP+rho*dtaudv*prtmass*cpP
                                                                   + rho*tau*dprtmassdv*cpP+ rho*tau*prtmass*dcpPdv));
    }
  };
} // namespace Coal

#endif // CoalTempMixRHS_coal_h
