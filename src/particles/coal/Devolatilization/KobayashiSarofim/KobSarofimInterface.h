/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   KobSaroFimInterface.h
 *  @author Hang Zhou
 */

#ifndef KOBSAROFIM_INTERFACE_UTIL_H_
#define KOBSAROFIM_INTERFACE_UTIL_H_

#include <expression/ExprLib.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>
#include <yaml-cpp/yaml.h>

#include "particles/coal/Devolatilization/DevolatilizationBase.h"

namespace KobSarofim {

    /**
     * @class KobSarofimTags
     * @ingroup KobSarofim
     * @brief Getting tags used in KobSarofim model
     * @author Hang Zhou
     */
    class KobSarofimTags {
    public:
        Expr::TagList yHVolTags,        ///< mass of H in volatile per gas mass. It is used to calculated H coefficient `a` in CHaOb
                      yOVolTags,        ///< mass of O in volatile per gas mass. It is used to calculated O coefficient `b` in CHaOb
                      yHVolRateTags,    ///< reaction rate of H mass fraction in volatile
                      yOVolRateTags,    ///< reaction rate of O mass fraction in volatile
                      yHVolInflowTags,  ///< inflow of H mass fraction in volatile
                      yOVolInflowTags,  ///< inflow of O mass fraction in volatile
                      yHVolMixRhsTags,  ///< mix RHS of H coefficient equation
                      yOVolMixRhsTags,  ///< mix RHS of O coefficient equation
                      yHVolKinRhsTags,  ///< kin RHS of H coefficient equation
                      yOVolKinRhsTags,  ///< kin RHS of O coefficient equation
                      yHVolFullRhsTags, ///< full RHS of H coefficient equation
                      yOVolFullRhsTags; ///< full RHS of O coefficient equation

        KobSarofimTags( const Expr::Context& state,
                        const std::vector<std::string>& particleNumArray);
    };

  /**
   *  @ingroup KobSarofim
   *  @class KobSarofimInterface
   *  @brief Provides an interface to the KobSarofim model
   */
  class KobSarofimInterface: public Dev::DevolatilizationBase
  {
      const Coal::CoalComposition coalcomp_;
      const Dev::DevModel dvm_;
      const TarAndSootInfo& tarSootInfo_;
      const YAML::Node& rootParser_;
      std::vector<std::string> particleNumArray_;
      std::vector<std::string> speciesArray_;
      std::vector<std::string> partNumSpeciesArray_;
      std::set<Expr::ExpressionID>& initRoots_;
      Expr::ExpressionFactory& initFactory_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      Expr::TagList& primitiveTags_;
      Expr::TagList& kinRhsTags_;
      int& equIndex_;
      const bool isRestart_;
  public:
    /**
     * @param coalType coal type
     * @param dvm devolatilization model
     * @param tarSootInfo tarSoot information
     * @param particleNumArray list of particle numbers with different sizes
     * @param speciesArray list of species involved in KS model
     * @param partNumSpeciesArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
      KobSarofimInterface(const Coal::CoalType coalType,
                          const Dev::DevModel dvm,
                          const TarAndSootInfo& tarSootInfo,
                          std::vector<std::string> particleNumArray,
                          std::vector<std::string> speciesArray,
                          std::vector<std::string> partNumSpeciesArray,
                          const YAML::Node& rootParser,
                          std::set<Expr::ExpressionID>& initRoots,
                          Expr::ExpressionFactory& initFactory,
                          Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                          Expr::TagList& primitiveTags,
                          Expr::TagList& kinRhsTags,
                          int& equIndex,
                          const bool isRestart);

      ~KobSarofimInterface();
      /**
       * @brief Adding required variables needed to restart in KS model
       * @param restartVars variables needed to restart
       */
      void restart_var_coal_dev(std::vector<std::string>& restartVars);

      /**
       * @brief Set the initial conditions for the KS model
       */
      void setup_initial_conditions_dev();

      /**
       * @brief Register all expressions required to implement the KS model and add equation into the time integrator
       */
      void build_equation_system_coal_dev();

      /**
       * @brief Initialize KS model
       */
      void initialize_coal_dev();

      /**
       * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in KS model
       */
      void modify_prim_kinrhs_tags_dev();

      /**
       * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in KS model
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       * @param kinRhsIdxMap kinetic rhs indices
       * @param rhsIdxMap rhs tags indices
       */
      void modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                              const std::map<Expr::Tag, int> &consVarIdxMap,
                              std::map<Expr::Tag, int> &kinRhsIdxMap,
                              const  std::map<Expr::Tag, int> &rhsIdxMap);
     /**
      * @brief Modify particle state transformation matrix
      * @param dVdUPart particle state transformation matrix
      * @param primVarIdxMap primitive variables indices
      * @param consVarIdxMap solved variables indices
      */
      void dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                          std::map<Expr::Tag, int> primVarIdxMap,
                          std::map<Expr::Tag, int> consVarIdxMap);

    /**
     * @brief Modify particle mixing rhs matrix (dmixPartdV)
     * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
     * @param primVarIdxMap primitive variables indices
     * @param rhsIdxMap rhs tags indices
     * @param consVarRhsMap solved variables rhs tags indices
     */
      void dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                std::map<Expr::Tag, int> primVarIdxMap,
                                std::map<Expr::Tag, int> rhsIdxMap,
                                std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  };
} // namespace KobSarofim

#endif /* KOBSAROFIM_INTERFACE_UTIL_H_ */
