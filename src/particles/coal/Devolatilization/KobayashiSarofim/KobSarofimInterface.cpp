/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>
#include <fstream>

#include <expression/Functions.h>
#include <expression/ExpressionFactory.h>
#include <TransformationMatrixExpressions.h>

#include "ZeroDimMixing.h"
#include "SumOp.h"
#include "ReactorEnsembleUtil.h"
#include "particles/ParticleInterface.h"
#include "particles/ParticleTransformExpressions.h"
#include "KobayashiSarofimModel.h"
#include "KobSarofimInterface.h"
#include "KobSarofimData.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

using Expr::matrix::sensitivity;

namespace KobSarofim {

  typedef Expr::LinearFunction<FieldT>::Builder LinearT;
  typedef Expr::PlaceHolder<FieldT>::Builder PlaceHolderT;
  typedef ReactorEnsembleUtil::ZeroDMixingCP<FieldT>::Builder ZeroDMixingCPT;
  typedef ReactorEnsembleUtil::SumOp<FieldT>::Builder SumOpT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivisionT;
  typedef Particles::SourceFromGasMassChange<FieldT>::Builder SourceFromGasMassT;
  typedef KobSarofim::KobayashiSarofimModel<FieldT>::Builder KobSaroT;

  //---------------------------------------------------------------------------
  KobSarofimTags::
  KobSarofimTags( const Expr::Context& state,
                  const std::vector<std::string>& particleNumArray)
  {
    yHVolTags        = Expr::tag_list( particleNumArray, state, "yH_vol_"              );
    yOVolTags        = Expr::tag_list( particleNumArray, state, "yO_vol_"              );
    yHVolRateTags    = Expr::tag_list( particleNumArray, state, "yH_vol_rate_"         );
    yOVolRateTags    = Expr::tag_list( particleNumArray, state, "yO_vol_rate_"         );
    yHVolInflowTags  = Expr::tag_list( particleNumArray, state, "yH_vol_", "_inflow"   );
    yOVolInflowTags  = Expr::tag_list( particleNumArray, state, "yO_vol_", "_inflow"   );
    yHVolMixRhsTags  = Expr::tag_list( particleNumArray, state, "yH_vol_", "_mix_rhs"  );
    yOVolMixRhsTags  = Expr::tag_list( particleNumArray, state, "yO_vol_", "_mix_rhs"  );
    yHVolKinRhsTags  = Expr::tag_list( particleNumArray, state, "yH_vol_", "_kin_rhs"  );
    yOVolKinRhsTags  = Expr::tag_list( particleNumArray, state, "yO_vol_", "_kin_rhs"  );
    yHVolFullRhsTags = Expr::tag_list( particleNumArray, state, "yH_vol_", "_full_rhs" );
    yOVolFullRhsTags = Expr::tag_list( particleNumArray, state, "yO_vol_", "_full_rhs" );
  }

    KobSarofimInterface::
    KobSarofimInterface(const Coal::CoalType coalType,
                        const Dev::DevModel dvm,
                        const TarAndSootInfo& tarSootInfo,
                        std::vector<std::string> particleNumArray,
                        std::vector<std::string> speciesArray,
                        std::vector<std::string> partNumSpeciesArray,
                        const YAML::Node& rootParser,
                        std::set<Expr::ExpressionID>& initRoots,
                        Expr::ExpressionFactory& initFactory,
                        Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                        Expr::TagList& primitiveTags,
                        Expr::TagList& kinRhsTags,
                        int& equIndex,
                        const bool isRestart)
          : Dev::DevolatilizationBase(),
            coalcomp_(coalType, rootParser["Particles"]),
            dvm_(dvm),
            tarSootInfo_(tarSootInfo),
            particleNumArray_(particleNumArray),
            speciesArray_(speciesArray),
            partNumSpeciesArray_(partNumSpeciesArray),
            rootParser_(rootParser),
            initRoots_(initRoots),
            initFactory_(initFactory),
            integrator_(integrator),
            primitiveTags_(primitiveTags),
            kinRhsTags_(kinRhsTags),
            equIndex_(equIndex),
            isRestart_(isRestart)
    {}
  //---------------destructor---------------------------
  KobSarofimInterface::~KobSarofimInterface(){
    std::cout << "Delete ~KobSarofimInterface(): " << this << std::endl;
  }


  //---------------------------------------------------------------------------
  void KobSarofimInterface::setup_initial_conditions_dev(){
    const Coal::CoalEnsembleTags tagsCoal( Expr::STATE_N, particleNumArray_ );
    const KobSarofim::KobSarofimTags tagsKoobS( Expr::STATE_N, particleNumArray_ );

    KobSarofimInformation kobsarofimdate(coalcomp_);
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsKoobS.yHVolTags[i], tagsCoal.volMassFracTags[i], kobsarofimdate.get_hydrogen_mass_fraction(), 0.0)));
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsKoobS.yOVolTags[i], tagsCoal.volMassFracTags[i], kobsarofimdate.get_oxygen_mass_fraction(), 0.0)));
      if(reactorType!="PFR"){
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsKoobS.yHVolInflowTags[i], tagsCoal.volMassFracInflowTags[i], kobsarofimdate.get_hydrogen_mass_fraction(), 0.0)));
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsKoobS.yOVolInflowTags[i], tagsCoal.volMassFracInflowTags[i], kobsarofimdate.get_oxygen_mass_fraction(), 0.0)));
      }
    }
  }

  //---------------------------------------------------------------------------
  void KobSarofimInterface::build_equation_system_coal_dev(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Dev::DevTags tagsEDev( Expr::STATE_NONE, particleNumArray_, speciesArray_, partNumSpeciesArray_ );
    const KobSarofim::KobSarofimTags tagsEKoobS( Expr::STATE_NONE, particleNumArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

    Expr::ExpressionFactory& execFactory = integrator_->factory();

    const int nparsize = particleNumArray_.size();
    const int nspecies = speciesArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      Expr::TagList devrhsTags;
      devrhsTags.clear();
      devrhsTags.push_back( tagsEDev.volRateTags[i] );
      devrhsTags.push_back( tagsEDev.volCharTags[i] );
      for( size_t j = 0;j < nspecies; ++j){
        devrhsTags.push_back( tagsEDev.volSpeciesGasTags[i*nparsize+j] );
      }
      devrhsTags.push_back( tagsEDev.volTarTags[i]);
      devrhsTags.push_back( tagsEKoobS.yHVolRateTags[i]);
      devrhsTags.push_back( tagsEKoobS.yOVolRateTags[i]);

      execFactory.register_expression( new KobSaroT( devrhsTags, tagsEPart.partTempTags[i], tagsECoal.volMassFracTags[i], tagsEKoobS.yHVolTags[i],
                                                     tagsEKoobS.yOVolTags[i], tarSootInfo_ ) );

      execFactory.register_expression( new SourceFromGasMassT( tagsEKoobS.yHVolKinRhsTags[i], tagsEPart.partToGasMassTag, tagsEKoobS.yHVolTags[i]));
      execFactory.attach_dependency_to_expression( tagsEKoobS.yHVolRateTags[i], tagsEKoobS.yHVolKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);

      execFactory.register_expression( new SourceFromGasMassT( tagsEKoobS.yOVolKinRhsTags[i], tagsEPart.partToGasMassTag, tagsEKoobS.yOVolTags[i]));
      execFactory.attach_dependency_to_expression( tagsEKoobS.yOVolRateTags[i], tagsEKoobS.yOVolKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);

      if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>()!="PFR"){
        execFactory.register_expression( new PlaceHolderT( tagsEKoobS.yHVolInflowTags[i]));
        execFactory.register_expression( new PlaceHolderT( tagsEKoobS.yOVolInflowTags[i]));
        execFactory.register_expression( new ZeroDMixingCPT( tagsEKoobS.yHVolMixRhsTags[i], tagsEKoobS.yHVolInflowTags[i], tagsEKoobS.yHVolTags[i],
                                                             tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
        execFactory.register_expression( new SumOpT( tagsEKoobS.yHVolFullRhsTags[i], {tagsEKoobS.yHVolMixRhsTags[i], tagsEKoobS.yHVolKinRhsTags[i]}));
        execFactory.register_expression( new ZeroDMixingCPT( tagsEKoobS.yOVolMixRhsTags[i], tagsEKoobS.yOVolInflowTags[i], tagsEKoobS.yOVolTags[i],
                                                             tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
        execFactory.register_expression( new SumOpT( tagsEKoobS.yOVolFullRhsTags[i], {tagsEKoobS.yOVolMixRhsTags[i], tagsEKoobS.yOVolKinRhsTags[i]}));
      }
      else{
        execFactory.register_expression( new DivisionT( tagsEKoobS.yHVolFullRhsTags[i], tagsEKoobS.yHVolKinRhsTags[i], tagsEpfr.uTag ));
        execFactory.register_expression( new DivisionT( tagsEKoobS.yOVolFullRhsTags[i], tagsEKoobS.yOVolKinRhsTags[i], tagsEpfr.uTag ));
      }
      // add variables to the integrator
      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsEKoobS.yHVolTags[i].name(), tagsEKoobS.yHVolFullRhsTags[i], equIndex_, equIndex_ );
      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsEKoobS.yOVolTags[i].name(), tagsEKoobS.yOVolFullRhsTags[i], equIndex_, equIndex_ );
    }
  }

  void KobSarofimInterface::initialize_coal_dev()
  {
    const KobSarofim::KobSarofimTags tagsEKoobS( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>()!="PFR"){
      for( size_t i = 0;i < nparsize;i++ ){
        if(!isRestart_){
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEKoobS.yHVolInflowTags[i].name());
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsEKoobS.yOVolInflowTags[i].name());
        }
        integrator_->lock_field<FieldT>( tagsEKoobS.yHVolInflowTags[i] );
        integrator_->lock_field<FieldT>( tagsEKoobS.yOVolInflowTags[i] );
      }
    }
  }

  void KobSarofimInterface::modify_prim_kinrhs_tags_dev()
  {
    const KobSarofim::KobSarofimTags tagsKoobS( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primitiveTags_.push_back(tagsKoobS.yHVolTags[i] );
      primitiveTags_.push_back(tagsKoobS.yOVolTags[i] );
      if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>()!="PFR"){
        kinRhsTags_.push_back( tagsKoobS.yHVolKinRhsTags[i] );
        kinRhsTags_.push_back( tagsKoobS.yOVolKinRhsTags[i] );
      }
      else{
        kinRhsTags_.push_back( tagsKoobS.yHVolFullRhsTags[i] );
        kinRhsTags_.push_back( tagsKoobS.yOVolFullRhsTags[i] );
      }
    }
  }

  void KobSarofimInterface::modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                                               const std::map<Expr::Tag, int> &consVarIdxMap,
                                               std::map<Expr::Tag, int> &kinRhsIdxMap,
                                               const  std::map<Expr::Tag, int> &rhsIdxMap)
  {
    const KobSarofim::KobSarofimTags tagsKoobS( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primVarIdxMap[tagsKoobS.yHVolTags[i]] = consVarIdxMap.at( tagsKoobS.yHVolTags[i]);
      primVarIdxMap[tagsKoobS.yOVolTags[i]] = consVarIdxMap.at( tagsKoobS.yOVolTags[i]);
      if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>()!="PFR"){
        kinRhsIdxMap[tagsKoobS.yHVolKinRhsTags[i]] = rhsIdxMap.at( tagsKoobS.yHVolFullRhsTags[i] );
        kinRhsIdxMap[tagsKoobS.yOVolKinRhsTags[i]] = rhsIdxMap.at( tagsKoobS.yOVolFullRhsTags[i] );
      }
      else{
        kinRhsIdxMap[tagsKoobS.yHVolFullRhsTags[i]] = rhsIdxMap.at( tagsKoobS.yHVolFullRhsTags[i] );
        kinRhsIdxMap[tagsKoobS.yOVolFullRhsTags[i]] = rhsIdxMap.at( tagsKoobS.yOVolFullRhsTags[i] );
      }
    }
  }

  void KobSarofimInterface::dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                           std::map<Expr::Tag, int> primVarIdxMap,
                                           std::map<Expr::Tag, int> consVarIdxMap)
  {
    const KobSarofim::KobSarofimTags tagsEKobS( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();

    for( size_t i = 0;i < nparsize;i++ ){
      dVdUPart->element<double>(primVarIdxMap.at( tagsEKobS.yHVolTags[i] ), consVarIdxMap.at( tagsEKobS.yHVolTags[i] )) = 1.0;
      dVdUPart->element<double>(primVarIdxMap.at( tagsEKobS.yOVolTags[i] ), consVarIdxMap.at( tagsEKobS.yOVolTags[i] )) = 1.0;
    }
  }

  void KobSarofimInterface::dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                                 std::map<Expr::Tag, int> primVarIdxMap,
                                                 std::map<Expr::Tag, int> rhsIdxMap,
                                                 std::map<Expr::Tag, Expr::Tag> consVarRhsMap)
  {
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const KobSarofim::KobSarofimTags tagsEKobS( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    const Expr::Tag&     rho         = tagsE.rhoTag;
    const Expr::TagList& yHVol       = tagsEKobS.yHVolTags;
    const Expr::TagList& yOVol       = tagsEKobS.yOVolTags;
    const Expr::TagList& yHVolMix    = tagsEKobS.yHVolMixRhsTags;
    const Expr::TagList& yOVolMix    = tagsEKobS.yOVolMixRhsTags;
    for( size_t i = 0;i < nparsize;i++ ){
      const auto& yHVolRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsEKobS.yHVolTags[i] ) );
      const auto& yHVolIdxi    = primVarIdxMap.at( tagsEKobS.yHVolTags[i] );
      const auto& yOVolRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsEKobS.yOVolTags[i] ) );
      const auto& yOVolIdxi    = primVarIdxMap.at( tagsEKobS.yOVolTags[i] );

      dmMixingdVPart->element<FieldT>( yHVolRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(yHVolMix[i], rho);
      dmMixingdVPart->element<FieldT>( yHVolRhsIdxi, yHVolIdxi ) = sensitivity(yHVolMix[i], yHVol[i]);

      dmMixingdVPart->element<FieldT>( yOVolRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(yOVolMix[i], rho);
      dmMixingdVPart->element<FieldT>( yOVolRhsIdxi, yOVolIdxi ) = sensitivity(yOVolMix[i], yOVol[i]);
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void KobSarofimInterface::restart_var_coal_dev(std::vector<std::string>& restartVars)
  {}

} // namespace KOBSAROFIM
