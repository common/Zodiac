/**
 * @file KobayashiSarofim.h
 * @brief Getting the devolatilization production rate from Kobayashi-Sarofim model
 * @author Hang Zhou
 */
#ifndef Dev_KobayashiSarofim_Expr_h
#define Dev_KobayashiSarofim_Expr_h

#include <expression/Expression.h>
#include <spatialops/structured/SpatialFieldStore.h>

namespace KobSarofim{
/**
 *  @class   KobayashiSarofimModel
 *  @ingroup Kobsarofim
 *  @brief   Evaluates the devolatilization rate and product by
 *           Kobayashi & Sarofim model (unit:1/s)
 *
 * Two competed reactions are considered in Kobayashi-Sarofim model.
 * \f[
 *    m_v \rightarrow \alpha_1 g_1 + (1-\alpha_1) C
 * \f]
 * \f[
 *    m_v \rightarrow \alpha_2 g_2 + (1-\alpha_2) C
 * \f]
 * Here, \f$ m_v\f$ is the mass of volatilize per gas mass. \f$ C\f$ is carbon.
 *        \f$ g_1 \f$ and \f$g_2\f$ includ the lignt gas and tar, and they are assumed to be the same.
 *       \f$\alpha_1\f$ and \f$\alpha_2\f$ are mass fraction of \f$ g_i \f$ from \f$ m_v\f$ in two reactions respetively.
 *
 *  The reaction rates of above two reactions are:
 *  \f[
 *     r_1 = A_1 \exp(-E_1/(R T_p))
 *  \f]
 *  \f[
 *     r_2 = A_2 \exp(-E_2/(R T_p))
 *  \f]
 * Here, \f$A_1\f$ and \f$A_2\f$ are pre-exponential factors for two reactions respectively.
 *       \f$E_1\f$ and \f$E_2\f$ are activation energies for two reactions respectively.
 *       \f$T_p\f$ is the particle temperature.
 * Devolatilization rate is given as:
 * \f[
 *    \frac{dm_v}{dt} = -(r_1+r_2) m_v
 * \f]
 *
 * The production of \f$C\f$ could be writen as:
 * \f[
 *    \frac{dm_C}{dt} = - \left(\beta(1-\alpha_1)+ (1-\beta)(1-\alpha_2) \right)\frac{dm_v}{dt}
 * \f]
 * Here, \f$Mv_C\f$ and \f$Mv_v\f$ are the molecular weights of carbon and volatile.
 *       \f$ \beta = \frac{r_1}{r_1+r_2} \f$.
 *
 * The production rate of \f$g_i\f$ is given as:
 * \f[
 *    \frac{dg_i}{dt} = - \left(\beta \alpha_1+ (1-\beta) \alpha_2 \right)\frac{dm_v}{dt}
 * \f]
 *
 * H and O reaction rates in volatile are given:
 * \f[
 *    \frac{dY_{H,vol}}{dt} = \frac{Y_{H,vol}}{m_v} \frac{dm_v}{dt}
 * \f]
 * \f[
 *   \frac{dY_{O,vol}}{dt} = \frac{Y_{O,vol}}{m_v} \frac{dm_v}{dt}
 * \f]
 * Here, \f$Y_{H,vol}\f$ and \f$Y_{O,vol}\f$ are the mass of H and O in volatile per gas mass.
 *
 * The CO production rate is: (The mole reation rate of CO is the same that of \f$Y_{O,vol}\f$.)
 * \f[
 *    \frac{dm_{CO}}{dt} = - \frac{dY_{O,vol}}{dt} \frac{Mw_{CO}}{Mw_O}
 * \f]
 * Here, \f$ Mw_{CO}\f$ and \f$ Mw_O\f$ are the molecular weight of CO and O respectively.
 *
 * The tar production rate is:
 * \f[
 *    \frac{dm_{tar}}{dt} = (\frac{dg_i}{dt}-\frac{dm_{CO}}{dt}-\frac{dY_{H,vol}}{dt})\frac{1}{1-\frac{n Mw_H}{Mw_{tar}}}
 * \f]
 * Here, \f$ Mw_H\f$ and \f$ Mw_{tar}\f$ are the molecular weight of H and tar respectively.
 *       \f$ n \f$ is the H coefficient in tar \f$C_mH_n\f$.
 *       The first term \f$\frac{dg_i}{dt}-\frac{dm_{CO}}{dt}-\frac{dY_{H,vol}}{dt}\f$ is the production rate of C in tar.
 *       The second term \f$1-\frac{n Mw_H}{Mw_{tar}\f$ is the mass fraction of C in tar.
 *
 * The H2 production rate is:
 * \f[
 *    \frac{dm_{H_2}}{dt} = \frac{dg_i}{dt}-\frac{dm_{CO}}{dt}-\frac{dm_{tar}}{dt}
 * \f]
 *
 * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
 *          All the parameters used here are the value for ONE specific particle size.
 *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */

  template <typename FieldT>
  class KobayashiSarofimModel: public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, tempP_, mv_, h_, o_)
    const TarAndSootInfo& tarSootInfo_;
    const double tarMW_, hTar_, cTar_;
    const double R_, A1_, A2_, E1_, E2_, alpha1_, alpha2_;

    KobayashiSarofimModel( const Expr::Tag& tempPtag,
                           const Expr::Tag& mvtag,
                           const Expr::Tag& yHVolTag,
                           const Expr::Tag& yOVolTag,
                           const TarAndSootInfo& tarSootInfo)
          : Expr::Expression<FieldT>(),
            tarSootInfo_(tarSootInfo),
            tarMW_( tarSootInfo.tar_mw()            ),
            hTar_ ( tarSootInfo.tar_hydrogen()      ),
            cTar_ ( tarSootInfo.tar_carbon()        ),
            R_    ( 1.987                           ), // kcal/kmol; ideal gas constant
            A1_   ( 3.7e5                           ), // 1/s; k1 pre-exponential factor // Values from Ubhayakar (1976):
            A2_   ( 1.46e13                         ), // 1/s; k2 pre-exponential factor
            E1_   ( 17600                           ), // kcal/kmol;  k1 activation energy
            E2_   ( 60000                           ), // kcal/kmol;  k2 activation energy
            alpha1_( 0.3                            ), // volatile fraction from proximate analysis
            alpha2_( 1.0                            )  // fraction devolatilized at higher temperatures
    {
      this->set_gpu_runnable( true );
      tempP_ = this->template create_field_request<FieldT>( tempPtag );
      mv_    = this->template create_field_request<FieldT>( mvtag    );
      h_     = this->template create_field_request<FieldT>( yHVolTag );
      o_     = this->template create_field_request<FieldT>( yOVolTag );
    }
  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tempPTag_, mvTag_, hTag_, oTag_;
        const TarAndSootInfo& tarSootInfo_;
    public:
        /**
         * @brief The mechanism for building a KobayashiSarofimModel object
         * @tparam FieldT
         * @param resultTags Devolatilization rate of particles
         * @param tempPTag Particle temperature
         * @param mvTag Mass of volatile per gas mass in the reactor
         * @param yHVolTag Mass of H in volatile per gas mass
         * @param yOVolTag Mass of O in volatile per gas mass
         * @param tarSootInfo TarAndSootInfo
         */
      Builder( const Expr::TagList& resultTags,
               const Expr::Tag& tempPTag,
               const Expr::Tag& mvTag,
               const Expr::Tag& yHVolTag,
               const Expr::Tag& yOVolTag,
               const TarAndSootInfo& tarSootInfo)
            : ExpressionBuilder( resultTags ),
              tempPTag_   ( tempPTag    ),
              mvTag_      ( mvTag       ),
              hTag_       ( yHVolTag    ),
              oTag_       ( yOVolTag    ),
              tarSootInfo_( tarSootInfo )
      {}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new KobayashiSarofimModel<FieldT>( tempPTag_, mvTag_, hTag_, oTag_, tarSootInfo_);
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& mvcharrhs = this->get_value_vec();

      FieldT& mvrhs   = *mvcharrhs[0]; // Volitle RHS
      FieldT& charrhs = *mvcharrhs[1]; // Char    RHS

      // Produced Species according to reaction [1]
      FieldT& co      = *mvcharrhs[2]; // CO   Consumption rate ( for gas Phase )
      FieldT& h2      = *mvcharrhs[3]; // H2   Consumption rate ( for gas Phase )
      FieldT& tar     = *mvcharrhs[4];// tar  Consumption rate ( for gas Phase )

      FieldT& dh      = *mvcharrhs[5]; // Hydrogen Consumption rate
      FieldT& dox     = *mvcharrhs[6]; // Oxygen Consumption rate

      /*
      const double Y1 = 0.39; // volatile fraction from proximate analysis
      const double Y2 = 0.80; // fraction devolatilized at higher temperatures
       */
      /*
       // Values from Kobayashi (1976):
      const double A1  =  2.0e5;       // 1/s; pre-exponential factor for k1
      const double A2  =  1.3e7;       // 1/s; pre-exponential factor for k2
      const double E1  =  -25000;      // kcal/kmol;  k1 activation energy
      const double E2  =  -40000;      // kcal/kmol;  k2 activation energy
       */

      /* Method of calculation
       *
       *  B1 = A1 * exp(-E1/RT) * mv
       *  B2 = A2 * exp(-E2/RT) * mv
       *
       *  mvrhs   = B1 + B2
       *  charrhs = (1-Y1)B1 + (1-Y2)B2 = (1-Y1)B1
       *  Species = Y1B1 + Y2B2 = Y1B1 + B2
       */
      // Y values from white book:

      SpatFldPtr<FieldT> r1 = SpatialFieldStore::get<FieldT>( mvrhs );
      SpatFldPtr<FieldT> r2 = SpatialFieldStore::get<FieldT>( mvrhs );

      SpatFldPtr<FieldT> beta = SpatialFieldStore::get<FieldT>( mvrhs );
      SpatFldPtr<FieldT> a1 = SpatialFieldStore::get<FieldT>( mvrhs );
      SpatFldPtr<FieldT> a2 = SpatialFieldStore::get<FieldT>( mvrhs );
      SpatFldPtr<FieldT> girhs = SpatialFieldStore::get<FieldT>( mvrhs );

      const FieldT& tempP = tempP_->field_ref();
      const FieldT& mv    = mv_   ->field_ref();
      const FieldT& yHVol = h_    ->field_ref();
      const FieldT& yOVol = o_    ->field_ref();

      *r1 <<= A1_* exp(-E1_/(R_ * tempP));
      *r2 <<= A2_ * exp(-E2_/(R_ * tempP));

      mvrhs   <<= cond( mv >0.0,  -(*r1 + *r2) * mv)
                      (           0.0              ) ; // volatile mass RHS; // negative

      *beta  <<= *r1 / (*r1 + *r2);
      *a1 <<= *beta*alpha1_+(1-*beta)*alpha2_;
      *a2 <<= *beta * (1.0 - alpha1_) + (1.0 - *beta) * (1.0 - alpha2_);
      *girhs <<= - *a1 * mvrhs; //positive
      charrhs <<= - *a2 * mvrhs; //positive

      dh <<= yHVol/mv * mvrhs;
      dox <<= yOVol/mv * mvrhs;

      co <<= - yOVol/mv * mvrhs / 16.0 * 28.0; //positive
      tar <<= cond((*girhs - co + dh)>0.0, (*girhs - co + dh) / (1-hTar_/tarMW_))(0.0);
      h2 <<= cond((*girhs - co - tar)>0.0, *girhs - co - tar)(0.0);

    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      typename Expr::Expression<FieldT>::ValVec& mvcharrhs = this->get_value_vec();
      std::vector<FieldT > drhsdv;
      for(size_t i=0;i<targetTags.size();++i){
        drhsdv.push_back(this->sensitivity_result(targetTags[i], var));
      }

      const FieldT& tempP    = tempP_->field_ref();
      const FieldT& mv       = mv_   ->field_ref();
      const FieldT& yHVol    = h_    ->field_ref();
      const FieldT& yOVol    = o_    ->field_ref();
      const FieldT& dtempPdv = tempP_->sens_field_ref(var);
      const FieldT& dmvdv    = mv_   ->sens_field_ref(var);
      const FieldT& dyHVoldv = h_    ->sens_field_ref(var);
      const FieldT& dyOVoldv = o_    ->sens_field_ref(var);

      SpatFldPtr<FieldT> r1        = SpatialFieldStore::get<FieldT>( *mvcharrhs[0] );
      SpatFldPtr<FieldT> r2        = SpatialFieldStore::get<FieldT>( *mvcharrhs[0] );
      SpatFldPtr<FieldT> dr1dv     = SpatialFieldStore::get<FieldT>( *mvcharrhs[0] );
      SpatFldPtr<FieldT> dr2dv     = SpatialFieldStore::get<FieldT>( *mvcharrhs[0] );

      SpatFldPtr<FieldT> beta = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> dbetadv = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> a1 = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> a2 = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> da1dv = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> da2dv = SpatialFieldStore::get<FieldT>(*mvcharrhs[0] );
      SpatFldPtr<FieldT> girhs = SpatialFieldStore::get<FieldT>( *mvcharrhs[0]);
      SpatFldPtr<FieldT> dgirhsdv = SpatialFieldStore::get<FieldT>( *mvcharrhs[0]);


      *r1 <<= A1_* exp(-E1_/(R_ * tempP));
      *r2 <<= A2_ * exp(-E2_/(R_ * tempP));
      *dr1dv <<= *r1 * (E1_/(R_ * square(tempP))) * dtempPdv;
      *dr2dv <<= *r2 * (E2_/(R_ * square(tempP))) * dtempPdv;

      drhsdv[0] <<= cond( mv >0.0,  -( (*dr1dv + *dr2dv) * mv + (*r1+*r2) * dmvdv))
                        (0.0);

      *beta  <<= *r1 / (*r1 + *r2);
      *dbetadv  <<= *dr1dv / (*r1 + *r2) - *r1/square(*r1+*r2) *(*dr1dv+*dr2dv);
      *a1 <<= *beta*alpha1_+(1-*beta)*alpha2_;
      *da1dv <<= *dbetadv*alpha1_-*dbetadv*alpha2_;
      *a2 <<= *beta * (1.0 - alpha1_) + (1.0 - *beta) * (1.0 - alpha2_);
      *da2dv <<= *dbetadv * (1.0 - alpha1_) - *dbetadv * (1.0 - alpha2_);
      *girhs <<= - *a1 * *mvcharrhs[0]; //positive
      *dgirhsdv <<= - *da1dv * *mvcharrhs[0] - *a1 * drhsdv[0]; //positive

      drhsdv[1] <<= -*da2dv * *mvcharrhs[0] - *a2 * drhsdv[0];

      drhsdv[2] <<= -28.0/16.0 *(dyOVoldv/mv* *mvcharrhs[0]
                                 - yOVol/square(mv)*dmvdv * *mvcharrhs[0]
                                 + yOVol/mv* drhsdv[0]);
      drhsdv[5] <<= dyHVoldv/mv* *mvcharrhs[0] - yHVol/square(mv)*dmvdv* *mvcharrhs[0] + yHVol/mv*drhsdv[0];
      drhsdv[6] <<= dyOVoldv/mv* *mvcharrhs[0] - yOVol/square(mv)*dmvdv* *mvcharrhs[0] + yOVol/mv*drhsdv[0];
      drhsdv[4] <<= cond((*girhs - *mvcharrhs[2] + *mvcharrhs[5])>0.0, (*dgirhsdv - drhsdv[2] + drhsdv[5]) / (1-hTar_/tarMW_))(0.0);
      drhsdv[3] <<= cond((*girhs - *mvcharrhs[2] - *mvcharrhs[4])>0.0, *dgirhsdv - drhsdv[2] - drhsdv[4])(0.0);
    }
  };

} // end of namespace KobSarofim

#endif // Dev_KobayashiSarrofim_Expr_h
