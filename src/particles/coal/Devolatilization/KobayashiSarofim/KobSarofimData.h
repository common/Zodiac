#ifndef SARROFIM_KobSarofimInformation_h
#define SARROFIM_KobSarofimInformation_h

/**
 * @file KobSarofimData.h
 * @par Getting the data needed for KobSarofim model
 */

#include "particles/coal/CoalData.h"

namespace KobSarofim {

  class KobSarofimInformation{
  public:
    KobSarofimInformation( const Coal::CoalComposition& coalType );
    const double get_hydrogen_mass_fraction() const{return h_; };
    const double get_oxygen_mass_fraction()   const{return o_; };
  protected:
    double h_, o_;
    const Coal::CoalComposition& coalcomp_;
  };

} // namespace end

#endif // SARROFIM_KobSarofimInformation_h
