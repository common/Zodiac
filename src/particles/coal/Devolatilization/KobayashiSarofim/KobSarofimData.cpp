/*
 *  KobSarrofimData.cpp
 *  ODT
 *
 *  Created by Babak Goshayeshi on 6/13/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "KobSarofimData.h"

namespace KobSarofim {

  KobSarofimInformation::
  KobSarofimInformation( const Coal::CoalComposition& coalType )
  : coalcomp_ ( coalType )
  {
    const double c = coalcomp_.get_C();
    const double h = coalcomp_.get_H();
    const double o = coalcomp_.get_O();
    const double vol = coalcomp_.get_vm();
    const double fixC = coalcomp_.get_fixed_c();

    h_ = h*(vol+fixC) / vol;
    o_ = o*(vol+fixC) / vol;
  }

} // namespace end
