/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>

#include <pokitt/CanteraObjects.h>
#include <pokitt/thermo/Enthalpy.h>
#include <fstream>
#include <expression/ExpressionFactory.h>

#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "ZeroDimMixing.h"
#include "SumOp.h"

#include "particles/ParticleInterface.h"
#include "particles/ParticleTransformExpressions.h"
#include "particles/coal/CoalData.h"
#include "particles/coal/CoalInterface.h"
#include "DevolatilizationInterface.h"
#include "CPD/c0_fun.h"
#include "particles/coal/TarSootReaction/TarAndSootInfo.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

namespace Dev {

  typedef pokitt::SpeciesEnthalpy<FieldT>::Builder SpecEnthT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivisionT;

  typedef Particles::PartToGasCV<FieldT>::Builder PartToGasCVT;
  typedef Particles::GasMassSourceFromPart<FieldT>::Builder GasMassSourceT;
  typedef Particles::SpeciesSourceFromPart<FieldT>::Builder SpeciesSourceT;
  typedef Particles::GasEnergySourceFromPart<FieldT>::Builder GasEnergySourceT;
  typedef Particles::DpDxSourceFromGasEnthalpyChange<FieldT>::Builder DpDxEnthalpyT;

    //---------------------------------------------------------------------------
  DevTags::
  DevTags( const Expr::Context& state,
           const std::vector<std::string>& particleNumArray,
           const std::vector<std::string>& SpeciesArray,
           const std::vector<std::string>& partNumSpeciesArray)
        : volMassGasSourceTag    ( Expr::Tag( "vol_mass_gas_source"     , state ) ),
          volHeatGasSourceTag    ( Expr::Tag( "vol_heat_gas_source"     , state ) ),
          volHeatGasSourcePFRTag ( Expr::Tag( "vol_heat_gas_source_pfr" , state ) ),
          volMassToGasTag        ( Expr::Tag( "vol_mass_to_gas"         , state ) ),
          volHeatToGasTag        ( Expr::Tag( "vol_heat_to_gas"         , state ) ),
          volHeatDpDxTag         ( Expr::Tag( "vol_heat_dpdx"           , state ) )
  {
    volRateTags                = Expr::tag_list( particleNumArray,                state, "vol_rate_"              );
    volSpeciesSourceTags       = Expr::tag_list( SpeciesArray,                    state, "vol_species_source_"    );
    volSpeciesSourcePFRTags    = Expr::tag_list( SpeciesArray,                    state, "vol_species_source_pfr_");
    volSpeciesRHSTags          = Expr::tag_list( SpeciesArray,                    state, "vol_species_rhs_"       );
    volSpeciesGasTags          = Expr::tag_list( partNumSpeciesArray,             state, "vol_species_mass_"      );
    volTarTags                 = Expr::tag_list( particleNumArray,                state, "vol_tar_"               );
    volCharTags                = Expr::tag_list( particleNumArray,                state, "vol_char_"              );
    enthTpartTags              = Expr::tag_list( partNumSpeciesArray,             state, "enthTpart_"             );
  }

  DevolatilizationInterface::
  DevolatilizationInterface( const Coal::CoalType coalType,
                             const DevModel dvm,
                             const Tarsoot::TarsootModel tsm,
                             std::vector<std::string> particleNumArray,
                             std::vector<std::string> SpeciesArray,
                             std::vector<std::string> partNumSpeciesArray,
                             const YAML::Node& rootParser,
                             std::set<Expr::ExpressionID>& initRoots,
                             Expr::ExpressionFactory& initFactory,
                             Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                             Expr::TagList& primitiveTags,
                             Expr::TagList& kinRhsTags,
                             int& equIndex,
                             const bool isRestart)
        : coaltype_(coalType),
          dvm_(dvm),
          tsm_(tsm),
          particleNumArray_(particleNumArray),
          speciesArray_(SpeciesArray),
          partNumSpeciesArray_(partNumSpeciesArray),
          rootParser_(rootParser),
          initRoots_(initRoots),
          initFactory_(initFactory),
          integrator_(integrator),
          primitiveTags_(primitiveTags),
          kinRhsTags_(kinRhsTags),
          equIndex_(equIndex),
          isRestart_(isRestart)
  {
    speciesArray_.clear();
    if (dvm_ == CPDM){
      speciesArray_.push_back("CO2");
      speciesArray_.push_back("H2O");
      speciesArray_.push_back("CO");
      speciesArray_.push_back("HCN");
      speciesArray_.push_back("NH3");
      speciesArray_.push_back("CH4");
      speciesArray_.push_back("H2");
    }
    else{
      speciesArray_.push_back("CO");
      speciesArray_.push_back("H2");
    }
    partNumSpeciesArray_.clear();
    for( size_t i = 0;i < particleNumArray_.size();i++){
      std::string I = std::to_string(i);
      for( size_t j = 0;j < speciesArray_.size();j++){
        partNumSpeciesArray_.push_back( speciesArray_[j] + "_" + I );
      }
    }
    TarAndSootInfo::setup(rootParser_);
    const TarAndSootInfo& tarSootInfo = TarAndSootInfo::get();
    switch (dvm_) {
      case INVALID_DEVMODEL:
        throw std::invalid_argument( "Invalid model in Devolatilization Interface constructor" );
        break;
      case OFF:
        std::cout << "Devolatilization model is turned off" << std::endl;
        break;
      case SINGLERATE:
        singleratemdel_ = new SingleRate::SingleRateInterface(coaltype_, dvm,  tarSootInfo, particleNumArray_, speciesArray_, partNumSpeciesArray_,
                                                              rootParser_, integrator_, isRestart_);
        devmodel_ = singleratemdel_;
        break;
      case KOBAYASHIM:
        kobmodel_ = new KobSarofim::KobSarofimInterface(coaltype_, dvm, tarSootInfo, particleNumArray_, speciesArray_, partNumSpeciesArray_, rootParser_,
                                                        initRoots_, initFactory_, integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_);
        devmodel_ = kobmodel_;
        break;
      case CPDM:
        cpdmodel_ = new CPD::CPDInterface(coaltype_, dvm, tarSootInfo, particleNumArray_, speciesArray_, partNumSpeciesArray_, rootParser_, initRoots_, initFactory_,
                                          integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_);
        devmodel_ = cpdmodel_;
    }
    if(tsm_ != Tarsoot::OFF){
      tarsoot_ = new Tarsoot::TarSootInterface(coaltype_, tarSootInfo, particleNumArray_, speciesArray_, partNumSpeciesArray_, rootParser_, initRoots_, initFactory_,
                                               integrator_, primitiveTags_, kinRhsTags_, equIndex_, isRestart_);
    }
    if(!isRestart_){
      setup_initial_conditions_dev();
    }
    build_equation_system_coal_dev();
    modify_prim_kinrhs_tags_dev();
  }

  //---------------destructor---------------------------
  DevolatilizationInterface::~DevolatilizationInterface()
  {
    std::cout << "Delete ~DevolatilizationInterface(): " << this << std::endl;
    delete devmodel_;
    if(tsm_ != Tarsoot::OFF){
      delete tarsoot_;
    }
  }

  //---------------------------------------------------------------------------
  void DevolatilizationInterface::setup_initial_conditions_dev(){

    devmodel_->setup_initial_conditions_dev();
  }

  //---------------------------------------------------------------------------
  void DevolatilizationInterface::build_equation_system_coal_dev(){

    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Dev::DevTags tagsEDev( Expr::STATE_NONE, particleNumArray_, speciesArray_, partNumSpeciesArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    const int nparsize = particleNumArray_.size();
    const int nspecies = speciesArray_.size();

    Expr::ExpressionFactory& execFactory = integrator_->factory();
    execFactory.register_expression( new GasMassSourceT( tagsEDev.volMassGasSourceTag, tagsEDev.volSpeciesGasTags)); // negative
    execFactory.attach_dependency_to_expression( tagsEDev.volMassGasSourceTag, tagsEPart.partToGasMassTag, Expr::SUBTRACT_SOURCE_EXPRESSION);
    execFactory.register_expression( new SpeciesSourceT( tagsEDev.volSpeciesSourceTags, tagsEDev.volSpeciesGasTags, speciesArray_.size())); // positive
    execFactory.register_expression( new GasEnergySourceT( tagsEDev.volHeatGasSourceTag, tagsEDev.volSpeciesGasTags, tagsEDev.enthTpartTags));

    for( size_t i = 0;i < nparsize;i++ ){
      // modify particle mass fraction equations
      execFactory.attach_dependency_to_expression( tagsEDev.volRateTags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
      execFactory.attach_dependency_to_expression( tagsEDev.volCharTags[i], tagsEPart.partMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
      // modify vol mass fraction equations
      if(dvm_ != CPDM){
        execFactory.attach_dependency_to_expression( tagsEDev.volRateTags[i], tagsECoal.volMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
      }
      // modify char mass fraction equations
      execFactory.attach_dependency_to_expression( tagsEDev.volCharTags[i], tagsECoal.charMassFracKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION ); //positive

      for( size_t j = 0; j<nspecies; j++){
        const int Idx = CanteraObjects::species_index( speciesArray_[j] );
        execFactory.register_expression( new SpecEnthT( tagsEDev.enthTpartTags[i*nspecies+j], tagsEPart.partTempTags[i], Idx ) );
      }
    }
    // modifying gas equation
    // species and energy changes in gas from devolatilization
    if( reactorType== "PSRConstantVolume"){
      execFactory.register_expression( new PartToGasCVT( tagsEDev.volMassToGasTag, tagsEDev.volMassGasSourceTag, tagsE.rhoTag ) );
      execFactory.register_expression( new PartToGasCVT( tagsEDev.volHeatToGasTag, tagsEDev.volHeatGasSourceTag, tagsE.rhoTag ) );
      execFactory.attach_dependency_to_expression( tagsEDev.volMassToGasTag, tagsE.rhoKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsEDev.volHeatToGasTag, tagsE.rhoEgyKinRhsTag, Expr::SUBTRACT_SOURCE_EXPRESSION );
      for( size_t j = 0;j < nspecies;j++ ){
        const int Idx = CanteraObjects::species_index( speciesArray_[j] );
        execFactory.register_expression( new PartToGasCVT( tagsEDev.volSpeciesRHSTags[j], tagsEDev.volSpeciesSourceTags[j], tagsE.rhoTag ) );
        execFactory.attach_dependency_to_expression( tagsEDev.volSpeciesRHSTags[j], tagsE.rhoYKinRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
    }
    if( reactorType == "PSRConstantPressure"){
      execFactory.attach_dependency_to_expression( tagsEDev.volHeatGasSourceTag, tagsE.enthKinRhsTag, Expr::SUBTRACT_SOURCE_EXPRESSION );
      for( size_t j = 0;j < nspecies;j++ ){
        const int Idx = CanteraObjects::species_index( speciesArray_[j] );
        execFactory.attach_dependency_to_expression( tagsEDev.volSpeciesSourceTags[j], tagsE.yKinRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
    }
    if( reactorType == "PFR"){
      execFactory.register_expression( new DivisionT( tagsEDev.volHeatGasSourcePFRTag, tagsEDev.volHeatGasSourceTag, tagsEpfr.uTag ));
      execFactory.attach_dependency_to_expression( tagsEDev.volHeatGasSourcePFRTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      execFactory.register_expression( new DpDxEnthalpyT( tagsEDev.volHeatDpDxTag, tagsEDev.volHeatGasSourceTag, tagsE.rhoTag, tagsE.tempTag,
                                                          tagsE.enthTag, tagsE.mmwTag, tagsE.cpTag, tagsEpfr.uTag));
      execFactory.attach_dependency_to_expression( tagsEDev.volHeatDpDxTag, tagsEpfr.dpdxTag, Expr::ADD_SOURCE_EXPRESSION );
      for( size_t j = 0;j < nspecies;j++ ){
        const int Idx = CanteraObjects::species_index( speciesArray_[j] );
        execFactory.register_expression( new DivisionT( tagsEDev.volSpeciesSourcePFRTags[j], tagsEDev.volSpeciesSourceTags[j], tagsEpfr.uTag ));
        execFactory.attach_dependency_to_expression( tagsEDev.volSpeciesSourcePFRTags[j], tagsE.yFullRhsTags[Idx], Expr::ADD_SOURCE_EXPRESSION );
      }
    }
    devmodel_->build_equation_system_coal_dev();
  }

  //---------------------------------------------------------------------------
  void DevolatilizationInterface::initialize_coal_dev(){
    devmodel_->initialize_coal_dev();
    if(tsm_ != Tarsoot::OFF){
      tarsoot_->initialize_coal_tarsoot();
    }
  }

  //---------------------------------------------------------------------------
  void DevolatilizationInterface::modify_prim_kinrhs_tags_dev(){
    devmodel_->modify_prim_kinrhs_tags_dev();
  }

  //---------------------------------------------------------------------------
  void DevolatilizationInterface::modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                                                     const std::map<Expr::Tag, int> &consVarIdxMap,
                                                     std::map<Expr::Tag, int> &kinRhsIdxMap,
                                                     const std::map<Expr::Tag, int> &rhsIdxMap){
    devmodel_->modify_idxmap_dev(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap);
    if(tsm_ != Tarsoot::OFF){
      tarsoot_->modify_idxmap_tarsoot(primVarIdxMap, consVarIdxMap, kinRhsIdxMap, rhsIdxMap);
    }
  }

  void DevolatilizationInterface::dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                                 std::map<Expr::Tag, int> primVarIdxMap,
                                                 std::map<Expr::Tag, int> consVarIdxMap){
    devmodel_->dvdu_coal_dev(dVdUPart, primVarIdxMap, consVarIdxMap);
    if(tsm_ != Tarsoot::OFF){
      tarsoot_->dvdu_coal_tarsoot(dVdUPart, primVarIdxMap, consVarIdxMap);
    }
  }

  void DevolatilizationInterface::dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                                       std::map<Expr::Tag, int> primVarIdxMap,
                                                       std::map<Expr::Tag, int> rhsIdxMap,
                                                       std::map<Expr::Tag, Expr::Tag> consVarRhsMap){
    devmodel_->dmMixingdv_coal_dev( dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap);

    if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>() == "PSRConstantPressure"){
      if(tsm_ != Tarsoot::OFF){
        tarsoot_->dmMixingdv_coal_tarsoot( dmMixingdVPart, primVarIdxMap, rhsIdxMap, consVarRhsMap);
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void DevolatilizationInterface::restart_var_coal_dev(std::vector<std::string>& restartVars){
    devmodel_->restart_var_coal_dev(restartVars);
  }

} // namespace Dev
