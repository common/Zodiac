#ifndef kb_Expr_h
#define kb_Expr_h
/**
 * @file kb.h
 * @par calculate reaction constant for l ( liable bridge )
 * @author Hang Zhou
 */
#include <expression/Expression.h>

#include "CPDData.h"

namespace CPD{

/**
 *  \ingroup CPD
 *  \class kb
 *  \brief calculate reaction constant for l ( liable bridge )
 *  \author Hang
 *
 *
 *  Reaction Constant for labile bridge reaction
 *  \f[
 *     k_{b}=A_{0} \exp{\left(-\frac{E}{RT}\right)}
 *  \f]
 *
 *  Reference:
 *  Grant, D. M., Pugmire, R. J., Fletcher, T. H., & Kerstein, A. R. (1989).
 *  Chemical Model of Coal Devolatilization Using Percolation Lattice Statistics. Energy & Fuels, 3, 175–186.
 */
  template<typename FieldT>
  class kb: public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, tempP_ )
    const double Ab_, gascon_;

    kb( const Expr::Tag tempPTag)
        : Expr::Expression<FieldT>(),
         Ab_(2.6e15),  // 1/s
         gascon_(1.985)
    {
      this->set_gpu_runnable(true);
      tempP_ = this->template create_field_request<FieldT>( tempPTag );
    }


  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tempPTag_;
    public:
      /**
       * \brief The mechanism for building a kb object
       *  \tparam FieldT
       *  \param kbTag reaction Constant for labile bridge reaction
       *  \param tempPTag particle temperature
       */
      Builder( const Expr::Tag kbTag,
               const Expr::Tag tempPTag)
            : ExpressionBuilder(kbTag),
              tempPTag_        (tempPTag ){}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new kb<FieldT>( tempPTag_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;

      const FieldT& tempP = tempP_->field_ref();

      this->value() <<= Ab_ * exp( - 55.4E3 / (gascon_ * tempP) ); // E0 was divided by R already
    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result(var);

      const FieldT& tempP    = tempP_->field_ref();
      const FieldT& dtempPdv = tempP_->sens_field_ref(var);
      dfdv <<= Ab_ * exp(- 55.4E3 / (gascon_ * tempP) ) * 55.4E3 / ( gascon_ * tempP * tempP) * dtempPdv;
    }
  };

} // namespace CPD

#endif // kb_Expr_h
