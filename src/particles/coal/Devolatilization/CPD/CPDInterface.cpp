#include "SumOp.h"
#include "ZeroDimMixing.h"
#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "particles/ParticleInterface.h"
#include "particles/ParticleTransformExpressions.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

// expressions we build here
#include "CPDInterface.h"
#include "F_E_fun.h"
#include "E_fun.h"
#include "L_RHS.h"
#include "CPDData.h"
#include "C_RHS.h"
#include "Gi_RHS.h"
#include "Deltai_RHS.h"
#include "c0_fun.h"
#include "kb.h"
#include "kg_i.h"
#include "MvRHS.h"
#include "Mv.h"
#include "dy_gi.h"

using Expr::matrix::sensitivity;

namespace CPD{

  typedef Expr::LinearFunction<FieldT>::Builder LinearT;
  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef Expr::PlaceHolder<FieldT>::Builder PlaceHolderT;

  typedef ReactorEnsembleUtil::SumOp<FieldT>::Builder SumT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivisionT;
  typedef ReactorEnsembleUtil::ZeroDMixingCP<FieldT>::Builder ZeroDMixingCPT;

  typedef Particles::SourceFromGasMassChange<FieldT>::Builder SourceFromGasMassT;

  typedef CPD::FE_fun_b<FieldT>::Builder FEfunbT;
  typedef CPD::FE_fun_gi<FieldT>::Builder FEfungiT;
  typedef CPD::Eb_fun<FieldT>::Builder EbfunT;
  typedef CPD::Egi_fun<FieldT>::Builder EgifunT;
  typedef CPD::kb<FieldT>::Builder kbT;
  typedef CPD::kg_i<FieldT>::Builder kgiT;
  typedef CPD::L_RHS<FieldT>::Builder LRhsT;
  typedef CPD::Deltai_RHS<FieldT>::Builder DeltaiRhsT;
  typedef CPD::Gi_RHS<FieldT>::Builder GiRhsT;
  typedef CPD::C_RHS<FieldT>::Builder CRhsT;
  typedef CPD::MvRHS<FieldT>::Builder MvRhsT;
  typedef CPD::Mv<FieldT>::Builder MvT;
  typedef CPD::dy_gi<FieldT>::Builder dygiT;

  //--------------------------------------------------------------------
  CPDTags::
  CPDTags( const Expr::Context& state,
           const std::vector<std::string>& particleNumArray,
           const std::vector<std::string>& cpdArray )
  {
    initPartMassFracTags         = Expr::tag_list( particleNumArray, state, "init_part_mass_frac_"            );
    lTags                        = Expr::tag_list( particleNumArray, state, "l_"                              );
    lInflowTags                  = Expr::tag_list( particleNumArray, state, "l_", "_inflow"                   );
    lMixRhsTags                  = Expr::tag_list( particleNumArray, state, "l_", "_mix_rhs"                  );
    lKinRhsTags                  = Expr::tag_list( particleNumArray, state, "l_", "_kin_rhs"                  );
    lReacTags                    = Expr::tag_list( particleNumArray, state, "l_rxn_"                          );
    lFullRhsTags                 = Expr::tag_list( particleNumArray, state, "l_", "_full_rhs"                 );
    fEfunbTags                   = Expr::tag_list( particleNumArray, state, "fE_fun_b_"                       );
    egybTags                     = Expr::tag_list( particleNumArray, state, "egy_b_"                          );
    kbTags                       = Expr::tag_list( particleNumArray, state, "kb_"                             );
    gTags                        = Expr::tag_list( cpdArray        , state, "g_"                              );
    gInflowTags                  = Expr::tag_list( cpdArray        , state, "g_", "_inflow"                   );
    gMixRhsTags                  = Expr::tag_list( cpdArray        , state, "g_", "_mix_rhs"                  );
    gKinRhsTags                  = Expr::tag_list( cpdArray        , state, "g_", "_kin_rhs"                  );
    gReacTags                    = Expr::tag_list( cpdArray        , state, "g_rxn_"                          );
    gFullRhsTags                 = Expr::tag_list( cpdArray        , state, "g_", "_full_rhs"                 );
    fEgTags                      = Expr::tag_list( cpdArray        , state, "fEg_"                            );
    egygTags                     = Expr::tag_list( cpdArray        , state, "egy_g_"                          );
    kgTags                       = Expr::tag_list( cpdArray        , state, "kg_"                             );
    deltaTags                    = Expr::tag_list( cpdArray        , state, "delta_"                          );
    deltaInflowTags              = Expr::tag_list( cpdArray        , state, "delta_", "_inflow"               );
    deltaMixRhsTags              = Expr::tag_list( cpdArray        , state, "delta_", "_mix_rhs"              );
    deltaKinRhsTags              = Expr::tag_list( cpdArray        , state, "delta_", "_kin_rhs"              );
    deltaReacTags                = Expr::tag_list( cpdArray        , state, "delta_rxn_"                      );
    deltaFullRhsTags             = Expr::tag_list( cpdArray        , state, "delta_", "_full_rhs"             );
  }

	
//--------------------------------------------------------------------
  CPDInterface::
  CPDInterface(const Coal::CoalType coalType,
               const Dev::DevModel dvm,
               const TarAndSootInfo& tarSootInfo,
               std::vector<std::string> particleNumArray,
               std::vector<std::string> speciesArray,
               std::vector<std::string> partNumSpeciesArray,
               const YAML::Node& rootParser,
               std::set<Expr::ExpressionID>& initRoots,
               Expr::ExpressionFactory& initFactory,
               Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
               Expr::TagList& primitiveTags,
               Expr::TagList& kinRhsTags,
               int& equIndex,
               const bool isRestart)
  : Dev::DevolatilizationBase(),
    cpdInfo_( coalType, rootParser["Particles"] ),
    dvm_(dvm),
    tarSootInfo_(tarSootInfo),
    particleNumArray_(particleNumArray),
    speciesArray_(speciesArray),
    partNumSpeciesArray_(partNumSpeciesArray),
    rootParser_(rootParser),
    initRoots_(initRoots),
    initFactory_(initFactory),
    integrator_(integrator),
    primitiveTags_(primitiveTags),
    kinRhsTags_(kinRhsTags),
    equIndex_(equIndex),
    isRestart_(isRestart),
    coalcomp_( cpdInfo_.get_coal_composition() ),
    c0( c0_fun( coalcomp_.get_C(), coalcomp_.get_O() )),
    tar0 ( tar_0(cpdInfo_, c0) ),
    vmFrac0 (coalcomp_.get_vm()*(1.0-c0) ),
    cpdSpeciesNum(cpdInfo_.get_nspec()+1) //include tar
  {
    for( size_t i = 0;i < particleNumArray_.size();i++){
      std::string I = std::to_string(i);
      for( size_t j = 0;j < cpdSpeciesNum;j++){
        std::string J = std::to_string(j);
        cpdArray_.push_back( J + "_" + I );
      }
    }
  }

  //---------------destructor---------------------------
  CPDInterface::~CPDInterface(){
    std::cout << "Delete ~CPDInterface(): " << this << std::endl;
  }

  //---------------------------------------------------------------------------
  void CPDInterface::setup_initial_conditions_dev(){
    const Particles::ParticleEnsembleTags tagsPart( Expr::STATE_N, particleNumArray_ );
    const CPD::CPDTags tagsCPD( Expr::STATE_N, particleNumArray_, cpdArray_ );
    const Coal::CoalEnsembleTags tagsCoal( Expr::STATE_N, particleNumArray_ );
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    std::vector<double> deltai0 = deltai_0( cpdInfo_, c0 );
    deltai0.push_back( tar0 );
    const std::vector<double> defaultgi(cpdSpeciesNum, 0.0);

    const int nparsize = particleNumArray_.size();
    double yvolInit, yvolInflow, l0_frac_init, l0_frac_inflow; // mass fraction l in particle volatile
    std::vector<double> delta_frac_init, delta_frac_inflow; // mass fraction delta in particle volatile
    std::vector<double> gi_init, gi_inflow; // mass of gi per gas mass
    if( rootParser_["Particles"]["CoalInitComp"] ){
      const std::vector<double> coalInitComp = rootParser_["Particles"]["CoalInitComp"].as<std::vector<double> >();
      yvolInit = coalInitComp[1];
    }
    else{ yvolInit = coalcomp_.get_vm(); }

    if(reactorType!="PFR"){
      if( rootParser_["Particles"]["CoalInflowComp"] ){
        const std::vector<double> coalInflowComp = rootParser_["Particles"]["CoalInflowComp"].as<std::vector<double> >();
        yvolInflow = coalInflowComp[1];
      }
      else{ yvolInflow = coalcomp_.get_vm(); }
      l0_frac_inflow = rootParser_["Particles"]["CPDInflow"]["l"].as<double>( cpdInfo_.get_l0_mass() );
      delta_frac_inflow = rootParser_["Particles"]["CPDInflow"]["deltai"].as<std::vector<double>>( deltai0 );
      gi_inflow = rootParser_["Particles"]["CPDInflow"]["gi"].as<std::vector<double>>( defaultgi );
    }

    l0_frac_init = rootParser_["Particles"]["CPDInit"]["l"].as<double>( cpdInfo_.get_l0_mass() );
    delta_frac_init = rootParser_["Particles"]["CPDInit"]["deltai"].as<std::vector<double>>( deltai0 );
    gi_init = rootParser_["Particles"]["CPDInit"]["gi"].as<std::vector<double>>( defaultgi );


    for( size_t i = 0;i < nparsize;i++ ){
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCPD.lTags[i], tagsPart.partMassFracTags[i], yvolInit * l0_frac_init, 0.0 )));
      if(reactorType!="PFR"){ initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCPD.lInflowTags[i], tagsPart.partMassFracInflowTags[i], yvolInflow * l0_frac_inflow, 0.0 )));}

      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCPD.initPartMassFracTags[i], tagsPart.partMassFracTags[i], 1.0, 0.0 )));

      for( size_t j = 0;j < cpdSpeciesNum;j++ ){
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCPD.deltaTags[i*cpdSpeciesNum + j], tagsPart.partMassFracTags[i], yvolInit * delta_frac_init[j], 0.0 )));
        initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsCPD.gTags[i*cpdSpeciesNum + j], gi_init[j] )));
        if(reactorType!="PFR"){
          initRoots_.insert( initFactory_.register_expression( new LinearT( tagsCPD.deltaInflowTags[i*cpdSpeciesNum + j], tagsPart.partMassFracInflowTags[i], yvolInflow * delta_frac_inflow[j], 0.0 )));
          initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsCPD.gInflowTags[i*cpdSpeciesNum + j], gi_inflow[j] )));
        }
      }
    }
  }

  //---------------------------------------------------------------------------
  void CPDInterface::build_equation_system_coal_dev(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_);
    const Dev::DevTags tagsEDev( Expr::STATE_NONE, particleNumArray_, speciesArray_, partNumSpeciesArray_ );
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

    Expr::ExpressionFactory& execFactory = integrator_->factory();
    const int nparsize = particleNumArray_.size();
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    for( size_t i = 0;i < nparsize;i++ ){
      execFactory.register_expression( new PlaceHolderT( tagsECPD.initPartMassFracTags[i] ));
      // kb
      execFactory.register_expression( new kbT( tagsECPD.kbTags[i], tagsEPart.partTempTags[i] ));
      //l_rhs
      execFactory.register_expression( new SourceFromGasMassT( tagsECPD.lKinRhsTags[i], tagsEPart.partToGasMassTag, tagsECPD.lTags[i]));
      execFactory.register_expression( new LRhsT( tagsECPD.lReacTags[i], tagsECPD.kbTags[i], tagsECPD.lTags[i] ));  //negative
      execFactory.attach_dependency_to_expression( tagsECPD.lReacTags[i], tagsECPD.lKinRhsTags[i], Expr::ADD_SOURCE_EXPRESSION);
      if(reactorType != "PFR"){
        execFactory.register_expression( new PlaceHolderT( tagsECPD.lInflowTags[i] ));
        execFactory.register_expression( new ZeroDMixingCPT( tagsECPD.lMixRhsTags[i], tagsECPD.lInflowTags[i], tagsECPD.lTags[i], tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
        execFactory.register_expression( new SumT( tagsECPD.lFullRhsTags[i], { tagsECPD.lKinRhsTags[i], tagsECPD.lMixRhsTags[i] } ));
      }
      else{
        execFactory.register_expression( new DivisionT( tagsECPD.lFullRhsTags[i], tagsECPD.lKinRhsTags[i], tagsEpfr.uTag ));
      }
      //C_rhs
      execFactory.register_expression( new CRhsT( tagsEDev.volCharTags[i], tagsECPD.kbTags[i], tagsECPD.lTags[i], cpdInfo_ )); // positive


      for( size_t j = 0;j < cpdSpeciesNum;j++ ){
        //kg_i
        execFactory.register_expression( new FEfungiT( tagsECPD.fEgTags[i * cpdSpeciesNum + j], tagsECPD.gTags[i * cpdSpeciesNum + j], tagsECPD.initPartMassFracTags[i], cpdInfo_, j ));
        execFactory.register_expression( new EgifunT( tagsECPD.egygTags[i * cpdSpeciesNum + j], tagsECPD.fEgTags[i * cpdSpeciesNum + j], cpdInfo_, j ));
        execFactory.register_expression( new kgiT( tagsECPD.kgTags[i * cpdSpeciesNum + j], tagsECPD.egygTags[i * cpdSpeciesNum + j], tagsEPart.partTempTags[i], cpdInfo_, j ));

        //delta_i
        execFactory.register_expression( new SourceFromGasMassT( tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j], tagsEPart.partToGasMassTag, tagsECPD.deltaTags[i * cpdSpeciesNum + j]));
        execFactory.register_expression( new DeltaiRhsT( tagsECPD.deltaReacTags[i * cpdSpeciesNum + j], tagsECPD.kbTags[i], tagsECPD.kgTags[i * cpdSpeciesNum + j], tagsECPD.deltaTags[i * cpdSpeciesNum + j],
                                                         tagsECPD.lTags[i], cpdInfo_, tarSootInfo_, j ));
        execFactory.attach_dependency_to_expression( tagsECPD.deltaReacTags[i * cpdSpeciesNum + j], tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j], Expr::ADD_SOURCE_EXPRESSION);

        //g_i
        execFactory.register_expression( new SourceFromGasMassT( tagsECPD.gKinRhsTags[i*cpdSpeciesNum+j], tagsEPart.partToGasMassTag, tagsECPD.gTags[i*cpdSpeciesNum+j]));
        execFactory.register_expression( new GiRhsT( tagsECPD.gReacTags[i * cpdSpeciesNum + j], tagsECPD.kbTags[i], tagsECPD.kgTags[i * cpdSpeciesNum + j], tagsECPD.deltaTags[i * cpdSpeciesNum + j],
                                                     tagsECPD.lTags[i], cpdInfo_, tarSootInfo_, j )); //positive
        execFactory.attach_dependency_to_expression( tagsECPD.gReacTags[i * cpdSpeciesNum + j], tagsECPD.gKinRhsTags[i * cpdSpeciesNum + j], Expr::ADD_SOURCE_EXPRESSION);

        if(reactorType != "PFR"){
          execFactory.register_expression( new PlaceHolderT( tagsECPD.deltaInflowTags[i * cpdSpeciesNum + j] ));
          execFactory.register_expression( new PlaceHolderT( tagsECPD.gInflowTags[i * cpdSpeciesNum + j] ));
          execFactory.register_expression( new ZeroDMixingCPT( tagsECPD.deltaMixRhsTags[i * cpdSpeciesNum + j], tagsECPD.deltaInflowTags[i * cpdSpeciesNum + j],tagsECPD.deltaTags[i * cpdSpeciesNum + j],
                                                               tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
          execFactory.register_expression( new SumT( tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j], { tagsECPD.deltaMixRhsTags[i * cpdSpeciesNum + j], tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j] } ));
          execFactory.register_expression( new ZeroDMixingCPT( tagsECPD.gMixRhsTags[i * cpdSpeciesNum + j], tagsECPD.gInflowTags[i * cpdSpeciesNum + j], tagsECPD.gTags[i * cpdSpeciesNum + j],
                                                               tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
          execFactory.register_expression( new SumT( tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j],{ tagsECPD.gMixRhsTags[i * cpdSpeciesNum + j],tagsECPD.gKinRhsTags[i * cpdSpeciesNum + j] } ));
        }
        else{
          execFactory.register_expression( new DivisionT( tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j], tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j], tagsEpfr.uTag ));
          execFactory.register_expression( new DivisionT( tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j], tagsECPD.gKinRhsTags[i * cpdSpeciesNum + j], tagsEpfr.uTag ));
        }
      }
      execFactory.register_expression( new LinearT( tagsEDev.volTarTags[i], tagsECPD.gReacTags[(i+1)*cpdSpeciesNum-1], 1.0, 0.0)); //positive
    }
    //vol_i
    execFactory.register_expression( new dygiT(tagsEDev.volSpeciesGasTags, tagsECPD.gReacTags, nparsize)); // positive
    for( size_t i = 0;i < nparsize;i++ ){
      // volatile mass kinetic rhs
      execFactory.register_expression( new MvRhsT(tagsEDev.volRateTags[i], tagsEDev.volCharTags[i], tagsEDev.volTarTags[i], tagsEDev.volSpeciesGasTags, nparsize, i)); //negative
      execFactory.register_expression( new MvT(tagsECoal.volMassFracTags[i], tagsECPD.lTags[i], tagsECPD.deltaTags, cpdInfo_, i));

      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsECPD.lTags[i].name(), tagsECPD.lFullRhsTags[i], equIndex_, equIndex_ );
      for(size_t j = 0; j<cpdSpeciesNum; j++){
        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsECPD.deltaTags[i*cpdSpeciesNum+j].name(), tagsECPD.deltaFullRhsTags[i*cpdSpeciesNum+j], equIndex_, equIndex_ );
        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsECPD.gTags[i*cpdSpeciesNum+j].name(), tagsECPD.gFullRhsTags[i*cpdSpeciesNum+j], equIndex_, equIndex_ );
      }

    }
  }

  void CPDInterface::initialize_coal_dev(){
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const std::string reactorType = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();

    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      if(!isRestart_){
        integrator_->copy_from_initial_condition_to_execution<FieldT>(tagsECPD.initPartMassFracTags[i].name());
        if(reactorType != "PFR"){ integrator_->copy_from_initial_condition_to_execution<FieldT>(tagsECPD.lInflowTags[i].name());}
      }

      integrator_->lock_field<FieldT>( tagsECPD.initPartMassFracTags[i] );
      if(reactorType != "PFR"){
        integrator_->lock_field<FieldT>( tagsECPD.lInflowTags[i] );
        for(size_t j = 0; j<cpdSpeciesNum; j++){
          if(!isRestart_){
            integrator_->copy_from_initial_condition_to_execution<FieldT>(tagsECPD.deltaInflowTags[i*cpdSpeciesNum+j].name());
            integrator_->copy_from_initial_condition_to_execution<FieldT>(tagsECPD.gInflowTags[i*cpdSpeciesNum+j].name());
          }
          integrator_->lock_field<FieldT>( tagsECPD.deltaInflowTags[i*cpdSpeciesNum+j] );
          integrator_->lock_field<FieldT>( tagsECPD.gInflowTags[i*cpdSpeciesNum+j] );
        }
      }
    }
  }

  void CPDInterface::modify_prim_kinrhs_tags_dev()
  {
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primitiveTags_.push_back( tagsECPD.lTags[i]);
      if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>() != "PFR"){
        kinRhsTags_.push_back( tagsECPD.lKinRhsTags[i]);
      }
      else{
        kinRhsTags_.push_back( tagsECPD.lFullRhsTags[i]);
      }

      for(size_t j = 0; j<cpdSpeciesNum; j++){
        primitiveTags_.push_back( tagsECPD.deltaTags[i*cpdSpeciesNum+j]);
        primitiveTags_.push_back( tagsECPD.gTags[i*cpdSpeciesNum+j]);
        if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>() != "PFR"){
          kinRhsTags_.push_back( tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j] );
          kinRhsTags_.push_back( tagsECPD.gKinRhsTags[i * cpdSpeciesNum + j] );
        }
        else{
          kinRhsTags_.push_back( tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j] );
          kinRhsTags_.push_back( tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j] );
        }
      }
    }
  }

  void CPDInterface::modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                                        const std::map<Expr::Tag, int> &consVarIdxMap,
                                        std::map<Expr::Tag, int> &kinRhsIdxMap,
                                        const  std::map<Expr::Tag, int> &rhsIdxMap)
  {
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const int nparsize = particleNumArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){
      primVarIdxMap[tagsECPD.lTags[i]] = consVarIdxMap.at( tagsECPD.lTags[i]);
      if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>() != "PFR"){
        kinRhsIdxMap[tagsECPD.lKinRhsTags[i]] = rhsIdxMap.at( tagsECPD.lFullRhsTags[i] );
      }
      else{
        kinRhsIdxMap[tagsECPD.lFullRhsTags[i]] = rhsIdxMap.at( tagsECPD.lFullRhsTags[i] );
      }
      for(size_t j = 0; j<cpdSpeciesNum; j++){
        primVarIdxMap[tagsECPD.deltaTags[i*cpdSpeciesNum+j]] = consVarIdxMap.at( tagsECPD.deltaTags[i*cpdSpeciesNum+j]);
        primVarIdxMap[tagsECPD.gTags[i*cpdSpeciesNum+j]] = consVarIdxMap.at( tagsECPD.gTags[i*cpdSpeciesNum+j]);
        if(rootParser_["ReactorParameters"]["ReactorType"].as<std::string>() != "PFR"){
          kinRhsIdxMap[tagsECPD.deltaKinRhsTags[i * cpdSpeciesNum + j]] = rhsIdxMap.at( tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j] );
          kinRhsIdxMap[tagsECPD.gKinRhsTags[i * cpdSpeciesNum + j]] = rhsIdxMap.at( tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j] );
        }
        else{
          kinRhsIdxMap[tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j]] = rhsIdxMap.at( tagsECPD.deltaFullRhsTags[i * cpdSpeciesNum + j] );
          kinRhsIdxMap[tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j]] = rhsIdxMap.at( tagsECPD.gFullRhsTags[i * cpdSpeciesNum + j] );
        }
      }
    }
  }

  void CPDInterface::dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                    std::map<Expr::Tag, int> primVarIdxMap,
                                    std::map<Expr::Tag, int> consVarIdxMap)
  {
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const int nparsize = particleNumArray_.size();

    for( size_t i = 0;i < nparsize;i++ ){
      dVdUPart->element<double>(primVarIdxMap.at(tagsECPD.lTags[i]), consVarIdxMap.at( tagsECPD.lTags[i])) = 1.0;
      for(size_t j = 0; j<cpdSpeciesNum; j++){
        dVdUPart->element<double>(primVarIdxMap.at(tagsECPD.deltaTags[i*cpdSpeciesNum+j]), consVarIdxMap.at( tagsECPD.deltaTags[i*cpdSpeciesNum+j])) = 1.0;
        dVdUPart->element<double>(primVarIdxMap.at(tagsECPD.gTags[i*cpdSpeciesNum+j]), consVarIdxMap.at( tagsECPD.gTags[i*cpdSpeciesNum+j])) = 1.0;
      }
    }
  }

  void CPDInterface::dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                          std::map<Expr::Tag, int> primVarIdxMap,
                                          std::map<Expr::Tag, int> rhsIdxMap,
                                          std::map<Expr::Tag, Expr::Tag> consVarRhsMap)
  {
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const int nparsize = particleNumArray_.size();
    const Expr::Tag&     rho        = tagsE.rhoTag;
    const Expr::TagList& ell        = tagsECPD.lTags;
    const Expr::TagList& ellMix     = tagsECPD.lMixRhsTags;
    const Expr::TagList& g          = tagsECPD.gTags;
    const Expr::TagList& gMix       = tagsECPD.gMixRhsTags;
    const Expr::TagList& delta      = tagsECPD.deltaTags;
    const Expr::TagList& deltaMix   = tagsECPD.deltaMixRhsTags;
    const Expr::TagList& tempPart   = tagsEPart.partTempTags;
    const Expr::TagList& mixPartTemp= tagsEPart.partTempMixRhsTags;

    for( size_t i = 0;i < nparsize;i++ ){
      const auto& ellRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsECPD.lTags[i] ) );
      const auto& ellIdxi    = primVarIdxMap.at( tagsECPD.lTags[i] );
      const auto& tempPartRhsIdxi = rhsIdxMap.at( consVarRhsMap.at( tagsEPart.partTempTags[i] ) );
      const auto& tempPartIdxi    = primVarIdxMap.at( tagsEPart.partTempTags[i] );

      dmMixingdVPart->element<FieldT>( ellRhsIdxi, primVarIdxMap.at(rho) ) = sensitivity(ellMix[i], rho);
      dmMixingdVPart->element<FieldT>( ellRhsIdxi, ellIdxi ) = sensitivity(ellMix[i], ell[i]);

      dmMixingdVPart->element<FieldT>( tempPartIdxi, ellIdxi ) = sensitivity(mixPartTemp[i], ell[i]);

      for(size_t j = 0; j<cpdSpeciesNum; j++){
        const auto& gRhsIdxj     = rhsIdxMap.at( consVarRhsMap.at( tagsECPD.gTags[i*cpdSpeciesNum+j] ) );
        const auto& gIdxj        = primVarIdxMap.at( tagsECPD.gTags[i*cpdSpeciesNum+j] );
        const auto& deltaRhsIdxj = rhsIdxMap.at( consVarRhsMap.at( tagsECPD.deltaTags[i*cpdSpeciesNum+j] ) );
        const auto& deltaIdxj    = primVarIdxMap.at( tagsECPD.deltaTags[i*cpdSpeciesNum+j] );
        dmMixingdVPart->element<FieldT>( gRhsIdxj, primVarIdxMap.at(rho) ) = sensitivity(gMix[i*cpdSpeciesNum+j], rho);
        dmMixingdVPart->element<FieldT>( gRhsIdxj, gIdxj ) = sensitivity(gMix[i*cpdSpeciesNum+j], g[i*cpdSpeciesNum+j]);
        dmMixingdVPart->element<FieldT>( deltaRhsIdxj, primVarIdxMap.at(rho) ) = sensitivity(deltaMix[i*cpdSpeciesNum+j], rho);
        dmMixingdVPart->element<FieldT>( deltaRhsIdxj, deltaIdxj ) = sensitivity(deltaMix[i*cpdSpeciesNum+j], delta[i*cpdSpeciesNum+j]);

        dmMixingdVPart->element<FieldT>( tempPartIdxi, deltaIdxj ) = sensitivity(mixPartTemp[i], delta[i*cpdSpeciesNum+j]);
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------
  void CPDInterface::restart_var_coal_dev(std::vector<std::string>& restartVars){
    const CPD::CPDTags tagsECPD( Expr::STATE_NONE, particleNumArray_, cpdArray_ );
    for( size_t i=0; i<particleNumArray_.size(); ++i){
      restartVars.push_back(tagsECPD.initPartMassFracTags[i].name());
    }
  }
} // namespace CPD
