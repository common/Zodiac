
set( src
        CPDData.cpp
        CPDInterface.cpp
        )

# advertise this upward, appending the path information...
get_filename_component( dir ${CMAKE_CURRENT_LIST_FILE} PATH )
foreach( file ${src} )
    list( APPEND SRCS ${dir}/${file} )
endforeach()
set( PARSER_SRC ${PARSER_SRC} ${SRCS} PARENT_SCOPE )