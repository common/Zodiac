#ifndef Gi_RHS_Expr_h
#define Gi_RHS_Expr_h
/**
 * @file Gi_RHS.h
 * @par
 * @author Hang Zhou
 */
#include <expression/Expression.h>
#include "CPDData.h"
#include "c0_fun.h"

namespace CPD{

/**
 *  \ingroup CPD
 *  \class Gi_RHS
 *  \brief Getting reaction rates of the 16 g (including 15 light gas species and tar). (unit: 1/s)
 *	       So this expresion will return a vector.
 *
 *  In this Expression the reaction rate in below being evaluated :
 *
 *  \f[ \frac{dg_{i}}{dt}=\left[\frac{2 k_{b}\ell}{\rho+1}\right]\frac{Mw_g}{Mw_l}\frac{fg_{i}}{\sum_{j}^{16}fg_{j}}+k_{gi}\delta_{i} \f]
 *
 *  where
 *  - \f$ k_{b} \f$ : reaction constant of labile bridge
 *  - \f$\ell\f$ : labile bridge
 *  - \f$\rho = 0.9\f$
 *  - \f$fg_{i}\f$ = functional group for each bond.
 *  - \f$\delta_{i}\f$ : amount of side chain i
 *  - \f$g_{i}\f$ : amount of gas produced from side chains and labile bridge.
 *  - \f$Mw_g\f$ : molecular weight of mixture of all 17 \f$g_i\f$ (including all light gas speices and tar)
 *  - \f$Mw_l\f$ : molecular weight of labile bridge
 *
 * In this expression, \f$\delta_{i}\f$ and \f$g_{i}\f$ is a vector of FieldT.
 *
 * In the first term of the equation, \f$Mw_g\f$ is the mixture molecular weight, which is a little hard to get.
 * Instead of getting it directly, we could use \f$ \frac{d \ell}{dt} - \frac{dC}{dt} \f$
 * in that path \f$ \ell \rightarrow \ell^* \rightarrow c+2g \f$, which gives the equation for \f$ \frac{dg_{i}}{dt} \f$:
 * \f[
 *   \frac{dg_{i}}{dt}=\left[\frac{k_{b}\ell}{\rho+1}-\frac{k_{b}\ell}{\rho+1}\frac{Mw_C}{Mw_l} \right]\frac{fg_{i}}{\sum_{j}^{17}fg_{j}}+k_{gi}\delta_{i}
 *                    = \frac{k_{b}\ell}{\rho+1}\left[1-\frac{Mw_C}{Mw_l}\right]\frac{fg_{i}}{\sum_{j}^{17}fg_{j}}+k_{gi}\delta_{i}
 * \f]
 *  \f$ Mw_C \f$ is the molecular weight of carbon.
 *
 * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
 *          All the parameters used here are the value for ONE specific particle size.
 *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */
  template <typename FieldT>
  class Gi_RHS: public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, kb_, kgi_, deltai_, l_ )
    const CPDInformation& cpd_;
    const TarAndSootInfo& tarSootInfo_;
    const int index_;
    const double sumfg_, mwl0_, rho_;
    double fgi_, mwfg_;

    Gi_RHS( const Expr::Tag& kbTag,
            const Expr::Tag& kgiTag,
            const Expr::Tag& deltaiTag,
            const Expr::Tag& lTag,
            const CPDInformation& cpd,
            const TarAndSootInfo& tarSootInfo,
            const int index)
          : Expr::Expression<FieldT>(),
            cpd_( cpd ),
            tarSootInfo_( tarSootInfo ),
            index_(index),
            sumfg_(cpd.get_sumfg()),
            mwl0_(cpd.l0_molecular_weight()),
            rho_(0.9)
    {
      this->set_gpu_runnable(true);
      kb_         = this->template create_field_request<FieldT>( kbTag );
      kgi_        = this->template create_field_request<FieldT>( kgiTag );
      deltai_     = this->template create_field_request<FieldT>( deltaiTag );
      l_          = this->template create_field_request<FieldT>( lTag  );

      if( index_ == cpd_.get_fgi().size()){
        fgi_ = cpd_.get_tarMassFrac();
        mwfg_ = tarSootInfo_.tar_mw();
      }
      else{
        mwfg_ = cpd_.get_mwVec()[index_];
        fgi_ = cpd_.get_fgi()[index_];
      }
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag kbTag_, kgiTag_, deltaiTag_, lTag_;
      const CPDInformation& cpd_;
      const TarAndSootInfo& tarSootInfo_;
      const int index_;
    public:
      /**
       *  \brief The mechanism for building a Gi_RHS object
       *  \tparam FieldT
       *  \param rhsTag production rate of \f$g_i\f$
       *  \param kbTag : reaction constant of labile bridge (FieldT)
       *  \param kgiTag : reaction constat of gas (vecotr of FieldT)
       *  \param deltaiTag : mass of delta_i per gas mass
       *  \param lTag  : mass of laible bridges per gas mass
       *  \param cpd : the CPDInformation object.  This must have a lifetime at least as long as this expression.
       *  \param tarSootInfo : TarAndSootInfo
       *  \param index : index of species in list of \f$g_i\f$
       */
      Builder( const Expr::Tag& rhsTag,
               const Expr::Tag& kbTag,
               const Expr::Tag& kgiTag,
               const Expr::Tag& deltaiTag,
               const Expr::Tag& lTag,
               const CPDInformation& cpd,
               const TarAndSootInfo& tarSootInfo,
               const int index)
            : ExpressionBuilder(rhsTag),
              kbTag_	        ( kbTag        ),
              kgiTag_         ( kgiTag       ),
              deltaiTag_      ( deltaiTag    ),
              lTag_           ( lTag         ),
              cpd_            ( cpd          ),
              tarSootInfo_    ( tarSootInfo  ),
              index_          (index         ){}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new Gi_RHS<FieldT>( kbTag_ ,kgiTag_, deltaiTag_, lTag_, cpd_, tarSootInfo_, index_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();

      const FieldT& kb         = kb_->field_ref();
      const FieldT& kgi        = kgi_->field_ref();
      const FieldT& deltai     = deltai_->field_ref();
      const FieldT& ell        = l_ ->field_ref();

      result <<= cond(fgi_ == 0.0 or (ell<=0.0 and deltai<=0.0), 0.0)
            (ell<=0.0 and deltai>0.0, kgi * deltai)
            (ell>0.0 and deltai<=0.0, kb * ell / (rho_ + 1.0) * (1-12.0/mwl0_) * fgi_ / sumfg_)
            ( kb * ell / (rho_ + 1.0) * (1-12.0/mwl0_) * fgi_ / sumfg_+ kgi * deltai);
    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );

      const FieldT& kb        = kb_->field_ref();
      const FieldT& kgi       = kgi_->field_ref();
      const FieldT& deltai    = deltai_->field_ref();
      const FieldT& ell       = l_ ->field_ref();
      const FieldT& dkbdv     = kb_->sens_field_ref( var );
      const FieldT& dkgidv    = kgi_->sens_field_ref( var );
      const FieldT& ddeltaidv = deltai_->sens_field_ref( var );
      const FieldT& dldv      = l_ ->sens_field_ref( var );

      dfdv <<= cond(fgi_ == 0.0 or (ell<=0.0 and deltai<=0.0), 0.0)
            (ell<=0.0 and deltai>0.0,  (dkgidv * deltai+kgi*ddeltaidv))
            (ell>0.0 and deltai<=0.0, (dkbdv * ell + kb*dldv) / (rho_ + 1.0) * (1-12.0/mwl0_) * fgi_ / sumfg_)
            (              (dkbdv * ell + kb*dldv) / (rho_ + 1.0) * (1-12.0/mwl0_) * fgi_ / sumfg_+  (dkgidv * deltai+kgi*ddeltaidv));

    }
  };


} // namespace CPD

#endif // Gi_RHS_Expr_h
