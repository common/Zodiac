#ifndef Deltai_RHS_Expr_h
#define Deltai_RHS_Expr_h

/**
 * @file Deltai_RHS.h
 * @par Getting reaction rates of the 16 delta (including 15 light gas species and tar).
 * @author Hang Zhou
 */

#include <expression/Expression.h>
#include "CPDData.h"
#include <vector>

namespace CPD{

  /**
   *  \ingroup CPD
   *  \class Deltai_RHS
   *
   *  \brief Getting reaction rates of the 16 delta (including 15 light gas species and tar). (unit: 1/s)
   *	       So this expresion will return a vector.
   *
   *  In this Expression the reaction rate in below being evaluated :
   *  \f[
   *   \frac{d\delta_{i}}{dt}=\left[\frac{\rho k_{b}\ell}{\rho+1}\right]\frac{fg_{i}}{\sum_{j=1}^{16}fg_{j}}-k_{g_i}\delta_{i}
   *  \f]
   *  where
   *  - \f$ k_{b} \f$ : reaction constant of labile bridge
   *  - \f$ k_{g_i} \f$ : reaction constant from \f$\delta_i\f$ to \f$g_i\f$.
   *  - \f$ \ell \f$ : labile bridge
   *  - \f$ \rho = 0.9 \f$
   *  - \f$ fg_{i} \f$ = functional group for each bond.
   *  - \f$ \delta_{i} \f$ : amount of side chain i
   * In this expression, \f$ \delta_{i} \f$ is a vector of FieldT.
   *
   * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
   *          All the parameters used here are the value for ONE specific particle size.
   *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
   */
  template<typename FieldT>
  class Deltai_RHS
        : public Expr::Expression<FieldT>
  {
    const CPDInformation& cpd_;
    const int index_;
    const double sumfg_, mwl0_, rho_, tarFrac0_;
    double fgi_, mwfg_;
    const TarAndSootInfo& tarSootInfo_;

    DECLARE_FIELDS( FieldT, kb_, kgi_, deltai_, l_ )

    Deltai_RHS( const Expr::Tag& kbTag,
                const Expr::Tag& kgiTag,
                const Expr::Tag& deltaiTag,
                const Expr::Tag& lTag,
                const CPDInformation& cpd,
                const TarAndSootInfo& tarSootInfo,
                const int index)
          : Expr::Expression<FieldT>(),
            cpd_( cpd ),
            tarSootInfo_( tarSootInfo ),
            index_(index),
            sumfg_(cpd.get_sumfg()),
            mwl0_(cpd.l0_molecular_weight()),
            tarFrac0_(cpd.get_tarMassFrac()),
            rho_(0.9)
    {
      this->set_gpu_runnable(true);
      kb_         = this->template create_field_request<FieldT>( kbTag  );
      kgi_        = this->template create_field_request<FieldT>( kgiTag );
      deltai_     = this->template create_field_request<FieldT>( deltaiTag );
      l_          = this->template create_field_request<FieldT>( lTag  );

      if( index_ == cpd_.get_fgi().size()){
        fgi_ = tarFrac0_;
        mwfg_ = tarSootInfo_.tar_mw();
      }
      else{
        mwfg_ = cpd_.get_mwVec()[index_];
        fgi_ = cpd_.get_fgi()[index_];
      }
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag kbTag_, kgiTag_, deltaiTag_, lTag_;
      const CPDInformation& cpd_;
      const TarAndSootInfo& tarSootInfo_;
      const int index_;
    public:
      /**
       * @brief The mechanism for building a Deltai_RHS object
       * @param rhsTag production rate of 15 lignt gas species and tar
       * @param kbTag reaction constant of labile bridge
       * @param kgiTag reaction constant from \f$\delta_i\f$ to \f$g_i\f$.
       * @param deltaiTag amount of side chain i
       * @param lTag labile bridge
       * @param cpd CPDInformation
       * @param tarSootInfo  TarAndSootInfo
       * @param index index of species in g_i list
       */
      Builder( const Expr::Tag& rhsTag,
               const Expr::Tag& kbTag,
               const Expr::Tag& kgiTag,
               const Expr::Tag& deltaiTag,
               const Expr::Tag& lTag,
               const CPDInformation& cpd,
               const TarAndSootInfo& tarSootInfo,
               const int index )
            : ExpressionBuilder(rhsTag),
              kbTag_         ( kbTag         ),
              kgiTag_        ( kgiTag        ),
              deltaiTag_     ( deltaiTag     ),
              lTag_          ( lTag          ),
              cpd_           ( cpd           ),
              tarSootInfo_   ( tarSootInfo   ),
              index_         ( index         ){}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new Deltai_RHS<FieldT>( kbTag_ ,kgiTag_, deltaiTag_, lTag_, cpd_, tarSootInfo_, index_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();

      const FieldT& kb         = kb_        ->field_ref();
      const FieldT& kgi        = kgi_       ->field_ref();
      const FieldT& deltai     = deltai_    ->field_ref();
      const FieldT& ell        = l_         ->field_ref();

      result <<= cond(fgi_ == 0.0 or (ell<=0.0 and deltai<=0.0), 0.0)
            (ell<=0.0 and deltai>0.0, - kgi * deltai)
            (ell>0.0 and deltai<=0.0, rho_ * kb * ell / (rho_ + 1.0)  * fgi_ / sumfg_)
            (              rho_ * kb * ell / (rho_ + 1.0)  * fgi_ / sumfg_ - kgi * deltai);

    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );

      const FieldT& kb = kb_->field_ref();
      const FieldT& kgi = kgi_->field_ref();
      const FieldT& deltai = deltai_->field_ref();
      const FieldT& ell = l_->field_ref();
      const FieldT& dkbdv = kb_->sens_field_ref( var );
      const FieldT& dkgidv = kgi_->sens_field_ref( var );
      const FieldT& ddeltaidv = deltai_->sens_field_ref( var );
      const FieldT& dldv = l_->sens_field_ref( var );

      dfdv <<= cond(fgi_ == 0.0 or (ell<=0.0 and deltai<=0.0), 0.0)
            (ell<=0.0 and deltai>0.0, - (dkgidv * deltai+kgi*ddeltaidv))
            (ell>0.0 and deltai<=0.0, rho_ *(dkbdv * ell + kb*dldv) / (rho_ + 1.0)  * fgi_ / sumfg_)
            (              rho_ *(dkbdv * ell + kb*dldv) / (rho_ + 1.0)  * fgi_ / sumfg_ - (dkgidv * deltai+kgi*ddeltaidv));
    }
  };
  //--------------------------------------------------------------------

} // namespace CPD

#endif // Deltai_RHS_Expr_h
