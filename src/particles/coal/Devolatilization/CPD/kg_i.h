#ifndef kg_i_Expr_h
#define kg_i_Expr_h
/**
 * @file kg_i.h
 * @par Calculating the reaction constant for each g_i
 * @author Hang Zhou
 */
#include <expression/Expression.h>

#include "CPDData.h"
#include "c0_fun.h"

namespace CPD{

/**
 *  \ingroup CPD
 *  \class kg_i
 *  \brief Calculating the reaction constant for each g_i
 *
 *  \f[
 *     kg_{i}=A_{i}exp\left(\frac{-E_{i}}{RT}\right)
 *  \f]
 *  where:
 *  \f$A_{i}\f$ is a constant obtained from CPDInformation class and
 *  \f$E_{i}\f$ is change with time and being calculated by Egi_fun().
 *
 */

  template <typename FieldT>
  class kg_i: public Expr::Expression<FieldT>
  {
    const std::vector<double>& A0_;
    const CPDInformation& cpd_;
    const std::vector<double>& fg_;
    const double c0_, tarMassFrac_;
    const int index_;
    double fgi_;

    DECLARE_FIELDS( FieldT, egi_, tempP_)

    kg_i( const Expr::Tag& egiTag,
          const Expr::Tag& tempPTag,
          const CPDInformation& cpd,
          const int index)
          : Expr::Expression<FieldT>(),
            cpd_ ( cpd ),
            index_(index),
            A0_( cpd.get_A0() ),
            fg_(cpd.get_fgi()),
            c0_(c0_fun( cpd.get_coal_composition().get_C(), cpd.get_coal_composition().get_O())),
            tarMassFrac_(cpd.get_tarMassFrac())
    {
      this->set_gpu_runnable(true);
      tempP_ = this->template create_field_request<FieldT>( tempPTag );
      egi_ = this->template create_field_request<FieldT>( egiTag );

      if( index_ == fg_.size()){
        fgi_ = tarMassFrac_;
      }
      else{
        fgi_ = fg_[index_];
      }
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag tempPTag_, egiTag_;
        const CPDInformation& cpd_;
        const int index_;
    public:
      /**
       * \brief The mechanism for building a kg_i object
       *  \param kgiTags    kgi
       *  \param egiTags    activation energy/R for gi
       *  \param tempPTag   Particle Temperature K
       *  \param cpd        the CPDInformation
       *  \param index      index of species in the list of gi
       */
      Builder( const Expr::Tag& kgiTag,
               const Expr::Tag& egiTag,
               const Expr::Tag& tempPTag,
               const CPDInformation& cpd,
               const int index)
            : ExpressionBuilder(kgiTag),
              egiTag_  ( egiTag   ),
              tempPTag_( tempPTag ),
              cpd_     ( cpd      ),
              index_   ( index    ){}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new kg_i<FieldT>( egiTag_, tempPTag_, cpd_, index_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      const FieldT& tempP = tempP_->field_ref();
      const FieldT& egi   = egi_->field_ref();

      this->value() <<= cond(fgi_ == 0.0 || c0_ == 1.0, 0.0)
                            (                           A0_[index_] * exp( - egi / tempP));
    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result(var);

      const FieldT& tempP    = tempP_->field_ref();
      const FieldT& dtempPdv = tempP_->sens_field_ref(var);
      const FieldT& egi       = egi_->field_ref();
      const FieldT& degidv    = egi_->sens_field_ref(var);
      dfdv <<= cond(fgi_ == 0.0 || c0_ == 1.0, 0.0)
                   (   A0_[index_] * exp(- egi / tempP ) * ( - degidv /tempP + egi / (tempP * tempP) * dtempPdv));
    }
  };

} // namespace CPD

#endif // kg_i_Expr_h
