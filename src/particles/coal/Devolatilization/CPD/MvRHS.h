#ifndef MvRHS_Expr_h
#define MvRHS_Expr_h
/**
 * @file MvRHS.h
 * @par Getting the reaction rate of volatile mass per gas mass.
 * @author Hang Zhou
 */
#include <expression/Expression.h>
#include "CPDData.h"


namespace CPD{

  /**
   *  @class MvRHS
   *  @ingroup MvRHS
   *  @brief Getting the reaction rate of volatile mass per gas mass.
   *
   *  The expression is given as
   *  \f[
   *    \frac{dm_v}{dt} = \frac{dm_C}{dt} + \frac{dm_{tar}}{dt} + \sum_{i=1}^{nspecies} \frac{dm_{y_i}}{dt}
   *  \f]
   *  Here, \f$\frac{dm_v}{dt}\f$ is the reaction rate of volatile mass per gas mass.
   *        \f$\frac{dm_C}{dt}\f$ is the production rate of carbon mass per gas mass from CPD.
   *        \f$\frac{dm_{tar}}{dt} \f$ is the production rate of tar mass per gas mass from CPD.
   *        \f$\frac{dm_{y_i}}{dt}\f$ is the production rate of gas species mass per gas mass from CPD.
   *        It includes CO2, H2, CO2, H2O, CH4, HCN and NH3.
   *        \f$nspecies=7\f$ is the number of species.
   *
   * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
   *          All the parameters used here are the value for ONE specific particle size.
   *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
   */
  template< typename FieldT >
  class MvRHS: public Expr::Expression<FieldT>
  {
    const int partIndex_, nparsize_, nspecies_;
    DECLARE_FIELDS( FieldT, cRHS_, tarRHS_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, dyiRHS_ )

    MvRHS( const Expr::Tag &cRHSTag,
           const Expr::Tag &tarRHSTag,
           const Expr::TagList &dyirhstag,
           const int nparsize,
           const int partIndex)
          : Expr::Expression<FieldT>(),
            nparsize_(nparsize),
            partIndex_(partIndex),
            nspecies_(dyirhstag.size()/nparsize)
    {
      this->set_gpu_runnable(true);
      cRHS_   = this->template create_field_request<FieldT>( cRHSTag   );
      tarRHS_ = this->template create_field_request<FieldT>( tarRHSTag   );
      this->template create_field_vector_request<FieldT>( dyirhstag, dyiRHS_ );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag cRHSTag_, tarRHSTag_;
        const Expr::TagList dyirhsTag_;
        const int partIndex_, nparsize_;
    public:
      /**
       * @brief The mechanism for building a MvRHS object
       * @tparam FieldT
       * @param mvrhsTag reaction rate of volatile mass per gas mass
       * @param cRHSTag production rate of carbon mass per gas mass from CPD
       * @param tarRHSTag production rate of tar mass per gas mass from CPD
       * @param dyirhstag production rate of gas species mass per gas mass from CPD. It includes CO2, H2, CO2, H2O, CH4, HCN and NH3.
       * @param nparsize number of particle sizes.
       * @param partIndex index of particle in the list of number of particle sizes,
       */
      Builder( const Expr::Tag &mvrhsTag,
               const Expr::Tag &cRHSTag,
               const Expr::Tag &tarRHSTag,
               const Expr::TagList &dyirhstag,
               const int nparsize,
               const int partIndex)
            : ExpressionBuilder(mvrhsTag),
              cRHSTag_     ( cRHSTag   ),
              tarRHSTag_   ( tarRHSTag ),
              dyirhsTag_   ( dyirhstag ),
              nparsize_    ( nparsize  ),
              partIndex_   ( partIndex ){}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new MvRHS<FieldT>( cRHSTag_, tarRHSTag_, dyirhsTag_, nparsize_, partIndex_);
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();

      result <<= - cRHS_->field_ref() - tarRHS_->field_ref();
      for( size_t i=0; i<nspecies_; ++i ){
        result <<= result - dyiRHS_[partIndex_*nspecies_ + i]->field_ref();
      }
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result(var);

      dfdv <<= - cRHS_->sens_field_ref(var) - tarRHS_->sens_field_ref(var);
      for( size_t i=0; i<nspecies_; ++i ){
        dfdv <<= dfdv - dyiRHS_[partIndex_*nspecies_ + i]->sens_field_ref(var);
      }
    }
  };

} // namespace CPD

#endif // MvRHS_Expr_h
