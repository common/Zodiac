
/**
 * @file E_fun.h
 * @par Getting the activation energy of reaction for CPD model
 * @author Hang Zhou
 */

#ifndef E_fun_h
#define E_fun_h

#include <spatialops/Nebo.h>
#include <expression/Expression.h>

namespace CPD{

  /**
   * @class Eb_fun
   * @ingroup CPD
   * @brief Getting the activation energy : E_b
   *
   *   Reaction Constant for labile bridge reaction
   *  \f[ k_{b}=A_{0} \exp{\left(-\frac{E}{RT}\right)} \f]
   *  where E is the activation energy of reaction and calculated by the following equation :
   *  \f[
   *     F(E) = \frac{1}{\sqrt{2\pi}\sigma}\intop_{-\infty}^{E}exp\left\{ -\frac{1}{2}\left(\frac{E-E_{0}}{\sigma}\right)\right\} dE
   *          = \frac{1}{2}\left(1+erf(\frac{E-E_0}{\sqrt{2}\sigma})\right)
   *  \f]
   *  \f[
   *     E_b = E
   *  \f]
   *  It has been divied by R.
   *
   *  Note: the sensitivity of error function is:
   *  \f[
   *    \frac{d}{d x} erf^{-1}(x) = \frac{\sqrt{\pi}}{2}\exp{(efr^{-1}(x))^2}
   * \f]
   */
  template<typename FieldT>
  class Eb_fun : public Expr::Expression<FieldT> {
    DECLARE_FIELD( FieldT, fEb_)
    const double sigma_;

    Eb_fun( const Expr::Tag fEbTag )
          : Expr::Expression<FieldT>(),
           sigma_(1800){
      this->set_gpu_runnable( true );
      fEb_ = this->template create_field_request<FieldT>( fEbTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag fEbTag_;
    public:
      /**
       * @brief The mechanism for building a Eb_fun object
       * @param activeEgybTag activation energy. It has been divied by R.
       * @param fEbTag F(E) function: \f$1-\frac{l}{\ell_{0}}\f$
       */
      Builder( const Expr::Tag activeEgybTag,
               const Expr::Tag fEbTag )
            : Expr::ExpressionBuilder( activeEgybTag ),
              fEbTag_                ( fEbTag        ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new Eb_fun<FieldT>( fEbTag_);
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      const double gascon = 1.985;
      const double Eb0 = 55.43E3;
      this->value() <<= Eb0/gascon - sqrt(2) * sigma_ * inv_erf( 1.0 - 2.0 * max( min( fEb_->field_ref(), 0.999 ), 1.0e-3 ) );
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );
      const FieldT& fEb = fEb_->field_ref();
      const FieldT& dfEbdv = fEb_->sens_field_ref( var );

      dfdv <<= cond( fEb > 1.0e-3 && fEb < 0.999, sqrt(M_PI*2) * sigma_ * exp(square(inv_erf(1.0 - 2.0 * fEb))) * dfEbdv)
                   (0.0);
    }
  };

  /**
   * @class Egi_fun
   * @ingroup CPD
   * @brief Getting the activation energy : Egi/R
   *
   *   Reaction Constant for labile bridge reaction
   *  \f[ k_{g_i}=A_{g_i} \exp{\left(-\frac{E}{RT}\right)} \f]
   *  where E is the activation energy of reaction and calculated by the following equation :
   *  \f[
   *     F(E) = \frac{1}{\sqrt{2\pi}\sigma}\intop_{-\infty}^{E}exp\left\{ -\frac{1}{2}\left(\frac{E-E_{0}}{\sigma}\right)\right\} dE
   *          = \frac{1}{2}\left(1+erf(\frac{E-E_0}{\sqrt{2}\sigma})\right)
   *  \f]
   *  \f[
   *     E_{g_i} = E
   *  \f]
   *  It has been divied by R.
   */
  template<typename FieldT>
  class Egi_fun : public Expr::Expression<FieldT> {
    std::vector<double> e0_, sigma_;
    const CPDInformation& cpd_;
    const int index_;
    DECLARE_FIELDS( FieldT, fEgi_)

    Egi_fun( const Expr::Tag& fEgiTag,
             const CPDInformation& cpd,
             const int index)
          : Expr::Expression<FieldT>(),
            cpd_( cpd ),
            index_( index ),
            e0_( cpd.get_E0() ),
            sigma_( cpd.get_sigma() ){
      this->set_gpu_runnable( true );
      fEgi_ = this->template create_field_request<FieldT>( fEgiTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag fEgiTag_;
      const CPDInformation& cpd_;
      const int index_;
    public:
      /**
       * @brief The mechanism for building a Egi_fun object
       * @param activeEgygiTag activation energy for \f$g_i\f$. It has been divied by R.
       * @param fEgiTag F(E) function
       * @param cpd CPDInformation
       * @param index index of species i in list \f$g_i\f$.
       */
      Builder( const Expr::Tag& activeEgygiTag,
               const Expr::Tag& fEgiTag,
               const CPDInformation& cpd,
               const int index)
            : Expr::ExpressionBuilder( activeEgygiTag ),
              fEgiTag_               ( fEgiTag         ),
              cpd_                   ( cpd             ),
              index_                 ( index           ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new Egi_fun<FieldT>( fEgiTag_, cpd_, index_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      this->value() <<= e0_[index_] - sqrt(2) * sigma_[index_] * inv_erf( 1.0 - 2.0 * max( min( fEgi_->field_ref(), 0.999 ), 1.0e-3 ) );
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );
      const FieldT& fEgi = fEgi_->field_ref();
      const FieldT& dfEgidv = fEgi_->sens_field_ref( var );

      dfdv <<= cond( fEgi > 1.0e-3 && fEgi < 0.999, sqrt(M_PI*2) * sigma_[index_] * exp(square(inv_erf(1.0 - 2.0 * fEgi))) * dfEgidv)
                   (0.0);

    }
  };

}  // namespace CPD

#endif // E_fun_h
