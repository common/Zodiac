
/**
 * @file dy_gi.h
 * @par This expression getting the rate of production of each species, including CO2, CO, H2, H2O, HCN, NH3, CH4,
 *         from CPD model by adding the results of extra loose, loose and tight for each species.
 * @author Hang Zhou
 */

#ifndef dy_gi_h
#define dy_gi_h

#include <expression/Expression.h>

namespace CPD{

/**
 *  @ingroup CPD
 *  @class dy_gi
 *  @brief This expression getting the rate of production of each species, including CO2, CO, H2, H2O, HCN, NH3 and CH4,
 *         from CPD model by adding the results of extra loose, loose and tight for each species.
 *
 */
  template <typename FieldT>
  class dy_gi
    : public Expr::Expression<FieldT>
  {
    typedef std::vector<int> VecI;
    const SpeciesSum& specSum_;
    DECLARE_VECTOR_OF_FIELDS( FieldT, gi_ )
    const int nparsize_, specnum_;
    const SpecContributeVec& spSum_;

    dy_gi( const Expr::TagList& giRHStag,
           const int nparsize)
          : Expr::Expression<FieldT>(),
            specSum_( SpeciesSum::self() ),
            nparsize_( nparsize),
            specnum_( giRHStag.size()/nparsize),
            spSum_(SpeciesSum::self().get_vec_comp())
    {
      this->template create_field_vector_request<FieldT>( giRHStag, gi_ );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::TagList giRHStag_;
        const int nparsize_;
    public:
      /**
       * @brief The mechanism for building a dy_gi object
       * @tparam FieldT
       * @param dyiTag gas species production rate in CPD model, including CO2, CO, H2, H2O, HCN, NH3 and CH4
       * @param giRHStag gas species production rate in CPD model, including extra loose, loose and tight for each species
       * @param nparsize number of particle sizes
       */
      Builder( const Expr::TagList& dyiTag,
               const Expr::TagList& giRHStag,
               const int nparsize)
            : ExpressionBuilder(dyiTag),
              giRHStag_( giRHStag ),
              nparsize_(nparsize){}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new dy_gi<FieldT>( giRHStag_, nparsize_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& dyi = this->get_value_vec();
      const int nspecies = dyi.size()/nparsize_;

      for (size_t partIndex = 0; partIndex<nparsize_; partIndex++){
        for( size_t i=0; i<nspecies; i++ ){
          const double mw = spSum_[i].first; // g/s
          const VecI& contributingIndices = spSum_[i].second;

          FieldT& dy = *dyi[partIndex*nspecies+i];
          dy <<= 0.0;
          for( size_t j=0; j<contributingIndices.size(); j++ ){
            const size_t index = contributingIndices[j];
            const FieldT& g = gi_[partIndex*specnum_+index - 1]->field_ref();
            dy <<= dy + g;
          }
        }
      }
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      std::vector<FieldT > drhsdv;
      const int nspecies = targetTags.size()/nparsize_;
      for(size_t i=0;i<targetTags.size();++i){
        drhsdv.push_back(this->sensitivity_result(targetTags[i], var));
      }

      for (size_t partIndex = 0; partIndex<nparsize_; partIndex++){
        for( size_t i=0; i<nspecies; ++i ){
          const double mw = spSum_[i].first; // g/s
          const VecI& contributingIndices = spSum_[i].second;

          drhsdv[partIndex*nspecies+i] <<= 0.0;
          for( size_t j=0; j<contributingIndices.size(); ++j ){
            const size_t index = contributingIndices[j];
            const FieldT& dgdv = gi_[partIndex*specnum_ + index - 1]->sens_field_ref(var);
            drhsdv[partIndex*nspecies+i] <<= drhsdv[partIndex*nspecies+i] + dgdv;
          }
        }
      }
    }
  };

} // namespace CPD

#endif // y_gi_h
