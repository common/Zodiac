#ifndef CPDInterface_h
#define CPDInterface_h

/**
 *  @file CPDInterface.h
 *  @defgroup CPD CPD Model
 *  @author Hang
 */

#include <expression/ExprLib.h>
#include "CPDData.h"
#include "particles/coal/Devolatilization/DevolatilizationBase.h"
#include "particles/coal/CoalData.h"

namespace CPD{
  /**
   * @class CPDTags
   * @ingroup CPD
   * @brief Getting tags used in CPD model
   * @author Hang Zhou
   */
    class CPDTags{
    public:
        Expr::TagList initPartMassFracTags,         ///< initial particle mass fraction
                      lTags,                        ///< mass of labile bridge
                      lInflowTags,                  ///< inflow mass of labile bridge
                      lMixRhsTags,                  ///< mixing RHS of labile bridge mass
                      lKinRhsTags,                  ///< kinetic RHS of labile bridge mass
                      lReacTags,                    ///< reaction rate of l in l mass fraction equation
                      lFullRhsTags,                 ///< full RHS of labile bridge mass
                      fEfunbTags,                   ///< F(E) function
                      egybTags,                     ///< active energy for kb
                      kbTags,                       ///< kb value
                      gTags,                        ///< mass of g
                      gInflowTags,                  ///< inflow mass of g
                      gMixRhsTags,                  ///< mixing RHS of g
                      gKinRhsTags,                  ///< kinetic RHS of g
                      gReacTags,                    ///< reaction rate of g in g mass fraction equation
                      gFullRhsTags,                 ///< full RHS of g
                      fEgTags,                      ///< F(E) function
                      egygTags,                     ///< active energy of kg_i
                      kgTags,                       ///< kg of g_i
                      deltaTags,                    ///< mass of delta
                      deltaInflowTags,              ///< inflow mass of delta
                      deltaMixRhsTags,              ///< mixing RHS of delta
                      deltaKinRhsTags,              ///< kinetic RHS of delta
                      deltaReacTags,                ///< reaction rate of delta in delta mass fraction equation
                      deltaFullRhsTags;             ///< full RHS of delta

        CPDTags(  const Expr::Context& state,
                  const std::vector<std::string>& particleNumArray,
                  const std::vector<std::string>& cpdArray);
    };

  /**
   *  @ingroup CPD
   *  @class CPDInterface
   *  @brief Provides an interface to the CPD model
   */
  class CPDInterface: public Dev::DevolatilizationBase
  {
    const CPD::CPDInformation cpdInfo_;
    const Coal::CoalComposition& coalcomp_;
    const Dev::DevModel dvm_;
    const TarAndSootInfo& tarSootInfo_;
    std::vector<std::string> particleNumArray_;
    std::vector<std::string> speciesArray_;
    std::vector<std::string> partNumSpeciesArray_;
    std::vector<std::string> cpdArray_;
    const YAML::Node& rootParser_;
    std::set<Expr::ExpressionID>& initRoots_;
    Expr::ExpressionFactory& initFactory_;
    Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
    Expr::TagList& primitiveTags_;
    Expr::TagList& kinRhsTags_;
    int& equIndex_;
    const bool isRestart_;

    const double c0, vmFrac0, tar0;
    const int cpdSpeciesNum;

  public:
    /**
     * @param coalType coal type
     * @param dvm devolatilization model
     * @param tarSootInfo tarSoot information
     * @param particleNumArray list of particle numbers with different sizes
     * @param speciesArray list of species involved in CPD model
     * @param partNumSpeciesArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
    CPDInterface( const Coal::CoalType coalType,
                  const Dev::DevModel dvm,
                  const TarAndSootInfo& tarSootInfo,
                  std::vector<std::string> particleNumArray,
                  std::vector<std::string> speciesArray,
                  std::vector<std::string> partNumSpeciesArray,
                  const YAML::Node& rootParser,
                  std::set<Expr::ExpressionID>& initRoots,
                  Expr::ExpressionFactory& initFactory,
                  Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                  Expr::TagList& primitiveTags,
                  Expr::TagList& kinRhsTags,
                  int& equIndex,
                  const bool isRestart);

    ~CPDInterface();

    /**
     * @brief Adding required variables needed to restart in CPD model
     * @param restartVars variables needed to restart
     */
    void restart_var_coal_dev(std::vector<std::string>& restartVars);

    /**
     * @brief Set the initial conditions for the CPD model
     */
    void setup_initial_conditions_dev();

    /**
     * @brief Register all expressions required to implement the CPD model and add equation into the time integrator
     */
    void build_equation_system_coal_dev();

    /**
     * @brief Initialize CPD model
     */
    void initialize_coal_dev();

    /**
     * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in CPD model
     */
    void modify_prim_kinrhs_tags_dev();

    /**
      * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in CPD model
      * @param primVarIdxMap primitive variables indices
      * @param consVarIdxMap solved variables indices
      * @param kinRhsIdxMap kinetic rhs indices
      * @param rhsIdxMap rhs tags indices
      */
    void modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                            const std::map<Expr::Tag, int> &consVarIdxMap,
                            std::map<Expr::Tag, int> &kinRhsIdxMap,
                            const  std::map<Expr::Tag, int> &rhsIdxMap);
     /**
      * @brief Modify particle state transformation matrix
      * @param dVdUPart particle state transformation matrix
      * @param primVarIdxMap primitive variables indices
      * @param consVarIdxMap solved variables indices
      */
    void dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                        std::map<Expr::Tag, int> primVarIdxMap,
                        std::map<Expr::Tag, int> consVarIdxMap);

    /**
     * @brief Modify particle mixing rhs matrix (dmixPartdV)
     * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
     * @param primVarIdxMap primitive variables indices
     * @param rhsIdxMap rhs tags indices
     * @param consVarRhsMap solved variables rhs tags indices
     */
    void dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                              std::map<Expr::Tag, int> primVarIdxMap,
                              std::map<Expr::Tag, int> rhsIdxMap,
                              std::map<Expr::Tag, Expr::Tag> consVarRhsMap);
  };

} // namespace CPD

#endif // CPDInterface_h
