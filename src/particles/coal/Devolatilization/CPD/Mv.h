#ifndef Mv_Expr_h
#define Mv_Expr_h
/**
 * @file Mv.h
 * @par Calculating volatile mass based on laible bridge (l) and side chains (delta_i)
 * @author Hang Zhou
 */
#include <expression/Expression.h>

namespace CPD{

/**
 *  \class Mv
 *  \ingroup CPD
 *  \brief Calculating volatile mass based on laible bridge (l) and side chains (delta_i)
 *
 *  The expression is given as:
 *  \f[
 *     m_v = m_{\ell} + \sum){i=1}^{nspecies}m_{\delta_i}
 *  \f]
 *  Here, \f$m_v\f$ is the mass of volatile per gas mass.
 *        \f$m_{\ell} is the mass of labile bridge per gas mass.
 *        \f$nspecies=15\f$ is the number of species released from CPD model.
 *        \f$m_{\delta_i}\f$ is the mass of side chain per gas mass for species i. (including all 15 light gas species and tar)
 *
 * NOTE: This expression calculates the volatile mass per gas mass from all particles with ONE specific particle size.
 *       All the parameters used here are the value for ONE specific particle size.
 */
  template< typename FieldT >
  class Mv
   : public Expr::Expression<FieldT>
  {
    const int partIndex_, specnum_;
    const CPDInformation& cpd_;
    DECLARE_FIELD( FieldT, l_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, deltai_ )

    Mv( const Expr::Tag& ltag,
        const Expr::TagList& deltaitag,
        const CPDInformation& cpd,
        const int partIndex)
        : Expr::Expression<FieldT>(),
          cpd_(cpd),
          partIndex_(partIndex),
          specnum_(cpd.get_nspec()+1){
      this->set_gpu_runnable( true );
      l_ = this->template create_field_request<FieldT>( ltag );
      this->template create_field_vector_request<FieldT>( deltaitag, deltai_ );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
        const Expr::Tag ltag_;
        const Expr::TagList deltaitag_;
        const CPDInformation& cpd_;
        const int partIndex_;
    public:
      /**
       * @brief The mechanism for buidling a Mv object
       * @tparam FieldT
       * @param mvTag mass of volatile per gas mass
       * @param ltag mass of labile bridge per gas mass
       * @param deltaitag taglist of side chains mass per gas mass. (including all 15 light gas species and tar)
       * @param cpd CPDInformation
       * @param partIndex particle index in the list of particle size
       */
      Builder( const Expr::Tag& mvTag,
               const Expr::Tag& ltag,
               const Expr::TagList& deltaitag,
               const CPDInformation& cpd,
               const int partIndex)
            : ExpressionBuilder(mvTag),
              ltag_      ( ltag ),
              deltaitag_ ( deltaitag ),
              cpd_       ( cpd ),
              partIndex_ ( partIndex){}
        ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new Mv<FieldT>( ltag_, deltaitag_, cpd_, partIndex_ );
      }
    };

    void evaluate(){
      FieldT& result = this->value();
      using namespace SpatialOps;
      const FieldT& ell = l_->  field_ref();

      result <<= cond(ell<=0.0, 0.0)(ell);

      for( size_t i=0; i< specnum_; ++i){
        const FieldT& deltai = deltai_[partIndex_*specnum_+i]->field_ref();
        result <<= result + cond(deltai<=0.0, 0.0)(deltai);
      }
    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( var );
      const FieldT& ell = l_->  field_ref();
      const FieldT& dldv   = l_->  sens_field_ref(var);
      dfdv <<= cond(ell>0, dldv)(0.0);

      for( size_t i=0; i< specnum_; ++i){
        const FieldT& deltai = deltai_[partIndex_*specnum_+i]->field_ref();
        const FieldT& ddeltaidv = deltai_[partIndex_*specnum_+i]->sens_field_ref(var);
        dfdv <<= dfdv + cond(deltai>0, ddeltaidv)(0.0);
      }
    }
  };
} // namespace
#endif // Mv_Expr_h
