
/**
 * @file F_E_fun.h
 * @par Getting F(E) function, which is used to get activation energy
 * @author Hang Zhou
 */

#ifndef FE_fun_h
#define FE_fun_h

#include <spatialops/Nebo.h>
#include <expression/Expression.h>
#include "c0_fun.h"

namespace CPD{

  /**
   * @class FE_fun_b
   * @ingroup CPD
   * @brief Getting F(E) function, which is used to get activation energy, \f$E_b\f$.
   *
   * The function is given as
   * \f[
   *    F(E) = 1-\frac{l}{l_0}
   * \f]
   * Here, \f$l\f$ is the labile bridge. \f$l_0\f$ is the initial labil bridge.
   */

  template<typename FieldT>
  class FE_fun_b : public Expr::Expression<FieldT> {
    const double l0_;
    const CPDInformation& cpd_;
    DECLARE_FIELD( FieldT, l_)

    FE_fun_b( const Expr::Tag& lTag,
              const CPDInformation& cpd )
          : Expr::Expression<FieldT>(),
            cpd_( cpd ),
            l0_( cpd.get_l0_mass() ){
      this->set_gpu_runnable( true );
      l_ = this->template create_field_request<FieldT>( lTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag lTag_;
      const CPDInformation& cpd_;
    public:
      /**
       * @brief The mechanism for building a FE_fun_b object
       * @param fEbTag F(E) function
       * @param lTag mass of labile bridge per gas mass
       * @param cpd CPD information
       */
      Builder( const Expr::Tag& fEbTag,
               const Expr::Tag& lTag,
               const CPDInformation& cpd )
            : Expr::ExpressionBuilder( fEbTag ),
              lTag_                  ( lTag   ),
              cpd_                   ( cpd    ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new FE_fun_b<FieldT>( lTag_, cpd_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      this->value() <<= 1.0 - l_->field_ref() / l0_;
    }

    void sensitivity( const Expr::Tag& var){
      FieldT& dfdv = this->sensitivity_result( var );
      dfdv <<= - l_->sens_field_ref(var) / l0_;
    }

  };

    /**
     * @class FE_fun_gi
     * @ingroup CPD
     * @brief Getting F(E) function, which is used to get activation energy, \f$E_{g_i}\f$.
     *
     * The function is given as
     * \f[
     *    F(E) = \frac{g_i}{2(1-c_0)m_{p,0}Y_{v,0}fg_i/\sum fg_i}
     * \f]
     * Here, \f$g_i\f$ is the mass of species i per gas mass from CPD model.
     *       \f$c_0\f$ is the initial mass of carbon in volatile.
     *       \f$m_{p,0}\f$ is the initial mass of particle per gas mass.
     *       \f$Y_{v,0}\f$ is the initial mass fraction of volatile in particle.
     *       \f$c_0\f$ is the initial mass of carbon in volatile.
     *       \f$fg_{i}\f$ is functional group for each bond.
     */

  template<typename FieldT>
  class FE_fun_gi : public Expr::Expression<FieldT> {
    DECLARE_FIELDS( FieldT, gi_, initpartmass_ )
    const CPDInformation& cpd_;
    const int index_;
    const std::vector<double>& fg_;
    const double sumfg_, c0_, vm_, tarFrac0_;
    double gimax_;

    FE_fun_gi( const Expr::Tag& giTag,
               const Expr::Tag& initpartmasstag,
               const CPDInformation& cpd,
               const int index)
          : Expr::Expression<FieldT>(),
            cpd_  ( cpd ),
            index_(index),
            fg_   (cpd.get_fgi()),
            sumfg_(cpd.get_sumfg()),
            c0_   (c0_fun( cpd.get_coal_composition().get_C(), cpd.get_coal_composition().get_O())),
            vm_   (cpd.get_coal_composition().get_vm()),
            tarFrac0_(cpd.get_tarMassFrac()){
      this->set_gpu_runnable( true );
      gi_           = this->template create_field_request<FieldT>( giTag );
      initpartmass_ = this->template create_field_request<FieldT>( initpartmasstag );

      if(index_ == fg_.size()){
        gimax_ = 2.0 * ( 1.0 - c0_ ) * tarFrac0_ / sumfg_;
      }
      else{
        gimax_ = 2.0 * ( 1.0 - c0_ ) * fg_[index_] / sumfg_;
      }
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag giTag_, initpartmassTag_;
      const CPDInformation& cpd_;
      const int index_;
    public:
      /**
       * @brief The mechanism for building a FE_fun_gi object
       * @param fEgiTag F(E) function
       * @param giTag the mass of species i per gas mass from CPD model.
       * @param initpartmassTag initial particle mass per gas mass
       * @param cpd CPDInformation
       * @param index index of species i in list of \f$g_i\f$.
       */
      Builder( const Expr::Tag& fEgiTag,
               const Expr::Tag& giTag,
               const Expr::Tag initpartmassTag,
               const CPDInformation& cpd,
               const int index)
            : Expr::ExpressionBuilder( fEgiTag         ),
              giTag_                 ( giTag           ),
              initpartmassTag_       ( initpartmassTag ),
              cpd_                   ( cpd             ),
              index_                 ( index           ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new FE_fun_gi<FieldT>( giTag_, initpartmassTag_, cpd_, index_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& fEgi = this->value();
      const FieldT& gi = gi_->field_ref();
      const FieldT& initPartMass = initpartmass_->field_ref();

      fEgi <<= cond( gimax_ * vm_ == 0.0, 0.0)
                   (                     gi / (gimax_ * initPartMass * vm_ )); //this assumes mass basis
  //               (               gi / (gimax * initPartMass * vm_ / cpd_.get_hypothetical_volatile_mw() )); //this assumes mole basis

    }

    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT & dfdv = this->sensitivity_result( var );
      const FieldT& initPartMas = initpartmass_->field_ref();

      dfdv <<= cond( gimax_ * vm_ == 0.0, 0.0)
            (                gi_->sens_field_ref(var) / ( gimax_ * initPartMas * vm_ )
                             - gi_->field_ref() / ( gimax_ * initPartMas * initPartMas * vm_ ) * initpartmass_->sens_field_ref(var) );  //this assumes mass basis
  //                 (                gi_->sens_field_ref(var) / ( gimax * initPartMas * vm_ / cpd_.get_hypothetical_volatile_mw() )
  //                                  - gi_->field_ref() / ( gimax * square(initPartMas) * vm_ / cpd_.get_hypothetical_volatile_mw() ) * initpartmass_->sens_field_ref(var) );   //this assumes mole basis

    }
  };

}  // namespace CPD

#endif // FE_fun_h
