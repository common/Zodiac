
/**
 * @file C_RHS.h
 * @par Getting the production rate of C in CPD model
 */
#ifndef C_RHS_Expr_h
#define C_RHS_Expr_h

#include <expression/Expression.h>

namespace CPD{

/**
 *  @class C_RHS
 *  @ingroup CPD
 *  @brief Getting the production rate of C in CPD model (unit: 1/s)
 *
 *  The reaction path for C from volatile is
 *  \f[
 *     l \rightarrow l^* \rightarrow C + 2g
 *  \f]
 *  Here, \f$l\f$ is the labile bridge. \f$g\f$ includes light gas and tar.
 *
 *  The production rate of C is given as:
 *  \f[
 *     \frac{dm_C}{dt} = k_c l^* = \frac{k_c k_b l}{k_{\delta}+k_c} \frac{Mw_C}{Mw_l} = \frac{k_b l}{\rho+k_c} \frac{Mw_C}{Mw_l}
 *  \f]
 *  Here, \f$ k_{b} \f$ is reaction constant from \f$l\f$ to \f$l^*\f$.
 *        \f$ k_{c} \f$ is reaction constant from \f$l^*\f$ to \f$C\f$.
 *        \f$ k_{\delta} \f$ is reaction constant from \f$l^*\f$ to \f$\delta\f$, which is not shown here)
 *        \f$ Mw_C\f$ and \f$ Mw_l\f$ are the molecular weight of \f$C\f$ and \f$l\f$.
 *        \f$\rho = \frac{k_{\delta}}{k_{c}}=0.9\f$.
 *
 * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
 *          All the parameters used here are the value for ONE specific particle size.
 *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
 */
  template<typename FieldT>
  class C_RHS: public Expr::Expression<FieldT>
  {
    const CPDInformation& cpd_;
    DECLARE_FIELDS( FieldT, kb_, l_ )
    const double mwl0_, rho_;

    C_RHS( const Expr::Tag& kbTag,
           const Expr::Tag& lTag,
           const CPDInformation& cpd )
          : Expr::Expression<FieldT>(),
            cpd_ ( cpd ),
            mwl0_( cpd.l0_molecular_weight() ),
            rho_ ( 0.9 )
    {
      this->set_gpu_runnable(true);
      kb_     = this->template create_field_request<FieldT>( kbTag    );
      l_      = this->template create_field_request<FieldT>( lTag     );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag lTag_, kbTag_;
      const CPDInformation& cpd_;
    public:
      /**
       * @brief The mechanism for building a C_RHS object
       * @tparam FieldT
       * @param crhsTag production rate of C from CPD model
       * @param kbTag reaction constant from \f$l\f$ to \f$l^*\f$.
       * @param lTag  mass of labile bridge per gas mass
       * @param cpd CPDInformation
       */
      Builder( const Expr::Tag& crhsTag,
               const Expr::Tag& kbTag,
               const Expr::Tag& lTag,
               const CPDInformation& cpd )
            : ExpressionBuilder(crhsTag),
              lTag_ ( lTag  ),
              kbTag_( kbTag ),
              cpd_( cpd   ){}
      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new C_RHS<FieldT>( kbTag_, lTag_, cpd_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();
      const FieldT& kb  = kb_->field_ref();
      const FieldT& ell = l_ ->field_ref();

      result <<= cond(ell<=0.0, 0.0)
                (( kb * ell ) / (rho_ + 1.0) * (12.0 / mwl0_));
    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result(var);
      const FieldT& kb    = kb_->field_ref();
      const FieldT& ell   = l_ ->field_ref();
      const FieldT& dkbdv = kb_->sens_field_ref(var);
      const FieldT& dldv  = l_->sens_field_ref(var);

      dfdv <<=  cond(ell<=0.0, 0.0)((dkbdv * ell + kb * dldv) / (rho_ + 1.0) * (12.0 / mwl0_)); // "ell" is in kg
    }
  };


} // namespace CPD

#endif // C_RHS_Expr_h
