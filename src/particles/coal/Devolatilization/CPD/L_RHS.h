#ifndef L_RHS_Expr_h
#define L_RHS_Expr_h
/**
 * @file L_RHS.h
 * @par Calculating the reaction rate of labile bridge
 * @author Hang Zhou
 */
#include <expression/Expression.h>

namespace CPD{

/**
 *  \ingroup CPD
 *  \class L_RHS
 *  \brief Calculating the reaction rate of labile bridge
 *
 *  In this Expression the RHS of the differential equation in below being evaluated :
 *  \f[ \frac{dl}{dt}=-k_{b}\ell \f]
 *  where
 *  - \f$k_{b}\f$ : reaction constant of labile bridge
 *  - \f$\ell\f$ : labile bridge
 */
  template<typename FieldT>
  class L_RHS
        : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, kb_, l_ )

    L_RHS( const Expr::Tag& kbTag,
           const Expr::Tag& lTag)
          : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      kb_    = this->template create_field_request<FieldT>( kbTag    );
      l_     = this->template create_field_request<FieldT>( lTag     );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag kbTag_, lTag_;
    public:
      /**
       * @brief The mechanism for building a L_RHS object
       * @tparam FieldT
       * @param rhsTag reaction rate of labile bridge
       * @param kbTag reaction constant of labile bridge
       * @param lTag mass of labile bridge per gas mass
       */
      Builder( const Expr::Tag& rhsTag,
               const Expr::Tag& kbTag,
               const Expr::Tag& lTag)
            : ExpressionBuilder(rhsTag),
              kbTag_( kbTag ),
              lTag_ ( lTag  ){}
      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new L_RHS<FieldT>( kbTag_, lTag_ );
      }

    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();
      const FieldT& kb = kb_->field_ref();
      const FieldT& ell= l_ ->field_ref();

      result <<= cond(ell<=0.0,0.0)( -kb* ell); //negative
    }
    void sensitivity( const Expr::Tag& var){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result(var);
      const FieldT& kb    = kb_->field_ref();
      const FieldT& ell   = l_ ->field_ref();
      const FieldT& dkbdv = kb_->sens_field_ref(var);
      const FieldT& dldv  = l_ ->sens_field_ref(var);

      dfdv <<= cond(ell<=0.0,0.0)(-(dkbdv * ell + kb * dldv));
    }
  };

//--------------------------------------------------------------------

#endif // L_RHS_Expr_h

} // namespace CPD
