/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   DevolatilizationInterface.h
 *  \date   Jul 08, 2018
 *  \author hang
 */

#ifndef DEVOLATILIZATION_INTERFACE_UTIL_H_
#define DEVOLATILIZATION_INTERFACE_UTIL_H_

#include <vector>
#include <expression/ExprLib.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>
#include <yaml-cpp/yaml.h>
#include "DevolatilizationBase.h"
#include "SingleRate/SingleRateInterface.h"
#include "KobayashiSarofim/KobSarofimInterface.h"
#include "CPD/CPDInterface.h"
#include "particles/coal/TarSootReaction/TarSootReactionInterface.h"
#include "particles/coal/TarSootReaction/TarSootBase.h"

namespace Dev {
  /**
    *  @ingroup Dev
    *  @class DevTags
    *  @brief Getting tags used in devolatilization model
    */
  class DevTags {
  public:
      const Expr::Tag volMassGasSourceTag,      ///< gas phase mass source term from devolatilization
                      volHeatGasSourceTag,      ///< gas phase energy source term from devolatilization
                      volHeatGasSourcePFRTag,   ///< gas phase energy source term from devolatilization for plug flow reactor
                      volMassToGasTag,          ///< rhs in gas phase mass equation from devolatilization
                      volHeatToGasTag,          ///< rhs in gas phase energy equation from devolatilization
                      volHeatDpDxTag;           ///< enthalpy source term in `Dp/Dx` from devolatilization for plug flow reactor
      Expr::TagList volRateTags,                ///< devolatilization rate
                    volSpeciesSourceTags,       ///< species source term in gas from devolatilization
                    volSpeciesSourcePFRTags,    ///< species source term in gas from devolatilization for plug flow reactor
                    volSpeciesRHSTags,          ///< rhs in species mass fraction equation in gas from
                    volSpeciesGasTags,          ///< species mass transport from every particle to gas
                    volTarTags,                 ///< production of tar from devolatilization for every particle
                    volCharTags,                ///< production of char from devolatilization for every particle
                    enthTpartTags;              ///< enthalpy of different species at particle temperature for each particle size
   DevTags( const Expr::Context& state,
            const std::vector<std::string>& particleNumArray,
            const std::vector<std::string>& SpeciesArray,
            const std::vector<std::string>& partNumSpeciesArray);
  };
  /**
   *  @ingroup Dev
   *  @class DevolatilizationInterface
   *  @brief Provides an interface to the Devolatilization model
   */
  class DevolatilizationInterface{

      const Coal::CoalType coaltype_;
      const Dev::DevModel dvm_;
      const Tarsoot::TarsootModel tsm_;
      Dev::DevolatilizationBase* devmodel_;
      SingleRate::SingleRateInterface* singleratemdel_;
      KobSarofim::KobSarofimInterface* kobmodel_;
      CPD::CPDInterface* cpdmodel_;
      Tarsoot::TarSootInterface* tarsoot_;

      std::vector<std::string> particleNumArray_;
      std::vector<std::string> speciesArray_;
      std::vector<std::string> partNumSpeciesArray_;
      const YAML::Node& rootParser_;
      std::set<Expr::ExpressionID>& initRoots_;
      Expr::ExpressionFactory& initFactory_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      Expr::TagList& primitiveTags_;
      Expr::TagList& kinRhsTags_;
      int& equIndex_;
      const bool isRestart_;

  public:
    /**
     * @param coalType coal type
     * @param dvm devolatilization model
     * @param tsm tarSoot model
     * @param particleNumArray list of particle numbers with different sizes
     * @param SpeciesArray list of species involved in devolatilization model
     * @param partNumSpeciesArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
      DevolatilizationInterface( const Coal::CoalType coalType,
                                 const Dev::DevModel dvm,
                                 const Tarsoot::TarsootModel tsm,
                                 std::vector<std::string> particleNumArray,
                                 std::vector<std::string> SpeciesArray,
                                 std::vector<std::string> partNumSpeciesArray,
                                 const YAML::Node& rootParser,
                                 std::set<Expr::ExpressionID>& initRoots,
                                 Expr::ExpressionFactory& initFactory,
                                 Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                                 Expr::TagList& primitiveTags,
                                 Expr::TagList& kinRhsTags,
                                 int& equIndex,
                                 const bool isRestart);
      ~DevolatilizationInterface();
      /**
       * @brief Adding required variables needed to restart in devolatilization model
       * @param restartVars variables needed to restart
       */
      void restart_var_coal_dev(std::vector<std::string>& restartVars);
      /**
       * @brief Initialize devolatilization model
       */
      void initialize_coal_dev();
      /**
       * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in devolatilization model
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       * @param kinRhsIdxMap kinetic rhs indices
       * @param rhsIdxMap rhs tags indices
       */
      void modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                              const std::map<Expr::Tag, int> &consVarIdxMap,
                              std::map<Expr::Tag, int> &kinRhsIdxMap,
                              const  std::map<Expr::Tag, int> &rhsIdxMap);
      /**
       * @brief Modify particle state transformation matrix
       * @param dVdUPart particle state transformation matrix
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       */
      void dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                          std::map<Expr::Tag, int> primVarIdxMap,
                          std::map<Expr::Tag, int> consVarIdxMap);

      /**
       * @brief Modify particle mixing rhs matrix (dmixPartdV)
       * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       */
      void dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                std::map<Expr::Tag, int> primVarIdxMap,
                                std::map<Expr::Tag, int> rhsIdxMap,
                                std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  private:
      /**
       * @brief Set the initial conditions for the devolatilization model
       */
      void setup_initial_conditions_dev();
      /**
       * @brief Register all expressions required to implement the devolatilization model and add equation into the time integrator
       */
      void build_equation_system_coal_dev();
      /**
       * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in devolatilization model
       */
      void modify_prim_kinrhs_tags_dev();

  };
} // namespace Dev

#endif /* DEVOLATILIZATION_INTERFACE_UTIL_H_ */
