/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   SingleRateInterface.h
 *  @date   Jun 30, 2018
 *  @author hang
 */

#ifndef SINGLERATE_INTERFACE_UTIL_H_
#define SINGLERATE_INTERFACE_UTIL_H_

#include <expression/ExprLib.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>
#include <yaml-cpp/yaml.h>

#include "particles/coal/Devolatilization/DevolatilizationBase.h"
#include "particles/coal/TarSootReaction/TarAndSootInfo.h"

namespace SingleRate {
  /**
    *  @ingroup SingleRate
    *  @class SingleRateInterface
    *  @brief Provides an interface to the SingleRate model
    */
  class SingleRateInterface: public Dev::DevolatilizationBase
  {
      const Coal::CoalComposition coalcomp_;
      const Dev::DevModel dvm_;
      const TarAndSootInfo& tarSootInfo_;
      const YAML::Node& rootParser_;
      std::vector<std::string> particleNumArray_;
      std::vector<std::string> speciesArray_;
      std::vector<std::string> partNumSpeciesArray_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      const bool isRestart_;
  public:
    /**
     * @param coalType coal type
     * @param dvm devolatilization model
     * @param tarSootInfo tarSoot information
     * @param particleNumArray list of particle numbers with different sizes
     * @param speciesArray list of species involved in SingleRate model
     * @param partNumSpeciesArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param integrator time integrator used in Zodiac
     * @param isRestart if restart or not (bool)
     */
      SingleRateInterface( const Coal::CoalType coalType,
                           const Dev::DevModel dvm,
                           const TarAndSootInfo& tarSootInfo,
                           std::vector<std::string> particleNumArray,
                           std::vector<std::string> speciesArray,
                           std::vector<std::string> partNumSpeciesArray,
                           const YAML::Node& rootParser,
                           Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                           const bool isRestart);

      ~SingleRateInterface();
     /**
      * @brief Adding required variables needed to restart in SingleRate model
      * @param restartVars variables needed to restart
      */
      void restart_var_coal_dev(std::vector<std::string>& restartVars);

      /**
       * @brief Set the initial conditions for the SingleRate model
       */
      void setup_initial_conditions_dev();

      /**
       * @brief Register all expressions required to implement the SingleRate model
       */
      void build_equation_system_coal_dev();

      /**
       * @brief Initialize SingleRate model
       */
      void initialize_coal_dev();

      /**
       * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in SingleRate model
       */
      void modify_prim_kinrhs_tags_dev();

      /**
       * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in SingleRate model
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       * @param kinRhsIdxMap kinetic rhs indices
       * @param rhsIdxMap rhs tags indices
       */
      void modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                              const std::map<Expr::Tag, int> &consVarIdxMap,
                              std::map<Expr::Tag, int> &kinRhsIdxMap,
                              const  std::map<Expr::Tag, int> &rhsIdxMap);

      /**
       * @brief Modify particle state transformation matrix
       * @param dVdUPart particle state transformation matrix
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       */
      void dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                          std::map<Expr::Tag, int> primVarIdxMap,
                          std::map<Expr::Tag, int> consVarIdxMap);

      /**
       * @brief Modify particle mixing rhs matrix (dmixPartdV)
       * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       */
      void dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                std::map<Expr::Tag, int> primVarIdxMap,
                                std::map<Expr::Tag, int> rhsIdxMap,
                                std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  };

} // namespace SingleRate

#endif /* SINGLERATE_INTERFACE_UTIL_H_ */
