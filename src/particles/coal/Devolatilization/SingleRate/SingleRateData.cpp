/*
 *  SingleRateData.cpp
 *  ODT
 *
 *  \author Babak Goshayeshi (www.bgoshayeshi.com)
 *  \date   May, 2013 
 *                 
 *   Department of Chemical Engineering - University of Utah
 */

#include "SingleRateData.h"

namespace SingleRate {

		// Copied from KobSarofimInformation !
  SingleRateInformation::
  SingleRateInformation( const Coal::CoalComposition& coalType )
  : coalcomp_ ( coalType )
  {
    const double c = coalcomp_.get_C();
    const double h = coalcomp_.get_H();
    const double o = coalcomp_.get_O();
    const double vol = coalcomp_.get_vm();
    const double fixC = coalcomp_.get_fixed_c();

    const double nc = (c*(vol+fixC)-fixC)/12.0;
    const double nh = h*(vol+fixC)/1.0;
    const double no = o*(vol+fixC)/16.0;

    h_ = nh/nc;
    o_ = no/nc;

    mw_= 12.0 + h_ + o_ * 16.0;
  }

} // namespace SingleRate
