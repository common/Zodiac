/**
 * @file SingleRateModel.h
 * @par Getting the devolatilization rate and production from devolatilization
 * @author Hang
 */

#ifndef Dev_SingleRateModel_Expr_h
#define Dev_SingleRateModel_Expr_h

#include <expression/Expression.h>
#include "SingleRateData.h"


namespace SingleRate {

  /**
   *  @class   SingleRateModel
   *  @ingroup SingleRate
   *  @brief   Evaluates the devolatilization rate and product by
   *           Single Rate  model: (unit: 1/s)
   *           \f$ CH_{h}O_{o}\rightarrow oCO+\frac{h-(1-o)n/m}{2}H_{2}+\frac{(1-o)}{m}C_{m}H_{n}\f$.
   *           In this model, the tar is regards as \f$ C_{m}H_{n}\f$, and it is assumed that there is no char production.
   *
   *  The volatile yield rate is given as
   *  \f[
   *     \frac{dm_v}{dt} = - A exp(-E/(RT_p)) m_v
   *  \f]
   *  Here, \f$ A = 4.5 \times 10^5 (1/s)\f$ and \f$ E = 8.1 \times 10^7 (J/k mol) \f$.
   *  \f$ m_v \f$ is the mass of volatile per gas mass.
   *
   * The sensitivity of volatile yield rate is given as
   * \f[
   *    \frac{\partial (dm_v/dt)}{\partial \phi} = - A exp(-E/RT_p) \left( E/(RT_p^2) m_v \frac{\partial T_p}{\partial \phi} +
   *                                               \frac{\partial m_v}{\partial \phi} \right)
   * \f]
   *
   * The CO production rate from devolatilization is given as
   *  \f[
   *     \frac{dm_CO}{dt} = - \frac{dm_v}{dt} \frac{Mw_{CO}}{Mw_v} o
   *  \f]
   *  Here, \f$ Mw_{CO}\f$ and \f$ Mw_v \f$ are the molecular weight of CO and volatile respectively.
   *        \f$ o \f$ is the coefficient of O in \f$ CH_hO_o \f$.
   *
   * The sensitivity of CO production rate is given as
   * \f[
   *    \frac{\partial (dm_{CO}/dt)}{\partial \phi} = - \frac{\partial (dm_v/dt)}{\partial \phi} \frac{Mw_{CO}}{Mw_v} o
   * \f]
   *
   * The H2 production rate from devolatilization is given as
   *  \f[
   *     \frac{dm_H2}{dt} = - \frac{dm_v}{dt} \frac{Mw_{H2}}{Mw_v} \frac{h-(1-o)n/m}{2}
   *  \f]
   *  Here, \f$ Mw_{H2}\f$ and \f$ Mw_v \f$ are the molecular weight of H2 and volatile respectively.
   *        \f$ o \f$ is the coefficient of O in \f$ CH_hO_o \f$.  \f$ h \f$ is the coefficient of H in \f$ CH_hO_o \f$.
   *        \f$ m \f$ is the coefficient of C in tar.  \f$ n \f$ is the coefficient of H in tar.
   *
   * The sensitivity of H2 production rate is given as
   * \f[
   *    \frac{\partial (dm_{H2}/dt)}{\partial \phi} = - \frac{\partial (dm_v/dt)}{\partial \phi} \frac{Mw_{H2}}{Mw_v} \frac{h-(1-o)n/m}{2}
   * \f]
   *
   * The tar production rate from devolatilization is given as
   *  \f[
   *     \frac{dm_{tar}}{dt} = - \frac{dm_v}{dt} \frac{Mw_{tar}}{Mw_v} \frac{(1-o)}{m}
   *  \f]
   *  Here, \f$ Mw_{tar}\f$ and \f$ Mw_v \f$ are the molecular weight of tar and volatile respectively.
   *        \f$ o \f$ is the coefficient of O in \f$ CH_hO_o \f$.
   *
   * The sensitivity of tar production rate is given as
   * \f[
   *    \frac{\partial (dm_{tar}/dt)}{\partial \phi} = - \frac{\partial (dm_v/dt)}{\partial \phi} \frac{Mw_{tar}}{Mw_v} \frac{(1-o)}{m}
   * \f]
   *
   * NOTE: 1. This expression calculates the reaction rate from all particles with ONE specific particle size.
   *          All the parameters used here are the value for ONE specific particle size.
   *       2. The production/consumption rate is calculated based on particle composition mass per gas mass, with unit (1/s).
   */

  template <typename FieldT>
  class SingleRateModel : public Expr::Expression<FieldT>
  {
      DECLARE_FIELDS( FieldT, tempP_, mv_)
      const SingleRateInformation data_;
      const TarAndSootInfo& tarSootInfo_;
      const double mw_, h_, o_, tarMW_, hTar_, cTar_;
      const double A0_, E0_;// Arheniuse parameters for Single Rate Model from Jovanovic (2012):

      SingleRateModel( const Expr::Tag& tempPTag,
                       const Expr::Tag& mvTag,
                       const SingleRateInformation& data,
                       const TarAndSootInfo& tarSootInfo)
            : Expr::Expression<FieldT>(),
              data_       (data),
              tarSootInfo_(tarSootInfo),
              mw_     ( data.get_molecularweight()      ),
              h_      ( data.get_hydrogen_coefficient() ),
              o_      ( data.get_oxygen_coefficient()   ),
              tarMW_  ( tarSootInfo.tar_mw()            ),
              hTar_   ( tarSootInfo.tar_hydrogen()      ),
              cTar_   ( tarSootInfo.tar_carbon()        ),
              A0_     ( 4.5E5                           ),  // 1/s; k1 pre-exponential factor
              E0_     ( 8.1E4                           )   // J/ mol;  k1 activation energy
      {
        this->set_gpu_runnable(true);
        tempP_ = this->template create_field_request<FieldT>( tempPTag );
        mv_    = this->template create_field_request<FieldT>( mvTag    );
      }

  public:
      /**
       *  @brief Build a SingleRateModel expression
       *  @tparam FieldT
       *  @param resultTag Volatile yields rate
       *  @param tempPTag  Particle Temperature tag in K
       *  @param mvTag     Volatile mass tag
       *  @param data      SingleRateInformation
       *  @param tarSootInfo TarAndSootInfor
       */
      class Builder : public Expr::ExpressionBuilder
      {
          const Expr::Tag tempPTag_, mvTag_;
          const SingleRateInformation data_;
          const TarAndSootInfo& tarSootInfo_;
      public:
          Builder( const Expr::TagList& resultTags,
                   const Expr::Tag& tempPTag,
                   const Expr::Tag& mvTag,
                   const SingleRateInformation& data,
                   const TarAndSootInfo& tarSootInfo)
                : ExpressionBuilder( resultTags ),
                  tempPTag_   ( tempPTag    ),
                  mvTag_      ( mvTag       ),
                  data_       ( data        ),
                  tarSootInfo_( tarSootInfo ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new SingleRateModel<FieldT>( tempPTag_, mvTag_, data_, tarSootInfo_);
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        typename Expr::Expression<FieldT>::ValVec& mvcharrhs = this->get_value_vec();

        FieldT& mvrhs   = *mvcharrhs[0]; // Volitle RHS

        // Produced Species according to reaction [1]
        FieldT& co   = *mvcharrhs[1]; // CO   Consumption rate ( for gas Phase )
        FieldT& h2   = *mvcharrhs[2]; // H2   Consumption rate ( for gas Phase )
        FieldT& tar  = *mvcharrhs[3]; // tar  Consumption rate ( for gas Phase )

        const FieldT& tempP = tempP_->field_ref();
        const FieldT& mv    = mv_   ->field_ref();

        mvrhs <<= cond( mv >0.0, -A0_ * exp(-E0_/ 8.31446 / tempP) * mv)
                      (          0.0                                 );// negative

        co    <<= - mvrhs / mw_ * o_ * 28.0 ;  // positive
        h2    <<= - mvrhs / mw_ * (h_ - (1-o_)*hTar_/cTar_) ; // 2 / 2 -> * 1   positive
        tar   <<= - mvrhs / mw_ * (1-o_)/cTar_ * tarMW_;  // positive

      }
      void sensitivity( const Expr::Tag& var){
        using namespace SpatialOps;
        const Expr::TagList& targetTags = this->get_tags();
        const FieldT& tempP    = tempP_->field_ref();
        const FieldT& mv       = mv_   ->field_ref();
        const FieldT& dtempPdv = tempP_->sens_field_ref(var);
        const FieldT& dmvdv    = mv_->sens_field_ref(var);

        std::vector<FieldT > drhsdv;
        for(size_t i=0;i<targetTags.size();++i){
          drhsdv.push_back(this->sensitivity_result(targetTags[i], var));
        }

        drhsdv[0] <<= cond( mv > 0.0, -A0_ * exp(-E0_/ 8.31446 / tempP) * (E0_/ 8.31446 / square(tempP) *dtempPdv * mv + dmvdv))
                          ( 0.0 );

        drhsdv[1] <<= - drhsdv[0] / mw_ * o_ * 28.0;
        drhsdv[2] <<= - drhsdv[0] / mw_ * (h_ - (1-o_)*hTar_/cTar_);
        drhsdv[3] <<= - drhsdv[0] / mw_ * (1-o_)/cTar_ * tarMW_;
      }
  };

} // end of namespace SingleRate

#endif // Dev_SingleRateModel_h

