#ifndef SingleRateInformation_h
#define SingleRateInformation_h

/**
 * @file KobSarofimData.h
 * @par Getting the data needed for SingleRate model
 */
#include "particles/coal/CoalData.h"

namespace SingleRate {

  class SingleRateInformation{
  public:
    SingleRateInformation( const Coal::CoalComposition& coalType );
    double get_hydrogen_coefficient() const{ return h_;  }
    double get_oxygen_coefficient()   const{ return o_;  }
    double get_molecularweight()      const{ return mw_; }
  protected:
    double h_, o_, mw_;
    const Coal::CoalComposition& coalcomp_;
  };

} // namespace end

#endif // SingleRateInformation_h
