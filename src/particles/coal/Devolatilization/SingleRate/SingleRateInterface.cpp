/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>
#include <fstream>

#include <expression/Functions.h>
#include <expression/ExpressionFactory.h>

#include "particles/ParticleInterface.h"
#include "SingleRateInterface.h"
#include "SingleRateModel.h"
#include "SingleRateData.h"

namespace SingleRate {

  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  typedef SingleRate::SingleRateModel<FieldT>::Builder SingleRateT;

  SingleRateInterface::
  SingleRateInterface( const Coal::CoalType coalType,
                       const Dev::DevModel dvm,
                       const TarAndSootInfo& tarSootInfo,
                       std::vector<std::string> particleNumArray,
                       std::vector<std::string> speciesArray,
                       std::vector<std::string> partNumSpeciesArray,
                       const YAML::Node& rootParser,
                       Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                       const bool isRestart)
  : Dev::DevolatilizationBase(),
    coalcomp_(coalType, rootParser["Particles"]),
    dvm_(dvm),
    tarSootInfo_(tarSootInfo),
    particleNumArray_(particleNumArray),
    speciesArray_(speciesArray),
    partNumSpeciesArray_(partNumSpeciesArray),
    rootParser_(rootParser),
    integrator_(integrator),
    isRestart_(isRestart)
  {}

  //---------------destructor---------------------------
  SingleRateInterface::~SingleRateInterface(){
    std::cout << "Delete ~SingleRateInterface(): " << this << std::endl;
  }

  //---------------------------------------------------------------------------
  void SingleRateInterface::setup_initial_conditions_dev()
  {}

  //---------------------------------------------------------------------------
  void SingleRateInterface::build_equation_system_coal_dev(){

    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Dev::DevTags tagsEDev( Expr::STATE_NONE, particleNumArray_, speciesArray_, partNumSpeciesArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );

    Expr::ExpressionFactory& execFactory = integrator_->factory();
    SingleRateInformation singleratedata(coalcomp_);

    const int nparsize = particleNumArray_.size();
    const int nspecies = speciesArray_.size();
    for( size_t i = 0;i < nparsize;i++ ){

      Expr::TagList devrhsTags;
      devrhsTags.clear();
      devrhsTags.push_back( tagsEDev.volRateTags[i] );  //negative
      for( size_t j = 0;j < nspecies; ++j){
        devrhsTags.push_back( tagsEDev.volSpeciesGasTags[i*nspecies+j] );  //positive
      }
      devrhsTags.push_back( tagsEDev.volTarTags[i] ); //positive

      execFactory.register_expression( new SingleRateT( devrhsTags, tagsEPart.partTempTags[i], tagsECoal.volMassFracTags[i], singleratedata, tarSootInfo_) );
      execFactory.register_expression( new ConstantT( tagsEDev.volCharTags[i], 0.0 ) );
    }

  }

  void SingleRateInterface::initialize_coal_dev()
  {}

  void SingleRateInterface::modify_prim_kinrhs_tags_dev()
  {}

  void SingleRateInterface::modify_idxmap_dev( std::map<Expr::Tag, int> &primVarIdxMap,
                                               const std::map<Expr::Tag, int> &consVarIdxMap,
                                               std::map<Expr::Tag, int> &kinRhsIdxMap,
                                               const  std::map<Expr::Tag, int> &rhsIdxMap)
  {}

  void SingleRateInterface::dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                           std::map<Expr::Tag, int> primVarIdxMap,
                                           std::map<Expr::Tag, int> consVarIdxMap)
  {}

  void SingleRateInterface::dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                                 std::map<Expr::Tag, int> primVarIdxMap,
                                                 std::map<Expr::Tag, int> rhsIdxMap,
                                                 std::map<Expr::Tag, Expr::Tag> consVarRhsMap)
  {}

  //---------------------------------------------------------------------------------------------------------------
  void SingleRateInterface::restart_var_coal_dev(std::vector<std::string>& restartVars)
  {}

} // namespace SingleRate
