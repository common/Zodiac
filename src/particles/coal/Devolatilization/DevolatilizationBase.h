#ifndef DEVOLATILIZATIONBase_h
#define DEVOLATILIZATIONBase_h

#include <expression/Tag.h>
#include <expression/ExpressionID.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include <yaml-cpp/yaml.h>
#include <FieldTypes.h>
#include <expression/matrix-assembly/SparseMatrix.h>

/**
 *  \file DevolatilizationBase.h
 *  \ingroup Dev
 *
 */

namespace Dev{

  enum DevModel{
    CPDM,
    KOBAYASHIM,
    SINGLERATE,
    INVALID_DEVMODEL,
    OFF
  };

  /**
   * @fn std::string dev_model_name( const DevModel model )
   * @param model the devolatilization model
   * @return the string name of the model
   */
  std::string dev_model_name( const DevModel model );

  /**
   * @fn DevModel devol_model( const std::string& modelName )
   * @param modelName the string name of the model
   * @return the corresponding enum value
   */
  DevModel devol_model( const std::string& modelName );

  class DevolatilizationBase{
    public:

       /**
        * @brief Adding required variables needed to restart in SingleRate model
        * @param restartVars variables needed to restart
        */
        virtual void restart_var_coal_dev(std::vector<std::string>& restartVars) = 0;
       /**
        * @brief Set the initial conditions for the devolatilization model
        */
        virtual void setup_initial_conditions_dev() = 0;
       /**
        * @brief Register all expressions required to implement the devolatilization model and add equation into the time integrator
        */
        virtual void build_equation_system_coal_dev() = 0;
        /**
         * @brief Initialize devolatilization model
         */
        virtual void initialize_coal_dev() = 0;

        /**
         * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in devolatilization model
         */
        virtual void modify_prim_kinrhs_tags_dev() = 0;

        /**
         * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in devolatilization model
         * @param primVarIdxMap primitive variables indices
         * @param consVarIdxMap solved variables indices
         * @param kinRhsIdxMap kinetic rhs indices
         * @param rhsIdxMap rhs tags indices
         */
        virtual void modify_idxmap_dev( std::map<Expr::Tag, int> &,
                                        const std::map<Expr::Tag, int> &,
                                        std::map<Expr::Tag, int> &,
                                        const  std::map<Expr::Tag, int> &) = 0;
       /**
        * @brief Modify particle state transformation matrix
        * @param dVdUPart particle state transformation matrix
        * @param primVarIdxMap primitive variables indices
        * @param consVarIdxMap solved variables indices
        */
        virtual void dvdu_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& ,
                                    std::map<Expr::Tag, int> ,
                                    std::map<Expr::Tag, int> ) = 0;
       /**
        * @brief Modify particle mixing rhs matrix (dmixPartdV)
        * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
        * @param primVarIdxMap primitive variables indices
        * @param rhsIdxMap rhs tags indices
        * @param consVarRhsMap solved variables rhs tags indices
        */
        virtual void dmMixingdv_coal_dev( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& ,
                                          std::map<Expr::Tag, int> ,
                                          std::map<Expr::Tag, int> ,
                                          std::map<Expr::Tag, Expr::Tag>) = 0;

        virtual ~DevolatilizationBase(){}
    };

} // namespace Dev

#endif // DEVOLATILIZATION_h
