
/**
 * @file CoalParticleLoading.h
 * @par Getting the particle number per gas mass based on given equivalence ratio, coal composition and gas phase composition
 * @author Hang Zhou
 */

#ifndef CoalParticleLoading_coal_h
#define CoalParticleLoading_coal_h

#include <expression/Expression.h>

namespace Coal {

  /**
   * @class CoalParticleLoading
   * @ingroup Coal
   * @brief Getting the particle number per gas mass based on given equivalence ratio, coal composition and gas phase composition
   *
   * Total particle mass from particle could be calculated using equivalence ratio, \f$r\f$ by
   * \f[
   *    m_p = r \left(\frac{m_p}{m_g}\right)_{st} m_g
   * \f]
   * The required gas mass \f$m_{g,required}\f$ under stoichiometry is given as
   * \f[
   *    m_{g,required} = m_p\left( \frac{Y_C}{Mw_C}*1.0 + \frac{Y_H}{Mw_H}*0.25 - \frac{Y_O}{Mw_O}*0.5 \right) \frac{Mw_{O_2}}{Y_{O_2}}
   * \f]
   * And the stoichiometric fuel-air mass ratio:
   * \f[
   *    \left(\frac{m_p}{m_{g,required}}\right)_{st} = \frac{Y_{O_2}}{Mw_{O_2}}/\left( \frac{Y_C}{Mw_C}*1.0 + \frac{Y_H}{Mw_H}*0.25 - \frac{Y_O}{Mw_O}*0.5 \right)
   * \f]
   * Putting this into the above \f$m_p\f$ equation to get total particle mass.
   * The number of particle per gas mass is given as
   * \f[
   *    n_p = \frac{N_p}{m_g} = \frac{m_p}{m_p^0 m_g}
   *        =\frac{r \left(\frac{m_p}{m_g}\right)_{st} m_g}{m_p^0 m_g} =  \frac{r \left(\frac{m_p}{m_g}\right)_{st}}{m_p^0}
   * \f]
   * Here, \f$r\f$ is the given fuel-air mass equivalence ratio.
   *       \f$Y_C\f$, \f$Y_H\f$ and \f$Y_O\f$ are the mass fraction of \f$C\f$, \f$H\f$ and \f$O\f$ in particle.
   *       They are depends on the devolatilization and char reaction model.
   *       \f$Mw_C\f$, \f$Mw_H\f$ and \f$Mw_O\f$ are the molecular weight of \f$C\f$, \f$H\f$ and \f$O\f$.
   *       \f$Y_{O_2}\f$ is the mass fraction in gas.
   *       \f$m_p^0\f$ is the mass of each group of particle with all particle sizes. (Each group includs all partile sizes)
   *
   * For \f$m_p^0\f$:
   * In Zodiac, different particle size distributions could be given: Identity, UserDefinedList, Uniform, Normal, LogNormal and Weibull.
   * If the particle size distribution is Identity, Uniform, Normal, LogNormal or Weibull, the number of particles with different sizes
   * are the same. The list of particle number ratio (`numList` used in the code) is \f$[1,1,1,...]\f$ with size of number of particle sizes.
   * \f[
   *    \f$m_p^0\f$ = \sum_{i=1}^{nsize} m_{p,i}^0
   * \f]
   * Here, \f$ nsize\f$ is the number of particle sizes.
   *       \f$ m_{p,i}^0 \f$ is the mass of each particles with ith size.
   *
   * If the particle size distribution is UserDefinedList, the list of particle number ratio  is given in the input file,
   *
   *\f[
   *   m_p^0 = = \sum_{i=1}^{nsize} n_i m_{p,i}^0
   * \f]
   * So, the final expression for number of particle per gas mass with ith size is:
   * \f[
   *    n_{p,i} = \frac{r \left(\frac{m_p}{m_g}\right)_{st}}{m_p^0} n_i
   * \f]
   * Here, \f$n_i\f$ is the ith number in the list.
   */

    template< typename FieldT >
    class CoalParticleLoading: public Expr::Expression<FieldT> {
        const double yvolGiven_, ycharGiven_;
        DECLARE_FIELDS(FieldT, yO2_, faEquivRatio_)
        DECLARE_VECTOR_OF_FIELDS(FieldT, sizePs_)
        DECLARE_VECTOR_OF_FIELDS(FieldT, rhoPs_)
        const Coal::CoalComposition coalComp_;
        const std::vector<double> numList_;
        const Dev::DevModel devModel_;
        const Char::CharModel charModel_;
        const double yC_, yH_, yO_, yvol_, ychar_, c0_;
        double  yC_vol_, yH_vol_, yO_vol_;

        CoalParticleLoading( const Expr::Tag yO2Tag,
                             const Expr::Tag faEquivRatioTag,
                             const Expr::TagList rhoPartTags,
                             const Expr::TagList sizePartTags,
                             const std::vector<double> numList,
                             const Coal::CoalComposition coalComp,
                             const double yvolGiven,
                             const double ycharGiven,
                             const Dev::DevModel devModel,
                             const Char::CharModel charModel)
              : Expr::Expression<FieldT>(),
                    numList_(numList),
                    coalComp_(coalComp),
                    yvolGiven_(yvolGiven),
                    ycharGiven_(ycharGiven),
                    devModel_(devModel),
                    charModel_(charModel),
                    yC_(coalComp.get_C()),
                    yH_(coalComp.get_H()),
                    yO_(coalComp.get_O()),
                    yvol_(coalComp.get_vm()),
                    ychar_(coalComp.get_fixed_c()),
                    c0_(CPD::c0_fun( coalComp.get_C(), coalComp.get_O())){
          this->set_gpu_runnable( true );
          yO2_          = this->template create_field_request<FieldT>( yO2Tag          );
          faEquivRatio_ = this->template create_field_request<FieldT>( faEquivRatioTag );
          this->template create_field_vector_request<FieldT>( rhoPartTags, rhoPs_);
          this->template create_field_vector_request<FieldT>( sizePartTags, sizePs_);

          // yC, yH and yO are given based on daf.
          if( devModel_ == Dev::OFF or devModel_ == Dev::CPDM){
            yC_vol_ = (yC_*(yvol_+ychar_)-(ychar_+yvol_*c0_)) / (yvol_*(1-c0_));
            yH_vol_ = yH_*(yvol_+ychar_) / (yvol_*(1-c0_));
            yO_vol_ = yO_*(yvol_+ychar_) / (yvol_*(1-c0_));
          }
          else{
            yC_vol_ = (yC_*(yvol_+ychar_)-ychar_)/yvol_;
            yH_vol_ = yH_*(yvol_+ychar_)/yvol_;
            yO_vol_ = yO_*(yvol_+ychar_)/yvol_;
          }
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag yO2Tag_, faEquivRatioTag_;
            const Expr::TagList sizePTags_, rhoPTags_;
            const Coal::CoalComposition coalComp_;
            const double yvolGiven_, ycharGiven_;
            const std::vector<double> numList_;
            const Dev::DevModel devModel_;
            const Char::CharModel charModel_;
        public:
          /**
           * @brief The mechanism for building a CoalParticleLoading object
           * @tparam FieldT
           * @param particleLoadingTags number of particle per gas mass
           * @param yO2Tag mass fraction of O2 in the gas phase
           * @param faEquivRatioTag fuel-air equivalence ration
           * @param rhoPartTags particle density
           * @param sizePartTags particle size(diameter)
           * @param numList list of particle numbers ratio with different sizes
           * @param coalComp coal composition
           * @param yvolGiven volatile mass fraction in each particle
           * @param ycharGiven char mass fraction in each particle
           * @param devModel Devolatilization model
           * @param charModel Char reaction model
           */
            Builder( const Expr::TagList particleLoadingTags,
                     const Expr::Tag yO2Tag,
                     const Expr::Tag faEquivRatioTag,
                     const Expr::TagList rhoPartTags,
                     const Expr::TagList sizePartTags,
                     const std::vector<double> numList,
                     const Coal::CoalComposition coalComp,
                     const double yvolGiven,
                     const double ycharGiven,
                     const Dev::DevModel devModel,
                     const Char::CharModel charModel)
                  : ExpressionBuilder( particleLoadingTags ),
                    yO2Tag_(yO2Tag),
                    faEquivRatioTag_(faEquivRatioTag),
                    rhoPTags_(rhoPartTags),
                    sizePTags_(sizePartTags),
                    numList_(numList),
                    coalComp_(coalComp),
                    yvolGiven_(yvolGiven),
                    ycharGiven_(ycharGiven),
                    devModel_(devModel),
                    charModel_(charModel){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new CoalParticleLoading<FieldT>( yO2Tag_, faEquivRatioTag_, rhoPTags_, sizePTags_, numList_, coalComp_, yvolGiven_, ycharGiven_, devModel_, charModel_ );
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();
          const FieldT& yO2 = yO2_->field_ref();
          const FieldT& equratio = faEquivRatio_->field_ref();

          SpatFldPtr<FieldT> required_ox = SpatialFieldStore::get<FieldT, FieldT>( yO2 ); // assume m_p=1
          if( charModel_ == Char::OFF and devModel_ != Dev::OFF){
            *required_ox <<= (yvolGiven_*yC_vol_/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / yO2;
          }
          else if( charModel_ != Char::OFF and devModel_ == Dev::OFF){
            *required_ox <<= ycharGiven_/12.0 * 32.0 / yO2;
          }
          else{
            *required_ox <<= ((yvolGiven_*yC_vol_+ycharGiven_)/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / yO2;
          }

          SpatFldPtr<FieldT> massPart = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          *massPart <<= 0.0;
          for(size_t i=0; i<numList_.size(); i++){
            const FieldT& sizePi = sizePs_[i]->field_ref();
            const FieldT& rhoPi = rhoPs_[i]->field_ref();
            *massPart <<= *massPart + (M_PI/6*sizePi*sizePi*sizePi*rhoPi) * numList_[i];
          }
          for(size_t i=0; i<numList_.size(); i++){
            *results[i] <<= 1/ *required_ox * equratio/ *massPart* numList_[i];
          }
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          const Expr::TagList& targetTags = this->get_tags();
          typename Expr::Expression<FieldT>::ValVec& mvcharrhs = this->get_value_vec();
          std::vector<FieldT > dresultsdv;
          for(size_t i=0;i<targetTags.size();++i){
            dresultsdv.push_back(this->sensitivity_result(targetTags[i], sensVarTag));
          }

          const FieldT& yO2 = yO2_->field_ref();
          const FieldT& equratio = faEquivRatio_->field_ref();
          const FieldT& dyO2dv = yO2_->sens_field_ref(sensVarTag);
          const FieldT& dequratiodv = faEquivRatio_->sens_field_ref(sensVarTag);

          SpatFldPtr<FieldT> required_ox = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          SpatFldPtr<FieldT> drequired_oxdv = SpatialFieldStore::get<FieldT, FieldT>( yO2 );

          if( charModel_ == Char::OFF and devModel_ != Dev::OFF){
            *required_ox <<= (yvolGiven_*yC_vol_/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / yO2;
            *drequired_oxdv <<= - (yvolGiven_*yC_vol_/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / square(yO2) * dyO2dv;
          }
          else if( charModel_ != Char::OFF and devModel_ == Dev::OFF){
            *required_ox <<= ycharGiven_/12.0 * 32.0 / yO2;
            *drequired_oxdv <<= -ycharGiven_/12.0*32.0/square(yO2) * dyO2dv;
          }
          else{
            *required_ox <<= ((yvolGiven_*yC_vol_+ycharGiven_)/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / yO2;
            *drequired_oxdv <<= - ((yvolGiven_*yC_vol_+ycharGiven_)/12.0 + yvolGiven_*yH_vol_*0.25 - yvolGiven_*yO_vol_/16.0*0.5) * 32.0 / square(yO2) * dyO2dv;
          }

          SpatFldPtr<FieldT> massPart = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          SpatFldPtr<FieldT> dmassPartdv = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          *massPart <<= 0.0;
          *dmassPartdv <<= 0.0;
          for(size_t i=0; i<numList_.size(); i++){
            const FieldT& sizePi = sizePs_[i]->field_ref();
            const FieldT& rhoPi = rhoPs_[i]->field_ref();
            const FieldT& dsizePidv = sizePs_[i]->sens_field_ref( sensVarTag );
            const FieldT& drhoPidv = rhoPs_[i]->sens_field_ref( sensVarTag);
            *massPart <<= *massPart + (M_PI/6*sizePi*sizePi*sizePi*rhoPi) * numList_[i];
            *dmassPartdv <<= *dmassPartdv + M_PI/6*numList_[i] * (3*square(sizePi)*dsizePidv*rhoPi + sizePi*sizePi*sizePi*drhoPidv);
          }

          for(size_t i=0; i<numList_.size(); i++){
            dresultsdv[i] <<= dequratiodv * numList_[i] / (*required_ox * *massPart)
                              - equratio*numList_[i] / square(*required_ox* *massPart) * (*drequired_oxdv* *massPart + *required_ox * *dmassPartdv);
          }
        }
    };
} // namespace Coal

#endif // CoalParticleLoading_coal_h
