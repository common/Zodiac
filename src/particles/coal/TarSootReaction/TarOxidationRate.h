
/**
 * @file TarOxidationRate.h
 * @par Calculates the rate of tar oxidation
 * @author Hang Zhou
 */
#ifndef TarOxidationRate_h
#define TarOxidationRate_h

#include <expression/Expression.h>

namespace Tarsoot {
/**
 *  @class TarOxidationRate
 *  @brief Calculates the rate of tar oxidation, which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass and tar mass per gas mass (for constant pressure) or per volume (for constant volume).
 *
 *  The rate of tar oxidation is given as
 *  For constant volume condition: (kg/m^3/s)
 * \f[
 *     r_{\mathrm{O,tar}} = (\rho_g m_{\mathrm{tar}})(\rho_g Y_{O_2})A_{O,\mathrm{tar}} * exp(-E_{O,\mathrm{tar}} / RT),
 * \f]
 *  For constant pressure condition: (1/s)
 * \f[
 *     r_{\mathrm{O,tar}} = \rho_g m_{\mathrm{tar}} Y_{O_2} A_{O,\mathrm{tar}} * exp(-E_{O,\mathrm{tar}} / RT),
 * \f]
 *  Here \f$ m_\mathrm{tar} \f$ is the mass of tar per gas mass.
 *       \f$ \rho_g \f$ is the gas density.
 *       \f$ Y_{O_2} \f$ is mass fraction of O2.
 *       \f$ A_{O,\mathrm{tar}} \f$ is Arrhenius preexponential factor(m^3/kg-s). [1,2]
 *       \f$ E_{O,\mathrm{tar} \f$ is activation energy(J/mol) . [1,2]
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *  [2]    Josephson, Alexander J.; Lignell, David O.; Brown, Alexander L.; and Fletcher, Thomas H.
 *        "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 */

  template<typename FieldT>
  class TarOxidationRate
        : public Expr::Expression<FieldT> {
    DECLARE_FIELDS( FieldT, density_, yO2_, massTar_, temp_ )
    const double A_, E_, gasCon_;
    const std::string reactorType_;

    TarOxidationRate( const Expr::Tag& densityTag,
                      const Expr::Tag& yO2Tag,
                      const Expr::Tag& massTarTag,
                      const Expr::Tag& tempTag,
                      const std::string& reactorType)
          : Expr::Expression<FieldT>(),
            reactorType_( reactorType ),
            A_     ( 6.77e5    ),
            E_     ( 52.3e3    ),
            gasCon_( 8.3144621 )
    {
      density_ = this->template create_field_request<FieldT>( densityTag );
      yO2_     = this->template create_field_request<FieldT>( yO2Tag     );
      massTar_ = this->template create_field_request<FieldT>( massTarTag );
      temp_    = this->template create_field_request<FieldT>( tempTag    );
    }

  public:
    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag densityTag_, yO2Tag_, massTarTag_, tempTag_;
      const std::string reactorType_;
    public:
      /**
       *  @brief Build a TarOxidationRate expression
       *  @tparam FieldT
       *  @param resultTag tar oxidation rate
       *  @param densityTag gas density
       *  @param yO2Tag mass fraction of O2 in gas phase
       *  @param massTarTag mass of tar per gas mass
       *  @param tempTag gas temperature
       *  @param reactorType reactor type
       */
      Builder( const Expr::Tag& resultTag,
               const Expr::Tag& densityTag,
               const Expr::Tag& yO2Tag,
               const Expr::Tag& massTarTag,
               const Expr::Tag& tempTag,
               const std::string reactorType)
            : ExpressionBuilder( resultTag ),
              densityTag_( densityTag ),
              yO2Tag_     ( yO2Tag    ),
              massTarTag_ ( massTarTag),
              tempTag_    ( tempTag   ),
              reactorType_( reactorType ){}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new TarOxidationRate<FieldT>( densityTag_, yO2Tag_, massTarTag_, tempTag_, reactorType_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& result = this->value();

      const FieldT& density = density_->field_ref();
      const FieldT& yO2     = yO2_    ->field_ref();
      const FieldT& massTar = massTar_->field_ref();
      const FieldT& temp    = temp_   ->field_ref();

      if( reactorType_=="PSRConstantVolume"){
        result <<=  cond( massTar <= 0.0, 0.0)
              ( - square(density) * massTar * yO2 * A_ * exp( -E_ / ( gasCon_* temp ) )); // negative
      }
      else{
        result <<=  cond( massTar <= 0.0, 0.0)
              ( - massTar * density * yO2 * A_ * exp( -E_ / ( gasCon_* temp ) )); // negative
      }

    }
    void sensitivity( const Expr::Tag& sensVarTag ){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( sensVarTag );
      const FieldT& density = density_->field_ref();
      const FieldT& yO2     = yO2_    ->field_ref();
      const FieldT& massTar = massTar_   ->field_ref();
      const FieldT& temp    = temp_   ->field_ref();
      const FieldT& ddensitydv = density_->sens_field_ref(sensVarTag);
      const FieldT& dyO2dv     = yO2_    ->sens_field_ref(sensVarTag);
      const FieldT& dmassTardv = massTar_->sens_field_ref(sensVarTag);
      const FieldT& dtempdv    = temp_   ->sens_field_ref(sensVarTag);

      if( reactorType_=="PSRConstantVolume"){
        dfdv<<= cond( massTar <= 0.0, 0.0 )
              ( - ( square(density) * dmassTardv * yO2 + 2 * density * ddensitydv * massTar * yO2 + square(density) * massTar * dyO2dv)
                * A_ * exp( -E_ / ( gasCon_* temp ) )
                - massTar * square(density) * yO2 * A_ * exp( -E_ / ( gasCon_* temp ) )
                  * E_ / (gasCon_ * temp * temp) * dtempdv );
      }
      else{
        dfdv<<= cond( massTar <= 0.0, 0.0 )
              ( - ( dmassTardv * density * yO2 + massTar * ddensitydv * yO2 + massTar * density * dyO2dv)
                * A_ * exp( -E_ / ( gasCon_* temp ) )
                - massTar * density * yO2 * A_ * exp( -E_ / ( gasCon_* temp ) )
                  * E_ / (gasCon_ * temp * temp) * dtempdv );
      }

    }
  };
}

#endif // TarOxidationRate_h
