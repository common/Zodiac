/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TarSootToGasTerm_Expr_h
#define TarSootToGasTerm_Expr_h

#include <expression/Expression.h>
namespace Tarsoot {

    /**
     * @class GasHeatSourceFromTarSoot
     * @ingroup Tarsoot
     * @author Hang
     * @brief Calculating the heat source term from tar and soot reaction to gas phase.
     *        It includes two part: gas transporation and reaction heat
     *
     */
    template<typename FieldT>
    class GasHeatSourceFromTarSoot: public Expr::Expression<FieldT> {
        DECLARE_FIELDS( FieldT, o2Consum_, coProd_, h2oProd_, h2Prod_, o2Enth_, coEnth_, h2oEnth_, h2Enth_);
        DECLARE_FIELDS( FieldT, tarOxid_, sootOxid_);
        const double deltaEtarOx_, deltaEsootOx_;
        const TarAndSootInfo& tarSootInfo_;

        GasHeatSourceFromTarSoot( const Expr::Tag o2ConsumTotalTag,
                                  const Expr::Tag coProdTotalTag,
                                  const Expr::Tag h2oProdTotalTag,
                                  const Expr::Tag h2ProdTotalTag,
                                  const Expr::Tag o2EnthTgasTag,
                                  const Expr::Tag coEnthTgasTag,
                                  const Expr::Tag h2oEnthTgasTag,
                                  const Expr::Tag h2EnthTgasTag,
                                  const Expr::Tag tarOxidTag,
                                  const Expr::Tag sootOxidTag,
                                  const TarAndSootInfo& tarSootInfo)
              : Expr::Expression<FieldT>(),
                tarSootInfo_(tarSootInfo),
                deltaEtarOx_(tarSootInfo.tar_heat_of_oxidation()),
                deltaEsootOx_(tarSootInfo.soot_heat_of_oxidation())
        {
          this->set_gpu_runnable( true );
          o2Consum_ = this->template create_field_request<FieldT>( o2ConsumTotalTag );
          coProd_   = this->template create_field_request<FieldT>( coProdTotalTag   );
          h2oProd_  = this->template create_field_request<FieldT>( h2oProdTotalTag  );
          h2Prod_   = this->template create_field_request<FieldT>( h2ProdTotalTag   );
          o2Enth_   = this->template create_field_request<FieldT>( o2EnthTgasTag    );
          coEnth_   = this->template create_field_request<FieldT>( coEnthTgasTag    );
          h2oEnth_  = this->template create_field_request<FieldT>( h2oEnthTgasTag   );
          h2Enth_   = this->template create_field_request<FieldT>( h2EnthTgasTag    );
          tarOxid_  = this->template create_field_request<FieldT>( tarOxidTag       );
          sootOxid_ = this->template create_field_request<FieldT>( sootOxidTag      );
        }

    public:

        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag o2ConsumTag_, coProdTag_, h2oProdTag_, h2ProdTag_, o2EnthTag_, coEnthTag_,
                            h2oEnthTag_, h2EnthTag_, tarOxidTag_, sootOxidTag_;
            const TarAndSootInfo& tarSootInfo_;
        public:
          /**
           * @brief The mechanism for building a GasHeatSourceFromTarSoot object
           * @param resultTag heat source term from tar and soot reaction to gas phase
           * @param o2ConsumTotalTag consumption rate of O2 by tar and soot
           * @param coProdTotalTag  production rate of CO by tar and soot
           * @param h2oProdTotalTag production rate of H2O by tar and soot
           * @param h2ProdTotalTag production rate of H2 by tar and soot
           * @param o2EnthTgasTag enthaply of O2 at gas temperature
           * @param coEnthTgasTag enthaply of CO at gas temperature
           * @param h2oEnthTgasTag enthaply of H2O at gas temperature
           * @param h2EnthTgasTag enthaply of H2 at gas temperature
           * @param tarOxidTag tar oxidation rate
           * @param sootOxidTag soot oxidation rate
           * @param tarSootInfo TarAndSootInfo
           */
            Builder( const Expr::Tag resultTag,
                     const Expr::Tag o2ConsumTotalTag,
                     const Expr::Tag coProdTotalTag,
                     const Expr::Tag h2oProdTotalTag,
                     const Expr::Tag h2ProdTotalTag,
                     const Expr::Tag o2EnthTgasTag,
                     const Expr::Tag coEnthTgasTag,
                     const Expr::Tag h2oEnthTgasTag,
                     const Expr::Tag h2EnthTgasTag,
                     const Expr::Tag tarOxidTag,
                     const Expr::Tag sootOxidTag,
                     const TarAndSootInfo& tarSootInfo)
                  : Expr::ExpressionBuilder( resultTag ),
                    o2ConsumTag_           ( o2ConsumTotalTag),
                    coProdTag_             ( coProdTotalTag  ),
                    h2oProdTag_            ( h2oProdTotalTag ),
                    h2ProdTag_             ( h2ProdTotalTag  ),
                    o2EnthTag_             ( o2EnthTgasTag   ),
                    coEnthTag_             ( coEnthTgasTag   ),
                    h2oEnthTag_            ( h2oEnthTgasTag  ),
                    h2EnthTag_             ( h2EnthTgasTag   ),
                    tarOxidTag_            ( tarOxidTag      ),
                    sootOxidTag_           ( sootOxidTag     ),
                    tarSootInfo_           ( tarSootInfo     ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new GasHeatSourceFromTarSoot<FieldT>( o2ConsumTag_, coProdTag_, h2oProdTag_, h2ProdTag_, o2EnthTag_,
                                                           coEnthTag_, h2oEnthTag_, h2EnthTag_, tarOxidTag_, sootOxidTag_,
                                                           tarSootInfo_);
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          result <<= o2Consum_->field_ref() * o2Enth_->field_ref()
                   + coProd_->field_ref() * coEnth_->field_ref()
                   + h2oProd_->field_ref() * h2oEnth_->field_ref()
                   + h2Prod_->field_ref() * h2Enth_->field_ref()
                   + tarOxid_->field_ref() * deltaEtarOx_
                   + sootOxid_->field_ref() * deltaEsootOx_;
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= o2Consum_->sens_field_ref(sensVarTag) * o2Enth_->field_ref() + o2Consum_->field_ref() * o2Enth_->sens_field_ref(sensVarTag)
                 + coProd_->sens_field_ref(sensVarTag) * coEnth_->field_ref() + coProd_->field_ref() * coEnth_->sens_field_ref(sensVarTag)
                 + h2oProd_->sens_field_ref(sensVarTag) * h2oEnth_->field_ref() + h2oProd_->field_ref() * h2oEnth_->sens_field_ref(sensVarTag)
                 + h2Prod_->sens_field_ref(sensVarTag) * h2Enth_->field_ref() + h2Prod_->field_ref() * h2Enth_->sens_field_ref(sensVarTag)
                 + tarOxid_->sens_field_ref(sensVarTag) * deltaEtarOx_ + sootOxid_->sens_field_ref(sensVarTag) * deltaEsootOx_;
        }
    };


    template< typename FieldT >
    class MassTarOverRho : public Expr::Expression<FieldT>
    {
        DECLARE_FIELDS( FieldT, rho_, massTar_);

        MassTarOverRho( const Expr::Tag rhoTag,
                        const Expr::Tag massTarTag )
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable(true);
          rho_     = this->template create_field_request<FieldT>( rhoTag     );
          massTar_ = this->template create_field_request<FieldT>( massTarTag );
        }

        /**
         * @brief The mechanism for building a MassFracsOverRho object
         * @tparam FieldT
         * @param resultTag Result of MassTarOverRho: \f$ - \frac{Y_{tar}}{\rho} \f$
         * @param rhoTag Density of gas phase
         * @param massTarTag mass fraction of tar in gas phase
         */
    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag rhoTag_, massTarTag_;
        public:
            Builder( const Expr::Tag resultTag,
                     const Expr::Tag rhoTag,
                     const Expr::Tag massTarTag ) :
                  Expr::ExpressionBuilder( resultTag ),
                  rhoTag_( rhoTag ),
                  massTarTag_( massTarTag )
            {}
            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new MassTarOverRho<FieldT>( rhoTag_, massTarTag_ );
            }
        };

        void evaluate()
        {
          using namespace SpatialOps;
          this->value() <<= - massTar_->field_ref() / rho_->field_ref();
        }
    };
}
//--------------------------------------------------------------------


#endif // TarSootToGasTerm_Expr_h
