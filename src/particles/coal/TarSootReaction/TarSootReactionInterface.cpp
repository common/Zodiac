/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>

#include <pokitt/CanteraObjects.h>
#include <pokitt/thermo/Enthalpy.h>
#include <pokitt/SpecificToVolumetric.h>
#include <pokitt/VolumetricToSpecific.h>
#include <fstream>

#include <expression/Functions.h>
#include <expression/ExpressionFactory.h>

#include "ReactorEnsembleUtil.h"
#include "TransformationMatrixExpressions.h"
#include "ZeroDimMixing.h"
#include "SumOp.h"
#include "particles/ParticleTransformExpressions.h"
#include "particles/ParticleInterface.h"
#include "particles/coal/CoalInterface.h"

#include "TarSootReactionInterface.h"
#include "TarOxidationRate.h"
#include "TarGasificationRate.h"
#include "SootFormationRate.h"
#include "SootOxidationRate.h"
#include "DerivedRxnTerms.h"
#include "TarSootTransformExpression.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >( #name );
using Expr::matrix::sensitivity;

namespace Tarsoot {

  typedef Expr::LinearFunction<FieldT>::Builder LinearT;
  typedef Expr::PlaceHolder<FieldT>::Builder PlaceHolderT;
  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;

  typedef pokitt::SpeciesEnthalpy<FieldT>::Builder SpecEnthT;
  typedef pokitt::VolumetricToSpecific <FieldT>::Builder PhiT;

  typedef ReactorEnsembleUtil::SumOp<FieldT>::Builder SumOpT;
  typedef ReactorEnsembleUtil::ZeroDMixing<FieldT>::Builder ZeroDMixingT;
  typedef ReactorEnsembleUtil::ZeroDMixingCP<FieldT>::Builder ZeroDMixingCPT;
  typedef ReactorEnsembleUtil::ProductExpression<FieldT>::Builder ProductT;
  typedef ReactorEnsembleUtil::DivisionExpression<FieldT>::Builder DivisionT;

  typedef Particles::SourceFromGasMassChange<FieldT>::Builder SourceFromGasMassT;
  typedef Particles::DpDxSourceFromGasEnthalpyChange<FieldT>::Builder DpDxEnthalpyT;

  typedef Tarsoot::TarOxidationRate<FieldT>::Builder TarOxiT;
  typedef Tarsoot::TarGasificationRate<FieldT>::Builder TarGasiT;
  typedef Tarsoot::SootFormationRate<FieldT>::Builder SootFormT;
  typedef Tarsoot::SootOxidationRate<FieldT>::Builder SootOxidT;
  typedef Tarsoot::DerivedRxnRate<FieldT>::Builder DerivedRxnRateT;
  typedef Tarsoot::GasHeatSourceFromTarSoot<FieldT>::Builder GasHeatSourceTarSootT;
  typedef Tarsoot::MassTarOverRho<FieldT>::Builder MassTarOverRhoT;

  //---------------------------------------------------------------------------
  TarSootTags::
  TarSootTags( const Expr::Context& state,
               const std::vector<std::string>& particleNumArray)
        : tarGasMassTag                  ( Expr::Tag( "tar_gas_mass"                                   , state ) ),
          tarGasInflowTag                ( Expr::Tag( "tar_gas_mass_inflow"                            , state ) ),
          tarGasSrcFromGasSrcTag         ( Expr::Tag( "tar_gas_src_from_gas_src"                       , state ) ),
          tarGasMixRhsTag                ( Expr::Tag( "tar_gas_mass_mix_rhs"                           , state ) ),
          tarGasKinRhsTag                ( Expr::Tag( "tar_gas_mass_kin_rhs"                           , state ) ),
          tarGasFullRhsTag               ( Expr::Tag( "tar_gas_mass_full_rhs"                          , state ) ),
          rhotarGasMassTag               ( Expr::Tag( "rho_tar_gas_mass"                               , state ) ),
          rhotarGasInflowTag             ( Expr::Tag( "rho_tar_gas_mass_inflow"                        , state ) ),
          rhotarGasMixRhsTag             ( Expr::Tag( "rho_tar_gas_mass_mix_rhs"                       , state ) ),
          rhotarGasKinRhsTag             ( Expr::Tag( "rho_tar_gas_mass_kin_rhs"                       , state ) ),
          rhotarGasFullRhsTag            ( Expr::Tag( "rho_tar_gas_mass_full_rhs"                      , state ) ),
          sootMassTag                    ( Expr::Tag( "soot_mass"                                      , state ) ),
          sootInflowTag                  ( Expr::Tag( "soot_mass_inflow"                               , state ) ),
          sootSrcFromGasSrcTag           ( Expr::Tag( "soot_src_from_gas_src"                          , state ) ),
          sootMixRhsTag                  ( Expr::Tag( "soot_mass_mix_rhs"                              , state ) ),
          sootKinRhsTag                  ( Expr::Tag( "soot_mass_kin_rhs"                              , state ) ),
          sootFullRhsTag                 ( Expr::Tag( "soot_mass_full_rhs"                             , state ) ),
          rhosootMassTag                 ( Expr::Tag( "rho_soot_mass"                                  , state ) ),
          rhosootInflowTag               ( Expr::Tag( "rho_soot_mass_inflow"                           , state ) ),
          rhosootMixRhsTag               ( Expr::Tag( "rho_soot_mass_mix_rhs"                          , state ) ),
          rhosootKinRhsTag               ( Expr::Tag( "rho_soot_mass_kin_rhs"                          , state ) ),
          rhosootFullRhsTag              ( Expr::Tag( "rho_soot_mass_full_rhs"                         , state ) ),
          o2EnthTgasTag                  ( Expr::Tag( "o2_enth_Tgas"                                   , state ) ),
          coEnthTgasTag                  ( Expr::Tag( "co_enth_Tgas"                                   , state ) ),
          h2oEnthTgasTag                 ( Expr::Tag( "h2o_enth_Tgas"                                  , state ) ),
          h2EnthTgasTag                  ( Expr::Tag( "h2_enth_Tgas"                                   , state ) ),
          o2ConsumTotalTag               ( Expr::Tag( "o2_consum_total"                                , state ) ),
          coProdTotalTag                 ( Expr::Tag( "co_prod_total"                                  , state ) ),
          h2oProdTotalTag                ( Expr::Tag( "h2o_prod_total"                                 , state ) ),
          o2ConsumTotalPFRTag            ( Expr::Tag( "o2_consum_total_pfr"                            , state ) ),
          coProdTotalPFRTag              ( Expr::Tag( "co_prod_total_pfr"                              , state ) ),
          h2oProdTotalPFRTag             ( Expr::Tag( "h2o_prod_total_pfr"                             , state ) ),
          tarSootMassGasSourceTag        ( Expr::Tag( "tar_soot_mass_gas_source"                       , state ) ),
          tarSootHeatGasSourceTag        ( Expr::Tag( "tar_soot_heat_gas_source"                       , state ) ),
          tarSootHeatGasSourcePFRTag     ( Expr::Tag( "tar_soot_heat_gas_source_pfr"                   , state ) ),
          tarSootHeatGasSourceDpDxTag    ( Expr::Tag( "tar_soot_heat_gas_source_dpdx"                  , state ) ),
          tarGasifTag                    ( Expr::Tag( "tar_gasif"                                      , state ) ),
          tarOxidTag                     ( Expr::Tag( "tar_oxid"                                       , state ) ),
          tarToSootTag                   ( Expr::Tag( "tar_to_soot"                                    , state ) ),
          sootFormTag                    ( Expr::Tag( "soot_form"                                      , state ) ),
          sootAggTag                     ( Expr::Tag( "soot_agg"                                       , state ) ),
          sootOxidTag                    ( Expr::Tag( "soot_oxid"                                      , state ) ),
          o2ConsumptionTarTag            ( Expr::Tag( "o2_consump_tar"                                 , state ) ),
          coProductionTarTag             ( Expr::Tag( "co_product_tar"                                 , state ) ),
          h2oProductionTarTag            ( Expr::Tag( "h2o_product_tar"                                , state ) ),
          h2ProductionTarTag             ( Expr::Tag( "h2_product_tar"                                 , state ) ),
          h2ProductionTarPFRTag          ( Expr::Tag( "h2_product_tar_pfr"                             , state ) ),
          o2ConsumptionSootTag           ( Expr::Tag( "o2_consump_soot"                                , state ) ),
          coProductionSootTag            ( Expr::Tag( "co_product_soot"                                , state ) ),
          h2oProductionSootTag           ( Expr::Tag( "h2o_product_soot"                               , state ) ),
          tarPartialRhoTag               ( Expr::Tag( "(partial tarMass)/(partial rho)"                , state ) ),
          sootPartialRhoTag              ( Expr::Tag( "(partial sootMass)/(partial rho)"               , state ) ),
          rhotarMixPartialrhoTag         ( Expr::Tag( "(partial rhotarMass_mix_rhs)/(partial rho)"     , state ) ),
          rhotarMixPartialtarTag         ( Expr::Tag( "(partial rhotarMass_mix_rhs)/(partial tar)"     , state ) ),
          rhosootMixPartialrhoTag        ( Expr::Tag( "(partial rhosoot_mix_rhs)/(partial rho)"        , state ) ),
          rhosootMixPartialsootTag       ( Expr::Tag( "(partial rhosoot_mix_rhs)/(partial soot)"       , state ) ),
          tarSootYoxidizerTag            ( Expr::Tag( "tar_soot_yoxidizer"                             , state ) )
  {
    rhovolTarTags = Expr::tag_list( particleNumArray, state, "rho_volTar_"  );
  }

  TarSootInterface::
  TarSootInterface( const Coal::CoalType coalType,
                    const TarAndSootInfo& tarSootInfo,
                    std::vector<std::string> particleNumArray,
                    std::vector<std::string> SpeciesArray,
                    std::vector<std::string> partNumSpeciesArray,
                    const YAML::Node& rootParser,
                    std::set<Expr::ExpressionID>& initRoots,
                    Expr::ExpressionFactory& initFactory,
                    Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                    Expr::TagList& primitiveTags,
                    Expr::TagList& kinRhsTags,
                    int& equIndex,
                    const bool isRestart)
        : coaltype_(coalType),
          tarSootInfo_(tarSootInfo),
          particleNumArray_(particleNumArray),
          speciesArray_(SpeciesArray),
          partNumSpeciesArray_(partNumSpeciesArray),
          rootParser_(rootParser),
          initRoots_(initRoots),
          initFactory_(initFactory),
          integrator_(integrator),
          primitiveTags_(primitiveTags),
          kinRhsTags_(kinRhsTags),
          equIndex_(equIndex),
          isRestart_(isRestart),
          haveSoot_(rootParser["Particles"]["SootIsCarbon"])
  {
    reactorType_ = rootParser_["ReactorParameters"]["ReactorType"].as<std::string>();
    if(!isRestart_){
      setup_initial_conditions_tarsoot();
    }

    build_equation_system_coal_tarsoot();
    modify_prim_kinrhs_tags_tarsoot();
  }

  //---------------destructor---------------------------
  TarSootInterface::~TarSootInterface()
  {
    std::cout << "Delete ~TarSootInterface(): " << this << std::endl;
  }


  //---------------------------------------------------------------------------
  void TarSootInterface::setup_initial_conditions_tarsoot(){
    const ReactorEnsembleUtil::ReactorEnsembleTags tags ( Expr::STATE_N);
    const Tarsoot::TarSootTags tagsTarsoot( Expr::STATE_N, particleNumArray_ );
    double tarInflow, sootInflow, tarInit, sootInit;
    const std::vector<double> defaultTarSoot = {0.0, 0.0};
    if(reactorType_!="PFR"){
      tarInflow  = rootParser_["Particles"]["TarSootInflowFrac"].as<std::vector<double>>(defaultTarSoot)[0];
      sootInflow = rootParser_["Particles"]["TarSootInflowFrac"].as<std::vector<double>>(defaultTarSoot)[1];
    }
    tarInit    = rootParser_["Particles"]["TarSootInitFrac"].as<std::vector<double>>(defaultTarSoot)[0];
    sootInit   = rootParser_["Particles"]["TarSootInitFrac"].as<std::vector<double>>(defaultTarSoot)[1];

    if( reactorType_ == "PSRConstantVolume" ){
      initRoots_.insert( initFactory_.register_expression( new LinearT( tagsTarsoot.rhotarGasMassTag, tags.rhoTag, tarInit, 0.0)));
      if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression(new LinearT( tagsTarsoot.rhotarGasInflowTag, tags.rhoInflowTag, tarInflow, 0.0 )));}
      if(haveSoot_){
        initRoots_.insert( initFactory_.register_expression( new LinearT( tagsTarsoot.rhosootMassTag, tags.rhoTag, sootInit, 0.0)));
        if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression(new LinearT( tagsTarsoot.rhosootInflowTag, tags.rhoInflowTag, sootInflow, 0.0 )));}
      }
    }
    else{
      initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsTarsoot.tarGasMassTag, tarInit )));
      if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsTarsoot.tarGasInflowTag,tarInflow ) ) );}
      if(haveSoot_){
        initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsTarsoot.sootMassTag, sootInit )));
        if(reactorType_!="PFR"){ initRoots_.insert( initFactory_.register_expression( new ConstantT( tagsTarsoot.sootInflowTag,sootInflow ) ) );}
      }
    }
  }

  //---------------------------------------------------------------------------
  void TarSootInterface::build_equation_system_coal_tarsoot(){

    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const Particles::ParticleEnsembleTags tagsEPart( Expr::STATE_NONE, particleNumArray_ );
    const Coal::CoalEnsembleTags tagsECoal( Expr::STATE_NONE, particleNumArray_ );
    const Dev::DevTags tagsEDev( Expr::STATE_NONE, particleNumArray_, speciesArray_, partNumSpeciesArray_ );
    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

    const int nparsize = particleNumArray_.size();
    const int IdxO2 = CanteraObjects::species_index( "O2" );
    const int IdxCO = CanteraObjects::species_index( "CO" );
    const int IdxH2O = CanteraObjects::species_index( "H2O" );
    const int IdxH2 = CanteraObjects::species_index( "H2" );
    const double tarMW = tarSootInfo_.tar_mw();
    const double tarO2 = tarSootInfo_.tar_required_moles_O2();
    const double tarCO = tarSootInfo_.tar_produced_moles_CO();
    const double tarH2O = tarSootInfo_.tar_produced_moles_H2O();
    const double tarH2  = tarSootInfo_.tar_produced_moles_H2();
    const double sootMW = tarSootInfo_.soot_mw();
    const double sootO2 = tarSootInfo_.soot_required_moles_O2();
    const double sootCO = tarSootInfo_.soot_produced_moles_CO();
    const double sootH2O = tarSootInfo_.soot_produced_moles_H2O();

    Expr::ExpressionFactory& execFactory = integrator_->factory();

    execFactory.register_expression( new TarOxiT( tagsETarsoot.tarOxidTag, tagsE.rhoTag, tagsE.massTags[IdxO2], tagsETarsoot.tarGasMassTag, tagsE.tempTag, reactorType_ )); // negative
//    execFactory.register_expression( new TarGasiT( tagsETarsoot.tarGasifTag, tagsETarsoot.tarGasMassTag, tagsE.tempTag, tagsE.rhoTag, reactorType )); // negative
    execFactory.register_expression( new ConstantT( tagsETarsoot.tarGasifTag, 0.0 )); // negative

    //modify gas equations
    // calculate the rate at which O2, CO, H2, and H2O source terms due to tar oxidation and soot formation:
    execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.o2ConsumptionTarTag, tarMW, 32.0, 1.0, tarO2, tagsETarsoot.tarOxidTag )); // negative
    execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.coProductionTarTag, tarMW, 28.01, -1.0, tarCO, tagsETarsoot.tarOxidTag )); // positive
    execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.h2oProductionTarTag, tarMW, 18.02, -1.0, tarH2O, tagsETarsoot.tarOxidTag )); // positive
    if(haveSoot_){
      execFactory.register_expression( new SootFormT( Expr::tag_list( tagsETarsoot.tarToSootTag, tagsETarsoot.sootFormTag ), tagsETarsoot.tarGasMassTag, tagsE.tempTag, tagsE.rhoTag,
                                                      tarSootInfo_, reactorType_ ));  // tarToSoot: negative; sootForm: positive
      execFactory.register_expression( new SootOxidT( tagsETarsoot.sootOxidTag, tagsE.massTags[IdxO2], tagsETarsoot.sootMassTag,
                                                      tagsE.presTag, tagsE.tempTag, tagsE.rhoTag, tagsE.mmwTag, reactorType_, tarSootInfo_ ));  //negative
      execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.h2ProductionTarTag, tarMW, 2.016, -1.0, tarH2, tagsETarsoot.tarToSootTag ));// positive
      // Calculate the O2, CO, and H2O source terms due to soot oxidation:
      execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.o2ConsumptionSootTag, sootMW, 32.0, 1.0, sootO2, tagsETarsoot.sootOxidTag )); // negative
      execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.coProductionSootTag, sootMW, 28.01, -1.0, sootCO, tagsETarsoot.sootOxidTag )); // positive
      execFactory.register_expression( new DerivedRxnRateT( tagsETarsoot.h2oProductionSootTag, sootMW, 18.02, -1.0, sootH2O, tagsETarsoot.sootOxidTag )); // positive

      execFactory.register_expression( new SumOpT( tagsETarsoot.o2ConsumTotalTag, {tagsETarsoot.o2ConsumptionTarTag, tagsETarsoot.o2ConsumptionSootTag})); // negative
      execFactory.register_expression( new SumOpT( tagsETarsoot.coProdTotalTag, {tagsETarsoot.coProductionTarTag, tagsETarsoot.coProductionSootTag})); // positive
      execFactory.register_expression( new SumOpT( tagsETarsoot.h2oProdTotalTag, {tagsETarsoot.h2oProductionTarTag, tagsETarsoot.h2oProductionSootTag})); // positive

    }
    else{
      execFactory.register_expression( new ConstantT( tagsETarsoot.tarToSootTag, 0.0));
      execFactory.register_expression( new ConstantT( tagsETarsoot.sootOxidTag, 0.0));
      execFactory.register_expression( new ConstantT( tagsETarsoot.h2ProductionTarTag, 0.0));
      execFactory.register_expression( new LinearT( tagsETarsoot.o2ConsumTotalTag, tagsETarsoot.o2ConsumptionTarTag, 1.0, 0.0)); // negative
      execFactory.register_expression( new LinearT( tagsETarsoot.coProdTotalTag, tagsETarsoot.coProductionTarTag, 1.0, 0.0)); // positive
      execFactory.register_expression( new LinearT( tagsETarsoot.h2oProdTotalTag, tagsETarsoot.h2oProductionTarTag, 1.0, 0.0)); // positive
    }


    execFactory.register_expression( new SumOpT( tagsETarsoot.tarSootMassGasSourceTag, {tagsETarsoot.o2ConsumTotalTag, tagsETarsoot.coProdTotalTag, tagsETarsoot.h2oProdTotalTag,
                                                                                        tagsETarsoot.h2ProductionTarTag}));
    execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootMassGasSourceTag, tagsEPart.partToGasMassTag, Expr::ADD_SOURCE_EXPRESSION);

    execFactory.register_expression( new SpecEnthT( tagsETarsoot.o2EnthTgasTag, tagsE.tempTag, IdxO2 ));
    execFactory.register_expression( new SpecEnthT( tagsETarsoot.coEnthTgasTag, tagsE.tempTag, IdxCO ));
    execFactory.register_expression( new SpecEnthT( tagsETarsoot.h2oEnthTgasTag, tagsE.tempTag, IdxH2O ));
    execFactory.register_expression( new SpecEnthT( tagsETarsoot.h2EnthTgasTag, tagsE.tempTag, IdxH2 ));
    execFactory.register_expression( new GasHeatSourceTarSootT( tagsETarsoot.tarSootHeatGasSourceTag, tagsETarsoot.o2ConsumTotalTag, tagsETarsoot.coProdTotalTag, tagsETarsoot.h2oProdTotalTag,
                                                                tagsETarsoot.h2ProductionTarTag, tagsETarsoot.o2EnthTgasTag, tagsETarsoot.coEnthTgasTag, tagsETarsoot.h2oEnthTgasTag, tagsETarsoot.h2EnthTgasTag,
                                                                tagsETarsoot.tarOxidTag, tagsETarsoot.sootOxidTag, tarSootInfo_));

    if( reactorType_ == "PSRConstantVolume" ){
      // tar mass fraction equation
      execFactory.register_expression( new PlaceHolderT( tagsETarsoot.rhotarGasInflowTag ) );
      execFactory.register_expression( new SumOpT( tagsETarsoot.rhotarGasKinRhsTag, {tagsETarsoot.tarOxidTag, tagsETarsoot.tarGasifTag, tagsETarsoot.tarToSootTag} ) );
      execFactory.register_expression(new ZeroDMixingT( tagsETarsoot.rhotarGasMixRhsTag, tagsETarsoot.rhotarGasInflowTag, tagsETarsoot.rhotarGasMassTag, tagsE.tauMixTag ));
      execFactory.register_expression( new SumOpT( tagsETarsoot.rhotarGasFullRhsTag, {tagsETarsoot.rhotarGasMixRhsTag, tagsETarsoot.rhotarGasKinRhsTag} ) );
      execFactory.register_expression( new PhiT( tagsETarsoot.tarGasMassTag, tagsE.rhoTag, tagsETarsoot.rhotarGasMassTag ) );

      // modify tar equations
      for( size_t i = 0;i < nparsize;i++ ){
        execFactory.register_expression( new ProductT( tagsETarsoot.rhovolTarTags[i], tagsEDev.volTarTags[i], tagsE.rhoTag));
        execFactory.attach_dependency_to_expression( tagsETarsoot.rhovolTarTags[i], tagsETarsoot.rhotarGasKinRhsTag, Expr::ADD_SOURCE_EXPRESSION ); //positive
      }

      // modify gas equation
      execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootMassGasSourceTag, tagsE.rhoKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );

      execFactory.attach_dependency_to_expression( tagsETarsoot.o2ConsumTotalTag, tagsE.rhoYKinRhsTags[IdxO2], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsETarsoot.coProdTotalTag, tagsE.rhoYKinRhsTags[IdxCO], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsETarsoot.h2oProdTotalTag, tagsE.rhoYKinRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
      execFactory.attach_dependency_to_expression( tagsETarsoot.h2ProductionTarTag,  tagsE.rhoYKinRhsTags[IdxH2], Expr::ADD_SOURCE_EXPRESSION );

      execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootHeatGasSourceTag, tagsE.rhoEgyKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.tarPartialRhoTag, tagsE.rhoTag, tagsETarsoot.tarGasMassTag));
      execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.rhotarMixPartialrhoTag, tagsE.tauMixTag, tagsETarsoot.tarGasMassTag));
      execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.rhotarMixPartialtarTag, tagsE.tauMixTag, tagsE.rhoTag));

      // add variables to the integrator
      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsETarsoot.rhotarGasMassTag.name(), tagsETarsoot.rhotarGasFullRhsTag, equIndex_, equIndex_ );
      if(haveSoot_){
        // soot mass fraction equation
        execFactory.register_expression( new PlaceHolderT( tagsETarsoot.rhosootInflowTag ) );
        execFactory.register_expression( new SumOpT( tagsETarsoot.rhosootKinRhsTag, {tagsETarsoot.sootFormTag, tagsETarsoot.sootOxidTag}) );
        execFactory.register_expression(new ZeroDMixingT( tagsETarsoot.rhosootMixRhsTag, tagsETarsoot.rhosootInflowTag, tagsETarsoot.rhosootMassTag, tagsE.tauMixTag ));
        execFactory.register_expression( new SumOpT( tagsETarsoot.rhosootFullRhsTag, {tagsETarsoot.rhosootMixRhsTag, tagsETarsoot.rhosootKinRhsTag} ) );
        execFactory.register_expression( new PhiT( tagsETarsoot.sootMassTag, tagsE.rhoTag, tagsETarsoot.rhosootMassTag ) );

        execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.sootPartialRhoTag, tagsE.rhoTag, tagsETarsoot.sootMassTag));
        execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.rhosootMixPartialrhoTag, tagsE.tauMixTag, tagsETarsoot.sootMassTag));
        execFactory.register_expression( new MassTarOverRhoT( tagsETarsoot.rhosootMixPartialsootTag, tagsE.tauMixTag, tagsE.rhoTag));

        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsETarsoot.rhosootMassTag.name(), tagsETarsoot.rhosootFullRhsTag, equIndex_, equIndex_ );
      }
    }

    if( reactorType_ == "PSRConstantPressure" or reactorType_ == "PFR"){
      // tar mass fraction equation
      integrator_->register_root_expression( new PlaceHolderT( tagsETarsoot.tarGasInflowTag ) );
      execFactory.register_expression( new SumOpT( tagsETarsoot.tarGasKinRhsTag, {tagsETarsoot.tarOxidTag, tagsETarsoot.tarGasifTag, tagsETarsoot.tarToSootTag} ) );

      // modify tar equations
      for( size_t i = 0;i < nparsize;i++ ){
        execFactory.attach_dependency_to_expression( tagsEDev.volTarTags[i], tagsETarsoot.tarGasKinRhsTag, Expr::ADD_SOURCE_EXPRESSION ); //positive
      }

      //modify tar, soot and soot particle equation: a source term related to the mass change of gas phase
      execFactory.register_expression( new SourceFromGasMassT( tagsETarsoot.tarGasSrcFromGasSrcTag, tagsEPart.partToGasMassTag, tagsETarsoot.tarGasMassTag));
      execFactory.attach_dependency_to_expression( tagsETarsoot.tarGasSrcFromGasSrcTag, tagsETarsoot.tarGasKinRhsTag, Expr::ADD_SOURCE_EXPRESSION);

      if( reactorType_ == "PSRConstantPressure"){
        execFactory.register_expression( new ZeroDMixingCPT( tagsETarsoot.tarGasMixRhsTag, tagsETarsoot.tarGasInflowTag, tagsETarsoot.tarGasMassTag, tagsE.tauMixTag,
                                                             tagsE.rhoInflowTag, tagsE.rhoTag ));
        execFactory.register_expression( new SumOpT( tagsETarsoot.tarGasFullRhsTag, {tagsETarsoot.tarGasMixRhsTag, tagsETarsoot.tarGasKinRhsTag} ) );

        // modify gas equations
        execFactory.attach_dependency_to_expression( tagsETarsoot.o2ConsumTotalTag, tagsE.yKinRhsTags[IdxO2], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.coProdTotalTag, tagsE.yKinRhsTags[IdxCO], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.h2oProdTotalTag, tagsE.yKinRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.h2ProductionTarTag,  tagsE.yKinRhsTags[IdxH2], Expr::ADD_SOURCE_EXPRESSION );

        execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootHeatGasSourceTag, tagsE.enthKinRhsTag, Expr::ADD_SOURCE_EXPRESSION );
      }
      else{
        execFactory.register_expression( new DivisionT( tagsETarsoot.tarGasFullRhsTag, tagsETarsoot.tarGasKinRhsTag, tagsEpfr.uTag ));
        // modify gas equations
        execFactory.register_expression( new DivisionT( tagsETarsoot.o2ConsumTotalPFRTag, tagsETarsoot.o2ConsumTotalTag, tagsEpfr.uTag ));
        execFactory.register_expression( new DivisionT( tagsETarsoot.coProdTotalPFRTag, tagsETarsoot.coProdTotalTag, tagsEpfr.uTag ));
        execFactory.register_expression( new DivisionT( tagsETarsoot.h2oProdTotalPFRTag, tagsETarsoot.h2oProdTotalTag, tagsEpfr.uTag ));
        execFactory.register_expression( new DivisionT( tagsETarsoot.h2ProductionTarPFRTag, tagsETarsoot.h2ProductionTarTag, tagsEpfr.uTag ));
        execFactory.attach_dependency_to_expression( tagsETarsoot.o2ConsumTotalPFRTag, tagsE.yFullRhsTags[IdxO2], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.coProdTotalPFRTag, tagsE.yFullRhsTags[IdxCO], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.h2oProdTotalPFRTag, tagsE.yFullRhsTags[IdxH2O], Expr::ADD_SOURCE_EXPRESSION );
        execFactory.attach_dependency_to_expression( tagsETarsoot.h2ProductionTarPFRTag,  tagsE.yFullRhsTags[IdxH2], Expr::ADD_SOURCE_EXPRESSION );

        execFactory.register_expression( new DivisionT( tagsETarsoot.tarSootHeatGasSourcePFRTag, tagsETarsoot.tarSootHeatGasSourceTag, tagsEpfr.uTag ));
        execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootHeatGasSourcePFRTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION );

        execFactory.register_expression( new DpDxEnthalpyT( tagsETarsoot.tarSootHeatGasSourceDpDxTag, tagsETarsoot.tarSootHeatGasSourceTag, tagsE.rhoTag, tagsE.tempTag,
                                                            tagsE.enthTag, tagsE.mmwTag, tagsE.cpTag, tagsEpfr.uTag));
        execFactory.attach_dependency_to_expression( tagsETarsoot.tarSootHeatGasSourceDpDxTag, tagsEpfr.dpdxTag, Expr::ADD_SOURCE_EXPRESSION );
      }

      // add variables to the integrator
      equIndex_ = equIndex_ + 1;
      integrator_->add_variable<FieldT>( tagsETarsoot.tarGasMassTag.name(), tagsETarsoot.tarGasFullRhsTag, equIndex_, equIndex_ );
      if(haveSoot_){
        // soot mass fraction equation
        integrator_->register_root_expression( new PlaceHolderT( tagsETarsoot.sootInflowTag ) );
        execFactory.register_expression( new SumOpT( tagsETarsoot.sootKinRhsTag, {tagsETarsoot.sootFormTag, tagsETarsoot.sootOxidTag}) );
        execFactory.register_expression( new SourceFromGasMassT( tagsETarsoot.sootSrcFromGasSrcTag, tagsEPart.partToGasMassTag, tagsETarsoot.sootMassTag));
        execFactory.attach_dependency_to_expression( tagsETarsoot.sootSrcFromGasSrcTag, tagsETarsoot.sootKinRhsTag, Expr::ADD_SOURCE_EXPRESSION);

        if( reactorType_ == "PSRConstantPressure"){
          execFactory.register_expression(new ZeroDMixingCPT( tagsETarsoot.sootMixRhsTag, tagsETarsoot.sootInflowTag, tagsETarsoot.sootMassTag,
                                                              tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
          execFactory.register_expression( new SumOpT( tagsETarsoot.sootFullRhsTag, {tagsETarsoot.sootMixRhsTag, tagsETarsoot.sootKinRhsTag} ) );
        }
        else{
          execFactory.register_expression( new DivisionT( tagsETarsoot.sootFullRhsTag, tagsETarsoot.sootKinRhsTag, tagsEpfr.uTag ));
        }
        equIndex_ = equIndex_ + 1;
        integrator_->add_variable<FieldT>( tagsETarsoot.sootMassTag.name(), tagsETarsoot.sootFullRhsTag, equIndex_, equIndex_ );
      }
    }
  }

  //---------------------------------------------------------------------------
  void TarSootInterface::initialize_coal_tarsoot(){
    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );
    if( reactorType_ == "PSRConstantVolume" ){
      if(!isRestart_){
        integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsETarsoot.rhotarGasInflowTag.name());
        if(haveSoot_){
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsETarsoot.rhosootInflowTag.name());
        }
      }
      integrator_->lock_field<FieldT>( tagsETarsoot.rhotarGasInflowTag );
      if(haveSoot_){
        integrator_->lock_field<FieldT>( tagsETarsoot.rhosootInflowTag );
      }
    }
    if( reactorType_ == "PSRConstantPressure" ){
      if(!isRestart_){
        integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsETarsoot.tarGasInflowTag.name());
        if(haveSoot_){
          integrator_->copy_from_initial_condition_to_execution<FieldT>( tagsETarsoot.sootInflowTag.name());
        }
      }
      integrator_->lock_field<FieldT>( tagsETarsoot.tarGasInflowTag );
      if(haveSoot_){
        integrator_->lock_field<FieldT>( tagsETarsoot.sootInflowTag );
      }
    }
  }

  //---------------------------------------------------------------------------
  void TarSootInterface::modify_prim_kinrhs_tags_tarsoot(){
    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );

    primitiveTags_.push_back( tagsETarsoot.tarGasMassTag );
    if(haveSoot_){
      primitiveTags_.push_back( tagsETarsoot.sootMassTag );
    }
    if( reactorType_ == "PSRConstantVolume" ){
      kinRhsTags_.push_back( tagsETarsoot.rhotarGasKinRhsTag );
      if(haveSoot_){
        kinRhsTags_.push_back( tagsETarsoot.rhosootKinRhsTag );
      }
    }
    if( reactorType_ == "PSRConstantPressure" ){
      kinRhsTags_.push_back( tagsETarsoot.tarGasKinRhsTag );
      if(haveSoot_){
        kinRhsTags_.push_back( tagsETarsoot.sootKinRhsTag );
      }
    }
    if( reactorType_ == "PFR" ){
      kinRhsTags_.push_back( tagsETarsoot.tarGasFullRhsTag );
      if(haveSoot_){
        kinRhsTags_.push_back( tagsETarsoot.sootFullRhsTag );
      }
    }
  }

  //---------------------------------------------------------------------------
  void TarSootInterface::modify_idxmap_tarsoot( std::map<Expr::Tag, int> &primVarIdxMap,
                                                const std::map<Expr::Tag, int> &consVarIdxMap,
                                                std::map<Expr::Tag, int> &kinRhsIdxMap,
                                                const std::map<Expr::Tag, int> &rhsIdxMap){

    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );
    if( reactorType_ == "PSRConstantVolume" ){
      primVarIdxMap[tagsETarsoot.tarGasMassTag] = consVarIdxMap.at( tagsETarsoot.rhotarGasMassTag );
      kinRhsIdxMap[tagsETarsoot.rhotarGasKinRhsTag] = rhsIdxMap.at( tagsETarsoot.rhotarGasFullRhsTag );
      if(haveSoot_){
        primVarIdxMap[tagsETarsoot.sootMassTag] = consVarIdxMap.at( tagsETarsoot.rhosootMassTag );
        kinRhsIdxMap[tagsETarsoot.rhosootKinRhsTag] = rhsIdxMap.at( tagsETarsoot.rhosootFullRhsTag );
      }
    }

    if( reactorType_ == "PSRConstantPressure" ){
      primVarIdxMap[tagsETarsoot.tarGasMassTag] = consVarIdxMap.at( tagsETarsoot.tarGasMassTag );
      kinRhsIdxMap[tagsETarsoot.tarGasKinRhsTag] = rhsIdxMap.at( tagsETarsoot.tarGasFullRhsTag );
      if(haveSoot_){
        primVarIdxMap[tagsETarsoot.sootMassTag] = consVarIdxMap.at( tagsETarsoot.sootMassTag );
        kinRhsIdxMap[tagsETarsoot.sootKinRhsTag] = rhsIdxMap.at( tagsETarsoot.sootFullRhsTag );
      }
    }
    if( reactorType_ == "PFR" ){
      primVarIdxMap[tagsETarsoot.tarGasMassTag] = consVarIdxMap.at( tagsETarsoot.tarGasMassTag );
      kinRhsIdxMap[tagsETarsoot.tarGasFullRhsTag] = rhsIdxMap.at( tagsETarsoot.tarGasFullRhsTag );
      if(haveSoot_){
        primVarIdxMap[tagsETarsoot.sootMassTag] = consVarIdxMap.at( tagsETarsoot.sootMassTag );
        kinRhsIdxMap[tagsETarsoot.sootFullRhsTag] = rhsIdxMap.at( tagsETarsoot.sootFullRhsTag );
      }
    }
  }

  void TarSootInterface::dvdu_coal_tarsoot( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                                            std::map<Expr::Tag, int> primVarIdxMap,
                                            std::map<Expr::Tag, int> consVarIdxMap){

    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );

    if( reactorType_ == "PSRConstantVolume" ){
      dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.tarGasMassTag ), consVarIdxMap.at( tagsE.rhoTag )) = tagsETarsoot.tarPartialRhoTag;
      dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.tarGasMassTag ), consVarIdxMap.at( tagsETarsoot.rhotarGasMassTag )) = tagsE.invrhoTag;
      if(haveSoot_){
        dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.sootMassTag ), consVarIdxMap.at( tagsE.rhoTag )) = tagsETarsoot.sootPartialRhoTag;
        dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.sootMassTag ), consVarIdxMap.at( tagsETarsoot.rhosootMassTag )) = tagsE.invrhoTag;
      }
    }

    else{
      dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.tarGasMassTag ), consVarIdxMap.at( tagsETarsoot.tarGasMassTag )) = 1.0;
      if(haveSoot_){
        dVdUPart->element<double>( primVarIdxMap.at( tagsETarsoot.sootMassTag ), consVarIdxMap.at( tagsETarsoot.sootMassTag )) = 1.0;
      }
    }
  }

  void TarSootInterface::dmMixingdv_coal_tarsoot( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                                  std::map<Expr::Tag, int> primVarIdxMap,
                                                  std::map<Expr::Tag, int> rhsIdxMap,
                                                  std::map<Expr::Tag, Expr::Tag> consVarRhsMap){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE);
    const Tarsoot::TarSootTags tagsETarsoot( Expr::STATE_NONE, particleNumArray_ );

    const Expr::Tag& rho = tagsE.rhoTag;
    const Expr::Tag& tarGas = tagsETarsoot.tarGasMassTag;
    auto massTarIdx = primVarIdxMap.at( tagsETarsoot.tarGasMassTag );

    if( reactorType_ == "PSRConstantVolume" ){
      const Expr::Tag& tarGasRhs = tagsETarsoot.rhotarGasMixRhsTag;

      const auto& massTarRhsIdx = rhsIdxMap.at( consVarRhsMap.at( tagsETarsoot.rhotarGasMassTag ));
      const auto& massSootRhsIdx = rhsIdxMap.at( consVarRhsMap.at( tagsETarsoot.rhosootMassTag ));

      dmMixingdVPart->element<FieldT>( massTarRhsIdx, primVarIdxMap.at(rho) ) = tagsETarsoot.rhotarMixPartialrhoTag;
      dmMixingdVPart->element<FieldT>( massTarRhsIdx, massTarIdx ) = tagsETarsoot.rhotarMixPartialtarTag;

      if(haveSoot_){
        const Expr::Tag& sootMass = tagsETarsoot.sootMassTag;
        auto massSootIdx = primVarIdxMap.at( tagsETarsoot.sootMassTag );
        dmMixingdVPart->element<FieldT>( massSootRhsIdx, primVarIdxMap.at(rho) ) = tagsETarsoot.rhosootMixPartialrhoTag;
        dmMixingdVPart->element<FieldT>( massSootRhsIdx, massSootIdx ) = tagsETarsoot.rhosootMixPartialsootTag;
      }
    }

    if( reactorType_ == "PSRConstantPressure" ){
      const Expr::Tag& tarGasRhs = tagsETarsoot.tarGasMixRhsTag;
      const auto& massTarRhsIdx = rhsIdxMap.at( consVarRhsMap.at( tagsETarsoot.tarGasMassTag ));

      dmMixingdVPart->element<FieldT>( massTarRhsIdx, primVarIdxMap.at(rho) ) = sensitivity(tarGasRhs, rho);
      dmMixingdVPart->element<FieldT>( massTarRhsIdx, massTarIdx ) = sensitivity(tarGasRhs, tarGas);

      if(haveSoot_){
        const Expr::Tag& sootMass = tagsETarsoot.sootMassTag;
        auto massSootIdx = primVarIdxMap.at( tagsETarsoot.sootMassTag );
        const Expr::Tag& sootMassRhs = tagsETarsoot.sootMixRhsTag;
        const auto& massSootRhsIdx = rhsIdxMap.at( consVarRhsMap.at( tagsETarsoot.sootMassTag ));
        dmMixingdVPart->element<FieldT>( massSootRhsIdx, primVarIdxMap.at(rho) ) = sensitivity(sootMassRhs, rho);
        dmMixingdVPart->element<FieldT>( massSootRhsIdx, massSootIdx ) = sensitivity(sootMassRhs, sootMass);
      }
    }
  }

} // namespace Tarsoot
