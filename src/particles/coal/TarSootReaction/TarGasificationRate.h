
/**
 * @file TarGasificationRate.h
 * @par Calculates the rate of tar gasification
 * @author Hang Zhou
 */
#ifndef TarGasificationRate_h
#define TarGasificationRate_h

#include <expression/Expression.h>

namespace Tarsoot {

/**
 *  @class TarGasificationRate
 *  @ingroup Tarsoot
 *  @brief Calculates the rate of tar gasification, which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass and tar mass per gas mass (for constant pressure) or per volume (for constant volume).
 *
 *  The rate of tar gasification is given as
 *  For constant pressure condition: (1/s)
 * \f[
 *     r_{\mathrm{G,tar}} = - m_{\mathrm{tar}} * A_{G,\mathrm{tar}} * exp(-E_{G,\mathrm{tar}} / RT),
 * \f]
 * For constant volume condition: (kg/m^3/s)
 * \f[
 *     r_{\mathrm{G,tar}} = - \rho m_{\mathrm{tar}} * A_{G,\mathrm{tar}} * exp(-E_{G,\mathrm{tar}} / RT),
 * \f]
 *
 * Here \f$ m_{\mathrm{tar}} \f$ is mass of tar per gas mass.
 *      \f$ A_{G,\mathrm{tar}} \f$ is Arrhenius preexponential factor(1/s). [1,2]
 *      \f$ E_{G,\mathrm{tar} \f$ is activation energy(J/mol) . [1,2]
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *  [2]    Josephson, Alexander J.; Lignell, David O.; Brown, Alexander L.; and Fletcher, Thomas H.
 *        "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 */
  template<typename FieldT>
  class TarGasificationRate : public Expr::Expression<FieldT> {
    DECLARE_FIELDS( FieldT, massTar_, temp_, rho_ )

    const double A_, E_, gasCon_;
    const std::string reactorType_;

    TarGasificationRate( const Expr::Tag& massTarTag,
                         const Expr::Tag& tempTag,
                         const Expr::Tag& rhoTag,
                         const std::string& reactorType)
          : Expr::Expression<FieldT>(),
            reactorType_( reactorType ),
            A_( 9.77E+10 ),
            E_( 286.9E+3 ),
            gasCon_( 8.3144621 ){
      this->set_gpu_runnable( true );
      massTar_ = this->template create_field_request<FieldT>( massTarTag );
      temp_    = this->template create_field_request<FieldT>( tempTag    );
      rho_     = this->template create_field_request<FieldT>( rhoTag     );
    }

  public:
    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag massTarTag_, tempTag_, rhoTag_;
      const std::string reactorType_;
    public:
      /**
       * @brief Build a TarGasificationRate expression
       * @tparam FieldT
       * @param resultTag tar gasification rate
       * @param massTarTag mass of tar per gas mass
       * @param tempTag gas phase temperature
       * @param rhoTag gas density
       * @param reactorType reactor type
       */
      Builder( const Expr::Tag& resultTag,
               const Expr::Tag& massTarTag,
               const Expr::Tag& tempTag,
               const Expr::Tag& rhoTag,
               const std::string& reactorType)
            : ExpressionBuilder( resultTag ),
              massTarTag_( massTarTag ),
              tempTag_( tempTag ),
              rhoTag_( rhoTag ),
              reactorType_( reactorType ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new TarGasificationRate<FieldT>( massTarTag_, tempTag_, rhoTag_, reactorType_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;

      FieldT& result = this->value();

      const FieldT& massTar = massTar_->field_ref();
      const FieldT& temp    = temp_->field_ref();

      if( reactorType_=="PSRConstantVolume"){
        const FieldT& rho =rho_->field_ref();
        result <<= cond( massTar <= 0.0, 0.0)
                       ( - rho *  massTar * A_ * exp( -E_ / ( gasCon_ * temp ))); //negative
      }
      else{
        result <<= cond( massTar <= 0.0, 0.0)
                       ( - massTar * A_ * exp( -E_ / ( gasCon_ * temp ))); //negative
      }
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( sensVarTag );
      const FieldT& massTar = massTar_->field_ref();
      const FieldT& temp = temp_->field_ref();
      const FieldT& dmassTardv = massTar_->sens_field_ref(sensVarTag);
      const FieldT& dtempdv = temp_->sens_field_ref(sensVarTag);

      if( reactorType_=="PSRConstantVolume"){
        const FieldT& rho =rho_->field_ref();
        const FieldT& drhodv =rho_->sens_field_ref(sensVarTag);
        dfdv <<= cond( massTar <= 0.0, 0.0)
                     ( - (rho*dmassTardv + drhodv*massTar) * A_ * exp( -E_ / ( gasCon_ * temp ))
                       - rho*massTar * A_ * exp(-E_ / ( gasCon_ * temp )) * E_ / (gasCon_*temp*temp) * dtempdv);

      }
      else{
        dfdv <<= cond( massTar <= 0.0, 0.0)
                     ( - dmassTardv * A_ * exp( -E_ / ( gasCon_ * temp ))
                      - massTar * A_ * exp(-E_ / ( gasCon_ * temp )) * E_ / (gasCon_*temp*temp) * dtempdv);
      }
    }
  };
}






#endif // TarGasificationRate_Expr_h
