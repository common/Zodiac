/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * @file DerivedRxnTerms.h
 * @par Calculates the rate at which a species 2 reacts given the reaction
 *      rate of species1 using the  molecular weights, and the stoichiomentric
 *      coefficients of species 1 and 2
 * @author Hang Zhou
 */

#ifndef DerivedRxnTerms_Expr_h
#define DerivedRxnTerms_Expr_h

#include <expression/Expression.h>
namespace Tarsoot {

/**
 *  @class DerivedRxnRate
 *  @ingroup Tarsoot
 *  @brief Calculates the rate at which a species 2 reacts given the reaction
 *         rate of species1 using the  molecular weights, and the stoichiomentric
 *         coefficients of species 1 and 2:
 *
 *         rxnRate2 = (nu2*mw2)/(nu1*mw1)*rxnRate1
 *
 *         As an example, consider the combustion of methane:
 *
 *         CH4 + 3*O2 --> CO2 + 2*H2O
 *
 *         Suppose we know the rate at which O2 is produced and it is desired
 *         to know the rate at which H2O is produced. Both rates are in
 *         kg/m^3-s. The rate at which H2O is produced is
 *
 *         [rate H2O] = (2*18.02)/(-3*32.00)*[rate O2].
 *
 */
    template<typename FieldT>
    class DerivedRxnRate: public Expr::Expression<FieldT> {
        DECLARE_FIELD( FieldT, rxnRate1_ )
        const double mw1_, mw2_, nu1_, nu2_;

        DerivedRxnRate( const double& mw1,
                        const double& mw2,
                        const double& nu1,
                        const double& nu2,
                        const Expr::Tag& rxnRate1Tag )
              : Expr::Expression<FieldT>(),
                mw1_( mw1 ),
                mw2_( mw2 ),
                nu1_( nu1 ),
                nu2_( nu2 )
        {
          rxnRate1_ = this->template create_field_request<FieldT>( rxnRate1Tag );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag rxnRate1Tag_;
            const double mw1_, mw2_, nu1_, nu2_;
        public:
            /**
             *  @brief Build a DerivedRxnRate expression
             *  @tparam FieldT
             *  @param resultTag reaction rate of species 2
             *  @param mw1:      molecular weight of species 1  (g/mol)
             *  @param mw2:      molecular weight of species 2  (g/mol)
             *  @param nu1:      stoichiometric coefficient of species 1
             *  @param nu2:      stoichiometric coefficient of species 2
             *  @param rxnRate1  reaction rate of species 1  (kg/m^3-s)
             */
            Builder( const Expr::Tag& resultTag,
                     const double& mw1,
                     const double& mw2,
                     const double& nu1,
                     const double& nu2,
                     const Expr::Tag& rxnRate1Tag,
                     const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
                  : ExpressionBuilder( resultTag, nghost ),
                    mw1_        ( mw1         ),
                    mw2_        ( mw2         ),
                    nu1_        ( nu1         ),
                    nu2_        ( nu2         ),
                    rxnRate1Tag_( rxnRate1Tag ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new DerivedRxnRate<FieldT>( mw1_, mw2_, nu1_, nu2_, rxnRate1Tag_ );
            }
        };
        void evaluate(){
          FieldT& result = this->value();
          const FieldT& rxnRate1 = rxnRate1_->field_ref();
          result <<= (mw2_*nu2_)/(mw1_*nu1_)*rxnRate1;
        }
        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= (mw2_*nu2_)/(mw1_*nu1_) * rxnRate1_->sens_field_ref(sensVarTag);
        }
    };
}
//--------------------------------------------------------------------


#endif // DerivedRxnTerms_Expr_h
