/*
 * The MIT License
 *
 * Copyright (c) 2012-2018 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TarAndSootInfo_h
#define TarAndSootInfo_h

#include <yaml-cpp/yaml.h>

  /**
   *
   *  \class  TarAndSootInfo
   *  \author Josh McConnell
   *  \date   November, 2016
   *
   */
  class  TarAndSootInfo
  {
  public:

    static void setup( const YAML::Node& parser );
    static const TarAndSootInfo& get();

    static double tar_hydrogen()           ;
    static double tar_carbon()             ;
    static double tar_required_moles_O2()  ;
    static double tar_produced_moles_CO()  ;
    static double tar_produced_moles_H2O() ;
    static double tar_produced_moles_H2()  ;
    static double tar_mw()                 ;
    static double tar_diffusivity()        ;
    static double tar_heat_of_formation()  ;
    static double tar_heat_of_oxidation()  ;
                                           ;
    static double soot_hydrogen()          ;
    static double soot_carbon()            ;
    static double soot_required_moles_O2() ;
    static double soot_produced_moles_CO() ;
    static double soot_produced_moles_H2O();
    static double soot_mw()                ;
    static double soot_density()           ;
    static double soot_cMin()              ;
    static double soot_heat_of_oxidation() ;
    static double soot_heat_of_formation() ;
  private:
    bool hasBeenSetup_;
    bool haveSoot_;
    const double sootDensity_, sootCMin_;

    // Tar properties. It is assumed that coal tar is Naphthalene.
    // Reaction of tar is assumed to occur by the following reaction:
    // tar + O2 --> CO + H2O
    double tarDiffusivity_,
           tarHydrogen_,
           tarCarbon_,
           tarRequiredMolesO2_,  // (moles O2 )/(moles tar) required for oxidation rxn
           tarProducedMolesCO_,  // (moles CO )/(moles tar) produced by oxidation rxn
           tarProducedMolesH2O_, // (moles H2O)/(moles tar) produced by oxidation rxn
           tarProducedMolesH2_,  // (moles H2 )/(moles tar) produced by tar->soot
           tarMW_,
           tarHeatOfFormation_,
           tarHeatOfOxidation_;

    // soot properties
    double sootHydrogen_,
           sootCarbon_,
           sootRequiredMolesO2_,  // (moles O2 )/(moles soot) required for oxidation rxn
           sootProducedMolesCO_,  // (moles CO )/(moles soot) produced by oxidation rxn
           sootProducedMolesH2O_, // (moles H2O)/(moles soot) produced by oxidation rxn
           sootMW_,
           sootHeatOfOxidation_,
           sootHeatOfFormation_;

    TarAndSootInfo();
    static TarAndSootInfo& self();
  };
#endif /* TarAndSootInfo_h */
