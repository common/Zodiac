/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   TarSootReactionInterface.h
 *  @author Hang Zhou
 */

#ifndef TARSOOTREACTION_INTERFACE_UTIL_H_
#define TARSOOTREACTION_INTERFACE_UTIL_H_

#include <vector>
#include <expression/ExprLib.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>
#include <yaml-cpp/yaml.h>

namespace Tarsoot {
  /**
   * @class TarSootTags
   * @ingroup Tarsoot
   * @brief Getting tags used in TarSoot model
   * @author Hang Zhou
   */
  struct TarSootTags {
    const Expr::Tag tarGasMassTag,                 ///< tar mass in gas phase
                    tarGasInflowTag,               ///< inflowing tar mass in gas phase-- it is always 0
                    tarGasSrcFromGasSrcTag,        ///< source term in tar mass equation from the change of gas phase mass
                    tarGasMixRhsTag,               ///< mixing RHS of tar gas mass equation
                    tarGasKinRhsTag,               ///< kinetic RHS of tar gas mass equation
                    tarGasFullRhsTag,              ///< full RHS of tar gas mass equation
                    rhotarGasMassTag,              ///< rho * tar mass in gas phase
                    rhotarGasInflowTag,            ///< inflowing rho * tar mass in gas phase-- it is always 0
                    rhotarGasMixRhsTag,            ///< mixing RHS of rho * tar gas mass equation
                    rhotarGasKinRhsTag,            ///< kinetic RHS of rho* tar gas mass equation
                    rhotarGasFullRhsTag,           ///< full RHS of rho* tar gas mass equation
                    sootMassTag,                   ///< soot mass
                    sootInflowTag,                 ///< inflowing soot mass -- it is always 0
                    sootSrcFromGasSrcTag,          ///< source term in soot mass equation from the change of gas phase mass
                    sootMixRhsTag,                 ///< mixing RHS of soot mass equation
                    sootKinRhsTag,                 ///< kinetic RHS of soot mass equation
                    sootFullRhsTag,                ///< full RHS of soot mass equation
                    rhosootMassTag,                ///< rho* soot mass
                    rhosootInflowTag,              ///< inflowing rho * soot mass -- it is always 0
                    rhosootMixRhsTag,              ///< mixing RHS of rho * soot mass equation
                    rhosootKinRhsTag,              ///< kinetic RHS of rho * soot mass equation
                    rhosootFullRhsTag,             ///< full RHS of rho * soot mass equation
                    tarGasifTag,                   ///< tar gasification term in tar gas mass equation
                    tarOxidTag,                    ///< tar oxidization term in tar gas mass equation
                    tarToSootTag,                  ///< tar gas consumption by production of soot
                    o2EnthTgasTag,                 ///< o2 enthalpy at tar temperature(Tgas)
                    coEnthTgasTag,                 ///< co enthalpy at tar temperature(Tgas)
                    h2oEnthTgasTag,                ///< h2o enthalpy at tar temperature(Tgas)
                    h2EnthTgasTag,                 ///< co enthalpy at tar temperature(Tgas)
                    o2ConsumTotalTag,              ///< o2 consumption by tar and soot oxidation by all particles
                    coProdTotalTag,                ///< co production by tar and soot oxidation by all particles
                    h2oProdTotalTag,               ///< h2o production by tar and soot oxidation by all particles
                    o2ConsumTotalPFRTag,           ///< o2 consumption by tar and soot oxidation by all particles for plug flow reactor
                    coProdTotalPFRTag,             ///< co production by tar and soot oxidation by all particles for plug flow reactor
                    h2oProdTotalPFRTag,            ///< h2o production by tar and soot oxidation by all particles for plug flow reactor
                    tarSootMassGasSourceTag,       ///< gas phase mass source term from tar and soot reaction
                    tarSootHeatGasSourceTag,       ///< gas phase energy source term from tar and soot reaction
                    tarSootHeatGasSourcePFRTag,    ///< gas phase energy source term from tar and soot reaction for plug flow reactor
                    tarSootHeatGasSourceDpDxTag,   ///< gas phase energy source term in `Dp/Dx` from tar and soot reaction for plug flow reactor
                    sootFormTag,                   ///< soot formation from tar
                    sootAggTag,                    ///< soot agglomeration term
                    sootOxidTag,                   ///< soot oxidization term in soot equation
                    o2ConsumptionTarTag,           ///< consumption of O2 by tar oxidation
                    coProductionTarTag,            ///< production of CO by tar oxidation
                    h2oProductionTarTag,           ///< production of H2O by tar oxidation
                    h2ProductionTarTag,            ///< production of h2 during soot formation
                    h2ProductionTarPFRTag,         ///< production of h2 during soot formation for plug flow reactor
                    o2ConsumptionSootTag,          ///< consumption of O2 by soot oxidation
                    coProductionSootTag,           ///< production of CO by soot oxidation
                    h2oProductionSootTag,          ///< production of H2O by soot oxidation
                    tarPartialRhoTag,              ///< (\partial tauMass)/(\partial \rho) - for matrix transformation dV/dU
                    sootPartialRhoTag,             ///< (\partial sootMass)/(\partial \rho) - for matrix transformation dV/dU
                    rhotarMixPartialrhoTag,        ///< (\partial rhotauMass_mix_rhs)/(\partial rho) - for matrix transformation
                    rhotarMixPartialtarTag,        ///< (\partial rhotauMass_mix_rhs)/(\partial tar) - for matrix transformation
                    rhosootMixPartialrhoTag,       ///< (\partial rhosoot_mix_rhs)/(\partial rho) - for matrix transformation
                    rhosootMixPartialsootTag,      ///< (\partial rhosoot_mix_rhs)/(\partial soot) - for matrix transformation
                    tarSootYoxidizerTag;           ///< source term in total mass oxidizer in reactor from tar and soot reaction
    Expr::TagList   rhovolTarTags;                 ///< rho*volTar: used in tar source term under constant volume condition
    TarSootTags( const Expr::Context& state,
                 const std::vector<std::string>& particleNumArray);
  };

  /**
   *  @ingroup Tarsoot
   *  @class TarSootInterface
   *  @brief Provides an interface to the TarSoot model
   */
  class TarSootInterface{

    const Coal::CoalType coaltype_;
    const TarAndSootInfo& tarSootInfo_;
    std::vector<std::string> particleNumArray_, speciesArray_, partNumSpeciesArray_;
    const YAML::Node& rootParser_;
    std::set<Expr::ExpressionID>& initRoots_;
    Expr::ExpressionFactory& initFactory_;
    Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
    Expr::TagList& primitiveTags_;
    Expr::TagList& kinRhsTags_;
    int& equIndex_;
    const bool isRestart_;
    const bool haveSoot_;
    std::string reactorType_;

  public:
    /**
     * @param coalType coal type
     * @param tarSootInfo tarSoot information
     * @param particleNumArray list of particle numbers with different sizes
     * @param speciesArray list of species involved in Dev model
     * @param partNumSpeciesArray list of combination of particle number index and species index
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
    TarSootInterface( const Coal::CoalType coalType,
                      const TarAndSootInfo& tarSootInfo,
                      std::vector<std::string> particleNumArray,
                      std::vector<std::string> SpeciesArray,
                      std::vector<std::string> partNumSpeciesArray,
                      const YAML::Node& rootParser,
                      std::set<Expr::ExpressionID>& initRoots,
                      Expr::ExpressionFactory& initFactory,
                      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                      Expr::TagList& primitiveTags,
                      Expr::TagList& kinRhsTags,
                      int& equIndex,
                      const bool isRestart);
    ~TarSootInterface();

    /**
     * @brief Initialize TarSoot model
     */
    void initialize_coal_tarsoot();

    /**
     * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved in TarSoot model
     * @param primVarIdxMap primitive variables indices
     * @param consVarIdxMap solved variables indices
     * @param kinRhsIdxMap kinetic rhs indices
     * @param rhsIdxMap rhs tags indices
     */
    void modify_idxmap_tarsoot( std::map<Expr::Tag, int> &primVarIdxMap,
                                const std::map<Expr::Tag, int> &consVarIdxMap,
                                std::map<Expr::Tag, int> &kinRhsIdxMap,
                                const  std::map<Expr::Tag, int> &rhsIdxMap);

    /**
     * @brief Modify particle state transformation matrix
     * @param dVdUPart particle state transformation matrix
     * @param primVarIdxMap primitive variables indices
     * @param consVarIdxMap solved variables indices
     */
    void dvdu_coal_tarsoot( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dVdUPart,
                            std::map<Expr::Tag, int> primVarIdxMap,
                            std::map<Expr::Tag, int> consVarIdxMap);

    /**
     * @brief Modify particle mixing rhs matrix (dmixPartdV)
     * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
     * @param primVarIdxMap primitive variables indices
     * @param rhsIdxMap rhs tags indices
     * @param consVarRhsMap solved variables rhs tags indices
     */
    void dmMixingdv_coal_tarsoot( boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>>& dmMixingdVPart,
                                  std::map<Expr::Tag, int> primVarIdxMap,
                                  std::map<Expr::Tag, int> rhsIdxMap,
                                  std::map<Expr::Tag, Expr::Tag> consVarRhsMap);

  private:
    /**
     * @brief Set the initial conditions for the TarSoot model
     */
    void setup_initial_conditions_tarsoot();

    /**
     * @brief Register all expressions required to implement the TarSoot model and add equation into the time integrator
     */
    void build_equation_system_coal_tarsoot();

    /**
     * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in TarSoot model
     */
    void modify_prim_kinrhs_tags_tarsoot();

  };
} // namespace Tarsoot

#endif /* TARSOOTREACTION_INTERFACE_UTIL_H_ */
