/*
 * The MIT License
 *
 * Copyright (c) 2012-2018 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <sstream>
#include "TarAndSootInfo.h"
#include <iostream>

  TarAndSootInfo::TarAndSootInfo():
    hasBeenSetup_(false),
    haveSoot_(false),
    tarDiffusivity_( 8.6e-6 ),            // (m^2/s), diffusivity of naphthalene in air (303 K, 101 kPa). Caldwell, 1984

    // These parameters result in a mean soot particle diameter of ~45 nm
    sootDensity_( 1950. ),               // kg/m^3 [1]
    sootCMin_( 1000 )          // (carbon atoms)/(incipient soot particle)
//    sootCMin_( 4.66595591e+06 )          // (carbon atoms)/(incipient soot particle)
  {
    // tar properties -- this assumes tar is C10H8.
    tarHydrogen_         = 0. ; // (mol H)/(mol tar)
    tarCarbon_           = 0. ; // (mol C)/(mol tar)
    tarRequiredMolesO2_  = 0. ; // (moles O2 )/(moles tar) required for oxidation rxn
    tarProducedMolesCO_  = 0. ; // (moles CO )/(moles tar) produced by oxidation rxn
    tarProducedMolesH2O_ = 0. ; // (moles H2O)/(moles tar) produced by oxidation rxn
    tarProducedMolesH2_  = 0. ; // (moles H2 )/(moles tar) produced by tar->soot
    tarMW_               = 0. ; // g/mol
    tarHeatOfFormation_  = 0. ;  // J/kg
    tarHeatOfOxidation_  = 0. ;  // J/kg, calculated from Hess's law

    // Soot properties -- this assumes soot has the same elemental makeup as tar.

    sootHydrogen_         = 0.; // (mol H)/(mol soot)
    sootCarbon_           = 0.; // (mol C)/(mol soot)
    sootRequiredMolesO2_  = 0.; // (moles O2 )/(moles tar) required for oxidation rxn
    sootProducedMolesCO_  = 0.; // (moles CO )/(moles tar) produced by oxidation rxn
    sootProducedMolesH2O_ = 0.; // (moles H2O)/(moles tar) produced by oxidation rxn
    sootMW_               = 0.; // g/mol
    sootHeatOfOxidation_  = 0.; // J/kg
    sootHeatOfFormation_  = 0.;
  }

  //------------------------------------------------------------------

  void
  TarAndSootInfo::setup(const YAML::Node& parser)
  {
    TarAndSootInfo& tsi = TarAndSootInfo::self();
    if( tsi.hasBeenSetup_ ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "TarAndSootInfo::setup() can only be called once" << std::endl;
      throw std::runtime_error( msg.str() );
    }

    const YAML::Node& tarParser = parser["Particles"];
    const bool tarIsC2H2    = tarParser["TarIsC2H2"   ].as<bool>();
    tsi.haveSoot_ = tarParser["SootIsCarbon"];
    bool sootIsCarbon;

    if( tarIsC2H2 ){
      // tar properties -- this assumes tar is 1,2-dihydronaphthalene (C10H10.
      // https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5769711/
      tsi.tarHydrogen_         = 10.;
      tsi.tarCarbon_           = 10.;
      tsi.tarMW_               = 130.1894;
      tsi.tarHeatOfFormation_  = 9.58603e+05;
      tsi.tarHeatOfOxidation_  = -1.780e+07;
    }
    else{
      tsi.tarHydrogen_         = 8.;
      tsi.tarCarbon_           = 10;
      tsi.tarMW_               = 128.17352;
      tsi.tarHeatOfFormation_  = 1.175e+06;
      tsi.tarHeatOfOxidation_  = -1.735e+07;
    }

    if(tsi.haveSoot_){
      sootIsCarbon = tarParser["SootIsCarbon"].as<bool>();
      if( sootIsCarbon) {
        tsi.sootHydrogen_         = 0.;
        tsi.sootCarbon_           = 1.;
        tsi.sootMW_               = 12.011;
        tsi.sootHeatOfOxidation_  = -9.2e+06;
        tsi.sootHeatOfFormation_  = 0.0;
        tsi.tarProducedMolesH2_   = tsi.tarHydrogen_/2;
      }
      else{
        tsi.sootHydrogen_         = tsi.tarHydrogen_;
        tsi.sootCarbon_           = tsi.tarCarbon_;
        tsi.sootMW_               = tsi.tarMW_;
        tsi.sootHeatOfOxidation_  = tsi.tarHeatOfOxidation_;
        tsi.sootHeatOfFormation_  = tsi.tarHeatOfFormation_;
        tsi.tarProducedMolesH2_   = 0.0;
      }
    }

    tsi.tarRequiredMolesO2_   = tsi.tarHydrogen_/4. + tsi.tarCarbon_/2.;
    tsi.tarProducedMolesCO_   = tsi.tarCarbon_;
    tsi.tarProducedMolesH2O_  = tsi.tarHydrogen_/2.;

    tsi.sootRequiredMolesO2_  = tsi.sootHydrogen_/4. + tsi.sootCarbon_/2.;
    tsi.sootProducedMolesCO_  = tsi.sootCarbon_;
    tsi.sootProducedMolesH2O_ = tsi.sootHydrogen_/2.;

    tsi.hasBeenSetup_ = true;

    std::cout << "\n TarAndSootInfo: properties: \n"
              << "-------------------------------------------------------\n"
              << "tar is C2H2   : " << (tarIsC2H2    ? "True" : "False")<< std::endl
              << "have soot     : " << (tsi.haveSoot_? "True" : "False")<< std::endl
              << std::endl
              << "tar hydrogen  : " << tsi.tar_hydrogen()            << std::endl
              << "tar carbon    : " << tsi.tar_carbon()              << std::endl
              << "tar MW        : " << tsi.tar_mw()                  << std::endl
              << "tar req. O2   : " << tsi.tar_required_moles_O2()   << std::endl
              << "tar prod. CO  : " << tsi.tar_produced_moles_CO()   << std::endl
              << "tar prod. H2O : " << tsi.tar_produced_moles_H2O()  << std::endl
              << "tar prod. H2  : " << tsi.tar_produced_moles_H2()   << std::endl
              << "tar deltaH_f  : " << tsi.tar_heat_of_formation()   << std::endl
              << "tar deltaH_ox : " << tsi.tar_heat_of_oxidation()   << std::endl
              << "tar diff coef : " << tsi.tar_diffusivity()         << std::endl
              << std::endl;
              if(tsi.haveSoot_){
                std::cout << "soot is carbon: " << (sootIsCarbon ? "True" : "False")<< std::endl
                          << "soot hydrogen : " << tsi.soot_hydrogen()           << std::endl
                          << "soot carbon   : " << tsi.soot_carbon()             << std::endl
                          << "soot MW       : " << tsi.soot_mw()                 << std::endl
                          << "soot req. O2  : " << tsi.soot_required_moles_O2()  << std::endl
                          << "soot prod. CO : " << tsi.soot_produced_moles_CO()  << std::endl
                          << "soot prod. H2O: " << tsi.soot_produced_moles_H2O() << std::endl
                          << "soot deltaH_ox: " << tsi.soot_heat_of_oxidation()  << std::endl
                          << "soot density  : " << tsi.soot_density()            << std::endl
                          << "soot cMin     : " << tsi.soot_cMin()               << std::endl
                          << std::endl;
              }
    std::cout << "-------------------------------------------------------\n"<< std::endl;
  }

  //------------------------------------------------------------------

  const TarAndSootInfo&
  TarAndSootInfo::get()
  {
    static const TarAndSootInfo& tsi = TarAndSootInfo::self();
    if( !tsi.hasBeenSetup_ ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "TarAndSootInfo must be setup before being used" << std::endl
                << "\n TarAndSootInfo: properties: \n"
                << "-------------------------------------------------------\n"
                << std::endl
                << "tar hydrogen  : " << tsi.tar_hydrogen()            << std::endl
                << "tar carbon    : " << tsi.tar_carbon()              << std::endl
                << "tar MW        : " << tsi.tar_mw()                  << std::endl
                << "tar req. O2   : " << tsi.tar_required_moles_O2()   << std::endl
                << "tar prod. CO  : " << tsi.tar_produced_moles_CO()   << std::endl
                << "tar prod. H2O : " << tsi.tar_produced_moles_H2O()  << std::endl
                << "tar deltaH    : " << tsi.tar_heat_of_oxidation()   << std::endl
                << "tar diff coef : " << tsi.tar_diffusivity()         << std::endl
                << std::endl;
      if(tsi.haveSoot_){
        std::cout << "soot hydrogen : " << tsi.soot_hydrogen()           << std::endl
                  << "soot carbon   : " << tsi.soot_carbon()             << std::endl
                  << "soot MW       : " << tsi.soot_mw()                 << std::endl
                  << "soot req. O2  : " << tsi.soot_required_moles_O2()  << std::endl
                  << "soot prod. CO : " << tsi.soot_produced_moles_CO()  << std::endl
                  << "soot prod. H2O: " << tsi.soot_produced_moles_H2O() << std::endl
                  << "soot deltaH_ox: " << tsi.soot_heat_of_oxidation()  << std::endl
                  << "soot density  : " << tsi.soot_density()            << std::endl
                  << "soot cMin     : " << tsi.soot_cMin()               << std::endl
                 << std::endl;
      }
      std::cout << "-------------------------------------------------------\n" << std::endl;
      throw std::runtime_error( msg.str() );
    }
    return tsi;
  }

  //------------------------------------------------------------------

  TarAndSootInfo&
  TarAndSootInfo::self()
  {
    static TarAndSootInfo tsi;
    return tsi;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_hydrogen()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarHydrogen_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_carbon()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarCarbon_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_required_moles_O2()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarRequiredMolesO2_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_produced_moles_CO()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarProducedMolesCO_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_produced_moles_H2O()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarProducedMolesH2O_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_produced_moles_H2()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarProducedMolesH2_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_mw()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarMW_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_diffusivity()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarDiffusivity_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::tar_heat_of_formation()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarHeatOfFormation_;
  }

  //------------------------------------------------------------------
  double
  TarAndSootInfo::tar_heat_of_oxidation()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.tarHeatOfOxidation_;
  }

  //------------------------------------------------------------------


  double
  TarAndSootInfo::soot_hydrogen()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootHydrogen_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_carbon()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootCarbon_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_required_moles_O2()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootRequiredMolesO2_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_produced_moles_CO()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootProducedMolesCO_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_produced_moles_H2O()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootProducedMolesH2O_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_mw()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootMW_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_density()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootDensity_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_cMin()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootCMin_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_heat_of_oxidation()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootHeatOfOxidation_;
  }

  //------------------------------------------------------------------

  double
  TarAndSootInfo::soot_heat_of_formation()
  {
    const TarAndSootInfo& tsi = TarAndSootInfo::self();
    assert(tsi.hasBeenSetup_);
    return tsi.sootHeatOfFormation_;
  }

  //------------------------------------------------------------------



/*
 * source for parameters:
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 */
