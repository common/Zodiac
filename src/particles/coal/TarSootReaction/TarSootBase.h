#ifndef TARSOOTBase_h
#define TARSOOTBase_h

#include <string>

/**
 *  \file TarSootBase.h
 *
 */

namespace Tarsoot{

  enum TarsootModel{
    ON,
    OFF,
    INVALID_TARSOOTMODEL
  };

  /**
   * @fn std::string tarsoot_model_name( const TarsootModel model )
   * @param model the tarsoot model
   * @return the string name of the model
   */
  std::string tarsoot_model_name( const TarsootModel model );

  /**
   * @fn TarsootModel tarsoot_model( const std::string& modelName )
   * @param modelName the string name of the model
   * @return the corresponding enum value
   */
  TarsootModel tarsoot_model( const std::string& modelName );

} // namespace Tarsoot

#endif // TARSOOT_h
