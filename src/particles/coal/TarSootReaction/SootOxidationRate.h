
/**
 * @file SootOxidationRate.h
 * @par Calculates the rate of soot oxidation
 * @author Hang Zhou
 */
#ifndef SootOxidationRate_h
#define SootOxidationRate_h

#include <expression/Expression.h>

namespace Tarsoot {
/**
 *  @class SootOxidationRate
 *  @ingroup Tarsoot
 *  @brief Calculates the rate of soot oxidation, which is required to
 *         calculate source terms for the transport equations  of soot
 *         mass and tar mass per gas mass (for constant pressure) or per volume (for constant volume).
 *
 *  The rate of soot formation is given as
 *  For constant pressure condition: (1/s)
 * \f[
 *     S_\mathrm{soot,form} = - SA_{\mathrm{soot}}\frac{P_{\mathrm{O_{2}}}}{T^{1/2}}A_{O,\mathrm{soot}}\exp\left(\frac{-E_{O,\mathrm{soot}}}{RT}\right),
 * \f]
 * For constant volume condition: (kg/m^3/s)
 * \f[
 *    S_\mathrm{soot,form} = - \rho SA_{\mathrm{soot}}\frac{P_{\mathrm{O_{2}}}}{T^{1/2}}A_{O,\mathrm{soot}}\exp\left(\frac{-E_{O,\mathrm{soot}}}{RT}\right),
 * \f]
 *  Here \f$ SA_{\mathrm{soot}} = \left( 36 \pi N_{soot} m_{soot}^2 / \rho_{soot}^2 \right)^{1/3}\f$
 *        It is the total surface area of the soot particles per gas mass.
 *       \f$ N_{soot} \f$ is the number of soot particle per gas mass.
 *       (assume number of carbon in soot, \f$cMin\f$, is constant.
 *       Number of soot particle equation is not solved in Zodiac, becasue it keeps increasing and causes convergence problem).
 *       \f$ m_{soot} \f$ is the mass of soot per gas mass.
 *       \f$ A_{O,\mathrm{soot}} \f$ is Arrhenius preexponential factor(kg K^{1/2}/(m^2 Pa s)) [1,2]
 *       \f$ E_{O,\mathrm{soot}}\f$ is activation energy (J/mol) [1,2]
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *  [2]    Josephson, Alexander J.; Lignell, David O.; Brown, Alexander L.; and Fletcher, Thomas H.
 *        "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 */

    template<typename FieldT>
    class SootOxidationRate
          : public Expr::Expression<FieldT> {
        DECLARE_FIELDS( FieldT, yO2_, massSoot_, press_, temp_, rho_, mmw_ )
        const double A_, E_, gasCon_, sootDens_, O2Mw_;
        const double cMin_, sootMW_, na_;
        const TarAndSootInfo& tarSootInfo_;
        const std::string reactorType_;

        /* declare operators associated with this expression here */

        SootOxidationRate( const Expr::Tag& yO2Tag,
                           const Expr::Tag& massSootTag,
                           const Expr::Tag& pressTag,
                           const Expr::Tag& tempTag,
                           const Expr::Tag& rhoTag,
                           const Expr::Tag& mmwTag,
                           const std::string& reactorType,
                           const TarAndSootInfo& tarSootInfo)
              : Expr::Expression<FieldT>(),
                reactorType_( reactorType ),
                tarSootInfo_( tarSootInfo ),
                A_       ( 1.09E+5/101325),
                E_       ( 164.5E+3     ),
                gasCon_  ( 8.3144621    ),
                sootDens_( 1950.0       ),
                O2Mw_    ( 32.0         ),
                cMin_( tarSootInfo.soot_cMin() ),
                sootMW_( tarSootInfo.soot_mw() ),
                na_( 6.02E+26 )
        {
          yO2_           = this->template create_field_request<FieldT>( yO2Tag           );
          massSoot_      = this->template create_field_request<FieldT>( massSootTag      );
          press_         = this->template create_field_request<FieldT>( pressTag         );
          temp_          = this->template create_field_request<FieldT>( tempTag          );
          rho_           = this->template create_field_request<FieldT>( rhoTag           );
          mmw_           = this->template create_field_request<FieldT>( mmwTag           );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag yO2Tag_, massSootTag_, pressTag_, tempTag_, rhoTag_, mmwTag_;
            const std::string reactorType_;
            const TarAndSootInfo& tarSootInfo_;
        public:
            /**
             *  @brief Build a SootOxidationRate expression
             *  @tparam FieldT
             *  @param resultTag the tag for the value that this expression computes
             *  @param yO2Tag mass fraction of O2
             *  @param massSootTag mass of soot per gas mass
             *  @param pressTag gas pressure
             *  @param tempTag particle temperature
             *  @param rhoTag  gas density
             *  @param mmwTag  mixture molecular weight of gas
             *  @param reactorType Reactor type
             *  @param tarSootInfo TarAndSootInfo
             */
            Builder( const Expr::Tag& resultTag,
                     const Expr::Tag& yO2Tag,
                     const Expr::Tag& massSootTag,
                     const Expr::Tag& pressTag,
                     const Expr::Tag& tempTag,
                     const Expr::Tag& rhoTag,
                     const Expr::Tag& mmwTag,
                     const std::string& reactorType,
                     const TarAndSootInfo& tarSootInfo)
                  : ExpressionBuilder( resultTag ),
                    yO2Tag_          ( yO2Tag           ),
                    massSootTag_     ( massSootTag      ),
                    pressTag_        ( pressTag         ),
                    tempTag_         ( tempTag          ),
                    rhoTag_          ( rhoTag           ),
                    mmwTag_          ( mmwTag           ),
                    reactorType_     ( reactorType      ),
                    tarSootInfo_     ( tarSootInfo      ){}

            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new SootOxidationRate<FieldT>( yO2Tag_, massSootTag_, pressTag_, tempTag_, rhoTag_, mmwTag_, reactorType_, tarSootInfo_ );
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          FieldT& result = this->value();
          const FieldT& yO2           = yO2_          ->field_ref();
          const FieldT& massSoot      = massSoot_     ->field_ref();
          const FieldT& press         = press_        ->field_ref();
          const FieldT& temp          = temp_         ->field_ref();
          const FieldT& mmw           = mmw_          ->field_ref();

          SpatFldPtr<FieldT> nSootParticle = SpatialFieldStore::get<FieldT,FieldT>( press );
          *nSootParticle <<=  na_ / ( cMin_ * sootMW_ ) * massSoot;

          if (reactorType_=="PSRConstantVolume"){
            const FieldT& rho = rho_ ->field_ref();
            result <<= cond( massSoot <= 0.0, 0.0)
                           ( - rho * pow( 36.0*M_PI * *nSootParticle * square(massSoot / sootDens_), 1.0/3.0 ) // (total surface area of soot particles)/density
                             * mmw/O2Mw_ * yO2 * press/sqrt(temp) * A_ * exp(-E_ / (gasCon_ * temp)) );
          }
          else{
            result <<= cond( massSoot <= 0.0, 0.0)
                  ( - pow( 36.0*M_PI * *nSootParticle * square(massSoot / sootDens_), 1.0/3.0 ) // (total surface area of soot particles)/density
                    * mmw/O2Mw_ * yO2 * press/sqrt(temp) * A_ * exp(-E_ / (gasCon_ * temp)) );
          }

        }
        void sensitivity( const Expr::Tag& sensVarTag ){
          using namespace SpatialOps;
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& yO2           = yO2_          ->field_ref();
          const FieldT& massSoot      = massSoot_     ->field_ref();
          const FieldT& press         = press_        ->field_ref();
          const FieldT& temp          = temp_         ->field_ref();
          const FieldT& mmw           = mmw_          ->field_ref();
          const FieldT& dyO2dv           = yO2_          ->sens_field_ref( sensVarTag );
          const FieldT& dmassSootdv      = massSoot_     ->sens_field_ref( sensVarTag );
          const FieldT& dpressdv         = press_        ->sens_field_ref( sensVarTag );
          const FieldT& dtempdv          = temp_         ->sens_field_ref( sensVarTag );
          const FieldT& dmmwdv           = mmw_          ->sens_field_ref( sensVarTag );

          SpatFldPtr<FieldT> nSootParticle = SpatialFieldStore::get<FieldT,FieldT>( press );
          SpatFldPtr<FieldT> dnSootParticledv = SpatialFieldStore::get<FieldT,FieldT>( press );
          *nSootParticle <<=  na_ / ( cMin_ * sootMW_ ) * massSoot;
          *dnSootParticledv <<=  na_ / ( cMin_ * sootMW_ ) * dmassSootdv;

          SpatFldPtr<FieldT> surfArea = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          SpatFldPtr<FieldT> dsurfAreadv = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          SpatFldPtr<FieldT> rateTerm = SpatialFieldStore::get<FieldT, FieldT>( yO2 );
          SpatFldPtr<FieldT> drateTermdv = SpatialFieldStore::get<FieldT, FieldT>( yO2 );

          *surfArea <<= cond( massSoot <= 0.0, 0.0)
                (pow( 36.0 * M_PI * *nSootParticle * square(massSoot / sootDens_), 1.0/3.0 ));
          *dsurfAreadv <<=cond(massSoot<=1e-100, 0.0)
                (1.0/3.0 * pow( 36.0*M_PI * *nSootParticle * square(massSoot / sootDens_), -2.0/3.0 )
                 * 36.0 * M_PI * (*dnSootParticledv * square(massSoot / sootDens_) + *nSootParticle * 2.0 * massSoot * dmassSootdv / (sootDens_*sootDens_)));

          *rateTerm <<= mmw/O2Mw_ * yO2 * press/sqrt(temp) * A_ * exp(-E_ / (gasCon_ * temp));
          *drateTermdv <<= ((dmmwdv/O2Mw_*yO2*press + mmw/O2Mw_*dyO2dv*press + dpressdv*mmw/O2Mw_*yO2)/sqrt(temp)
                            - mmw/O2Mw_ * yO2*press * 1.0 / 2.0 * pow(temp, -1.5) * dtempdv) * A_ * exp(-E_ / (gasCon_ * temp))
                           + mmw/O2Mw_ * yO2 * press/sqrt(temp) * A_ * exp(-E_ / (gasCon_ * temp)) * E_/(gasCon_ * temp * temp) * dtempdv;

          if (reactorType_=="PSRConstantVolume"){
            const FieldT& rho = rho_->field_ref();
            const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
            dfdv <<= cond( massSoot <= 0.0, 0.0)
                  ( - drhodv * *surfArea * *rateTerm
                    - rho * *dsurfAreadv * *rateTerm
                    - rho * *surfArea * *drateTermdv);
          }

          else{
            dfdv <<= cond( massSoot <= 0.0, 0.0)
                  ( - *dsurfAreadv * *rateTerm
                    - *surfArea * *drateTermdv);
          }
        }
    };
}

#endif // SootOxidationRate_h
