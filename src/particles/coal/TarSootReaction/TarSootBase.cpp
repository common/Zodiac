#include "TarSootBase.h"

#include <stdexcept>
#include <sstream>

namespace Tarsoot{

  std::string tarsoot_model_name( const TarsootModel model )
  {
    std::string name;
    switch (model){
      case ON                  : name="ON"     ; break;
      case OFF                 : name="OFF"    ; break;
      case INVALID_TARSOOTMODEL: name="INVALID"; break;
    }
    return name;
  }

  TarsootModel tarsoot_model( const std::string& name )
  {
    if     ( name == tarsoot_model_name( ON  ) ) return ON;
    else if( name == tarsoot_model_name( OFF ) ) return OFF;
    else{
      std::ostringstream msg;
      msg << std::endl
          << __FILE__ << " : " << __LINE__ << std::endl
          << "Unsupported tarsoot model: '" << name << "'\n\n"
          << " Supported models:"
          << "\n\t" << tarsoot_model_name( ON )
          << "\n\t" << tarsoot_model_name( OFF)
          << std::endl;
      throw std::invalid_argument( msg.str() );
    }
    return INVALID_TARSOOTMODEL;
  }
}
