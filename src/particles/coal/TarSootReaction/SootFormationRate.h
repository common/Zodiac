/**
 * @file SootFormationRate.h
 * @author Hang Zhou
 * @par Calculates the rate of soot formation
 */

#ifndef SootFormationRate_h
#define SootFormationRate_h

#include <expression/Expression.h>

namespace Tarsoot {
/**
 *  @class SootFormationRate
 *  @ingroup Tarsoot
 *  @brief Calculates the rate of soot formation, which is required to
 *         calculate source terms for the transport equations of soot
 *         mass and tar mass per gas mass (for constant pressure) or per volume (for constant volume).
 *
 *  The rate of soot formation is given as
 *  For constant pressure condition: (1/s)
 * \f[
 *     r_{\mathrm{F,soot}} = - m_{tar} * A_{F,\mathrm{soot}} * exp(-E_{F,\mathrm{soot}} / (RT)),
 * \f]
 *  For constant volume condition: (kg/m^3/s)
 * \f[
 *     r_{\mathrm{F,soot}} = - \rho m_{tar} * A_{F,\mathrm{soot}} * exp(-E_{F,\mathrm{soot}} / (RT)),
 * \f]
 * where \f$ m_{tar} \f$ is the mass of tar per gas mass.
 *       \f$T\f$ is the gas temperature.
 *       \f$\rho\f$ is the gas density.
 *       \f$A\f$ is arrhenius preexponential factor (1/s) [1,2].
 *       \f$E\f$ is activation energy (J/mol) [1,2].
 *       \f$R\f$ is gas constant (J/(mol*K)).
 *
 *
 *  The above equation assumes that all tar in the system considered
 *  is composed of monomers of a single "tar" molecule.
 *
 *  Source for parameters:
 *
 *  [1]    Brown, A. L.; Fletcher, T. H.
 *         "Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *  [2]    Josephson, Alexander J.; Lignell, David O.; Brown, Alexander L.; and Fletcher, Thomas H.
 *        "Revision to Modeling Soot Derived from Pulverized Coal"
 *         Energy & Fuels, 1998, 12, 745-757
 *
 */

  template<typename FieldT>
  class SootFormationRate : public Expr::Expression<FieldT> {
    DECLARE_FIELDS( FieldT, massTar_, temp_, rho_ )
    const double A_, E_, gasCon_, tarMw_, tarCarbon_, sootMw_, sootCarbon_;
    const TarAndSootInfo& tarSootInfo_;
    const std::string reactorType_;
    SootFormationRate( const Expr::Tag& massTarTag,
                       const Expr::Tag& tempTag,
                       const Expr::Tag& rhoTag,
                       const TarAndSootInfo& tarSootInfo,
                       const std::string reactorType)
          : Expr::Expression<FieldT>(),
            tarSootInfo_(tarSootInfo),
            reactorType_(reactorType),
            A_( 5.02E+8 ),
            E_( 198.9E+3 ),
            gasCon_( 8.3144621 ),
            tarMw_(tarSootInfo.tar_mw()),
            tarCarbon_(tarSootInfo.tar_carbon()),
            sootMw_(tarSootInfo.soot_mw()),
            sootCarbon_(tarSootInfo.soot_carbon())
    {
      massTar_ = this->template create_field_request<FieldT>( massTarTag );
      temp_ = this->template create_field_request<FieldT>( tempTag );
      rho_ = this->template create_field_request<FieldT>( rhoTag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag massTarTag_, tempTag_, rhoTag_;
      const std::string reactorType_;
      const TarAndSootInfo& tarSootInfo_;
    public:
      /**
       *  @brief Build a SootFormationRate expression
       *  @tparam FieldT
       *  @param resultTag   soot formation reaction rate
       *  @param massTarTag  mass of tar per gas mass
       *  @param tempTag     gas temperature (K)
       *  @param rhoTag      gas density
       *  @param tarSootInfo TarAndSootInfo
       *  @param reactorType reactor type
       */
      Builder( const Expr::TagList& resultTags,
               const Expr::Tag& massTarTag,
               const Expr::Tag& tempTag,
               const Expr::Tag& rhoTag,
               const TarAndSootInfo& tarSootInfo,
               const std::string reactorType)
            : ExpressionBuilder( resultTags ),
              massTarTag_( massTarTag ),
              tempTag_( tempTag ),
              rhoTag_( rhoTag ),
              tarSootInfo_( tarSootInfo ),
              reactorType_(reactorType){}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new SootFormationRate<FieldT>( massTarTag_, tempTag_, rhoTag_, tarSootInfo_, reactorType_);
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& results = this->get_value_vec();

      FieldT& tarConsump = *results[0];
      FieldT& sootForma  = *results[1];

      const FieldT& massTar = massTar_->field_ref();
      const FieldT& temp = temp_->field_ref();

      if (reactorType_ == "PSRConstantVolume"){
        const FieldT& rho = rho_->field_ref();
        tarConsump <<= cond( massTar <= 0.0, 0.0)
              ( - rho * massTar * A_ * exp( -E_ / ( gasCon_ * temp )));    // negative
      }

      else{
        tarConsump <<= cond( massTar <= 0.0, 0.0)
              ( - massTar * A_ * exp( -E_ / ( gasCon_ * temp )));    // negative
      }
        
       sootForma <<= - tarConsump/ tarMw_ * tarCarbon_ / sootCarbon_ * sootMw_; // positive
    }
    void sensitivity( const Expr::Tag& sensVarTag ){
      using namespace SpatialOps;
      const Expr::TagList& targetTags = this->get_tags();
      std::vector<FieldT> dfdvs;
      for(size_t i=0;i<targetTags.size();++i){
        dfdvs.push_back(this->sensitivity_result(targetTags[i], sensVarTag));
      }
      const FieldT& massTar = massTar_->field_ref();
      const FieldT& temp = temp_->field_ref();
      const FieldT& dmassTardv = massTar_->sens_field_ref( sensVarTag );
      const FieldT& dtempdv    = temp_->sens_field_ref( sensVarTag );

      if (reactorType_ == "PSRConstantVolume"){
        const FieldT& rho = rho_->field_ref();
        const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
        dfdvs[0] <<= cond( massTar <= 0.0, 0.0)
              (- ( drhodv * massTar + rho * dmassTardv) * A_ * exp( -E_ / ( gasCon_ * temp ))
               - rho * massTar * A_ * exp(-E_ / ( gasCon_ * temp )) * E_ / (gasCon_*temp*temp) * dtempdv);
      }

      else{
        dfdvs[0] <<= cond( massTar <= 0.0, 0.0)
              ( - dmassTardv * A_ * exp( -E_ / ( gasCon_ * temp ))
                - massTar * A_ * exp(-E_ / ( gasCon_ * temp )) * E_ / (gasCon_*temp*temp) * dtempdv);
      }

      dfdvs[1] <<= - dfdvs[0] / tarMw_ * tarCarbon_ / sootCarbon_ * sootMw_;
    }
  };
}

#endif // SootFormationRate_h
