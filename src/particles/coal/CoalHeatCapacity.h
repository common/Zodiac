
/**
 * @file CoalHeatCapacity.h
 * @author Hang
 */

#ifndef CoalHeatCapacity_coal_h
#define CoalHeatCapacity_coal_h

#include <expression/Expression.h>

namespace Coal {

/**
 * @class: CoalHeatCapacity
 * @ingroup: Coal
 * @brief Getting the heat capacity of coal particle
 * @author Hang
 *
 * The heat capacity of coal particle (J/kg/K) is given as
 * \f[
 *    Cp_{coal} = \frac{m_w Cp_w + m_v Cp_v + m_c Cp_c + m_{ash} Cp_{ash}}{m_p}
 * \f]
 * Here, the subscripts \f$w\f$, \f$v\f$, \f$c\f$ and \f$ash\f$ represent moisture, volatile, char ans ash.
 * The heat capacity of these compositions are given as
 * \f[
 *    Cp_w = 4200 J/kg/K
 * \f]
 * \f[
 *    Cp_v = 1500.5 + 2.9725 T_p
 * \f]
 * \f[
 *    Cp_c = \frac{R}{M_{w,c} \left[ f1 + 2f2 \right]
 * \f]
 * Here, \f$M_{w,c}\f$ is the molecular weight of char. \f$R\f$ is the gas constant.
 * \f[
 *    f1 = \frac{(380.0/T_p)^2 e^{380.0/T_p}}{(e^{380.0/T_p} - 1)^2}
 * \f]
 * \f[
 *    f2 = \frac{(1800.0/T_p)^2 e^{1800.0/T_p}}{(e^{1800.0/T_p} - 1)^2}
 * \f]
 * \f[
 *    Cp_{ahs} = 594 + 0.586T_p
 * \f]
 *
 * The sensitivity of this heat capacity is given as
 * \f[
 *    \frac{\partial Cp_{coal}}{\partial \phi} = (\frac{\partial m_w}{\partial \phi} Cp_w + m_w \frac{\partial Cp_w}{\partial \phi})
 *                                             + (\frac{\partial m_v}{\partial \phi} Cp_v + m_v \frac{\partial Cp_v}{\partial \phi})
 *                                             + (\frac{\partial m_c}{\partial \phi} Cp_c + m_c \frac{\partial Cp_c}{\partial \phi})
 *                                             + (\frac{\partial m_{ahs}}{\partial \phi} Cp_{ahs} + m_{ahs} \frac{\partial Cp_{ahs}}{\partial \phi})
 * \f]
 *
 *   Refrences :
 *
 *   [1] MacDonald, Rosemary a., Jane E. Callanan, and Kathleen M. McDermott.
 *       Heat capacity of a medium-volatile bituminous premium coal from 300 to 520 K.
 *       Comparison with a high-volatile bituminous nonpremium coal,
 *       Energy & Fuels 1, no. 6 (November 1987): 535-540.
 *       http://pubs.acs.org/doi/abs/10.1021/ef00006a014.
 *
 *
 *   [2] W. Eisermann, P. Johnson, W.L. Conger, Estimating thermodynamic properties of
 *       coal, char, tar and ash,Fuel Processing Technology, Volume 3,
 *       Issue 1, January 1980, Pages 39-53
 *       http://www.sciencedirect.com/science/article/pii/0378382080900223
 *
 */

  template< typename FieldT >
  class CoalHeatCapacity: public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, mvol_, mchar_, mmois_, prtmass_, tempP_ )

      CoalHeatCapacity( const Expr::Tag& mvolTag,
                        const Expr::Tag& mcharTag,
                        const Expr::Tag& mmoistTag,
                        const Expr::Tag& prtmassTag,
                        const Expr::Tag& tempPTag )
            : Expr::Expression<FieldT>(){
        this->set_gpu_runnable( true );

        mvol_ = this->template create_field_request<FieldT>( mvolTag );
        mchar_ = this->template create_field_request<FieldT>( mcharTag );
        mmois_ = this->template create_field_request<FieldT>( mmoistTag );
        prtmass_ = this->template create_field_request<FieldT>( prtmassTag );
        tempP_ = this->template create_field_request<FieldT>( tempPTag );
      }

  public:
    class Builder : public Expr::ExpressionBuilder {
        const Expr::Tag mvolTag_, mcharTag_, mmoistTag_, prtmassTag_, tempPTag_;
    public:
      /**
       * @brief The mechanism for building a CoalHeatCapacity object
       * @tparam FieldT
       * @param coalCpTag Heat capacity of coal particle
       * @param mvolTag Mass of volatile in coal particle (per gas mass)
       * @param mcharTag Mass of char in coal particle (per gas mass)
       * @param mmoistTag Mass of moisture in coal particle (per gas mass)
       * @param prtmassTag Mass of coal particle (per gas mass)
       * @param tempPTag Temperature of coal particle
       */
        Builder( const Expr::Tag& coalCpTag,
                 const Expr::Tag& mvolTag,
                 const Expr::Tag& mcharTag,
                 const Expr::Tag& mmoistTag,
                 const Expr::Tag& prtmassTag,
                 const Expr::Tag& tempPTag )
              : ExpressionBuilder( coalCpTag ),
                mvolTag_( mvolTag ),
                mcharTag_( mcharTag ),
                mmoistTag_( mmoistTag ),
                prtmassTag_( prtmassTag ),
                tempPTag_( tempPTag ){}

        ~Builder(){}

        Expr::ExpressionBase* build() const{
          return new CoalHeatCapacity<FieldT>( mvolTag_, mcharTag_, mmoistTag_, prtmassTag_, tempPTag_ );
        }
    };

    void evaluate(){
      using namespace SpatialOps;
      FieldT& cp = this->value();

      const FieldT& mvol = mvol_->field_ref();
      const FieldT& mchar = mchar_->field_ref();
      const FieldT& mmois = mmois_->field_ref();
      const FieldT& prtmass = prtmass_->field_ref();
      const FieldT& tempP = tempP_->field_ref();

      SpatFldPtr<FieldT> g1 = SpatialFieldStore::get<FieldT, FieldT>( cp );
      *g1 <<= ( exp( 380.0 / tempP ) - 1.0 ) / ( 380.0 / tempP );
      *g1 <<= exp( 380.0 / tempP ) / ( *g1 * *g1 );

      SpatFldPtr<FieldT> g2 = SpatialFieldStore::get<FieldT, FieldT>( cp );
      *g2 <<= ( exp( 1800.0 / tempP ) - 1.0 ) / ( 1800.0 / tempP );
      *g2 <<= exp( 1800.0 / tempP ) / ( *g2 * *g2 );

      SpatFldPtr<FieldT> ashMass = SpatialFieldStore::get<FieldT, FieldT>( cp );
      *ashMass <<= prtmass - ( mchar + mmois + mvol );

      cp <<= (
                   cond( mchar > 0.0, ( 8.314 / 12.0 * ( *g1 + 2.0 * *g2 )) * mchar )
                         ( 0.0 )
                   + cond( mvol > 0.0, ( 1.5005 + 2.9725E-3 * tempP ) * mvol ) //  (1.2 * mvol)
                         ( 0.0 )
                   + cond( mmois > 0.0, 4.2 * mmois )
                         ( 0.0 )
                   + ( 0.594 + 5.86E-4 * tempP ) * *ashMass
             ) * 1000.0 / prtmass;
    }

    void sensitivity( const Expr::Tag& sensVarTag ){
      using namespace SpatialOps;
      FieldT& dfdv = this->sensitivity_result( sensVarTag );
      const FieldT& mvol = mvol_->field_ref();
      const FieldT& mchar = mchar_->field_ref();
      const FieldT& mmois = mmois_->field_ref();
      const FieldT& prtmass = prtmass_->field_ref();
      const FieldT& tempP = tempP_->field_ref();
      const FieldT& dmvoldv = mvol_->sens_field_ref( sensVarTag );
      const FieldT& dmchardv = mchar_->sens_field_ref( sensVarTag );
      const FieldT& dmmoisdv = mmois_->sens_field_ref( sensVarTag );
      const FieldT& dprtmassdv = prtmass_->sens_field_ref( sensVarTag );
      const FieldT& dtempPdv = tempP_->sens_field_ref( sensVarTag );

      SpatFldPtr<FieldT> g1 = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> g2 = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dg1dv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dg2dv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dchardv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dvoldv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dmoisdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );
      SpatFldPtr<FieldT> dashdv = SpatialFieldStore::get<FieldT, FieldT>( mvol );

      *g1 <<= ( exp( 380.0 / tempP ) - 1.0 ) / ( 380.0 / tempP );
      *g1 <<= exp( 380.0 / tempP ) / ( *g1 * *g1 );
      *g2 <<= ( exp( 1800.0 / tempP ) - 1.0 ) / ( 1800.0 / tempP );
      *g2 <<= exp( 1800.0 / tempP ) / ( *g2 * *g2 );

      *dg1dv <<= exp( 380.0 / tempP ) * square( 380.0 / tempP ) / square( exp( 380.0 / tempP ) - 1.0 )
                 * ( -380.0 / square( tempP ) - 2.0 / tempP +
                     2.0 * exp( 380.0 / tempP ) * 380.0 / square( tempP ) / ( exp( 380.0 / tempP ) - 1.0 )) *
                 dtempPdv;
      *dg2dv <<= exp( 1800.0 / tempP ) * square( 1800.0 / tempP ) / square( exp( 1800.0 / tempP ) - 1.0 )
                 * ( -1800.0 / square( tempP ) - 2.0 / tempP +
                     2.0 * exp( 1800.0 / tempP ) * 1800.0 / square( tempP ) / ( exp( 1800.0 / tempP ) - 1.0 )) *
                 dtempPdv;

      *dchardv <<= (( 8.314 / 12.0 * ( *dg1dv + 2.0 * *dg2dv ) - 5.86E-4 * dtempPdv ) * mchar
                    + ( 8.314 / 12.0 * ( *g1 + 2.0 * *g2 ) - ( 0.594 + 5.86E-4 * tempP )) * dmchardv) * 1000.0 / prtmass
                   - ( 8.314 / 12.0 * ( *g1 + 2.0 * *g2 ) - ( 0.594 + 5.86E-4 * tempP )) * mchar * 1000.0 / square( prtmass ) * dprtmassdv;
      *dvoldv <<= (( 2.9725E-3 - 5.86E-4 ) * dtempPdv * mvol
                  + ( 1.5005 + 2.9725E-3 * tempP - ( 0.594 + 5.86E-4 * tempP )) * dmvoldv) * 1000.0 / prtmass
                  - ( 1.5005 + 2.9725E-3 * tempP - ( 0.594 + 5.86E-4 * tempP )) * mvol * 1000.0 / square( prtmass ) * dprtmassdv;
      *dmoisdv <<= ( -5.86E-4 * dtempPdv * mmois
                    + ( 4.2 - ( 0.594 + 5.86E-4 * tempP )) * dmmoisdv) * 1000.0 / prtmass
                  - ( 4.2 - ( 0.594 + 5.86E-4 * tempP )) * mmois * 1000.0 / square( prtmass ) * dprtmassdv;
      *dashdv <<= 5.86E-4 * 1000.0 * dtempPdv;

      dfdv <<= ( cond( mchar > 0.0, *dchardv )
                     ( 0.0 )
                + cond( mvol > 0.0, *dvoldv )
                      ( 0.0 )
                + cond( mmois > 0.0, *dmoisdv )
                      ( 0.0 )
                + *dashdv);
    }
  };
} // namespace Coal

#endif // CoalHeatCapacity_coal_h
