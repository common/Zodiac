/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   ParticleInterface.h
 *  \date   Jun 12, 2018
 *  \author hang
 */

#ifndef PARTICLE_INTERFACE_UTIL_H_
#define PARTICLE_INTERFACE_UTIL_H_

#include <expression/ExprLib.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include <spatialops/structured/SpatialFieldStore.h>

#include "FieldTypes.h"
#include "Vaporization/VaporizationInterface.h"
#include "coal/CoalInterface.h"

namespace Particles {
  /**
   * @class ParticleEnsembleTags
   * @ingroup Particles
   * @brief Getting tags used for particle setup
   * @author Hang Zhou
   */
  class ParticleEnsembleTags {
  public:
    const Expr::Tag partTempInitTag,       ///< particle initial temperature from input file
                    partTempInflowTag,     ///< inflowing particle temperature
                    partRhoInitTag,        ///< particle initial density from input file
                    partRhoInflowTag,      ///< inflow particle density
                    partCpInitTag,         ///< particle initial heat capacity from input file, mainly for solid and liquid particles
                    partConvecToGasTag,    ///< convection between gas and part in gas enthalpy equation
                    partRadiaToGasTag,     ///< radiation between gas and part in gas enthalpy equation
                    partConvecToGasPFRTag, ///< convection between gas and part in gas enthalpy equation for plug flow reactor
                    partRadiaToGasPFRTag,  ///< radiation between gas and part in gas enthalpy equation for plug flow reactor
                    tempRadiaWallTag,      ///< wall temperature having radiation heat transfer with particles
                    partToGasMassTag,      ///< mass from particle to gas
                    gasEnthSrcTag,         ///< gas enthalpy source term because of the change of gas mass
                    gasEnthSrcPFRTag,      ///< gas enthalpy source term because of the change of gas mass for plug flow reactor
                    gasDensitySrcPFRTag,   ///< gas density source term because of the change of gas mass for plug flow reactor
                    gasDpDxSrcPFRTag;      /// change of `Dp/Dx` term because of the change of gas mass for plug flow reactor
    Expr::TagList massEachPartTags,        ///< mass of each particle
                  partNumPerVolumeTags,    ///< number of particles per volume
                  partNumFracTags,         ///< number of particles per mass of gas in the reactor
                  partTempTags,            ///< particle temperature
                  partCpTags,              ///< particle heat capacity
                  partMassFracTags,        ///< particle mass fraction: (kg particle)/(kg gas)
                  partRhoTags,             ///< particle density
                  partSizeTags,            ///< particle diameter
                  partConvecTags,          ///< convection term in particle temperature equation
                  partGasRadiaTags,        ///< radiation term between particle and gas-phase in particle temperature equation
                  partWallRadiaTags,       ///< radiation term between particle and wall in particle temperature equation
                  partTempInflowTags,      ///< inflow particle temperature
                  partTempMixRhsTags,      ///< mixing RHS of particle enthalpy equation
                  partTempKinRhsTags,      ///< kinetics RHS of particle enthalpy equation
                  partTempHeatRhsTags,     ///< heat transfer RHS of particle enthalpy equation
                  partTempKinRhsPFRTags,   ///< kinetics RHS of particle enthalpy equation for plug flow reactor
                  partTempHeatRhsPFRTags,  ///< heat transfer RHS of particle enthalpy equation for plug flow reactor
                  partTempFullRhsTags,     ///< full RHS of particle enthalpy equation
                  partMassFracInitialTags, ///< initial particle mass fraction
                  partMassFracInflowTags,  ///< inflowing particle mass fraction
                  partMassFracMixRhsTags,  ///< mixing RHS of particle mass fraction equation
                  partMassFracKinRhsTags,  ///< kinetics RHS of particle mass fraction equation
                  partMassFracFullRhsTags, ///< full RHS of particle mass fraction equation
                  partNumFracInflowTags,   ///< inflowing number of particles per mass of gas
                  partNumFracMixRhsTags,   ///< mixing RHS of particle number equation per mass of gas
                  partNumFracKinRhsTags,   ///< kinetics RHS of particle number equation per mass of gas
                  partNumFracFullRhsTags,  ///< full RHS of number of particles equation per mass of gas
                  shNumberTags,            ///< sherwood number --  used to get mass transfer coefficient
                  reynoldsNumberTags,      ///< particle Re number
                  nusseltNumberTags,       ///< Nu number --  used to get convective coefficient between gas and particle
                  gasSpeciesSrcTags,       ///< gas speices source term because of the change of gas mass
                  gasSpeciesSrcPFRTags;    ///< gas speices source term because of the change of gas mass in plug flow reactor
      ParticleEnsembleTags( const Expr::Context& state,
                            const std::vector<std::string>& particleNumArray);
  };
  /**
   *  @ingroup Particles
   *  @class ParticleSetup
   *  @brief Provides an interface to the particle setup
   */
  class ParticleSetup {

      std::vector<std::string>& particleNumArray_;
      const YAML::Node& rootParser_;
      std::set<Expr::ExpressionID>& initRoots_;
      Expr::ExpressionFactory& initFactory_;
      Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator_;
      Expr::TagList& primitiveTags_;
      Expr::TagList& kinRhsTags_;
      int& equIndex_;
      Vap::Vaporization* Vaporization_;
      Coal::CoalInterface* CoalInterface_;
      bool liquidimplement_;  // if true, it means that there are liquid particles
      bool coalimplement_;  // if true, it means that there are coal particles
      const bool isRestart_;
      double npar_, nparInflow_; // number of particles per gas mass
      int nparsize_; // number of particle size
      std::string reactorType_;

  public:
    /**
     * @param particleNumArray list of particle numbers with different sizes
     * @param rootParser yaml node read from input file
     * @param initRoots initial root in initial tree
     * @param initFactory initial expression factory
     * @param integrator time integrator used in Zodiac
     * @param primitiveTags list of primitive parameters
     * @param kinRhsTags list of reaction RHS
     * @param equIndex index of equation
     * @param isRestart if restart or not (bool)
     */
      ParticleSetup( std::vector<std::string>& particleNumArray,
                     const YAML::Node& rootParser,
                     std::set<Expr::ExpressionID>& initRoots,
                     Expr::ExpressionFactory& initFactory,
                     Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT>* integrator,
                     Expr::TagList& primitiveTags,
                     Expr::TagList& kinRhsTags,
                     int& equIndex,
                     const bool isRestart);

      ~ParticleSetup();

      /**
       * @brief Adding required variables needed to restart for particle setup
       * @param restartVars variables needed to restart
       */
      void restart_var_particle(std::vector<std::string>& restartVars);

      /**
       * @brief Initialize particle setup
       */
      void initialize_particle( );

      /**
       * @brief Modify primVarIdxMap and kinRhsIdxMap based on variables and equations solved for particle setup
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       * @param kinRhsIdxMap kinetic rhs indices
       * @param rhsIdxMap rhs tags indices
       */
      void modify_idxmap( std::map<Expr::Tag, int>& primVarIdxMap,
                          const std::map<Expr::Tag, int>& consVarIdxMap,
                          std::map<Expr::Tag, int>& kinRhsIdxMap,
                          const std::map<Expr::Tag, int>& rhsIdxMap );

      typedef boost::shared_ptr<Expr::matrix::SparseMatrix<FieldT>> sharePtrT;
      /**
       * @brief Generate particle state transformation matrix
       * @param primVarIdxMap primitive variables indices
       * @param consVarIdxMap solved variables indices
       */
      sharePtrT dvdu_part( std::map<Expr::Tag, int> primVarIdxMap,
                           std::map<Expr::Tag, int> consVarIdxMap);

      /**
       * @brief Generate particle heat transfer RHS term matrix (dQdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       * @return
       */
      sharePtrT dqdv_part( std::map<Expr::Tag, int> primVarIdxMap,
                           std::map<Expr::Tag, int> rhsIdxMap,
                           std::map<Expr::Tag, Expr::Tag> consVarRhsMap);
      /**
       * @brief Generate particle mixing rhs matrix (dmixPartdV)
       * @param dmMixingdVPart particle mixing rhs matrix (dmixPartdV)
       * @param primVarIdxMap primitive variables indices
       * @param rhsIdxMap rhs tags indices
       * @param consVarRhsMap solved variables rhs tags indices
       */
      sharePtrT dmMixingdv_part( std::map<Expr::Tag, int> primVarIdxMap,
                                 std::map<Expr::Tag, int> rhsIdxMap,
                                 std::map<Expr::Tag, Expr::Tag> consVarRhsMap );

  private:

      /**
      * @brief Set up initial conditions for all the particle variables
      */

      void setup_initial_conditions_particle( );

      /**
       * @brief Build equation system for particles
       */
      void build_equation_system_particle( );


      /**
       * \brief  Generate a vector of double with the size of particles.
       *
       *  \param  params            : A yaml class including the required node
       *  \param  particleSizeArray : a vector of double with size of npar
       *  \param  npar              : number of particle sizes
       */
      void particle_size( const YAML::Node& params,
                          std::vector<double>& particleSizeArray,
                          const int& npar );

      /**
       * @brief parser paticle parameters from the input file
       */
      void parse_particle_parameters();

      /**
       * @brief Modify primitiveTags and kinRhsTags based on variables and equations solved in particle setup
       */
      void modify_prim_kinrhs_tags();

      /**
       * @brief setup coal interface if `coal` is specified in the input file
       */
      void setup_coal_subprocess();
  };

} // namespace ParticleEnsembleUtil

#endif /* PARTICLE_INTERFACE_UTIL_H_ */