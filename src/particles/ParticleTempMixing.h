
/**
 *  @file    ParticleTempMixing.h
 *  @par     Getting the mixing term in particle temperature equation, not coal particle
 *  @author Hang
 */

#ifndef PARTICLETEMPMIXING_H_
#define PARTICLETEMPMIXING_H_

#include <expression/Expression.h>

namespace Particles{

    /**
     * @class ParticleTempMixing
     * @ingroup Particles
     * @brief Getting the mixing term in particle temperature equation, not coal particle
     * @author Hang
     *
     * The mixing term \f$ W \f$ is given as
     * \f[
     *     W = \frac{\rho_{in} Y_{p,in}}{\rho \tau_{mix} Y_p}(T_{in,p}-T_p)
     * \f]
     *
     * Here, \f$\rho_{in}\f$ and \f$\rho\f$ are the density of the gas in the inflow stream and in the reactor.
     *       \f$Y_{p,in}\f$ and \f$Y_p\f$ are the mass of the particle per gas mass in the inflow stream and in the reactor.
     *       \f$T_{in,p}\f$ and \f$T_p\f$ are the particle temperature in the inflow stream and in the reactor.
     *       \f$\tau_{mix}\f$ is the residence time.
     */

    template< typename FieldT >
    class ParticleTempMixing : public Expr::Expression<FieldT> {

        DECLARE_FIELDS( FieldT, tempPart_, tempPartIn_, partMassFracIn_, partMassFrac_, mixingTime_, rhoInflow_, rho_);
      ParticleTempMixing( const Expr::Tag& tempPartTag,
                          const Expr::Tag& tempPartInTag,
                          const Expr::Tag& partMassFracTag,
                          const Expr::Tag& partMassFracInTag,
                          const Expr::Tag& mixingTimeTag,
                          const Expr::Tag& rhoInflowTag,
                          const Expr::Tag& rhoTag )
              : Expr::Expression<FieldT>()
        {
          this->set_gpu_runnable(true);
          tempPart_       = this->template create_field_request<FieldT>( tempPartTag       );
          tempPartIn_     = this->template create_field_request<FieldT>( tempPartInTag     );
          partMassFrac_   = this->template create_field_request<FieldT>( partMassFracTag   );
          partMassFracIn_ = this->template create_field_request<FieldT>( partMassFracInTag );
          mixingTime_     = this->template create_field_request<FieldT>( mixingTimeTag     );
          rhoInflow_      = this->template create_field_request<FieldT>( rhoInflowTag      );
          rho_            = this->template create_field_request<FieldT>( rhoTag            );
        }

    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag tempPartTag_, tempPartInTag_, partMassFracInTag_, partMassFracTag_, mixingTimeTag_, rhoInflowTag_, rhoTag_;
        public:
          /**
           * @brief The mechanism for building a ZeroDMixingCP object
           * @tparam FieldT
           * @param mixingTag Mixing term in gas phase equations under constant pressure conditions
           * @param inflowTag Inflowing value of the variable that the equation solves
           * @param varTag Value of the variable in the reactor and the outlet
           * @param mixingTimeTag Residence time based on inlet volume flow rate for gas phase \f$\tau_{mix} = \frac{V}{\dot V_{in}}\f$
           * @param rhoInflowTag Inflowing density value of gas phase
           * @param rhoTag Density value of gas phase in the reactor and the outlet
           *
           */
            Builder( const Expr::Tag& mixingTag,
                     const Expr::Tag& tempPartTag,
                     const Expr::Tag& tempPartInTag,
                     const Expr::Tag& partMassFracTag,
                     const Expr::Tag& partMassFracInTag,
                     const Expr::Tag& mixingTimeTag,
                     const Expr::Tag& rhoInflowTag,
                     const Expr::Tag& rhoTag )
                  : Expr::ExpressionBuilder( mixingTag ),
                    tempPartTag_(tempPartTag),
                    tempPartInTag_(tempPartInTag),
                    partMassFracTag_(partMassFracTag),
                    partMassFracInTag_(partMassFracInTag),
                    mixingTimeTag_( mixingTimeTag ),
                    rhoInflowTag_( rhoInflowTag ),
                    rhoTag_( rhoTag )
            {}

            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new ParticleTempMixing<FieldT>( tempPartTag_, tempPartInTag_, partMassFracTag_, partMassFracInTag_,
                                                     mixingTimeTag_, rhoInflowTag_, rhoTag_);
            }
        };

        void evaluate()
        {
          using namespace SpatialOps;
          this->value() <<= rhoInflow_->field_ref() * partMassFracIn_->field_ref()
                           / (rho_->field_ref() * partMassFrac_->field_ref() * mixingTime_->field_ref() )
                            * ( tempPartIn_->field_ref() - tempPart_->field_ref() );
        }

        void sensitivity( const Expr::Tag& sensVarTag )
        {
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& tempPIn        = tempPartIn_->field_ref();
          const FieldT& tempP          = tempPart_->field_ref();
          const FieldT& massFracPIn    = partMassFracIn_->field_ref();
          const FieldT& massFracP      = partMassFrac_->field_ref();
          const FieldT& tau            = mixingTime_->field_ref();
          const FieldT& rhoInflow      = rhoInflow_->field_ref();
          const FieldT& rho            = rho_->field_ref();
          const FieldT& dtempPIndv     = tempPartIn_->sens_field_ref(sensVarTag);
          const FieldT& dtempPdv       = tempPart_->sens_field_ref(sensVarTag);
          const FieldT& dmassFracPIndv = partMassFracIn_->sens_field_ref(sensVarTag);
          const FieldT& dmassFracPdv   = partMassFrac_->sens_field_ref(sensVarTag);
          const FieldT& dtaudv         = mixingTime_->sens_field_ref( sensVarTag );
          const FieldT& drhoIndv       = rhoInflow_->sens_field_ref( sensVarTag );
          const FieldT& drhodv         = rho_->sens_field_ref( sensVarTag );

          dfdv <<= (drhoIndv*massFracPIn+rhoInflow*dmassFracPIndv)*(tempPIn-tempP)/(rho*massFracP*tau)
                   + rhoInflow*massFracPIn*(dtempPIndv-dtempPdv)/(rho*massFracP*tau)
                   - rhoInflow*massFracPIn*(tempPIn-tempP)*(drhodv/rho+dmassFracPdv/massFracP+dtaudv/tau)/(rho*massFracP*tau);
        }
    };

} // namespace Particles

#endif /* PARTICLETEMPMIXING_H_ */
