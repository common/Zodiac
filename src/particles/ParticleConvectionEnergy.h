/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * @file ParticleConvectionEnergy.h
 * @par Getting the convection heat transfer between gas and particle
 * @author Hang
 */

#ifndef PARTICLETEMPERATURECONVECTION_H_
#define PARTICLETEMPERATURECONVECTION_H_

#include <expression/Expression.h>

namespace Particles{

  /**
   *  @class ConvectionPart
   *  @ingroup Particles
   *  @brief Evaluating the convective heat transfer term between gas and particle in particle temperature equation.
   *  @author Hang
   *
   * The convection heat transfer term is given as
   * \f[
   *    Q_{p,cov} = \frac{A_{p,total} k_{gp} (T - T_p) }{\rho V Y_p cp_p}
   * \f]
   * Here, \f$ A_{p,total} = N_p \pi d_p^2 \f$ is the total surface area of particles with this size.
   *       \f$ k_{gp} = \frac{Nu \lambda}{d_p} \f$ is the convective heat transfer coefficient between gas and particle.
   *       \f$ \lambda \f$ is the conductive heat transfer coefficient of gas phase.
   *       \f$ d_p \f$ is the particle diameter.
   *       \f$ N_p \f$ is the number of particle with this size.
   *       \f$ Y_p \f$ is the particle mass fraction (kg particle/kg gas).
   *
   * Therefore, the final equation for this term is
   * \f[
   *    Q_{p,cov} = \frac{\pi N_p d_p Nu \lambda (T- T_p)}{\rho V Y_p cp_p}
   * \f]
   *
   * The sensitivity of this term is given as
   * \f[
   *    \frac{Q_{p,cov}}{\phi} = \frac{\pi N_p d_p Nu \lambda (T- T_p)} {\rho V Y_p cp_p}
   *                                ( \frac{\frac{\partial N_p}{\partial \phi}}{N_p}
   *                                   + \frac{\frac{\partial d_p}{\partial \phi}}{d_p}
   *                                   + \frac{\frac{\partial Nu}{\partial \phi}}{Nu}
   *                                   + \frac{\frac{\partial \lambda}{\partial \phi}}{\lambda}
   *                                   + \frac{\frac{\partial T}{\partial \phi}-\frac{\partial T_p}{\partial \phi}}{T-T_p}
   *                                   - \frac{\frac{\partial \rho}{\partial \phi}}{\rho}
   *                                   - \frac{\frac{\partial Y_p}{\partial \phi}}{Y_p}
   *                                   - \frac{\frac{\partial cp_p}{\partial \phi}}{cp_p})
   * \f]
   *
   *  NOTE: This expression calculates the source term from all particles with ONE specific particle size.
   *        All the parameters used here are the value for ONE specific particle size.
   */

  template< typename FieldT >
  class ConvectionPart : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, temp_, tempPart_, sizePart_, thermalConduc_, nunumber_, massFracPart_, cpPart_, numPart_ );

    ConvectionPart( const Expr::Tag tempTag,
                    const Expr::Tag tempPartTag,
                    const Expr::Tag sizePartTag,
                    const Expr::Tag thermalConducTag,
                    const Expr::Tag nunumberTag,
                    const Expr::Tag massFracPartTag,
                    const Expr::Tag cpPartTag,
                    const Expr::Tag numPartTag)
          : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);
      temp_          = this->template create_field_request<FieldT>( tempTag          );
      tempPart_      = this->template create_field_request<FieldT>( tempPartTag      );
      sizePart_      = this->template create_field_request<FieldT>( sizePartTag      );
      thermalConduc_ = this->template create_field_request<FieldT>( thermalConducTag );
      nunumber_      = this->template create_field_request<FieldT>( nunumberTag      );
      massFracPart_  = this->template create_field_request<FieldT>( massFracPartTag  );
      cpPart_        = this->template create_field_request<FieldT>( cpPartTag        );
      numPart_       = this->template create_field_request<FieldT>( numPartTag       );

    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag tempTag_, tempPartTag_, sizePartTag_, thermalConducTag_;
      const Expr::Tag nunumberTag_, massFracPartTag_, cpPartTag_, numPartTag_;
    public:
      /**
       * @brief The mechanism for building a ConvectionPart object
       * @tparam FieldT
       * @param partConvecTag Convective heat transfer term between gas and particle in particle temperature equation
       * @param tempTag Gas phase temperature
       * @param tempPartTag Particle temperature
       * @param sizePartTag Particle size/diameter
       * @param thermalConducTag Conductive heat transfer coefficient of gas phase
       * @param nunumberTag Nusselt number
       * @param massPartTag Particle mass fraction (particle mass per gas mass)
       * @param cpPartTag Particle specific heat capacity
       * @param numPartTag number of particles per gas mass
       */
      Builder( const Expr::Tag partConvecTag,
               const Expr::Tag tempTag,
               const Expr::Tag tempPartTag,
               const Expr::Tag sizePartTag,
               const Expr::Tag thermalConducTag,
               const Expr::Tag nunumberTag,
               const Expr::Tag massFracPartTag,
               const Expr::Tag cpPartTag,
               const Expr::Tag numPartTag)
            : ExpressionBuilder(partConvecTag),
              tempTag_         ( tempTag          ),
              tempPartTag_     ( tempPartTag      ),
              sizePartTag_     ( sizePartTag      ),
              thermalConducTag_( thermalConducTag ),
              nunumberTag_     ( nunumberTag      ),
              massFracPartTag_ ( massFracPartTag  ),
              cpPartTag_       ( cpPartTag        ),
              numPartTag_      ( numPartTag       )
      {}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new ConvectionPart<FieldT>( tempTag_, tempPartTag_, sizePartTag_, thermalConducTag_,
                                           nunumberTag_, massFracPartTag_, cpPartTag_, numPartTag_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      const FieldT& temp        = temp_->field_ref();
      const FieldT& tempPart    = tempPart_->field_ref();
      const FieldT& sizePart    = sizePart_->field_ref();
      const FieldT& thermalCond = thermalConduc_->field_ref();
      const FieldT& Nu          = nunumber_->field_ref();
      const FieldT& massFracPart= massFracPart_->field_ref();
      const FieldT& cpPart      = cpPart_->field_ref();
      const FieldT& numPart     = numPart_->field_ref();
      // numPart is number fraction of particles per gas mass. It has been divided by rho*V.
      this->value() <<= M_PI * numPart * sizePart * Nu * thermalCond * ( temp - tempPart )
            / (massFracPart * cpPart);
    }

    void sensitivity( const Expr::Tag& var )
    {
      using namespace SpatialOps;
      FieldT & dfdv = this->sensitivity_result( var );
      const FieldT& temp        = temp_->field_ref();
      const FieldT& tempPart    = tempPart_->field_ref();
      const FieldT& sizePart    = sizePart_->field_ref();
      const FieldT& thermalCond = thermalConduc_->field_ref();
      const FieldT& Nu          = nunumber_->field_ref();
      const FieldT& massFracPart= massFracPart_->field_ref();
      const FieldT& cpPart      = cpPart_->field_ref();
      const FieldT& numPart     = numPart_->field_ref();

      const FieldT& dTdv         = temp_->sens_field_ref(var);
      const FieldT& dTPartdv     = tempPart_->sens_field_ref(var);
      const FieldT& dsizedv      = sizePart_->sens_field_ref(var);
      const FieldT& dkdv         = thermalConduc_->sens_field_ref(var);
      const FieldT& dnudv        = nunumber_->sens_field_ref(var);
      const FieldT& dmassFracPdv = massFracPart_->sens_field_ref(var);
      const FieldT& dcpPdv       = cpPart_->sens_field_ref(var);
      const FieldT& dnumPartdv   = numPart_->sens_field_ref(var);

      dfdv <<=  M_PI * (dnumPartdv*sizePart*Nu*thermalCond*(temp-tempPart) + numPart*dsizedv*Nu*thermalCond*(temp-tempPart)
                        +numPart*sizePart*dnudv*thermalCond*(temp-tempPart) + numPart*sizePart*Nu*dkdv*(temp-tempPart)
                        +numPart*sizePart*Nu*thermalCond*(dTdv-dTPartdv)) / (massFracPart*cpPart)
                - M_PI * numPart*sizePart*Nu*thermalCond*(temp-tempPart)/ square(massFracPart * cpPart)
                 * (dmassFracPdv*cpPart+massFracPart*dcpPdv);
    }
  };

} // Particles

#endif /* PARTICLETEMPERATURECONVECTION_H_ */
