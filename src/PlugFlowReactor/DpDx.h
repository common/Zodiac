/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   DpDx.h
 *  @par    Getting `Dp/Dx` term in continuity and momentum equation for plug flow reactor
 *  @authors Hang
 */

#ifndef DpDx_H_
#define DpDx_H_

#include <expression/Expression.h>

namespace PFR{

    /**
     * @class DpDx
     * @ingroup PFR
     * @brief Getting `Dp/Dx` term in continuity and momentum equation for plug flow reactor
     * @author Hang
     *
     * Ideal Gas EOS is used to get \f$\frac{Dp}{Dx}$\f. From ideal gas EOS, we can get \f$p=\frac{\rho RT}{M}\f$.
     * \f[
     *    \frac{Dp}{Dx} = \frac{RT}{M}\frac{D\rho}{Dx}+\frac{\rho R}{M}\frac{DT}{Dx}-\frac{\rho R T}{M^2}\frac{DM}{Dx}
     * \f]
     * Also,
     * \f[
     *    \frac{DT}{Dx} = \frac{1}{cp}\left(\frac{Dh}{Dx}-\sum_{i=1}^{n_s-1}(h_i-h_{n_s})\frac{DY_i}{Dx}\right)
     * \f]
     * \f[
     *    \frac{DM}{Dx} = -M^2 \sum_{i=1}^{n_s-1}(\frac{1}{M_i}-\frac{1}{M_{n_s}})\frac{DY_i}{Dx}
     * \f]
     * From continuity equation,
     * \f[
     *    \frac{D\rho}{Dx}=\frac{1}{u^2}\frac{Dp}{Dx}+\frac{2S_m}{u}
     * \f]
     * From enthalpy equation,
     * \f[
     *    \frac{Dh}{Dx}=\frac{Q+S_h-hS_m}{\rho u}+\frac{\frac{Dp}{Dx}}{\rho}
     * \f]
     * Substitute \f$\frac{DT}{Dx}\f$, \f$\frac{DM}{Dx}\f$, \f$\frac{D\rho}{Dx}\f$ and \f$\frac{Dp}{Dx}\f$ into \f$\frac{Dp}{Dx}\f$,
     * \f[
     *    \frac{Dp}{Dx} = \frac{\frac{2S_mT}{u}+\frac{Q+S_h-hS_m}{ucp}-\sum_{i=1}^{n_s-1}\left(\frac{\rho}{cp}(h_i-h_{n_s})-\rho MT(\frac{1}{M_i}-\frac{1}{M_{n_s}})\right)\frac{DY_i}{Dx}}{\frac{M}{R}-\frac{T}{u^2}-\frac{1}{cp}}
     * \f]
     * Here, \f$n_s\f$ is the number of species.
     *       \f$\rho\f$, \f$u\f$, \f$T\f$ and \f$cp\f$ are the density, velocity, temperature and heat capacity of gas phase.
     *       \f$Q\f$ is the heat transfer term in enthalpy equation (J/m^3/s), including heat transfer between gas and surroundings and between particles and gas.
     *       \f$S_m\f$ and \f$S_h\f$ are the mass and energy source terms from particle to gas. They are not added here.
     *       They are added using `attach_dependency_to_expression` in each sub-models of particle reactions.
     */
    template< typename FieldT >
    class DpDx : public Expr::Expression<FieldT>
    {
        const double  gasConstant_;
        const int nspec_;
        DECLARE_FIELDS( FieldT, temp_, mmw_, rho_, cp_, u_, enthHeatRhs_ );
        DECLARE_VECTOR_OF_FIELDS(FieldT, yiRhs_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, specEnth_);
        DECLARE_VECTOR_OF_FIELDS(FieldT, specMw_);

        DpDx( const Expr::Tag& uTag,
              const Expr::Tag& tempTag,
              const Expr::Tag& rhoTag,
              const Expr::Tag& mmwTag,
              const Expr::Tag& cpTag,
              const Expr::Tag& enthHeatRhsTag,
              const Expr::TagList& yiRhsTags,
              const Expr::TagList& specEnthTags,
              const Expr::TagList& specMwTags)
              : Expr::Expression<FieldT>(),
                gasConstant_( CanteraObjects::gas_constant() ),
                nspec_(yiRhsTags.size())
        {
          this->set_gpu_runnable(true);
          u_           = this->template create_field_request<FieldT>( uTag            );
          temp_        = this->template create_field_request<FieldT>( tempTag         );
          rho_         = this->template create_field_request<FieldT>( rhoTag          );
          mmw_         = this->template create_field_request<FieldT>( mmwTag          );
          cp_          = this->template create_field_request<FieldT>( cpTag           );
          enthHeatRhs_ = this->template create_field_request<FieldT>( enthHeatRhsTag  );
          this->template create_field_vector_request<FieldT>( yiRhsTags,    yiRhs_    );
          this->template create_field_vector_request<FieldT>( specEnthTags, specEnth_ );
          this->template create_field_vector_request<FieldT>( specMwTags,   specMw_   );
        }

    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag tempTag_, mmwTag_, rhoTag_, cpTag_, uTag_, enthHeatRhsTag_ ;
            const Expr::TagList yiRhsTags_, specEnthTags_, specMwTags_;
        public:
            /**
             * @brief The mechanism for building a ConvectionHeatRateCV object
             * @tparam FieldT
             * @param dpdxTag Dp/Dx` term in continuity and momentum equation for plug flow reactor
             * @param uTag Gas phase velocity
             * @param tempTag Gas phase temperature
             * @param rhoTag Gas phase density
             * @param mmwTag Mixture molecular weight of gas phase
             * @param cpTag Specific heat capacity of gas phase
             * @param enthHeatRhsTag Heat RHS in enthalpy equation (has been divided by \f$\rho u\f$)
             * @param yiRhsTags Full RHS of species equations
             * @param specEnthTags species specific enthalpy
             * @param specMwTags molecular weight of species
             */
            Builder( const Expr::Tag& dpdxTag,
                     const Expr::Tag& uTag,
                     const Expr::Tag& tempTag,
                     const Expr::Tag& rhoTag,
                     const Expr::Tag& mmwTag,
                     const Expr::Tag& cpTag,
                     const Expr::Tag& enthHeatRhsTag,
                     const Expr::TagList& yiRhsTags,
                     const Expr::TagList& specEnthTags,
                     const Expr::TagList& specMwTags)
                  : Expr::ExpressionBuilder( dpdxTag ),
                    uTag_( uTag ),
                    tempTag_( tempTag ),
                    rhoTag_( rhoTag ),
                    mmwTag_( mmwTag ),
                    cpTag_( cpTag ),
                    enthHeatRhsTag_( enthHeatRhsTag ),
                    yiRhsTags_( yiRhsTags ),
                    specEnthTags_( specEnthTags ),
                    specMwTags_( specMwTags )
            {}
            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new DpDx<FieldT>( uTag_, tempTag_, rhoTag_, mmwTag_, cpTag_, enthHeatRhsTag_, yiRhsTags_, specEnthTags_, specMwTags_ );
            }
        };


        void evaluate()
        {
          using namespace SpatialOps;
          FieldT& result = this->value();
          const FieldT& u        = u_                 ->field_ref();
          const FieldT& temp     = temp_              ->field_ref();
          const FieldT& rho      = rho_               ->field_ref();
          const FieldT& mmw      = mmw_               ->field_ref();
          const FieldT& cp       = cp_                ->field_ref();
          const FieldT& enthHeat = enthHeatRhs_       ->field_ref();
          const FieldT& hn       = specEnth_[nspec_-1]->field_ref();
          const FieldT& mwn      = specMw_[nspec_-1]  ->field_ref();
          result <<= enthHeat*rho/cp;
          for( size_t i=0; i<nspec_-1; i++){
            const FieldT& yiRhs = yiRhs_[i]   ->field_ref();
            const FieldT& hi    = specEnth_[i]->field_ref();
            const FieldT& mwi   = specMw_[i]  ->field_ref();
            result <<= result - (rho/cp*(hi-hn)-rho*mmw*temp*(1/mwi-1/mwn))*yiRhs;
          }
          result <<= result / (mmw/gasConstant_-temp/square(u)-1/cp);
        }

        void sensitivity( const Expr::Tag& sensVarTag )
        {
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          FieldT& result = this->value();
          const FieldT& u        = u_                 ->field_ref();  const FieldT& dudv        = u_                 ->sens_field_ref(sensVarTag);
          const FieldT& temp     = temp_              ->field_ref();  const FieldT& dtempdv     = temp_              ->sens_field_ref(sensVarTag);
          const FieldT& rho      = rho_               ->field_ref();  const FieldT& drhodv      = rho_               ->sens_field_ref(sensVarTag);
          const FieldT& mmw      = mmw_               ->field_ref();  const FieldT& dmmwdv      = mmw_               ->sens_field_ref(sensVarTag);
          const FieldT& cp       = cp_                ->field_ref();  const FieldT& dcpdv       = cp_                ->sens_field_ref(sensVarTag);
          const FieldT& enthHeat = enthHeatRhs_       ->field_ref();  const FieldT& denthHeatdv = enthHeatRhs_       ->sens_field_ref(sensVarTag);
          const FieldT& hn       = specEnth_[nspec_-1]->field_ref();  const FieldT& dhndv       = specEnth_[nspec_-1]->sens_field_ref(sensVarTag);
          const FieldT& mwn      = specMw_[nspec_-1]  ->field_ref();  const FieldT& dmwndv      = specMw_[nspec_-1]  ->sens_field_ref(sensVarTag);
          dfdv <<= denthHeatdv*rho/cp + enthHeat*drhodv/cp - enthHeat*rho/square(cp)*dcpdv;
          for( size_t i=0; i<nspec_-1; i++){
            const FieldT& yiRhs = yiRhs_[i]   ->field_ref(); const FieldT& dyiRhsdv = yiRhs_[i]   ->sens_field_ref(sensVarTag);
            const FieldT& hi    = specEnth_[i]->field_ref(); const FieldT& dhidv    = specEnth_[i]->sens_field_ref(sensVarTag);
            const FieldT& mwi   = specMw_[i]  ->field_ref(); const FieldT& dmwidv   = specMw_[i]  ->sens_field_ref(sensVarTag);
            dfdv <<= dfdv - (drhodv/cp*(hi-hn)-rho/square(cp)*dcpdv*(hi-hn)+rho/cp*(dhidv-dhndv) - (drhodv*mmw*temp+rho*dmmwdv*temp+rho*mmw*dtempdv)*(1/mwi-1/mwn)) *yiRhs
                     - (rho/cp*(hi-hn)-rho*mmw*temp*(1/mwi-1/mwn))*dyiRhsdv;
          }
          dfdv <<= dfdv / (mmw/gasConstant_-temp/square(u)-1/cp)
                   - result/(mmw/gasConstant_-temp/square(u)-1/cp)*(dmmwdv/gasConstant_-dtempdv/square(u)+2*temp/cube(u)*dudv+dcpdv/square(cp));
        }
    };

} // namespace PFR

#endif /* DpDx_H_ */
