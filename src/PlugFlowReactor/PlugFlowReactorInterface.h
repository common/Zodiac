/**
 *  \file   PlugFlowReactorInterface.h
 *  \date   July, 2020
 *  \author Hang
 */

#ifndef PLUGFLOWREACTORINTERFACE_H_
#define PLUGFLOWREACTORINTERFACE_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <yaml-cpp/yaml.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/MatVecFields.h>

#include <expression/ExprLib.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <FieldTypes.h>

namespace PlugFlowReactorInterface{

  struct PlugFlowReactorTags
  {
    const Expr::Tag uTag,        ///< velocity
                    dpdxTag,     ///< Dp/Dx in continuity and momentum equation
                    uFullRhsTag; ///< full RHS for velocity equation
      PlugFlowReactorTags( const Expr::Context& state );
  };
   /**
     * @brief Set the initial conditions for plug flow reactor
     */
  void setup_pfr( std::set<Expr::ExpressionID>& initRoots,
                  Expr::ExpressionFactory& initFactory,
                  const YAML::Node& parser);
  /**
   * @brief Register all expressions required for plug flow reactor and add equation into the time integrator
   */
  void build_equation_system_pfr( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                                  const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                  const YAML::Node& parser,
                                  int& equIndex,
                                  const double tempTolerance = 1e-3,
                                  const double maximumTemp   = 5000,
                                  const double maxTempIter   = 1000 );

  /**
   * @brief Initialize the reactor system for plug flow reactor
   */
  void initialize_pfr( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                       Expr::FieldManagerList& fml,
                       const Expr::ExprPatch& patch,
                       Expr::ExpressionTree& initTree,
                       const Expr::Tag dsTag,
                       const double gesatMax,
                       const double timeStep,
                       const bool isRestart);

} // namespace PlugFlowReactorInterface


#endif /* PLUGFLOWREACTORINTERFACE_H_ */
