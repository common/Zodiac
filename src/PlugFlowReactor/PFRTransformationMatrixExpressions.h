/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file    PFRTransformationMatrixExpressions.h
 *  \par     Getting the expressions needed to build transformation matrix for plug flow reactor
 *  \authors Hang
 */

#ifndef PFRTRANSFORMATIONMATRIXEXPRESSIONS_H_
#define PFRTRANSFORMATIONMATRIXEXPRESSIONS_H_

#include <expression/Expression.h>

namespace PFR{

  /**
   * @class SourceOverRhoU
   * @ingroup PFR
   * @brief Calculate the ratio between volumetric source term and (density * velocity).
   *        It is needed for the source term for plug flow reactor.
   * @author Hang
   *
   * The result is given as
   * \f[
   *    K = c \frac{\omega_i}{\rho u}
   * \f]
   *
   * The sensitivity of this term \f$ \frac{\partial K}{\partial V} \f$ is
   *  \f[
   *    \frac{\partial K}{\partial \phi} = c \left(\frac{1}{\rho u} \frac{\partial \omega_i}{\partial \phi}
   *                                          - \frac{\omega_i}{\rho^2 u }\frac{\partial \rho}{\partial \phi}
   *                                          - \frac{\omega_i}{\rho u^2 }\frac{\partial u}{\partial \phi} \right)
   *  \f]
   */

  template<typename FieldT>
  class SourceOverRhoU : public Expr::Expression<FieldT> {
      DECLARE_FIELDS( FieldT, rho_, source_, u_);
      const double coeff_;

      SourceOverRhoU( const Expr::Tag sourceTag,
                      const Expr::Tag rhoTag,
                      const Expr::Tag uTag,
                      const double coefficient)
            : Expr::Expression<FieldT>(),
              coeff_(coefficient){
        this->set_gpu_runnable( true );
        rho_ = this->template create_field_request<FieldT>( rhoTag );
        source_ = this->template create_field_request<FieldT>( sourceTag );
        u_ = this->template create_field_request<FieldT>( uTag );
      }

  public:
      class Builder : public Expr::ExpressionBuilder {
          const Expr::Tag sourceTag_, rhoTag_, uTag_;
          const double coeff_;
      public:
        /**
         * @brief The mechanism for building a RatePFR object
         * @tparam FieldT
         * @param resultTag Result of RatePFR
         * @param sourceTag volumetric source term
         * @param rhoTag Density of gas phase
         * @param uTag Velocity of gas phase
         * @param coefficient coefficient in the expression. It is 1 or -1 for present case
         */
          Builder( const Expr::Tag resultTag,
                   const Expr::Tag sourceTag,
                   const Expr::Tag rhoTag,
                   const Expr::Tag uTag,
                   const double coefficient):
                Expr::ExpressionBuilder( resultTag ),
                sourceTag_( sourceTag ),
                rhoTag_( rhoTag ),
                uTag_( uTag ),
                coeff_( coefficient ){}

          ~Builder(){}

          Expr::ExpressionBase* build() const{
            return new SourceOverRhoU<FieldT>( sourceTag_, rhoTag_, uTag_, coeff_ );
          }
      };

      void evaluate(){
        using namespace SpatialOps;
        this->value() <<= coeff_ * source_->field_ref() / rho_->field_ref() / u_->field_ref();
      }

      void sensitivity( const Expr::Tag& sensVarTag ){
        FieldT & dfdv = this->sensitivity_result( sensVarTag );
        const FieldT& source = source_->field_ref();
        const FieldT& rho = rho_->field_ref();
        const FieldT& u = u_->field_ref();
        const FieldT& dsourcedv = source_->sens_field_ref( sensVarTag );
        const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
        const FieldT& dudv = u_->sens_field_ref( sensVarTag );
        dfdv <<= coeff_ * (dsourcedv/rho/u - source/square(rho)/u*drhodv - source/rho/square(u)*dudv);
      }
  };
}


#endif /* PFRTRANSFORMATIONMATRIXEXPRESSIONS_H_ */
