/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>
#include "ReactorEnsembleUtil.h"

#include <fstream>

#include <expression/Functions.h>

// PoKiTT
#include <pokitt/CanteraObjects.h>
#include <pokitt/MixtureMolWeight.h>
#include <pokitt/thermo/Temperature.h>
#include <pokitt/thermo/Enthalpy.h>
#include <pokitt/kinetics/ReactionRates.h>
#include <pokitt/SpeciesN.h>
#include <pokitt/thermo/HeatCapacity_Cp.h>
#include <pokitt/thermo/Pressure.h>
#include <pokitt/thermo/Density.h>
#include "TransformationMatrixExpressions.h"
#include "PFRTransformationMatrixExpressions.h"
#include "PlugFlowReactorInterface.h"
#include "DpDx.h"

namespace PlugFlowReactorInterface{

  typedef Expr::PlaceHolder   <FieldT>::Builder PlaceHolderT;
  typedef Expr::LinearFunction<FieldT>::Builder LinearFunctionT;
  typedef Expr::ConstantExpr  <FieldT>::Builder ConstantT;

  typedef pokitt::SpeciesEnthalpy      <FieldT>::Builder SpecEnthT;
  typedef pokitt::HeatCapacity_Cp      <FieldT>::Builder HeatCpT;
  typedef pokitt::MixtureMolWeight     <FieldT>::Builder MixtureMWT;
  typedef pokitt::Temperature          <FieldT>::Builder TemperatureT;
  typedef pokitt::ReactionRates        <FieldT>::Builder RatesT;
  typedef pokitt::Enthalpy             <FieldT>::Builder EnthalpyT;
  typedef pokitt::SpeciesN             <FieldT>::Builder SpeciesNT;
  typedef pokitt::Pressure             <FieldT>::Builder PressureT;
  typedef pokitt::Density              <FieldT>::Builder DensityT;

  typedef ReactorEnsembleUtil::DivisionExpression  <FieldT>::Builder DivideT;
  typedef ReactorEnsembleUtil::YNRHS               <FieldT>::Builder YNRHST;
  typedef ReactorEnsembleUtil::InverseExpression   <FieldT>::Builder InverseT;
  typedef ReactorEnsembleUtil::SpeciesEnthOffsets  <FieldT>::Builder SpcEnthOffsetsT;

  typedef PFR::SourceOverRhoU <FieldT>::Builder SourceOverRhoUT;
  typedef PFR::DpDx           <FieldT>::Builder DpDxT;

  //---------------------------------------------------------------------------

  PlugFlowReactorTags::
  PlugFlowReactorTags( const Expr::Context& state )
  : uTag        ( Expr::Tag( "u"          , state ) ),
    dpdxTag     ( Expr::Tag( "dpdx"       , state ) ),
    uFullRhsTag ( Expr::Tag( "u_full_rhs" , state ) ){}

  //---------------------------------------------------------------------------

  void setup_pfr( std::set<Expr::ExpressionID>& initRoots,
                  Expr::ExpressionFactory& initFactory,
                  const YAML::Node& parser ){

    const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_N );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsPFR( Expr::STATE_N );

    // initial state
    initRoots.insert( initFactory.register_expression( new EnthalpyT( tags.enthTag, tags.tempTag, tags.massTags )));
    initRoots.insert( initFactory.register_expression( new DensityT( tags.rhoTag, tags.tempTag, tags.presTag, tags.mmwTag )));

    const int ns = CanteraObjects::number_species();
    for( size_t i = 0;i < ns;++i ){
      initRoots.insert( initFactory.register_expression( new SpecEnthT( tags.specEnthTags[i], tags.tempTag, i )));
    }
  }

  //---------------------------------------------------------------------------

  void build_equation_system_pfr( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                                  const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                  const YAML::Node& parser,
                                  int& equIndex,
                                  const double tempTolerance,
                                  const double maximumTemp,
                                  const double maxTempIter ){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

    const int ns = CanteraObjects::number_species();

    Expr::ExpressionFactory& execFactory = integrator->factory();

    integrator->register_root_expression( new PlaceHolderT( tagsE.timeTag ));
    execFactory.register_expression( new PlaceHolderT( tagsE.convecCoeffTag ) );
    execFactory.register_expression( new PlaceHolderT( tagsE.convecTempTag ) );
    integrator->register_root_expression( new PressureT( tagsE.presTag, tagsE.tempTag, tagsE.rhoTag, tagsE.mmwTag));

    execFactory.register_expression( new SpeciesNT( tagsE.massTags[ns - 1], tagsE.massTags, pokitt::CLIPSPECN ));

    execFactory.register_expression( new TemperatureT( tagsE.tempTag, tagsE.massTags, tagsE.enthTag,
                                                                            Expr::Tag(), tempTolerance, maximumTemp, maxTempIter ));
    execFactory.register_expression( new MixtureMWT( tagsE.mmwTag, tagsE.massTags, pokitt::FractionType::MASS ));

    const std::vector<double>& SpecMW = CanteraObjects::molecular_weights();
    for( size_t i = 0;i < ns;++i ){
      execFactory.register_expression( new SpecEnthT( tagsE.specEnthTags[i], tagsE.tempTag, i ));
      execFactory.register_expression( new ConstantT( tagsE.molarWeightTags[i], SpecMW[i] ));
    }

    execFactory.register_expression( new HeatCpT( tagsE.cpTag, tagsE.tempTag, tagsE.massTags ));

    // the rhs
    if ((!parser["ReactionInGas"]) or (parser["ReactionInGas"].as<std::string>() == "True") or (parser["ReactionInGas"].as<std::string>() == "true") ){
      execFactory.register_expression( new RatesT( tagsE.rateTags, tagsE.tempTag, tagsE.rhoTag, tagsE.massTags, tagsE.mmwTag, csj ));
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new SourceOverRhoUT( tagsE.yFullRhsTags[i], tagsE.rateTags[i], tagsE.rhoTag, tagsEpfr.uTag, 1.0 )); }
    }
    else{
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new ConstantT( tagsE.yFullRhsTags[i], 0 )); }
    }
    execFactory.register_expression( new DpDxT( tagsEpfr.dpdxTag, tagsEpfr.uTag, tagsE.tempTag, tagsE.rhoTag, tagsE.mmwTag, tagsE.cpTag,
                                                tagsE.enthHeatRhsTag, tagsE.yFullRhsTags, tagsE.specEnthTags, tagsE.molarWeightTags));
    // the full rhs
    execFactory.register_expression( new SourceOverRhoUT(tagsEpfr.uFullRhsTag, tagsEpfr.dpdxTag, tagsE.rhoTag, tagsEpfr.uTag, -1.0));
    execFactory.register_expression( new SourceOverRhoUT(tagsE.rhoFullRhsTag, tagsEpfr.dpdxTag, tagsEpfr.uTag, tagsEpfr.uTag, 1.0));

    execFactory.register_expression( new DivideT(tagsE.enthFullRhsTag, tagsEpfr.dpdxTag, tagsE.rhoTag));
    execFactory.attach_dependency_to_expression(tagsE.enthHeatRhsTag, tagsE.enthFullRhsTag, Expr::ADD_SOURCE_EXPRESSION);

    execFactory.register_expression( new YNRHST( tagsE.yFullRhsTags[ns-1], tagsE.yFullRhsTags));
    // matrix transformation fields
    execFactory.register_expression( new SpcEnthOffsetsT( tagsE.enthOffsetTags, tagsE.cpTag, tagsE.specEnthTags ));
    execFactory.register_expression( new InverseT( tagsE.invCpTag, tagsE.cpTag ));

    // add variables to the integrator
    integrator->add_variable<FieldT>( tagsEpfr.uTag.name(), tagsEpfr.uFullRhsTag, equIndex, equIndex);
    equIndex += 1;
    integrator->add_variable<FieldT>( tagsE.rhoTag.name(), tagsE.rhoFullRhsTag, equIndex, equIndex );
    equIndex += 1;
    integrator->add_variable<FieldT>( tagsE.enthTag.name(), tagsE.enthFullRhsTag, equIndex, equIndex );
    equIndex += 1;
    for( size_t i = 0;i < ns - 1;++i ){
      integrator->add_variable<FieldT>( tagsE.massTags[i].name(), tagsE.yFullRhsTags[i], i + 3, i + 3 );
    }
    equIndex += ns-2;
  }

  //---------------------------------------------------------------------------

  void initialize_pfr( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                       Expr::FieldManagerList& fml,
                       const Expr::ExprPatch& patch,
                       Expr::ExpressionTree& initTree,
                       const Expr::Tag dsTag,
                       const double gesatMax,
                       const double timeStep,
                       const bool isRestart){
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsN( Expr::STATE_N );
    const PlugFlowReactorInterface::PlugFlowReactorTags tagsEpfr( Expr::STATE_NONE );

    const int ns = CanteraObjects::number_species();

    if(!isRestart){
      initTree.register_fields( fml );
      initTree.bind_fields( fml );
      initTree.lock_fields( fml );
      std::ofstream initOut( "init-tree-reactor-ensemble.dot" );
      initTree.write_tree( initOut );
    }

    SpatialOps::OperatorDatabase sodb;
    integrator->prepare_for_integration( fml, sodb, patch.field_info());

    {
      std::ofstream execOut( "exec-tree-reactor-ensemble.dot" );
      integrator->get_tree().write_tree( execOut );
    }

    fml.allocate_fields( patch.field_info());
    if(!isRestart){
      initTree.execute_tree();

      // copy the residence time from STATE_N as initialized to STATE_NONE as used in mixing time expressions
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecCoeffTag.name());
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecTempTag.name());

      // copy the temperature because we need it for the initial guess in the T solve
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tempTag.name());

    }

    // initialize the simulation time and time step
    // The time step must be initialized here because we need to update t -> t + dt
    // before the dual time execution tree runs.
    fml.field_ref<FieldT>( tagsE.timeTag ) <<= 0.;
    fml.field_ref<SingleValueField>( tagsE.dtTag ) <<= timeStep;

    // initialize the dual time step
    fml.field_ref<FieldT>( dsTag ) <<= gesatMax;

    // jcs why are we locking these fields?  Can this be done automatically?
    integrator->lock_field<FieldT>( tagsE.dsTag );
    integrator->lock_field<FieldT>( tagsE.convecCoeffTag );
    integrator->lock_field<FieldT>( tagsE.convecTempTag );
    integrator->lock_field<FieldT>( tagsE.timeTag );
    integrator->lock_field<SingleValueField>( tagsE.dtTag );
    integrator->lock_field<FieldT>( tagsE.tempTag );
    integrator->lock_field<FieldT>( tagsE.gesatEigTag );
  }
  //---------------------------------------------------------------------------

} // namespace PlugFlowReactorInterface
