/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   ConvectiveHeatTransferRate.h
 *  @par    Getting the convective heat transfer term between gas phase and surroundings in gas phase energy/enthalpy equation
 *  @authors Mike, Hang
 */

#ifndef CONVECTIVEHEATTRANSFERRATE_H_
#define CONVECTIVEHEATTRANSFERRATE_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil{

    /**
     * @class ConvectionHeatRateCV
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the convective heat transfer term between gas phase and surroundings
     *        in gas phase energy equation under constant volume condition
     * @author Mike
     *
     * The convective heat transfer term under constant volume condition is give as
     * \f[
     *    Q_{cov} = k \frac{A}{V} (T_{\inf} - T)
     * \f]
     * Here, \f$ k \f$ is the convective coefficient between gas phase and surroundings,
     *       \f$ T_{\inf} \f$ is the surroundings temperature.
     *       \f$ \frac{A}{V}\f$ is the area/volume ratio of the reactor.
     *  For spherical reactor, \f$ \frac{A}{V}=\frac{3}{r}\f$, with \f$r\f$ represents the reacotr radius.
     *  For cylinder reactor, \f$ \frac{A}{V}=\frac{2}{r}\f$, (only surrounding surface is considered)
     *  with \f$r\f$ and \f$h\f$ represent radius of the circular end of the cylinder and height of the cylinder.
     *
     * The sensitivity is given as
     * \f[
     *    \frac{\partial Q_{cov}}{\partial \phi} = - k \frac{A}{V} \frac{\partial T}{\partial \phi}
     * \f]
     */
    template< typename FieldT >
    class ConvectionHeatRateCV : public Expr::Expression<FieldT>
    {
        const double  surfaceAreaPerVolume_;
        DECLARE_FIELDS( FieldT, temp_, convCoeff_, convTemp_ );

        ConvectionHeatRateCV( const Expr::Tag tempTag,
                              const Expr::Tag convectionCoeffTag,
                              const Expr::Tag convectionTempTag,
                              const double surfaceAreaPerVolume)
              : Expr::Expression<FieldT>(),
                surfaceAreaPerVolume_( surfaceAreaPerVolume )
        {
          this->set_gpu_runnable(true);
          temp_ = this->template create_field_request<FieldT>( tempTag );
          convCoeff_ = this->template create_field_request<FieldT>( convectionCoeffTag );
          convTemp_ = this->template create_field_request<FieldT>( convectionTempTag );
        }

    public:
        class Builder : public Expr::ExpressionBuilder
        {
            const Expr::Tag tempTag_, convCoeffTag_, convTempTag_;
            const double surfaceAreaPerVolume_;
        public:
          /**
           * @brief The mechanism for building a ConvectionHeatRateCV object
           * @tparam FieldT
           * @param heatRateTag Convective heat transfer term in gas phase energy equation under constant volume condition
           * @param tempTag Gas phase temperature
           * @param convectionCoeffTag Convective heat transfer coefficient between gas phase and surroundings
           * @param convectionTempTag Surroundings temperature
           * @param surfaceAreaPerVolume Ratio between surface area and volume of reactor
           */
            Builder( const Expr::Tag heatRateTag,
                     const Expr::Tag tempTag,
                     const Expr::Tag convectionCoeffTag,
                     const Expr::Tag convectionTempTag,
                     const double surfaceAreaPerVolume)
                  : Expr::ExpressionBuilder( heatRateTag ),
                    tempTag_( tempTag ),
                    convCoeffTag_( convectionCoeffTag ),
                    convTempTag_( convectionTempTag ),
                    surfaceAreaPerVolume_( surfaceAreaPerVolume )
            {}
            ~Builder(){}
            Expr::ExpressionBase* build() const{
              return new ConvectionHeatRateCV<FieldT>( tempTag_, convCoeffTag_, convTempTag_, surfaceAreaPerVolume_ );
            }
        };


        void evaluate()
        {
          using namespace SpatialOps;
          this->value() <<= convCoeff_->field_ref() * surfaceAreaPerVolume_ * ( convTemp_->field_ref() - temp_->field_ref() );
        }

        void sensitivity( const Expr::Tag& sensVarTag )
        {
          FieldT& dfdv = this->sensitivity_result( sensVarTag );
          dfdv <<= convCoeff_->sens_field_ref(sensVarTag) * surfaceAreaPerVolume_ * ( convTemp_->field_ref() - temp_->field_ref() )
                   + convCoeff_->field_ref() * surfaceAreaPerVolume_ * (convTemp_->sens_field_ref( sensVarTag) - temp_->sens_field_ref( sensVarTag ));
        }
    };

    /**
     * @class ConvectionHeatRateCP
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the convective heat transfer term between gas phase and surroundings
     *        in gas phase enthalpy equation under constant pressure condition
     * @author Hang
     *
     * The convective heat transfer term under constant pressure condition is give as
     * \f[
     *    Q_{cov} = k \frac{A}{\rho V} (T_{\inf} - T)
     * \f]
     * Here, \f$ k \f$ is the convective coefficient between gas phase and surroundings,
     *       \f$ T_{\inf} \f$ is the surroundings temperature.
     *       \f$ \frac{A}{V}\f$ is the area/volume ratio of the reactor.
     *  For spherical reactor, \f$ \frac{A}{V}=\frac{3}{r}\f$, with \f$r\f$ represents the reacotr radius.
     *  For cylinder reactor, \f$ \frac{A}{V}=\frac{2}{r}\f$, (only surrounding surface is considered)
     *  with \f$r\f$ and \f$h\f$ represent radius of the circular end of the cylinder and height of the cylinder.
     *
     * The sensitivity is given as
     * \f[
     *    \frac{\partial Q_{cov}}{\partial \phi} = \frac{A}{V} (\frac{\partial k}{\partial \phi}\frac{T_{\inf}-T}{\rho}
     *                                                         + \frac{k}{\rho}(\frac{\partial T_{\inf}}{\partial \phi}-\frac{\partial T}{\partial \phi})
     *                                                         - \frac{k}{\rho^2}\frac{\partial \rho}{\partial \phi}(T_{\inf}-T))
     * \f]
     */
    template<typename FieldT>
    class ConvectionHeatRateCP : public Expr::Expression<FieldT> {
        const double surfaceAreaPerVolume_;
        DECLARE_FIELDS( FieldT, temp_, rho_, convCoeff_, convTemp_);

        ConvectionHeatRateCP( const Expr::Tag tempTag,
                              const Expr::Tag rhoTag,
                              const Expr::Tag convectionCoeffTag,
                              const Expr::Tag convectionTempTag,
                              const double surfaceAreaPerVolume)
              : Expr::Expression<FieldT>(),
                surfaceAreaPerVolume_( surfaceAreaPerVolume){
          this->set_gpu_runnable( true );
          temp_ = this->template create_field_request<FieldT>( tempTag );
          rho_ = this->template create_field_request<FieldT>( rhoTag );
          convCoeff_ = this->template create_field_request<FieldT>( convectionCoeffTag );
          convTemp_ = this->template create_field_request<FieldT>( convectionTempTag );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag tempTag_, rhoTag_, convectionCoeffTag_, convectionTempTag_;
            const double surfaceAreaPerVolume_;
        public:
          /**
           * @brief The mechanism for building a ConvectionHeatRateCP object
           * @tparam FieldT
           * @param heatRateTag Convective heat transfer term in gas phase enthalpy equation under constant pressure condition
           * @param tempTag Gas phase temperature
           * @param rhoTag Gas phase density
           * @param convectionCoeffTag Convective heat transfer coefficient between gas phase and surroundings
           * @param convectionTempTag Surroundings temperature
           * @param surfaceAreaPerVolume Ratio between surface area and volume of reactor
           */
            Builder( const Expr::Tag heatRateTag,
                     const Expr::Tag tempTag,
                     const Expr::Tag rhoTag,
                     const Expr::Tag convectionCoeffTag,
                     const Expr::Tag convectionTempTag,
                     const double surfaceAreaPerVolume)
                  : Expr::ExpressionBuilder( heatRateTag ),
                    tempTag_( tempTag ),
                    rhoTag_( rhoTag ),
                    convectionCoeffTag_( convectionCoeffTag ),
                    convectionTempTag_( convectionTempTag ),
                    surfaceAreaPerVolume_( surfaceAreaPerVolume ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new ConvectionHeatRateCP<FieldT>( tempTag_, rhoTag_, convectionCoeffTag_, convectionTempTag_, surfaceAreaPerVolume_ );
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          const FieldT& temp = temp_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& convCoeff = convCoeff_->field_ref();
          const FieldT& convTemp = convTemp_->field_ref();
          this->value() <<= convCoeff * surfaceAreaPerVolume_ /rho * (convTemp - temp);
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& temp = temp_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& convCoeff = convCoeff_->field_ref();
          const FieldT& convTemp = convTemp_->field_ref();
          const FieldT& dTdv = temp_->sens_field_ref( sensVarTag );
          const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
          const FieldT& dconvCoeffdv = convCoeff_->sens_field_ref( sensVarTag );
          const FieldT& dconvTempdv = convTemp_->sens_field_ref( sensVarTag );
          dfdv <<= surfaceAreaPerVolume_ * ((dconvCoeffdv/rho-convCoeff/square(rho)*drhodv) * (convTemp - temp)
                                            + convCoeff/rho * (dconvTempdv-dTdv));
        }
    };


    /**
    * @class ConvectionHeatRatePFR
    * @ingroup ReactorEnsembleUtil
    * @brief Getting the convective heat transfer term between gas phase and surroundings
    *        in gas phase enthalpy equation for plug flow reactor
    * @author Hang
    *
    * The convective heat transfer term in enthalpy equation for plug flow reactor is give as
    * \f[
    *    Q_{cov} = k \frac{A}{\rho V u} (T_{\inf} - T)
    * \f]
    * Here, \f$ k \f$ is the convective coefficient between gas phase and surroundings,
    *       \f$ T_{\inf} \f$ is the surroundings temperature.
    *       \f$ \frac{A}{V}\f$ is the area/volume ratio of the reactor.
    *       \f$ u\f$ is the gas phase velocity in the reactor.
    *  For spherical reactor, \f$ \frac{A}{V}=\frac{3}{r}\f$, with \f$r\f$ represents the reacotr radius.
    *  For cylinder reactor, \f$ \frac{A}{V}=\frac{2}{r}\f$, (only surrounding surface is considered)
    *  with \f$r\f$ represents radius of the circular end of the cylinder.
    *
    * The sensitivity is given as
    * \f[
    *    \frac{\partial Q_{cov}}{\partial \phi} = \frac{A}{V} (\frac{\partial k}{\partial \phi}\frac{T_{\inf}-T}{\rho u}
    *                                                         + \frac{k}{\rho u}(\frac{\partial T_{\inf}}{\partial \phi}-\frac{\partial T}{\partial \phi})
    *                                                         - \frac{k}{\rho^2 u}\frac{\partial \rho}{\partial \phi}(T_{\inf}-T)
     *                                                        - \frac{k}{\rho u^2}\frac{\partial u}{\partial \phi}(T_{\inf}-T))
    * \f]
    */
    template<typename FieldT>
    class ConvectionHeatRatePFR : public Expr::Expression<FieldT> {
        const double surfaceAreaPerVolume_;
        DECLARE_FIELDS( FieldT, temp_, rho_, convCoeff_, convTemp_, u_);

        ConvectionHeatRatePFR( const Expr::Tag tempTag,
                               const Expr::Tag rhoTag,
                               const Expr::Tag convectionCoeffTag,
                               const Expr::Tag convectionTempTag,
                               const Expr::Tag uTag,
                               const double surfaceAreaPerVolume)
              : Expr::Expression<FieldT>(),
                surfaceAreaPerVolume_( surfaceAreaPerVolume){
          this->set_gpu_runnable( true );
          temp_      = this->template create_field_request<FieldT>( tempTag            );
          rho_       = this->template create_field_request<FieldT>( rhoTag             );
          convCoeff_ = this->template create_field_request<FieldT>( convectionCoeffTag );
          convTemp_  = this->template create_field_request<FieldT>( convectionTempTag  );
          u_         = this->template create_field_request<FieldT>( uTag               );
        }

    public:
        class Builder : public Expr::ExpressionBuilder {
            const Expr::Tag tempTag_, rhoTag_, convectionCoeffTag_, convectionTempTag_, uTag_;
            const double surfaceAreaPerVolume_;
        public:
            /**
             * @brief The mechanism for building a ConvectionHeatRateCP object
             * @tparam FieldT
             * @param heatRateTag Convective heat transfer term in gas phase enthalpy equation under constant pressure condition
             * @param tempTag Gas phase temperature
             * @param rhoTag Gas phase density
             * @param convectionCoeffTag Convective heat transfer coefficient between gas phase and surroundings
             * @param convectionTempTag Surroundings temperature
             * @param uTag Velocity of gas phase
             * @param surfaceAreaPerVolume Ratio between surface area and volume of reactor
             */
            Builder( const Expr::Tag heatRateTag,
                     const Expr::Tag tempTag,
                     const Expr::Tag rhoTag,
                     const Expr::Tag convectionCoeffTag,
                     const Expr::Tag convectionTempTag,
                     const Expr::Tag uTag,
                     const double surfaceAreaPerVolume)
                  : Expr::ExpressionBuilder( heatRateTag ),
                    tempTag_( tempTag ),
                    rhoTag_( rhoTag ),
                    convectionCoeffTag_( convectionCoeffTag ),
                    convectionTempTag_( convectionTempTag ),
                    uTag_( uTag ),
                    surfaceAreaPerVolume_( surfaceAreaPerVolume ){}

            ~Builder(){}

            Expr::ExpressionBase* build() const{
              return new ConvectionHeatRatePFR<FieldT>( tempTag_, rhoTag_, convectionCoeffTag_, convectionTempTag_, uTag_, surfaceAreaPerVolume_ );
            }
        };

        void evaluate(){
          using namespace SpatialOps;
          const FieldT& temp = temp_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& convCoeff = convCoeff_->field_ref();
          const FieldT& convTemp = convTemp_->field_ref();
          const FieldT& u = u_->field_ref();
          this->value() <<= convCoeff * surfaceAreaPerVolume_ /rho/u * (convTemp - temp);
        }

        void sensitivity( const Expr::Tag& sensVarTag ){
          FieldT & dfdv = this->sensitivity_result( sensVarTag );
          const FieldT& temp = temp_->field_ref();
          const FieldT& rho = rho_->field_ref();
          const FieldT& convCoeff = convCoeff_->field_ref();
          const FieldT& convTemp = convTemp_->field_ref();
          const FieldT& u = u_->field_ref();
          const FieldT& dTdv = temp_->sens_field_ref( sensVarTag );
          const FieldT& drhodv = rho_->sens_field_ref( sensVarTag );
          const FieldT& dconvCoeffdv = convCoeff_->sens_field_ref( sensVarTag );
          const FieldT& dconvTempdv = convTemp_->sens_field_ref( sensVarTag );
          const FieldT& dudv = u_->sens_field_ref( sensVarTag );
          dfdv <<= surfaceAreaPerVolume_ * ((dconvCoeffdv/rho/u-convCoeff/square(rho)/u*drhodv-convCoeff/square(u)/rho*dudv) * (convTemp - temp)
                                            + convCoeff/rho/u * (dconvTempdv-dTdv));
        }
    };

} // namespace ReactorEnsembleUtil

#endif /* CONVECTIVEHEATTRANSFERRATE_H_ */
