/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <FieldTypes.h>
#include "ReactorEnsembleUtil.h"

#include <fstream>

#include <expression/Functions.h>

// PoKiTT
#include <pokitt/CanteraObjects.h>
#include <pokitt/thermo/InternalEnergy.h>
#include <pokitt/MixtureMolWeight.h>
#include <pokitt/SpecificToVolumetric.h>
#include <pokitt/VolumetricToSpecific.h>
#include <pokitt/thermo/Temperature.h>
#include <pokitt/thermo/Enthalpy.h>
#include <pokitt/kinetics/ReactionRates.h>
#include <pokitt/SpeciesN.h>
#include <pokitt/thermo/HeatCapacity_Cv.h>
#include <pokitt/thermo/HeatCapacity_Cp.h>

#include "ZeroDimMixing.h"
#include "SumOp.h"
#include "TransformationMatrixExpressions.h"
#include "Density.h"

namespace ReactorEnsembleUtil{

  typedef Expr::PlaceHolder   <FieldT>::Builder PlaceHolderT;
  typedef Expr::LinearFunction<FieldT>::Builder LinearFunctionT;
  typedef Expr::ConstantExpr  <FieldT>::Builder ConstantT;

  typedef pokitt::InternalEnergy       <FieldT>::Builder EnergyT;
  typedef pokitt::SpeciesInternalEnergy<FieldT>::Builder SpecEnergyT;
  typedef pokitt::SpeciesEnthalpy      <FieldT>::Builder SpecEnthT;
  typedef pokitt::HeatCapacity_Cp      <FieldT>::Builder HeatCpT;
  typedef pokitt::HeatCapacity_Cv      <FieldT>::Builder HeatCvT;
  typedef pokitt::MixtureMolWeight     <FieldT>::Builder MixtureMWT;
  typedef pokitt::TemperatureFromE0    <FieldT>::Builder TemperatureFromE0T;
  typedef pokitt::Temperature          <FieldT>::Builder TemperatureT;
  typedef pokitt::ReactionRates        <FieldT>::Builder RatesT;
  typedef pokitt::Enthalpy             <FieldT>::Builder EnthalpyT;
  typedef pokitt::SpeciesN             <FieldT>::Builder SpeciesNT;
  typedef pokitt::SpecificToVolumetric <FieldT>::Builder RhoPhiT;
  typedef pokitt::VolumetricToSpecific <FieldT>::Builder PhiT;

  typedef ReactorEnsembleUtil::Density            <FieldT>::Builder DensityT;
  typedef ReactorEnsembleUtil::ZeroDMixing        <FieldT>::Builder ZeroDMixingT;
  typedef ReactorEnsembleUtil::ZeroDMixingCP      <FieldT>::Builder ZeroDMixingCPT;
  typedef ReactorEnsembleUtil::SumOp              <FieldT>::Builder SumOpT;

  typedef ReactorEnsembleUtil::SpeciesEnergyOffsets<FieldT>::Builder SpcEgyOffsetsT;
  typedef ReactorEnsembleUtil::SpeciesEnthOffsets  <FieldT>::Builder SpcEnthOffsetsT;
  typedef ReactorEnsembleUtil::MassFracsOverRho    <FieldT>::Builder YOverRhoT;
  typedef ReactorEnsembleUtil::RateOverRho         <FieldT>::Builder WOverRhoT;
  typedef ReactorEnsembleUtil::InverseExpression   <FieldT>::Builder InverseT;
  typedef ReactorEnsembleUtil::ProductExpression   <FieldT>::Builder ProductT;
  typedef ReactorEnsembleUtil::DivisionExpression  <FieldT>::Builder DivisionT;
  typedef ReactorEnsembleUtil::RhoPartialH         <FieldT>::Builder RhoPartialHT;
  typedef ReactorEnsembleUtil::RhoPartialY         <FieldT>::Builder RhoPartialYT;
  typedef ReactorEnsembleUtil::LogFunction         <FieldT>::Builder LogT;
  typedef ReactorEnsembleUtil::YmixPartialY        <FieldT>::Builder YmixPartialYT;
  typedef ReactorEnsembleUtil::HmixPartialT        <FieldT>::Builder HmixPartialTT;
  typedef ReactorEnsembleUtil::HmixPartialY        <FieldT>::Builder HmixPartialYT;
  typedef ReactorEnsembleUtil::YNRHS               <FieldT>::Builder YNRHST;

  //---------------------------------------------------------------------------

  ReactorEnsembleTags::
  ReactorEnsembleTags( const Expr::Context& state )
  : tempTag           ( Expr::Tag( "T"                            , state ) ),
    presTag           ( Expr::Tag( "p"                            , state ) ),
    egyTag            ( Expr::Tag( "egy"                          , state ) ),
    enthTag           ( Expr::Tag( "enth"                         , state ) ),
    rhoTag            ( Expr::Tag( "rho"                          , state ) ),
    mmwTag            ( Expr::Tag( "Mmix"                         , state ) ),
    rhoEgyTag         ( Expr::Tag( "rhoegy"                       , state ) ),
    keTag             ( Expr::Tag( "ke"                           , state ) ),
    cvTag             ( Expr::Tag( "cv"                           , state ) ),
    cpTag             ( Expr::Tag( "cp"                           , state ) ),
    expEigTag         ( Expr::Tag( "lambda+"                      , state ) ),
    reactEigTag       ( Expr::Tag( "lambda-"                      , state ) ),
    gesatEigTag       ( Expr::Tag( "lambda_gesat"                 , state ) ),
    tauMixTag         ( Expr::Tag( "tau_mix"                      , state ) ),
    tauMixLogTag      ( Expr::Tag( "tau_mix_log"                  , state ) ),
    mmwInflowTag      ( Expr::Tag( "Mmix_inflow"                  , state ) ),
    tempInflowTag     ( Expr::Tag( "T_inflow"                     , state ) ),
    presInflowTag     ( Expr::Tag( "p_inflow"                     , state ) ),
    rhoInflowTag      ( Expr::Tag( "rho_inflow"                   , state ) ),
    egyInflowTag      ( Expr::Tag( "egy_inflow"                   , state ) ),
    rhoEgyInflowTag   ( Expr::Tag( "rho_egy_inflow"               , state ) ),
    enthInflowTag     ( Expr::Tag( "enth_inflow"                  , state ) ),
    rhoKinRhsTag      ( Expr::Tag( "rho_kin_rhs"                  , state ) ),
    rhoEgyKinRhsTag   ( Expr::Tag( "rhoegy_kin_rhs"               , state ) ),
    presKinRhsTag     ( Expr::Tag( "pres_kin_rhs"                 , state ) ),
    enthKinRhsTag     ( Expr::Tag( "enth_kin_rhs"                 , state ) ),
    rhoMixRhsTag      ( Expr::Tag( "rho_mix_rhs"                  , state ) ),
    rhoEgyMixRhsTag   ( Expr::Tag( "rhoegy_mix_rhs"               , state ) ),
    presMixRhsTag     ( Expr::Tag( "pres_mix_rhs"                 , state ) ),
    enthMixRhsTag     ( Expr::Tag( "enth_mix_rhs"                 , state ) ),
    rhoEgyHeatRhsTag  ( Expr::Tag( "rhoegy_heat_rhs"              , state ) ),
    enthHeatRhsTag    ( Expr::Tag( "enth_heat_rhs"                , state ) ),
    rhoFullRhsTag     ( Expr::Tag( "rho_full_rhs"                 , state ) ),
    rhoEgyFullRhsTag  ( Expr::Tag( "rhoegy_full_rhs"              , state ) ),
    presFullRhsTag    ( Expr::Tag( "pres_full_rhs"                , state ) ),
    enthFullRhsTag    ( Expr::Tag( "enth_full_rhs"                , state ) ),
    rhocvTag          ( Expr::Tag( "rho_cv"                       , state ) ),
    invrhocvTag       ( Expr::Tag( "1/rhocv"                      , state ) ),
    invrhoTag         ( Expr::Tag( "1/rho"                        , state ) ),
    rhoOverPresTag    ( Expr::Tag( "rho/p"                        , state ) ),
    rhoPartialHTag    ( Expr::Tag( "(partial_rho)/(partial_h)"    , state ) ),
    invCpTag          ( Expr::Tag( "1/cp"                         , state ) ),
    invTauTag         ( Expr::Tag( "1/tau_mix"                    , state ) ),
    yMixPartialYTag   ( Expr::Tag( "(partial_Yi_mix)/(partial_Yi)", state ) ),
    hMixPartialTTag   ( Expr::Tag( "(partial_h_mix)/(partial_T)"  , state ) ),
    thermalConducTag  ( Expr::Tag( "thermal_conduc_gas"           , state ) ),
    dynviscosityTag   ( Expr::Tag( "dyn_viscosity"                , state ) ),
    scNumberTag       ( Expr::Tag( "sc_number"                    , state ) ),
    convecCoeffTag    ( Expr::Tag( "heat_transfer_coefficient"    , state ) ),
    convecTempTag     ( Expr::Tag( "surroundings_temperature"     , state ) ),
    dsTag             ( Expr::Tag( "ds"                           , state ) ),
    dtTag             ( Expr::Tag( "dt"                           , state ) ),
    mixtureFractionTag( Expr::Tag( "mixture_fraction"             , state ) ),
    timeTag           ( Expr::Tag( "simulation_time"              , state ) )
  {
    const std::vector<std::string>& spNames = CanteraObjects::species_names();
    moleTags        = Expr::tag_list( spNames, state, "X_"                          );
    massTags        = Expr::tag_list( spNames, state, "Y_"                          );
    massInflowTags  = Expr::tag_list( spNames, state, "Y_", "_inflow"               );
    moleInflowTags  = Expr::tag_list( spNames, state, "X_", "_inflow"               );
    rhoYTags        = Expr::tag_list( spNames, state, "rhoY_"                       );
    rateTags        = Expr::tag_list( spNames, state, "w_"                          );
    specEgyTags     = Expr::tag_list( spNames, state, "e_"                          );
    specEnthTags    = Expr::tag_list( spNames, state, "h_"                          );
    molarWeightTags = Expr::tag_list( spNames, state, "M_"                          );
    rhoYKinRhsTags  = Expr::tag_list( spNames, state, "rhoY_", "_kin_rhs"           );
    rhoYMixRhsTags  = Expr::tag_list( spNames, state, "rhoY_", "_mix_rhs"           );
    rhoYFullRhsTags = Expr::tag_list( spNames, state, "rhoY_", "_full_rhs"          );
    rhoYInflowTags  = Expr::tag_list( spNames, state, "rhoY_", "_inflow"            );
    egyOffsetTags   = Expr::tag_list( spNames, state, "egy-offset_"                 );
    mYoverRhoTags   = Expr::tag_list( spNames, state, "-Y/rho_"                     );
    yKinRhsTags     = Expr::tag_list( spNames, state, "Y_", "_kin_rhs"              );
    yMixRhsTags     = Expr::tag_list( spNames, state, "Y_", "_mix_rhs"              );
    yFullRhsTags    = Expr::tag_list( spNames, state, "Y_", "_full_rhs"             );
    enthOffsetTags  = Expr::tag_list( spNames, state, "enth_offset_"                );
    rhoPartialYTags = Expr::tag_list( spNames, state, "(partial_rho)/(partial_Y_"   );
    hMixPartialYTags= Expr::tag_list( spNames, state, "(partial_h_mix)/(partial_Y_" );
  }

  //---------------------------------------------------------------------------

  void setup_cv( std::set<Expr::ExpressionID>& initRoots,
                 Expr::ExpressionFactory& initFactory,
                 const YAML::Node& parser)
  {
    const int ns = CanteraObjects::number_species();

    const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_N );

    if(!parser["InitialConditions"]){
      initFactory.register_expression( new LinearFunctionT( tags.tempTag, tags.tempInflowTag, 1, 0 ) );
      initFactory.register_expression( new LinearFunctionT( tags.presTag, tags.presInflowTag, 1, 0 ) );
      const int ns = CanteraObjects::number_species();
      for( size_t i=0; i<ns; ++i ){
        initFactory.register_expression ( new LinearFunctionT( tags.massTags[i], tags.massInflowTags[i], 1, 0 ) );
        initRoots.insert(initFactory.register_expression ( new LinearFunctionT( tags.moleTags[i], tags.moleInflowTags[i], 1, 0 ) ) );
      }
      initFactory.register_expression( new MixtureMWT( tags.mmwTag, tags.massTags, pokitt::FractionType::MASS ));
    }

    for( size_t i=0; i<ns; ++i ){
      initRoots.insert( initFactory.register_expression( new RhoPhiT( tags.rhoYTags[i], tags.rhoTag, tags.massTags[i] ) ) );
      initRoots.insert( initFactory.register_expression( new RhoPhiT( tags.rhoYInflowTags[i], tags.rhoInflowTag, tags.massInflowTags[i]) ) );
    }
    // initial state
    initFactory.register_expression( new EnergyT( tags.egyTag, tags.tempTag, tags.massTags ) );
    initFactory.register_expression( new DensityT( tags.rhoTag, tags.tempTag, tags.presTag, tags.mmwTag ) );
    initRoots.insert( initFactory.register_expression( new RhoPhiT( tags.rhoEgyTag, tags.rhoTag, tags.egyTag ) ) );

    // inflow state ( not necessary to be the same as initial state)
    initRoots.insert( initFactory.register_expression( new LogT( tags.tauMixTag, tags.tauMixLogTag ) ) );
    initFactory.register_expression( new EnergyT( tags.egyInflowTag, tags.tempInflowTag, tags.massInflowTags ) );
    initFactory.register_expression( new DensityT( tags.rhoInflowTag, tags.tempInflowTag, tags.presInflowTag, tags.mmwInflowTag ) );
    initRoots.insert( initFactory.register_expression( new RhoPhiT( tags.rhoEgyInflowTag, tags.rhoInflowTag, tags.egyInflowTag ) ) );

  }

  //---------------------------------------------------------------------------

  void build_equation_system_cv( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                                 const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                 const YAML::Node& parser,
                                 int& equIndex,
                                 const double tempTolerance,
                                 const double maximumTemp,
                                 const double maxTempIter )
  {
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );

    const int ns = CanteraObjects::number_species();

    Expr::ExpressionFactory& execFactory = integrator->factory();

    integrator->register_root_expression( new PlaceHolderT( tagsE.timeTag ) );
    integrator->register_root_expression( new PlaceHolderT( tagsE.tempInflowTag ) );
    integrator->register_root_expression( new PlaceHolderT( tagsE.presInflowTag ) );

    execFactory.register_expression( new InverseT( tagsE.invTauTag, tagsE.tauMixTag ));
    for( size_t i=0; i<ns-1; ++i ){
      execFactory.register_expression( new ReactorEnsembleUtil::PhiT( tagsE.massTags[i], tagsE.rhoTag, tagsE.rhoYTags[i] ) );
    }
    execFactory.register_expression( new ReactorEnsembleUtil::SpeciesNT( tagsE.massTags[ns-1], tagsE.massTags, pokitt::CLIPSPECN ) );

    execFactory.register_expression( new ReactorEnsembleUtil::PhiT( tagsE.egyTag, tagsE.rhoTag, tagsE.rhoEgyTag ) );

    execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.keTag, 0.0 ) );
    execFactory.register_expression( new ReactorEnsembleUtil::TemperatureFromE0T( tagsE.tempTag, tagsE.massTags, tagsE.egyTag, tagsE.keTag,
                                                                            Expr::Tag(), tempTolerance, maximumTemp, maxTempIter ) );
    execFactory.register_expression( new ReactorEnsembleUtil::MixtureMWT( tagsE.mmwTag, tagsE.massTags, pokitt::FractionType::MASS ) );

    for( size_t i=0; i<ns; ++i ){
      execFactory.register_expression( new SpecEnergyT( tagsE.specEgyTags[i], tagsE.tempTag, i ) );
    }
    execFactory.register_expression( new HeatCvT( tagsE.cvTag, tagsE.tempTag, tagsE.massTags ) );

    // kinetics component of the rhs
    execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.rhoKinRhsTag, 0.0 ) );
    execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.rhoEgyKinRhsTag, 0.0 ) );
    if ((!parser["ReactionInGas"]) or (parser["ReactionInGas"].as<std::string>() == "True") or (parser["ReactionInGas"].as<std::string>() == "true") ){
      execFactory.register_expression( new ReactorEnsembleUtil::RatesT( tagsE.rateTags,
                                                                        tagsE.tempTag,
                                                                        tagsE.rhoTag,
                                                                        tagsE.massTags,
                                                                        tagsE.mmwTag,
                                                                        csj ) );
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new LinearFunctionT( tagsE.rhoYKinRhsTags[i], tagsE.rateTags[i], 1.0, 0.0 )); }
    }
    else{
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new ConstantT( tagsE.rhoYKinRhsTags[i], 0 ));}
    }


    // inflow component of the rhs
    execFactory.register_expression( new PlaceHolderT( tagsE.rhoInflowTag ) );
    execFactory.register_expression( new PlaceHolderT( tagsE.rhoEgyInflowTag ) );
    for( size_t i=0; i<ns-1; ++i ){
      execFactory.register_expression( new PlaceHolderT( tagsE.rhoYInflowTags[i] ) );
    }

    execFactory.register_expression( new PlaceHolderT( tagsE.tauMixTag ) );
    execFactory.register_expression( new PlaceHolderT( tagsE.convecCoeffTag ) );
    execFactory.register_expression( new PlaceHolderT( tagsE.convecTempTag ) );

//  mah (2/14/2018): this commented-out expression is an example of a time-dependent residence time.
//     execFactory.register_expression( new LinearFunctionT( tagsE.tauMixTag, tagsE.timeTag, 1.e7, 1.e-8 ) );

    execFactory.register_expression( new ZeroDMixingT( tagsE.rhoMixRhsTag, tagsE.rhoInflowTag, tagsE.rhoTag, tagsE.tauMixTag ) );
    execFactory.register_expression( new ZeroDMixingT( tagsE.rhoEgyMixRhsTag, tagsE.rhoEgyInflowTag, tagsE.rhoEgyTag, tagsE.tauMixTag ) );
    for( size_t i=0; i<ns-1; ++i ){
      execFactory.register_expression( new ZeroDMixingT( tagsE.rhoYMixRhsTags[i], tagsE.rhoYInflowTags[i], tagsE.rhoYTags[i], tagsE.tauMixTag ) );
    }

    // the full rhs
    execFactory.register_expression( new SumOpT( tagsE.rhoFullRhsTag, {tagsE.rhoKinRhsTag, tagsE.rhoMixRhsTag} ) );
    execFactory.register_expression( new SumOpT( tagsE.rhoEgyFullRhsTag, {tagsE.rhoEgyKinRhsTag, tagsE.rhoEgyMixRhsTag, tagsE.rhoEgyHeatRhsTag} ) );
    for( size_t i=0; i<ns-1; ++i ){
      execFactory.register_expression( new SumOpT( tagsE.rhoYFullRhsTags[i], {tagsE.rhoYKinRhsTags[i], tagsE.rhoYMixRhsTags[i]} ) );
    }
    // matrix transformation fields
    execFactory.register_expression( new SpcEgyOffsetsT( tagsE.egyOffsetTags, tagsE.rhoTag, tagsE.cvTag, tagsE.specEgyTags ) );
    execFactory.register_expression( new YOverRhoT( tagsE.mYoverRhoTags, tagsE.rhoTag, tagsE.massTags ) );
    execFactory.register_expression( new ProductT( tagsE.rhocvTag, tagsE.rhoTag, tagsE.cvTag ) );
    execFactory.register_expression( new InverseT( tagsE.invrhocvTag, tagsE.rhocvTag ) );
    execFactory.register_expression( new InverseT( tagsE.invrhoTag, tagsE.rhoTag ) );

    // add variables to the integrator
    integrator->add_variable<FieldT>( tagsE.rhoTag.name(), tagsE.rhoFullRhsTag, equIndex, equIndex );
    equIndex += 1;
    integrator->add_variable<FieldT>( tagsE.rhoEgyTag.name(), tagsE.rhoEgyFullRhsTag, equIndex, equIndex );
    equIndex += 1;
    for( size_t i=0; i<ns-1; ++i ){
      integrator->add_variable<FieldT>( tagsE.rhoYTags[i].name(), tagsE.rhoYFullRhsTags[i], i + equIndex, i + equIndex );
    }
    equIndex += ns-2;
  }

  //---------------------------------------------------------------------------

  void initialize_cv( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator,
                      Expr::FieldManagerList& fml,
                      const Expr::ExprPatch& patch,
                      Expr::ExpressionTree& initTree,
                      const Expr::Tag dsTag,
                      const double gesatMax,
                      const double timeStep,
                      const bool isRestart)
  {
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
    const ReactorEnsembleUtil::ReactorEnsembleTags tagsN( Expr::STATE_N );

    const int ns = CanteraObjects::number_species();

    if(!isRestart){
      initTree.register_fields( fml );
      initTree.bind_fields( fml );
      initTree.lock_fields( fml );
      std::ofstream initOut( "init-tree-reactor-ensemble.dot" );
      initTree.write_tree( initOut );
    }

    SpatialOps::OperatorDatabase sodb;
    integrator->prepare_for_integration( fml, sodb, patch.field_info() );

    {
      std::ofstream execOut( "exec-tree-reactor-ensemble.dot" );
      integrator->get_tree().write_tree( execOut );
    }

    fml.allocate_fields( patch.field_info() );
    if(!isRestart){
      initTree.execute_tree();
      // copy the residence time from STATE_N as initialized to STATE_NONE as used in mixing time expressions
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tauMixTag.name() );
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecCoeffTag.name() );
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecTempTag.name() );

      // copy the temperature because we need it for the initial guess in the T solve
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tempTag.name() );


      // copy the inflows over because they are fixed in time
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.rhoInflowTag.name() );
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.rhoEgyInflowTag.name() );
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tempInflowTag.name());
      integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.presInflowTag.name());
      for( size_t i=0; i<ns-1; ++i ){
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.rhoYInflowTags[i].name() );
      }
    }

    // initialize the simulation time and time step
    // The time step must be initialized here because we need to update t -> t + dt
    // before the dual time execution tree runs.
    fml.field_ref<FieldT>( tagsE.timeTag ) <<= 0.;
    fml.field_ref<SingleValueField>( tagsE.dtTag ) <<= timeStep;

    // initialize the dual time step
    fml.field_ref<FieldT>( dsTag ) <<= gesatMax;

    // jcs why are we locking these fields?  Can this be done automatically?
    integrator->lock_field<FieldT>( tagsE.dsTag );
    integrator->lock_field<FieldT>( tagsE.tauMixTag );
    integrator->lock_field<FieldT>( tagsE.convecCoeffTag );
    integrator->lock_field<FieldT>( tagsE.convecTempTag );
    integrator->lock_field<FieldT>( tagsE.timeTag );
    integrator->lock_field<SingleValueField>( tagsE.dtTag );
    integrator->lock_field<FieldT>( tagsE.tempTag );
    integrator->lock_field<FieldT>( tagsE.gesatEigTag );
    integrator->lock_field<FieldT>( tagsE.rhoInflowTag );
    integrator->lock_field<FieldT>( tagsE.rhoEgyInflowTag );
    for( size_t i=0; i<ns-1; ++i ){
      integrator->lock_field<FieldT>( tagsE.rhoYInflowTags[i] );
    }
  }

    //---------------------------------------------------------------------------

    void setup_cp( std::set<Expr::ExpressionID>& initRoots,
                   Expr::ExpressionFactory& initFactory,
                   const YAML::Node& parser ){

      const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_N );

      if(!parser["InitialConditions"]){
        initFactory.register_expression( new LinearFunctionT( tags.tempTag, tags.tempInflowTag, 1, 0 ) );
        initFactory.register_expression( new LinearFunctionT( tags.presTag, tags.presInflowTag, 1, 0 ) );
        const int ns = CanteraObjects::number_species();
        for( size_t i=0; i<ns; ++i ){
          initFactory.register_expression ( new LinearFunctionT( tags.massTags[i], tags.massInflowTags[i], 1, 0 ) );
        }
        initFactory.register_expression( new MixtureMWT( tags.mmwTag, tags.massTags, pokitt::FractionType::MASS ));
      }
      // initial state
      initRoots.insert( initFactory.register_expression( new EnthalpyT( tags.enthTag, tags.tempTag, tags.massTags )));
      initRoots.insert( initFactory.register_expression( new DensityT( tags.rhoTag, tags.tempTag, tags.presTag, tags.mmwTag )));
      initRoots.insert( initFactory.register_expression( new LogT( tags.tauMixTag, tags.tauMixLogTag )));
      // inflow state ( not necessary to be the same as initial state)
      initRoots.insert( initFactory.register_expression( new DensityT( tags.rhoInflowTag, tags.tempInflowTag, tags.presInflowTag, tags.mmwInflowTag )));
      initRoots.insert( initFactory.register_expression( new EnthalpyT( tags.enthInflowTag, tags.tempInflowTag, tags.massInflowTags )));

      const int ns = CanteraObjects::number_species();
      for( size_t i = 0;i < ns;++i ){
        initRoots.insert( initFactory.register_expression( new SpecEnthT( tags.specEnthTags[i], tags.tempTag, i )));
      }
    }

    //---------------------------------------------------------------------------

    void build_equation_system_cp( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                                   const boost::shared_ptr<pokitt::ChemicalSourceJacobian> csj,
                                   const YAML::Node& parser,
                                   int& equIndex,
                                   const double tempTolerance,
                                   const double maximumTemp,
                                   const double maxTempIter ){
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );

      const int ns = CanteraObjects::number_species();

      Expr::ExpressionFactory& execFactory = integrator->factory();

      integrator->register_root_expression( new PlaceHolderT( tagsE.timeTag ));
      integrator->register_root_expression( new PlaceHolderT( tagsE.tempInflowTag ) );
      integrator->register_root_expression( new PlaceHolderT( tagsE.presInflowTag ) );

      execFactory.register_expression( new ReactorEnsembleUtil::SpeciesNT( tagsE.massTags[ns - 1], tagsE.massTags, pokitt::CLIPSPECN ));

      execFactory.register_expression( new ReactorEnsembleUtil::TemperatureT( tagsE.tempTag, tagsE.massTags, tagsE.enthTag,
                                                                              Expr::Tag(), tempTolerance, maximumTemp, maxTempIter ));
      execFactory.register_expression( new ReactorEnsembleUtil::MixtureMWT( tagsE.mmwTag, tagsE.massTags, pokitt::FractionType::MASS ));

      const std::vector<double>& SpecMW = CanteraObjects::molecular_weights();
      for( size_t i = 0;i < ns;++i ){
        execFactory.register_expression( new SpecEnthT( tagsE.specEnthTags[i], tagsE.tempTag, i ));
        execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.molarWeightTags[i], SpecMW[i] ));
      }

      execFactory.register_expression( new HeatCpT( tagsE.cpTag, tagsE.tempTag, tagsE.massTags ));

      // kinetics component of the rhs
      execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.presKinRhsTag, 0.0 ));
      execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.enthKinRhsTag, 0.0 ));
      if ((!parser["ReactionInGas"]) or (parser["ReactionInGas"].as<std::string>() == "True") or (parser["ReactionInGas"].as<std::string>() == "true") ){
        execFactory.register_expression( new ReactorEnsembleUtil::RatesT( tagsE.rateTags,
                                                                          tagsE.tempTag,
                                                                          tagsE.rhoTag,
                                                                          tagsE.massTags,
                                                                          tagsE.mmwTag,
                                                                          csj ));
        for( size_t i = 0;i < ns - 1;++i ){
          execFactory.register_expression( new WOverRhoT( tagsE.yKinRhsTags[i], tagsE.rateTags[i], tagsE.rhoTag )); }
      }
      else{
        for( size_t i = 0;i < ns - 1;++i ){
          execFactory.register_expression( new ConstantT( tagsE.yKinRhsTags[i], 0 )); }
      }

      execFactory.register_expression( new DensityT( tagsE.rhoTag, tagsE.tempTag, tagsE.presTag, tagsE.mmwTag ));

      // inflow component of the rhs
      execFactory.register_expression( new PlaceHolderT( tagsE.enthInflowTag ));

      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new PlaceHolderT( tagsE.massInflowTags[i] ));
      }
      execFactory.register_expression( new PlaceHolderT( tagsE.rhoInflowTag ));
      execFactory.register_expression( new PlaceHolderT( tagsE.tauMixTag ));
      execFactory.register_expression( new PlaceHolderT( tagsE.convecCoeffTag ) );
      execFactory.register_expression( new PlaceHolderT( tagsE.convecTempTag ) );

//  mah (2/14/2018): this commented-out expression is an example of a time-dependent residence time.
//     execFactory.register_expression( new LinearFunctionT( tagsE.tauMixTag, tagsE.timeTag, 1.e7, 1.e-8 ) );

      execFactory.register_expression( new ReactorEnsembleUtil::ConstantT( tagsE.presMixRhsTag, 0.0 ));
      execFactory.register_expression( new ZeroDMixingCPT( tagsE.enthMixRhsTag, tagsE.enthInflowTag, tagsE.enthTag, tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new ZeroDMixingCPT( tagsE.yMixRhsTags[i], tagsE.massInflowTags[i], tagsE.massTags[i], tagsE.tauMixTag, tagsE.rhoInflowTag, tagsE.rhoTag ));
      }

      // the full rhs
      execFactory.register_expression( new SumOpT( tagsE.presFullRhsTag, { tagsE.presKinRhsTag, tagsE.presMixRhsTag } ));
      execFactory.register_expression( new SumOpT( tagsE.enthFullRhsTag, { tagsE.enthKinRhsTag, tagsE.enthMixRhsTag, tagsE.enthHeatRhsTag } ));
      for( size_t i = 0;i < ns - 1;++i ){
        execFactory.register_expression( new SumOpT( tagsE.yFullRhsTags[i], { tagsE.yKinRhsTags[i], tagsE.yMixRhsTags[i] } ));
      }
      execFactory.register_expression( new YNRHST( tagsE.yFullRhsTags[ns-1], tagsE.yFullRhsTags));
      // matrix transformation fields
      execFactory.register_expression( new SpcEnthOffsetsT( tagsE.enthOffsetTags, tagsE.cpTag, tagsE.specEnthTags ));
      execFactory.register_expression( new InverseT( tagsE.invCpTag, tagsE.cpTag ));
      execFactory.register_expression( new DivisionT( tagsE.rhoOverPresTag, tagsE.rhoTag, tagsE.presTag ));
      execFactory.register_expression( new RhoPartialHT( tagsE.rhoPartialHTag, tagsE.rhoTag, tagsE.cpTag, tagsE.tempTag ));
      execFactory.register_expression( new RhoPartialYT( tagsE.rhoPartialYTags, tagsE.rhoTag, tagsE.cpTag, tagsE.tempTag, tagsE.mmwTag, tagsE.molarWeightTags, tagsE.specEnthTags ));
      execFactory.register_expression( new YmixPartialYT( tagsE.yMixPartialYTag, tagsE.rhoTag, tagsE.rhoInflowTag, tagsE.tauMixTag ));
      execFactory.register_expression( new HmixPartialTT( tagsE.hMixPartialTTag, tagsE.rhoTag, tagsE.rhoInflowTag, tagsE.tauMixTag, tagsE.cpTag ));
      execFactory.register_expression( new HmixPartialYT( tagsE.hMixPartialYTags, tagsE.rhoTag, tagsE.rhoInflowTag, tagsE.tauMixTag, tagsE.specEnthTags ));

      // add variables to the integrator
      integrator->add_variable<FieldT>( tagsE.presTag.name(), tagsE.presFullRhsTag, equIndex, equIndex );
      equIndex += 1;
      integrator->add_variable<FieldT>( tagsE.enthTag.name(), tagsE.enthFullRhsTag, equIndex, equIndex );
      equIndex += 1;
      for( size_t i = 0;i < ns - 1;++i ){
        integrator->add_variable<FieldT>( tagsE.massTags[i].name(), tagsE.yFullRhsTags[i], i + equIndex, i + equIndex );
      }
      equIndex += ns-2;
    }

    //---------------------------------------------------------------------------

    void initialize_cp( Expr::DualTime::BlockImplicitBDFDualTimeIntegrator <FieldT, FieldT>* integrator,
                        Expr::FieldManagerList& fml,
                        const Expr::ExprPatch& patch,
                        Expr::ExpressionTree& initTree,
                        const Expr::Tag dsTag,
                        const double gesatMax,
                        const double timeStep,
                        const bool isRestart){
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsE( Expr::STATE_NONE );
      const ReactorEnsembleUtil::ReactorEnsembleTags tagsN( Expr::STATE_N );

      const int ns = CanteraObjects::number_species();

      if(!isRestart){
        initTree.register_fields( fml );
        initTree.bind_fields( fml );
        initTree.lock_fields( fml );
        std::ofstream initOut( "init-tree-reactor-ensemble.dot" );
        initTree.write_tree( initOut );
      }

      SpatialOps::OperatorDatabase sodb;
      integrator->prepare_for_integration( fml, sodb, patch.field_info());

      {
        std::ofstream execOut( "exec-tree-reactor-ensemble.dot" );
        integrator->get_tree().write_tree( execOut );
      }

      fml.allocate_fields( patch.field_info());
      if(!isRestart){
        initTree.execute_tree();

        // copy the residence time from STATE_N as initialized to STATE_NONE as used in mixing time expressions
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tauMixTag.name());
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecCoeffTag.name());
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.convecTempTag.name());

        // copy the temperature because we need it for the initial guess in the T solve
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tempTag.name());

        // copy the inflows over because they are fixed in time
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.enthInflowTag.name());
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.rhoInflowTag.name());
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.tempInflowTag.name());
        integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.presInflowTag.name());
        for( size_t i = 0;i < ns - 1;++i ){
          integrator->copy_from_initial_condition_to_execution<FieldT>( tagsE.massInflowTags[i].name());
        }
      }

      // initialize the simulation time and time step
      // The time step must be initialized here because we need to update t -> t + dt
      // before the dual time execution tree runs.
      fml.field_ref<FieldT>( tagsE.timeTag ) <<= 0.;
      fml.field_ref<SingleValueField>( tagsE.dtTag ) <<= timeStep;

      // initialize the dual time step
      fml.field_ref<FieldT>( dsTag ) <<= gesatMax;

      // jcs why are we locking these fields?  Can this be done automatically?
      integrator->lock_field<FieldT>( tagsE.dsTag );
      integrator->lock_field<FieldT>( tagsE.tauMixTag );
      integrator->lock_field<FieldT>( tagsE.convecCoeffTag );
      integrator->lock_field<FieldT>( tagsE.convecTempTag );
      integrator->lock_field<FieldT>( tagsE.timeTag );
      integrator->lock_field<SingleValueField>( tagsE.dtTag );
      integrator->lock_field<FieldT>( tagsE.tempTag );
      integrator->lock_field<FieldT>( tagsE.gesatEigTag );
      integrator->lock_field<FieldT>( tagsE.enthInflowTag );
      integrator->lock_field<FieldT>( tagsE.rhoInflowTag );
      for( size_t i = 0;i < ns - 1;++i ){
        integrator->lock_field<FieldT>( tagsE.massInflowTags[i] );
      }
    }


  //---------------------------------------------------------------------------

} // namespace ReactorEnsembleUtil
