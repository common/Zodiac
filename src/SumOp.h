/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  @file   SumOp.h
 *  @par    Getting the sum of the values in the given list
 *  @date   Oct 20, 2016
 *  @author Mike
 */

#ifndef SUMOP_H_
#define SUMOP_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil{
    /**
     * @class SumOp
     * @ingroup ReactorEnsembleUtil
     * @brief Getting the sum of the values in the given list
     * @author Hang
     *
     * The result is given as
     * \f[
     *    Sum = \sum^N var_i
     * \f]
     * Here, \f$ N \f$ is the number of values in the given list.
     *
     * The sensitivity of the result is given as
     * \f[
     *    \frac{\partial Sum}{\partial \phi} = \sum^N \frac{\partial var_i}{\partial \phi}
     * \f]
     */

template< typename FieldT >
  class SumOp : public Expr::Expression<FieldT>
  {

  DECLARE_VECTOR_OF_FIELDS( FieldT, operandList_ )
  const int nOperands_;

  SumOp( const Expr::TagList operandList )
  : Expr::Expression<FieldT>(),
    nOperands_( operandList.size() )
  {
    this->set_gpu_runnable(true);
    this->template create_field_vector_request<FieldT>( operandList, operandList_ );
  }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::TagList operandList_;
    public:
      /**
       * @brief The mechanism for building a SumOp object
       * @tparam FieldT
       * @param sumTag The sum result
       * @param operandList List of the values that need to be added
       */
      Builder( const Expr::Tag& sumTag,
               const Expr::TagList operandList )
      : Expr::ExpressionBuilder( sumTag ),
        operandList_( operandList )
      {}
      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new SumOp<FieldT>( operandList_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= operandList_[0]->field_ref();
      for( int i=1; i<nOperands_; ++i ){
        this->value() <<= this->value() + operandList_[i]->field_ref();
      }
    }
    void sensitivity( const Expr::Tag& var )
    {
      FieldT& dfdv = this->sensitivity_result( var );
      dfdv <<= operandList_[0]->sens_field_ref( var );
      for( int i=1; i<nOperands_; ++i ){
        dfdv <<= dfdv + operandList_[i]->sens_field_ref( var );
      }
    }
  };

} // namespace ReactorEnsembleUtil

#endif /* SUMOP_H_ */
