#ifndef ParseInputFile_h
#define ParseInputFile_h

#include <yaml-cpp/yaml.h>

#include <boost/shared_ptr.hpp>

#include <spatialops/structured/Grid.h>

#include <expression/ExprFwd.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include "FieldTypes.h"

namespace Expr{
  class ExpressionFactory; // forward
}

struct IntegratorParameters{
  double gesatMax   {1.0  };
  double gesatSafety{0.1  };
  double gesatRamp  {1.1  };
  double tolerance  {1e-4 };
  double timeStep   {1e305};
  double tend       {1e305};
};

typedef boost::shared_ptr<const SpatialOps::Grid> GridPtr;

GridPtr
parse_initial_inflow_conditions( const YAML::Node& parser,
                                 Expr::ExpressionFactory& factory,
                                 std::set<Expr::ExpressionID>& initRoots,
                                 const bool isRestart);

void
parse_reactor_parameters( const YAML::Node& parser,
                          Expr::ExpressionFactory& factory );

IntegratorParameters
parse_integrator_params( const YAML::Node& inputFile,
                         Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator );


struct FieldOutputInfo{
  struct FieldInfo{
    Expr::Context context;
    std::string alias;
    FieldInfo( const Expr::Context& c, const std::string& s ) : context(c), alias(s) {}
    FieldInfo(){}
  };
  typedef std::map< std::string, FieldInfo > FieldOutputRequest;
  FieldOutputRequest fields;
  int timeOutputInterval;
  int dualtimeOutputInterval;
  std::string outputPrefix;
  bool customPrefix = false;
};

FieldOutputInfo
parse_field_output( const YAML::Node& parser,
                    std::vector<std::string>,
                    std::vector<std::string>,
                    std::map<std::string, std::string>&);


#endif // ParseInputFile_h
