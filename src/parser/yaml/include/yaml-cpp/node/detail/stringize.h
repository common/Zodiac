#ifndef YAML_STRINGIZE_H_
#define YAML_STRINGIZE_H_

#include <string>
#include <vector>
#include <map>
#include <list>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

#include "yaml-cpp/null.h"

namespace YAML
{
  namespace detail
  {
    template<typename T>
    inline std::string stringize( const T& t ){
      return boost::lexical_cast<std::string>( t );
    }

    template<>
    inline std::string stringize<_Null>( const _Null& ){
      return "NULL YAML NODE";
    }

    template<typename T>
    inline std::string stringize( const std::vector<T>& tVec ){
      std::string s;
      BOOST_FOREACH( const T& t, tVec ){
        s += "\t" + boost::lexical_cast<std::string>( t ) + "\n";
      }
      return s;
    }

    template<typename T>
    inline std::string stringize( const std::list<T>& tList ){
      std::string s;
      BOOST_FOREACH( const T& t, tList ){
        s += "\t" + boost::lexical_cast<std::string>( t ) + "\n";
      }
      return s;
    }

    template<>
    inline std::string stringize( const std::map<std::string,std::string>& tMap ){
      typedef std::map<std::string,std::string> map_type;
      std::string s;
      BOOST_FOREACH( const map_type::value_type& vt, tMap ){
        s += "\t" + boost::lexical_cast<std::string>( vt.first  ) + " : "
            + boost::lexical_cast<std::string>( vt.second ) + "\n";
      }
      return s;
    }
  }
}


#endif /* YAML_STRINGIZE_H_ */
