#include <FieldTypes.h>
#include <parser/ParseInputFile.h>
#include <parser/GridVar.h>
#include <ReactorEnsembleUtil.h>

#include <ConvectiveHeatTransferRate.h>
#include <TransformationMatrixExpressions.h>
#include <GesatStepSize.h>

#include <string>
#include <sstream>
#include <exception>

#include <boost/foreach.hpp>
#include <boost/make_shared.hpp>

#include <pokitt/CanteraObjects.h>
#include <pokitt/SpeciesN.h>
#include <pokitt/MixtureFractionExpr.h>
#include <pokitt/MixtureMolWeight.h>
#include <pokitt/MoleToMassFracs.h>
#include <pokitt/MassToMoleFracs.h>

#include <expression/Functions.h>
#include <expression/Expression.h>

#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include "particles/ParticleInterface.h"
#include "PlugFlowReactor/PlugFlowReactorInterface.h"

//-------------------------------------------------------------------

struct GridSpec{
  SpatialOps::IntVec npts;
  SpatialOps::DoubleVec lo, hi;
  std::string name[3];
};

//-------------------------------------------------------------------

struct SpecInfo{
  std::map<std::string,double> comp;
  bool isMoleFrac;
};

SpecInfo
parse_species_comps( const YAML::Node& parser )
{
  SpecInfo specInfo;

  BOOST_FOREACH( const std::string& sp, CanteraObjects::species_names() ){
    specInfo.comp[sp] = 0.0;
  }

  // look through IC nodes to determine which ones form the grid
  int idim=0;
  BOOST_FOREACH( const YAML::Node& spec, parser ){
    assert( spec.IsDefined() );
    const YAML::Node& var = spec["species"     ];
    const YAML::Node& val = spec["MoleFraction"];
    const std::string name = var.as<std::string>();
    specInfo.comp[name] = val.as<double>();
  }
  return specInfo;
}

//-------------------------------------------------------------------

//void setup_species_expressions( Expr::ExpressionFactory& factory,
//                                const SpecInfo& specInfo )
//{
//  const size_t nspec = CanteraObjects::number_species();
//  typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;
//  typedef pokitt::SpeciesN  <FieldT>::Builder SpeciesN;
//  for( auto& sp : specInfo.comp ){
//    const std::string& spName = sp.first;
//    const double moleFrac = sp.second;
//    if( spName == CanteraObjects::species_name(nspec-1) ){
//      const Expr::Tag specN( "X_"+spName, Expr::STATE_N );
//      factory.register_expression( new SpeciesN( specN,
//                                                 Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N, "X_" ),
//                                                 pokitt::ERRORSPECN ),
//                                   true /* allow overwrite */ );
//    }
//    else{
//      factory.register_expression( new ConstExpr( Expr::Tag("X_"+spName,Expr::STATE_N), moleFrac ) );
//    }
//  }
//}

//-------------------------------------------------------------------

void parse_mixture_fraction( Expr::ExpressionFactory& factory,
                             const YAML::Node& parser,
                             const std::string reactorType)
{
  const SpecInfo fuelComp = parse_species_comps( parser["fuel"    ] );
  const SpecInfo oxidComp = parse_species_comps( parser["oxidizer"] );

  const double zLo = parser["minimum"].as<double>(0);
  const double zHi = parser["maximum"].as<double>(1);

  const std::string compState = parser["composition"].as<std::string>();
  const bool reacted = ( compState == "reacted" );

  if( reacted ){
    // jcs note that to get the reacted state, we would need to also determine
    // the reacted temperature of the mixture, which we are not yet set up to do.
    std::ostringstream msg;
    msg << "ERROR: " << __FILE__ << " : " << __LINE__
        << "\nReacted initial state from mixture fraction is not yet supported.\n\n";
  }

  auto mole_to_mass = []( const SpecInfo& si ){
    const auto nspec = CanteraObjects::number_species();
    std::vector<double> massFracs(nspec,0.0);
    const auto& mw = CanteraObjects::molecular_weights();
    double mwMix = 0;
    for( const auto& sp : si.comp ){
      const double xi = sp.second;
      const size_t i =  CanteraObjects::species_index(sp.first);
      const double mwi = mw[i];
      massFracs[i] = xi*mwi;
      mwMix += xi * mwi;
    }
    for( auto& yi : massFracs ){
      yi /= mwMix;
    }
    return massFracs;
  };

  const ReactorEnsembleUtil::ReactorEnsembleTags tagsN(Expr::STATE_N);

  // create the expression to calculate the composition from the mixture fraction
  typedef pokitt::MixtureFractionToSpecies<FieldT> Z2Y;
  typedef Z2Y::Builder MixFracToSpec;
  if( reactorType !="PFR"){
    factory.register_expression( new MixFracToSpec( tagsN.massInflowTags,
                                                    tagsN.mixtureFractionTag,
                                                    mole_to_mass( fuelComp ),
                                                    mole_to_mass( oxidComp ),
                                                    reacted ? Z2Y::REACTED_COMPOSITION : Z2Y::UNREACTED_COMPOSITION ) );
  }
}

//-------------------------------------------------------------------

void
create_ic_inflow_expression( Expr::ExpressionFactory& factory,
                             const GridPtr grid,
                             const GridSpec& gridSpec,
                             const YAML::Node& parser,
                             std::set<Expr::ExpressionID>& initRoots )
{
  const Expr::Tag tag( parser["variable"].as<std::string>(), Expr::STATE_N );
  const YAML::Node& val = parser["value"];

  if( val.size() <= 1 ){
    long double ld;
    typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;
    if ((std::istringstream(val.as<std::string>()) >> ld >> std::ws).eof()){
      // we need to insert it as a root because things like the mixing time will NOT be depended upon in the initial condition tree
      initRoots.insert( factory.register_expression( new ConstExpr( tag, val.as<double>())));
    }
  }
  else{
    if( tag.name() == gridSpec.name[0] ){
      typedef GridVar<FieldT, SpatialOps::XDIR>::Builder GridVar;
      initRoots.insert( factory.register_expression( new GridVar( tag, grid )));
    }
    else if( tag.name() == gridSpec.name[1] ){
      typedef GridVar<FieldT, SpatialOps::YDIR>::Builder GridVar;
      initRoots.insert( factory.register_expression( new GridVar( tag, grid )));
    }
    else if( tag.name() == gridSpec.name[2] ){
      typedef GridVar<FieldT, SpatialOps::ZDIR>::Builder GridVar;
      initRoots.insert( factory.register_expression( new GridVar( tag, grid )));
    }
    else{
      std::ostringstream msg;
      msg << "ERROR: Something went wrong in detecting grid information\n"
          << "  while looking at " << tag.name() << std::endl
          << "Detected grid variables follow:\n";
      BOOST_FOREACH( const std::string& nam, gridSpec.name ){
              msg << "\t" << nam << std::endl;
            }
      msg << __FILE__ << " : " << __LINE__ << std::endl;
      throw std::runtime_error( msg.str());
    }
  }


}

//-------------------------------------------------------------------

void
parse_reactor_parameters( const YAML::Node& rootParser,
                          Expr::ExpressionFactory& factory )
{

  const YAML::Node parser = rootParser["ReactorParameters"];

  if( !parser ){
    std::cout << "No 'ReactorParameters' spec was found in the input file.  Using default parameters\n";
  }
  const std::string reactorShape = parser["ReactorShape"].as<std::string>("Spherical");
  const double radius = parser["Radius"].as<double>( 0.001 );
  const double height = parser["Height"].as<double>( 0.0 );
  double SoV;

  std::cout << std::endl;
  std::cout << "Reactor ensemble specifications"              << std::endl
            << "------------------------------------"         << std::endl
            << " - reactor shape          : " << reactorShape << std::endl
            << " - reactor radius (m)     : " << radius       << std::endl;
  if(reactorShape == "Cylinder"){
    std::cout << " - reactor height (m)     : " << height       << std::endl;
    SoV = 2.0 / radius; // 1/m, surface area to volume ratio; only surrounding surface is considered
  }
  if(reactorShape == "Spherical"){
    SoV = 3.0 / radius;
  }
  std::cout << " - area/volume ratio (1/m): " << SoV        << std::endl;

  // build the expressions implied by these quantities
  const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_NONE );

  if (parser["ReactorType"].as<std::string>() == "PSRConstantVolume"){
    typedef ReactorEnsembleUtil::ConvectionHeatRateCV<FieldT>::Builder ConvectionCVT;
    factory.register_expression( new ConvectionCVT( tags.rhoEgyHeatRhsTag, tags.tempTag, tags.convecCoeffTag, tags.convecTempTag, SoV ) );
  }
  if (parser["ReactorType"].as<std::string>() == "PSRConstantPressure"){
    typedef ReactorEnsembleUtil::ConvectionHeatRateCP<FieldT>::Builder ConvectionCPT;
    factory.register_expression( new ConvectionCPT( tags.enthHeatRhsTag, tags.tempTag, tags.rhoTag, tags.convecCoeffTag, tags.convecTempTag, SoV ));
  }
  if(parser["ReactorType"].as<std::string>() == "PFR"){
    const PlugFlowReactorInterface::PlugFlowReactorTags tagspfr( Expr::STATE_NONE );
    typedef ReactorEnsembleUtil::ConvectionHeatRatePFR<FieldT>::Builder ConvectionPFRT;
    factory.register_expression( new ConvectionPFRT( tags.enthHeatRhsTag, tags.tempTag, tags.rhoTag, tags.convecCoeffTag, tags.convecTempTag, tagspfr.uTag, SoV ));
  }


  const std::string ReactionInGas = rootParser["ReactionInGas"].as<std::string>( "True" );
  if(ReactionInGas == "True"){
    std::cout << " - number of species      : " << CanteraObjects::number_species() << std::endl
              << " - number of reactions    : " << CanteraObjects::number_rxns()    << std::endl
              << "------------------------------------"      << std::endl
              << std::endl;
  }
  else{
    std::cout << " - Chemical reaction is turned off for gas phase" << std::endl;
  }

}

//-------------------------------------------------------------------

GridPtr
parse_initial_inflow_conditions( const YAML::Node& parser,
                                 Expr::ExpressionFactory& factory,
                                 std::set<Expr::ExpressionID>& initRoots,
                                 const bool isRestart)
{
  const std::string reactorType = parser["ReactorParameters"]["ReactorType"].as<std::string>();
  if( reactorType != "PFR"){
    if( !parser["InflowConditions"] ){
      std::ostringstream msg;
      msg << "No inflow condition block 'InflowConditions' was found!\n";
      throw std::runtime_error( msg.str());
    }

    if( !parser["InitialConditions"] ){
      std::cout  << "No initial condition block 'InitialConditions' was found! Initial conditions are set to be the same as inflow conditions\n" << std::endl;
    }
  }
  else{
    if( !parser["InitialConditions"] ){
      std::ostringstream msg;
      msg << "No initial condition block 'InitialConditions' was found! (No inflow condition was found for plug flow reactor)\n";
      throw std::runtime_error( msg.str());
    }
  }


  bool hasMixtureFraction = false;
  int ic_inflow_size = 0;

  YAML::Node ic_inflow;
  if(reactorType!="PFR"){
    const YAML::Node inflow = parser["InflowConditions"];
    ic_inflow_size = inflow.size();
    for( int i = 0;i < inflow.size() ;++i ){
      ic_inflow[i] = inflow[i];
    }
    if(parser["InitialConditions"]){
      const YAML::Node ic = parser["InitialConditions"];
      for( int i = inflow.size();i < ic.size() + inflow.size();++i ){
        ic_inflow[i] = ic[i - inflow.size()];
      }
      ic_inflow_size += ic.size();
    }
  }
  else{
    const YAML::Node ic = parser["InitialConditions"];
    ic_inflow_size = ic.size();
    for( int i = 0;i < ic.size() ;++i ){
      ic_inflow[i] = ic[i];
    }
  }

  // Both of heat transfer coefficient and surroundings temperature must be given
  // or not given in "HeatTransferParameter" simultaneously
  if(parser["ReactorParameters"]["HeatTransferParameter"]){
    const YAML::Node heatTrans = parser["ReactorParameters"]["HeatTransferParameter"];
    ic_inflow[ic_inflow_size]["variable"]="heat_transfer_coefficient";
    ic_inflow[ic_inflow_size]["value"]=heatTrans["HeatTransferCoefficient"];
    ic_inflow[ic_inflow_size+1]["variable"]="surroundings_temperature";
    ic_inflow[ic_inflow_size+1]["value"]=heatTrans["SurroundingsTemperature"];
    ic_inflow_size += 2;
  }

  if(parser["Particles"] and parser["Particles"]["ParticleType"].as<std::string>() == "Coal"){
    const YAML::Node equvaRatio = parser["Particles"]["FuelAirEquivalenceRatio"];
    ic_inflow[ic_inflow_size]["variable"]="fuel_air_equiv_ratio";
    ic_inflow[ic_inflow_size]["value"]=equvaRatio;
  }

  GridSpec gridSpec;
  gridSpec.npts = SpatialOps::IntVec( 1, 1, 1 );
  std::set<std::string> uninitializedSpecies, uninflowSpecies;
  if(parser["InitialConditions"]){
    BOOST_FOREACH( const std::string& sp_ic, CanteraObjects::species_names()){
            uninitializedSpecies.insert( sp_ic );
          }
  }
  if(reactorType!="PFR"){
    BOOST_FOREACH( const std::string& sp_inflow, CanteraObjects::species_names()){
            uninflowSpecies.insert( sp_inflow );
          }
  }

  // look through IC and inflow nodes to determine which ones form the grid
  int idim = 0;
  BOOST_FOREACH( const YAML::Node& ic_inflow_Spec, ic_inflow ){
          assert( ic_inflow_Spec.IsDefined());
          const YAML::Node& var = ic_inflow_Spec["variable"];
          const YAML::Node& val = ic_inflow_Spec["value"];
          const std::string name = var.as<std::string>();
          if( val.size() > 1 ){
            gridSpec.npts[idim] = val["npts"].as<int>();
            gridSpec.lo[idim] = val["minimum"].as<double>();
            gridSpec.hi[idim] = val["maximum"].as<double>();
            gridSpec.name[idim] = name;
            if( idim > 2 ){
              std::ostringstream msg;
              msg << "ERROR: cannot have more than three grid variables initialized\n"
                  << "Ensure that the 'InitialConditions' block has at most 3 non-constant specifications\n"
                  << __FILE__ << " : " << __LINE__ << std::endl;
              throw std::runtime_error( msg.str());
            }
            ++idim;
          }
          // keep track of initialized/inflow species
          if( name.substr( 0, 2 ) == "X_" || name.substr( 0, 2 ) == "Y_" ){
            if(reactorType!="PFR"){
              if(parser["InitialConditions"]){
                // check if it is inflow parameters ( The minimum size of name is 3. So, just check the last three letters.)
                if( name.substr( name.size() - 3 ) == "low" ){
                  const std::string spnam = name.substr( 2, name.size() - 9 );
                  uninflowSpecies.erase( spnam );
                }
                else{
                  const std::string spnam = name.substr( 2 );
                  uninitializedSpecies.erase( spnam );
                }
              }
              else{
                const std::string spnam = name.substr( 2, name.size() - 9 );
                uninflowSpecies.erase( spnam );
              }
            }
            else{
              const std::string spnam = name.substr( 2 );
              uninitializedSpecies.erase( spnam );
            }
          }
        }

  if( idim == 0 ){
    std::cout << "WARNING: no 'grid' variables were found in parsing\n";
  }

  const GridPtr grid = boost::make_shared<SpatialOps::Grid>( gridSpec.npts,
                                                             SpatialOps::DoubleVec( gridSpec.hi - gridSpec.lo ),
                                                             gridSpec.lo );

  if(!isRestart){
    BOOST_FOREACH( const YAML::Node& ic_inflow_Spec, ic_inflow ){
            create_ic_inflow_expression( factory, grid, gridSpec, ic_inflow_Spec, initRoots );
            if( ic_inflow_Spec["variable"].as<std::string>() == "mixture_fraction" ){
              hasMixtureFraction = true;
              parse_mixture_fraction( factory, ic_inflow_Spec, reactorType );
            }
          }
    const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_N );
    const int nspec = CanteraObjects::number_species();
    typedef pokitt::SpeciesN<FieldT>::Builder SpeciesN;
    typedef pokitt::MixtureMolWeight<FieldT>::Builder MixtureMWT;
    typedef pokitt::MoleToMassFracs<FieldT>::Builder MoleToMassT;
    typedef pokitt::MassToMoleFracs<FieldT>::Builder MassToMoleT;
    if(parser["InitialConditions"]){
      if(parser["InitialInflowData"]["InitialSpeciesSetting"].as<std::string>("FromInputFile") == "FromInputFile"){
        if( uninitializedSpecies.size() == CanteraObjects::number_species()){
          std::ostringstream msg;
          msg << "\nERROR from " << __FILE__ << " : " << __LINE__
              << "No species composition has been specified in the initial conditions.\n\n";
          throw std::runtime_error( msg.str());
        }

        // set uninitialized species mole fractions to zero
        BOOST_FOREACH( const std::string& sp_ic, uninitializedSpecies ){
                typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;
                if( sp_ic == CanteraObjects::species_name( nspec - 1 )) continue;
                const auto ix = CanteraObjects::species_index( sp_ic );
                factory.register_expression( new ConstExpr( tags.moleTags[ix], 0 ));
              }
        // Species N
        factory.register_expression( new SpeciesN( tags.moleTags[nspec - 1],
                                                   Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N, "X_" ),
                                                   pokitt::ERRORSPECN ),
                                     true /* allow overwrite */ );
      }
      else{
        // Species N
        factory.register_expression( new SpeciesN( tags.moleTags[nspec - 1],
                                                   Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N, "X_" ),
                                                   pokitt::CLIPSPECN ),
                                     true /* allow overwrite */ );
      }


      factory.register_expression( new MixtureMWT( tags.mmwTag, tags.moleTags, pokitt::FractionType::MOLE ));
      factory.register_expression( new MoleToMassT( tags.massTags, tags.moleTags, tags.mmwTag ));
    }

    if(reactorType!="PFR"){
      if( !hasMixtureFraction ){

        if(parser["InitialInflowData"]["InflowSpeciesSetting"].as<std::string>("FromInputFile") == "FromInputFile"){
          if(( uninflowSpecies.size() == CanteraObjects::number_species())){
            std::ostringstream msg;
            msg << "\nERROR from " << __FILE__ << " : " << __LINE__
                << "No species composition has been specified in the inflow conditions.\n\n";
            throw std::runtime_error( msg.str());
          }

          // set unspecified inflow species mole fractions to zero
          BOOST_FOREACH( const std::string& sp_inflow, uninflowSpecies ){
                  typedef Expr::ConstantExpr<FieldT>::Builder ConstExpr;
                  if( sp_inflow == CanteraObjects::species_name( nspec - 1 )) continue;
                  const auto ix = CanteraObjects::species_index( sp_inflow );
                  factory.register_expression( new ConstExpr( tags.moleInflowTags[ix], 0 ));
                }
          // Species N
          factory.register_expression( new SpeciesN( tags.moleInflowTags[nspec - 1],
                                                     Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N, "X_",
                                                                     "_inflow" ),
                                                     pokitt::ERRORSPECN ),
                                       true /* allow overwrite */ );
        }
        else{
          // Species N
          factory.register_expression( new SpeciesN( tags.moleInflowTags[nspec - 1],
                                                     Expr::tag_list( CanteraObjects::species_names(), Expr::STATE_N, "X_",
                                                                     "_inflow" ),
                                                     pokitt::CLIPSPECN ),
                                       true /* allow overwrite */ );
        }
        factory.register_expression( new MoleToMassT( tags.massInflowTags, tags.moleInflowTags, tags.mmwInflowTag ));
        factory.register_expression( new MixtureMWT( tags.mmwInflowTag, tags.moleInflowTags, pokitt::FractionType::MOLE ));
      }
      else{
        typedef pokitt::MixtureMolWeight<FieldT>::Builder MixtureMWT;
        factory.register_expression( new MassToMoleT( tags.moleInflowTags, tags.massInflowTags, tags.mmwInflowTag ));
        factory.register_expression( new MixtureMWT( tags.mmwInflowTag, tags.massInflowTags, pokitt::FractionType::MASS ));
      }
    }

    if(!parser["ReactorParameters"]["HeatTransferParameter"]){
      typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
      //default heat transfer coefficient (0) and surroundings temperature (300K) between gas and surroundings
      initRoots.insert( factory.register_expression( new ConstantT( tags.convecCoeffTag , 0.0)));
      initRoots.insert( factory.register_expression( new ConstantT( tags.convecTempTag , 300)));
    }
  }

  return grid;
}


//-------------------------------------------------------------------

IntegratorParameters
parse_integrator_params( const YAML::Node& parser,
                         Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT,FieldT>* integrator )
{
  IntegratorParameters params;

  if( !parser["TimeIntegrator"] ){
    std::cout << "A 'TimeIntegrator' block was not found -- setting default time integrator parameters\n";
  }
  else{
    // user specified a TimeIntegrator block, so parse it and set values as they are found
    const YAML::Node integrator = parser["TimeIntegrator"];

    params.gesatMax    = integrator["GESAT_Max"   ].as<double>(params.gesatMax   );
    params.gesatSafety = integrator["GESAT_Safety"].as<double>(params.gesatSafety);
    params.gesatRamp   = integrator["GESAT_Ramp"  ].as<double>(params.gesatRamp  );
    params.tolerance   = integrator["tolerance"   ].as<double>(params.tolerance  );
    params.timeStep    = integrator["timestep"    ].as<double>(params.timeStep   );
    params.tend        = integrator["end_time"    ].as<double>(params.tend       );
  }

  const ReactorEnsembleUtil::ReactorEnsembleTags tags( Expr::STATE_NONE );

  typedef Expr::ConstantExpr<SingleValueField>::Builder ConstantSingleValueT;
  typedef GesatStepSize     <FieldT          >::Builder GesatStepSizeT;

  integrator->set_physical_time_step_expression( new ConstantSingleValueT( integrator->get_time_step_tag(), params.timeStep ) );
  integrator->set_dual_time_step_expression    ( new GesatStepSizeT( integrator->get_dual_time_step_tag(), tags.gesatEigTag, params.gesatSafety, params.gesatMax, params.gesatRamp ) );

  return params;
}

//-------------------------------------------------------------------

FieldOutputInfo
parse_field_output( const YAML::Node& parser,
                    std::vector<std::string> particleNumArray,
                    std::vector<std::string> restartVars,
                    std::map<std::string, std::string>& varMap)
{
  const YAML::Node& output = parser["Output"];

  FieldOutputInfo info;
  info.dualtimeOutputInterval = output["dualtime_interval"].as<int>();
  info.timeOutputInterval     = output["time_interval"    ].as<int>();

  if( output["output_prefix"] ){
    info.customPrefix = true;
    info.outputPrefix = output["output_prefix"].as<std::string>();
  }

  const Expr::Context context( Expr::STATE_NONE );

  // set fields that we always want output.  Users can override the alias as needed below.
  {
    const ReactorEnsembleUtil::ReactorEnsembleTags tags( context );
    auto insert = [&]( const Expr::Tag& tag ){
      info.fields[tag.name()] = FieldOutputInfo::FieldInfo(context, tag.name());
    };
    // request output of all solution variables and temperature so that we can restart.
    for( std::vector<std::string>::const_iterator inm=restartVars.begin(); inm!=restartVars.end(); ++inm ){
      insert( Expr::Tag(*inm,context) );
      varMap.insert( std::pair<std::string, std::string> (*inm, *inm) );
    }
    insert( tags.dtTag     );
    insert( tags.expEigTag );

    if (parser["ReactorParameters"]["ReactorType"].as<std::string>() == "PSRConstantVolume"){
      insert( tags.presTag   );
      for( size_t i=0; i<CanteraObjects::number_species(); ++i){
        insert( tags.massTags[i] );
      }
    }
    if (parser["ReactorParameters"]["ReactorType"].as<std::string>() == "PSRConstantPressure"){
      insert( tags.rhoTag    );
    }
    if (parser["ReactorParameters"]["ReactorType"].as<std::string>() == "PFR"){
      insert( tags.presTag    );
    }

    if(parser["Particles"]){
      const Particles::ParticleEnsembleTags tagspart( Expr::STATE_NONE, particleNumArray );
      const int npartsize = particleNumArray.size();
      for( size_t i=0; i<npartsize; ++i){
        insert( tagspart.massEachPartTags[i] );
        insert( tagspart.partNumPerVolumeTags[i] );
        insert( tagspart.partSizeTags[i] );
        insert( tagspart.partRhoTags[i] );
      }
      if(parser["Particles"]["ParticleType"].as<std::string>() == "Coal"){
        const Coal::CoalEnsembleTags tagsCoal(Expr::STATE_NONE, particleNumArray );
        insert( tagsCoal.fuelAirEquivalenceRatio );
        for( size_t i=0; i<npartsize; ++i){
          insert( tagsCoal.moistureMassEachPartTags[i] );
          insert( tagsCoal.volMassEachPartTags[i] );
          insert( tagsCoal.charMassEachPartTags[i] );
        }
      }
    }

  }

  // grab user-requested field output, which may overwrite the above information with a different alias.
  const YAML::Node& fields = output["Fields"];
  BOOST_FOREACH( const YAML::Node& f, fields ){
    const std::string name = f["name"].as<std::string>();
    info.fields[ name ] = FieldOutputInfo::FieldInfo( context, f["alias"].as<std::string>(name) );
    // Check if the variable requested for output is one required for restarts. If so, update its alias.
    std::map<std::string, std::string>::iterator itr = varMap.find(name);
    if( itr != varMap.end()) itr->second = f["alias"].as<std::string>(name);
  }

  return info;
}

//-------------------------------------------------------------------
