/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef GridVar_Expr_h
#define GridVar_Expr_h

#include <expression/Expression.h>

#include <spatialops/structured/Grid.h>
#include <boost/shared_ptr.hpp>
#include "FieldTypes.h"


/**
 *  \class GridVar
 */
template< typename FieldT,
          typename CoordT >
class GridVar
 : public Expr::Expression<FieldT>
{
  const boost::shared_ptr<const SpatialOps::Grid> grid_;
  GridVar( const boost::shared_ptr<const SpatialOps::Grid> grid );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const boost::shared_ptr<const SpatialOps::Grid> grid_;
  public:
    /**
     *  @brief Build a GridVar expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const boost::shared_ptr<const SpatialOps::Grid> grid )
      : ExpressionBuilder( resultTag, 0 /* no ghosts */ ),
        grid_( grid )
    {}

    Expr::ExpressionBase* build() const{
      return new GridVar<FieldT,CoordT>( grid_ );
    }

  };  /* end of Builder class */

  ~GridVar(){}
  void bind_operators( const SpatialOps::OperatorDatabase& opDB ){}
  void evaluate();
};


#endif // GridVar_Expr_h
