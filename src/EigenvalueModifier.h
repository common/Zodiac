/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * @file EigenvalueModifier.h
 * @par Getting the GESAT eigenvalue of the RHS jacobian matrix
 * @authors Hang
 */

#ifndef EIGENVALUE_MODIFIER_H_
#define EIGENVALUE_MODIFIER_H_

#include <expression/Expression.h>

namespace ReactorEnsembleUtil{

  /**
   * @class EigenvalueModifier
   * @ingroup ReactorEnsembleUtil
   * @brief Getting the GESAT eigenvalue of the RHS jacobian matrix under constant volume condition
   * @author Mike
   * @par Reference:
   *      "Dual Timestepping Methods for Detailed Combustion Chemistry", Michael Hansen, James Sutherland. Combustion Theory and Modeling.
   */

template< typename FieldT >
  class EigenvalueModifier : public Expr::Expression<FieldT>
  {

  DECLARE_FIELDS( FieldT, expEig_ )

  const double timeStepSize_;

  EigenvalueModifier( const Expr::Tag& expEigTag,
                      const double timeStepSize )
  : Expr::Expression<FieldT>(),
    timeStepSize_( timeStepSize )
  {
    this->set_gpu_runnable(true);
    expEig_ = this->template create_field_request<FieldT>( expEigTag );
  }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag expEigTag_;
      const double timeStepSize_;
    public:
      /**
       * @brief The mechanism for building a EigenvalueModifier object
       * @tparam FieldT
       * @param gesatEigTag GESAT eigenvalue
       * @param expEigTag explosive eigenvalue
       * @param temStepSize time step
       */
      Builder( const Expr::Tag& gesatEigTag,
               const Expr::Tag& expEigTag,
               const double timeStepSize )
      : Expr::ExpressionBuilder( gesatEigTag ),
        expEigTag_( expEigTag ),
        timeStepSize_( timeStepSize )
      {}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new EigenvalueModifier<FieldT>( expEigTag_, timeStepSize_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;
      this->value() <<= max(expEig_->field_ref() - 1. / timeStepSize_, 0.);
    }
  };

} // namespace ReactorEnsembleUtil

#endif /* EIGENVALUE_MODIFIER_H_ */
