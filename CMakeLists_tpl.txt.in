# See https://crascit.com/2015/07/25/cmake-gtest/ for the original place that I got the idea of how to do this.

cmake_minimum_required( VERSION 3.5 )
project( zodiac_tpl LANGUAGES CXX )
include( ExternalProject )

message( STATUS "Building PoKiTT, ExprLib, and SpatialOps in @TPL_DIR@" )

# Set desired versions for the TPLs 
set( SO_HASH     41b39898909475fc9e2b2065618b7297f6b5cd56 )
set( EXPR_HASH   661c6abaa15dbf1dcf49a6ee7c9e9cee08053622 )
set( POKITT_HASH fc1aa3bb281b1a7d6a08264c91a83fcdd7c8758e )

# --- SpatialOps
set( SO_CMAKE_ARGS
    -DCMAKE_BUILD_TYPE=@CMAKE_BUILD_TYPE@
    -DCMAKE_INSTALL_PREFIX=@TPL_DIR@
    -DENABLE_THREADS=OFF
    -DENABLE_CUDA=OFF
    -DENABLE_TESTS=OFF
    -DENABLE_EXAMPLES=OFF
    -DBOOST_INCLUDEDIR=@BOOST_INCLUDE@
    -DCMAKE_CXX_COMPILER=@CMAKE_CXX_COMPILER@
    )
message( STATUS "SpatialOps configure line:\n\t${SO_CMAKE_ARGS}\n" )
ExternalProject_Add(
    SpatialOps_BUILD
    PREFIX          @TPL_DIR@
    GIT_REPOSITORY  https://gitlab.multiscale.utah.edu/common/SpatialOps.git
    GIT_TAG         ${SO_HASH}
    CMAKE_ARGS      ${SO_CMAKE_ARGS}
    BUILD_COMMAND   make -j8 
    INSTALL_COMMAND make -j8 install
    )

# --- ExprLib
set( EXPR_CMAKE_ARGS
    -DCMAKE_BUILD_TYPE=@CMAKE_BUILD_TYPE@
    -DCMAKE_INSTALL_PREFIX=@TPL_DIR@
    -DSpatialOps_DIR=@TPL_DIR@/share
    -DDEFAULT_NUMBER_OF_GHOSTS=0
    -DENABLE_TESTS=OFF
    -DENABLE_OUTPUT=ON
    -DBUILD_GUI=OFF
    -DCMAKE_CXX_COMPILER=@CMAKE_CXX_COMPILER@
    )
message( STATUS "ExprLib configure line:\n\t${EXPR_CMAKE_ARGS}\n" )
ExternalProject_Add( 
    ExprLib_BUILD
    DEPENDS         SpatialOps_BUILD 
    PREFIX          @TPL_DIR@
    GIT_REPOSITORY  https://gitlab.multiscale.utah.edu/common/ExprLib.git
    GIT_TAG         ${EXPR_HASH}
    CMAKE_ARGS      ${EXPR_CMAKE_ARGS}
    BUILD_COMMAND   make -j8 
    INSTALL_COMMAND make -j8 install
    )

# --- PoKiTT
set( POKITT_CMAKE_ARGS  
    -DCMAKE_BUILD_TYPE=@CMAKE_BUILD_TYPE@
    -DCMAKE_INSTALL_PREFIX=@TPL_DIR@
    -DExprLib_DIR=@TPL_DIR@/share
    -DBUILD_UPSTREAM_LIBS=OFF
    -DENABLE_TESTS=OFF
    -DENABLE_EXAMPLES=OFF
    -DCMAKE_CXX_COMPILER=@CMAKE_CXX_COMPILER@
    -DCantera_DIR=@Cantera_DIR@
    )
message( STATUS "PoKiTT configure line:\n\t${POKITT_CMAKE_ARGS}\n" )
ExternalProject_Add( 
    PoKiTT_BUILD
    DEPENDS         ExprLib_BUILD 
    PREFIX          @TPL_DIR@
    GIT_REPOSITORY  https://gitlab.multiscale.utah.edu/common/PoKiTT.git
    GIT_TAG         ${POKITT_HASH}
    CMAKE_ARGS      ${POKITT_CMAKE_ARGS}
    BUILD_COMMAND   make -j8 
    INSTALL_COMMAND make -j8 install
    )
