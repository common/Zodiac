To run `Zodiac` you will need to have  `cantera` installed which can be cloned from:
https://gitlab.multiscale.utah.edu/james/cantera

You will need `python2.7` and `scons` for `python2.7` (e.g. `scons 2.5.1`) to install this `cantera`.

Instructions for installing `cantera` can be found at:
https://multiscale.utah.edu/installing-cantera/

You will also need `cmake` to install `Zodiac`.

Clone the `Zodiac` repository and create a `build` directory:
```
git clone https://gitlab.multiscale.utah.edu/common/Zodiac.git
cd Zodiac
mkdir build
```

Run `cmake` inside the `build` directory that was created and install `Zodiac`:
```
cd build
cmake -DCantera_DIR=[path to directory where cantera was installed] -DCMAKE_BUILD_TYPE=Release ..
make install
```

This creates a `zodiac` executable which can be found in the `bin` directory inside the `build` directory that was created.

Instructions for using this executable can be found in the [Zodiac-Manual](doc/Zodiac-Manual.pdf) document inside the [doc](doc) directory of `Zodiac`.